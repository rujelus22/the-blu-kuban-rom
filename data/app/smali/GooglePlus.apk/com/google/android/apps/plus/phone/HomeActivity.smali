.class public Lcom/google/android/apps/plus/phone/HomeActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "HomeActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;
.implements Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/analytics/InstrumentedActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;",
        "Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;"
    }
.end annotation


# static fields
.field private static final REMOVE:Landroid/net/Uri;


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mDestination:Landroid/os/Bundle;

.field private mDestinationState:[Landroid/os/Parcelable;

.field private mDestinationsConfigured:Z

.field private mDialogTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

.field private mNavigationBar:Landroid/widget/ListView;

.field private mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

.field private mNavigationBarScrollPosition:I

.field private mNotificationCount:I

.field private mNotificationsLoaded:Z

.field private mRequestId:Ljava/lang/Integer;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mShakeListener:Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;

.field private mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 67
    const-string v0, "https://plus.google.com/downgrade/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/phone/HomeActivity;->REMOVE:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarScrollPosition:I

    .line 88
    const/16 v0, 0x8

    new-array v0, v0, [Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    .line 788
    new-instance v0, Lcom/google/android/apps/plus/phone/HomeActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/HomeActivity$2;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 92
    new-instance v0, Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/SignOnManager;-><init>(Landroid/support/v4/app/FragmentActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/HomeActivity;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->handleServiceCallback$b5e9bbb(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/HostLayout;
    .registers 2
    .parameter "x0"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/phone/HomeActivity;Landroid/os/Bundle;)Landroid/os/Bundle;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    return-object v0
.end method

.method private buildDestinationBundleForIntent()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 358
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 360
    .local v1, extras:Landroid/os/Bundle;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    .line 361
    if-nez v1, :cond_27

    .line 362
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v4, "destination"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 371
    :goto_19
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v4, "account"

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 372
    return-void

    .line 364
    :cond_27
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 365
    const-string v3, "destination"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 367
    .local v0, destination:I
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v4, "destination"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_19
.end method

.method private configureDestinations()V
    .registers 10

    .prologue
    const v8, 0x7f02010f

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 409
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->removeAllDestinations()V

    .line 411
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const v5, 0x7f02010b

    const v6, 0x7f08005d

    invoke-virtual {v4, v2, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 416
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x5

    const v6, 0x7f020108

    const v7, 0x7f080058

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 421
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 422
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_b0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v4

    if-eqz v4, :cond_b0

    move v1, v3

    .line 423
    .local v1, isPlusPage:Z
    :goto_30
    if-eqz v1, :cond_b3

    .line 424
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v3, v8, v5, v6}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(IILjava/lang/CharSequence;Ljava/lang/String;)V

    .line 436
    :goto_3f
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x7

    const v6, 0x7f02010e

    const v7, 0x7f080059

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 441
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x2

    const v6, 0x7f020109

    const v7, 0x7f08005b

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 447
    if-eqz v0, :cond_72

    if-nez v1, :cond_72

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v4

    if-eqz v4, :cond_72

    .line 449
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x3

    const v6, 0x7f02010a

    const v7, 0x7f08005a

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 455
    :cond_72
    if-nez v1, :cond_80

    .line 456
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x4

    const v6, 0x7f02010d

    const v7, 0x7f080057

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 462
    :cond_80
    invoke-static {}, Lcom/google/android/apps/plus/util/MapUtils;->getPlacesActivityIntent$7ec49240()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/high16 v6, 0x1

    invoke-virtual {v5, v4, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_95

    move v2, v3

    .line 463
    .local v2, localSupported:Z
    :cond_95
    if-eqz v2, :cond_a5

    if-nez v1, :cond_a5

    .line 464
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const/4 v5, 0x6

    const v6, 0x7f02010c

    const v7, 0x7f08005c

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    .line 470
    :cond_a5
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->showDestinations()V

    .line 471
    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationsConfigured:Z

    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->restoreNavigationBarScrollPosition()V

    .line 473
    return-void

    .end local v1           #isPlusPage:Z
    .end local v2           #localSupported:Z
    :cond_b0
    move v1, v2

    .line 422
    goto/16 :goto_30

    .line 430
    .restart local v1       #isPlusPage:Z
    :cond_b3
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    const v5, 0x7f080056

    invoke-virtual {v4, v3, v8, v5}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(III)V

    goto :goto_3f
.end method

.method private handleServiceCallback$b5e9bbb(I)V
    .registers 3
    .parameter "requestId"

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_d

    .line 806
    :cond_c
    :goto_c
    return-void

    .line 803
    :cond_d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    .line 805
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    goto :goto_c
.end method

.method private static isLauncherIntent(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    .prologue
    .line 346
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_26

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v0

    const-string v1, "android.intent.category.LAUNCHER"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_26

    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25
.end method

.method private navigateToDestination(ILandroid/os/Bundle;ZLandroid/support/v4/app/Fragment$SavedState;)V
    .registers 16
    .parameter "destinationId"
    .parameter "args"
    .parameter "animate"
    .parameter "savedState"

    .prologue
    .line 529
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 530
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_19

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v5

    if-eqz v5, :cond_19

    const/4 v4, 0x1

    .line 533
    .local v4, isPlusPage:Z
    :goto_f
    const/4 v5, 0x4

    if-ne p1, v5, :cond_15

    if-eqz v4, :cond_15

    .line 534
    const/4 p1, 0x0

    .line 537
    :cond_15
    packed-switch p1, :pswitch_data_14c

    .line 621
    :cond_18
    :goto_18
    :pswitch_18
    return-void

    .line 530
    .end local v4           #isPlusPage:Z
    :cond_19
    const/4 v4, 0x0

    goto :goto_f

    .line 539
    .restart local v4       #isPlusPage:Z
    :pswitch_1b
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;-><init>()V

    .line 540
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->setArguments(Landroid/os/Bundle;)V

    .line 541
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto :goto_18

    .line 546
    .end local v2           #fragment:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
    :pswitch_29
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;-><init>()V

    .line 547
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    const-string v5, "person_id"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_45

    .line 548
    const-string v5, "person_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_45
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setArguments(Landroid/os/Bundle;)V

    .line 551
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto :goto_18

    .line 556
    .end local v2           #fragment:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
    :pswitch_4e
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;-><init>()V

    .line 557
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;
    const-string v5, "person_id"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6a

    .line 558
    const-string v5, "person_id"

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :cond_6a
    const-string v5, "photos_home"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_78

    .line 562
    const-string v5, "photos_home"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 564
    :cond_78
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 565
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto :goto_18

    .line 570
    .end local v2           #fragment:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;
    :pswitch_81
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;-><init>()V

    .line 571
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
    const-string v5, "refresh"

    const/4 v6, 0x1

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 572
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedEventListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 573
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto :goto_18

    .line 578
    .end local v2           #fragment:Lcom/google/android/apps/plus/fragments/HostedEventListFragment;
    :pswitch_95
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 582
    .local v1, context:Landroid/content/Context;
    if-eqz v0, :cond_ba

    const/4 v5, 0x0

    :try_start_9c
    invoke-static {v1, v0, v5}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v5

    if-eqz v5, :cond_ba

    .line 586
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    .line 588
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;-><init>()V

    .line 589
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->setArguments(Landroid/os/Bundle;)V

    .line 590
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    .line 594
    .end local v2           #fragment:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
    :cond_ba
    if-eqz v0, :cond_18

    const/4 v5, 0x0

    invoke-static {v1, v0, v5}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-ne v5, v6, :cond_18

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v5

    if-eqz v5, :cond_18

    .line 598
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommService;->getNotificationIntent()Landroid/content/Intent;

    move-result-object v3

    .line 600
    .local v3, intent:Landroid/content/Intent;
    if-eqz v3, :cond_18

    .line 601
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_e0
    .catch Ljava/lang/LinkageError; {:try_start_9c .. :try_end_e0} :catch_e2

    goto/16 :goto_18

    .line 605
    .end local v3           #intent:Landroid/content/Intent;
    :catch_e2
    move-exception v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0802f5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const-string v7, "showError: message=%s finishOnOk=%s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    const/4 v9, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0801c4

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const v10, 0x1080027

    invoke-static {v7, v5, v8, v9, v10}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    new-instance v7, Lcom/google/android/apps/plus/phone/HomeActivity$3;

    invoke-direct {v7, p0, v6}, Lcom/google/android/apps/plus/phone/HomeActivity$3;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;Z)V

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "error"

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_18

    .line 611
    .end local v1           #context:Landroid/content/Context;
    :pswitch_12c
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;-><init>(Z)V

    .line 612
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setArguments(Landroid/os/Bundle;)V

    .line 613
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v5, v2, p3, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto/16 :goto_18

    .line 618
    .end local v2           #fragment:Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
    :pswitch_13c
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;-><init>()V

    .line 619
    .local v2, fragment:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 620
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    const/4 v6, 0x1

    invoke-virtual {v5, v2, v6, p4}, Lcom/google/android/apps/plus/views/HostLayout;->showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto/16 :goto_18

    .line 537
    :pswitch_data_14c
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_29
        :pswitch_81
        :pswitch_95
        :pswitch_13c
        :pswitch_12c
        :pswitch_18
        :pswitch_4e
    .end packed-switch
.end method

.method private restoreNavigationBarScrollPosition()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 827
    iget v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarScrollPosition:I

    if-eq v0, v2, :cond_16

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationsConfigured:Z

    if-eqz v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationsLoaded:Z

    if-eqz v0, :cond_16

    .line 828
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarScrollPosition:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 829
    iput v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarScrollPosition:I

    .line 831
    :cond_16
    return-void
.end method

.method private saveDestinationState()V
    .registers 5

    .prologue
    const/4 v3, -0x1

    .line 257
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    if-eqz v1, :cond_19

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 259
    .local v0, destination:I
    if-eq v0, v3, :cond_19

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostLayout;->saveHostedFragmentState()Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v2

    aput-object v2, v1, v0

    .line 263
    .end local v0           #destination:I
    :cond_19
    return-void
.end method

.method private showCurrentDestination()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz v1, :cond_a

    .line 396
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->dismissPopupMenus()V

    .line 399
    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 401
    :cond_16
    :goto_16
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 402
    .local v0, destinationId:I
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->navigateToDestination(ILandroid/os/Bundle;ZLandroid/support/v4/app/Fragment$SavedState;)V

    .line 403
    return-void

    .line 399
    .end local v0           #destinationId:I
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2f
    :goto_2f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_47

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismissAllowingStateLoss()V

    goto :goto_2f

    :cond_47
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    goto :goto_16
.end method

.method private updateNotificationsSpinner()V
    .registers 2

    .prologue
    .line 812
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    if-eqz v0, :cond_d

    .line 813
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->showProgressIndicator()V

    .line 819
    :cond_d
    :goto_d
    return-void

    .line 816
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->hideProgressIndicator()V

    goto :goto_d
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 898
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 906
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/SignOnManager;->onActivityResult$6eb84b56(II)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 744
    :goto_8
    return-void

    .line 743
    :cond_9
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_8
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    if-eqz v0, :cond_10

    instance-of v0, p1, Lcom/google/android/apps/plus/phone/HostedFragment;

    if-eqz v0, :cond_10

    .line 380
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/phone/HostedFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/HostLayout;->onAttachFragment(Lcom/google/android/apps/plus/phone/HostedFragment;)V

    .line 383
    :cond_10
    instance-of v0, p1, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_28

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    if-nez v0, :cond_1f

    .line 385
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    .line 387
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDialogTags:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    :cond_28
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNavigationBarVisible()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    .line 281
    :cond_d
    :goto_d
    return-void

    .line 275
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_d

    .line 280
    :cond_22
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onBackPressed()V

    goto :goto_d
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 100
    if-eqz p1, :cond_f

    .line 101
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 104
    :cond_f
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->isTaskRoot()Z

    move-result v2

    if-nez v2, :cond_33

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v5, "destination"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 113
    .local v0, destination:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->isLauncherIntent(Landroid/content/Intent;)Z

    move-result v2

    if-nez v2, :cond_2f

    const/4 v2, 0x4

    if-ne v0, v2, :cond_33

    .line 114
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    .line 198
    .end local v0           #destination:I
    :cond_32
    :goto_32
    return-void

    .line 119
    :cond_33
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v2, p1, v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->onCreate(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_32

    .line 125
    const v2, 0x7f03004a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->setContentView(I)V

    .line 127
    const v2, 0x7f090105

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/HostLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    .line 128
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/HostLayout;->setListener(Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;)V

    .line 130
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostLayout;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    .line 131
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V

    .line 134
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    const v5, 0x7f0803e2

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/HostActionBar;->setUpButtonContentDescription(Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostLayout;->getNavigationBar()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    .line 137
    new-instance v2, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 139
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->configureDestinations()V

    .line 143
    if-eqz p1, :cond_e9

    .line 144
    const-string v2, "reqId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a7

    .line 145
    const-string v2, "reqId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    .line 147
    :cond_a7
    const-string v2, "scrollPos"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarScrollPosition:I

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostLayout;->attachActionBar()V

    .line 166
    :cond_b4
    :goto_b4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    if-eqz v2, :cond_c4

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 170
    :cond_c4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationCount:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->setNotificationCount(I)V

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v1

    .line 174
    .local v1, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v1, :cond_32

    .line 175
    new-instance v2, Lcom/google/android/apps/plus/phone/HomeActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/HomeActivity$1;-><init>(Lcom/google/android/apps/plus/phone/HomeActivity;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mShakeListener:Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mShakeListener:Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/ShakeDetector;->addEventListener(Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;)V

    .line 196
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    goto/16 :goto_32

    .line 153
    .end local v1           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_e9
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->isSignedIn()Z

    move-result v2

    if-eqz v2, :cond_b4

    .line 154
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->buildDestinationBundleForIntent()V

    .line 155
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->showCurrentDestination()V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v5, "show_notifications"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_108

    .line 158
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostLayout;->showNavigationBarDelayed()V

    .line 161
    :cond_108
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    if-eqz v2, :cond_144

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v2

    if-eqz v2, :cond_144

    move v5, v3

    :goto_117
    if-nez v5, :cond_148

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncPreferenceSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v2

    if-nez v2, :cond_146

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_146

    move v2, v3

    :goto_12c
    if-eqz v2, :cond_148

    new-instance v2, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;-><init>(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v5, "new_features"

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_b4

    :cond_144
    move v5, v4

    goto :goto_117

    :cond_146
    move v2, v4

    goto :goto_12c

    :cond_148
    if-nez v5, :cond_b4

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v2, v5, :cond_181

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncPreferenceSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v2

    if-nez v2, :cond_181

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_181

    move v2, v3

    :goto_163
    if-eqz v2, :cond_b4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/google/android/apps/plus/phone/ContactsSyncConfigActivity;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "account"

    invoke-virtual {v3, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_b4

    :cond_181
    move v2, v4

    goto :goto_163
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "arg"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 751
    packed-switch p1, :pswitch_data_20

    .line 765
    :cond_4
    :goto_4
    return-object v4

    .line 753
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    .line 754
    .local v7, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v7, :cond_4

    .line 758
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 760
    .local v2, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "timestamp DESC"

    move-object v1, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_4

    .line 751
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 653
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100013

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 654
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 205
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onDestroy()V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 207
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_15

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mShakeListener:Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->removeEventListener(Lcom/google/android/apps/plus/phone/ShakeDetector$ShakeEventListener;)V

    .line 209
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    .line 211
    :cond_15
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "item"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, view:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    .line 494
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->isNotificationHeader(I)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 495
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/EsService;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->updateNotificationsSpinner()V

    .line 522
    :cond_18
    :goto_18
    return-void

    .line 497
    :cond_19
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getDestinationId(I)I

    move-result v0

    .line 498
    .local v0, destinationId:I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_70

    .line 499
    const/4 v1, 0x6

    if-ne v0, v1, :cond_32

    .line 500
    invoke-static {}, Lcom/google/android/apps/plus/util/MapUtils;->getPlacesActivityIntent$7ec49240()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    goto :goto_18

    .line 505
    :cond_32
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    if-eqz v1, :cond_46

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_46

    .line 507
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    goto :goto_18

    .line 511
    :cond_46
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->saveDestinationState()V

    .line 513
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    .line 514
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 515
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    const-string v2, "destination"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 516
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestination:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    aget-object v1, v1, v0

    check-cast v1, Landroid/support/v4/app/Fragment$SavedState;

    invoke-direct {p0, v0, v2, v5, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->navigateToDestination(ILandroid/os/Bundle;ZLandroid/support/v4/app/Fragment$SavedState;)V

    goto :goto_18

    .line 519
    :cond_70
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    if-eqz v1, :cond_18

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {p0, v2, v1}, Lcom/google/android/apps/plus/service/AndroidNotification;->newViewNotificationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_18

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xb

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eq v1, v5, :cond_9b

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v3}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_9b
    const-string v1, "com.google.plus.analytics.intent.extra.START_VIEW"

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    invoke-virtual {v2, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_18
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->setNotifications(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getUnreadNotificationCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationCount:I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationCount:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->setNotificationCount(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNotificationsLoaded:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->restoreNavigationBarScrollPosition()V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 786
    .local p1, arg0:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onNavigationBarVisibilityChange(Z)V
    .registers 4
    .parameter "visible"

    .prologue
    .line 480
    if-nez p1, :cond_19

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBarAdapter:Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getUnreadNotificationCount()I

    move-result v1

    if-lez v1, :cond_19

    .line 482
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 483
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_19

    .line 484
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->tellServerNotificationsWereRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    .line 487
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_19
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->isSignedIn()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->isLauncherIntent(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 224
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->buildDestinationBundleForIntent()V

    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->showCurrentDestination()V

    .line 228
    const-string v0, "show_notifications"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->showNavigationBarDelayed()V

    .line 232
    :cond_28
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 692
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v4, p1}, Lcom/google/android/apps/plus/views/HostLayout;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 729
    :goto_9
    return v3

    .line 696
    :cond_a
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_5e

    .line 729
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_9

    .line 698
    :sswitch_16
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 699
    .local v2, intent:Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_9

    .line 704
    .end local v2           #intent:Landroid/content/Intent;
    :sswitch_25
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 705
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    goto :goto_9

    .line 710
    :sswitch_2e
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {p0, v4}, Lcom/google/android/apps/plus/phone/Intents;->getSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    .line 711
    .restart local v2       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_9

    .line 716
    .end local v2           #intent:Landroid/content/Intent;
    :sswitch_3c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08040f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 718
    .local v1, helpUrlParam:Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 719
    .local v0, helpUrl:Landroid/net/Uri;
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/HomeActivity;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_9

    .line 724
    .end local v0           #helpUrl:Landroid/net/Uri;
    .end local v1           #helpUrlParam:Ljava/lang/String;
    :sswitch_56
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/SignOnManager;->signOut(Z)V

    goto :goto_9

    .line 696
    nop

    :sswitch_data_5e
    .sparse-switch
        0x7f09028e -> :sswitch_2e
        0x7f09028f -> :sswitch_25
        0x7f090290 -> :sswitch_3c
        0x7f09029a -> :sswitch_16
        0x7f0902c6 -> :sswitch_56
    .end sparse-switch
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 337
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onPause()V

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->onPause()V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 340
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 7
    .parameter "menu"

    .prologue
    const/4 v4, 0x1

    .line 664
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    .line 665
    .local v2, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_6
    if-ge v0, v2, :cond_1e

    .line 666
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 667
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_24

    .line 676
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 665
    :goto_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 673
    :sswitch_1a
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_17

    .line 682
    .end local v1           #item:Landroid/view/MenuItem;
    :cond_1e
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/plus/views/HostLayout;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 684
    return v4

    .line 667
    :sswitch_data_24
    .sparse-switch
        0x7f09028e -> :sswitch_1a
        0x7f09028f -> :sswitch_1a
        0x7f090290 -> :sswitch_1a
        0x7f09029a -> :sswitch_1a
        0x7f0902c6 -> :sswitch_1a
    .end sparse-switch
.end method

.method protected onResume()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 303
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onResume()V

    .line 304
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->onResume()Z

    move-result v0

    .line 305
    .local v0, signInJustCompleted:Z
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_32

    .line 308
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_32

    .line 309
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->handleServiceCallback$b5e9bbb(I)V

    .line 313
    :cond_32
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "sign_out"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->signOut(Z)V

    .line 315
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    sget-object v3, Lcom/google/android/apps/plus/phone/HomeActivity;->REMOVE:Landroid/net/Uri;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->startExternalActivity(Landroid/content/Intent;)V

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    .line 330
    :cond_53
    :goto_53
    return-void

    .line 320
    :cond_54
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mSignOnManager:Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_61

    .line 321
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mDestinationState:[Landroid/os/Parcelable;

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 324
    :cond_61
    if-eqz v0, :cond_53

    .line 325
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 326
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->configureDestinations()V

    .line 327
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->buildDestinationBundleForIntent()V

    .line 328
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->showCurrentDestination()V

    goto :goto_53
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 239
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 241
    const-string v0, "reqId"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 243
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    if-eqz v0, :cond_21

    .line 244
    const-string v0, "scrollPos"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 248
    :cond_21
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HomeActivity;->saveDestinationState()V

    .line 251
    return-void
.end method

.method public final onUpButtonClick()V
    .registers 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNavigationBarVisible()Z

    move-result v0

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onUpButtonClicked()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 295
    :goto_1c
    return-void

    .line 294
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->toggleNavigationBarVisibility()V

    goto :goto_1c
.end method
