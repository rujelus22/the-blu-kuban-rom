.class public abstract Lcom/google/android/apps/plus/phone/PhotoCursorLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PhotoCursorLoader.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/Pageable;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mAlbumId:Ljava/lang/String;

.field private mCircleOffset:I

.field private mDataSourceIsLoading:Z

.field private final mEventId:Ljava/lang/String;

.field private mEventResumeToken:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private mHasMore:Z

.field private final mInitialPageCount:I

.field private mIsLoadingMore:Z

.field private mLoadLimit:I

.field private mNetworkRequestMade:Z

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private mPageable:Z

.field mPageableLoadingListener:Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;

.field private final mPaging:Z

.field private final mPhotoOfUserGaiaId:Ljava/lang/String;

.field private final mPhotoUrl:Ljava/lang/String;

.field private final mStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "ownerGaiaId"
    .parameter "albumId"
    .parameter "photoOfUserGaiaId"
    .parameter "streamId"
    .parameter "eventId"
    .parameter "photoUrl"
    .parameter "paging"
    .parameter "initialPageCount"

    .prologue
    const/4 v0, -0x1

    .line 97
    invoke-static/range {p3 .. p8}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getNotificationUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 67
    new-instance v1, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v1, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 74
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHandler:Landroid/os/Handler;

    .line 82
    const/16 v1, 0x10

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    .line 99
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 100
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    .line 101
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    .line 102
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    .line 103
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    .line 104
    iput-object p6, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    .line 105
    iput-object p8, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoUrl:Ljava/lang/String;

    .line 106
    iput-boolean p9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPaging:Z

    .line 107
    iput-boolean p9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    .line 108
    iput p10, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mInitialPageCount:I

    .line 109
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v1, :cond_3a

    if-eq p10, v0, :cond_3a

    mul-int/lit8 v0, p10, 0x10

    :cond_3a
    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    .line 111
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/PhotoCursorLoader;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mDataSourceIsLoading:Z

    return p1
.end method

.method private doNetworkRequest()V
    .registers 15

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 271
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mNetworkRequestMade:Z

    if-eqz v0, :cond_8

    .line 309
    :goto_7
    return-void

    .line 274
    :cond_8
    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mNetworkRequestMade:Z

    .line 276
    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->invokeLoadingListener(Z)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_29

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v8, v3

    move-object v9, v3

    invoke-static/range {v0 .. v9}, Lcom/google/android/apps/plus/content/EsEventData;->readEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)Z

    .line 282
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->updateEventResumeToken()V

    .line 283
    iput-boolean v6, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mNetworkRequestMade:Z

    .line 308
    :goto_25
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->invokeLoadingListener(Z)V

    goto :goto_7

    .line 285
    :cond_29
    const/4 v7, 0x0

    .line 287
    .local v7, op:Lcom/google/android/apps/plus/network/HttpOperation;
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    if-eqz v0, :cond_4f

    const-string v0, "profiles_scrapbook"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4f

    .line 288
    new-instance v7, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    .end local v7           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    move-object v12, v3

    move-object v13, v3

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 298
    .restart local v7       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    :cond_49
    :goto_49
    if-eqz v7, :cond_79

    .line 301
    invoke-virtual {v7}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    goto :goto_25

    .line 290
    :cond_4f
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_63

    .line 291
    new-instance v7, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    .end local v7           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    move-object v11, v3

    move-object v12, v3

    invoke-direct/range {v7 .. v12}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .restart local v7       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    goto :goto_49

    .line 293
    :cond_63
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_49

    .line 294
    new-instance v7, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    .end local v7           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    move-object v12, v3

    move-object v13, v3

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .restart local v7       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    goto :goto_49

    .line 304
    :cond_79
    const-string v0, "PhotoCursorLoader"

    const-string v1, "No valid IDs to load photos for"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_25
.end method

.method private static getNotificationUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .registers 10
    .parameter "ownerGaiaId"
    .parameter "albumId"
    .parameter "photoOfUserId"
    .parameter "streamId"
    .parameter "eventId"
    .parameter "photoUrl"

    .prologue
    .line 350
    if-eqz p3, :cond_28

    const-string v2, "profiles_scrapbook"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28

    .line 351
    if-nez p0, :cond_15

    .line 352
    const-string v2, "PhotoCursorLoader"

    const-string v3, "Viewing stream photos w/o a valid owner GAIA ID"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :cond_13
    :goto_13
    const/4 v1, 0x0

    .line 378
    .local v1, notificationUri:Landroid/net/Uri;
    :goto_14
    return-object v1

    .line 355
    .end local v1           #notificationUri:Landroid/net/Uri;
    :cond_15
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 356
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, p0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 358
    .restart local v1       #notificationUri:Landroid/net/Uri;
    goto :goto_14

    .line 359
    .end local v0           #builder:Landroid/net/Uri$Builder;
    .end local v1           #notificationUri:Landroid/net/Uri;
    :cond_28
    if-eqz p4, :cond_31

    .line 360
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #notificationUri:Landroid/net/Uri;
    goto :goto_14

    .line 362
    .end local v1           #notificationUri:Landroid/net/Uri;
    :cond_31
    if-eqz p2, :cond_3a

    .line 363
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #notificationUri:Landroid/net/Uri;
    goto :goto_14

    .line 365
    .end local v1           #notificationUri:Landroid/net/Uri;
    :cond_3a
    if-eqz p1, :cond_13

    .line 366
    if-nez p0, :cond_46

    .line 367
    const-string v2, "PhotoCursorLoader"

    const-string v3, "Viewing album photos w/o a valid owner GAIA ID"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_13

    .line 370
    :cond_46
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #notificationUri:Landroid/net/Uri;
    goto :goto_14
.end method

.method private invokeLoadingListener(Z)V
    .registers 4
    .parameter "isActive"

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/phone/PhotoCursorLoader$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoCursorLoader;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 249
    return-void
.end method

.method private updateEventResumeToken()V
    .registers 6

    .prologue
    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DetailsQuery;->PROJECTION:[Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 116
    .local v0, eventCursor:Landroid/database/Cursor;
    if-eqz v0, :cond_1d

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 117
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    .line 120
    :cond_1d
    return-void
.end method


# virtual methods
.method public esLoadInBackground()Landroid/database/Cursor;
    .registers 12

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getUri()Landroid/net/Uri;

    move-result-object v9

    if-nez v9, :cond_19

    .line 125
    const-string v7, "PhotoCursorLoader"

    const-string v8, "load NULL URI; return empty cursor"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    new-instance v5, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getProjection()[Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 191
    :cond_18
    :goto_18
    return-object v5

    .line 129
    :cond_19
    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    if-eqz v9, :cond_2c

    .line 130
    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mIsLoadingMore:Z

    if-eqz v9, :cond_d3

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_d3

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->doNetworkRequest()V

    .line 137
    :cond_2c
    :goto_2c
    iget v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    .line 138
    .local v3, loadLimit:I
    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v9, :cond_d8

    iget v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_d8

    move v0, v7

    .line 139
    .local v0, changeSortOrder:Z
    :goto_38
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getSortOrder()Ljava/lang/String;

    move-result-object v4

    .line 142
    .local v4, origSortOrder:Ljava/lang/String;
    if-nez v4, :cond_51

    .line 143
    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    const-string v10, "posts"

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_e3

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    if-eqz v9, :cond_db

    const-string v9, "pending_status DESC,timestamp ASC"

    :goto_4e
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->setSortOrder(Ljava/lang/String;)V

    .line 147
    :cond_51
    if-eqz v0, :cond_73

    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getSortOrder()Ljava/lang/String;

    move-result-object v6

    .line 151
    .local v6, sortOrder:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v6, :cond_e7

    .end local v6           #sortOrder:Ljava/lang/String;
    :goto_5e
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LIMIT 0, "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->setSortOrder(Ljava/lang/String;)V

    .line 154
    :cond_73
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v5

    .line 156
    .local v5, returnCursor:Landroid/database/Cursor;
    if-eqz v5, :cond_eb

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 157
    .local v1, cursorCount:I
    :goto_7d
    if-ne v1, v3, :cond_ed

    move v2, v7

    .line 158
    .local v2, cursorFull:Z
    :goto_80
    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v9, :cond_ef

    if-nez v2, :cond_8e

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_ef

    :cond_8e
    move v9, v7

    :goto_8f
    iput-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    .line 159
    iput-boolean v8, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mIsLoadingMore:Z

    .line 162
    if-nez v1, :cond_99

    .line 163
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 164
    const/4 v5, 0x0

    .line 168
    :cond_99
    if-nez v5, :cond_cc

    .line 170
    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mCircleOffset:I

    .line 173
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->doNetworkRequest()V

    .line 176
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v5

    .line 178
    if-eqz v5, :cond_f1

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 179
    :goto_aa
    if-ne v1, v3, :cond_f3

    move v2, v7

    .line 182
    :goto_ad
    iget v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mCircleOffset:I

    if-ne v1, v9, :cond_b9

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_f5

    :cond_b9
    move v9, v7

    :goto_ba
    iput-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    .line 183
    iget-boolean v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v9, :cond_f7

    if-nez v2, :cond_ca

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventResumeToken:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_f7

    :cond_ca
    :goto_ca
    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    .line 187
    :cond_cc
    if-eqz v0, :cond_18

    .line 188
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->setSortOrder(Ljava/lang/String;)V

    goto/16 :goto_18

    .line 133
    .end local v0           #changeSortOrder:Z
    .end local v1           #cursorCount:I
    .end local v2           #cursorFull:Z
    .end local v3           #loadLimit:I
    .end local v4           #origSortOrder:Ljava/lang/String;
    .end local v5           #returnCursor:Landroid/database/Cursor;
    :cond_d3
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->updateEventResumeToken()V

    goto/16 :goto_2c

    .restart local v3       #loadLimit:I
    :cond_d8
    move v0, v8

    .line 138
    goto/16 :goto_38

    .line 143
    .restart local v0       #changeSortOrder:Z
    .restart local v4       #origSortOrder:Ljava/lang/String;
    :cond_db
    iget-object v9, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    if-eqz v9, :cond_e3

    const-string v9, "timestamp DESC"

    goto/16 :goto_4e

    :cond_e3
    const-string v9, "pending_status DESC,timestamp DESC"

    goto/16 :goto_4e

    .line 151
    .restart local v6       #sortOrder:Ljava/lang/String;
    :cond_e7
    const-string v6, ""

    goto/16 :goto_5e

    .end local v6           #sortOrder:Ljava/lang/String;
    .restart local v5       #returnCursor:Landroid/database/Cursor;
    :cond_eb
    move v1, v8

    .line 156
    goto :goto_7d

    .restart local v1       #cursorCount:I
    :cond_ed
    move v2, v8

    .line 157
    goto :goto_80

    .restart local v2       #cursorFull:Z
    :cond_ef
    move v9, v8

    .line 158
    goto :goto_8f

    :cond_f1
    move v1, v8

    .line 178
    goto :goto_aa

    :cond_f3
    move v2, v8

    .line 179
    goto :goto_ad

    :cond_f5
    move v9, v8

    .line 182
    goto :goto_ba

    :cond_f7
    move v7, v8

    .line 183
    goto :goto_ca
.end method

.method public final getCurrentPage()I
    .registers 3

    .prologue
    const/4 v0, -0x1

    .line 254
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v1, :cond_d

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    if-eq v1, v0, :cond_d

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    div-int/lit8 v0, v0, 0x10

    :cond_d
    return v0
.end method

.method final getLoaderUri()Landroid/net/Uri;
    .registers 9

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mOwnerGaiaId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAlbumId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoOfUserGaiaId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mStreamId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mEventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPhotoUrl:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getNotificationUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 320
    .local v7, notificationUri:Landroid/net/Uri;
    if-eqz v7, :cond_19

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    .line 325
    .local v6, loaderUri:Landroid/net/Uri;
    :goto_18
    return-object v6

    .line 323
    .end local v6           #loaderUri:Landroid/net/Uri;
    :cond_19
    const/4 v6, 0x0

    .restart local v6       #loaderUri:Landroid/net/Uri;
    goto :goto_18
.end method

.method public final hasMore()Z
    .registers 2

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final isDataSourceLoading()Z
    .registers 2

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mDataSourceIsLoading:Z

    return v0
.end method

.method public final loadMore()V
    .registers 2

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageable:Z

    if-eqz v0, :cond_14

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mHasMore:Z

    if-eqz v0, :cond_14

    .line 197
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    add-int/lit8 v0, v0, 0x30

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mLoadLimit:I

    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mIsLoadingMore:Z

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->onContentChanged()V

    .line 201
    :cond_14
    return-void
.end method

.method protected final onAbandon()V
    .registers 3

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_14

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 223
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    .line 226
    :cond_14
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->onAbandon()V

    .line 227
    return-void
.end method

.method protected final onStartLoading()V
    .registers 5

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    if-nez v0, :cond_17

    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mObserverRegistered:Z

    .line 216
    :cond_17
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->onStartLoading()V

    .line 217
    return-void
.end method

.method public final setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->mPageableLoadingListener:Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;

    .line 237
    return-void
.end method
