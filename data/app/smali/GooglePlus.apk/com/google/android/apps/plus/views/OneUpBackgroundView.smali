.class public abstract Lcom/google/android/apps/plus/views/OneUpBackgroundView;
.super Landroid/view/View;
.source "OneUpBackgroundView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;
    }
.end annotation


# static fields
.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field protected static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field protected static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field protected mHasSeenImage:Z

.field protected mListener:Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;

.field protected mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mShouldTriggerViewLoaded:Z

.field protected mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 34
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    sget-object v0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    if-nez v0, :cond_d

    .line 57
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 59
    :cond_d
    return-void
.end method


# virtual methods
.method public final init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;)V
    .registers 6
    .parameter "mediaRef"
    .parameter "type"
    .parameter "listener"

    .prologue
    .line 62
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_f

    .line 63
    const v0, 0x3a83126f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->setAlpha(F)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mHasSeenImage:Z

    .line 67
    :cond_f
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 68
    iput p2, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mType:I

    .line 69
    iput-object p3, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mListener:Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->requestLayout()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->invalidate()V

    .line 72
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 94
    .local v0, bitmap:Landroid/graphics/Bitmap;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mShouldTriggerViewLoaded:Z

    if-nez v1, :cond_18

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mHasSeenImage:Z

    if-nez v1, :cond_24

    if-eqz v0, :cond_24

    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mListener:Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;

    if-eqz v1, :cond_24

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mListener:Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;->onBackgroundViewLoaded(Lcom/google/android/apps/plus/views/OneUpBackgroundView;)V

    .line 96
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mShouldTriggerViewLoaded:Z

    .line 99
    :cond_24
    if-eqz v0, :cond_53

    .line 100
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mHasSeenImage:Z

    if-nez v1, :cond_53

    .line 101
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mHasSeenImage:Z

    .line 102
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_53

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    if-nez v1, :cond_3e

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    :cond_3e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f80

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 105
    :cond_53
    return-void
.end method

.method protected final onImageChanged()V
    .registers 2

    .prologue
    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mHasSeenImage:Z

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mShouldTriggerViewLoaded:Z

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->invalidate()V

    .line 122
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_9

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 78
    :cond_9
    return-void
.end method

.method public final onStop()V
    .registers 4

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_10

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/MediaImage;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->mMediaImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 85
    :cond_10
    return-void
.end method
