.class final Lcom/google/android/apps/plus/content/EsNotificationData$1;
.super Ljava/lang/Object;
.source "EsNotificationData.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 944
    check-cast p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    .end local p1
    check-cast p2, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    .end local p2
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, -0x1

    goto :goto_15
.end method
