.class public abstract Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.super Lcom/android/common/widget/CompositeCursorAdapter;
.source "PeopleSearchAdapter.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/Filterable;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/common/widget/CompositeCursorAdapter;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/Filterable;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;"
    }
.end annotation


# static fields
.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_PROJECTION_WITH_PHONE:[Ljava/lang/String;

.field private static final GAIA_ID_CIRCLE_PROJECTION:[Ljava/lang/String;

.field private static final LOCAL_PROFILE_PROJECTION:[Ljava/lang/String;

.field public static final PEOPLE_PROJECTION:[Ljava/lang/String;

.field private static final PUBLIC_PROFILE_PROJECTION:[Ljava/lang/String;


# instance fields
.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActiveLoaderCount:I

.field private mActivityId:Ljava/lang/String;

.field protected mAddToCirclesActionEnabled:Z

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field protected final mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCircleUsageType:I

.field private mCirclesError:Z

.field private mCirclesLoaded:Z

.field private final mCirclesLoaderId:I

.field private mContactsCursor:Landroid/database/Cursor;

.field private mContactsError:Z

.field private mContactsLoaded:Z

.field private final mContactsLoaderId:I

.field private mFilter:Landroid/widget/Filter;

.field private volatile mFilterLatch:Ljava/util/concurrent/CountDownLatch;

.field private mFilterNullGaiaIds:Z

.field private final mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mGaiaIdCircleCursor:Landroid/database/Cursor;

.field private final mGaiaIdLoaderId:I

.field private final mHandler:Landroid/os/Handler;

.field private mIncludePeopleInCircles:Z

.field protected mIncludePhoneNumberContacts:Z

.field private mIncludePlusPages:Z

.field private mIsMentionsAdapter:Z

.field protected mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

.field private final mLoaderManager:Landroid/support/v4/app/LoaderManager;

.field private mLocalProfileError:Z

.field private mLocalProfilesCursor:Landroid/database/Cursor;

.field private mLocalProfilesLoaded:Z

.field private final mPeopleLoaderId:I

.field private final mProfilesLoaderId:I

.field private mPublicProfileSearchEnabled:Z

.field private mPublicProfilesCursor:Landroid/database/Cursor;

.field private mPublicProfilesError:Z

.field private mPublicProfilesLoading:Z

.field private mPublicProfilesNotFound:Z

.field protected mQuery:Ljava/lang/String;

.field private mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

.field private mResultsPreserved:Z

.field private mShowPersonNameDialog:Z

.field private mShowProgressWhenEmpty:Z


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 69
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "circle_id"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "circle_name"

    aput-object v1, v0, v6

    const-string v1, "contact_count"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CIRCLES_PROJECTION:[Ljava/lang/String;

    .line 83
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "blocked"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->PEOPLE_PROJECTION:[Ljava/lang/String;

    .line 102
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->LOCAL_PROFILE_PROJECTION:[Ljava/lang/String;

    .line 121
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;

    .line 128
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    const-string v1, "phone"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION_WITH_PHONE:[Ljava/lang/String;

    .line 144
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v3

    const-string v1, "packed_circle_ids"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->GAIA_ID_CIRCLE_PROJECTION:[Ljava/lang/String;

    .line 152
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "snippet"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->PUBLIC_PROFILE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 11
    .parameter "context"
    .parameter "fragmentManager"
    .parameter "loaderManager"
    .parameter "account"

    .prologue
    .line 366
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    .line 367
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .registers 14
    .parameter "context"
    .parameter "fragmentManager"
    .parameter "loaderManager"
    .parameter "account"
    .parameter "instanceId"

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 381
    invoke-direct {p0, p1, v7}, Lcom/android/common/widget/CompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    .line 198
    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    .line 215
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowProgressWhenEmpty:Z

    .line 218
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    .line 225
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    .line 233
    new-instance v5, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-direct {v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    .line 242
    new-instance v5, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    .line 309
    new-instance v5, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$2;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleContentObserver:Landroid/database/DataSetObserver;

    .line 382
    const/4 v0, 0x0

    .local v0, i:I
    :goto_24
    const/4 v5, 0x6

    if-ge v0, v5, :cond_2d

    .line 383
    invoke-virtual {p0, v7, v7}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->addPartition(ZZ)V

    .line 382
    add-int/lit8 v0, v0, 0x1

    goto :goto_24

    .line 386
    :cond_2d
    mul-int/lit8 v5, p5, 0xa

    add-int/lit16 v1, v5, 0x400

    .line 387
    .local v1, loaderId:I
    add-int/lit8 v2, v1, 0x1

    .end local v1           #loaderId:I
    .local v2, loaderId:I
    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    .line 388
    add-int/lit8 v1, v2, 0x1

    .end local v2           #loaderId:I
    .restart local v1       #loaderId:I
    iput v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    .line 389
    add-int/lit8 v2, v1, 0x1

    .end local v1           #loaderId:I
    .restart local v2       #loaderId:I
    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    .line 390
    add-int/lit8 v1, v2, 0x1

    .end local v2           #loaderId:I
    .restart local v1       #loaderId:I
    iput v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    .line 391
    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    .line 393
    const-string v5, "people_search_results"

    invoke-virtual {p2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;

    .line 395
    .local v4, savedInstance:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;
    if-nez v4, :cond_8d

    .line 396
    new-instance v4, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;

    .end local v4           #savedInstance:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;
    invoke-direct {v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;-><init>()V

    .line 397
    .restart local v4       #savedInstance:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;
    invoke-virtual {p2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const-string v6, "people_search_results"

    invoke-virtual {v5, v4, v6}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 408
    :cond_5f
    :goto_5f
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->setPeopleSearchResults(Lcom/google/android/apps/plus/fragments/PeopleSearchResults;)V

    .line 410
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 411
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    .line 412
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 413
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setMyProfile(Ljava/lang/String;)V

    .line 414
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setIncludePeopleInCircles(Z)V

    .line 415
    new-instance v5, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v5, p1, p3, v6, p5}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 416
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 417
    return-void

    .line 400
    :cond_8d
    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->getPeopleSearchResults()Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    move-result-object v3

    .line 401
    .local v3, results:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
    if-eqz v3, :cond_5f

    .line 402
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    .line 403
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getQuery()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    .line 404
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResultsPreserved:Z

    goto :goto_5f
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    :cond_f
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;

    if-eqz v0, :cond_30

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_30

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->getToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_30

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    :cond_30
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterLatch:Ljava/util/concurrent/CountDownLatch;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private changeCursorForPeoplePartition()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 882
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 883
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 884
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1a

    .line 885
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 890
    :goto_19
    return-void

    .line 888
    :cond_1a
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_19
.end method

.method private getWellFormedEmailAddress()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 943
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    move-object v0, v2

    .line 955
    :cond_a
    :goto_a
    return-object v0

    .line 947
    :cond_b
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    .line 948
    .local v1, tokens:[Landroid/text/util/Rfc822Token;
    if-eqz v1, :cond_2f

    array-length v3, v1

    if-lez v3, :cond_2f

    .line 949
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 950
    .local v0, address:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2f

    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_a

    .end local v0           #address:Ljava/lang/String;
    :cond_2f
    move-object v0, v2

    .line 955
    goto :goto_a
.end method

.method private getWellFormedSmsAddress()Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 963
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 979
    :cond_9
    :goto_9
    return-object v4

    .line 966
    :cond_a
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->isWellFormedSmsAddress(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 969
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    .line 970
    .local v3, len:I
    const/4 v1, 0x1

    .line 971
    .local v1, firstChar:Z
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1a
    if-ge v2, v3, :cond_32

    .line 972
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 973
    .local v0, c:C
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0x2b

    if-ne v0, v5, :cond_2e

    if-eqz v1, :cond_9

    .line 977
    :cond_2e
    const/4 v1, 0x0

    .line 971
    add-int/lit8 v2, v2, 0x1

    goto :goto_1a

    .line 979
    .end local v0           #c:C
    :cond_32
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    goto :goto_9
.end method

.method private releaseLatch()V
    .registers 2

    .prologue
    .line 904
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterLatch:Ljava/util/concurrent/CountDownLatch;

    .line 905
    .local v0, latch:Ljava/util/concurrent/CountDownLatch;
    if-eqz v0, :cond_7

    .line 906
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 908
    :cond_7
    return-void
.end method

.method private updatePublicProfileSearchStatus()V
    .registers 7

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 993
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    if-nez v1, :cond_8

    .line 1017
    :goto_7
    return-void

    .line 997
    :cond_8
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 998
    .local v0, cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3f

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v5, :cond_3f

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesLoaded:Z

    if-eqz v1, :cond_3f

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaded:Z

    if-eqz v1, :cond_3f

    .line 1002
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    if-eqz v1, :cond_4d

    .line 1003
    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1012
    :cond_3f
    :goto_3f
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_48

    .line 1013
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->showEmptyPeopleSearchResults()V

    .line 1016
    :cond_48
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_7

    .line 1004
    :cond_4d
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesNotFound:Z

    if-eqz v1, :cond_61

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    if-nez v1, :cond_61

    .line 1005
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_3f

    .line 1006
    :cond_61
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    if-eqz v1, :cond_3f

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    if-nez v1, :cond_3f

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowProgressWhenEmpty:Z

    if-nez v1, :cond_75

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCount()I

    move-result v1

    if-lez v1, :cond_3f

    .line 1008
    :cond_75
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_3f
.end method


# virtual methods
.method protected final continueLoadingPublicProfiles()V
    .registers 3

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->hasMoreResults()Z

    move-result v0

    if-nez v0, :cond_9

    .line 616
    :goto_8
    return-void

    .line 609
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$3;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_8
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 2

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilter:Landroid/widget/Filter;

    if-nez v0, :cond_b

    .line 1244
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilter:Landroid/widget/Filter;

    .line 1246
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilter:Landroid/widget/Filter;

    return-object v0
.end method

.method protected final getItemViewType(II)I
    .registers 3
    .parameter "partition"
    .parameter "position"

    .prologue
    .line 597
    return p1
.end method

.method public final getItemViewTypeCount()I
    .registers 2

    .prologue
    .line 592
    const/4 v0, 0x6

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 571
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final isError()Z
    .registers 2

    .prologue
    .line 578
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesError:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfileError:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsError:Z

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final isLoaded()Z
    .registers 3

    .prologue
    .line 585
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesLoaded:Z

    if-eqz v0, :cond_1b

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaded:Z

    if-eqz v0, :cond_1b

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_11

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaded:Z

    if-eqz v0, :cond_1b

    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method public final isSearchingForFirstResult()Z
    .registers 2

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCount()I

    move-result v0

    if-nez v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    if-eqz v0, :cond_1c

    :cond_1a
    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 633
    if-eqz p1, :cond_2b

    .line 634
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 635
    const-string v0, "search_list_adapter.query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    .line 636
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResultsPreserved:Z

    if-nez v0, :cond_2b

    .line 637
    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    .line 640
    :cond_2b
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 13
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 697
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    if-ne p1, v0, :cond_18

    .line 698
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    sget-object v4, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CIRCLES_PROJECTION:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const/16 v6, 0xa

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)V

    .line 717
    :goto_17
    return-object v0

    .line 700
    :cond_18
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    if-ne p1, v0, :cond_2f

    .line 701
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->GAIA_ID_CIRCLE_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_17

    .line 703
    :cond_2f
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    if-ne p1, v0, :cond_4d

    .line 704
    new-instance v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v2, :cond_4a

    sget-object v2, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION_WITH_PHONE:[Ljava/lang/String;

    :goto_41
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const/4 v4, 0x2

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_17

    :cond_4a
    sget-object v2, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;

    goto :goto_41

    .line 708
    :cond_4d
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    if-ne p1, v0, :cond_6b

    .line 709
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->LOCAL_PROFILE_PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActivityId:Ljava/lang/String;

    const/16 v9, 0xa

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;I)V

    goto :goto_17

    .line 712
    :cond_6b
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    if-ne p1, v0, :cond_8a

    .line 713
    new-instance v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->PUBLIC_PROFILE_PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getToken()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;)V

    goto :goto_17

    .line 717
    :cond_8a
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1190
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 1197
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1183
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 11
    .parameter "args"
    .parameter "tag"

    .prologue
    const/4 v7, 0x0

    .line 1151
    const-string v5, "add_email_dialog"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_43

    .line 1152
    const-string v5, "message"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1153
    .local v1, name:Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedEmailAddress()Ljava/lang/String;

    move-result-object v0

    .line 1154
    .local v0, email:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_37

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_37

    .line 1155
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "e:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1156
    .local v3, personId:Ljava/lang/String;
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v5, :cond_38

    .line 1157
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    .end local v0           #email:Ljava/lang/String;
    .end local v1           #name:Ljava/lang/String;
    .end local v3           #personId:Ljava/lang/String;
    :cond_37
    :goto_37
    return-void

    .line 1159
    .restart local v0       #email:Ljava/lang/String;
    .restart local v1       #name:Ljava/lang/String;
    .restart local v3       #personId:Ljava/lang/String;
    :cond_38
    new-instance v2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v2, v7, v1, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    .local v2, person:Lcom/google/android/apps/plus/content/PersonData;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v7, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_37

    .line 1163
    .end local v0           #email:Ljava/lang/String;
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #person:Lcom/google/android/apps/plus/content/PersonData;
    .end local v3           #personId:Ljava/lang/String;
    :cond_43
    const-string v5, "add_sms_dialog"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_37

    .line 1164
    const-string v5, "message"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1165
    .restart local v1       #name:Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedSmsAddress()Ljava/lang/String;

    move-result-object v4

    .line 1166
    .local v4, sms:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_37

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_37

    .line 1167
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "p:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1168
    .restart local v3       #personId:Ljava/lang/String;
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v5, :cond_7a

    .line 1169
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_37

    .line 1171
    :cond_7a
    new-instance v2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v2, v7, v1, v3}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    .restart local v2       #person:Lcom/google/android/apps/plus/content/PersonData;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v7, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_37
.end method

.method public final onItemClick(I)V
    .registers 27
    .parameter "position"

    .prologue
    .line 1024
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    .line 1025
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_9

    .line 1118
    :cond_8
    :goto_8
    return-void

    .line 1029
    :cond_9
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getPartitionForPosition(I)I

    move-result v13

    .line 1030
    .local v13, partition:I
    packed-switch v13, :pswitch_data_218

    goto :goto_8

    .line 1032
    :pswitch_11
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1033
    .local v15, personId:Ljava/lang/String;
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1034
    .local v10, gaiaId:Ljava/lang/String;
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1035
    .local v12, name:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v14, v10, v12, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    .local v14, person:Lcom/google/android/apps/plus/content/PersonData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1, v14}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_8

    .line 1041
    .end local v10           #gaiaId:Ljava/lang/String;
    .end local v12           #name:Ljava/lang/String;
    .end local v14           #person:Lcom/google/android/apps/plus/content/PersonData;
    .end local v15           #personId:Ljava/lang/String;
    :pswitch_42
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1042
    .local v7, context:Landroid/content/Context;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1043
    .local v6, circleId:Ljava/lang/String;
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1044
    .restart local v12       #name:Ljava/lang/String;
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 1045
    .local v18, type:I
    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1046
    .local v16, size:I
    new-instance v5, Lcom/google/android/apps/plus/content/CircleData;

    move/from16 v0, v18

    move/from16 v1, v16

    invoke-direct {v5, v6, v0, v12, v1}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 1048
    .local v5, circle:Lcom/google/android/apps/plus/content/CircleData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v19

    if-eqz v19, :cond_d2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v7, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenMinorPublicExtendedDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v19

    if-nez v19, :cond_d2

    .line 1050
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1051
    .local v4, builder:Landroid/app/AlertDialog$Builder;
    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1052
    const v19, 0x7f080342

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1054
    const v19, 0x7f0801c4

    new-instance v20, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$4;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v5, v7}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$4;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;Landroid/content/Context;)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1063
    const v19, 0x7f0801c5

    new-instance v20, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$5;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$5;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1071
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_8

    .line 1073
    .end local v4           #builder:Landroid/app/AlertDialog$Builder;
    :cond_d2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    goto/16 :goto_8

    .line 1078
    .end local v5           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v6           #circleId:Ljava/lang/String;
    .end local v7           #context:Landroid/content/Context;
    .end local v12           #name:Ljava/lang/String;
    .end local v16           #size:I
    .end local v18           #type:I
    :pswitch_df
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1079
    .restart local v15       #personId:Ljava/lang/String;
    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1080
    .local v11, lookupKey:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v20, v0

    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    move/from16 v23, v0

    if-eqz v23, :cond_138

    const/16 v23, 0xa

    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_138

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v24, "p:"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    :cond_138
    if-nez v19, :cond_142

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    :cond_142
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_158

    const/16 v19, 0x9

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_158

    const/16 v19, 0x0

    :cond_158
    new-instance v23, Lcom/google/android/apps/plus/content/PersonData;

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-interface {v0, v15, v11, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto/16 :goto_8

    .line 1084
    .end local v11           #lookupKey:Ljava/lang/String;
    .end local v15           #personId:Ljava/lang/String;
    :pswitch_16e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    move/from16 v19, v0

    if-nez v19, :cond_8

    .line 1086
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    move/from16 v19, v0

    if-eqz v19, :cond_189

    .line 1088
    const-string v19, "add_email_dialog"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1091
    :cond_189
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedEmailAddress()Ljava/lang/String;

    move-result-object v9

    .line 1092
    .local v9, email:Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 1093
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "e:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1094
    .restart local v15       #personId:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v14, v0, v1, v9}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    .restart local v14       #person:Lcom/google/android/apps/plus/content/PersonData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1, v14}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto/16 :goto_8

    .line 1101
    .end local v9           #email:Ljava/lang/String;
    .end local v14           #person:Lcom/google/android/apps/plus/content/PersonData;
    .end local v15           #personId:Ljava/lang/String;
    :pswitch_1c2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    move/from16 v19, v0

    if-nez v19, :cond_8

    .line 1103
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    move/from16 v19, v0

    if-eqz v19, :cond_1dd

    .line 1105
    const-string v19, "add_sms_dialog"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1108
    :cond_1dd
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedSmsAddress()Ljava/lang/String;

    move-result-object v17

    .line 1109
    .local v17, sms:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_8

    .line 1110
    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "p:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1111
    .restart local v15       #personId:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v14, v0, v1, v15}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1112
    .restart local v14       #person:Lcom/google/android/apps/plus/content/PersonData;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1, v14}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto/16 :goto_8

    .line 1030
    :pswitch_data_218
    .packed-switch 0x0
        :pswitch_11
        :pswitch_42
        :pswitch_16e
        :pswitch_1c2
        :pswitch_df
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 16
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v7, 0x0

    const/4 v10, 0x1

    .line 47
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    if-ne v0, v1, :cond_35

    if-nez p2, :cond_33

    move v0, v10

    :goto_12
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesError:Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaded:Z

    invoke-virtual {p0, v10, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    :cond_19
    :goto_19
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    :cond_25
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    if-gtz v0, :cond_32

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    :cond_32
    return-void

    :cond_33
    move v0, v7

    goto :goto_12

    :cond_35
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    if-ne v0, v1, :cond_71

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_46
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onStartGaiaIdsAndCircles()V

    if-eqz p2, :cond_68

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_68

    :cond_55
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addGaiaIdAndCircles(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_55

    :cond_68
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onFinishGaiaIdsAndCircles()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    goto :goto_19

    :cond_71
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    if-ne v0, v1, :cond_13e

    if-nez p2, :cond_135

    move v0, v10

    :goto_78
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsError:Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_89

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_89

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_89
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onStartContacts()V

    if-eqz p2, :cond_c5

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c5

    :cond_98
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v0, :cond_138

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_a1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v0, :cond_13b

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_aa
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_98

    :cond_c5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onFinishContacts()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v11, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v7

    const-string v2, "address"

    aput-object v2, v1, v10

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz p2, :cond_fb

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_fb

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedEmailAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_fb

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v1, v2, v10

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_fb
    invoke-virtual {p0, v11, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v0, :cond_19

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v11, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v7

    const-string v2, "address"

    aput-object v2, v1, v10

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz p2, :cond_130

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_130

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedSmsAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_130

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v1, v2, v10

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_130
    invoke-virtual {p0, v12, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto/16 :goto_19

    :cond_135
    move v0, v7

    goto/16 :goto_78

    :cond_138
    move-object v5, v8

    goto/16 :goto_a1

    :cond_13b
    move-object v6, v8

    goto/16 :goto_aa

    :cond_13e
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    if-ne v0, v1, :cond_19b

    if-nez p2, :cond_145

    move v7, v10

    :cond_145
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfileError:Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_156

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_156

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_156
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onStartLocalProfiles()V

    if-eqz p2, :cond_191

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_191

    :cond_165
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v9, v8

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addLocalProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_165

    :cond_191
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onFinishLocalProfiles()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    goto/16 :goto_19

    :cond_19b
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    if-ne v0, v1, :cond_19

    sget-object v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->ABORTED:Landroid/database/MatrixCursor;

    if-eq p2, v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1b5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_1b5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1b5
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    if-eqz p2, :cond_1bf

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1ca

    :cond_1bf
    move v0, v10

    :goto_1c0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    if-eqz v0, :cond_1cc

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    goto/16 :goto_19

    :cond_1ca
    move v0, v7

    goto :goto_1c0

    :cond_1cc
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_19

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setToken(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_21a

    move v0, v10

    :goto_1f0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setHasMoreResults(Z)V

    :goto_1f3
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_21c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addPublicProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1f3

    :cond_21a
    move v0, v7

    goto :goto_1f0

    :cond_21c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getPublicProfileCount()I

    move-result v0

    if-nez v0, :cond_22b

    :goto_224
    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesNotFound:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    goto/16 :goto_19

    :cond_22b
    move v10, v7

    goto :goto_224
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 987
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 646
    const-string v0, "search_list_adapter.query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->isParcelable()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 648
    const-string v0, "search_list_adapter.results"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 650
    :cond_16
    return-void
.end method

.method public final onStart()V
    .registers 6

    .prologue
    .line 653
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 654
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 655
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 656
    .local v0, args:Landroid/os/Bundle;
    const-string v2, "query"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_25

    .line 658
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 660
    :cond_25
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 661
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    if-nez v2, :cond_37

    .line 662
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 664
    :cond_37
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    if-eqz v2, :cond_42

    .line 665
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 667
    :cond_42
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    .line 669
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v3, "add_person_dialog_listener"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;

    .line 671
    .local v1, listener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;
    if-eqz v1, :cond_54

    .line 672
    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->setAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    .line 674
    :cond_54
    return-void
.end method

.method public final onStop()V
    .registers 3

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 678
    return-void
.end method

.method public final setAddToCirclesActionEnabled(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 438
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    .line 439
    return-void
.end method

.method public final setCircleUsageType(I)V
    .registers 2
    .parameter "circleUsageType"

    .prologue
    .line 423
    iput p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    .line 424
    return-void
.end method

.method public final setFilterNullGaiaIds(Z)V
    .registers 2
    .parameter "filterNullGaiaIds"

    .prologue
    .line 430
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    .line 431
    return-void
.end method

.method public final setIncludePeopleInCircles(Z)V
    .registers 4
    .parameter "flag"

    .prologue
    .line 476
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setIncludePeopleInCircles(Z)V

    .line 478
    return-void
.end method

.method public final setIncludePhoneNumberContacts(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 462
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    .line 463
    return-void
.end method

.method public final setIncludePlusPages(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 469
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    .line 470
    return-void
.end method

.method public final setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 501
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    .line 502
    return-void
.end method

.method public final setMention(Ljava/lang/String;)V
    .registers 3
    .parameter "activityId"

    .prologue
    .line 493
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActivityId:Ljava/lang/String;

    .line 494
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    .line 495
    return-void
.end method

.method public final setPublicProfileSearchEnabled(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 454
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    .line 455
    return-void
.end method

.method public final setQueryString(Ljava/lang/String;)V
    .registers 7
    .parameter "queryString"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 505
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 506
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    .line 559
    :cond_d
    :goto_d
    return-void

    .line 510
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setQueryString(Ljava/lang/String;)V

    .line 511
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 512
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 514
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    .line 515
    iput v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    .line 516
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_53

    .line 517
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 518
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 519
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 520
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 521
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->clearPartitions()V

    .line 522
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    .line 523
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v1, :cond_d

    .line 524
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    goto :goto_d

    .line 527
    :cond_53
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 528
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_71

    .line 530
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    .line 531
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 534
    :cond_71
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    .line 535
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 537
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    if-nez v1, :cond_8f

    .line 538
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    .line 539
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 542
    :cond_8f
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    if-eqz v1, :cond_d

    .line 543
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    .line 544
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesNotFound:Z

    .line 545
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    .line 547
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 553
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 554
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 556
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    goto/16 :goto_d
.end method

.method public final setShowPersonNameDialog(Z)V
    .registers 3
    .parameter "showPersonNameDialog"

    .prologue
    .line 446
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    .line 447
    return-void
.end method

.method public final setShowProgressWhenEmpty(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowProgressWhenEmpty:Z

    .line 487
    return-void
.end method

.method protected final showEmptyPeopleSearchResults()V
    .registers 4

    .prologue
    .line 896
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 897
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 898
    .local v0, cursor:Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_16

    .line 899
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 901
    :cond_16
    return-void
.end method

.method protected final showPersonNameDialog(Ljava/lang/String;)V
    .registers 11
    .parameter "tag"

    .prologue
    const/4 v5, 0x0

    .line 1124
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v1, "add_person_dialog_listener"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;

    .line 1126
    .local v8, listener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;
    if-nez v8, :cond_21

    .line 1127
    new-instance v8, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;

    .end local v8           #listener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;
    invoke-direct {v8}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;-><init>()V

    .line 1128
    .restart local v8       #listener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "add_person_dialog_listener"

    invoke-virtual {v0, v8, v1}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1131
    :cond_21
    invoke-virtual {v8, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->setAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    .line 1133
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 1134
    .local v6, context:Landroid/content/Context;
    const v0, 0x7f08029c

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f08029d

    invoke-virtual {v6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/high16 v4, 0x104

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->newInstance$405ed676(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/EditFragmentDialog;

    move-result-object v7

    .line 1142
    .local v7, dialog:Lcom/google/android/apps/plus/fragments/EditFragmentDialog;
    invoke-virtual {v7, v8, v5}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1143
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v7, v0, p1}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1144
    return-void
.end method
