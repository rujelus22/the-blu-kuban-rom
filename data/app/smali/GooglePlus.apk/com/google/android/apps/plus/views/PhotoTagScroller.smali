.class public Lcom/google/android/apps/plus/views/PhotoTagScroller;
.super Landroid/widget/HorizontalScrollView;
.source "PhotoTagScroller.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoTagScroller$PhotoShapeQuery;
    }
.end annotation


# instance fields
.field private mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

.field private mExternalClickListener:Landroid/view/View$OnClickListener;

.field private mHideTags:Z

.field private mMyApprovedShapeId:Ljava/lang/Long;

.field private mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

.field private final mScrollerRect:Landroid/graphics/Rect;

.field private mShapeNeedsApproval:Z

.field private mTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/PhotoTagAvatarView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 83
    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    .line 75
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    .line 75
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    .line 75
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    .line 92
    return-void
.end method


# virtual methods
.method public final bind(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .registers 34
    .parameter "context"
    .parameter "account"
    .parameter "cursor"
    .parameter "tagRoot"

    .prologue
    .line 217
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 221
    .local v12, inflater:Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    move-object/from16 v25, v0

    if-eqz v25, :cond_177

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    move-object/from16 v25, v0

    const v26, 0x7f09003c

    invoke-virtual/range {v25 .. v26}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Long;

    .line 223
    .local v23, tagValue:Ljava/lang/Long;
    if-eqz v23, :cond_173

    .line 224
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 233
    .end local v23           #tagValue:Ljava/lang/Long;
    .local v8, checkedShapeId:J
    :goto_21
    const/4 v14, 0x0

    .line 234
    .local v14, newCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    .line 235
    const/16 v25, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    .line 236
    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mShapeNeedsApproval:Z

    .line 237
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 238
    const/16 v25, -0x1

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 240
    :cond_47
    :goto_47
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v25

    if-eqz v25, :cond_1da

    .line 241
    const/16 v25, 0x4

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    .line 242
    .local v17, shapeId:J
    const/16 v25, 0x7

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 243
    .local v21, subjectGaiaId:Ljava/lang/String;
    const/16 v25, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 244
    .local v22, subjectName:Ljava/lang/String;
    const/16 v25, 0x2

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 245
    .local v10, creatorName:Ljava/lang/String;
    const/16 v25, 0x5

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 246
    .local v20, status:Ljava/lang/String;
    const/16 v25, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    .line 248
    .local v6, bounds:[B
    const-string v25, "PENDING"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v15

    .line 250
    .local v15, pending:Z
    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v13

    .line 253
    .local v13, isMe:Z
    const-string v25, "DETECTED"

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_47

    .line 254
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v25

    if-nez v25, :cond_47

    .line 259
    invoke-static {}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->getInstance()Lcom/google/api/services/plusi/model/DataRectRelativeJson;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/DataRectRelative;

    .line 266
    .local v16, rect:Lcom/google/api/services/plusi/model/DataRectRelative;
    if-eqz v13, :cond_1c7

    .line 267
    if-nez v15, :cond_17b

    .line 268
    const v25, 0x7f03008f

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v24

    .line 269
    .local v24, tagView:Landroid/view/View;
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    .line 270
    .local v11, index:I
    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    .line 288
    :goto_d8
    move-object/from16 v0, p4

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 289
    const v25, 0x7f090048

    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .line 290
    .local v5, avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setSubjectGaiaId(Ljava/lang/String;)V

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 293
    cmp-long v25, v8, v17

    if-nez v25, :cond_102

    .line 294
    move-object v14, v5

    .line 298
    :cond_102
    new-instance v19, Landroid/graphics/RectF;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->left:Ljava/lang/Double;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Double;->floatValue()F

    move-result v25

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->top:Ljava/lang/Double;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Double;->floatValue()F

    move-result v26

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->right:Ljava/lang/Double;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Double;->floatValue()F

    move-result v27

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->bottom:Ljava/lang/Double;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Double;->floatValue()F

    move-result v28

    move-object/from16 v0, v19

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 300
    .local v19, shapeRect:Landroid/graphics/RectF;
    const v25, 0x7f09003b

    move/from16 v0, v25

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setTag(ILjava/lang/Object;)V

    .line 301
    const v25, 0x7f09003a

    move/from16 v0, v25

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setTag(ILjava/lang/Object;)V

    .line 302
    const v25, 0x7f09003c

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setTag(ILjava/lang/Object;)V

    .line 303
    if-eqz v13, :cond_47

    if-eqz v15, :cond_47

    .line 304
    const v25, 0x7f090049

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v25

    check-cast v25, Landroid/widget/TextView;

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_47

    .line 226
    .end local v5           #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    .end local v6           #bounds:[B
    .end local v8           #checkedShapeId:J
    .end local v10           #creatorName:Ljava/lang/String;
    .end local v11           #index:I
    .end local v13           #isMe:Z
    .end local v14           #newCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    .end local v15           #pending:Z
    .end local v16           #rect:Lcom/google/api/services/plusi/model/DataRectRelative;
    .end local v17           #shapeId:J
    .end local v19           #shapeRect:Landroid/graphics/RectF;
    .end local v20           #status:Ljava/lang/String;
    .end local v21           #subjectGaiaId:Ljava/lang/String;
    .end local v22           #subjectName:Ljava/lang/String;
    .end local v24           #tagView:Landroid/view/View;
    .restart local v23       #tagValue:Ljava/lang/Long;
    :cond_173
    const-wide/16 v8, 0x0

    .line 228
    .restart local v8       #checkedShapeId:J
    goto/16 :goto_21

    .line 229
    .end local v8           #checkedShapeId:J
    .end local v23           #tagValue:Ljava/lang/Long;
    :cond_177
    const-wide/16 v8, 0x0

    .restart local v8       #checkedShapeId:J
    goto/16 :goto_21

    .line 272
    .restart local v6       #bounds:[B
    .restart local v10       #creatorName:Ljava/lang/String;
    .restart local v13       #isMe:Z
    .restart local v14       #newCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    .restart local v15       #pending:Z
    .restart local v16       #rect:Lcom/google/api/services/plusi/model/DataRectRelative;
    .restart local v17       #shapeId:J
    .restart local v20       #status:Ljava/lang/String;
    .restart local v21       #subjectGaiaId:Ljava/lang/String;
    .restart local v22       #subjectName:Ljava/lang/String;
    :cond_17b
    const v25, 0x7f03008e

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v24

    .line 275
    .restart local v24       #tagView:Landroid/view/View;
    const v25, 0x7f09018f

    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 276
    .local v7, button:Landroid/view/View;
    const v25, 0x7f09003c

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 277
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    const v25, 0x7f090190

    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 279
    const v25, 0x7f09003c

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 280
    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    const/4 v11, 0x0

    .line 282
    .restart local v11       #index:I
    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mShapeNeedsApproval:Z

    goto/16 :goto_d8

    .line 285
    .end local v7           #button:Landroid/view/View;
    .end local v11           #index:I
    .end local v24           #tagView:Landroid/view/View;
    :cond_1c7
    const v25, 0x7f03008f

    const/16 v26, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v24

    .line 286
    .restart local v24       #tagView:Landroid/view/View;
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    .restart local v11       #index:I
    goto/16 :goto_d8

    .line 308
    .end local v6           #bounds:[B
    .end local v10           #creatorName:Ljava/lang/String;
    .end local v11           #index:I
    .end local v13           #isMe:Z
    .end local v15           #pending:Z
    .end local v16           #rect:Lcom/google/api/services/plusi/model/DataRectRelative;
    .end local v17           #shapeId:J
    .end local v20           #status:Ljava/lang/String;
    .end local v21           #subjectGaiaId:Ljava/lang/String;
    .end local v22           #subjectName:Ljava/lang/String;
    .end local v24           #tagView:Landroid/view/View;
    :cond_1da
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v25

    if-lez v25, :cond_22a

    .line 310
    if-eqz v14, :cond_216

    .line 312
    move-object v5, v14

    .line 318
    .restart local v5       #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    :goto_1e3
    const v25, 0x7f09003b

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/graphics/RectF;

    .line 319
    .restart local v19       #shapeRect:Landroid/graphics/RectF;
    const v25, 0x7f09003a

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    .line 320
    .restart local v22       #subjectName:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V

    .line 321
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setChecked(Z)V

    .line 327
    .end local v5           #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    .end local v19           #shapeRect:Landroid/graphics/RectF;
    .end local v22           #subjectName:Ljava/lang/String;
    :goto_20f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->invalidate()V

    .line 328
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->requestLayout()V

    .line 329
    return-void

    .line 315
    :cond_216
    const/16 v25, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    .line 316
    .restart local v24       #tagView:Landroid/view/View;
    const v25, 0x7f090048

    invoke-virtual/range {v24 .. v25}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .restart local v5       #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    goto :goto_1e3

    .line 324
    .end local v5           #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    .end local v24           #tagView:Landroid/view/View;
    :cond_22a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-virtual/range {v25 .. v27}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V

    goto :goto_20f
.end method

.method public final clear()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 168
    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .line 169
    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 170
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    .line 172
    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    .line 173
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .line 174
    .local v0, avatar:Landroid/view/View;
    const v2, 0x7f09003b

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 175
    const v2, 0x7f09003a

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 176
    const v2, 0x7f09003c

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_12

    .line 178
    .end local v0           #avatar:Landroid/view/View;
    :cond_31
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 179
    return-void
.end method

.method public final getMyApprovedShapeId()Ljava/lang/Long;
    .registers 2

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    return-object v0
.end method

.method public final hasTags()Z
    .registers 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hideTags(ZZ)V
    .registers 4
    .parameter "hide"
    .parameter "animate"

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    if-eq v0, p1, :cond_12

    .line 343
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    .line 344
    if-eqz p1, :cond_13

    .line 345
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->hideTagShape()V

    .line 350
    :cond_12
    :goto_12
    return-void

    .line 347
    :cond_13
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->showTagShape()V

    goto :goto_12
.end method

.method public final interceptMoveLeft$2548a39(F)Z
    .registers 5
    .parameter "origY"

    .prologue
    const/4 v0, 0x0

    .line 152
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    if-eqz v1, :cond_6

    .line 160
    :cond_5
    :goto_5
    return v0

    .line 156
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-gtz v1, :cond_5

    .line 160
    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final interceptMoveRight$2548a39(F)Z
    .registers 5
    .parameter "origY"

    .prologue
    const/4 v0, 0x0

    .line 139
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    if-eqz v1, :cond_6

    .line 147
    :cond_5
    :goto_5
    return v0

    .line 143
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    .line 144
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-gtz v1, :cond_5

    .line 147
    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final isWaitingMyApproval()Z
    .registers 2

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mShapeNeedsApproval:Z

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .registers 10
    .parameter "buttonView"
    .parameter "isChecked"

    .prologue
    const/4 v5, 0x0

    .line 103
    instance-of v4, p1, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-eqz v4, :cond_e

    move-object v0, p1

    .line 104
    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .line 106
    .local v0, avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    if-eqz p2, :cond_2e

    .line 107
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-ne p1, v4, :cond_f

    .line 135
    .end local v0           #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    :cond_e
    :goto_e
    return-void

    .line 112
    .restart local v0       #avatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .line 113
    .local v1, prevChecked:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    .line 114
    if-eqz v1, :cond_19

    .line 115
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setChecked(Z)V

    .line 125
    .end local v1           #prevChecked:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
    :cond_19
    :goto_19
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-nez v4, :cond_35

    move-object v3, v5

    .line 128
    .local v3, shapeRect:Landroid/graphics/RectF;
    :goto_1e
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-nez v4, :cond_42

    move-object v2, v5

    .line 132
    .local v2, shapeName:Ljava/lang/CharSequence;
    :goto_23
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V

    .line 133
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_e

    .line 119
    .end local v2           #shapeName:Ljava/lang/CharSequence;
    .end local v3           #shapeRect:Landroid/graphics/RectF;
    :cond_2e
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-ne p1, v4, :cond_19

    .line 121
    iput-object v5, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    goto :goto_19

    .line 125
    :cond_35
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    const v6, 0x7f09003b

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    move-object v3, v4

    goto :goto_1e

    .line 128
    .restart local v3       #shapeRect:Landroid/graphics/RectF;
    :cond_42
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    const v5, 0x7f09003a

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    move-object v2, v4

    goto :goto_23
.end method

.method public onClick(Landroid/view/View;)V
    .registers 3
    .parameter "v"

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_9

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 99
    :cond_9
    return-void
.end method

.method public setExternalOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    .line 186
    return-void
.end method

.method public setHeaderView(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V
    .registers 2
    .parameter "header"

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 193
    return-void
.end method
