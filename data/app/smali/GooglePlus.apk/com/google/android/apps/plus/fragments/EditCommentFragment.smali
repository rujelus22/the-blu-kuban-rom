.class public Lcom/google/android/apps/plus/fragments/EditCommentFragment;
.super Landroid/support/v4/app/Fragment;
.source "EditCommentFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field protected mChanged:Z

.field protected mCommentId:Ljava/lang/String;

.field protected mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field protected mPendingRequestId:Ljava/lang/Integer;

.field protected mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 48
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditCommentFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditCommentFragment;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->handleEditComment(Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditCommentFragment;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditCommentActivity;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->invalidateMenu()V

    :cond_b
    return-void
.end method

.method private handleEditComment(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 10
    .parameter "result"

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 231
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 233
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_51

    .line 234
    const v3, 0x32c15

    invoke-virtual {v0, v3}, Landroid/app/Activity;->dismissDialog(I)V

    .line 235
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_5d

    .line 236
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    .line 237
    .local v2, exception:Ljava/lang/Exception;
    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_52

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v2           #exception:Ljava/lang/Exception;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_52

    .line 240
    const v3, 0x7f080158

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f080157

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    .line 244
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {v1, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 253
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_51
    :goto_51
    return-void

    .line 248
    :cond_52
    const v3, 0x7f080150

    invoke-static {v0, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_51

    .line 250
    :cond_5d
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_51
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    if-eqz p1, :cond_22

    .line 70
    const-string v0, "changed"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mChanged:Z

    .line 72
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 73
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 76
    :cond_22
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 81
    const v4, 0x7f030023

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 83
    .local v3, view:Landroid/view/View;
    const v4, 0x7f09004f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 86
    .local v2, intent:Landroid/content/Intent;
    const-string v4, "account"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 87
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    const-string v4, "activity_id"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, activityId:Ljava/lang/String;
    const-string v4, "comment_id"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentId:Ljava/lang/String;

    .line 90
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const/4 v5, 0x0

    invoke-virtual {v4, p0, v0, v1, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 91
    if-nez p3, :cond_44

    .line 92
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const-string v5, "comment"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHtml(Ljava/lang/String;)V

    .line 95
    :cond_44
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/EditCommentFragment$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditCommentFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 111
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 113
    return-object v3
.end method

.method public final onDiscard()V
    .registers 3

    .prologue
    .line 162
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditCommentActivity;

    .line 165
    .local v0, editCommentActivity:Lcom/google/android/apps/plus/phone/EditCommentActivity;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mChanged:Z

    if-eqz v1, :cond_16

    .line 166
    const v1, 0xdc072

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->showDialog(I)V

    .line 171
    :goto_15
    return-void

    .line 168
    :cond_16
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->setResult(I)V

    .line 169
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->finish()V

    goto :goto_15
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-ne p1, v0, :cond_7

    .line 148
    packed-switch p2, :pswitch_data_e

    .line 154
    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 150
    :pswitch_9
    invoke-static {p1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 151
    const/4 v0, 0x1

    goto :goto_8

    .line 148
    :pswitch_data_e
    .packed-switch 0x6
        :pswitch_9
    .end packed-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 130
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 132
    return-void
.end method

.method public final onPost()V
    .registers 13

    .prologue
    .line 177
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EditCommentActivity;

    .line 180
    .local v0, editCommentActivity:Lcom/google/android/apps/plus/phone/EditCommentActivity;
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mChanged:Z

    if-eqz v3, :cond_1b

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_23

    .line 181
    :cond_1b
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->setResult(I)V

    .line 182
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->finish()V

    .line 202
    :goto_22
    return-void

    .line 186
    :cond_23
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 187
    .local v7, intent:Landroid/content/Intent;
    const-string v3, "account"

    invoke-virtual {v7, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    .line 188
    .local v1, account:Lcom/google/android/apps/plus/content/EsAccount;
    const-string v3, "activity_id"

    invoke-virtual {v7, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 189
    .local v2, activityId:Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentTextView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v5

    .line 192
    .local v5, commentText:Ljava/lang/String;
    const-string v3, "photo_id"

    invoke-virtual {v7, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_76

    .line 193
    const-string v3, "photo_id"

    const-wide/16 v10, 0x0

    invoke-virtual {v7, v3, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    .line 194
    .local v8, photoId:J
    const-string v3, "gaia_id"

    invoke-virtual {v7, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 195
    .local v6, gaiaId:Ljava/lang/String;
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;->newBuilder()Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->setFocusObfuscatedOwnerId(Ljava/lang/String;)Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    invoke-virtual {v3, v8, v9}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->setPhotoId(J)Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    invoke-virtual {v3}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->build()Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->editPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 201
    .end local v6           #gaiaId:Ljava/lang/String;
    .end local v8           #photoId:J
    :goto_6f
    const v3, 0x32c15

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/EditCommentActivity;->showDialog(I)V

    goto :goto_22

    .line 198
    :cond_76
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v5}, Lcom/google/android/apps/plus/service/EsService;->editComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_6f
.end method

.method public final onResume()V
    .registers 2

    .prologue
    .line 118
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_25

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v0

    if-nez v0, :cond_25

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->handleEditComment(Lcom/google/android/apps/plus/service/ServiceResult;)V

    .line 126
    :cond_25
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 138
    const-string v0, "changed"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 141
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditCommentFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    :cond_19
    return-void
.end method
