.class public final Lcom/google/android/apps/plus/service/ContactsStatsSync;
.super Ljava/lang/Object;
.source "ContactsStatsSync.java"


# static fields
.field private static final PROJECTION_FOR_ICS_AND_LATER:[Ljava/lang/String;

.field private static final PROJECTION_FOR_PRE_ICS:[Ljava/lang/String;


# instance fields
.field private isFirstStatsSync:Z

.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MobileContact;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private maxLastContacted:J


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "times_contacted"

    aput-object v1, v0, v2

    const-string v1, "last_time_contacted"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->PROJECTION_FOR_ICS_AND_LATER:[Ljava/lang/String;

    .line 47
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "times_contacted"

    aput-object v1, v0, v2

    const-string v1, "last_time_contacted"

    aput-object v1, v0, v3

    const-string v1, "sourceid"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->PROJECTION_FOR_PRE_ICS:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->maxLastContacted:J

    .line 113
    iput-object p1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    .line 114
    iput-object p2, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContacts:Ljava/util/List;

    .line 116
    iput-object p3, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 117
    return-void
.end method

.method public static sync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 75
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 89
    :cond_6
    :goto_6
    return-void

    .line 78
    :cond_7
    const-string v0, "ContactsStatsSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 79
    const-string v0, "ContactsStatsSync"

    const-string v1, "Contacts stats sync operation started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_17
    new-instance v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;

    invoke-direct {v6, p0, p1, p2}, Lcom/google/android/apps/plus/service/ContactsStatsSync;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 83
    .local v6, sync:Lcom/google/android/apps/plus/service/ContactsStatsSync;
    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    iget-object v2, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const-string v3, "times_contacted > 0"

    iget-object v0, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    iget-object v2, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v7

    const-wide/16 v4, 0x0

    cmp-long v0, v7, v4

    if-gtz v0, :cond_ed

    const/4 v0, 0x1

    :goto_4b
    iput-boolean v0, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->isFirstStatsSync:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v0, v2, :cond_f0

    sget-object v2, Lcom/google/android/apps/plus/service/ContactsStatsSync;->PROJECTION_FOR_PRE_ICS:[Ljava/lang/String;

    :goto_55
    :try_start_55
    iget-object v0, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const-string v5, "last_time_contacted"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_61
    .catch Ljava/lang/RuntimeException; {:try_start_55 .. :try_end_61} :catch_f4

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-boolean v2, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->isFirstStatsSync:Z

    if-eqz v2, :cond_72

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v9, 0x2

    div-long/2addr v4, v9

    sub-long/2addr v0, v4

    :cond_72
    :goto_72
    :try_start_72
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_132

    new-instance v4, Lcom/google/api/services/plusi/model/MobileContact;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/MobileContact;-><init>()V

    new-instance v2, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    iput-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->id:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    new-instance v2, Lcom/google/api/services/plusi/model/MobileContactAffinity;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileContactAffinity;-><init>()V

    iput-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v2, Lcom/google/api/services/plusi/model/MobileContactAffinity;->outgoingPhoneCallCount:Ljava/lang/Integer;

    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    const/4 v5, 0x1

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, Lcom/google/api/services/plusi/model/MobileContactAffinity;->lastOutgoingPhoneCallPosixTimestamp:Ljava/lang/Long;

    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/MobileContactAffinity;->lastOutgoingPhoneCallPosixTimestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v2, v9, v11

    if-gtz v2, :cond_11e

    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iput-object v5, v2, Lcom/google/api/services/plusi/model/MobileContactAffinity;->lastOutgoingPhoneCallPosixTimestamp:Ljava/lang/Long;

    :goto_bb
    const/4 v2, 0x2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    const/4 v9, 0x3

    if-le v2, v9, :cond_12b

    const/4 v2, 0x3

    invoke-interface {v3, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_12b

    const-string v2, "plus"

    const/4 v9, 0x3

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12b

    const/4 v2, 0x1

    :goto_dc
    if-eqz v2, :cond_12d

    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->id:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iput-object v5, v2, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    :goto_e2
    iget-object v2, v6, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContacts:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_e7
    .catchall {:try_start_72 .. :try_end_e7} :catchall_e8

    goto :goto_72

    :catchall_e8
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_ed
    const/4 v0, 0x0

    goto/16 :goto_4b

    :cond_f0
    sget-object v2, Lcom/google/android/apps/plus/service/ContactsStatsSync;->PROJECTION_FOR_ICS_AND_LATER:[Ljava/lang/String;

    goto/16 :goto_55

    :catch_f4
    move-exception v0

    const-string v1, "ContactsStatsSync"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Query on RawContacts failed. "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :goto_109
    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->upload()V

    .line 86
    const-string v0, "ContactsStatsSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 87
    const-string v0, "ContactsStatsSync"

    const-string v1, "Contacts stats sync operation complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 83
    :cond_11e
    :try_start_11e
    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/MobileContactAffinity;->lastOutgoingPhoneCallPosixTimestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v2, v9, v7

    if-lez v2, :cond_72

    goto :goto_bb

    :cond_12b
    const/4 v2, 0x0

    goto :goto_dc

    :cond_12d
    iget-object v2, v4, Lcom/google/api/services/plusi/model/MobileContact;->id:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iput-object v5, v2, Lcom/google/api/services/plusi/model/DataCircleMemberId;->contactId:Ljava/lang/String;
    :try_end_131
    .catchall {:try_start_11e .. :try_end_131} :catchall_e8

    goto :goto_e2

    :cond_132
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_109
.end method

.method private upload()V
    .registers 10

    .prologue
    const/4 v6, 0x0

    .line 213
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7e

    .line 214
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->maxLastContacted:J

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :cond_12
    :goto_12
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_47

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x14

    if-ge v0, v1, :cond_47

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContacts:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/MobileContact;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-wide v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->maxLastContacted:J

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/MobileContactAffinity;->lastOutgoingPhoneCallPosixTimestamp:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v1, v1, v7

    if-gez v1, :cond_12

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileContact;->affinity:Lcom/google/api/services/plusi/model/MobileContactAffinity;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileContactAffinity;->lastOutgoingPhoneCallPosixTimestamp:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->maxLastContacted:J

    goto :goto_12

    :cond_47
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v1, "ContactsStatsSync:PartialUpload"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AndroidUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const-string v5, "FULL"

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    new-instance v2, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_7f

    const-string v1, "ContactsStatsSync"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->logError(Ljava/lang/String;)V

    .line 216
    :cond_7e
    return-void

    .line 214
    :cond_7f
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "last_stats_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "account_status"

    invoke-virtual {v1, v3, v2, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->maxLastContacted:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->isFirstStatsSync:Z

    if-eqz v0, :cond_c5

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_c5
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/service/ContactsStatsSync;->maxLastContacted:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    goto/16 :goto_1
.end method

.method public static wipeout(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "opListener"

    .prologue
    const/4 v8, 0x3

    .line 93
    const-string v1, "ContactsStatsSync"

    invoke-static {v1, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 94
    const-string v1, "ContactsStatsSync"

    const-string v2, "Contacts stats wipeout operation started"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_10
    new-instance v0, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "WIPEOUT"

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 105
    .local v0, op:Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SyncMobileContactsOperation;->startThreaded()V

    .line 107
    const-string v1, "ContactsStatsSync"

    invoke-static {v1, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 108
    const-string v1, "ContactsStatsSync"

    const-string v2, "Contacts stats wipeout operation complete"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_36
    return-void
.end method
