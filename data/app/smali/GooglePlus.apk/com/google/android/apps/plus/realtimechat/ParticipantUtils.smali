.class public final Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;
.super Ljava/lang/Object;
.source "ParticipantUtils.java"


# direct methods
.method public static getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;
    .registers 6
    .parameter "person"

    .prologue
    .line 173
    const/4 v2, 0x0

    .line 174
    .local v2, participantId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v1

    .line 176
    .local v1, gaiaId:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 177
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 188
    :cond_1a
    :goto_1a
    return-object v2

    .line 178
    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1a

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v0

    .line 182
    .local v0, email:Ljava/lang/String;
    const-string v3, "p:"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4a

    .line 183
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "p:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1a

    .line 185
    :cond_4a
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "e:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1a
.end method

.method public static getParticipantListFromAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Ljava/util/List;
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "audience"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 40
    .local v16, participantTable:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v15, participantList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v8

    .local v8, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v13, v8

    .local v13, len$:I
    const/4 v12, 0x0

    .local v12, i$:I
    :goto_10
    if-ge v12, v13, :cond_61

    aget-object v17, v8, v12

    .line 43
    .local v17, person:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5f

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    if-lez v4, :cond_2a

    const/4 v1, 0x0

    aget-object v1, v3, v1

    :cond_2a
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v3

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v14

    .line 44
    .local v14, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :goto_42
    if-eqz v14, :cond_5c

    .line 45
    invoke-virtual {v14}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5c

    .line 46
    invoke-virtual {v14}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 47
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_5c
    add-int/lit8 v12, v12, 0x1

    goto :goto_10

    .line 43
    .end local v14           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_5f
    const/4 v14, 0x0

    goto :goto_42

    .line 52
    .end local v17           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_61
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v8

    .local v8, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v13, v8

    const/4 v12, 0x0

    :goto_67
    if-ge v12, v13, :cond_e3

    aget-object v9, v8, v12

    .line 53
    .local v9, circle:Lcom/google/android/apps/plus/content/CircleData;
    const/4 v10, 0x0

    .line 55
    .local v10, cursor:Landroid/database/Cursor;
    :try_start_6c
    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v5, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 57
    :cond_88
    :goto_88
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_dd

    .line 58
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 59
    .local v11, firstName:Ljava/lang/String;
    const-string v1, " "

    invoke-virtual {v11, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 60
    .local v18, words:[Ljava/lang/String;
    move-object/from16 v0, v18

    array-length v1, v0

    if-lez v1, :cond_a1

    .line 61
    const/4 v1, 0x0

    aget-object v11, v18, v1

    .line 63
    :cond_a1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v14

    .line 68
    .restart local v14       #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v14}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_88

    .line 69
    invoke-virtual {v14}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_d7
    .catchall {:try_start_6c .. :try_end_d7} :catchall_d8

    goto :goto_88

    .line 74
    .end local v11           #firstName:Ljava/lang/String;
    .end local v14           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v18           #words:[Ljava/lang/String;
    :catchall_d8
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_dd
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 52
    add-int/lit8 v12, v12, 0x1

    goto :goto_67

    .line 77
    .end local v9           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v10           #cursor:Landroid/database/Cursor;
    :cond_e3
    return-object v15
.end method

.method public static makePersonFromParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/content/PersonData;
    .registers 6
    .parameter "participant"

    .prologue
    const/4 v4, 0x0

    .line 159
    if-eqz p0, :cond_24

    .line 160
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v2

    .line 161
    .local v2, participantId:Ljava/lang/String;
    const-string v3, "g:"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_15

    .line 162
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3

    .line 164
    :cond_15
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, gaiaId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, name:Ljava/lang/String;
    new-instance v3, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v3, v0, v1, v4}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .end local v0           #gaiaId:Ljava/lang/String;
    .end local v1           #name:Ljava/lang/String;
    .end local v2           #participantId:Ljava/lang/String;
    :goto_23
    return-object v3

    :cond_24
    move-object v3, v4

    goto :goto_23
.end method
