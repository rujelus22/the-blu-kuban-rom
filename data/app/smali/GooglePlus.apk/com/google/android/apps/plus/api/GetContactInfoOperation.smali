.class public final Lcom/google/android/apps/plus/api/GetContactInfoOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetContactInfoOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/LoadPeopleRequest;",
        "Lcom/google/api/services/plusi/model/LoadCircleMembersResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mGaiaId:Ljava/lang/String;

.field private mPerson:Lcom/google/api/services/plusi/model/DataCirclePerson;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 47
    const-string v3, "loadpeople"

    invoke-static {}, Lcom/google/api/services/plusi/model/LoadPeopleRequestJson;->getInstance()Lcom/google/api/services/plusi/model/LoadPeopleRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/LoadCircleMembersResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LoadCircleMembersResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 54
    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->mGaiaId:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method public final getPerson()Lcom/google/api/services/plusi/model/DataCirclePerson;
    .registers 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->mPerson:Lcom/google/api/services/plusi/model/DataCirclePerson;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/LoadCircleMembersResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadCircleMembersResponse;->circlePerson:Ljava/util/List;

    if-eqz v0, :cond_19

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadCircleMembersResponse;->circlePerson:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_19

    iget-object v0, p1, Lcom/google/api/services/plusi/model/LoadCircleMembersResponse;->circlePerson:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCirclePerson;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->mPerson:Lcom/google/api/services/plusi/model/DataCirclePerson;

    :cond_19
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"

    .prologue
    const/4 v2, 0x0

    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/LoadPeopleRequest;

    .end local p1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LoadPeopleRequest;->circleMemberId:Ljava/util/List;

    new-instance v0, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->mGaiaId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/LoadPeopleRequest;->circleMemberId:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LoadPeopleRequest;->includeIsFollowing:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/LoadPeopleRequest;->includeMemberships:Ljava/lang/Boolean;

    return-void
.end method
