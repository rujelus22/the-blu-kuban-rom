.class public final Lcom/google/android/apps/plus/api/EventReadOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "EventReadOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventReadRequest;",
        "Lcom/google/api/services/plusi/model/EventLeafResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final EVENT_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mEventId:Ljava/lang/String;

.field private final mFetchNewer:Z

.field private final mInvitationToken:Ljava/lang/String;

.field private mPollingToken:Ljava/lang/String;

.field private final mResolveTokens:Z

.field private mResumeToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 64
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "polling_token"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "resume_token"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/api/EventReadOperation;->EVENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "pollingToken"
    .parameter "resumeToken"
    .parameter "invitationToken"
    .parameter "fetchNewer"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 75
    const-string v3, "eventread"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventReadRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventLeafResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventLeafResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 77
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Event ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_20
    iput-object p3, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    .line 82
    iput-object p5, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    .line 83
    iput-object p6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mInvitationToken:Ljava/lang/String;

    .line 84
    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResolveTokens:Z

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "fetchNewer"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v6, 0x0

    .line 90
    const-string v3, "eventread"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventReadRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventLeafResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventLeafResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 92
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Event ID must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_20
    iput-object p3, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    .line 96
    iput-object v6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    .line 97
    iput-object v6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    .line 98
    iput-object v6, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mInvitationToken:Ljava/lang/String;

    .line 99
    iput-boolean p4, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResolveTokens:Z

    .line 101
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 24
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    check-cast p1, Lcom/google/api/services/plusi/model/EventLeafResponse;

    .end local p1
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->update:Lcom/google/api/services/plusi/model/Update;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->activityId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v8, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static {v3, v4, v8}, Lcom/google/android/apps/plus/content/EsEventData;->getDisplayTime(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)J

    move-result-wide v12

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->resumeToken:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_172

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-eqz v3, :cond_16f

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    :goto_35
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    if-eqz v3, :cond_41

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-eqz v3, :cond_178

    :cond_41
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->pollingToken:Ljava/lang/String;

    :goto_45
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    if-nez v3, :cond_51

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    if-eqz v3, :cond_17e

    :cond_51
    const/4 v11, 0x1

    :goto_52
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-nez v3, :cond_181

    const/4 v3, 0x1

    :goto_59
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-eqz v4, :cond_35b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    if-eqz v4, :cond_35b

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->resumeToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_35b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v10, v6, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-static {v3, v4, v10}, Lcom/google/android/apps/plus/content/EsEventData;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    const/4 v3, 0x1

    move v4, v3

    :goto_7e
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->photosData:Ljava/util/List;

    if-eqz v3, :cond_184

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->photosData:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_91
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_184

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/ReadResponsePhotosData;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/ReadResponsePhotosData;->photos:Ljava/util/List;

    move-object/from16 v16, v0

    if-eqz v16, :cond_91

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ReadResponsePhotosData;->photos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_a9
    :goto_a9
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_91

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataPhoto;

    new-instance v17, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const/16 v18, 0x64

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e2

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    const-wide v20, 0x408f400000000000L

    mul-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    move-object/from16 v2, v17

    iput-wide v0, v2, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_e2
    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v18, v0

    if-eqz v18, :cond_13a

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    new-instance v18, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct/range {v18 .. v18}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->profilePhotoUrl:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_13a
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_a9

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    move-object/from16 v18, v0

    if-eqz v18, :cond_a9

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_a9

    if-eqz v4, :cond_15a

    move-object/from16 v0, v17

    iget-wide v12, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_15a
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhotoJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhotoJson;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/api/services/plusi/model/DataPhotoJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a9

    :cond_16f
    const/4 v9, 0x0

    goto/16 :goto_35

    :cond_172
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->resumeToken:Ljava/lang/String;

    goto/16 :goto_35

    :cond_178
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    goto/16 :goto_45

    :cond_17e
    const/4 v11, 0x0

    goto/16 :goto_52

    :cond_181
    const/4 v3, 0x0

    goto/16 :goto_59

    :cond_184
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->comments:Ljava/util/List;

    if-eqz v3, :cond_260

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->comments:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_192
    :goto_192
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_260

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Comment;

    new-instance v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct {v15}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const/16 v16, 0x5

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1bb

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_1bb
    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    new-instance v16, Lcom/google/api/services/plusi/model/EmbedsPerson;

    invoke-direct/range {v16 .. v16}, Lcom/google/api/services/plusi/model/EmbedsPerson;-><init>()V

    iget-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    iget-object v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/api/services/plusi/model/EmbedsPerson;->imageUrl:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v16, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsEventData$EventComment;-><init>()V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->commentId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    if-eqz v17, :cond_220

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->ownedByViewer:Z

    :cond_220
    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v17, v0

    if-eqz v17, :cond_243

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    move-object/from16 v17, v0

    if-eqz v17, :cond_243

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v0, v3

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    move-object/from16 v2, v16

    iput-wide v0, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->totalPlusOnes:J

    :cond_243
    iget-object v3, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    if-eqz v3, :cond_192

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_192

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_192

    :cond_260
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->frames:Ljava/util/List;

    if-eqz v3, :cond_34f

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/EventLeafResponse;->frames:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_26e
    :goto_26e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_34f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/EventFrame;

    new-instance v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct {v15}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const-string v16, "INVITED"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2fb

    const/16 v16, 0x2

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    :goto_291
    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->lastTimeMillis:Ljava/lang/Long;

    move-object/from16 v16, v0

    if-eqz v16, :cond_2a3

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->lastTimeMillis:Ljava/lang/Long;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    :cond_2a3
    new-instance v16, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;-><init>()V

    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->person:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_336

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventFrame;->person:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_2bf
    :goto_2bf
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_336

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/EmbedsPerson;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2bf

    new-instance v18, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    invoke-direct/range {v18 .. v18}, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;-><init>()V

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EmbedsPerson;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EmbedsPerson;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v14, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2bf

    :cond_2fb
    const-string v16, "RSVP_NO"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_30e

    const/16 v16, 0x3

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    goto :goto_291

    :cond_30e
    const-string v16, "RSVP_YES"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_322

    const/16 v16, 0x4

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    goto/16 :goto_291

    :cond_322
    const-string v16, "CHECKIN"

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EventFrame;->verbType:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_26e

    const/16 v16, 0x1

    move/from16 v0, v16

    iput v0, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    goto/16 :goto_291

    :cond_336
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_26e

    sget-object v3, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v15, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_26e

    :cond_34f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static/range {v3 .. v14}, Lcom/google/android/apps/plus/content/EsEventData;->updateEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;ZJLjava/util/List;)V

    return-void

    :cond_35b
    move v4, v3

    goto/16 :goto_7e
.end method

.method protected final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 7
    .parameter "errorCode"
    .parameter "reasonPhrase"
    .parameter "ex"

    .prologue
    .line 323
    const/16 v0, 0x194

    if-ne p1, v0, :cond_e

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsEventData;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 334
    :cond_d
    :goto_d
    return-void

    .line 325
    :cond_e
    const/16 v0, 0x190

    if-lt p1, v0, :cond_d

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/InstantUpload;->getInstantShareEventId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 328
    const-string v0, "HttpTransaction"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 329
    const-string v0, "HttpTransaction"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[EVENT_READ] received error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; disable IS"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    :cond_43
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->disableInstantShare(Landroid/content/Context;)V

    goto :goto_d
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 10
    .parameter "x0"

    .prologue
    const/4 v7, 0x1

    .line 50
    check-cast p1, Lcom/google/api/services/plusi/model/EventReadRequest;

    .end local p1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResolveTokens:Z

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/api/EventReadOperation;->EVENT_PROJECTION:[Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :try_start_13
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;
    :try_end_27
    .catchall {:try_start_13 .. :try_end_27} :catchall_ad

    :cond_27
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2a
    new-instance v0, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;-><init>()V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;->includeActivityId:Ljava/lang/Boolean;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;->includeUpdate:Ljava/lang/Boolean;

    new-instance v1, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;-><init>()V

    const/16 v2, 0x3e8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;->maxFrames:Ljava/lang/Integer;

    new-instance v2, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;-><init>()V

    const/16 v3, 0x1f4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;->maxComments:Ljava/lang/Integer;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;-><init>()V

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/ReadOptionsPhotosOptions;->maxPhotos:Ljava/lang/Integer;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/api/services/plusi/model/EventSelector;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/EventSelector;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mEventId:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/api/services/plusi/model/EventSelector;->eventId:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lcom/google/api/services/plusi/model/ReadOptions;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/ReadOptions;-><init>()V

    iput-object v3, v6, Lcom/google/api/services/plusi/model/ReadOptions;->photosOptions:Ljava/util/List;

    iput-object v1, v6, Lcom/google/api/services/plusi/model/ReadOptions;->framesOptions:Lcom/google/api/services/plusi/model/ReadOptionsFramesOptions;

    iput-object v2, v6, Lcom/google/api/services/plusi/model/ReadOptions;->commentsOptions:Lcom/google/api/services/plusi/model/ReadOptionsCommentsOptions;

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->eventUpdateOptions:Lcom/google/api/services/plusi/model/ReadOptionsUpdateOptions;

    const-string v0, "LIST"

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->responseFormat:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->includePlusEvent:Ljava/lang/Boolean;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ReadOptions;->resolvePersons:Ljava/lang/Boolean;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v5, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->readOptions:Ljava/util/List;

    iput-object v4, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mInvitationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->invitationToken:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mFetchNewer:Z

    if-nez v0, :cond_a8

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    if-nez v0, :cond_b2

    :cond_a8
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mPollingToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->pollingToken:Ljava/lang/String;

    :goto_ac
    return-void

    :catchall_ad
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_b2
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EventReadOperation;->mResumeToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventReadRequest;->resumeToken:Ljava/lang/String;

    goto :goto_ac
.end method
