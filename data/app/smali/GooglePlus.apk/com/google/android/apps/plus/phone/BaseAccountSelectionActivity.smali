.class public abstract Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "BaseAccountSelectionActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;,
        Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;
    }
.end annotation


# instance fields
.field private mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

.field private mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

.field private mAddAccountPendingRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mShowOnAttach:Z

.field private mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 68
    new-instance v0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 78
    new-instance v0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    .line 93
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Lcom/google/android/apps/plus/fragments/AccountsListFragment;
    .registers 2
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mShowOnAttach:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;ILcom/google/android/apps/plus/content/EsAccount;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method private chooseAccountManually()V
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    const/4 v0, 0x0

    .line 498
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_32

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v1, "allowSkip"

    invoke-virtual {v7, v1, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "introMessage"

    const v2, 0x7f08002a

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    new-array v2, v3, [Ljava/lang/String;

    const-string v1, "com.google"

    aput-object v1, v2, v9

    const-string v5, "webupdates"

    move-object v1, v0

    move-object v4, v0

    move-object v6, v0

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    .line 499
    .local v8, intent:Landroid/content/Intent;
    :goto_2e
    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 500
    return-void

    .end local v8           #intent:Landroid/content/Intent;
    :cond_32
    move-object v8, v0

    .line 498
    goto :goto_2e
.end method

.method private handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 9
    .parameter "result"

    .prologue
    const v4, 0x7f08003d

    const v6, 0x7f08003a

    const/4 v5, 0x0

    .line 331
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    .line 332
    .local v2, exception:Ljava/lang/Exception;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 333
    .local v0, args:Landroid/os/Bundle;
    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_83

    .line 334
    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v2           #exception:Ljava/lang/Exception;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v1

    .line 335
    .local v1, errCode:I
    sparse-switch v1, :sswitch_data_9a

    .line 366
    const-string v3, "error_title"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v3, "error_message"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    invoke-virtual {p0, v5, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 377
    .end local v1           #errCode:I
    :goto_32
    return-void

    .line 337
    .restart local v1       #errCode:I
    :sswitch_33
    const-string v3, "error_message"

    const v4, 0x7f08003b

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_32

    .line 344
    :sswitch_44
    const-string v3, "error_message"

    const v4, 0x7f080038

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    const/4 v3, 0x2

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_32

    .line 351
    :sswitch_55
    const-string v3, "error_message"

    const v4, 0x7f080039

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    const/4 v3, 0x4

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_32

    .line 357
    :sswitch_66
    const-string v3, "error_title"

    const v4, 0x7f08003e

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const-string v3, "error_message"

    const v4, 0x7f08003f

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const/4 v3, 0x3

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_32

    .line 373
    .end local v1           #errCode:I
    .restart local v2       #exception:Ljava/lang/Exception;
    :cond_83
    const-string v3, "error_title"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v3, "error_message"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-virtual {p0, v5, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_32

    .line 335
    nop

    :sswitch_data_9a
    .sparse-switch
        0x1 -> :sswitch_44
        0xa -> :sswitch_33
        0xc -> :sswitch_55
        0xf -> :sswitch_66
    .end sparse-switch
.end method

.method private handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "requestId"
    .parameter "account"

    .prologue
    .line 306
    if-nez p2, :cond_3

    .line 314
    :goto_2
    return-void

    .line 309
    :cond_3
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeIncompleteOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v0

    .line 311
    .local v0, oobResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v1

    .line 313
    .local v1, settings:Lcom/google/android/apps/plus/content/AccountSettingsData;
    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V

    goto :goto_2
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 507
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getUpgradeOrigin()Ljava/lang/String;
    .registers 2

    .prologue
    .line 296
    const-string v0, "DEFAULT"

    return-object v0
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 515
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final handleUpgradeFailure()V
    .registers 4

    .prologue
    .line 579
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 580
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "error_title"

    const v2, 0x7f08003d

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v1, "error_message"

    const v2, 0x7f08003a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 583
    return-void
.end method

.method public final handleUpgradeSuccess(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "account"

    .prologue
    .line 566
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 567
    .local v0, target:Landroid/content/Intent;
    if-nez v0, :cond_12

    .line 568
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    .line 571
    :cond_12
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->startActivity(Landroid/content/Intent;)V

    .line 572
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->finish()V

    .line 573
    return-void
.end method

.method protected abstract onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 274
    packed-switch p1, :pswitch_data_1e

    .line 286
    :goto_3
    return-void

    .line 276
    :pswitch_4
    const/4 v1, -0x1

    if-ne p2, v1, :cond_15

    if-eqz p3, :cond_15

    .line 277
    const-string v1, "authAccount"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, accountName:Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;->addAccount(Ljava/lang/String;)V

    goto :goto_3

    .line 280
    .end local v0           #accountName:Ljava/lang/String;
    :cond_15
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setResult(I)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->finish()V

    goto :goto_3

    .line 274
    nop

    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 230
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    if-eqz v0, :cond_1e

    .line 231
    check-cast p1, Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->setAccountsAdder(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;)V

    .line 234
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mShowOnAttach:Z

    if-eqz v0, :cond_1e

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mShowOnAttach:Z

    .line 239
    :cond_1e
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter "dialog"

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showAccountList()V

    .line 456
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showAccountList()V

    .line 448
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v1, 0x0

    .line 142
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 144
    if-eqz p1, :cond_2e

    .line 145
    const-string v0, "aa_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 146
    const-string v0, "aa_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    .line 152
    :goto_1a
    const-string v0, "ua_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 153
    const-string v0, "ua_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    .line 159
    :cond_2e
    :goto_2e
    return-void

    .line 149
    :cond_2f
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    goto :goto_1a

    .line 156
    :cond_32
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    goto :goto_2e
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 10
    .parameter "dialogId"
    .parameter "args"

    .prologue
    const v6, 0x7f0801c4

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 384
    if-nez p2, :cond_f

    move-object v3, v2

    .line 385
    .local v3, title:Ljava/lang/String;
    :goto_8
    if-nez p2, :cond_16

    move-object v1, v2

    .line 387
    .local v1, message:Ljava/lang/String;
    :goto_b
    packed-switch p1, :pswitch_data_80

    .line 439
    :goto_e
    :pswitch_e
    return-object v2

    .line 384
    .end local v1           #message:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    :cond_f
    const-string v4, "error_title"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_8

    .line 385
    .restart local v3       #title:Ljava/lang/String;
    :cond_16
    const-string v4, "error_message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    .line 393
    .restart local v1       #message:Ljava/lang/String;
    :pswitch_1d
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 394
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 395
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 396
    invoke-virtual {v0, v6, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 397
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 398
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_e

    .line 402
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :pswitch_33
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 403
    .local v2, progressDialog:Landroid/app/ProgressDialog;
    const v4, 0x7f080037

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 404
    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 405
    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_e

    .line 410
    .end local v2           #progressDialog:Landroid/app/ProgressDialog;
    :pswitch_49
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 411
    .restart local v2       #progressDialog:Landroid/app/ProgressDialog;
    const v4, 0x7f080041

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 412
    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 413
    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_e

    .line 418
    .end local v2           #progressDialog:Landroid/app/ProgressDialog;
    :pswitch_5f
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 419
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 420
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 421
    new-instance v4, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$2;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    invoke-virtual {v0, v6, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 428
    new-instance v4, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$3;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 435
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_e

    .line 387
    nop

    :pswitch_data_80
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_1d
        :pswitch_5f
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_33
        :pswitch_49
    .end packed-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    .prologue
    .line 263
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 264
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_c

    .line 265
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setIntent(Landroid/content/Intent;)V

    .line 267
    :cond_c
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 221
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 222
    return-void
.end method

.method public onResume()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xe

    const/4 v3, 0x0

    .line 166
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_55

    .line 172
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_55

    .line 173
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->dismissDialog(I)V

    .line 174
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_2a

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    .line 177
    :cond_2a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 178
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    if-eqz v0, :cond_53

    .line 179
    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_46

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_8d

    .line 180
    :cond_46
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V

    .line 187
    :cond_53
    :goto_53
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    .line 191
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_55
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_85

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_85

    .line 194
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->dismissDialog(I)V

    .line 195
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 197
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    if-eqz v0, :cond_83

    .line 198
    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_91

    .line 199
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleUpgradeSuccess(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 205
    :cond_83
    :goto_83
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    .line 209
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_85
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_8c

    .line 210
    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->overridePendingTransition(II)V

    .line 212
    :cond_8c
    return-void

    .line 183
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_8d
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_53

    .line 201
    :cond_91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleUpgradeFailure()V

    goto :goto_83
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 246
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 249
    const-string v0, "aa_reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 252
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_21

    .line 253
    const-string v0, "ua_reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 256
    :cond_21
    return-void
.end method

.method protected final showAccountList()V
    .registers 3

    .prologue
    .line 462
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_c

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    .line 467
    :goto_b
    return-void

    .line 465
    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->chooseAccountManually()V

    goto :goto_b
.end method

.method protected final showAccountSelectionOrUpgradeAccount(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/16 v3, 0xe

    const/16 v2, 0xb

    const/4 v4, 0x0

    .line 526
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccountUnsafe(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 527
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_1f

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isAccountUpgradeRequired(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 528
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(I)V

    .line 550
    :cond_1e
    :goto_1e
    return-void

    .line 532
    :cond_1f
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v3, :cond_58

    .line 533
    const/high16 v1, 0x7f03

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setContentView(I)V

    .line 535
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v2, :cond_39

    .line 537
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showTitlebar(Z)V

    .line 540
    const v1, 0x7f080029

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 544
    :cond_39
    const v1, 0x7f090045

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080030

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 546
    const v1, 0x7f090046

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f080031

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1e

    .line 547
    :cond_58
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_1e

    if-nez p1, :cond_1e

    .line 548
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_1e

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasLoggedInThePast(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_9c

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9c

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasVisitedOob(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_83

    invoke-static {p0, v4}, Lcom/google/android/apps/plus/content/EsAccountsData;->setHasVisitedOob(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->finish()V

    goto :goto_1e

    :cond_83
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v3, v1

    if-lez v3, :cond_9a

    aget-object v1, v1, v4

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_96
    invoke-interface {v2, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;->addAccount(Ljava/lang/String;)V

    goto :goto_1e

    :cond_9a
    const/4 v1, 0x0

    goto :goto_96

    :cond_9c
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->chooseAccountManually()V

    goto/16 :goto_1e
.end method
