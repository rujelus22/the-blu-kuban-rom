.class public final Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "FindMorePeopleOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/FindMorePeopleRequest;",
        "Lcom/google/api/services/plusi/model/FindMorePeopleResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 40
    const-string v3, "findmorepeople"

    invoke-static {}, Lcom/google/api/services/plusi/model/FindMorePeopleRequestJson;->getInstance()Lcom/google/api/services/plusi/model/FindMorePeopleRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/FindMorePeopleResponseJson;->getInstance()Lcom/google/api/services/plusi/model/FindMorePeopleResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 43
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 9
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/FindMorePeopleResponse;

    .end local p1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/FindMorePeopleResponse;->suggestion:Ljava/util/List;

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/google/api/services/plusi/model/FindMorePeopleResponse;->suggestion:Ljava/util/List;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_10
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_14
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    if-eqz v3, :cond_3e

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    if-eqz v3, :cond_3e

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->esUser:Ljava/lang/Boolean;

    if-eqz v3, :cond_3e

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->esUser:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_42

    :cond_3e
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_14

    :cond_42
    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->score:Ljava/lang/Double;

    if-eqz v3, :cond_55

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->score:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    const-wide v5, 0x3f9999999999999aL

    cmpg-double v0, v3, v5

    if-gez v0, :cond_14

    :cond_55
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_14

    :cond_59
    iget-object v0, p0, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/FindMorePeopleRequest;

    .end local p1
    const/16 v0, 0x50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/FindMorePeopleRequest;->maxSuggestions:Ljava/lang/Integer;

    return-void
.end method
