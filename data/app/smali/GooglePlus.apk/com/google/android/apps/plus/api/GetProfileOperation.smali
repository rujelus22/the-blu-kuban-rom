.class public final Lcom/google/android/apps/plus/api/GetProfileOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetProfileOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;",
        "Lcom/google/api/services/plusi/model/GetSimpleProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mGaiaId:Ljava/lang/String;

.field private final mInsertInDatabase:Z

.field private mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "insertInDatabase"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v6, 0x0

    .line 43
    const-string v3, "getsimpleprofile"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetSimpleProfileRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetSimpleProfileRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetSimpleProfileResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetSimpleProfileResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 47
    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mGaiaId:Ljava/lang/String;

    .line 48
    iput-boolean p4, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mInsertInDatabase:Z

    .line 49
    return-void
.end method


# virtual methods
.method public final getProfile()Lcom/google/api/services/plusi/model/SimpleProfile;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/GetSimpleProfileResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetSimpleProfileResponse;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mInsertInDatabase:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mGaiaId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    :cond_15
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetProfileOperation;->mGaiaId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetSimpleProfileRequest;->ownerId:Ljava/lang/String;

    return-void
.end method
