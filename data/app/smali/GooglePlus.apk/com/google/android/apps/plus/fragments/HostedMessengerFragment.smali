.class public Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedMessengerFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$SuggestionsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ParticipantsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

.field private mConnected:Ljava/lang/Boolean;

.field private mConversationCursor:Landroid/database/Cursor;

.field private mConversationsUri:Landroid/net/Uri;

.field private mInvitationConversationBundle:Landroid/os/Bundle;

.field private mListView:Landroid/widget/ListView;

.field private final mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

.field private mRecordedConversationsEmpty:Z

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mScrollOffset:I

.field private mScrollPos:I

.field private mSuggestionCursor:Landroid/database/Cursor;

.field private mSuggestionsUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 159
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    .line 347
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)Landroid/database/Cursor;
    .registers 2
    .parameter "x0"

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method private isLoading()Z
    .registers 2

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_16

    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getConversationsLoaded()Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method private updateView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    const/16 v1, 0x8

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3e

    .line 281
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090073

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 289
    :goto_3d
    return-void

    .line 282
    :cond_3e
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 283
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_3d

    .line 284
    :cond_48
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 285
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_3d

    .line 287
    :cond_52
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->showContent(Landroid/view/View;)V

    goto :goto_3d
.end method


# virtual methods
.method protected final doShowEmptyView(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 316
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 317
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 318
    const v0, 0x7f090073

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 319
    const v0, 0x7f090066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 321
    :cond_27
    const v0, 0x7f090072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 322
    return-void
.end method

.method protected final doShowEmptyViewProgress(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    const/16 v2, 0x8

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 304
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 305
    const v0, 0x7f090073

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 306
    const v0, 0x7f090066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 308
    :cond_27
    const v0, 0x7f090072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 309
    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 924
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 799
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_14

    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getConversationsLoaded()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method protected final needsAsyncData()Z
    .registers 2

    .prologue
    .line 932
    const/4 v0, 0x1

    return v0
.end method

.method public final onActionButtonClicked(I)V
    .registers 5
    .parameter "actionId"

    .prologue
    .line 904
    const/16 v0, 0x64

    if-ne p1, v0, :cond_17

    .line 905
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    .line 907
    :cond_17
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 709
    const/4 v0, 0x1

    if-ne p1, v0, :cond_12

    .line 710
    const/4 v0, -0x1

    if-ne p2, v0, :cond_12

    if-eqz p3, :cond_12

    .line 711
    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 714
    :cond_12
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    const/4 v1, 0x0

    .line 209
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConnected:Ljava/lang/Boolean;

    .line 212
    if-eqz p1, :cond_27

    .line 213
    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    .line 214
    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    .line 220
    :goto_19
    if-eqz p1, :cond_26

    .line 221
    const-string v0, "InvitationConversationBundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRecordedConversationsEmpty:Z

    .line 229
    :cond_26
    return-void

    .line 216
    :cond_27
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    .line 217
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    goto :goto_19
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 12
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 600
    const-string v0, "ConversationList"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 601
    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateLoader "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    :cond_1f
    if-ne p1, v6, :cond_34

    .line 604
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationsUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "is_visible=1 AND is_pending_leave=0"

    const-string v6, "latest_message_timestamp DESC"

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 624
    :goto_33
    return-object v0

    .line 609
    :cond_34
    if-ne p1, v4, :cond_49

    .line 610
    new-instance v1, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionsUri:Landroid/net/Uri;

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$SuggestionsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "sequence ASC"

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    move-object v0, v1

    goto :goto_33

    .line 614
    :cond_49
    const/4 v0, 0x2

    if-ne p1, v0, :cond_74

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "conversation_row_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v2

    .line 617
    .local v2, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ParticipantsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "participant_id!=?"

    new-array v5, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string v6, "first_name ASC"

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_33

    .end local v2           #uri:Landroid/net/Uri;
    :cond_74
    move-object v0, v5

    .line 624
    goto :goto_33
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x0

    .line 549
    const v2, 0x7f03001c

    invoke-virtual {p1, v2, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 550
    .local v1, view:Landroid/view/View;
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    .line 551
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 552
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/content/Context;Landroid/widget/AbsListView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    .line 553
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 555
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_4a

    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_4a

    .line 557
    const v2, 0x7f090074

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 558
    .local v0, spacer:Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 561
    .end local v0           #spacer:Landroid/view/View;
    :cond_4a
    return-object v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 21
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 730
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getPartitionForPosition(I)I

    move-result v14

    .line 731
    .local v14, partition:I
    if-nez v14, :cond_61

    .line 732
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    .line 733
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_17

    .line 769
    :cond_16
    :goto_16
    return-void

    .line 736
    :cond_17
    const/16 v2, 0xd

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_47

    const/16 v2, 0xe

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_47

    .line 738
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v4, 0xe

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x3

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_45

    const/4 v7, 0x1

    :goto_3b
    move-wide/from16 v4, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/plus/phone/Intents;->getConversationInvititationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v11

    .line 742
    .local v11, intent:Landroid/content/Intent;
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_16

    .line 738
    .end local v11           #intent:Landroid/content/Intent;
    :cond_45
    const/4 v7, 0x0

    goto :goto_3b

    .line 744
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x3

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_5f

    const/4 v2, 0x1

    :goto_55
    move-wide/from16 v0, p4

    invoke-static {v3, v4, v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Landroid/content/Intent;

    move-result-object v11

    .line 747
    .restart local v11       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_16

    .line 744
    .end local v11           #intent:Landroid/content/Intent;
    :cond_5f
    const/4 v2, 0x0

    goto :goto_55

    .line 752
    .end local v8           #cursor:Landroid/database/Cursor;
    :cond_61
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    .line 753
    .restart local v8       #cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_16

    .line 756
    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 757
    .local v13, participantId:Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 758
    .local v10, fullName:Ljava/lang/String;
    const/4 v2, 0x3

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 759
    .local v9, firstName:Ljava/lang/String;
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v12

    .line 765
    .local v12, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v11, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/ConversationActivity;

    invoke-direct {v11, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "unique"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account"

    invoke-virtual {v11, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "participant"

    invoke-virtual {v11, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "is_group"

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 767
    .restart local v11       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_16
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 13
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 70
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    const-string v0, "ConversationList"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27

    const-string v0, "ConversationList"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onLoadFinished "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_27
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v9, :cond_ad

    if-eqz p2, :cond_97

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_34
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "g:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_59

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_4c
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v6, v2, v3, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_34

    :cond_59
    const-string v2, "e:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_67

    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    goto :goto_4c

    :cond_67
    const-string v2, "p:"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_123

    move-object v2, v1

    goto :goto_4c

    :cond_71
    new-instance v3, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v3, v4, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    const-string v1, "conversation_is_group"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const v2, 0x7f080283

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x6

    move v6, v5

    move v7, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_97
    :goto_97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    :cond_9e
    :goto_9e
    return-void

    :cond_9f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_97

    :cond_ad
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-eq v0, v5, :cond_b9

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v4, :cond_be

    :cond_b9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    :cond_be
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_11a

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    :cond_c6
    :goto_c6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_9e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_9e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_ee

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    if-nez v0, :cond_e1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    if-eqz v0, :cond_ee

    :cond_e1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    iput v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    iput v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    :cond_ee
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p2, :cond_fa

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_fb

    :cond_fa
    move v8, v5

    :cond_fb
    if-eqz v8, :cond_110

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRecordedConversationsEmpty:Z

    if-nez v1, :cond_110

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_110

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRecordedConversationsEmpty:Z

    :cond_110
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->isLoading()Z

    move-result v0

    if-nez v0, :cond_9e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->onAsyncData()V

    goto :goto_9e

    :cond_11a
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v4, :cond_c6

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionCursor:Landroid/database/Cursor;

    goto :goto_c6

    :cond_123
    move-object v0, v1

    move-object v2, v1

    goto/16 :goto_4c
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 722
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "item"

    .prologue
    .line 888
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_2c

    .line 897
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_b
    return v1

    .line 891
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080414

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 893
    .local v0, helpUrlParam:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->startExternalActivity(Landroid/content/Intent;)V

    .line 894
    const/4 v1, 0x1

    goto :goto_b

    .line 888
    nop

    :pswitch_data_2c
    .packed-switch 0x7f090290
        :pswitch_c
    .end packed-switch
.end method

.method public final onPause()V
    .registers 4

    .prologue
    .line 266
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 267
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f090076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    .local v0, helpTextView:Landroid/widget/TextView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 5
    .parameter "actionBar"

    .prologue
    .line 848
    const v0, 0x7f080057

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    .line 849
    const/16 v0, 0x64

    const v1, 0x7f0200f7

    const v2, 0x7f08022a

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 853
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 6
    .parameter "menu"

    .prologue
    .line 872
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    .line 873
    .local v2, size:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_5
    if-ge v0, v2, :cond_1a

    .line 874
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 875
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_1c

    .line 873
    :goto_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 877
    :pswitch_15
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_12

    .line 881
    .end local v1           #item:Landroid/view/MenuItem;
    :cond_1a
    return-void

    .line 875
    nop

    :pswitch_data_1c
    .packed-switch 0x64
        :pswitch_15
    .end packed-switch
.end method

.method public final onResume()V
    .registers 10

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 236
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 237
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$RTCServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 238
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f090076

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "plusone_messenger_promo"

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0802c5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Spanned;->length()I

    move-result v3

    const-class v5, Landroid/text/style/URLSpan;

    invoke-interface {v4, v7, v3, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/URLSpan;

    array-length v5, v3

    if-lez v5, :cond_6b

    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    aget-object v3, v3, v7

    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-interface {v4, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v4

    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$1;

    invoke-direct {v7, p0, v3}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/text/style/URLSpan;)V

    const/16 v3, 0x21

    invoke-virtual {v5, v7, v6, v4, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 239
    :cond_6b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->updateView(Landroid/view/View;)V

    .line 240
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v2, :cond_8f

    .line 241
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    const-string v3, "conversation_row_id"

    const-wide/16 v4, -0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 243
    .local v0, conversationRowId:J
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->inviteParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)I

    .line 245
    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    .line 246
    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 248
    .end local v0           #conversationRowId:J
    :cond_8f
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 253
    const-string v0, "InvitationConversationBundle"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mInvitationConversationBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_35

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_35

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    .line 256
    :cond_27
    const-string v0, "scroll_pos"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    const-string v0, "scroll_off"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mScrollOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 259
    :cond_35
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 6
    .parameter "args"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 860
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 861
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_45

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationsUri:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mSuggestionsUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const-string v0, "ConversationList"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_45

    const-string v0, "ConversationList"

    const-string v1, "setAccount"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    :cond_45
    const-string v0, "reset_notifications"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 863
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->resetNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 865
    :cond_57
    return-void
.end method

.method protected final showContent(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    const/16 v1, 0x8

    .line 331
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    .line 332
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    const v0, 0x7f090073

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 334
    const v0, 0x7f090066

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 335
    const v0, 0x7f090072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 336
    return-void
.end method
