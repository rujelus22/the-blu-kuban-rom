.class public Lcom/google/android/apps/plus/content/EsProvider;
.super Landroid/content/ContentProvider;
.source "EsProvider.java"


# static fields
.field private static final ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ACCOUNT_STATUS_URI:Landroid/net/Uri;

.field private static final ACTIVITIES_BY_CIRCLE_ID_URI:Landroid/net/Uri;

.field private static final ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

.field public static final ACTIVITIES_URI:Landroid/net/Uri;

.field private static final ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final ACTIVITY_SUMMARY_URI:Landroid/net/Uri;

.field public static final ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

.field public static final ACTIVITY_VIEW_URI:Landroid/net/Uri;

.field public static final ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

.field public static final ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

.field private static final ALBUM_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CIRCLES_URI:Landroid/net/Uri;

.field public static final COMMENTS_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

.field private static final COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final COMMENTS_VIEW_URI:Landroid/net/Uri;

.field public static final CONTACTS_BY_CIRCLE_ID_URI:Landroid/net/Uri;

.field private static final CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONTACTS_QUERY_URI:Landroid/net/Uri;

.field private static final CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONTACTS_URI:Landroid/net/Uri;

.field public static final CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

.field private static final CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final CONVERSATIONS_URI:Landroid/net/Uri;

.field public static final EVENTS_ALL_URI:Landroid/net/Uri;

.field private static final HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

.field private static final LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LOCATION_QUERIES_VIEW_URI:Landroid/net/Uri;

.field private static final MESSAGES_BY_CONVERSATION_URI:Landroid/net/Uri;

.field private static final MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MESSAGE_NOTIFICATIONS_URI:Landroid/net/Uri;

.field private static final MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

.field private static final NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_DATA_STATS_URI:Landroid/net/Uri;

.field private static final NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_DATA_TRANSACTIONS_URI:Landroid/net/Uri;

.field private static final NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final NOTIFICATIONS_URI:Landroid/net/Uri;

.field private static final PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PARTICIPANTS_URI:Landroid/net/Uri;

.field private static final PHOTOS_BY_ALBUM_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTOS_BY_ALBUM_VIEW_SQL:Ljava/lang/String;

.field private static final PHOTOS_BY_EVENT_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTOS_BY_EVENT_VIEW_SQL:Ljava/lang/String;

.field private static final PHOTOS_BY_STREAM_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTOS_BY_STREAM_VIEW_SQL:Ljava/lang/String;

.field private static final PHOTOS_BY_USER_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTOS_BY_USER_VIEW_SQL:Ljava/lang/String;

.field public static final PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

.field public static final PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

.field public static final PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

.field public static final PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

.field public static final PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

.field private static final PHOTO_COMMENTS_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTO_HOME_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PHOTO_HOME_URI:Landroid/net/Uri;

.field public static final PHOTO_NOTIFICATION_COUNT_URI:Landroid/net/Uri;

.field private static final PHOTO_NOTIFICATION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

.field public static final PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

.field private static final PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PHOTO_URI:Landroid/net/Uri;

.field private static final PHOTO_VIEW_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final PHOTO_VIEW_SQL:Ljava/lang/String;

.field private static final PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PLATFORM_AUDIENCE_URI:Landroid/net/Uri;

.field private static final PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final PLUS_PAGES_URI:Landroid/net/Uri;

.field private static final SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/16 v7, 0x4a

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1740
    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photo_view"

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, ""

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_SQL:Ljava/lang/String;

    .line 1743
    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_album_view"

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_in_album ON photo.photo_id=photos_in_album.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_SQL:Ljava/lang/String;

    .line 1751
    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_event_view"

    aput-object v2, v1, v4

    const-string v2, "photos_in_event.event_id as event_id, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_in_event ON photo.photo_id=photos_in_event.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_SQL:Ljava/lang/String;

    .line 1760
    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_stream_view"

    aput-object v2, v1, v4

    const-string v2, "photos_in_stream.stream_id as stream_id, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_in_stream ON photo.photo_id=photos_in_stream.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_SQL:Ljava/lang/String;

    .line 1769
    const-string v0, "CREATE VIEW %s AS SELECT photo._id as _id, photo.action_state as action_state, photo.album_id as album_id, photo.comment_count as comment_count, photo.description as description, photo.downloadable as downloadable, photo.entity_version as entity_version, photo.height as height, photo.owner_id as owner_id, photo.photo_id as photo_id, photo.fingerprint as fingerprint, photo.timestamp as timestamp, photo.title as title, photo.upload_status as upload_status, photo.url as url, photo.video_data as video_data, photo.width as width, photo_plusone.plusone_count as plusone_count, photo_plusone.plusone_data as plusone_data, photo_plusone.plusone_by_me as plusone_by_me, photo_plusone.plusone_id as plusone_id, album.title as album_name, album.stream_id as album_stream, contacts.name as owner_name, %s (SELECT a.status FROM account_status,photo_shape as a WHERE a.photo_id=photo.photo_id AND a.subject_id=account_status.user_id AND a.status=\'PENDING\' LIMIT 1) AS pending_status FROM photo LEFT JOIN photo_plusone ON photo.photo_id=photo_plusone.photo_id LEFT JOIN album ON photo.album_id=album.album_id LEFT JOIN contacts ON photo.owner_id=contacts.gaia_id %s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "photos_by_user_view"

    aput-object v2, v1, v4

    const-string v2, "photos_of_user.photo_of_user_id as photo_of_user_id, "

    aput-object v2, v1, v3

    const-string v2, "INNER JOIN photos_of_user ON photo.photo_id=photos_of_user.photo_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_SQL:Ljava/lang/String;

    .line 2232
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/account_status"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    .line 2235
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activities"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_URI:Landroid/net/Uri;

    .line 2236
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activities_stream_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

    .line 2238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_by_circle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_BY_CIRCLE_ID_URI:Landroid/net/Uri;

    .line 2241
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activities/summary"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_URI:Landroid/net/Uri;

    .line 2244
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/activity_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_URI:Landroid/net/Uri;

    .line 2245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    .line 2248
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/comments_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_URI:Landroid/net/Uri;

    .line 2249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    .line 2252
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/location_queries_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_URI:Landroid/net/Uri;

    .line 2255
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/notifications"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    .line 2257
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/conversations"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    .line 2259
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/message_notifications_view"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_URI:Landroid/net/Uri;

    .line 2262
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/messages/conversation"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_BY_CONVERSATION_URI:Landroid/net/Uri;

    .line 2265
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/participants"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_URI:Landroid/net/Uri;

    .line 2267
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/messenger_suggestions"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_URI:Landroid/net/Uri;

    .line 2270
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/hangout_suggestions"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

    .line 2273
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/circles"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    .line 2274
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/contacts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    .line 2275
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/contacts/id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    .line 2277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/circle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_CIRCLE_ID_URI:Landroid/net/Uri;

    .line 2278
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/query"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_QUERY_URI:Landroid/net/Uri;

    .line 2279
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/contacts/suggested"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

    .line 2283
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_home"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    .line 2284
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/album_view_by_user"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

    .line 2286
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/album_view_by_album_and_owner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    .line 2292
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    .line 2293
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    .line 2296
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_album"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    .line 2298
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_event"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_EVENT_ID_URI:Landroid/net/Uri;

    .line 2300
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_user"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    .line 2302
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photos_by_stream_and_owner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    .line 2304
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_notification_count"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_NOTIFICATION_COUNT_URI:Landroid/net/Uri;

    .line 2306
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_comment_by_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    .line 2308
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/photo_shape_by_photo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    .line 2311
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/network_data_transactions"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_URI:Landroid/net/Uri;

    .line 2313
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/network_data_stats"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_URI:Landroid/net/Uri;

    .line 2316
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/platform_audience"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_URI:Landroid/net/Uri;

    .line 2319
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/plus_pages"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_URI:Landroid/net/Uri;

    .line 2322
    const-string v0, "content://com.google.android.apps.plus.content.EsProvider/events"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    .line 2402
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 2404
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "account_status"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2406
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities"

    const/16 v3, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2407
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activity_view/activity/*"

    const/16 v3, 0x16

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2408
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities_stream_view/stream/*"

    const/16 v3, 0x15

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2410
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities_stream_view_by_circle/*"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2413
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "activities/summary"

    const/16 v3, 0x18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2415
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "comments_view/activity/*"

    const/16 v3, 0x1e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2417
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "location_queries_view/query/*"

    const/16 v3, 0x28

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2420
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "notifications"

    const/16 v3, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2422
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "circles"

    const/16 v3, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2424
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts"

    const/16 v3, 0x46

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2425
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/circle/*"

    const/16 v3, 0x47

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2426
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/query/*"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2427
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/query"

    invoke-virtual {v0, v1, v2, v7}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2428
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/id/*"

    const/16 v3, 0x48

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2429
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "contacts/suggested"

    const/16 v3, 0x49

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2431
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "circle_contact"

    const/16 v3, 0x3e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2433
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "conversations"

    const/16 v3, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2435
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "participants/conversation/*"

    const/16 v3, 0x6e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2438
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "message_notifications_view"

    const/16 v3, 0xa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2441
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "messages/conversation/*"

    const/16 v3, 0x78

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2444
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "messenger_suggestions"

    const/16 v3, 0x73

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2446
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "hangout_suggestions"

    const/16 v3, 0x74

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2448
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_home"

    const/16 v3, 0x82

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2450
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view/*"

    const/16 v3, 0x83

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2452
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view_by_user/*"

    const/16 v3, 0x84

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2453
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view_by_album_and_owner/*/*"

    const/16 v3, 0x90

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2456
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "album_view_by_stream/*"

    const/16 v3, 0x85

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2458
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_photo/*"

    const/16 v3, 0x86

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2459
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_album/*"

    const/16 v3, 0x87

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2460
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_event/*"

    const/16 v3, 0x91

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2461
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_user/*"

    const/16 v3, 0x8b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2462
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photos_by_stream_and_owner/*/*"

    const/16 v3, 0x8a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2464
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_comment_by_photo/*"

    const/16 v3, 0x8d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2465
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_shape_by_photo/*"

    const/16 v3, 0x8f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2466
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "photo_notification_count"

    const/16 v3, 0x8c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2468
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "network_data_transactions"

    const/16 v3, 0xb4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2469
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "network_data_stats"

    const/16 v3, 0xb5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2471
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "platform_audience/*"

    const/16 v3, 0xb6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2474
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    const-string v2, "plus_pages"

    const/16 v3, 0xbe

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2511
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2512
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_sync_time"

    const-string v2, "circle_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2514
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_sync_time"

    const-string v2, "last_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2516
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_stats_sync_time"

    const-string v2, "last_stats_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2518
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_contacted_time"

    const-string v2, "last_contacted_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2520
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "wipeout_stats"

    const-string v2, "wipeout_stats"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2522
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "people_sync_time"

    const-string v2, "people_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2524
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "people_last_update_token"

    const-string v2, "people_last_update_token"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2526
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "avatars_downloaded"

    const-string v2, "avatars_downloaded"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2528
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "audience_data"

    const-string v2, "audience_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2530
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "user_id"

    const-string v2, "user_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2532
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contacts_sync_version"

    const-string v2, "contacts_sync_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2534
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "push_notifications"

    const-string v2, "push_notifications"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2536
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_analytics_sync_time"

    const-string v2, "last_analytics_sync_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2539
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2540
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2542
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2544
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "data_state"

    const-string v2, "data_state"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2546
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_activity"

    const-string v2, "last_activity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2548
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "token"

    const-string v2, "token"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2550
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2552
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2554
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "source_name"

    const-string v2, "source_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2556
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "total_comment_count"

    const-string v2, "total_comment_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2558
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "plus_one_data"

    const-string v2, "plus_one_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2560
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "public"

    const-string v2, "public"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2562
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "acl_display"

    const-string v2, "acl_display"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2564
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_comment"

    const-string v2, "can_comment"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2566
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "can_reshare"

    const-string v2, "can_reshare"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2568
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "has_muted"

    const-string v2, "has_muted"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2570
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "has_read"

    const-string v2, "has_read"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2572
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "loc"

    const-string v2, "loc"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2574
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "media"

    const-string v2, "media"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2576
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2578
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2580
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "a2a_hangout_data"

    const-string v2, "a2a_hangout_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2582
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2584
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "photo_collection"

    const-string v2, "photo_collection"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2586
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "popular_post"

    const-string v2, "popular_post"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2588
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "content_flags"

    const-string v2, "content_flags"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2590
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "annotation"

    const-string v2, "annotation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2592
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "annotation_plaintext"

    const-string v2, "annotation_plaintext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2594
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2596
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "title_plaintext"

    const-string v2, "title_plaintext"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2598
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "original_author_id"

    const-string v2, "original_author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2600
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "original_author_name"

    const-string v2, "original_author_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2602
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "event_data"

    const-string v2, "event_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2604
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "embed_json"

    const-string v2, "embed_json"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2607
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2608
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2610
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2612
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2614
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "modified"

    const-string v2, "modified"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2617
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2618
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2620
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2622
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "comment_id"

    const-string v2, "comment_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2624
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2626
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "content"

    const-string v2, "content"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2628
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "created"

    const-string v2, "created"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2630
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2632
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "plus_one_data"

    const-string v2, "plus_one_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2635
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2636
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2638
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2640
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "location"

    const-string v2, "location"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2643
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2644
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2646
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notif_id"

    const-string v2, "notif_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2648
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "category"

    const-string v2, "category"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2650
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "message"

    const-string v2, "message"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2652
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2654
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "enabled"

    const-string v2, "enabled"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2656
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "read"

    const-string v2, "read"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2658
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_data"

    const-string v2, "circle_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2660
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_gaia_id"

    const-string v2, "pd_gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2662
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_album_id"

    const-string v2, "pd_album_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2664
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "pd_photo_id"

    const-string v2, "pd_photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2666
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "activity_id"

    const-string v2, "activity_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2668
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "ed_event"

    const-string v2, "ed_event"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2670
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "ed_event_id"

    const-string v2, "ed_event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2672
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "ed_creator_id"

    const-string v2, "ed_creator_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2674
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notification_type"

    const-string v2, "notification_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2676
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "coalescing_code"

    const-string v2, "coalescing_code"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2678
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "entity_type"

    const-string v2, "entity_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2681
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2682
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "circles.rowid AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2684
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_id"

    const-string v2, "circles.circle_id AS circle_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2686
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "circle_name"

    const-string v2, "circle_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2688
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2690
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "semantic_hints"

    const-string v2, "semantic_hints"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2692
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contact_count"

    const-string v2, "contact_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2694
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "member_ids"

    const-string v2, "group_concat(link_person_id, \'|\') AS member_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2697
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "show_order"

    const-string v2, "show_order"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2699
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2700
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "contacts.rowid AS _id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2702
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "person_id"

    const-string v2, "contacts.person_id AS person_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2705
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2707
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "avatar"

    const-string v2, "avatar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2709
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2711
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last_updated_time"

    const-string v2, "last_updated_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2713
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "profile_type"

    const-string v2, "profile_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2715
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "in_my_circles"

    const-string v2, "in_my_circles"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2717
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "for_sharing"

    const-string v2, "(CASE WHEN person_id IN (SELECT link_person_id FROM circle_contact WHERE link_circle_id IN (SELECT circle_id FROM circles WHERE semantic_hints & 64 != 0)) THEN 1 ELSE 0 END) AS for_sharing"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2719
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "blocked"

    const-string v2, "blocked"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2721
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "packed_circle_ids"

    const-string v2, "group_concat(link_circle_id, \'|\') AS packed_circle_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2724
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contact_update_time"

    const-string v2, "contact_update_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2726
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "contact_proto"

    const-string v2, "contact_proto"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2728
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "profile_update_time"

    const-string v2, "profile_update_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2730
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "profile_proto"

    const-string v2, "profile_proto"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2733
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 2734
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "email"

    const-string v2, "email"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2736
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 2738
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2740
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "person_id"

    const-string v2, "person_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2742
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "packed_circle_ids"

    const-string v2, "packed_circle_ids"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2744
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "phone"

    const-string v2, "phone"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2746
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_WITH_PHONES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "phone_type"

    const-string v2, "phone_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2749
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 2750
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "category"

    const-string v2, "category"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2752
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "category_label"

    const-string v2, "category_label"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2754
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "explanation"

    const-string v2, "explanation"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2756
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "properties"

    const-string v2, "properties"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2759
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2760
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2762
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2764
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_muted"

    const-string v2, "is_muted"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2766
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_visible"

    const-string v2, "is_visible"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2768
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_event_timestamp"

    const-string v2, "latest_event_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2770
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_timestamp"

    const-string v2, "latest_message_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2772
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "earliest_event_timestamp"

    const-string v2, "earliest_event_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2774
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "has_older_events"

    const-string v2, "has_older_events"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2776
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "unread_count"

    const-string v2, "unread_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2778
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2780
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "generated_name"

    const-string v2, "generated_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2782
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_text"

    const-string v2, "latest_message_text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2784
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_image_url"

    const-string v2, "latest_message_image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2786
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_id"

    const-string v2, "latest_message_author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2788
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_type"

    const-string v2, "latest_message_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2790
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_full_name"

    const-string v2, "latest_message_author_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2792
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_first_name"

    const-string v2, "latest_message_author_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2794
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "latest_message_author_type"

    const-string v2, "latest_message_author_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2796
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_group"

    const-string v2, "is_group"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2798
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_pending_accept"

    const-string v2, "is_pending_accept"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2800
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_id"

    const-string v2, "inviter_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2802
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_full_name"

    const-string v2, "inviter_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2804
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_first_name"

    const-string v2, "inviter_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2806
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_type"

    const-string v2, "inviter_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2808
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "fatal_error_type"

    const-string v2, "fatal_error_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2810
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_pending_leave"

    const-string v2, "is_pending_leave"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2812
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_awaiting_event_stream"

    const-string v2, "is_awaiting_event_stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2814
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "is_awaiting_older_events"

    const-string v2, "is_awaiting_older_events"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2816
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_event_timestamp"

    const-string v2, "first_event_timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2818
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "packed_participants"

    const-string v2, "packed_participants"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2821
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2822
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2824
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "participant_id"

    const-string v2, "participant_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2826
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2828
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2830
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2832
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "active"

    const-string v2, "active"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2834
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sequence"

    const-string v2, "sequence"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2836
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2839
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2840
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2842
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "participant_id"

    const-string v2, "participant_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2844
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2846
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2849
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2850
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2852
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "participant_id"

    const-string v2, "participant_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2854
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first_name"

    const-string v2, "first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2856
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "full_name"

    const-string v2, "full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2859
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2860
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2862
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "message_id"

    const-string v2, "message_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2864
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2866
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2868
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "text"

    const-string v2, "text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2870
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2872
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2874
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2876
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_first_name"

    const-string v2, "author_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2878
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_full_name"

    const-string v2, "author_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2880
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_type"

    const-string v2, "author_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2882
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "image_url"

    const-string v2, "image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2885
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2886
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2888
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "message_id"

    const-string v2, "message_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2890
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_id"

    const-string v2, "conversation_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2892
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2894
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "text"

    const-string v2, "text"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2896
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "image_url"

    const-string v2, "image_url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2898
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2900
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2902
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2904
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "notification_seen"

    const-string v2, "notification_seen"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2906
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_full_name"

    const-string v2, "author_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2908
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_first_name"

    const-string v2, "author_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2910
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "author_type"

    const-string v2, "author_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2912
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_muted"

    const-string v2, "conversation_muted"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2914
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_group"

    const-string v2, "conversation_group"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2916
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_name"

    const-string v2, "conversation_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2918
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "generated_name"

    const-string v2, "generated_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2920
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_pending_accept"

    const-string v2, "conversation_pending_accept"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2923
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "conversation_pending_leave"

    const-string v2, "conversation_pending_leave"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2926
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_id"

    const-string v2, "inviter_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2928
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_full_name"

    const-string v2, "inviter_full_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2930
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_first_name"

    const-string v2, "inviter_first_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2932
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "inviter_type"

    const-string v2, "inviter_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2935
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2936
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2938
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "photo_count"

    const-string v2, "photo_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2940
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "height"

    const-string v2, "height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2942
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "image"

    const-string v2, "image"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2944
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "notification_count"

    const-string v2, "notification_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2946
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2948
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "photo_home_key"

    const-string v2, "photo_home_key"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2950
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "size"

    const-string v2, "size"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2952
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "sort_order"

    const-string v2, "sort_order"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2954
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2956
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2958
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2960
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    const-string v1, "width"

    const-string v2, "width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2963
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2964
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_NOTIFICATION_MAP:Ljava/util/HashMap;

    const-string v1, "_count"

    const-string v2, "notification_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2967
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2968
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2969
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2970
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_type"

    const-string v2, "album_type"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2971
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_key"

    const-string v2, "album_key"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2972
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "cover_photo_id"

    const-string v2, "cover_photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2973
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "entity_version"

    const-string v2, "entity_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2974
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "height"

    const-string v2, "height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2975
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "is_activity"

    const-string v2, "is_activity"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2976
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_id"

    const-string v2, "owner_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2977
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_count"

    const-string v2, "photo_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2978
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2979
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "size"

    const-string v2, "size"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2980
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "sort_order"

    const-string v2, "sort_order"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2981
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "stream_id"

    const-string v2, "stream_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2982
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2983
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2984
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2985
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "width"

    const-string v2, "width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2987
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2988
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2989
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "action_state"

    const-string v2, "action_state"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2990
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_id"

    const-string v2, "album_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2991
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_name"

    const-string v2, "album_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2992
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "album_stream"

    const-string v2, "album_stream"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2993
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "comment_count"

    const-string v2, "comment_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2994
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "description"

    const-string v2, "description"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2995
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "downloadable"

    const-string v2, "downloadable"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2996
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "entity_version"

    const-string v2, "entity_version"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2997
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "height"

    const-string v2, "height"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2998
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_id"

    const-string v2, "owner_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2999
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "owner_name"

    const-string v2, "owner_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3000
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "pending_status"

    const-string v2, "pending_status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3001
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3002
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_by_me"

    const-string v2, "plusone_by_me"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3003
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_count"

    const-string v2, "plusone_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3004
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_data"

    const-string v2, "plusone_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3005
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_id"

    const-string v2, "plusone_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3006
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "fingerprint"

    const-string v2, "fingerprint"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3007
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "timestamp"

    const-string v2, "timestamp"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3008
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3009
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "upload_status"

    const-string v2, "upload_status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3010
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "url"

    const-string v2, "url"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3011
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "video_data"

    const-string v2, "video_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3012
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "width"

    const-string v2, "width"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3013
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "_count"

    const-string v2, "count(*)  as _count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3014
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_of_user_id"

    const-string v2, "NULL AS photo_of_user_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3018
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_MAP:Ljava/util/HashMap;

    .line 3021
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3022
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "event_id"

    const-string v2, "event_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3025
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_MAP:Ljava/util/HashMap;

    .line 3028
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3029
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_of_user_id"

    const-string v2, "photos_of_user.photo_of_user_id as photo_of_user_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3033
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3034
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "photo_id"

    const-string v2, "photo_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3036
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "bounds"

    const-string v2, "bounds"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3038
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "creator_id"

    const-string v2, "creator_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3040
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "shape_id"

    const-string v2, "shape_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3042
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "status"

    const-string v2, "status"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3044
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "subject_id"

    const-string v2, "subject_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3046
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "creator_name"

    const-string v2, "creator_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3048
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    const-string v1, "subject_name"

    const-string v2, "subject_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3051
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3052
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3054
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "author_id"

    const-string v2, "author_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3056
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "comment_id"

    const-string v2, "comment_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3058
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "content"

    const-string v2, "content"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3060
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "create_time"

    const-string v2, "create_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3062
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "plusone_data"

    const-string v2, "plusone_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3064
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "truncated"

    const-string v2, "truncated"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3066
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "update_time"

    const-string v2, "update_time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3068
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    const-string v1, "owner_name"

    const-string v2, "contacts.name as owner_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3071
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3072
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3074
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3076
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "time"

    const-string v2, "time"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3078
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "network_duration"

    const-string v2, "network_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3081
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "process_duration"

    const-string v2, "process_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3084
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sent"

    const-string v2, "sent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3086
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "recv"

    const-string v2, "recv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3088
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "req_count"

    const-string v2, "req_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3090
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "exception"

    const-string v2, "exception"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3093
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3094
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3096
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3098
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "first"

    const-string v2, "first"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3100
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "last"

    const-string v2, "last"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3102
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "network_duration"

    const-string v2, "network_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3104
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "process_duration"

    const-string v2, "process_duration"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3106
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "sent"

    const-string v2, "sent"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3108
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "recv"

    const-string v2, "recv"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3110
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "req_count"

    const-string v2, "req_count"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3113
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3114
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "package_name"

    const-string v2, "package_name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3116
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "audience_data"

    const-string v2, "audience_data"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3120
    sput-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "gaia_id"

    const-string v2, "gaia_id"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3121
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "name"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3122
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 957
    return-void
.end method

.method public static analyzeDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 4
    .parameter "context"
    .parameter "account"

    .prologue
    .line 4379
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4381
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "ANALYZE"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4382
    const-string v1, "ANALYZE sqlite_master"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 4383
    return-void
.end method

.method public static appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;
    .registers 4
    .parameter "builder"
    .parameter "account"

    .prologue
    .line 3959
    const-string v0, "account"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 3
    .parameter "uri"
    .parameter "account"

    .prologue
    .line 3967
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static buildActivityViewUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 3889
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3890
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3891
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 3892
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "account"
    .parameter "queryKey"

    .prologue
    .line 3919
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3920
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3921
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 3922
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;
    .registers 6
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 3979
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_BY_CONVERSATION_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3980
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3981
    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3983
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .registers 4
    .parameter "account"

    .prologue
    .line 4024
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 4025
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 4027
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;
    .registers 6
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 4009
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 4010
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "conversation"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 4011
    const-string v1, "account"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 4013
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildPeopleQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZZLjava/lang/String;I)Landroid/net/Uri;
    .registers 9
    .parameter "account"
    .parameter "queryKey"
    .parameter "includePlusPages"
    .parameter "includePeopleInCircles"
    .parameter "activityId"
    .parameter "limit"

    .prologue
    .line 3940
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_QUERY_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3941
    .local v0, builder:Landroid/net/Uri$Builder;
    if-nez p1, :cond_a

    const-string p1, ""

    .end local p1
    :cond_a
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3942
    const-string v1, "limit"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3943
    const-string v1, "self_gaia_id"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3944
    const-string v2, "plus_pages"

    if-eqz p2, :cond_40

    const-string v1, "true"

    :goto_25
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3946
    const-string v2, "in_circles"

    if-eqz p3, :cond_43

    const-string v1, "true"

    :goto_2e
    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3948
    if-eqz p4, :cond_38

    .line 3949
    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3951
    :cond_38
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 3952
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1

    .line 3944
    :cond_40
    const-string v1, "false"

    goto :goto_25

    .line 3946
    :cond_43
    const-string v1, "false"

    goto :goto_2e
.end method

.method public static buildPlatformAudienceUri(Ljava/lang/String;)Landroid/net/Uri;
    .registers 3
    .parameter "packageName"

    .prologue
    .line 4038
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 4039
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 4040
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "account"
    .parameter "streamKey"

    .prologue
    .line 3874
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_STREAM_VIEW_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 3875
    .local v0, builder:Landroid/net/Uri$Builder;
    const-string v1, "stream"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 3876
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 3877
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public static declared-synchronized cleanupData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "force"

    .prologue
    .line 4321
    const-class v12, Lcom/google/android/apps/plus/content/EsProvider;

    monitor-enter v12

    :try_start_3
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 4323
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v11, Ljava/io/File;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v7

    .line 4324
    .local v7, startLength:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 4327
    .local v9, startTime:J
    const-wide/32 v13, 0x989680

    cmp-long v11, v7, v13

    if-gez v11, :cond_8f

    if-nez p2, :cond_8f

    .line 4349
    const-string v11, "EsProvider"

    const/4 v13, 0x4

    invoke-static {v11, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_8d

    .line 4350
    new-instance v11, Ljava/io/File;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 4351
    .local v4, endLength:J
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 4352
    .local v6, sb:Ljava/lang/StringBuffer;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long v2, v13, v9

    .line 4354
    .local v2, deltaTime:J
    const-wide/16 v13, 0x3e8

    div-long v13, v2, v13

    invoke-virtual {v6, v13, v14}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, "."

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-wide/16 v13, 0x3e8

    rem-long v13, v2, v13

    invoke-virtual {v11, v13, v14}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, " seconds"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4359
    const-string v11, "EsProvider"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, ">>>>> cleanup db took "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " old size: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", new size: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8d
    .catchall {:try_start_3 .. :try_end_8d} :catchall_122

    .line 4363
    .end local v2           #deltaTime:J
    .end local v4           #endLength:J
    .end local v6           #sb:Ljava/lang/StringBuffer;
    :cond_8d
    :goto_8d
    monitor-exit v12

    return-void

    .line 4331
    :cond_8f
    :try_start_8f
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_92
    .catchall {:try_start_8f .. :try_end_92} :catchall_12a

    .line 4333
    :try_start_92
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPostsData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4337
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsNotificationData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4338
    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->cleanupData(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4339
    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsPhotosData;->cleanupData(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4340
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4341
    invoke-static {}, Lcom/google/android/apps/plus/content/EsNetworkData;->cleanupData$3105fef4()V

    .line 4342
    const-string v11, "platform_audience"

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v1, v11, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 4343
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 4344
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b5
    .catchall {:try_start_92 .. :try_end_b5} :catchall_125

    .line 4346
    :try_start_b5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_b8
    .catchall {:try_start_b5 .. :try_end_b8} :catchall_12a

    .line 4349
    :try_start_b8
    const-string v11, "EsProvider"

    const/4 v13, 0x4

    invoke-static {v11, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_8d

    .line 4350
    new-instance v11, Ljava/io/File;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 4351
    .restart local v4       #endLength:J
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 4352
    .restart local v6       #sb:Ljava/lang/StringBuffer;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long v2, v13, v9

    .line 4354
    .restart local v2       #deltaTime:J
    const-wide/16 v13, 0x3e8

    div-long v13, v2, v13

    invoke-virtual {v6, v13, v14}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, "."

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v11

    const-wide/16 v13, 0x3e8

    rem-long v13, v2, v13

    invoke-virtual {v11, v13, v14}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v11

    const-string v13, " seconds"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4359
    const-string v11, "EsProvider"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, ">>>>> cleanup db took "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " old size: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", new size: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_120
    .catchall {:try_start_b8 .. :try_end_120} :catchall_122

    goto/16 :goto_8d

    .line 4321
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v2           #deltaTime:J
    .end local v4           #endLength:J
    .end local v6           #sb:Ljava/lang/StringBuffer;
    .end local v7           #startLength:J
    .end local v9           #startTime:J
    :catchall_122
    move-exception v11

    monitor-exit v12

    throw v11

    .line 4346
    .restart local v1       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v7       #startLength:J
    .restart local v9       #startTime:J
    :catchall_125
    move-exception v11

    :try_start_126
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v11
    :try_end_12a
    .catchall {:try_start_126 .. :try_end_12a} :catchall_12a

    .line 4349
    :catchall_12a
    move-exception v11

    :try_start_12b
    const-string v13, "EsProvider"

    const/4 v14, 0x4

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_193

    .line 4350
    new-instance v13, Ljava/io/File;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 4351
    .restart local v4       #endLength:J
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 4352
    .restart local v6       #sb:Ljava/lang/StringBuffer;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    sub-long v2, v13, v9

    .line 4354
    .restart local v2       #deltaTime:J
    const-wide/16 v13, 0x3e8

    div-long v13, v2, v13

    invoke-virtual {v6, v13, v14}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-wide/16 v14, 0x3e8

    rem-long v14, v2, v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, " seconds"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4359
    const-string v13, "EsProvider"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, ">>>>> cleanup db took "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " old size: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", new size: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4361
    .end local v2           #deltaTime:J
    .end local v4           #endLength:J
    .end local v6           #sb:Ljava/lang/StringBuffer;
    :cond_193
    throw v11
    :try_end_194
    .catchall {:try_start_12b .. :try_end_194} :catchall_122
.end method

.method public static deleteDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 3
    .parameter "context"
    .parameter "account"

    .prologue
    .line 4372
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->deleteDatabase()V

    .line 4373
    return-void
.end method

.method static getIndexSQLs()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 4187
    const/16 v1, 0xf

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE INDEX contacts_in_my_circles ON contacts(in_my_circles,person_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE INDEX contacts_name ON contacts(name)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE INDEX contacts_sort_key ON contacts(sort_key)"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE INDEX contacts_gaia_id ON contacts(gaia_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE UNIQUE INDEX circle_contact_forward ON circle_contact(link_circle_id,link_person_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE UNIQUE INDEX circle_contact_backward ON circle_contact(link_person_id,link_circle_id)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE INDEX contact_search_key ON contact_search(search_key)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE INDEX album_album_id ON album(album_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE INDEX photo_photo_id ON photo(photo_id)"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CREATE INDEX photo_comment_photo_id ON photo_comment(photo_id,comment_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CREATE INDEX photo_shape_photo_id ON photo_shape(photo_id,shape_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CREATE INDEX photos_in_stream_photo_id ON photos_in_stream(stream_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CREATE INDEX photos_in_album_album_id ON photos_in_album(album_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CREATE INDEX photos_in_event_event_id ON photos_in_event(event_id)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE INDEX photos_of_user_photo_id ON photo_comment(photo_id)"

    aput-object v2, v0, v1

    .line 4221
    .local v0, sqls:[Ljava/lang/String;
    return-object v0
.end method

.method protected static getTableSQLs()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 4116
    const/16 v1, 0x2e

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE TABLE account_status (user_id TEXT,last_sync_time INT DEFAULT(-1),last_stats_sync_time INT DEFAULT(-1),last_contacted_time INT DEFAULT(-1),wipeout_stats INT DEFAULT(0),circle_sync_time INT DEFAULT(-1),people_sync_time INT DEFAULT(-1),people_last_update_token TEXT,suggested_people_sync_time INT DEFAULT(-1),suggested_celeb_sync_time INT DEFAULT(-1),blocked_people_sync_time INT DEFAULT(-1),event_list_sync_time INT DEFAULT(-1),event_themes_sync_time INT DEFAULT(-1),avatars_downloaded INT DEFAULT(0),audience_data BLOB,contacts_sync_version INT DEFAULT(0),push_notifications INT DEFAULT(0),last_analytics_sync_time INT DEFAULT(-1));"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "INSERT INTO account_status DEFAULT VALUES;"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE TABLE activity_streams (stream_key TEXT NOT NULL,activity_id TEXT NOT NULL,sort_index INT NOT NULL,last_activity INT,token TEXT,PRIMARY KEY (stream_key,activity_id));"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE TABLE activities (_id INTEGER PRIMARY KEY, activity_id TEXT UNIQUE NOT NULL, data_state INT NOT NULL DEFAULT (0), author_id TEXT NOT NULL, source_name TEXT, total_comment_count INT NOT NULL, plus_one_data BLOB, public INT NOT NULL, acl_display TEXT, can_comment INT NOT NULL, can_reshare INT NOT NULL, has_muted INT NOT NULL, has_read INT NOT NULL, loc BLOB, media BLOB, created INT NOT NULL, modified INT NOT NULL, popular_post INT NOT NULL DEFAULT(0), content_flags INT NOT NULL DEFAULT(0), annotation TEXT, annotation_plaintext TEXT, title TEXT, title_plaintext TEXT, original_author_id TEXT, original_author_name TEXT, a2a_hangout_data BLOB, event_id TEXT, photo_collection BLOB, embed_json BLOB, album_id TEXT);"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE TABLE media (_id INTEGER PRIMARY KEY,activity_id TEXT NOT NULL,thumbnail_url TEXT NOT NULL,FOREIGN KEY (activity_id) REFERENCES activities(activity_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE TABLE activity_comments (_id INTEGER PRIMARY KEY,activity_id TEXT NOT NULL,comment_id TEXT UNIQUE NOT NULL,author_id TEXT NOT NULL,content TEXT,created INT NOT NULL,plus_one_data BLOB,FOREIGN KEY (activity_id) REFERENCES activities(activity_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE TABLE locations (_id INTEGER PRIMARY KEY,qrid INT NOT NULL,name TEXT,location BLOB,FOREIGN KEY (qrid) REFERENCES location_queries(_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE TABLE location_queries (_id INTEGER PRIMARY KEY,key TEXT UNIQUE NOT NULL);"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE TABLE notifications (_id INTEGER, notif_id TEXT UNIQUE NOT NULL, coalescing_code TEXT PRIMARY KEY, category INT NOT NULL DEFAULT(0), message TEXT, enabled INT, read INT NOT NULL, seen INT NOT NULL, timestamp INT NOT NULL, circle_data BLOB, pd_gaia_id TEXT, pd_album_id TEXT, pd_photo_id INT, activity_id TEXT, ed_event INT DEFAULT(0),ed_event_id TEXT, ed_creator_id TEXT, notification_type INT NOT NULL DEFAULT(0),entity_type INT NOT NULL DEFAULT(0));"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "CREATE TABLE contacts (person_id TEXT PRIMARY KEY,gaia_id TEXT,avatar TEXT,name TEXT,sort_key TEXT,last_updated_time INT,profile_type INT DEFAULT(0),profile_state INT DEFAULT(0),in_my_circles INT DEFAULT(0),blocked INT DEFAULT(0) );"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CREATE TABLE circles (circle_id TEXT PRIMARY KEY,circle_name TEXT,sort_key TEXT,type INT, contact_count INT,semantic_hints INT NOT NULL DEFAULT(0),show_order INT);"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "CREATE TABLE circle_contact (link_circle_id TEXT NOT NULL,link_person_id TEXT NOT NULL,UNIQUE (link_circle_id, link_person_id), FOREIGN KEY (link_circle_id) REFERENCES circles(circle_id) ON DELETE CASCADE,FOREIGN KEY (link_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "CREATE TABLE suggested_people (_id INTEGER PRIMARY KEY, suggested_person_id TEXT NOT NULL,dismissed INT DEFAULT(0),sort_order INT DEFAULT(0),category TEXT NOT NULL,category_label TEXT,category_sort_key TEXT,explanation TEXT,properties TEXT );"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "CREATE TABLE circle_action (gaia_id TEXT NOT NULL,notification_id INT NOT NULL,UNIQUE (gaia_id, notification_id), FOREIGN KEY (notification_id) REFERENCES notifications(notif_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE TABLE photo_home (_id INTEGER PRIMARY KEY AUTOINCREMENT,type TEXT NOT NULL,photo_count INT,sort_order INT NOT NULL DEFAULT( 100 ),timestamp INT,notification_count INT);"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CREATE TABLE photo_home_cover (photo_home_key INT NOT NULL,photo_id INT,url TEXT NOT NULL,width INT,height INT,size INT,image BLOB, FOREIGN KEY (photo_home_key) REFERENCES photo_home(_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CREATE TABLE profiles (profile_person_id TEXT PRIMARY KEY,contact_update_time INT,contact_proto BLOB,profile_update_time INT,profile_proto BLOB,FOREIGN KEY (profile_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "CREATE TABLE album ( _id INTEGER PRIMARY KEY AUTOINCREMENT, album_id TEXT UNIQUE NOT NULL, title TEXT, photo_count INT, sort_order INT NOT NULL DEFAULT( 100 ), owner_id TEXT, timestamp INT, entity_version INT, album_type TEXT NOT NULL DEFAULT(\'ALL_OTHERS\'), cover_photo_id INT, stream_id TEXT, is_activity BOOLEAN DEFAULT \'0\' );"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "CREATE TABLE album_cover (album_key INT NOT NULL,photo_id INT,url TEXT,width INT,height INT,size INT, FOREIGN KEY (album_key) REFERENCES album(_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CREATE TABLE photo (_id INTEGER PRIMARY KEY AUTOINCREMENT, photo_id INT NOT NULL, url TEXT, title TEXT, description TEXT, action_state INT, comment_count INT, owner_id TEXT, plus_one_key INT NOT NULL, width INT, height INT, album_id TEXT NOT NULL, timestamp INT, entity_version INT, fingerprint BLOB, video_data BLOB, upload_status TEXT, downloadable BOOLEAN, UNIQUE (photo_id) FOREIGN KEY (album_id) REFERENCES album(album_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "CREATE TABLE photo_comment (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, comment_id TEXT UNIQUE NOT NULL, author_id TEXT NOT NULL, content TEXT, create_time INT, truncated INT, update_time INT, plusone_data BLOB, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "CREATE TABLE photo_plusone (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, plusone_id TEXT, plusone_by_me BOOLEAN DEFAULT \'0\' NOT NULL, plusone_count INTEGER, plusone_data BLOB, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE );"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "CREATE TABLE photos_in_album (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, album_id INT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "CREATE TABLE photos_of_user (photo_id INT NOT NULL, photo_of_user_id TEXT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "CREATE TABLE photos_in_event (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, event_id TEXT NOT NULL, UNIQUE (photo_id, event_id) FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "CREATE TABLE photos_in_stream (_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, stream_id TEXT NOT NULL, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "CREATE TABLE photo_shape (shape_id INTEGER PRIMARY KEY, photo_id INT NOT NULL, subject_id TEXT, creator_id TEXT NOT NULL, status TEXT, bounds BLOB, FOREIGN KEY (photo_id) REFERENCES photo(photo_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "CREATE TABLE conversations (_id INTEGER PRIMARY KEY, conversation_id TEXT, is_muted INT, is_visible INT, latest_event_timestamp INT, latest_message_timestamp INT, earliest_event_timestamp INT, has_older_events INT, unread_count INT, name TEXT, generated_name TEXT, latest_message_text TEXT, latest_message_image_url TEXT, latest_message_author_id TEXT, latest_message_type INT, is_group INT, is_pending_accept INT, inviter_id TEXT, fatal_error_type INT, is_pending_leave INT, is_awaiting_event_stream INT, is_awaiting_older_events INT, first_event_timestamp INT, packed_participants TEXT, UNIQUE (conversation_id ));"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "CREATE TABLE conversation_participants (conversation_id INT, participant_id TEXT, active INT, sequence INT, UNIQUE (conversation_id,participant_id) ON CONFLICT REPLACE, FOREIGN KEY (conversation_id) REFERENCES conversations(_id) ON DELETE CASCADE, FOREIGN KEY (participant_id) REFERENCES participants(participant_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "CREATE TABLE participants (_id INTEGER PRIMARY KEY, participant_id TEXT UNIQUE ON CONFLICT REPLACE, full_name TEXT, first_name TEXT,type INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "CREATE TABLE messages (_id INTEGER PRIMARY KEY, message_id TEXT, conversation_id INT, author_id TEXT, text TEXT, timestamp INT, status INT, type INT, notification_seen INT, image_url TEXT, FOREIGN KEY (conversation_id) REFERENCES conversations(_id) ON DELETE CASCADE,FOREIGN KEY (author_id) REFERENCES participants(participant_id) ON DELETE CASCADE, UNIQUE (conversation_id,timestamp) ON CONFLICT REPLACE);"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "CREATE TABLE messenger_suggestions (_id INTEGER PRIMARY KEY, participant_id TEXT UNIQUE ON CONFLICT REPLACE, full_name TEXT, first_name TEXT,sequence INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "CREATE TABLE hangout_suggestions (_id INTEGER PRIMARY KEY, participant_id TEXT UNIQUE ON CONFLICT REPLACE, full_name TEXT, first_name TEXT,sequence INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "CREATE TABLE realtimechat_metadata (key TEXT UNIQUE, value TEXT)"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "CREATE TABLE analytics_events (event_data BLOB NOT NULL)"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "CREATE TABLE search (search_key TEXT NOT NULL,continuation_token TEXT,PRIMARY KEY (search_key));"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "CREATE TABLE contact_search(search_person_id TEXT NOT NULL,search_key_type TEXT NOT NULL DEFAULT(0),search_key TEXT,FOREIGN KEY (search_person_id) REFERENCES contacts(person_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "CREATE TABLE network_data_transactions(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL,time INT,sent INT,recv INT,network_duration INT,process_duration INT,req_count INT,exception TEXT);"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "CREATE TABLE network_data_stats(_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL,first INT,last INT,sent INT,recv INT,network_duration INT,process_duration INT,req_count INT);"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "CREATE TABLE platform_audience(package_name TEXT PRIMARY KEY, audience_data BLOB);"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "CREATE TABLE events(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT UNIQUE NOT NULL, owner_gaia_id TEXT NOT NULL, activity_id TEXT UNIQUE, name TEXT, source INT, update_timestamp INT, refresh_timestamp INT, activity_refresh_timestamp INT, invitee_roster_timestamp INT, fingerprint INT NOT NULL DEFAULT(0), start_time INT NOT NULL, end_time INT NOT NULL, can_invite_people INT DEFAULT (0), can_post_photos INT DEFAULT (0), can_comment INT DEFAULT(0) NOT NULL, mine INT DEFAULT (0) NOT NULL, polling_token TEXT,resume_token TEXT,display_time INT DEFAULT (0),event_data BLOB, invitee_roster BLOB);"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "CREATE TABLE event_people(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT NOT NULL, gaia_id TEXT NOT NULL, FOREIGN KEY (event_id) REFERENCES events(event_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "CREATE TABLE plus_pages(gaia_id TEXT PRIMARY KEY, name TEXT);"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "CREATE TABLE event_activities(_id INTEGER PRIMARY KEY AUTOINCREMENT, event_id TEXT NOT NULL, type INT, owner_gaia_id TEXT, owner_name TEXT, timestamp INT, fingerprint INT NOT NULL DEFAULT(0), url TEXT,comment TEXT,data BLOB, FOREIGN KEY (event_id) REFERENCES events(event_id) ON DELETE CASCADE);"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "CREATE TABLE event_themes(_id INTEGER PRIMARY KEY AUTOINCREMENT, theme_id INTEGER UNIQUE NOT NULL, is_default INT DEFAULT(0), is_featured INT DEFAULT(0), image_url TEXT NOT NULL);"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "CREATE TABLE deep_link_installs(_id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INT DEFAULT(0), package_name TEXT UNIQUE NOT NULL, activity_id TEXT NOT NULL, author_id TEXT NOT NULL);"

    aput-object v2, v0, v1

    .line 4164
    .local v0, sqls:[Ljava/lang/String;
    return-object v0
.end method

.method static getViewNames()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 4228
    const/16 v1, 0x11

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "activities_stream_view"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "activity_view"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "comments_view"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "location_queries_view"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "conversations_view"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "participants_view"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "messages_view"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "photo_home_view"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "album_view"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "photo_view"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "photos_by_album_view"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "photos_by_event_view"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "photos_by_stream_view"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "photos_by_user_view"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "photo_shape_view"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "message_notifications_view"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "deep_link_installs_view"

    aput-object v2, v0, v1

    .line 4247
    .local v0, views:[Ljava/lang/String;
    return-object v0
.end method

.method static getViewSQLs()[Ljava/lang/String;
    .registers 3

    .prologue
    .line 4254
    const/16 v1, 0x11

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "CREATE VIEW activities_stream_view AS SELECT activity_streams.stream_key as stream_key,activity_streams.sort_index as sort_index,activity_streams.last_activity as last_activity,activity_streams.token as token,activities._id as _id,activities.activity_id as activity_id,activities.author_id as author_id,activities.source_name as source_name,activities.total_comment_count as total_comment_count,activities.plus_one_data as plus_one_data,activities.public as public,activities.acl_display as acl_display,activities.can_comment as can_comment,activities.can_reshare as can_reshare,activities.has_muted as has_muted,activities.has_read as has_read,activities.loc as loc,activities.media as media,activities.created as created,activities.modified as modified,activities.a2a_hangout_data as a2a_hangout_data,activities.data_state as data_state,activities.event_id as event_id,activities.photo_collection as photo_collection,activities.popular_post as popular_post,activities.content_flags as content_flags,activities.annotation as annotation,activities.annotation_plaintext as annotation_plaintext,activities.title as title,activities.title_plaintext as title_plaintext,activities.original_author_id as original_author_id,activities.original_author_name as original_author_name,activities.embed_json as embed_json,events.event_data as event_data,contacts.name as name FROM activity_streams INNER JOIN activities ON activity_streams.activity_id=activities.activity_id INNER JOIN contacts ON activities.author_id=contacts.gaia_id LEFT OUTER JOIN events ON activities.event_id=events.event_id WHERE data_state    IN (1, 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "CREATE VIEW activity_view AS SELECT activities._id as _id,activities.activity_id as activity_id,activities.author_id as author_id,activities.source_name as source_name,activities.total_comment_count as total_comment_count,activities.plus_one_data as plus_one_data,activities.public as public,activities.acl_display as acl_display,activities.can_comment as can_comment,activities.can_reshare as can_reshare,activities.has_muted as has_muted,activities.has_read as has_read,activities.loc as loc,activities.media as media,activities.created as created,activities.modified as modified,activities.a2a_hangout_data as a2a_hangout_data,activities.data_state as data_state,contacts.name as name,activities.photo_collection as photo_collection,activities.popular_post as popular_post,activities.content_flags as content_flags,activities.annotation as annotation,activities.annotation_plaintext as annotation_plaintext,activities.title as title,activities.title_plaintext as title_plaintext,activities.original_author_id as original_author_id,activities.original_author_name as original_author_name,activities.embed_json as embed_json,events.event_data as event_data FROM activities JOIN contacts ON activities.author_id=contacts.gaia_id LEFT OUTER JOIN events ON activities.event_id=events.event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CREATE VIEW comments_view AS SELECT activity_comments._id as _id,activity_comments.activity_id as activity_id,activity_comments.comment_id as comment_id,activity_comments.author_id as author_id,activity_comments.content as content,activity_comments.created as created,activity_comments.plus_one_data as plus_one_data,contacts.name as name FROM activity_comments JOIN contacts ON activity_comments.author_id=contacts.gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "CREATE VIEW location_queries_view AS SELECT location_queries.key as key,locations._id as _id,locations.name as name,locations.location as location FROM location_queries LEFT JOIN locations ON location_queries._id=locations.qrid"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "CREATE VIEW conversations_view AS SELECT conversations._id as _id, conversations.conversation_id as conversation_id, conversations.is_muted as is_muted, conversations.is_visible as is_visible, conversations.latest_event_timestamp as latest_event_timestamp, conversations.latest_message_timestamp as latest_message_timestamp, conversations.earliest_event_timestamp as earliest_event_timestamp, conversations.has_older_events as has_older_events, conversations.unread_count as unread_count, conversations.name as name, conversations.generated_name as generated_name, conversations.latest_message_text as latest_message_text, conversations.latest_message_image_url as latest_message_image_url, conversations.latest_message_author_id as latest_message_author_id, conversations.latest_message_type as latest_message_type, conversations.is_group as is_group, conversations.is_pending_accept as is_pending_accept, conversations.inviter_id as inviter_id, conversations.fatal_error_type as fatal_error_type, conversations.is_pending_leave as is_pending_leave, conversations.is_awaiting_event_stream as is_awaiting_event_stream, conversations.is_awaiting_older_events as is_awaiting_older_events, conversations.first_event_timestamp as first_event_timestamp, conversations.packed_participants as packed_participants, participants.full_name as latest_message_author_full_name, participants.first_name as latest_message_author_first_name, participants.type as latest_message_author_type, inviter_alias.full_name as inviter_full_name, inviter_alias.first_name as inviter_first_name, inviter_alias.type as inviter_type  FROM conversations LEFT JOIN participants ON conversations.latest_message_author_id=participants.participant_id LEFT JOIN participants inviter_alias ON conversations.inviter_id=inviter_alias.participant_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "CREATE VIEW participants_view AS SELECT participants._id as _id, participants.participant_id as participant_id, participants.full_name as full_name, participants.first_name as first_name, participants.type as type, conversation_participants.conversation_id as conversation_id, conversation_participants.active as active, conversation_participants.sequence as sequence FROM participants JOIN conversation_participants ON participants.participant_id=conversation_participants.participant_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "CREATE VIEW messages_view AS SELECT messages._id as _id, messages.message_id as message_id, messages.conversation_id as conversation_id, messages.author_id as author_id, messages.text as text, messages.timestamp as timestamp, messages.status as status, messages.type as type, messages.image_url as image_url, participants.full_name as author_full_name, participants.first_name as author_first_name, participants.type as author_type FROM messages LEFT JOIN participants ON messages.author_id=participants.participant_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "CREATE VIEW photo_home_view AS SELECT photo_home._id as _id, photo_home.photo_count as photo_count, photo_home.notification_count as notification_count, photo_home.sort_order as sort_order, photo_home.timestamp as timestamp, photo_home.type as type, photo_home_cover.height as height, photo_home_cover.image as image, photo_home_cover.photo_id as photo_id, photo_home_cover.photo_home_key as photo_home_key, photo_home_cover.size as size, photo_home_cover.url as url, photo_home_cover.width as width FROM photo_home LEFT JOIN photo_home_cover ON photo_home._id=photo_home_cover.photo_home_key"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "CREATE VIEW album_view AS SELECT album._id as _id, album.album_id as album_id, album.entity_version as entity_version, album.is_activity as is_activity, album.owner_id as owner_id, album.photo_count as photo_count, album.sort_order as sort_order, album.stream_id as stream_id, album.timestamp as timestamp, album.title as title, album.cover_photo_id as cover_photo_id, album.album_type as album_type, album_cover.album_key as album_key, album_cover.height as height, album_cover.photo_id as photo_id, album_cover.size as size, album_cover.url as url, album_cover.width as width FROM album LEFT JOIN album_cover ON album._id=album_cover.album_key"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_SQL:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "CREATE VIEW photo_shape_view AS SELECT photo_shape.photo_id as photo_id, photo_shape.bounds as bounds, photo_shape.creator_id as creator_id, photo_shape.shape_id as shape_id, photo_shape.status as status, photo_shape.subject_id as subject_id, (SELECT a.name FROM contacts as a WHERE a.gaia_id=photo_shape.creator_id ) AS creator_name, (SELECT b.name FROM contacts as b WHERE b.gaia_id=photo_shape.subject_id ) AS subject_name FROM photo_shape"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "CREATE VIEW message_notifications_view AS SELECT messages._id as _id, messages.message_id as message_id, messages.conversation_id as conversation_id, messages.author_id as author_id, messages.text as text, messages.image_url as image_url, messages.timestamp as timestamp, messages.status as status, messages.type as type, messages.notification_seen as notification_seen, author_alias.full_name as author_full_name, author_alias.first_name as author_first_name, author_alias.type as author_type, conversations.is_muted as conversation_muted, conversations.is_visible as conversation_visible, conversations.is_group as conversation_group, conversations.is_pending_accept as conversation_pending_accept, conversations.is_pending_leave as conversation_pending_leave, conversations.name as conversation_name, conversations.generated_name as generated_name, inviter_alias.participant_id as inviter_id, inviter_alias.full_name as inviter_full_name, inviter_alias.first_name as inviter_first_name, inviter_alias.type as inviter_type FROM messages LEFT JOIN participants author_alias ON messages.author_id=author_alias.participant_id LEFT JOIN conversations ON messages.conversation_id=conversations._id LEFT JOIN participants inviter_alias ON conversations.inviter_id=inviter_alias.participant_id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "CREATE VIEW deep_link_installs_view AS SELECT deep_link_installs._id as _id,deep_link_installs.timestamp as timestamp,deep_link_installs.package_name as package_name,contacts.name as name,activities.source_name as source_name,activities.embed_json as embed_json FROM deep_link_installs INNER JOIN activities ON deep_link_installs.activity_id=activities.activity_id INNER JOIN contacts ON deep_link_installs.author_id=contacts.gaia_id;"

    aput-object v2, v0, v1

    .line 4273
    .local v0, sqls:[Ljava/lang/String;
    return-object v0
.end method

.method private static insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V
    .registers 9
    .parameter "db"
    .parameter "circleId"
    .parameter "name"
    .parameter "type"
    .parameter "showOrder"

    .prologue
    .line 4302
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4303
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4304
    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4305
    const-string v1, "type"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4306
    const-string v1, "contact_count"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4307
    const-string v1, "semantic_hints"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4308
    const-string v1, "show_order"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4309
    const-string v1, "circles"

    const-string v2, "circle_id"

    const/4 v3, 0x4

    invoke-virtual {p0, v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 4311
    return-void
.end method

.method public static insertVirtualCircles(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 6
    .parameter "context"
    .parameter "db"

    .prologue
    const/4 v3, -0x1

    .line 4282
    const-string v0, "v.nearby"

    const v1, 0x7f0800df

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2710

    invoke-static {p1, v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4287
    const-string v0, "v.all.circles"

    const v1, 0x7f0800e0

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4292
    const-string v0, "v.whatshot"

    const v1, 0x7f0800e1

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {p1, v0, v1, v3, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircle(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;II)V

    .line 4295
    return-void
.end method

.method private static varargs isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 13
    .parameter "projection"
    .parameter "columns"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 4389
    if-nez p0, :cond_5

    .line 4410
    :cond_4
    :goto_4
    return v8

    .line 4394
    :cond_5
    array-length v10, p1

    if-ne v10, v8, :cond_1a

    .line 4395
    aget-object v2, p1, v9

    .line 4396
    .local v2, column:Ljava/lang/String;
    move-object v0, p0

    .local v0, arr$:[Ljava/lang/String;
    array-length v5, p0

    .local v5, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_d
    if-ge v3, v5, :cond_36

    aget-object v7, v0, v3

    .line 4397
    .local v7, test:Ljava/lang/String;
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 4396
    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    .line 4402
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v2           #column:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    .end local v7           #test:Ljava/lang/String;
    :cond_1a
    move-object v0, p0

    .restart local v0       #arr$:[Ljava/lang/String;
    array-length v5, p0

    .restart local v5       #len$:I
    const/4 v3, 0x0

    .restart local v3       #i$:I
    move v4, v3

    .end local v0           #arr$:[Ljava/lang/String;
    .end local v3           #i$:I
    .end local v5           #len$:I
    .local v4, i$:I
    :goto_1e
    if-ge v4, v5, :cond_36

    aget-object v7, v0, v4

    .line 4403
    .restart local v7       #test:Ljava/lang/String;
    move-object v1, p1

    .local v1, arr$:[Ljava/lang/String;
    array-length v6, p1

    .local v6, len$:I
    const/4 v3, 0x0

    .end local v4           #i$:I
    .restart local v3       #i$:I
    :goto_25
    if-ge v3, v6, :cond_32

    aget-object v2, v1, v3

    .line 4404
    .restart local v2       #column:Ljava/lang/String;
    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 4403
    add-int/lit8 v3, v3, 0x1

    goto :goto_25

    .line 4402
    .end local v2           #column:Ljava/lang/String;
    :cond_32
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    .end local v3           #i$:I
    .restart local v4       #i$:I
    goto :goto_1e

    .end local v1           #arr$:[Ljava/lang/String;
    .end local v4           #i$:I
    .end local v6           #len$:I
    .end local v7           #test:Ljava/lang/String;
    :cond_36
    move v8, v9

    .line 4410
    goto :goto_4
.end method

.method public static localeChanged(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    .prologue
    .line 3844
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 3845
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_7

    .line 3863
    :goto_6
    return-void

    .line 3849
    :cond_7
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    .line 3850
    .local v2, helper:Lcom/google/android/apps/plus/content/EsDatabaseHelper;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 3851
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V

    .line 3852
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3854
    :try_start_19
    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 3855
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1f
    .catchall {:try_start_19 .. :try_end_1f} :catchall_35

    .line 3857
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3860
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v3

    const-string v4, "com.google.android.apps.plus.content.EsProvider"

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    invoke-static {v3, v4, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_6

    .line 3857
    :catchall_35
    move-exception v3

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private static preparePeopleSearchQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .registers 15
    .parameter "qb"
    .parameter "query"
    .parameter "includePlusPages"
    .parameter "limit"
    .parameter "selfGaiaId"
    .parameter "includePeopleInCicles"
    .parameter "additionalContacts"

    .prologue
    const/16 v7, 0x25

    .line 3651
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "[\\u0009\\u000A\\u000B\\u000C\\u000D\\u0020\\u0085\\u00A0\\u1680\\u180E\\u2000\\u2001\\u2002\\u2003\\u2004\\u2005\\u2006\\u2007\\u2008\\u2009\\u200A\\u2028\\u2029\\u202F\\u205F\\u3000]"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 3655
    .local v0, clause:[Ljava/lang/String;
    const-string v1, ""

    .line 3656
    .local v1, filter:Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_f
    array-length v4, v0

    if-ge v2, v4, :cond_a5

    .line 3657
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "SELECT contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS filtered_person_id, MIN((CASE WHEN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_key_type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " THEN search_key"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ELSE NULL END)) AS email"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " JOIN contact_search"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ON (contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=search_person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") WHERE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "search_key GLOB "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v0, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x2a

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND in_my_circles"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!=0 GROUP BY filtered_person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " INTERSECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3656
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_f

    .line 3673
    :cond_a5
    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0xb

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 3675
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_143

    .line 3676
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UNION SELECT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "contacts."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "person_id AS filtered_person_id, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " NULL AS email"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " FROM contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " WHERE gaia_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " IN ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") AND ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "name LIKE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " OR name"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIKE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "% "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3687
    :cond_143
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " LIMIT "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 3690
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "contacts JOIN ("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") ON (contacts"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".person_id"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=filtered_person_id) LEFT OUTER JOIN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "circle_contact ON ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "contacts."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "person_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "circle_contact."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "link_person_id)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 3697
    .local v3, tables:Ljava/lang/String;
    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3699
    const-string v4, "gaia_id"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3700
    const-string v4, " != \'"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3701
    invoke-virtual {p0, p4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3702
    const-string v4, "\'"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3704
    if-nez p2, :cond_1c7

    .line 3705
    const-string v4, " AND "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3706
    const-string v4, "profile_type"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3707
    const-string v4, " != "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3708
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3711
    :cond_1c7
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1d7

    .line 3713
    const-string v4, " AND "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3714
    const-string v4, "0"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3717
    :cond_1d7
    if-nez p5, :cond_1e8

    .line 3718
    const-string v4, " AND "

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3719
    const-string v4, "in_my_circles"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3720
    const-string v4, " = 0"

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3722
    :cond_1e8
    return-void
.end method

.method private static varargs prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .registers 7
    .parameter "oldArgList"
    .parameter "args"

    .prologue
    const/4 v3, 0x0

    .line 3748
    if-eqz p1, :cond_6

    array-length v4, p1

    if-nez v4, :cond_8

    :cond_6
    move-object v1, p0

    .line 3759
    :cond_7
    :goto_7
    return-object v1

    .line 3751
    :cond_8
    if-nez p0, :cond_19

    move v2, v3

    .line 3752
    .local v2, oldArgCount:I
    :goto_b
    array-length v0, p1

    .line 3754
    .local v0, newArgCount:I
    add-int v4, v2, v0

    new-array v1, v4, [Ljava/lang/String;

    .line 3755
    .local v1, newArgs:[Ljava/lang/String;
    invoke-static {p1, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3756
    if-lez v2, :cond_7

    .line 3757
    invoke-static {p0, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_7

    .line 3751
    .end local v0           #newArgCount:I
    .end local v1           #newArgs:[Ljava/lang/String;
    .end local v2           #oldArgCount:I
    :cond_19
    array-length v2, p0

    goto :goto_b
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 3783
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Delete not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    .prologue
    .line 3791
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    sparse-switch v0, :sswitch_data_3c

    .line 3834
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3793
    :sswitch_1e
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.accounts"

    .line 3831
    :goto_20
    return-object v0

    .line 3798
    :sswitch_21
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.activities"

    goto :goto_20

    .line 3802
    :sswitch_24
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.comments"

    goto :goto_20

    .line 3806
    :sswitch_27
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.locations"

    goto :goto_20

    .line 3810
    :sswitch_2a
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.notifications"

    goto :goto_20

    .line 3815
    :sswitch_2d
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.contacts"

    goto :goto_20

    .line 3819
    :sswitch_30
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.conversations"

    goto :goto_20

    .line 3823
    :sswitch_33
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.participants"

    goto :goto_20

    .line 3827
    :sswitch_36
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.messages"

    goto :goto_20

    .line 3831
    :sswitch_39
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.message_notifications"

    goto :goto_20

    .line 3791
    :sswitch_data_3c
    .sparse-switch
        0x1 -> :sswitch_1e
        0x15 -> :sswitch_21
        0x16 -> :sswitch_21
        0x1e -> :sswitch_24
        0x28 -> :sswitch_27
        0x32 -> :sswitch_2a
        0x46 -> :sswitch_2d
        0x48 -> :sswitch_2d
        0x64 -> :sswitch_30
        0x6e -> :sswitch_33
        0x78 -> :sswitch_36
        0xa0 -> :sswitch_39
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 6
    .parameter "uri"
    .parameter "initialValues"

    .prologue
    .line 3767
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Insert not supported "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .registers 2

    .prologue
    .line 3129
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 39
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    .line 3138
    const-string v9, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 3139
    .local v24, accountIndexParameter:Ljava/lang/String;
    if-nez v24, :cond_21

    .line 3140
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Every URI must have the \'account\' parameter specified.\nURI: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 3144
    :cond_21
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 3146
    .local v23, accountIndex:I
    const/4 v13, 0x0

    .line 3147
    .local v13, groupBy:Ljava/lang/String;
    const-string v9, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3148
    .local v5, limit:Ljava/lang/String;
    move-object/from16 v12, p4

    .line 3150
    .local v12, queryArgs:[Ljava/lang/String;
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v29

    .line 3151
    .local v29, match:I
    const-string v9, "EsProvider"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_63

    .line 3152
    const-string v9, "EsProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "QUERY URI: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " -> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move/from16 v0, v29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3155
    :cond_63
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 3156
    .local v2, qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    sparse-switch v29, :sswitch_data_7be

    .line 3618
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unknown URI "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 3158
    :sswitch_82
    const-string v9, "account_status"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3159
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3160
    const/16 v21, 0x0

    .line 3623
    .local v21, orderBy:Ljava/lang/String;
    :cond_8e
    :goto_8e
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_96

    .line 3624
    move-object/from16 v21, p5

    .line 3626
    :cond_96
    const-string v9, "EsProvider"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_c7

    .line 3627
    const-string v17, "EsProvider"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v9, "QUERY: "

    move-object/from16 v0, v18

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v14, 0x0

    move-object v9, v2

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v15, p5

    move-object/from16 v16, v5

    invoke-virtual/range {v9 .. v16}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-static {v0, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3631
    :cond_c7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    move/from16 v0, v23

    invoke-static {v9, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;I)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v15

    const/16 v20, 0x0

    move-object v14, v2

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, v12

    move-object/from16 v19, v13

    move-object/from16 v22, v5

    invoke-virtual/range {v14 .. v22}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 3635
    .local v26, cursor:Landroid/database/Cursor;
    const-string v9, "EsProvider"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_107

    .line 3636
    const-string v9, "EsProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "QUERY results: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3638
    :cond_107
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-interface {v0, v9, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 3639
    return-object v26

    .line 3165
    .end local v21           #orderBy:Ljava/lang/String;
    .end local v26           #cursor:Landroid/database/Cursor;
    :sswitch_117
    const-string v9, "activities"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3166
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3167
    const/16 v21, 0x0

    .line 3168
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3172
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_125
    const-string v9, "activity_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3173
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3174
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3178
    :sswitch_144
    const-string v9, "activity_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3179
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3180
    const/16 v21, 0x0

    .line 3181
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3185
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_152
    const-string v9, "activities_stream_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3186
    const-string v9, "stream_key"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3187
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3188
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3189
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3191
    const/16 v21, 0x0

    .line 3192
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3196
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_17f
    const-string v9, "activities_stream_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3200
    const-string v9, "(\'g:\'||author_id) IN ( SELECT link_person_id FROM circle_contact WHERE link_circle_id=?)"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3204
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3205
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3206
    move-object/from16 v21, p5

    .line 3209
    .restart local v21       #orderBy:Ljava/lang/String;
    if-eqz v5, :cond_1b9

    .line 3210
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v27

    .line 3211
    .local v27, iLimit:J
    const-wide/16 v9, 0x14

    cmp-long v9, v27, v9

    if-lez v9, :cond_8e

    .line 3212
    const-wide/16 v9, 0x14

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_8e

    .line 3216
    .end local v27           #iLimit:J
    :cond_1b9
    const-wide/16 v9, 0x14

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 3218
    goto/16 :goto_8e

    .line 3222
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_1c1
    const-string v9, "comments_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3223
    const-string v9, "activity_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3224
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3225
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3226
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3227
    const/16 v21, 0x0

    .line 3228
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3232
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_1ee
    const-string v9, "location_queries_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3233
    const-string v9, "key"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3234
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3235
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3236
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->LOCATION_QUERIES_VIEW_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3237
    const/16 v21, 0x0

    .line 3238
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3242
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_21b
    const-string v9, "notifications"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3243
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3245
    if-eqz v5, :cond_23b

    .line 3246
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v27

    .line 3247
    .restart local v27       #iLimit:J
    const-wide/16 v9, 0xc8

    cmp-long v9, v27, v9

    if-lez v9, :cond_237

    .line 3248
    const-wide/16 v9, 0xc8

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .line 3254
    .end local v27           #iLimit:J
    :cond_237
    :goto_237
    const/16 v21, 0x0

    .line 3255
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3252
    .end local v21           #orderBy:Ljava/lang/String;
    :cond_23b
    const-wide/16 v9, 0xc8

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    goto :goto_237

    .line 3259
    :sswitch_242
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "member_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_297

    .line 3260
    const-string v30, "SELECT link_circle_id,link_person_id FROM circle_contact JOIN contacts AS c  ON (c.person_id=link_person_id) ORDER BY c.sort_key, UPPER(c.name)"

    .line 3268
    .local v30, orderedMembers:Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "circles LEFT OUTER JOIN ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") AS "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circle_contact ON ( "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circle_contact."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "link_circle_id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circles."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "circle_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3273
    const-string v13, "circle_id"

    .line 3277
    .end local v30           #orderedMembers:Ljava/lang/String;
    :goto_28e
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3278
    const/16 v21, 0x0

    .line 3279
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3275
    .end local v21           #orderBy:Ljava/lang/String;
    :cond_297
    const-string v9, "circles"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_28e

    .line 3283
    :sswitch_29d
    const-string v9, "person_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3284
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3285
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3290
    :sswitch_2bc
    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "contact_update_time"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "contact_proto"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string v11, "profile_update_time"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const-string v11, "profile_proto"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_312

    .line 3295
    const-string v32, "contacts LEFT OUTER JOIN profiles ON (contacts.person_id=profiles.profile_person_id)"

    .line 3304
    .local v32, tables:Ljava/lang/String;
    :goto_2dd
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "packed_circle_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_304

    .line 3306
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LEFT OUTER JOIN circle_contact ON ( circle_contact.link_person_id = contacts.person_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 3309
    const-string v13, "person_id"

    .line 3311
    :cond_304
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3312
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3313
    const-string v21, "sort_key, UPPER(name)"

    .line 3314
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3301
    .end local v21           #orderBy:Ljava/lang/String;
    .end local v32           #tables:Ljava/lang/String;
    :cond_312
    const-string v32, "contacts"

    .restart local v32       #tables:Ljava/lang/String;
    goto :goto_2dd

    .line 3318
    .end local v32           #tables:Ljava/lang/String;
    :sswitch_315
    const-string v9, "contacts JOIN circle_contact ON (contacts.person_id=circle_contact.link_person_id) JOIN circles ON (circle_contact.link_circle_id = circles.circle_id)"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3324
    const-string v9, "person_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3325
    const-string v9, " IN ("

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3327
    const-string v9, "SELECT "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3328
    const-string v9, "link_person_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3329
    const-string v9, " FROM "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3330
    const-string v9, "circle_contact"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3331
    const-string v9, " WHERE "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3332
    const-string v9, "link_circle_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3333
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3334
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3336
    const-string v9, ")"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3337
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3338
    const-string v21, "UPPER(name)"

    .line 3339
    .restart local v21       #orderBy:Ljava/lang/String;
    const-string v13, "person_id"

    .line 3340
    goto/16 :goto_8e

    .line 3344
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_36c
    const-string v32, "contacts JOIN suggested_people ON (contacts.person_id=suggested_people.suggested_person_id)"

    .line 3348
    .restart local v32       #tables:Ljava/lang/String;
    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "packed_circle_ids"

    aput-object v11, v9, v10

    move-object/from16 v0, p2

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/content/EsProvider;->isInProjection([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_395

    .line 3349
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " LEFT OUTER JOIN circle_contact ON ( circle_contact.link_person_id = contacts.person_id)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    .line 3352
    const-string v13, "suggested_people._id"

    .line 3354
    :cond_395
    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3355
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3356
    const-string p3, "dismissed=0 AND blocked=0"

    .line 3358
    const-string v21, "CAST (category_sort_key AS INTEGER),sort_order"

    .line 3360
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3364
    .end local v21           #orderBy:Ljava/lang/String;
    .end local v32           #tables:Ljava/lang/String;
    :sswitch_3a5
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v31

    .line 3365
    .local v31, segments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_463

    const-string v3, ""

    .line 3366
    .local v3, query:Ljava/lang/String;
    :goto_3b2
    const-string v9, "self_gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 3367
    .local v6, selfGaiaId:Ljava/lang/String;
    const-string v9, "true"

    const-string v10, "plus_pages"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 3369
    .local v4, includePlusPages:Z
    const-string v9, "true"

    const-string v10, "in_circles"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 3371
    .local v7, includePeopleInCircles:Z
    const-string v9, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 3372
    .local v25, activityId:Ljava/lang/String;
    const/4 v8, 0x0

    .line 3373
    .local v8, participants:Ljava/lang/String;
    if-eqz v25, :cond_41e

    .line 3374
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "SELECT author_id FROM activities WHERE activity_id =  "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v25 .. v25}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " UNION "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "SELECT author_id FROM activity_comments WHERE activity_id = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v25 .. v25}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3376
    :cond_41e
    const-string v9, "+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_42e

    const-string v9, "@"

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_433

    .line 3377
    :cond_42e
    const/4 v9, 0x1

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 3379
    :cond_433
    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/plus/content/EsProvider;->preparePeopleSearchQuery(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 3382
    const-string v13, "person_id"

    .line 3383
    const/4 v5, 0x0

    .line 3384
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_SEARCH_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3385
    const-string v21, "UPPER(name)"

    .line 3386
    .restart local v21       #orderBy:Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8e

    .line 3387
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "gaia_id IN ("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") DESC,"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_8e

    .line 3365
    .end local v3           #query:Ljava/lang/String;
    .end local v4           #includePlusPages:Z
    .end local v6           #selfGaiaId:Ljava/lang/String;
    .end local v7           #includePeopleInCircles:Z
    .end local v8           #participants:Ljava/lang/String;
    .end local v21           #orderBy:Ljava/lang/String;
    .end local v25           #activityId:Ljava/lang/String;
    :cond_463
    const/4 v9, 0x2

    move-object/from16 v0, v31

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3b2

    .line 3394
    .end local v31           #segments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_472
    const-string v9, "conversations_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3395
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3396
    move-object/from16 v21, p5

    .line 3397
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3401
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_480
    const-string v9, "messenger_suggestions"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3402
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->MESSENGER_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3403
    move-object/from16 v21, p5

    .line 3404
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3408
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_48e
    const-string v9, "hangout_suggestions"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3409
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3410
    move-object/from16 v21, p5

    .line 3411
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3415
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_49c
    const-string v9, "participants_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3416
    const-string v9, "conversation_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3417
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3418
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3419
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PARTICIPANTS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3420
    move-object/from16 v21, p5

    .line 3421
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3425
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_4c9
    const-string v9, "message_notifications_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3426
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGE_NOTIFICATIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3427
    move-object/from16 v21, p5

    .line 3428
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3432
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_4d7
    const-string v9, "messages_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3433
    const-string v9, "conversation_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3434
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3435
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3436
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->MESSAGES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3437
    move-object/from16 v21, p5

    .line 3438
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3442
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_504
    const-string v9, "photo_home_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3443
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3444
    move-object/from16 v21, p5

    .line 3445
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3449
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_512
    const-string v9, "album_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3450
    const-string v9, "owner_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3451
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3452
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3453
    const-string v9, "title"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3454
    const-string v9, " IS NOT NULL"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3455
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3456
    const-string v9, "url"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3457
    const-string v9, " IS NOT NULL"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3458
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3459
    const-string v9, "is_activity"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3460
    const-string v9, " = 0"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3461
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3462
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3463
    move-object/from16 v21, p5

    .line 3464
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3468
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_56c
    const-string v9, "album_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3469
    const-string v9, "album_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3470
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3471
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3472
    const-string v9, "owner_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3473
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3474
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3475
    const-string v9, "title"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3476
    const-string v9, " IS NOT NULL"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3477
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3478
    const-string v9, "is_activity"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3479
    const-string v9, " = 0"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3480
    const/4 v9, 0x2

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    const/4 v11, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3482
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3483
    move-object/from16 v21, p5

    .line 3484
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3488
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_5d4
    const-string v9, "photo_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3489
    const-string v9, "photo_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3490
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3491
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3492
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3493
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3494
    const-string v5, "1"

    .line 3495
    move-object/from16 v21, p5

    .line 3496
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3500
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_607
    const-string v9, "photos_by_album_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3501
    const-string v9, "album_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3502
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3503
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3504
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_ALBUM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3505
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3506
    move-object/from16 v21, p5

    .line 3507
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3511
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_638
    const-string v9, "photos_by_event_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3512
    const-string v9, "event_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3513
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3514
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3515
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_EVENT_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3516
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3517
    move-object/from16 v21, p5

    .line 3518
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3522
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_669
    const-string v9, "photos_by_user_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3523
    const-string v9, "photo_of_user_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3524
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3525
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3526
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_USER_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3527
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3528
    move-object/from16 v21, p5

    .line 3529
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3533
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_69a
    const-string v9, "photos_by_stream_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3534
    const-string v9, "stream_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3535
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3536
    const-string v9, " AND "

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3537
    const-string v9, "owner_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3538
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3539
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTOS_BY_STREAM_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3540
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3541
    move-object/from16 v21, p5

    .line 3542
    .restart local v21       #orderBy:Ljava/lang/String;
    const/4 v9, 0x2

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    const/4 v11, 0x1

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x2

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3544
    goto/16 :goto_8e

    .line 3548
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_6e8
    const-string v9, "photo_home_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3549
    const-string v9, "type"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3550
    const-string v9, "=\'"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3551
    const-string v9, "photos_of_me"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3552
    const-string v9, "\'"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3553
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_NOTIFICATION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3554
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3555
    move-object/from16 v21, p5

    .line 3556
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3560
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_70e
    const-string v9, "photo_comment JOIN contacts ON photo_comment.author_id=contacts.gaia_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3565
    const-string v9, "photo_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3566
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3567
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3568
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3569
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3570
    move-object/from16 v21, p5

    .line 3571
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3575
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_73f
    const-string v9, "photo_shape_view"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3576
    const-string v9, "photo_id"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3577
    const-string v9, "=?"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3578
    const/4 v9, 0x1

    new-array v10, v9, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v9

    const/4 v14, 0x1

    invoke-interface {v9, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    aput-object v9, v10, v11

    invoke-static {v12, v10}, Lcom/google/android/apps/plus/content/EsProvider;->prependArgs([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 3579
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPE_VIEW_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3580
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3581
    move-object/from16 v21, p5

    .line 3582
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3586
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_770
    const-string v9, "network_data_transactions"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3587
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_TRANSACTIONS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3588
    const/16 v21, 0x0

    .line 3589
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3593
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_77e
    const-string v9, "network_data_stats"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3594
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->NETWORK_DATA_STATS_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3595
    const/16 v21, 0x0

    .line 3596
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3600
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_78c
    const-string v9, "platform_audience"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3601
    const-string v9, "package_name"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3602
    const-string v9, "="

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 3603
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhereEscapeString(Ljava/lang/String;)V

    .line 3604
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PLATFORM_AUDIENCE_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3605
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setDistinct(Z)V

    .line 3606
    const/16 v21, 0x0

    .line 3607
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3611
    .end local v21           #orderBy:Ljava/lang/String;
    :sswitch_7af
    const-string v9, "plus_pages"

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 3612
    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PLUS_PAGES_PROJECTION_MAP:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 3613
    const/16 v21, 0x0

    .line 3614
    .restart local v21       #orderBy:Ljava/lang/String;
    goto/16 :goto_8e

    .line 3156
    nop

    :sswitch_data_7be
    .sparse-switch
        0x1 -> :sswitch_82
        0x14 -> :sswitch_144
        0x15 -> :sswitch_152
        0x16 -> :sswitch_125
        0x17 -> :sswitch_17f
        0x18 -> :sswitch_117
        0x1e -> :sswitch_1c1
        0x28 -> :sswitch_1ee
        0x32 -> :sswitch_21b
        0x3c -> :sswitch_242
        0x46 -> :sswitch_2bc
        0x47 -> :sswitch_315
        0x48 -> :sswitch_29d
        0x49 -> :sswitch_36c
        0x4a -> :sswitch_3a5
        0x64 -> :sswitch_472
        0x6e -> :sswitch_49c
        0x73 -> :sswitch_480
        0x74 -> :sswitch_48e
        0x78 -> :sswitch_4d7
        0x82 -> :sswitch_504
        0x84 -> :sswitch_512
        0x86 -> :sswitch_5d4
        0x87 -> :sswitch_607
        0x8a -> :sswitch_69a
        0x8b -> :sswitch_669
        0x8c -> :sswitch_6e8
        0x8d -> :sswitch_70e
        0x8f -> :sswitch_73f
        0x90 -> :sswitch_56c
        0x91 -> :sswitch_638
        0xa0 -> :sswitch_4c9
        0xb4 -> :sswitch_770
        0xb5 -> :sswitch_77e
        0xb6 -> :sswitch_78c
        0xbe -> :sswitch_7af
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 8
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 3775
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Update not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
