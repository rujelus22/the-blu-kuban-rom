.class public final enum Lcom/google/android/apps/plus/analytics/OzActions;
.super Ljava/lang/Enum;
.source "OzActions.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/analytics/OzActions;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum C2DM_MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_ADD_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_ADD_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_ADD_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_REMOVE_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_REMOVE_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_REMOVE_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_ATTRIBUTION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_ATTRIBUTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_ATTRIBUTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_ATTRIBUTION_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_RENAME:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum LOOP_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_CANCEL_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_OPEN_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PHOTOS_SURPRISE_CAID:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CLICKED_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CLICKED_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_UNDO_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_HELP:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

.field public static final enum TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;


# instance fields
.field private final mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x5

    const/16 v8, 0xd

    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v5, 0x4

    .line 19
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EXIT"

    const/4 v2, 0x0

    const-string v3, "natapp"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 20
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LAUNCH"

    const/4 v2, 0x1

    const-string v3, "natapp"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 22
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_HELP"

    const-string v2, "Settings"

    const/16 v3, 0x12

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_HELP:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 23
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_FEEDBACK"

    const/4 v2, 0x3

    const-string v3, "Settings"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 24
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_TOS"

    const-string v2, "Settings"

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 25
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SETTINGS_SIGNOUT"

    const-string v2, "Settings"

    const/16 v3, 0x15

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 27
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LOOP_POST"

    const/4 v2, 0x6

    const-string v3, "ttn"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 28
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "LOOP_CHECKIN"

    const/4 v2, 0x7

    const-string v3, "ttn"

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 30
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_SELECT_ACTIVITY"

    const-string v2, "str"

    const/16 v3, 0x16

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 31
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_SELECT_AUTHOR"

    const/16 v2, 0x9

    const-string v3, "pr"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 33
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_POST"

    const/16 v2, 0xa

    const-string v3, "ttn"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 36
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_CHANGE_ACL"

    const/16 v2, 0xb

    const-string v3, "ttn"

    const/16 v4, 0x5e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 38
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_ADD_VISIBILITY_ACL"

    const/16 v2, 0xc

    const-string v3, "ttn"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 39
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_REMOVE_VISIBILITY_ACL"

    const-string v2, "ttn"

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 40
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_ADD_CIRCLE_ACL"

    const/16 v2, 0xe

    const-string v3, "ttn"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 41
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_REMOVE_CIRCLE_ACL"

    const/16 v2, 0xf

    const-string v3, "ttn"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 42
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_ADD_INDIVIDUAL_ACL"

    const/16 v2, 0x10

    const-string v3, "ttn"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 43
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_REMOVE_INDIVIDUAL_ACL"

    const/16 v2, 0x11

    const-string v3, "ttn"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 45
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_CHANGE_LOCATION"

    const/16 v2, 0x12

    const-string v3, "ttn"

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 46
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_TAKE_PHOTO"

    const/16 v2, 0x13

    const-string v3, "ttn"

    const/16 v4, 0x5f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 47
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "COMPOSE_CHOOSE_PHOTO"

    const/16 v2, 0x14

    const-string v3, "ttn"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 49
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_SELECT_AUTHOR"

    const/16 v2, 0x15

    const-string v3, "pr"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 50
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_SELECT_PERSON"

    const/16 v2, 0x16

    const-string v3, "pr"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 51
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_SELECT_PHOTO"

    const/16 v2, 0x17

    const-string v3, "str"

    const/16 v4, 0x34

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 52
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_POST_COMMENT"

    const/16 v2, 0x18

    const-string v3, "str"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 53
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_RESHARE"

    const/16 v2, 0x19

    const-string v3, "str"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 55
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_REMOVE_ACTIVITY"

    const/16 v2, 0x1a

    const-string v3, "str"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 56
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_MUTE_ACTIVITY"

    const/16 v2, 0x1b

    const-string v3, "str"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 57
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_REPORT_ABUSE_ACTIVITY"

    const/16 v2, 0x1c

    const-string v3, "str"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 59
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_MARK_ACTIVITY_AS_READ"

    const/16 v2, 0x1d

    const-string v3, "str"

    const/16 v4, 0x39

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 60
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "STREAM_MARK_ACTIVITY_AS_READ"

    const/16 v2, 0x1e

    const-string v3, "str"

    const/16 v4, 0x39

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 64
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_REPORT_ABUSE_COMMENT"

    const/16 v2, 0x1f

    const-string v3, "str"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 65
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_OPEN_REPORT_ABUSE_COMMENT"

    const/16 v2, 0x20

    const-string v3, "str"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_OPEN_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 66
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_CANCEL_REPORT_ABUSE_COMMENT"

    const/16 v2, 0x21

    const-string v3, "str"

    const/16 v4, 0x1a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_CANCEL_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 68
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_DELETE_COMMENT"

    const/16 v2, 0x22

    const-string v3, "str"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 69
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_UP_EDIT_COMMENT"

    const/16 v2, 0x23

    const-string v3, "str"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 71
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PHOTOS_SURPRISE_CAID"

    const/16 v2, 0x24

    const-string v3, "phst"

    const/16 v4, 0x3b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PHOTOS_SURPRISE_CAID:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 73
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATIONS_START_NEW"

    const/16 v2, 0x25

    const-string v3, "messenger"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 74
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_ABORT_NEW"

    const/16 v2, 0x26

    const-string v3, "messenger"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 75
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_RETRY_SEND"

    const/16 v2, 0x27

    const-string v3, "messenger"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 77
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_POST"

    const/16 v2, 0x28

    const-string v3, "messenger"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 78
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_TAKE_PHOTO"

    const/16 v2, 0x29

    const-string v3, "messenger"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 79
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_CHOOSE_PHOTO"

    const/16 v2, 0x2a

    const-string v3, "messenger"

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 80
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_MUTE"

    const/16 v2, 0x2b

    const-string v3, "messenger"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 81
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_UNMUTE"

    const/16 v2, 0x2c

    const-string v3, "messenger"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 82
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_ADD_PEOPLE"

    const/16 v2, 0x2d

    const-string v3, "messenger"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 83
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_MAKE_CIRCLE"

    const/16 v2, 0x2e

    const-string v3, "messenger"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 84
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_LEAVE"

    const/16 v2, 0x2f

    const-string v3, "messenger"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 85
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "GROUP_CONVERSATION_RENAME"

    const/16 v2, 0x30

    const-string v3, "messenger"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_RENAME:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 87
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_POST"

    const/16 v2, 0x31

    const-string v3, "messenger"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 88
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_TAKE_PHOTO"

    const/16 v2, 0x32

    const-string v3, "messenger"

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 89
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO"

    const/16 v2, 0x33

    const-string v3, "messenger"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 91
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_MUTE"

    const/16 v2, 0x34

    const-string v3, "messenger"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 92
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_UNMUTE"

    const/16 v2, 0x35

    const-string v3, "messenger"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 93
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "ONE_ON_ONE_CONVERSATION_ADD_PEOPLE"

    const/16 v2, 0x36

    const-string v3, "messenger"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 95
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_OPTED_IN"

    const/16 v2, 0x37

    const-string v3, "Settings"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 96
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_OPTED_OUT"

    const/16 v2, 0x38

    const-string v3, "Settings"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 97
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_SYNC_ALL"

    const/16 v2, 0x39

    const-string v3, "Settings"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 99
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE"

    const/16 v2, 0x3a

    const-string v3, "Settings"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 100
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY"

    const/16 v2, 0x3b

    const-string v3, "Settings"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 101
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE"

    const/16 v2, 0x3c

    const-string v3, "Settings"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 103
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY"

    const/16 v2, 0x3d

    const-string v3, "Settings"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 106
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_ROAMING_ENABLED"

    const/16 v2, 0x3e

    const-string v3, "Settings"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 107
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CS_SETTINGS_ROAMING_DISABLED"

    const/16 v2, 0x3f

    const-string v3, "Settings"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 109
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CLICKED_PLUSONE"

    const/16 v2, 0x40

    const-string v3, "plusone"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 110
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PLUSONE_CONFIRMED"

    const/16 v2, 0x41

    const-string v3, "plusone"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 111
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PLUSONE_CANCELED"

    const/16 v2, 0x42

    const-string v3, "plusone"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 112
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_UNDO_PLUSONE_CONFIRMED"

    const/16 v2, 0x43

    const-string v3, "plusone"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 113
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_UNDO_PLUSONE_CANCELED"

    const/16 v2, 0x44

    const-string v3, "plusone"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 114
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_READ_PLUSONES"

    const/16 v2, 0x45

    const-string v3, "plusone"

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 115
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_READ_PLUSONES_ERROR"

    const/16 v2, 0x46

    const-string v3, "plusone"

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 116
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_WRITE_PLUSONE"

    const/16 v2, 0x47

    const-string v3, "plusone"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 117
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_WRITE_PLUSONE_ERROR"

    const/16 v2, 0x48

    const-string v3, "plusone"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 118
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_ERROR_PLUSONE"

    const/16 v2, 0x49

    const-string v3, "plusone"

    const/16 v4, 0x12

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 119
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PLUSONE_PREVIEW_SHOWN"

    const/16 v2, 0x4a

    const-string v3, "plusone"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 122
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CLICKED_SHARE"

    const/16 v2, 0x4b

    const-string v3, "ttn"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 124
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONFIRM_SHARE"

    const/16 v2, 0x4c

    const-string v3, "ttn"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 125
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CANCEL_SHARE"

    const/16 v2, 0x4d

    const-string v3, "ttn"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 128
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONFIRM_SHARE_FROM_PLUSONE"

    const/16 v2, 0x4e

    const-string v3, "ttn"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 130
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CANCEL_SHARE_FROM_PLUSONE"

    const/16 v2, 0x4f

    const-string v3, "ttn"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 132
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CLICKED_SHARE_FROM_PLUSONE"

    const/16 v2, 0x50

    const-string v3, "ttn"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 133
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_ERROR_SHARE"

    const/16 v2, 0x51

    const-string v3, "ttn"

    const/16 v4, 0x2f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 134
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_PREVIEW_SHOWN"

    const/16 v2, 0x52

    const-string v3, "ttn"

    const/16 v4, 0x59

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 135
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_PREVIEW_ERROR"

    const/16 v2, 0x53

    const-string v3, "ttn"

    const/16 v4, 0x5a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 136
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_COMMENT_ADDED"

    const/16 v2, 0x54

    const-string v3, "ttn"

    const/16 v4, 0x5b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 138
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST"

    const/16 v2, 0x55

    const-string v3, "ttn"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 139
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_ERROR"

    const/16 v2, 0x56

    const-string v3, "ttn"

    const/16 v4, 0x2f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 142
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_ATTACHMENT"

    const/16 v2, 0x57

    const-string v3, "ttn"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 146
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_COMMENT"

    const/16 v2, 0x58

    const-string v3, "ttn"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 149
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_LOCATION"

    const/16 v2, 0x59

    const-string v3, "ttn"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 152
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_URL"

    const/16 v2, 0x5a

    const-string v3, "ttn"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 155
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_POST_WITH_DEEP_LINK"

    const/16 v2, 0x5b

    const-string v3, "ttn"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 158
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CIRCLES_SHARE_ACL_ADDED"

    const/16 v2, 0x5c

    const-string v3, "ttn"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 159
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_PEOPLE_SHARE_ACL_ADDED"

    const/16 v2, 0x5d

    const-string v3, "ttn"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 160
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_GET_ACCOUNT"

    const/16 v2, 0x5e

    const-string v3, "signin"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 162
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_LOCATION"

    const/16 v2, 0x5f

    const-string v3, "ttn"

    const/16 v4, 0x5d

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 163
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_ACL"

    const/16 v2, 0x60

    const-string v3, "ttn"

    const/16 v4, 0x5e

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 165
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_CAMERA"

    const/16 v2, 0x61

    const-string v3, "ttn"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 168
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_SHARE_CLICKED_GALLERY"

    const/16 v2, 0x62

    const-string v3, "ttn"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 171
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONNECT_SHOW_OOB"

    const/16 v2, 0x63

    const-string v3, "oob"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 172
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_CONNECT_SELECT_ACCOUNT"

    const/16 v2, 0x64

    const-string v3, "oob"

    const/16 v4, 0x29

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 173
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_AUDIENCE_VIEW_CLICKED"

    const/16 v2, 0x65

    const-string v3, "ttn"

    const/16 v4, 0x3f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 174
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED"

    const/16 v2, 0x66

    const-string v3, "ttn"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 176
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "PLATFORM_AUDIENCE_VIEW_PERSON_ADDED"

    const/16 v2, 0x67

    const-string v3, "pr"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 179
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_ENABLED"

    const/16 v2, 0x68

    const-string v3, "Settings"

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 180
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CAMERA_SYNC_DISABLED"

    const/16 v2, 0x69

    const-string v3, "Settings"

    const/16 v4, 0x11

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 182
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATIONS_EMPTY"

    const/16 v2, 0x6a

    const-string v3, "messenger"

    const/16 v4, 0x13

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 183
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_AUTO_RETRY_SUCCESS"

    const/16 v2, 0x6b

    const-string v3, "messenger"

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 184
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_AUTO_RETRY_FAIL"

    const/16 v2, 0x6c

    const-string v3, "messenger"

    const/16 v4, 0x15

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 185
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_MANUAL_RETRY_FAIL"

    const/16 v2, 0x6d

    const-string v3, "messenger"

    const/16 v4, 0x16

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 186
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "CONVERSATION_MANUAL_RETRY_SUCCESS"

    const/16 v2, 0x6e

    const-string v3, "messenger"

    const/16 v4, 0x17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 187
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "C2DM_MESSAGE_RECEIVED"

    const/16 v2, 0x6f

    const-string v3, "messenger"

    const/16 v4, 0x18

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->C2DM_MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 188
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "MESSAGE_RECEIVED"

    const/16 v2, 0x70

    const-string v3, "messenger"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 190
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENTS_PARTY_MODE_ON"

    const/16 v2, 0x71

    const-string v3, "oevt"

    const/16 v4, 0x3a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 191
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "EVENTS_PARTY_MODE_OFF"

    const/16 v2, 0x72

    const-string v3, "oevt"

    const/16 v4, 0x3b

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 193
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_ATTRIBUTION_CONSUMED"

    const/16 v2, 0x73

    const-string v3, "dl"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 194
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_ATTRIBUTION_CLICKED"

    const/16 v2, 0x74

    const-string v3, "dl"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 195
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_ATTRIBUTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED"

    const/16 v2, 0x75

    const-string v3, "dl"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 196
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_OFFERED"

    const/16 v2, 0x76

    const-string v3, "dl"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 197
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_DECLINED"

    const/16 v2, 0x77

    const-string v3, "dl"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 198
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_ACCEPTED"

    const/16 v2, 0x78

    const-string v3, "dl"

    const/4 v4, 0x6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 199
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED"

    const/16 v2, 0x79

    const-string v3, "dl"

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 200
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_CONSUMED"

    const/16 v2, 0x7a

    const-string v3, "dl"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 201
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_UNRESOLVED"

    const/16 v2, 0x7b

    const-string v3, "dl"

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 202
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_ATTRIBUTION_ENABLED"

    const/16 v2, 0x7c

    const-string v3, "dl"

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 203
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE"

    const/16 v2, 0x7d

    const-string v3, "dl"

    const/16 v4, 0xb

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 204
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE"

    const/16 v2, 0x7e

    const-string v3, "dl"

    const/16 v4, 0xc

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 205
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION"

    const/16 v2, 0x7f

    const-string v3, "dl"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 208
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "TICKLE_NOTIFICATION_RECEIVED"

    const/16 v2, 0x80

    const-string v3, "nots"

    const/16 v4, 0x19

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 209
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "TICKLE_EVENT_RECEIVED"

    const/16 v2, 0x81

    const-string v3, "nots"

    const/16 v4, 0x1a

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 210
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "NOTIFICATION_FETCHED_FROM_TICKLE"

    const/16 v2, 0x82

    const-string v3, "nots"

    const/16 v4, 0x17

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 211
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "NOTIFICATION_FETCHED_FROM_USER_REFRESH"

    const/16 v2, 0x83

    const-string v3, "nots"

    const/16 v4, 0x18

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 212
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "AGGREGATED_NOTIFICATION_CLICKED"

    const/16 v2, 0x84

    const-string v3, "nots"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 213
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SINGLE_NOTIFICATION_CLICKED"

    const/16 v2, 0x85

    const-string v3, "nots"

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 216
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzActions;

    const-string v1, "SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION"

    const/16 v2, 0x86

    const-string v3, "phst"

    const/16 v4, 0x3f

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 16
    const/16 v0, 0x87

    new-array v0, v0, [Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EXIT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LAUNCH:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_HELP:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v9

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v7

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_VISIBILITY_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v1, v0, v8

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_CIRCLE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_ADD_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_REMOVE_INDIVIDUAL_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_OPEN_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_CANCEL_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PHOTOS_SURPRISE_CAID:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_START_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MAKE_CIRCLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_RENAME:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_IN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_OPTED_OUT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_MOBILE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_UPLOAD_VIA_PHOTOS_AND_VIDEOS_VIA_WIFI_ONLY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_ROAMING_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SHOW_OOB:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONNECT_SELECT_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATIONS_EMPTY:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->C2DM_MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->MESSAGE_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->AGGREGATED_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SINGLE_NOTIFICATION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzActions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V
    .registers 11
    .parameter
    .parameter
    .parameter "namespace"
    .parameter "typeNum"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/OzActions;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 226
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter "namespace"
    .parameter "typeNum"
    .parameter "typeStr"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 232
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 233
    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p4, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeStr:Ljava/lang/String;

    .line 237
    return-void
.end method

.method public static getName(Lcom/google/android/apps/plus/analytics/OzActions;)Ljava/lang/String;
    .registers 2
    .parameter "action"

    .prologue
    .line 263
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/OzActions;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzActions;
    .registers 2
    .parameter "name"

    .prologue
    .line 16
    const-class v0, Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/OzActions;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/analytics/OzActions;
    .registers 1

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/analytics/OzActions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/analytics/OzActions;

    return-object v0
.end method


# virtual methods
.method public final getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;
    .registers 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzActions;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    return-object v0
.end method
