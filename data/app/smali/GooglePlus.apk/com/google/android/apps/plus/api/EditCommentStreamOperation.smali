.class public final Lcom/google/android/apps/plus/api/EditCommentStreamOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "EditCommentStreamOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EditCommentRequest;",
        "Lcom/google/api/services/plusi/model/EditCommentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mCommentId:Ljava/lang/String;

.field private final mContent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "content"

    .prologue
    .line 46
    const-string v3, "editcomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/EditCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EditCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EditCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EditCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 53
    iput-object p5, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mActivityId:Ljava/lang/String;

    .line 54
    iput-object p6, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mCommentId:Ljava/lang/String;

    .line 55
    iput-object p7, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContent:Ljava/lang/String;

    .line 56
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/EditCommentResponse;

    .end local p1
    if-eqz p1, :cond_2c

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentResponse;->comment:Lcom/google/api/services/plusi/model/Comment;

    if-eqz v0, :cond_2c

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2c

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mCommentId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2c

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->updateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updatePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/Comment;)V

    :cond_2c
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/EditCommentRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->activityId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mCommentId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentText:Ljava/lang/String;

    return-void
.end method
