.class public Lcom/google/android/apps/plus/content/EsApiProvider;
.super Landroid/content/ContentProvider;
.source "EsApiProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    }
.end annotation


# static fields
.field private static sMatcher:Landroid/content/UriMatcher;

.field private static final sPlusoneCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/api/services/pos/model/Plusones;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPreviewCache:Landroid/support/v4/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/apps/plus/network/ApiaryActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/16 v4, 0x14

    .line 85
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    .line 86
    sput-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.ApiProvider"

    const-string v2, "plusone"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.ApiProvider"

    const-string v2, "account"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    const-string v1, "com.google.android.apps.plus.content.ApiProvider"

    const-string v2, "preview"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 107
    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v4}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    .line 109
    new-instance v0, Landroid/support/v4/util/LruCache;

    invoke-direct {v0, v4}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 231
    return-void
.end method

.method static synthetic access$000()Landroid/support/v4/util/LruCache;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/content/EsApiProvider;ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 50
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedUrls(ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/content/EsApiProvider;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/content/EsApiProvider;->updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    move-result-object v0

    return-object v0
.end method

.method private buildPlusOneCursorFromCache([Ljava/lang/String;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;
    .registers 12
    .parameter "urls"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Lcom/google/api/services/pos/model/Plusones;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 576
    .local p2, result:Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;,"Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults<Lcom/google/api/services/pos/model/Plusones;>;"
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 579
    .local v0, cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "com.google.circles.platform.result.extra.ERROR_CODE"

    iget-object v8, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 581
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "com.google.circles.platform.result.extra.ERROR_MESSAGE"

    iget-object v8, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/ServiceResult;->getReasonPhrase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    iget-object v6, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_ae

    .line 586
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 588
    .local v2, items:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/pos/model/Plusones;>;"
    sget-object v7, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v7

    .line 589
    const/4 v1, 0x0

    .local v1, i:I
    :goto_38
    :try_start_38
    array-length v6, p1

    if-ge v1, v6, :cond_82

    .line 591
    iget-object v6, p2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mResults:Ljava/util/Map;

    aget-object v8, p1, v1

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/pos/model/Plusones;

    .line 592
    .local v3, plusones:Lcom/google/api/services/pos/model/Plusones;
    if-nez v3, :cond_59

    .line 594
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    aget-object v8, p1, v1

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3           #plusones:Lcom/google/api/services/pos/model/Plusones;
    check-cast v3, Lcom/google/api/services/pos/model/Plusones;

    .line 597
    .restart local v3       #plusones:Lcom/google/api/services/pos/model/Plusones;
    :cond_59
    if-nez v3, :cond_6a

    .line 598
    new-instance v3, Lcom/google/api/services/pos/model/Plusones;

    .end local v3           #plusones:Lcom/google/api/services/pos/model/Plusones;
    invoke-direct {v3}, Lcom/google/api/services/pos/model/Plusones;-><init>()V

    .line 599
    .restart local v3       #plusones:Lcom/google/api/services/pos/model/Plusones;
    aget-object v6, p1, v1

    iput-object v6, v3, Lcom/google/api/services/pos/model/Plusones;->id:Ljava/lang/String;

    .line 600
    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->expandPlusOneToCursor(Lcom/google/api/services/pos/model/Plusones;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    .line 589
    :goto_67
    add-int/lit8 v1, v1, 0x1

    goto :goto_38

    .line 602
    :cond_6a
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    aget-object v8, p1, v1

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/pos/model/Plusones;

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->expandPlusOneToCursor(Lcom/google/api/services/pos/model/Plusones;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    .line 603
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7e
    .catchall {:try_start_38 .. :try_end_7e} :catchall_7f

    goto :goto_67

    .line 606
    .end local v3           #plusones:Lcom/google/api/services/pos/model/Plusones;
    :catchall_7f
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_82
    :try_start_82
    monitor-exit v7
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_7f

    .line 609
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v6, v7, :cond_af

    const/4 v5, 0x1

    .line 611
    .local v5, privileged:Z
    :goto_94
    if-eqz v5, :cond_ae

    .line 612
    new-instance v4, Lcom/google/api/services/pos/model/List;

    invoke-direct {v4}, Lcom/google/api/services/pos/model/List;-><init>()V

    .line 614
    .local v4, plusonesList:Lcom/google/api/services/pos/model/List;
    iput-object v2, v4, Lcom/google/api/services/pos/model/List;->items:Ljava/util/List;

    .line 615
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "com.google.android.apps.content.EXTRA_PLUSONES"

    invoke-static {}, Lcom/google/api/services/pos/model/ListJson;->getInstance()Lcom/google/api/services/pos/model/ListJson;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/google/api/services/pos/model/ListJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    .end local v1           #i:I
    .end local v2           #items:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/pos/model/Plusones;>;"
    .end local v4           #plusonesList:Lcom/google/api/services/pos/model/List;
    .end local v5           #privileged:Z
    :cond_ae
    return-object v0

    .line 609
    .restart local v1       #i:I
    .restart local v2       #items:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/pos/model/Plusones;>;"
    :cond_af
    const/4 v5, 0x0

    goto :goto_94
.end method

.method private static buildPreviewCursorFromCache([Ljava/lang/String;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;
    .registers 10
    .parameter "urls"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Lcom/google/android/apps/plus/network/ApiaryActivity;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .local p1, updateResults:Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;,"Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults<Lcom/google/android/apps/plus/network/ApiaryActivity;>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 527
    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "uri"

    aput-object v4, v3, v6

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 530
    .local v1, cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.circles.platform.result.extra.ERROR_CODE"

    iget-object v5, p1, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 532
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.circles.platform.result.extra.ERROR_MESSAGE"

    iget-object v5, p1, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/ServiceResult;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    array-length v3, p0

    new-array v0, v3, [Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 536
    .local v0, activities:[Lcom/google/android/apps/plus/network/ApiaryActivity;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_2f
    array-length v3, v0

    if-ge v2, v3, :cond_65

    array-length v3, p0

    if-ge v2, v3, :cond_65

    .line 537
    new-array v3, v7, [Ljava/lang/Object;

    aget-object v4, p0, v2

    aput-object v4, v3, v6

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 538
    iget-object v3, p1, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mResults:Ljava/util/Map;

    aget-object v4, p0, v2

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/network/ApiaryActivity;

    aput-object v3, v0, v2

    .line 539
    aget-object v3, v0, v2

    if-nez v3, :cond_62

    .line 540
    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    aget-object v4, p0, v2

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/network/ApiaryActivity;

    aput-object v3, v0, v2

    .line 536
    :cond_62
    add-int/lit8 v2, v2, 0x1

    goto :goto_2f

    .line 543
    :cond_65
    array-length v3, v0

    if-lez v3, :cond_71

    .line 544
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "com.google.android.apps.content.EXTRA_ACTIVITY"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 547
    :cond_71
    return-object v1
.end method

.method private static expandPlusOneToCursor(Lcom/google/api/services/pos/model/Plusones;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    .registers 11
    .parameter "plusones"
    .parameter "cursor"

    .prologue
    .line 399
    iget-object v5, p0, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    .line 400
    .local v5, setByViewer:Ljava/lang/Boolean;
    if-eqz v5, :cond_3a

    .line 401
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_37

    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_PLUSONED:Ljava/lang/Integer;

    .line 407
    .local v6, state:Ljava/lang/Integer;
    :goto_c
    const-wide/16 v0, 0x0

    .line 408
    .local v0, count:J
    iget-object v3, p0, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    .line 409
    .local v3, metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;
    if-eqz v3, :cond_1c

    .line 410
    iget-object v2, v3, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    .line 411
    .local v2, globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;
    if-eqz v2, :cond_1c

    .line 412
    iget-object v7, v2, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    invoke-virtual {v7}, Ljava/lang/Double;->longValue()J

    move-result-wide v0

    .line 416
    .end local v2           #globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;
    :cond_1c
    const/4 v7, 0x4

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/api/services/pos/model/Plusones;->id:Ljava/lang/String;

    aput-object v8, v4, v7

    const/4 v7, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x2

    aput-object v6, v4, v7

    const/4 v7, 0x3

    iget-object v8, p0, Lcom/google/api/services/pos/model/Plusones;->abtk:Ljava/lang/String;

    aput-object v8, v4, v7

    .line 420
    .local v4, row:[Ljava/lang/Object;
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 421
    return-void

    .line 401
    .end local v0           #count:J
    .end local v3           #metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;
    .end local v4           #row:[Ljava/lang/Object;
    .end local v6           #state:Ljava/lang/Integer;
    :cond_37
    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_NOTPLUSONED:Ljava/lang/Integer;

    goto :goto_c

    .line 404
    :cond_3a
    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_ANONYMOUS:Ljava/lang/Integer;

    .restart local v6       #state:Ljava/lang/Integer;
    goto :goto_c
.end method

.method private getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .registers 21
    .parameter "uri"
    .parameter "hostKey"

    .prologue
    .line 550
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 551
    .local v14, context:Landroid/content/Context;
    const-string v1, "pkg"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 553
    .local v16, pkgFromQuery:Ljava/lang/String;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v14}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v1, v2, :cond_5d

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5d

    .line 555
    move-object/from16 v4, v16

    .line 562
    .local v4, pkg:Ljava/lang/String;
    :goto_20
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v14}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v10, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "apiKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "clientId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "apiVersion"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-object/from16 v8, p2

    move-object v12, v6

    move-object v13, v1

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    return-object v7

    .line 557
    .end local v4           #pkg:Ljava/lang/String;
    :cond_5d
    invoke-virtual {v14}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 558
    .local v17, pm:Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v15

    .line 559
    .local v15, packages:[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v4, v15, v1

    .restart local v4       #pkg:Ljava/lang/String;
    goto :goto_20
.end method

.method private static getUncachedUrls(ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;
    .registers 9
    .parameter "skip"
    .parameter
    .parameter "urls"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(Z",
            "Landroid/support/v4/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "TT;>;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 343
    .local p1, cache:Landroid/support/v4/util/LruCache;,"Landroid/support/v4/util/LruCache<Landroid/net/Uri;TT;>;"
    new-instance v4, Ljava/util/ArrayList;

    array-length v5, p2

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 345
    .local v4, urlList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-nez p0, :cond_25

    .line 346
    monitor-enter p1

    .line 347
    move-object v0, p2

    .local v0, arr$:[Ljava/lang/String;
    :try_start_a
    array-length v2, p2

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_c
    if-ge v1, v2, :cond_20

    aget-object v3, v0, v1

    .line 348
    .local v3, url:Ljava/lang/String;
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_1d

    .line 349
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    :cond_1d
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 352
    .end local v3           #url:Ljava/lang/String;
    :cond_20
    monitor-exit p1
    :try_end_21
    .catchall {:try_start_a .. :try_end_21} :catchall_22

    .line 356
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #i$:I
    .end local v2           #len$:I
    :goto_21
    return-object v4

    .line 352
    .restart local v0       #arr$:[Ljava/lang/String;
    :catchall_22
    move-exception v5

    monitor-exit p1

    throw v5

    .line 354
    .end local v0           #arr$:[Ljava/lang/String;
    :cond_25
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_21
.end method

.method public static makePreviewUri(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Landroid/net/Uri;
    .registers 5
    .parameter "info"

    .prologue
    .line 726
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v0

    .line 727
    .local v0, source:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    const-string v1, "content://com.google.android.apps.plus.content.ApiProvider/preview"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "apiKey"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "clientId"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "apiVersion"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "pkg"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hostKey"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method private updatePlusoneEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    .registers 15
    .parameter "account"
    .parameter "info"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/network/ApiaryApiInfo;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Lcom/google/api/services/pos/model/Plusones;",
            ">;"
        }
    .end annotation

    .prologue
    .local p3, urlList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 277
    .local v1, context:Landroid/content/Context;
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 279
    .local v8, plusOnesMap:Ljava/util/Map;,"Ljava/util/Map<Landroid/net/Uri;Lcom/google/api/services/pos/model/Plusones;>;"
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_62

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 280
    .local v6, url:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/api/PlusOnesOperation;

    move-object v2, p1

    move-object v4, v3

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/PlusOnesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/lang/String;)V

    .line 282
    .local v0, op:Lcom/google/android/apps/plus/api/PlusOnesOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->start()V

    .line 284
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getErrorCode()I

    move-result v2

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_4b

    .line 285
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getPlusones()Lcom/google/api/services/pos/model/Plusones;

    move-result-object v9

    .line 286
    .local v9, plusone:Lcom/google/api/services/pos/model/Plusones;
    if-eqz v9, :cond_e

    .line 287
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v8, v2, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v4, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v4

    .line 290
    :try_start_3d
    sget-object v2, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v5, v9}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    monitor-exit v4
    :try_end_47
    .catchall {:try_start_3d .. :try_end_47} :catchall_48

    goto :goto_e

    :catchall_48
    move-exception v2

    monitor-exit v4

    throw v2

    .line 294
    .end local v9           #plusone:Lcom/google/api/services/pos/model/Plusones;
    :cond_4b
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/PlusOnesOperation;->getException()Ljava/lang/Exception;

    move-result-object v10

    invoke-direct {v3, v4, v5, v10}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;)V

    .line 298
    .end local v0           #op:Lcom/google/android/apps/plus/api/PlusOnesOperation;
    .end local v6           #url:Ljava/lang/String;
    :goto_61
    return-object v2

    :cond_62
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    invoke-direct {v2, v3, v8}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/Map;)V

    goto :goto_61
.end method

.method private updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;
    .registers 15
    .parameter "account"
    .parameter "info"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/network/ApiaryApiInfo;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults",
            "<",
            "Lcom/google/android/apps/plus/network/ApiaryActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .local p3, urlList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 313
    .local v1, context:Landroid/content/Context;
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 314
    .local v8, results:Ljava/util/Map;,"Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/apps/plus/network/ApiaryActivity;>;"
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_60

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 315
    .local v5, url:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;

    move-object v2, p1

    move-object v4, v3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    .line 317
    .local v0, op:Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->start()V

    .line 318
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->getErrorCode()I

    move-result v2

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_49

    .line 319
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 320
    .local v9, uri:Landroid/net/Uri;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v2

    invoke-interface {v8, v9, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v4, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    monitor-enter v4

    .line 322
    :try_start_3b
    sget-object v2, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->getActivity()Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v6

    invoke-virtual {v2, v9, v6}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    monitor-exit v4
    :try_end_45
    .catchall {:try_start_3b .. :try_end_45} :catchall_46

    goto :goto_e

    :catchall_46
    move-exception v2

    monitor-exit v4

    throw v2

    .line 324
    .end local v9           #uri:Landroid/net/Uri;
    :cond_49
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;->getException()Ljava/lang/Exception;

    move-result-object v10

    invoke-direct {v3, v4, v6, v10}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;)V

    .line 331
    .end local v0           #op:Lcom/google/android/apps/plus/network/ApiaryPreviewOperation;
    .end local v5           #url:Ljava/lang/String;
    :goto_5f
    return-object v2

    :cond_60
    new-instance v2, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    invoke-direct {v2, v3, v8}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/Map;)V

    goto :goto_5f
.end method

.method private writePlusOne(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;)Z
    .registers 26
    .parameter "uri"
    .parameter "values"
    .parameter "urlToPlusOne"

    .prologue
    .line 630
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 631
    .local v3, context:Landroid/content/Context;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/content/EsApiProvider;->getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v7

    .line 632
    .local v7, info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    .line 633
    .local v4, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 634
    .local v17, uriToPlusOne:Landroid/net/Uri;
    const-string v5, "state"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_PLUSONED:Ljava/lang/Integer;

    if-ne v5, v6, :cond_109

    const/4 v9, 0x1

    .line 636
    .local v9, add:Z
    :goto_26
    const-string v5, "token"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 638
    .local v10, token:Ljava/lang/String;
    const/4 v13, 0x0

    .line 639
    .local v13, cachedSetByViewer:Ljava/lang/Boolean;
    const/4 v12, 0x0

    .line 640
    .local v12, cachedCount:Ljava/lang/Double;
    const/4 v14, 0x0

    .line 641
    .local v14, hashOfCachedPlusone:I
    const/4 v15, 0x0

    .line 643
    .local v15, notify:Z
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v6

    .line 644
    :try_start_35
    sget-object v5, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/pos/model/Plusones;

    .line 645
    .local v11, cached:Lcom/google/api/services/pos/model/Plusones;
    if-eqz v11, :cond_84

    .line 647
    iget-object v13, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    .line 648
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    iput-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    .line 649
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v5, :cond_7f

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v5, :cond_7f

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v5, :cond_7f

    .line 652
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    .line 653
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v8, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    invoke-virtual {v12}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v18

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_10c

    const/4 v5, 0x1

    :goto_74
    int-to-double v0, v5

    move-wide/from16 v20, v0

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    iput-object v5, v8, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    .line 656
    :cond_7f
    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v14

    .line 657
    const/4 v15, 0x1

    .line 659
    :cond_84
    monitor-exit v6
    :try_end_85
    .catchall {:try_start_35 .. :try_end_85} :catchall_10f

    .line 662
    if-eqz v15, :cond_9a

    .line 663
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 665
    const/4 v15, 0x0

    .line 668
    :cond_9a
    new-instance v2, Lcom/google/android/apps/plus/api/WritePlusOneOperation;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/lang/String;ZLjava/lang/String;)V

    .line 670
    .local v2, op:Lcom/google/android/apps/plus/api/WritePlusOneOperation;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->start()V

    .line 671
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->getErrorCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_115

    .line 674
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->getPlusones()Lcom/google/api/services/pos/model/Plusones;

    move-result-object v16

    .line 675
    .local v16, plusone:Lcom/google/api/services/pos/model/Plusones;
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v6

    .line 676
    :try_start_b5
    sget-object v5, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11           #cached:Lcom/google/api/services/pos/model/Plusones;
    check-cast v11, Lcom/google/api/services/pos/model/Plusones;

    .line 677
    .restart local v11       #cached:Lcom/google/api/services/pos/model/Plusones;
    if-eqz v11, :cond_ea

    .line 678
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    if-eq v5, v8, :cond_ea

    .line 679
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    iput-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    .line 680
    if-eqz v12, :cond_e9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v5, :cond_e9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v5, :cond_e9

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v5, :cond_e9

    .line 684
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iput-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    .line 687
    :cond_e9
    const/4 v15, 0x1

    .line 690
    :cond_ea
    monitor-exit v6
    :try_end_eb
    .catchall {:try_start_b5 .. :try_end_eb} :catchall_112

    .line 712
    .end local v16           #plusone:Lcom/google/api/services/pos/model/Plusones;
    :goto_eb
    if-eqz v15, :cond_ff

    .line 713
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 717
    :cond_ff
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/WritePlusOneOperation;->getErrorCode()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_156

    const/4 v5, 0x1

    :goto_108
    return v5

    .line 634
    .end local v2           #op:Lcom/google/android/apps/plus/api/WritePlusOneOperation;
    .end local v9           #add:Z
    .end local v10           #token:Ljava/lang/String;
    .end local v11           #cached:Lcom/google/api/services/pos/model/Plusones;
    .end local v12           #cachedCount:Ljava/lang/Double;
    .end local v13           #cachedSetByViewer:Ljava/lang/Boolean;
    .end local v14           #hashOfCachedPlusone:I
    .end local v15           #notify:Z
    :cond_109
    const/4 v9, 0x0

    goto/16 :goto_26

    .line 653
    .restart local v9       #add:Z
    .restart local v10       #token:Ljava/lang/String;
    .restart local v11       #cached:Lcom/google/api/services/pos/model/Plusones;
    .restart local v12       #cachedCount:Ljava/lang/Double;
    .restart local v13       #cachedSetByViewer:Ljava/lang/Boolean;
    .restart local v14       #hashOfCachedPlusone:I
    .restart local v15       #notify:Z
    :cond_10c
    const/4 v5, -0x1

    goto/16 :goto_74

    .line 659
    .end local v11           #cached:Lcom/google/api/services/pos/model/Plusones;
    :catchall_10f
    move-exception v5

    monitor-exit v6

    throw v5

    .line 690
    .restart local v2       #op:Lcom/google/android/apps/plus/api/WritePlusOneOperation;
    .restart local v16       #plusone:Lcom/google/api/services/pos/model/Plusones;
    :catchall_112
    move-exception v5

    monitor-exit v6

    throw v5

    .line 691
    .end local v16           #plusone:Lcom/google/api/services/pos/model/Plusones;
    .restart local v11       #cached:Lcom/google/api/services/pos/model/Plusones;
    :cond_115
    sget-object v6, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v6

    .line 693
    :try_start_118
    sget-object v5, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11           #cached:Lcom/google/api/services/pos/model/Plusones;
    check-cast v11, Lcom/google/api/services/pos/model/Plusones;

    .line 694
    .restart local v11       #cached:Lcom/google/api/services/pos/model/Plusones;
    if-eqz v11, :cond_151

    invoke-virtual {v11}, Ljava/lang/Object;->hashCode()I

    move-result v5

    if-ne v14, v5, :cond_151

    .line 696
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    if-eq v5, v13, :cond_151

    .line 697
    iput-object v13, v11, Lcom/google/api/services/pos/model/Plusones;->isSetByViewer:Ljava/lang/Boolean;

    .line 698
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iput-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    .line 699
    if-eqz v12, :cond_150

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    if-eqz v5, :cond_150

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    if-eqz v5, :cond_150

    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    if-eqz v5, :cond_150

    .line 703
    iget-object v5, v11, Lcom/google/api/services/pos/model/Plusones;->metadata:Lcom/google/api/services/pos/model/Plusones$Metadata;

    iget-object v5, v5, Lcom/google/api/services/pos/model/Plusones$Metadata;->globalCounts:Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;

    iput-object v12, v5, Lcom/google/api/services/pos/model/Plusones$Metadata$GlobalCounts;->count:Ljava/lang/Double;

    .line 705
    :cond_150
    const/4 v15, 0x1

    .line 708
    :cond_151
    monitor-exit v6
    :try_end_152
    .catchall {:try_start_118 .. :try_end_152} :catchall_153

    goto :goto_eb

    .end local v11           #cached:Lcom/google/api/services/pos/model/Plusones;
    :catchall_153
    move-exception v5

    monitor-exit v6

    throw v5

    .line 717
    .restart local v11       #cached:Lcom/google/api/services/pos/model/Plusones;
    :cond_156
    const/4 v5, 0x0

    goto :goto_108
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 5
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 3
    .parameter "uri"

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_14

    .line 136
    const/4 v0, 0x0

    :goto_a
    return-object v0

    .line 127
    :pswitch_b
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.plusones"

    goto :goto_a

    .line 130
    :pswitch_e
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.account"

    goto :goto_a

    .line 133
    :pswitch_11
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.activitypreview"

    goto :goto_a

    .line 125
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_b
        :pswitch_e
        :pswitch_11
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 5
    .parameter "uri"
    .parameter "values"

    .prologue
    .line 145
    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()Z
    .registers 2

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 17
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_164

    .line 180
    :cond_9
    const/4 v0, 0x0

    :goto_a
    return-object v0

    .line 164
    :pswitch_b
    if-eqz p4, :cond_9

    array-length v0, p4

    if-lez v0, :cond_9

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v8

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v9

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    if-eq v1, v2, :cond_161

    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v9}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    move-object v6, v0

    :goto_3a
    const-string v0, "skipCache"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    sget-object v0, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-static {v2, v0, p4}, Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedUrls(ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_8e

    invoke-direct {p0, v8, v9, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->updatePlusoneEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    move-result-object v0

    move-object v7, v0

    :goto_55
    if-eqz v8, :cond_74

    const-string v0, "no_preview"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_74

    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/google/android/apps/plus/content/EsApiProvider$1;

    move-object v1, p0

    move-object v3, p4

    move-object v4, v8

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsApiProvider$1;-><init>(Lcom/google/android/apps/plus/content/EsApiProvider;Z[Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    :cond_74
    invoke-direct {p0, p4, v7}, Lcom/google/android/apps/plus/content/EsApiProvider;->buildPlusOneCursorFromCache([Ljava/lang/String;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v6, :cond_8b

    iget-object v0, v7, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_95

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v8, v6, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_8b
    move-object v0, v1

    goto/16 :goto_a

    :cond_8e
    new-instance v0, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>()V

    move-object v7, v0

    goto :goto_55

    :cond_95
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_READ_PLUSONES_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_84

    .line 171
    :pswitch_98
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v9

    new-instance v8, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v0, Lcom/google/android/apps/plus/external/PlatformContract$AccountContent;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v8, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz v9, :cond_106

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_e3

    array-length v0, v6

    if-lez v0, :cond_e3

    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v6, v3

    const/4 v5, 0x0

    aget-object v5, v6, v5

    invoke-static {v5, v4}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    move-result-object v5

    :cond_e3
    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_GET_ACCOUNT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v9, v0, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_106
    move-object v0, v8

    goto/16 :goto_a

    .line 174
    :pswitch_109
    if-eqz p4, :cond_9

    array-length v0, p4

    if-lez v0, :cond_9

    .line 175
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v0, v1, :cond_127

    const/4 v0, 0x1

    :goto_11f
    if-nez v0, :cond_129

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_127
    const/4 v0, 0x0

    goto :goto_11f

    :cond_129
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    const-string v1, "hostKey"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/content/EsApiProvider;->getApiaryApiInfo(Landroid/net/Uri;Ljava/lang/String;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v1

    const-string v2, "skipCache"

    invoke-virtual {p1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-static {v2, v3, p4}, Lcom/google/android/apps/plus/content/EsApiProvider;->getUncachedUrls(ZLandroid/support/v4/util/LruCache;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_15b

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/content/EsApiProvider;->updatePreviewEntries(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/util/List;)Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    move-result-object v0

    :goto_155
    invoke-static {p4, v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->buildPreviewCursorFromCache([Ljava/lang/String;Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;)Landroid/database/Cursor;

    move-result-object v0

    goto/16 :goto_a

    :cond_15b
    new-instance v0, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsApiProvider$UpdateResults;-><init>()V

    goto :goto_155

    :cond_161
    move-object v6, v0

    goto/16 :goto_3a

    .line 162
    :pswitch_data_164
    .packed-switch 0x1
        :pswitch_b
        :pswitch_98
        :pswitch_109
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 11
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 191
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v3, v4, :cond_1c

    move v0, v1

    .line 192
    .local v0, privileged:Z
    :goto_14
    if-nez v0, :cond_1e

    .line 193
    new-instance v1, Ljava/lang/SecurityException;

    invoke-direct {v1}, Ljava/lang/SecurityException;-><init>()V

    throw v1

    .end local v0           #privileged:Z
    :cond_1c
    move v0, v2

    .line 191
    goto :goto_14

    .line 196
    .restart local v0       #privileged:Z
    :cond_1e
    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_74

    :cond_27
    :goto_27
    move v1, v2

    .line 224
    :cond_28
    :goto_28
    return v1

    .line 199
    :pswitch_29
    if-eqz p4, :cond_27

    array-length v3, p4

    if-ne v3, v1, :cond_27

    .line 200
    aget-object v3, p4, v2

    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/apps/plus/content/EsApiProvider;->writePlusOne(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_28

    move v1, v2

    .line 203
    goto :goto_28

    .line 211
    :pswitch_38
    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    monitor-enter v3

    .line 212
    :try_start_3b
    sget-object v1, Lcom/google/android/apps/plus/content/EsApiProvider;->sPlusoneCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 213
    monitor-exit v3
    :try_end_41
    .catchall {:try_start_3b .. :try_end_41} :catchall_6d

    .line 214
    sget-object v3, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    monitor-enter v3

    .line 215
    :try_start_44
    sget-object v1, Lcom/google/android/apps/plus/content/EsApiProvider;->sPreviewCache:Landroid/support/v4/util/LruCache;

    invoke-virtual {v1}, Landroid/support/v4/util/LruCache;->evictAll()V

    .line 216
    monitor-exit v3
    :try_end_4a
    .catchall {:try_start_44 .. :try_end_4a} :catchall_70

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "content://com.google.android.apps.plus.content.ApiProvider/account"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsApiProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_27

    .line 213
    :catchall_6d
    move-exception v1

    monitor-exit v3

    throw v1

    .line 216
    :catchall_70
    move-exception v1

    monitor-exit v3

    throw v1

    .line 196
    nop

    :pswitch_data_74
    .packed-switch 0x1
        :pswitch_29
        :pswitch_38
    .end packed-switch
.end method
