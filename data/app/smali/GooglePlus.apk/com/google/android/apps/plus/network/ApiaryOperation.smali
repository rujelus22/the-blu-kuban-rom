.class public abstract Lcom/google/android/apps/plus/network/ApiaryOperation;
.super Lcom/google/android/apps/plus/network/HttpOperation;
.source "ApiaryOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Request:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Response:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">",
        "Lcom/google/android/apps/plus/network/HttpOperation;"
    }
.end annotation


# instance fields
.field protected final mRequestJson:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;"
        }
    .end annotation
.end field

.field protected final mResponseJson:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Ljava/lang/String;)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "url"
    .parameter
    .parameter
    .parameter "intent"
    .parameter "listener"
    .parameter "requestConfiguration"
    .parameter "method"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/HttpRequestConfiguration;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    .local p4, requestJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p5, responseJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TResponse;>;"
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p9

    move-object v3, p3

    move-object/from16 v4, p8

    move-object v5, p2

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 60
    iput-object p4, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    .line 61
    iput-object p5, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter "url"
    .parameter "backendOverrideUrl"
    .parameter "scope"
    .parameter
    .parameter
    .parameter "intent"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    .local p6, requestParser:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p7, responseParser:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TResponse;>;"
    new-instance v12, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;

    move-object/from16 v0, p5

    move-object/from16 v1, p4

    invoke-direct {v12, p1, p2, v0, v1}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/plus/network/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    .line 105
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V
    .registers 21
    .parameter "context"
    .parameter "account"
    .parameter "url"
    .parameter "backendOverrideUrl"
    .parameter "scope"
    .parameter
    .parameter
    .parameter "intent"
    .parameter "listener"
    .parameter "requestConfiguration"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/HttpRequestConfiguration;",
            ")V"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    .local p6, requestParser:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p7, responseParser:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TResponse;>;"
    const-string v9, "POST"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/network/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Ljava/lang/String;)V

    .line 84
    return-void
.end method


# virtual methods
.method protected createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequest;)",
            "Lcom/google/android/apps/plus/network/JsonHttpEntity",
            "<TRequest;>;"
        }
    .end annotation

    .prologue
    .line 156
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    .local p1, request:Lcom/google/android/apps/plus/json/GenericJson;,"TRequest;"
    new-instance v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;-><init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;)V

    return-object v0
.end method

.method protected createPostData()Lorg/apache/http/HttpEntity;
    .registers 6

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    const/4 v4, 0x3

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    if-nez v1, :cond_b

    .line 120
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/network/ApiaryOperation;->createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;

    move-result-object v1

    .line 132
    :goto_a
    return-object v1

    .line 122
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/EsJson;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/GenericJson;

    .line 123
    .local v0, request:Lcom/google/android/apps/plus/json/GenericJson;,"TRequest;"
    new-instance v1, Lcom/google/api/services/plusi/model/ApiaryFields;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ApiaryFields;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/network/ClientVersion;->from(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ApiaryFields;->appVersion:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v2

    if-eqz v2, :cond_34

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/ApiaryFields;->effectiveUser:Ljava/lang/String;

    :cond_34
    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    const-string v3, "commonFields"

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/apps/plus/json/EsJson;->setField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z

    .line 124
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/ApiaryOperation;->populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V

    .line 126
    const-string v1, "HttpTransaction"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_72

    .line 127
    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Apiary request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_72
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/ApiaryOperation;->createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;

    move-result-object v1

    goto :goto_a
.end method

.method protected abstract handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResponse;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected final isAuthenticationError(Ljava/lang/Exception;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 239
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    instance-of v0, p1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v0, :cond_e

    move-object v0, p1

    .line 240
    check-cast v0, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v0

    packed-switch v0, :pswitch_data_16

    .line 245
    :cond_e
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v0

    :goto_12
    return v0

    .line 242
    :pswitch_13
    const/4 v0, 0x1

    goto :goto_12

    .line 240
    nop

    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_13
    .end packed-switch
.end method

.method protected final isImmediatelyRetryableError(Ljava/lang/Exception;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 227
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    instance-of v0, p1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v0, :cond_e

    move-object v0, p1

    .line 228
    check-cast v0, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_16

    .line 234
    :cond_e
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isImmediatelyRetryableError(Ljava/lang/Exception;)Z

    move-result v0

    :goto_12
    return v0

    .line 231
    :sswitch_13
    const/4 v0, 0x1

    goto :goto_12

    .line 228
    nop

    :sswitch_data_16
    .sparse-switch
        0x1 -> :sswitch_13
        0x6 -> :sswitch_13
    .end sparse-switch
.end method

.method protected final onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .registers 7
    .parameter "inputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    const/4 v4, 0x2

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryOperation;->onStartResultProcessing()V

    .line 165
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v1, :cond_55

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/json/GenericJson;

    move-object v0, v1

    .line 167
    .local v0, response:Lcom/google/android/apps/plus/json/GenericJson;,"TResponse;"
    :goto_11
    const-string v1, "HttpTransaction"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_51

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v1, :cond_57

    .line 169
    const-string v2, "HttpTransaction"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "Apiary response: "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/EsJson;->newInstance()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mResponseJson:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v2, v1}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_51
    :goto_51
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/ApiaryOperation;->handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V

    .line 178
    return-void

    .line 165
    .end local v0           #response:Lcom/google/android/apps/plus/json/GenericJson;,"TResponse;"
    :cond_55
    const/4 v0, 0x0

    goto :goto_11

    .line 173
    .restart local v0       #response:Lcom/google/android/apps/plus/json/GenericJson;,"TResponse;"
    :cond_57
    const-string v1, "HttpTransaction"

    const-string v2, "Apiary response ignored"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_51
.end method

.method public onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    .registers 12
    .parameter "inputStream"
    .parameter "contentType"
    .parameter "contentLength"
    .parameter "header"
    .parameter "statusCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    .local p0, this:Lcom/google/android/apps/plus/network/ApiaryOperation;,"Lcom/google/android/apps/plus/network/ApiaryOperation<TRequest;TResponse;>;"
    const-string v3, "HttpTransaction"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v3, "Apiary error response: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryOperation;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 187
    invoke-static {p1, p3, v0}, Lcom/google/android/apps/plus/network/ApiaryOperation;->captureResponse(Ljava/io/InputStream;ILjava/lang/StringBuilder;)Ljava/io/InputStream;

    move-result-object p1

    .line 188
    const-string v3, "HttpTransaction"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    .end local v0           #builder:Ljava/lang/StringBuilder;
    :cond_2e
    const/16 v3, 0x191

    if-ne p5, v3, :cond_33

    .line 212
    :cond_32
    return-void

    .line 196
    :cond_33
    sget-object v3, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v3, p1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;

    .line 197
    .local v1, error:Lcom/google/android/apps/plus/api/ApiaryErrorResponse;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorType()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_32

    .line 198
    const-string v3, "HttpTransaction"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_66

    .line 199
    const-string v3, "HttpTransaction"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Apiary error reason: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_66
    new-instance v2, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-direct {v2, v1}, Lcom/google/android/apps/plus/api/OzServerException;-><init>(Lcom/google/android/apps/plus/api/ApiaryErrorResponse;)V

    .line 203
    .local v2, serverException:Lcom/google/android/apps/plus/api/OzServerException;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    packed-switch v3, :pswitch_data_7a

    .line 210
    :goto_72
    throw v2

    .line 205
    :pswitch_73
    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryOperation;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/AndroidNotification;->showUpgradeRequiredNotification(Landroid/content/Context;)V

    goto :goto_72

    .line 203
    nop

    :pswitch_data_7a
    .packed-switch 0xa
        :pswitch_73
    .end packed-switch
.end method

.method protected abstract populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRequest;)V"
        }
    .end annotation
.end method
