.class public Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "HangoutParticipantListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mParticipantList:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private toastsView:Lcom/google/android/apps/plus/hangout/ToastsView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private canInviteMoreParticipants()Z
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v0, :cond_a

    sget-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private inviteMoreParticipants()V
    .registers 14

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08020e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, title:Ljava/lang/String;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v12, users:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 204
    .local v11, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-static {v11}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->makePersonFromParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_18

    .line 206
    .end local v11           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_2c
    new-instance v3, Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v0, 0x0

    invoke-direct {v3, v12, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 207
    .local v3, currentAudience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x5

    move-object v0, p0

    move v6, v5

    move v8, v7

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v10

    .line 210
    .local v10, intent:Landroid/content/Intent;
    invoke-virtual {p0, v10, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 211
    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 241
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 215
    if-nez p1, :cond_27

    const/4 v1, -0x1

    if-ne p2, v1, :cond_27

    if-eqz p3, :cond_27

    .line 216
    const-string v1, "audience"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    .line 218
    .local v0, inviteAudience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    const-string v2, "HANGOUT"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->toastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    const v2, 0x7f080327

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    .line 226
    .end local v0           #inviteAudience:Lcom/google/android/apps/plus/content/AudienceData;
    :goto_26
    return-void

    .line 224
    :cond_27
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_26
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09023a

    if-ne v0, v1, :cond_c

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->inviteMoreParticipants()V

    .line 159
    :cond_c
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x1

    .line 47
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    const v1, 0x7f030074

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->setContentView(I)V

    .line 50
    const v1, 0x7f0900e1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/ToastsView;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->toastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 53
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    .line 54
    .local v0, nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHangoutInfo()Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 57
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->showTitlebar(Z)V

    .line 61
    const v1, 0x7f10000f

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->createTitlebarButtons(I)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "hangout_participants"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080326

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->showTitlebar(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08020c

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mParticipantList:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->showTitlebar(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    .line 96
    .line 97
    const/4 v0, 0x0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 138
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_14

    .line 148
    :goto_7
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 140
    :sswitch_9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->inviteMoreParticipants()V

    goto :goto_7

    .line 144
    :sswitch_d
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 145
    const/4 v0, 0x1

    goto :goto_8

    .line 138
    :sswitch_data_14
    .sparse-switch
        0x102002c -> :sswitch_d
        0x7f0902a9 -> :sswitch_9
    .end sparse-switch
.end method

.method protected onPause()V
    .registers 1

    .prologue
    .line 88
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 89
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter "menu"

    .prologue
    .line 109
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 4
    .parameter "menu"

    .prologue
    .line 123
    .line 124
    const v1, 0x7f0902a9

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 125
    .local v0, inviteItem:Landroid/view/MenuItem;
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->canInviteMoreParticipants()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 127
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->finish()V

    .line 81
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantListActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 167
    return-void
.end method
