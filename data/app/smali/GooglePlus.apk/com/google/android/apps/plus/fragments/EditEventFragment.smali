.class public Lcom/google/android/apps/plus/fragments/EditEventFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "EditEventFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;
.implements Lcom/google/android/apps/plus/views/EsImageView$OnImageLoadedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;,
        Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;,
        Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;",
        "Lcom/google/android/apps/plus/views/EsImageView$OnImageLoadedListener;"
    }
.end annotation


# static fields
.field private static final EVENT_COLUMNS:[Ljava/lang/String;

.field private static final THEME_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

.field private mChanged:Z

.field private mDescriptionView:Landroid/widget/EditText;

.field private mEndDateView:Landroid/widget/Button;

.field private mEndTimeView:Landroid/widget/Button;

.field private mError:Z

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mEventDescriptionTextWatcher:Landroid/text/TextWatcher;

.field private mEventId:Ljava/lang/String;

.field private mEventLoaded:Z

.field private mEventNameTextWatcher:Landroid/text/TextWatcher;

.field private mEventNameView:Landroid/widget/EditText;

.field private mEventThemeId:I

.field private mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

.field private mExternalId:Ljava/lang/String;

.field private mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

.field private mLocationView:Landroid/widget/TextView;

.field private mNewEvent:Z

.field private mOwnerId:Ljava/lang/String;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mStartDateView:Landroid/widget/Button;

.field private mStartTimeView:Landroid/widget/Button;

.field private mThemeProgressBar:Landroid/widget/ProgressBar;

.field private mThemeSelectionButton:Landroid/view/View;

.field private mThemeSelectionTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 105
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "theme_id"

    aput-object v1, v0, v2

    const-string v1, "image_url"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->THEME_COLUMNS:[Ljava/lang/String;

    .line 113
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "event_data"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->EVENT_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    .line 1125
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 1138
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$5;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameTextWatcher:Landroid/text/TextWatcher;

    .line 1161
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventDescriptionTextWatcher:Landroid/text/TextWatcher;

    .line 1273
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onAudienceChanged()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditEventFragment;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    return v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .registers 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->THEME_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .registers 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->EVENT_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;
    .registers 2
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/EditEventFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/EditEventFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter "x0"

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    return-object v0
.end method

.method private bindEndDate()V
    .registers 6

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_21

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const v4, 0x8016

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 426
    :goto_20
    return-void

    .line 424
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_20
.end method

.method private bindEndTime()V
    .registers 6

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_25

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_25

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 447
    :goto_24
    return-void

    .line 445
    :cond_25
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_24
.end method

.method private bindEvent()V
    .registers 3

    .prologue
    .line 387
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_5

    .line 399
    :goto_4
    return-void

    .line 391
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v0, :cond_31

    const/4 v0, 0x0

    :goto_c
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 396
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartDate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 397
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartTime()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 398
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindLocation()V

    goto :goto_4

    .line 391
    :cond_31
    const/16 v0, 0x8

    goto :goto_c
.end method

.method private bindLocation()V
    .registers 4

    .prologue
    .line 450
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    .line 451
    .local v0, location:Lcom/google/api/services/plusi/model/Place;
    if-eqz v0, :cond_e

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 456
    :goto_d
    return-void

    .line 454
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_d
.end method

.method private bindStartDate()V
    .registers 6

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartDateView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const v4, 0x8016

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 413
    return-void
.end method

.method private bindStartTime()V
    .registers 6

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_24

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_24

    .line 435
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartTimeView:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 438
    :cond_24
    return-void
.end method

.method private clearEndTime()V
    .registers 3

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    .line 755
    return-void
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 1375
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private static getDefaultEventTime()J
    .registers 4

    .prologue
    const/16 v3, 0xc

    const/4 v2, 0x0

    .line 196
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 197
    .local v0, cal:Ljava/util/Calendar;
    const/16 v1, 0x5a

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->add(II)V

    .line 198
    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 199
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 200
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 202
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    return-wide v1
.end method

.method private isEmptyAudience()Z
    .registers 4

    .prologue
    .line 308
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 309
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v2

    add-int/2addr v1, v2

    if-nez v1, :cond_13

    const/4 v1, 0x1

    :goto_12
    return v1

    :cond_13
    const/4 v1, 0x0

    goto :goto_12
.end method

.method private onAudienceChanged()V
    .registers 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v0, :cond_6

    .line 460
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    .line 462
    :cond_6
    return-void
.end method

.method private recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 5
    .parameter "action"

    .prologue
    .line 1366
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1367
    .local v1, context:Landroid/content/Context;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 1368
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_11

    .line 1369
    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    .line 1370
    .local v2, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    invoke-static {v1, v0, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1372
    .end local v2           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_11
    return-void
.end method

.method private setEndTime(J)V
    .registers 6
    .parameter "timeInMillis"

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-nez v0, :cond_2b

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v1, Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/EventTime;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getDefaultEventTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    .line 745
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    .line 747
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-eqz v0, :cond_46

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    .line 749
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    .line 751
    :cond_46
    return-void
.end method

.method private setEventTheme(ILjava/lang/String;Z)V
    .registers 6
    .parameter "themeId"
    .parameter "imageUrl"
    .parameter "override"

    .prologue
    .line 958
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_5

    .line 976
    :cond_4
    :goto_4
    return-void

    .line 962
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    if-nez v0, :cond_14

    .line 963
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v1, Lcom/google/api/services/plusi/model/ThemeSpecification;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ThemeSpecification;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    .line 966
    :cond_14
    if-nez p3, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ThemeSpecification;->themeId:Ljava/lang/Integer;

    if-nez v0, :cond_30

    .line 967
    :cond_1e
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    .line 968
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ThemeSpecification;->themeId:Ljava/lang/Integer;

    .line 969
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    goto :goto_4

    .line 970
    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ThemeSpecification;->themeId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_4

    .line 971
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    .line 974
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    goto :goto_4
.end method

.method private setStartTime(J)V
    .registers 5
    .parameter "timeInMillis"

    .prologue
    .line 735
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p1

    if-eqz v0, :cond_1b

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    .line 737
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    .line 739
    :cond_1b
    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1003
    if-eqz p1, :cond_9

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v2, :cond_a

    .line 1028
    :cond_9
    :goto_9
    return-void

    .line 1007
    :cond_a
    const v2, 0x7f090072

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1008
    .local v1, serverErrorView:Landroid/widget/TextView;
    const v2, 0x7f09008a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1009
    .local v0, contentView:Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_28

    .line 1010
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1011
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1012
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_9

    .line 1013
    :cond_28
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventLoaded:Z

    if-nez v2, :cond_36

    .line 1014
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1015
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1016
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_9

    .line 1017
    :cond_36
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mError:Z

    if-eqz v2, :cond_4a

    .line 1018
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1019
    const v2, 0x7f080358

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1020
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1021
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_9

    .line 1023
    :cond_4a
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1024
    const v2, 0x7f080359

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1025
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1026
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_9
.end method


# virtual methods
.method public final createEvent()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_74

    .line 175
    new-instance v0, Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PlusEvent;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v1, Lcom/google/api/services/plusi/model/EventOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/EventOptions;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventOptions;->openEventAcl:Ljava/lang/Boolean;

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventOptions;->openPhotoAcl:Ljava/lang/Boolean;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v1, Lcom/google/api/services/plusi/model/EventTime;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/EventTime;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getDefaultEventTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    .line 187
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    .line 189
    :cond_74
    return-void
.end method

.method public final editEvent(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "eventId"
    .parameter "ownerId"

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    .line 210
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mOwnerId:Ljava/lang/String;

    .line 211
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    .line 213
    return-void
.end method

.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    .line 1101
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_e

    .line 1123
    :cond_d
    :goto_d
    return-void

    .line 1105
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1107
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1f

    .line 1108
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1111
    :cond_1f
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1113
    if-eqz p2, :cond_41

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 1114
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v1, :cond_3d

    const v1, 0x7f0801a9

    :goto_35
    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    :cond_3d
    const v1, 0x7f0801a8

    goto :goto_35

    .line 1117
    :cond_41
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v1, :cond_d

    .line 1118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v1, :cond_5d

    const v1, 0x7f080113

    :goto_50
    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1121
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventSaved()V

    goto :goto_d

    .line 1118
    :cond_5d
    const v1, 0x7f080114

    goto :goto_50
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 762
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 763
    if-ne p2, v4, :cond_9

    if-nez p3, :cond_a

    .line 794
    :cond_9
    :goto_9
    return-void

    .line 767
    :cond_a
    packed-switch p1, :pswitch_data_56

    goto :goto_9

    .line 769
    :pswitch_e
    const-string v3, "location"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 770
    .local v0, bytes:[B
    if-nez v0, :cond_1e

    .line 771
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v5, v3, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    .line 775
    :goto_1a
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindLocation()V

    goto :goto_9

    .line 773
    :cond_1e
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/PlaceJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Place;

    iput-object v3, v4, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    goto :goto_1a

    .line 780
    .end local v0           #bytes:[B
    :pswitch_2d
    const-string v3, "theme_id"

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 781
    .local v2, themeId:I
    const-string v3, "theme_url"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 782
    .local v1, imageUrl:Ljava/lang/String;
    if-eq v2, v4, :cond_9

    if-eqz v1, :cond_9

    .line 783
    const/4 v3, 0x1

    invoke-direct {p0, v2, v1, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEventTheme(ILjava/lang/String;Z)V

    .line 784
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_9

    .line 790
    .end local v1           #imageUrl:Ljava/lang/String;
    .end local v2           #themeId:I
    :pswitch_4a
    const-string v3, "audience"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    goto :goto_9

    .line 767
    nop

    :pswitch_data_56
    .packed-switch 0x0
        :pswitch_e
        :pswitch_2d
        :pswitch_4a
    .end packed-switch
.end method

.method public final onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .parameter "personId"
    .parameter "name"
    .parameter "forSharing"

    .prologue
    .line 499
    return-void
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 508
    return-void
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 4
    .parameter "circleId"
    .parameter "circle"

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    .line 486
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 14
    .parameter "v"

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 523
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_15c

    .line 602
    :goto_9
    return-void

    .line 525
    :sswitch_a
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 526
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const v2, 0x7f080388

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    const/16 v4, 0xb

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_9

    .line 538
    :sswitch_31
    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;

    invoke-direct {v10, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;-><init>(I)V

    .line 539
    .local v10, dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;
    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 541
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 542
    .local v9, args:Landroid/os/Bundle;
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 543
    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    .line 544
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_9

    .line 549
    .end local v9           #args:Landroid/os/Bundle;
    .end local v10           #dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;
    :sswitch_5a
    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;

    invoke-direct {v10, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;-><init>(I)V

    .line 550
    .restart local v10       #dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;
    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 552
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 553
    .restart local v9       #args:Landroid/os/Bundle;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_89

    .line 554
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 558
    :goto_7c
    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    .line 559
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "date"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_9

    .line 556
    :cond_89
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_7c

    .line 564
    .end local v9           #args:Landroid/os/Bundle;
    .end local v10           #dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$DatePickerFragmentDialog;
    :sswitch_99
    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;

    invoke-direct {v10, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;-><init>(I)V

    .line 565
    .local v10, dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;
    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 567
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 568
    .restart local v9       #args:Landroid/os/Bundle;
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 569
    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    .line 570
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 575
    .end local v9           #args:Landroid/os/Bundle;
    .end local v10           #dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;
    :sswitch_c3
    new-instance v10, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;

    invoke-direct {v10, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;-><init>(I)V

    .line 576
    .restart local v10       #dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;
    invoke-virtual {v10, p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 578
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 580
    .restart local v9       #args:Landroid/os/Bundle;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_f3

    .line 581
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 586
    :goto_e5
    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    .line 587
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "time"

    invoke-virtual {v10, v0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_9

    .line 583
    :cond_f3
    const-string v0, "date_time"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/32 v3, 0x6ddd00

    add-long/2addr v1, v3

    invoke-virtual {v9, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_e5

    .line 592
    .end local v9           #args:Landroid/os/Bundle;
    .end local v10           #dialog:Lcom/google/android/apps/plus/fragments/EditEventFragment$TimePickerFragmentDialog;
    :sswitch_107
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHANGE_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 593
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    new-instance v11, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/EventLocationActivity;

    invoke-direct {v11, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.PICK"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account"

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz v2, :cond_138

    const-string v0, "location"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/PlaceJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 595
    .local v11, intent:Landroid/content/Intent;
    :cond_138
    invoke-virtual {p0, v11, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_9

    .line 600
    .end local v11           #intent:Landroid/content/Intent;
    :sswitch_13d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    new-instance v11, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;

    invoke-direct {v11, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account"

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 601
    .restart local v11       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v11, v7}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_9

    .line 523
    nop

    :sswitch_data_15c
    .sparse-switch
        0x7f090057 -> :sswitch_a
        0x7f09008d -> :sswitch_13d
        0x7f090090 -> :sswitch_31
        0x7f090091 -> :sswitch_99
        0x7f090092 -> :sswitch_5a
        0x7f090093 -> :sswitch_c3
        0x7f090095 -> :sswitch_107
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 227
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 228
    if-eqz p1, :cond_5c

    .line 229
    const-string v1, "new_event"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    .line 230
    const-string v1, "event_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    .line 231
    const-string v1, "owner_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mOwnerId:Ljava/lang/String;

    .line 232
    const-string v1, "event"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 233
    const-string v1, "event"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 234
    .local v0, bytes:[B
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 236
    .end local v0           #bytes:[B
    :cond_38
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 237
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 239
    :cond_4c
    const-string v1, "external_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    .line 240
    const-string v1, "changed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    .line 243
    :cond_5c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 245
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-nez v1, :cond_74

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v1, :cond_74

    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 248
    :cond_74
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 9
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 881
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 882
    .local v4, context:Landroid/content/Context;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    .line 883
    .local v5, account:Lcom/google/android/apps/plus/content/EsAccount;
    packed-switch p1, :pswitch_data_20

    .line 911
    const/4 v0, 0x0

    :goto_c
    return-object v0

    .line 885
    :pswitch_d
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$2;

    invoke-direct {v0, p0, v4, v4, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;Landroid/content/Context;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_c

    .line 899
    :pswitch_13
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditEventFragment$3;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_c

    .line 883
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_d
        :pswitch_13
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 318
    const v2, 0x7f030024

    invoke-virtual {p1, v2, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 319
    .local v1, view:Landroid/view/View;
    const v2, 0x7f09008b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EventThemeView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 320
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnImageLoadedListener(Lcom/google/android/apps/plus/views/EsImageView$OnImageLoadedListener;)V

    .line 321
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventThemeView;->setClickable(Z)V

    .line 322
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    const v2, 0x7f09008c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;

    const v3, 0x7f080383

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    const v2, 0x7f09008e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeProgressBar:Landroid/widget/ProgressBar;

    .line 329
    const v2, 0x7f09008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    .line 330
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventNameTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 332
    const v2, 0x7f090090

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartDateView:Landroid/widget/Button;

    .line 333
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartDateView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    const v2, 0x7f090092

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    .line 336
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndDateView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    const v2, 0x7f090091

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartTimeView:Landroid/widget/Button;

    .line 339
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mStartTimeView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    const v2, 0x7f090093

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    .line 342
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEndTimeView:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    const v2, 0x7f090095

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    .line 345
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mLocationView:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 347
    const v2, 0x7f090052

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 349
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    const v3, 0x7f08037d

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setEmptyAudienceHint(I)V

    .line 350
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/EditEventFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditEventFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    .line 360
    const v2, 0x7f09008d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    const v2, 0x7f090096

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    .line 364
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mDescriptionView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventDescriptionTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 366
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0f003d

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 369
    .local v0, themeContext:Landroid/content/Context;
    new-instance v2, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    .line 371
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    .line 372
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setShowPersonNameDialog(Z)V

    .line 373
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    .line 374
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    .line 375
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAutoCompleteAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;)V

    .line 376
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 377
    const v2, 0x7f090057

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEvent()V

    .line 381
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->updateView(Landroid/view/View;)V

    .line 383
    return-object v1
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 867
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 874
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 860
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 848
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 849
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v0, :cond_11

    .line 850
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventClosed()V

    .line 853
    :cond_11
    return-void
.end method

.method public final onDiscard()V
    .registers 7

    .prologue
    const v5, 0x7f0801c8

    const v4, 0x7f0801c6

    const/4 v2, 0x0

    .line 816
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v1, :cond_5b

    .line 817
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_25

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->isEmptyAudience()Z

    move-result v1

    if-nez v1, :cond_4f

    :cond_25
    const/4 v1, 0x1

    :goto_26
    if-eqz v1, :cond_51

    .line 818
    const v1, 0x7f080399

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f08039a

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 823
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 824
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 841
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_4e
    :goto_4e
    return-void

    :cond_4f
    move v1, v2

    .line 817
    goto :goto_26

    .line 825
    :cond_51
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v1, :cond_4e

    .line 826
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventClosed()V

    goto :goto_4e

    .line 829
    :cond_5b
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    if-eqz v1, :cond_86

    .line 830
    const v1, 0x7f08039b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f08039c

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 835
    .restart local v0       #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 836
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_4e

    .line 837
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_86
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    if-eqz v1, :cond_4e

    .line 838
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;->onEventClosed()V

    goto :goto_4e
.end method

.method public final onDismissSuggestionAction(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 516
    return-void
.end method

.method public final onEndDateCleared()V
    .registers 1

    .prologue
    .line 666
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->clearEndTime()V

    .line 667
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 668
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 669
    return-void
.end method

.method public final onEndDateSet(III)V
    .registers 11
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    const-wide/32 v5, 0x6ddd00

    .line 638
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 639
    .local v0, cal:Ljava/util/Calendar;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_62

    .line 640
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 645
    :goto_1a
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_35

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p1, :cond_35

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p2, :cond_35

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, p3, :cond_61

    .line 648
    :cond_35
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 650
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 652
    .local v1, newEndTimeInMillis:J
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v1

    if-lez v3, :cond_56

    .line 653
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long v1, v3, v5

    .line 655
    :cond_56
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(J)V

    .line 656
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 657
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 658
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    .line 660
    .end local v1           #newEndTimeInMillis:J
    :cond_61
    return-void

    .line 642
    :cond_62
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_1a
.end method

.method public final onEndTimeCleared()V
    .registers 1

    .prologue
    .line 729
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->clearEndTime()V

    .line 730
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 731
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 732
    return-void
.end method

.method public final onEndTimeSet(II)V
    .registers 12
    .parameter "hourOfDay"
    .parameter "minute"

    .prologue
    const-wide/32 v7, 0x6ddd00

    const/16 v6, 0xc

    const/16 v5, 0xb

    .line 701
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 702
    .local v0, cal:Ljava/util/Calendar;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_60

    .line 703
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 708
    :goto_1e
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_30

    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p1, :cond_30

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, p2, :cond_5f

    .line 710
    :cond_30
    invoke-virtual {v0, v5, p1}, Ljava/util/Calendar;->set(II)V

    .line 711
    invoke-virtual {v0, v6, p2}, Ljava/util/Calendar;->set(II)V

    .line 713
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 715
    .local v1, newEndTimeInMillis:J
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v1

    if-lez v3, :cond_54

    .line 716
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long v1, v3, v7

    .line 718
    :cond_54
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(J)V

    .line 719
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 720
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 721
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    .line 723
    .end local v1           #newEndTimeInMillis:J
    :cond_5f
    return-void

    .line 705
    :cond_60
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v3, v7

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    goto :goto_1e
.end method

.method public final onImageLoaded$7ad36aad()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 997
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeSelectionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 999
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mThemeProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1000
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_6a

    :cond_b
    :goto_b
    return-void

    :pswitch_c
    if-eqz p2, :cond_b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEventTheme(ILjava/lang/String;Z)V

    goto :goto_b

    :pswitch_20
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventLoaded:Z

    if-nez p2, :cond_2e

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mError:Z

    :cond_26
    :goto_26
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->updateView(Landroid/view/View;)V

    goto :goto_b

    :cond_2e
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mError:Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v0

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    if-eqz v1, :cond_57

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Theme;->themeId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_57
    iget v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    if-eq v0, v1, :cond_65

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventThemeId:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_65
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEvent()V

    goto :goto_26

    nop

    :pswitch_data_6a
    .packed-switch 0x0
        :pswitch_c
        :pswitch_20
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 983
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 1080
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    .line 1081
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 1082
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 5
    .parameter "personId"
    .parameter "contactLookupKey"
    .parameter "person"

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    .line 495
    return-void
.end method

.method public final onResume()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1057
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 1058
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 1060
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2e

    .line 1061
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 1062
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 1063
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 1064
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1068
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_3e

    .line 1069
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 1070
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 1071
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onAudienceChanged()V

    .line 1073
    :cond_3e
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 255
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 259
    const-string v0, "new_event"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    const-string v0, "event_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v0, "owner_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_30

    .line 263
    const-string v0, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-virtual {v1, v2}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 265
    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_3f

    .line 266
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 268
    :cond_3f
    const-string v0, "external_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v0, "changed"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 270
    return-void
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 2
    .parameter "adapter"

    .prologue
    .line 477
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 1035
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStart()V

    .line 1036
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 1037
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    .line 1039
    :cond_c
    return-void
.end method

.method public final onStartDateSet(III)V
    .registers 9
    .parameter "year"
    .parameter "monthOfYear"
    .parameter "dayOfMonth"

    .prologue
    .line 611
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 612
    .local v0, cal:Ljava/util/Calendar;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 614
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p1, :cond_26

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p2, :cond_26

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, p3, :cond_56

    .line 617
    :cond_26
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 619
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 620
    .local v1, newStartTimeInMillis:J
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setStartTime(J)V

    .line 621
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartDate()V

    .line 623
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_54

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v1

    if-gez v3, :cond_54

    .line 626
    const-wide/32 v3, 0x6ddd00

    add-long/2addr v3, v1

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(J)V

    .line 627
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 628
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 630
    :cond_54
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    .line 632
    .end local v1           #newStartTimeInMillis:J
    :cond_56
    return-void
.end method

.method public final onStartTimeSet(II)V
    .registers 10
    .parameter "hourOfDay"
    .parameter "minute"

    .prologue
    const/16 v6, 0xc

    const/16 v5, 0xb

    .line 675
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 676
    .local v0, cal:Ljava/util/Calendar;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 678
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v3, p1, :cond_21

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-eq v3, p2, :cond_54

    .line 679
    :cond_21
    invoke-virtual {v0, v5, p1}, Ljava/util/Calendar;->set(II)V

    .line 680
    invoke-virtual {v0, v6, p2}, Ljava/util/Calendar;->set(II)V

    .line 682
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 683
    .local v1, newStartTimeInMillis:J
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setStartTime(J)V

    .line 684
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindStartTime()V

    .line 686
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v3, :cond_52

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v1

    if-gez v3, :cond_52

    .line 689
    const-wide/32 v3, 0x6ddd00

    add-long/2addr v3, v1

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setEndTime(J)V

    .line 690
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndDate()V

    .line 691
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->bindEndTime()V

    .line 693
    :cond_52
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    .line 695
    .end local v1           #newStartTimeInMillis:J
    :cond_54
    return-void
.end method

.method public final onStop()V
    .registers 2

    .prologue
    .line 1046
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStop()V

    .line 1047
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    .line 1050
    :cond_c
    return-void
.end method

.method public final onUnblockPersonAction(Ljava/lang/String;Z)V
    .registers 3
    .parameter "personId"
    .parameter "isPlusPage"

    .prologue
    .line 512
    return-void
.end method

.method public final save()V
    .registers 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 800
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v2, :cond_3f

    move v0, v1

    :cond_7
    :goto_7
    if-eqz v0, :cond_3e

    .line 801
    const v0, 0x7f08037c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 803
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v0, :cond_85

    .line 804
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mExternalId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->createEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 810
    :cond_3e
    :goto_3e
    return-void

    .line 800
    :cond_3f
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_61

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080143

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    goto :goto_7

    :cond_61
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mNewEvent:Z

    if-eqz v2, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->isEmptyAudience()Z

    move-result v2

    if-eqz v2, :cond_81

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080142

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_81
    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_7

    .line 807
    :cond_85
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->updateEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_3e
.end method

.method public final setOnEventChangedListener(Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 219
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditEventFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;

    .line 220
    return-void
.end method
