.class public final Lcom/google/android/apps/plus/api/PostClientLogsOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PostClientLogsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostClientLogsRequest;",
        "Lcom/google/api/services/plusi/model/PostClientLogsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 41
    const-string v3, "postclientlogs"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostClientLogsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostClientLogsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PostClientLogsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostClientLogsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 47
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOzExtension(Landroid/content/Context;)Lcom/google/api/services/plusi/model/ClientOzExtension;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    .line 48
    return-void
.end method


# virtual methods
.method public final getClientOzEvents()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientEvent:Ljava/util/List;

    goto :goto_5
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->onStartResultProcessing()V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/PostClientLogsRequest;

    .end local p1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostClientLogsRequest;->enableTracing:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostClientLogsRequest;->clientLog:Lcom/google/api/services/plusi/model/ClientOzExtension;

    return-void
.end method

.method public final setClientOzEvents(Ljava/util/List;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, clientOzEvents:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->mClientOzExtension:Lcom/google/api/services/plusi/model/ClientOzExtension;

    iput-object p1, v0, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientEvent:Ljava/util/List;

    .line 52
    return-void
.end method
