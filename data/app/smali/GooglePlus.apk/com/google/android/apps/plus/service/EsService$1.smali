.class final Lcom/google/android/apps/plus/service/EsService$1;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/PhotoCache$CacheListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/plus/service/PhotoCache$CacheListener",
        "<",
        "Lcom/google/android/apps/plus/service/EsService$ImageKey;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 541
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic onImageDownload(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Object;III)V
    .registers 13
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    .prologue
    .line 541
    move-object v1, p3

    check-cast v1, Lcom/google/android/apps/plus/service/EsService$ImageKey;

    invoke-interface {v1}, Lcom/google/android/apps/plus/service/EsService$ImageKey;->getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_16

    move-object v0, p1

    move-object v1, p2

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->access$100(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;III)V

    :goto_15
    return-void

    :cond_16
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->access$200()Lcom/google/android/apps/plus/service/PhotoCache;

    move-result-object v0

    const/4 v2, 0x0

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/PhotoCache;->downloadComplete(Ljava/lang/Object;Landroid/graphics/Bitmap;III)V

    goto :goto_15
.end method

.method public final bridge synthetic onImageLoaded(Ljava/lang/Object;Landroid/graphics/Bitmap;I)V
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 541
    check-cast p1, Lcom/google/android/apps/plus/service/EsService$ImageKey;

    .end local p1
    invoke-interface {p1}, Lcom/google/android/apps/plus/service/EsService$ImageKey;->getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    instance-of v0, p1, Lcom/google/android/apps/plus/service/EsService$LocalImageKey;

    if-eqz v0, :cond_2c

    check-cast p1, Lcom/google/android/apps/plus/service/EsService$LocalImageKey;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->access$300()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/EsServiceListener;

    #getter for: Lcom/google/android/apps/plus/service/EsService$LocalImageKey;->mWidth:I
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService$LocalImageKey;->access$400(Lcom/google/android/apps/plus/service/EsService$LocalImageKey;)I

    move-result v3

    #getter for: Lcom/google/android/apps/plus/service/EsService$LocalImageKey;->mHeight:I
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService$LocalImageKey;->access$500(Lcom/google/android/apps/plus/service/EsService$LocalImageKey;)I

    move-result v4

    invoke-virtual {v0, v1, p2, v3, v4}, Lcom/google/android/apps/plus/service/EsServiceListener;->onLocalImageLoaded(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;II)Z

    goto :goto_14

    :cond_2c
    instance-of v0, p1, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;

    if-eqz v0, :cond_48

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->access$300()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_38
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_48

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPhotoImageLoaded$b81653(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;I)V

    goto :goto_38

    :cond_48
    return-void
.end method
