.class public final Lcom/google/android/apps/plus/api/MarkItemReadOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "MarkItemReadOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MarkItemReadRequest;",
        "Lcom/google/api/services/plusi/model/MarkItemReadResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mIsNotificationType:Z

.field private final mItemIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter
    .parameter "isNotificationId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p5, itemIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "markitemread"

    invoke-static {}, Lcom/google/api/services/plusi/model/MarkItemReadRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MarkItemReadRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MarkItemReadResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MarkItemReadResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 49
    iput-object p5, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    .line 50
    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    .line 51
    return-void
.end method


# virtual methods
.method public final getItemIds()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_d
    return-void
.end method

.method public final isNotificationType()Z
    .registers 2

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    return v0
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;

    .end local p1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mIsNotificationType:Z

    if-eqz v0, :cond_f

    const-string v0, "4"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;->networkType:Ljava/lang/String;

    :goto_a
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->mItemIds:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;->itemIds:Ljava/util/List;

    return-void

    :cond_f
    const-string v0, "3"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MarkItemReadRequest;->networkType:Ljava/lang/String;

    goto :goto_a
.end method
