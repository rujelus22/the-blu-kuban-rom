.class final Lcom/google/android/apps/plus/fragments/WriteReviewFragment$1;
.super Ljava/lang/Object;
.source "WriteReviewFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/WriteReviewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter "s"

    .prologue
    .line 131
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "count"
    .parameter "after"

    .prologue
    .line 135
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mYourReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->access$100(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->mWriteReview:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->access$200(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    .line 140
    return-void
.end method
