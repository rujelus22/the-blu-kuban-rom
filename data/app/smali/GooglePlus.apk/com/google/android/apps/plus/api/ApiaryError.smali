.class public Lcom/google/android/apps/plus/api/ApiaryError;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ApiaryError.java"


# static fields
.field public static final JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/api/ApiaryError;",
            ">;"
        }
    .end annotation
.end field

.field private static final NESTED_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/api/ApiaryError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public code:Ljava/lang/Integer;

.field public domain:Ljava/lang/String;

.field public errors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/ApiaryError;",
            ">;"
        }
    .end annotation
.end field

.field public message:Ljava/lang/String;

.field public reason:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    const-class v0, Lcom/google/android/apps/plus/api/ApiaryError;

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "code"

    aput-object v2, v1, v3

    const-string v2, "domain"

    aput-object v2, v1, v4

    const-string v2, "reason"

    aput-object v2, v1, v5

    const-string v2, "message"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryError;->NESTED_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 24
    const-class v0, Lcom/google/android/apps/plus/api/ApiaryError;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "code"

    aput-object v2, v1, v3

    const-string v2, "domain"

    aput-object v2, v1, v4

    const-string v2, "reason"

    aput-object v2, v1, v5

    const-string v2, "message"

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/apps/plus/api/ApiaryError;->NESTED_JSON:Lcom/google/android/apps/plus/json/EsJson;

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string v3, "errors"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryError;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/apps/plus/api/ApiaryError;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
