.class public final Lcom/google/android/apps/plus/util/GifDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "GifDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Ljava/lang/Runnable;


# static fields
.field private static final NETSCAPE2_0:[B

.field private static sPaint:Landroid/graphics/Paint;

.field private static sScalePaint:Landroid/graphics/Paint;


# instance fields
.field private mActiveColorTable:[I

.field private mBackgroundColor:I

.field private mBackgroundIndex:I

.field private mBackup:[I

.field private mBackupSaved:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBlock:[B

.field private mBlockSize:I

.field private mCloned:Z

.field private mColorTableBuffer:[B

.field private mColors:[I

.field private final mData:[B

.field private mDisposalMethod:I

.field private mDone:Z

.field private mError:Z

.field private mFirstFrame:Landroid/graphics/Bitmap;

.field private mFrameCount:I

.field private mFrameDelay:I

.field private mFrameHeight:I

.field private mFrameWidth:I

.field private mFrameX:I

.field private mFrameY:I

.field private mGlobalColorTable:[I

.field private mGlobalColorTableSize:I

.field private mGlobalColorTableUsed:Z

.field private mHeight:I

.field private mInterlace:Z

.field private mIntrinsicHeight:I

.field private mIntrinsicWidth:I

.field private mLocalColorTable:[I

.field private mLocalColorTableSize:I

.field private mLocalColorTableUsed:Z

.field private mPixelStack:[B

.field private mPixels:[B

.field private mPrefix:[S

.field private mRunning:Z

.field private mScale:Z

.field private mScaleFactor:F

.field private final mStream:Ljava/io/ByteArrayInputStream;

.field private mSuffix:[B

.field private mTransparency:Z

.field private mTransparentColorIndex:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 62
    const-string v0, "NETSCAPE2.0"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/GifDrawable;->NETSCAPE2_0:[B

    return-void
.end method

.method public constructor <init>([B)V
    .registers 9
    .parameter "data"

    .prologue
    const/16 v6, 0x1000

    const/16 v4, 0x100

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 119
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 85
    const/16 v1, 0x300

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColorTableBuffer:[B

    .line 86
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTable:[I

    .line 97
    new-array v1, v4, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    .line 99
    iput v5, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    .line 104
    new-array v1, v6, [S

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    .line 105
    new-array v1, v6, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    .line 106
    const/16 v1, 0x1001

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    .line 120
    iput-object p1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    .line 121
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mStream:Ljava/io/ByteArrayInputStream;

    .line 122
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v1

    const/16 v4, 0x47

    if-ne v1, v4, :cond_98

    move v1, v2

    :goto_3a
    if-eqz v1, :cond_9a

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v1

    const/16 v4, 0x49

    if-ne v1, v4, :cond_9a

    move v1, v2

    :goto_45
    if-eqz v1, :cond_9c

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v1

    const/16 v4, 0x46

    if-ne v1, v4, :cond_9c

    move v1, v2

    :goto_50
    if-nez v1, :cond_9e

    iput-boolean v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    .line 125
    :cond_54
    :goto_54
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mStream:Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1, v3}, Ljava/io/ByteArrayInputStream;->mark(I)V

    .line 127
    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v1, :cond_82

    .line 128
    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    .line 131
    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    mul-int v0, v1, v3

    .line 132
    .local v0, pixelCount:I
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    .line 133
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    .line 135
    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    .line 136
    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readNextFrame()V

    .line 142
    .end local v0           #pixelCount:I
    :cond_82
    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_97

    .line 143
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sPaint:Landroid/graphics/Paint;

    .line 144
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 145
    sput-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sScalePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 147
    :cond_97
    return-void

    :cond_98
    move v1, v3

    .line 122
    goto :goto_3a

    :cond_9a
    move v1, v3

    goto :goto_45

    :cond_9c
    move v1, v3

    goto :goto_50

    :cond_9e
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v4

    and-int/lit16 v1, v4, 0x80

    if-eqz v1, :cond_ea

    move v1, v2

    :goto_c0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTableUsed:Z

    and-int/lit8 v1, v4, 0x7

    shl-int v1, v5, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTableSize:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundIndex:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTableUsed:Z

    if-eqz v1, :cond_54

    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v1, :cond_54

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTable:[I

    iget v4, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTableSize:I

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/plus/util/GifDrawable;->readColorTable([II)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTable:[I

    iget v4, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundIndex:I

    aget v1, v1, v4

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundColor:I

    goto/16 :goto_54

    :cond_ea
    move v1, v3

    goto :goto_c0
.end method

.method private backupFrame()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 618
    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    if-eqz v1, :cond_6

    .line 635
    :cond_5
    :goto_5
    return-void

    .line 622
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    if-nez v1, :cond_14

    .line 623
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    .line 625
    :try_start_d
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    array-length v1, v1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I
    :try_end_14
    .catch Ljava/lang/OutOfMemoryError; {:try_start_d .. :try_end_14} :catch_26

    .line 631
    :cond_14
    :goto_14
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    if-eqz v1, :cond_5

    .line 632
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    iget-object v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    iget-object v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 633
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    goto :goto_5

    .line 626
    :catch_26
    move-exception v0

    .line 627
    .local v0, e:Ljava/lang/OutOfMemoryError;
    const-string v1, "GifDrawable"

    const-string v2, "GifDrawable.backupFrame threw an OOME"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_14
.end method

.method public static isGif([B)Z
    .registers 5
    .parameter "data"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 158
    array-length v2, p0

    const/4 v3, 0x3

    if-lt v2, v3, :cond_1a

    aget-byte v2, p0, v1

    const/16 v3, 0x47

    if-ne v2, v3, :cond_1a

    aget-byte v2, p0, v0

    const/16 v3, 0x49

    if-ne v2, v3, :cond_1a

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    const/16 v3, 0x46

    if-ne v2, v3, :cond_1a

    :goto_19
    return v0

    :cond_1a
    move v0, v1

    goto :goto_19
.end method

.method private read()I
    .registers 3

    .prologue
    .line 808
    const/4 v0, 0x0

    .line 810
    .local v0, curByte:I
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mStream:Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->read()I
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_6} :catch_8

    move-result v0

    .line 814
    :goto_7
    return v0

    .line 812
    :catch_8
    move-exception v1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    goto :goto_7
.end method

.method private readBlock()I
    .registers 7

    .prologue
    .line 823
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    .line 824
    const/4 v2, 0x0

    .line 825
    .local v2, n:I
    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    if-lez v3, :cond_2b

    .line 827
    const/4 v0, 0x0

    .line 828
    .local v0, count:I
    :goto_c
    :try_start_c
    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    if-ge v2, v3, :cond_24

    .line 829
    iget-object v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mStream:Ljava/io/ByteArrayInputStream;

    iget-object v4, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    iget v5, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    sub-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v5}, Ljava/io/ByteArrayInputStream;->read([BII)I
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_1a} :catch_20

    move-result v0

    .line 830
    const/4 v3, -0x1

    if-eq v0, v3, :cond_24

    .line 831
    add-int/2addr v2, v0

    goto :goto_c

    .line 835
    :catch_20
    move-exception v1

    .line 836
    .local v1, e:Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 838
    .end local v1           #e:Ljava/lang/Exception;
    :cond_24
    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    if-ge v2, v3, :cond_2b

    .line 839
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    .line 842
    .end local v0           #count:I
    :cond_2b
    return v2
.end method

.method private readColorTable([II)V
    .registers 16
    .parameter "colorTable"
    .parameter "ncolors"

    .prologue
    .line 404
    mul-int/lit8 v8, p2, 0x3

    .line 405
    .local v8, nbytes:I
    const/4 v7, 0x0

    .line 407
    .local v7, n:I
    :try_start_3
    iget-object v10, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mStream:Ljava/io/ByteArrayInputStream;

    iget-object v11, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColorTableBuffer:[B

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12, v8}, Ljava/io/ByteArrayInputStream;->read([BII)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_b} :catch_12

    move-result v7

    .line 412
    :goto_c
    if-ge v7, v8, :cond_1b

    .line 413
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    .line 424
    :cond_11
    return-void

    .line 408
    :catch_12
    move-exception v1

    .line 409
    .local v1, e:Ljava/lang/Exception;
    const-string v10, "GifDrawable"

    const-string v11, "Cannot read color table"

    invoke-static {v10, v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_c

    .line 415
    .end local v1           #e:Ljava/lang/Exception;
    :cond_1b
    const/4 v3, 0x0

    .line 416
    .local v3, i:I
    const/4 v5, 0x0

    .local v5, j:I
    move v6, v5

    .end local v5           #j:I
    .local v6, j:I
    move v4, v3

    .line 417
    .end local v3           #i:I
    .local v4, i:I
    :goto_1f
    if-ge v4, p2, :cond_11

    .line 418
    iget-object v10, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColorTableBuffer:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6           #j:I
    .restart local v5       #j:I
    aget-byte v10, v10, v6

    and-int/lit16 v9, v10, 0xff

    .line 419
    .local v9, r:I
    iget-object v10, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColorTableBuffer:[B

    add-int/lit8 v6, v5, 0x1

    .end local v5           #j:I
    .restart local v6       #j:I
    aget-byte v10, v10, v5

    and-int/lit16 v2, v10, 0xff

    .line 420
    .local v2, g:I
    iget-object v10, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColorTableBuffer:[B

    add-int/lit8 v5, v6, 0x1

    .end local v6           #j:I
    .restart local v5       #j:I
    aget-byte v10, v10, v6

    and-int/lit16 v0, v10, 0xff

    .line 421
    .local v0, b:I
    add-int/lit8 v3, v4, 0x1

    .end local v4           #i:I
    .restart local v3       #i:I
    const/high16 v10, -0x100

    shl-int/lit8 v11, v9, 0x10

    or-int/2addr v10, v11

    shl-int/lit8 v11, v2, 0x8

    or-int/2addr v10, v11

    or-int/2addr v10, v0

    aput v10, p1, v4

    move v6, v5

    .end local v5           #j:I
    .restart local v6       #j:I
    move v4, v3

    .line 422
    .end local v3           #i:I
    .restart local v4       #i:I
    goto :goto_1f
.end method

.method private readNextFrame()V
    .registers 29

    .prologue
    .line 433
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    packed-switch v2, :pswitch_data_3e0

    .line 435
    :cond_7
    :goto_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    .line 436
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    .line 437
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    .line 438
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    .line 441
    :cond_1b
    :goto_1b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v10

    .line 442
    .local v10, code:I
    sparse-switch v10, :sswitch_data_3ec

    .line 483
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    .line 484
    :cond_27
    :goto_27
    return-void

    .line 433
    .end local v10           #code:I
    :pswitch_28
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    goto :goto_7

    :pswitch_2e
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_7

    :pswitch_47
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    if-nez v3, :cond_57

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundColor:I

    :cond_57
    const/4 v3, 0x0

    :goto_58
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    if-ge v3, v4, :cond_7

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    add-int/2addr v4, v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    mul-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    add-int/2addr v5, v4

    :goto_72
    if-ge v4, v5, :cond_7d

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    aput v2, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_72

    :cond_7d
    add-int/lit8 v3, v3, 0x1

    goto :goto_58

    .line 444
    .restart local v10       #code:I
    :sswitch_80
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v10

    .line 445
    sparse-switch v10, :sswitch_data_3fa

    .line 470
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto :goto_1b

    .line 447
    :sswitch_8b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v2

    and-int/lit8 v3, v2, 0x1c

    shr-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_ba

    const/4 v2, 0x1

    :goto_9f
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    goto/16 :goto_1b

    :cond_ba
    const/4 v2, 0x0

    goto :goto_9f

    .line 450
    :sswitch_bc
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readBlock()I

    .line 451
    const/4 v12, 0x1

    .line 452
    .local v12, netscape:Z
    const/4 v11, 0x0

    .local v11, i:I
    :goto_c1
    sget-object v2, Lcom/google/android/apps/plus/util/GifDrawable;->NETSCAPE2_0:[B

    array-length v2, v2

    if-ge v11, v2, :cond_d6

    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    aget-byte v2, v2, v11

    sget-object v3, Lcom/google/android/apps/plus/util/GifDrawable;->NETSCAPE2_0:[B

    aget-byte v3, v3, v11

    if-eq v2, v3, :cond_d3

    .line 454
    const/4 v12, 0x0

    .line 452
    :cond_d3
    add-int/lit8 v11, v11, 0x1

    goto :goto_c1

    .line 457
    :cond_d6
    if-eqz v12, :cond_e9

    .line 458
    :cond_d8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readBlock()I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    if-lez v2, :cond_1b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-eqz v2, :cond_d8

    goto/16 :goto_1b

    .line 460
    :cond_e9
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto/16 :goto_1b

    .line 464
    .end local v11           #i:I
    .end local v12           #netscape:Z
    :sswitch_ee
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto/16 :goto_1b

    .line 467
    :sswitch_f3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto/16 :goto_1b

    .line 475
    :sswitch_f8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v3

    and-int/lit16 v2, v3, 0x80

    if-eqz v2, :cond_1c7

    const/4 v2, 0x1

    :goto_121
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableUsed:Z

    const-wide/high16 v4, 0x4000

    and-int/lit8 v2, v3, 0x7

    add-int/lit8 v2, v2, 0x1

    int-to-double v6, v2

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableSize:I

    and-int/lit8 v2, v3, 0x40

    if-eqz v2, :cond_1ca

    const/4 v2, 0x1

    :goto_13a
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mInterlace:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableUsed:Z

    if-eqz v2, :cond_1cd

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    if-nez v2, :cond_152

    const/16 v2, 0x100

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    :cond_152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableSize:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/plus/util/GifDrawable;->readColorTable([II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    :cond_167
    :goto_167
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    if-eqz v3, :cond_183

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    const/4 v5, 0x0

    aput v5, v3, v4

    :cond_183
    move v13, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    if-nez v2, :cond_18f

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    :cond_18f
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v2, :cond_27

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    mul-int v20, v2, v3

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v21

    const/4 v2, 0x1

    shl-int v22, v2, v21

    add-int/lit8 v23, v22, 0x1

    add-int/lit8 v16, v22, 0x2

    const/4 v15, -0x1

    add-int/lit8 v14, v21, 0x1

    const/4 v2, 0x1

    shl-int/2addr v2, v14

    add-int/lit8 v5, v2, -0x1

    const/4 v2, 0x0

    :goto_1b2
    move/from16 v0, v22

    if-ge v2, v0, :cond_1e5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    const/4 v4, 0x0

    aput-short v4, v3, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    int-to-byte v4, v2

    aput-byte v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1b2

    :cond_1c7
    const/4 v2, 0x0

    goto/16 :goto_121

    :cond_1ca
    const/4 v2, 0x0

    goto/16 :goto_13a

    :cond_1cd
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mGlobalColorTable:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundIndex:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    if-ne v2, v3, :cond_167

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundColor:I

    goto :goto_167

    :cond_1e5
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/16 v17, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    move/from16 v19, v2

    move v2, v3

    move v3, v4

    move v4, v5

    move v5, v14

    :goto_1f4
    move/from16 v0, v19

    move/from16 v1, v20

    if-ge v0, v1, :cond_2ec

    if-nez v6, :cond_3ce

    if-ge v8, v5, :cond_218

    if-nez v3, :cond_207

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readBlock()I

    move-result v3

    if-lez v3, :cond_2ec

    const/4 v2, 0x0

    :cond_207
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    aget-byte v14, v14, v2

    and-int/lit16 v14, v14, 0xff

    shl-int/2addr v14, v8

    add-int/2addr v9, v14

    add-int/lit8 v8, v8, 0x8

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_1f4

    :cond_218
    and-int v14, v9, v4

    shr-int/2addr v9, v5

    sub-int/2addr v8, v5

    move/from16 v0, v16

    if-gt v14, v0, :cond_2ec

    move/from16 v0, v23

    if-eq v14, v0, :cond_2ec

    move/from16 v0, v22

    if-ne v14, v0, :cond_232

    add-int/lit8 v5, v21, 0x1

    const/4 v4, 0x1

    shl-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v16, v22, 0x2

    const/4 v15, -0x1

    goto :goto_1f4

    :cond_232
    const/16 v18, -0x1

    move/from16 v0, v18

    if-ne v15, v0, :cond_24c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    add-int/lit8 v7, v6, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    move-object/from16 v18, v0

    aget-byte v18, v18, v14

    aput-byte v18, v15, v6

    move v6, v7

    move v15, v14

    move v7, v14

    goto :goto_1f4

    :cond_24c
    move/from16 v0, v16

    if-ne v14, v0, :cond_3c9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v24, v0

    add-int/lit8 v18, v6, 0x1

    int-to-byte v7, v7

    aput-byte v7, v24, v6

    move v6, v15

    :goto_25c
    move/from16 v0, v22

    if-le v6, v0, :cond_27d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v24, v0

    add-int/lit8 v7, v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    move-object/from16 v25, v0

    aget-byte v25, v25, v6

    aput-byte v25, v24, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    move-object/from16 v18, v0

    aget-short v6, v18, v6

    move/from16 v18, v7

    goto :goto_25c

    :cond_27d
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    aget-byte v6, v7, v6

    and-int/lit16 v7, v6, 0xff

    const/16 v6, 0x1000

    move/from16 v0, v16

    if-ge v0, v6, :cond_2ec

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v24, v0

    add-int/lit8 v6, v18, 0x1

    int-to-byte v0, v7

    move/from16 v25, v0

    aput-byte v25, v24, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    move-object/from16 v18, v0

    int-to-short v15, v15

    aput-short v15, v18, v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    int-to-byte v0, v7

    move/from16 v18, v0

    aput-byte v18, v15, v16

    add-int/lit8 v15, v16, 0x1

    and-int v16, v15, v4

    if-nez v16, :cond_2b9

    const/16 v16, 0x1000

    move/from16 v0, v16

    if-ge v15, v0, :cond_2b9

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v15

    :cond_2b9
    move/from16 v26, v6

    move v6, v8

    move v8, v4

    move/from16 v4, v26

    move/from16 v27, v9

    move v9, v5

    move v5, v7

    move/from16 v7, v27

    :goto_2c5
    add-int/lit8 v18, v4, -0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    add-int/lit8 v16, v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v18

    aput-byte v24, v4, v17

    add-int/lit8 v4, v19, 0x1

    move/from16 v19, v4

    move/from16 v17, v16

    move v4, v8

    move/from16 v16, v15

    move v8, v6

    move v15, v14

    move/from16 v6, v18

    move/from16 v26, v9

    move v9, v7

    move v7, v5

    move/from16 v5, v26

    goto/16 :goto_1f4

    :cond_2ec
    move/from16 v2, v17

    :goto_2ee
    move/from16 v0, v20

    if-ge v2, v0, :cond_2fc

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    const/4 v4, 0x0

    aput-byte v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2ee

    :cond_2fc
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v2, :cond_27

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_30f

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->backupFrame()V

    :cond_30f
    const/4 v5, 0x1

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_314
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    if-ge v2, v6, :cond_389

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mInterlace:Z

    if-eqz v6, :cond_3c6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    if-lt v3, v6, :cond_32b

    add-int/lit8 v5, v5, 0x1

    packed-switch v5, :pswitch_data_40c

    :cond_32b
    :goto_32b
    add-int v6, v3, v4

    move/from16 v26, v3

    move v3, v6

    move/from16 v6, v26

    :goto_332
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    add-int/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    if-ge v6, v7, :cond_386

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    mul-int/2addr v7, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    add-int v8, v7, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    add-int/2addr v6, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    add-int/2addr v9, v7

    if-ge v9, v6, :cond_359

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    add-int/2addr v6, v7

    :cond_359
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    mul-int/2addr v7, v2

    move v9, v8

    :goto_35f
    if-ge v9, v6, :cond_386

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    add-int/lit8 v8, v7, 0x1

    aget-byte v7, v14, v7

    and-int/lit16 v7, v7, 0xff

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    aget v7, v14, v7

    if-eqz v7, :cond_379

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    aput v7, v14, v9

    :cond_379
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    move v7, v8

    goto :goto_35f

    :pswitch_37e
    const/4 v3, 0x4

    goto :goto_32b

    :pswitch_380
    const/4 v3, 0x2

    const/4 v4, 0x4

    goto :goto_32b

    :pswitch_383
    const/4 v3, 0x1

    const/4 v4, 0x2

    goto :goto_32b

    :cond_386
    add-int/lit8 v2, v2, 0x1

    goto :goto_314

    :cond_389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    if-eqz v2, :cond_3b3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    aput v13, v2, v3

    :cond_3b3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    goto/16 :goto_27

    .line 479
    :sswitch_3bf
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    goto/16 :goto_27

    :cond_3c6
    move v6, v2

    goto/16 :goto_332

    :cond_3c9
    move/from16 v18, v6

    move v6, v14

    goto/16 :goto_25c

    :cond_3ce
    move v14, v15

    move/from16 v15, v16

    move/from16 v26, v8

    move v8, v4

    move v4, v6

    move/from16 v6, v26

    move/from16 v27, v5

    move v5, v7

    move v7, v9

    move/from16 v9, v27

    goto/16 :goto_2c5

    .line 433
    nop

    :pswitch_data_3e0
    .packed-switch 0x0
        :pswitch_28
        :pswitch_28
        :pswitch_47
        :pswitch_2e
    .end packed-switch

    .line 442
    :sswitch_data_3ec
    .sparse-switch
        0x21 -> :sswitch_80
        0x2c -> :sswitch_f8
        0x3b -> :sswitch_3bf
    .end sparse-switch

    .line 445
    :sswitch_data_3fa
    .sparse-switch
        0x1 -> :sswitch_f3
        0xf9 -> :sswitch_8b
        0xfe -> :sswitch_ee
        0xff -> :sswitch_bc
    .end sparse-switch

    .line 475
    :pswitch_data_40c
    .packed-switch 0x2
        :pswitch_37e
        :pswitch_380
        :pswitch_383
    .end packed-switch
.end method

.method private readShort()I
    .registers 3

    .prologue
    .line 850
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method private reset()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mStream:Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->reset()V

    .line 351
    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    .line 352
    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    .line 353
    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    .line 354
    return-void
.end method

.method private skip()V
    .registers 2

    .prologue
    .line 858
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readBlock()I

    .line 859
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlockSize:I

    if-lez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-eqz v0, :cond_0

    .line 860
    :cond_b
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter "canvas"

    .prologue
    const/4 v2, 0x0

    .line 215
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v0, :cond_d

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    if-nez v0, :cond_e

    .line 235
    :cond_d
    :goto_d
    return-void

    .line 219
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    if-eqz v0, :cond_36

    .line 220
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 221
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScaleFactor:F

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScaleFactor:F

    invoke-virtual {p1, v0, v1, v2, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sScalePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 223
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 228
    :goto_26
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    if-eqz v0, :cond_3e

    .line 229
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_d

    .line 225
    :cond_36
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_26

    .line 230
    :cond_3e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    if-nez v0, :cond_46

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->start()V

    goto :goto_d

    .line 233
    :cond_46
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/util/GifDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    goto :goto_d
.end method

.method public final getFirstFrame()Landroid/graphics/Bitmap;
    .registers 5

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrame:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v0, :cond_2a

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    if-lez v0, :cond_2a

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    if-lez v0, :cond_2a

    .line 166
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    if-eqz v0, :cond_2d

    .line 167
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrame:Landroid/graphics/Bitmap;

    .line 168
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrame:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/util/GifDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 174
    :cond_2a
    :goto_2a
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrame:Landroid/graphics/Bitmap;

    return-object v0

    .line 170
    :cond_2d
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrame:Landroid/graphics/Bitmap;

    goto :goto_2a
.end method

.method public final getIntrinsicHeight()I
    .registers 2

    .prologue
    .line 250
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .registers 2

    .prologue
    .line 242
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    return v0
.end method

.method public final getOpacity()I
    .registers 2

    .prologue
    .line 258
    const/4 v0, 0x0

    return v0
.end method

.method public final isRunning()Z
    .registers 2

    .prologue
    .line 280
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    return v0
.end method

.method public final newDrawable()Lcom/google/android/apps/plus/util/GifDrawable;
    .registers 3

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mCloned:Z

    if-nez v0, :cond_8

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mCloned:Z

    .line 154
    .end local p0
    :goto_7
    return-object p0

    .restart local p0
    :cond_8
    new-instance v0, Lcom/google/android/apps/plus/util/GifDrawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;-><init>([B)V

    move-object p0, v0

    goto :goto_7
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .registers 5
    .parameter "bounds"

    .prologue
    .line 182
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 183
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    .line 184
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    .line 185
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    if-eq v0, v1, :cond_3d

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    if-eq v0, v1, :cond_3d

    const/4 v0, 0x1

    :goto_1c
    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    .line 186
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    if-eqz v0, :cond_36

    .line 187
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScaleFactor:F

    .line 190
    :cond_36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrame:Landroid/graphics/Bitmap;

    .line 191
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->reset()V

    .line 192
    return-void

    .line 185
    :cond_3d
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method public final run()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 319
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    if-eqz v0, :cond_f

    .line 322
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    if-le v0, v1, :cond_2e

    .line 323
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    .line 324
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->reset()V

    .line 333
    :cond_f
    invoke-direct {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readNextFrame()V

    .line 334
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    if-nez v0, :cond_22

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    if-nez v0, :cond_22

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    if-eq v0, v1, :cond_f

    .line 338
    :cond_22
    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    if-nez v0, :cond_2a

    .line 339
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    .line 342
    :cond_2a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->invalidateSelf()V

    .line 343
    :goto_2d
    return-void

    .line 326
    :cond_2e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->stop()V

    goto :goto_2d
.end method

.method public final setAlpha(I)V
    .registers 2
    .parameter "alpha"

    .prologue
    .line 266
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .parameter "cf"

    .prologue
    .line 273
    return-void
.end method

.method public final setVisible(ZZ)Z
    .registers 4
    .parameter "visible"
    .parameter "restart"

    .prologue
    .line 199
    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    .line 200
    .local v0, changed:Z
    if-eqz p1, :cond_e

    .line 201
    if-nez v0, :cond_a

    if-eqz p2, :cond_d

    .line 202
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->start()V

    .line 207
    :cond_d
    :goto_d
    return v0

    .line 205
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->stop()V

    goto :goto_d
.end method

.method public final start()V
    .registers 2

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_c

    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->run()V

    .line 292
    :cond_c
    return-void
.end method

.method public final stop()V
    .registers 2

    .prologue
    .line 299
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 300
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/util/GifDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 302
    :cond_9
    return-void
.end method

.method public final unscheduleSelf(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "what"

    .prologue
    .line 309
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    .line 311
    return-void
.end method
