.class public Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;
.super Landroid/view/ViewGroup;
.source "HostActionBarWidgetLayout.java"


# instance fields
.field private mMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->parseAttr(Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->parseAttr(Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method private parseAttr(Landroid/util/AttributeSet;)V
    .registers 7
    .parameter "attrs"

    .prologue
    const/4 v4, 0x0

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x101011f

    aput v3, v2, v4

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 36
    .local v0, a:Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->mMaxWidth:I

    .line 37
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 38
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .registers 12
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    const/4 v5, 0x0

    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->getChildCount()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_9

    .line 88
    :goto_8
    return-void

    .line 84
    :cond_9
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 86
    .local v1, height:I
    sub-int v3, p5, p3

    sub-int/2addr v3, v1

    div-int/lit8 v2, v3, 0x2

    .line 87
    .local v2, top:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int v4, v2, v1

    invoke-virtual {v0, v5, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    goto :goto_8
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/4 v9, -0x2

    const/high16 v8, 0x4000

    const/4 v7, 0x0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->getChildCount()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_f

    .line 43
    invoke-virtual {p0, v7, v7}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->setMeasuredDimension(II)V

    .line 76
    :goto_e
    return-void

    .line 47
    :cond_f
    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 50
    .local v3, requestedHeight:I
    if-ne v3, v9, :cond_57

    .line 51
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    const/high16 v6, -0x8000

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 58
    .local v1, childHeightMeasureSpec:I
    :goto_25
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v4, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 59
    .local v4, requestedWidth:I
    if-ne v4, v9, :cond_60

    .line 60
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v5, v1}, Landroid/view/View;->measure(II)V

    .line 67
    :goto_34
    iget v5, p0, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->mMaxWidth:I

    invoke-static {v5, p1}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->resolveSize(II)I

    move-result v2

    .line 68
    .local v2, maxWidth:I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    if-le v5, v2, :cond_47

    .line 69
    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v5, v1}, Landroid/view/View;->measure(II)V

    .line 74
    :cond_47
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-static {v6, p2}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->resolveSize(II)I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/plus/views/HostActionBarWidgetLayout;->setMeasuredDimension(II)V

    goto :goto_e

    .line 54
    .end local v1           #childHeightMeasureSpec:I
    .end local v2           #maxWidth:I
    .end local v4           #requestedWidth:I
    :cond_57
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .restart local v1       #childHeightMeasureSpec:I
    goto :goto_25

    .line 63
    .restart local v4       #requestedWidth:I
    :cond_60
    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v5, v1}, Landroid/view/View;->measure(II)V

    goto :goto_34
.end method
