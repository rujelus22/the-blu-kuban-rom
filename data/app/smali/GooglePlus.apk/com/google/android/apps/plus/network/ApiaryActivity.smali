.class public Lcom/google/android/apps/plus/network/ApiaryActivity;
.super Ljava/lang/Object;
.source "ApiaryActivity.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/network/ApiaryActivity$Type;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/network/ApiaryActivity;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDeepLinkMetadata:Landroid/os/Bundle;

.field private mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 192
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryActivity$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiaryActivity$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/network/ApiaryActivity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method private update()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-nez v0, :cond_12

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    if-nez v0, :cond_20

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/MediaLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    .line 163
    :cond_20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public getContent()Ljava/lang/String;
    .registers 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .registers 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getEmbed(Ljava/lang/String;)Lcom/google/api/services/plusi/model/EmbedClientItem;
    .registers 6
    .parameter "deepLinkId"

    .prologue
    .line 94
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->embedItem:Ljava/util/List;

    if-eqz v1, :cond_25

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->embedItem:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    .line 102
    .local v0, embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    :goto_15
    if-eqz p1, :cond_24

    if-eqz v0, :cond_24

    .line 103
    new-instance v1, Lcom/google/api/services/plusi/model/DeepLinkData;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DeepLinkData;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    .line 104
    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iput-object p1, v1, Lcom/google/api/services/plusi/model/DeepLinkData;->deepLinkId:Ljava/lang/String;

    .line 106
    :cond_24
    return-object v0

    .line 96
    .end local v0           #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    :cond_25
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    if-eqz v1, :cond_68

    .line 97
    new-instance v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedClientItem;-><init>()V

    new-instance v1, Lcom/google/api/services/plusi/model/Thing;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Thing;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v3, "title"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Thing;->name:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v3, "description"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Thing;->description:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->thing:Lcom/google/api/services/plusi/model/Thing;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v3, "thumbnailUrl"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Thing;->imageUrl:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->type:Ljava/util/List;

    const-string v2, "THING"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .restart local v0       #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    goto :goto_15

    .line 99
    .end local v0           #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    :cond_68
    const/4 v0, 0x0

    .restart local v0       #embed:Lcom/google/api/services/plusi/model/EmbedClientItem;
    goto :goto_15
.end method

.method public getFavIconUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 227
    const/4 v0, 0x0

    return-object v0
.end method

.method public getImage()Ljava/lang/String;
    .registers 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v1, "thumbnailUrl"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMediaJson()Ljava/lang/String;
    .registers 7

    .prologue
    const/4 v4, 0x0

    .line 73
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-nez v5, :cond_f

    move-object v1, v4

    .line 75
    .local v1, dataList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :goto_6
    if-eqz v1, :cond_e

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 89
    :cond_e
    :goto_e
    return-object v4

    .line 73
    .end local v1           #dataList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_f
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    iget-object v1, v5, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->blackboxPreviewData:Ljava/util/List;

    goto :goto_6

    .line 79
    .restart local v1       #dataList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_14
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 81
    .local v3, mediaJson:Ljava/lang/StringBuilder;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_34

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    .local v0, data:Ljava/lang/String;
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1f

    .line 86
    .end local v0           #data:Ljava/lang/String;
    :cond_34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 87
    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_e
.end method

.method public getType()Lcom/google/android/apps/plus/network/ApiaryActivity$Type;
    .registers 2

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/apps/plus/network/ApiaryActivity$Type;->NONE:Lcom/google/android/apps/plus/network/ApiaryActivity$Type;

    return-object v0
.end method

.method public final setDeepLinkMetadata(Landroid/os/Bundle;)V
    .registers 2
    .parameter "deepLinkData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update()V

    .line 149
    return-void
.end method

.method public final setLinkPreview(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)V
    .registers 2
    .parameter "linkPreview"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    .line 138
    invoke-direct {p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update()V

    .line 139
    return-void
.end method

.method protected update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .registers 4
    .parameter "mediaLayout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-nez v0, :cond_c

    .line 167
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No metadata."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_c
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    if-eqz v0, :cond_17

    .line 185
    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mLinkPreview:Lcom/google/api/services/plusi/model/LinkPreviewResponse;

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 189
    :goto_11
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryActivity;->mDeepLinkMetadata:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 190
    return-void

    .line 187
    :cond_17
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_11
.end method
