.class public Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
.super Ljava/lang/Object;
.source "PhotoOneUpAnimationController.java"


# instance fields
.field private mCurrentOffset:F

.field private mSlideFromTop:Z

.field private final mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

.field private mState:I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .registers 4
    .parameter "view"
    .parameter "slideFromTop"

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    .line 34
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    .line 78
    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 14
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 14
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 14
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    return v0
.end method

.method private startAnimation(FI)V
    .registers 7
    .parameter "finalDelta"
    .parameter "time"

    .prologue
    const/4 v3, 0x0

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    .line 105
    .local v1, currentAnimation:Landroid/view/animation/Animation;
    if-eqz v1, :cond_c

    .line 106
    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    .line 109
    :cond_c
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    invoke-direct {v0, v3, v3, v2, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 110
    .local v0, anim:Landroid/view/animation/Animation;
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 111
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 112
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 115
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 116
    return-void
.end method

.method private updateVisibility()V
    .registers 5

    .prologue
    .line 120
    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    if-nez v2, :cond_13

    .line 121
    const/4 v1, 0x0

    .line 132
    .local v1, visible:Z
    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    if-eqz v1, :cond_43

    const/4 v2, 0x0

    :goto_a
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setClickable(Z)V

    .line 136
    return-void

    .line 123
    .end local v1           #visible:Z
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->getHideOffset(Z)I

    move-result v0

    .line 124
    .local v0, hideOffset:I
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-nez v2, :cond_2c

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-lez v2, :cond_2c

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    int-to-float v3, v0

    cmpl-float v2, v2, v3

    if-gez v2, :cond_3f

    :cond_2c
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-eqz v2, :cond_41

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-lez v2, :cond_41

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    int-to-float v3, v0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_41

    .line 126
    :cond_3f
    const/4 v1, 0x0

    .restart local v1       #visible:Z
    goto :goto_5

    .line 128
    .end local v1           #visible:Z
    :cond_41
    const/4 v1, 0x1

    .restart local v1       #visible:Z
    goto :goto_5

    .line 132
    .end local v0           #hideOffset:I
    :cond_43
    const/16 v2, 0x8

    goto :goto_a
.end method


# virtual methods
.method public final animate(Z)V
    .registers 6
    .parameter "hide"

    .prologue
    const/16 v3, 0x64

    .line 82
    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    if-eqz v1, :cond_b

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_15

    :cond_b
    if-nez p1, :cond_15

    .line 83
    const/4 v1, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->startAnimation(FI)V

    .line 89
    :cond_11
    :goto_11
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V

    .line 90
    return-void

    .line 84
    :cond_15
    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_11

    :cond_1f
    if-eqz p1, :cond_11

    .line 85
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->getHideOffset(Z)I

    move-result v0

    .line 86
    .local v0, hideOffset:I
    int-to-float v1, v0

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->startAnimation(FI)V

    .line 87
    int-to-float v1, v0

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    goto :goto_11
.end method

.method protected getHideOffset(Z)I
    .registers 4
    .parameter "slideFromTop"

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-eqz v0, :cond_13

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    neg-int v0, v0

    .line 99
    :goto_12
    return v0

    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_12
.end method
