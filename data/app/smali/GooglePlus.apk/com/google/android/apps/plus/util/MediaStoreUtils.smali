.class public final Lcom/google/android/apps/plus/util/MediaStoreUtils;
.super Ljava/lang/Object;
.source "MediaStoreUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/MediaStoreUtils$VideoQuery;
    }
.end annotation


# static fields
.field private static final PAT_RESOLUTION:Ljava/util/regex/Pattern;

.field public static final PHONE_STORAGE_IMAGES_URI:Landroid/net/Uri;

.field public static final PHONE_STORAGE_VIDEO_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 36
    const-string v0, "phoneStorage"

    invoke-static {v0}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_IMAGES_URI:Landroid/net/Uri;

    .line 39
    const-string v0, "phoneStorage"

    invoke-static {v0}, Landroid/provider/MediaStore$Video$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_VIDEO_URI:Landroid/net/Uri;

    .line 60
    const-string v0, "(\\d+)[xX](\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PAT_RESOLUTION:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static deleteLocalFileAndMediaStore(Landroid/content/ContentResolver;Landroid/net/Uri;)Z
    .registers 6
    .parameter "resolver"
    .parameter "localContentUri"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 162
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getFilePath(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 164
    .local v1, filePath:Ljava/lang/String;
    invoke-virtual {p0, p1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-ne v3, v2, :cond_20

    .line 166
    .local v2, status:Z
    :goto_c
    if-eqz v2, :cond_1f

    if-eqz v1, :cond_1f

    .line 167
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, file:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 169
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v2

    .line 173
    .end local v0           #file:Ljava/io/File;
    :cond_1f
    return v2

    .line 164
    .end local v2           #status:Z
    :cond_20
    const/4 v2, 0x0

    goto :goto_c
.end method

.method private static getFilePath(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .registers 11
    .parameter "cr"
    .parameter "uri"

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x5

    const/4 v3, 0x0

    .line 270
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_data"

    aput-object v0, v2, v1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 272
    .local v6, cursor:Landroid/database/Cursor;
    if-nez v6, :cond_31

    .line 273
    const-string v0, "MediaStoreUtils"

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 274
    const-string v0, "MediaStoreUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getFilePath: query returned null cursor for uri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_30
    :goto_30
    return-object v3

    .line 279
    :cond_31
    const/4 v7, 0x0

    .line 281
    .local v7, path:Ljava/lang/String;
    :try_start_32
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_59

    .line 282
    const-string v0, "MediaStoreUtils"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 283
    const-string v0, "MediaStoreUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getFilePath: query returned empty cursor for uri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_55
    .catchall {:try_start_32 .. :try_end_55} :catchall_8a

    .line 285
    :cond_55
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_30

    .line 288
    :cond_59
    const/4 v0, 0x0

    :try_start_5a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 289
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 290
    const-string v0, "MediaStoreUtils"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 291
    const-string v0, "MediaStoreUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getFilePath: MediaColumns.DATA was empty for uri="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_81
    .catchall {:try_start_5a .. :try_end_81} :catchall_8a

    .line 293
    :cond_81
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_30

    :cond_85
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v3, v7

    .line 299
    goto :goto_30

    .line 297
    :catchall_8a
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getThumbnail(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    .registers 5
    .parameter "context"
    .parameter "uri"
    .parameter "kind"

    .prologue
    const/4 v1, 0x1

    .line 128
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->getMaxThumbnailDimension(Landroid/content/Context;I)I

    move-result v0

    .line 129
    .local v0, microSize:I
    invoke-static {p0, p1, v0, v0, v1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getThumbnailHelper(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static getThumbnail(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .registers 7
    .parameter "context"
    .parameter "uri"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v0, 0x3

    .line 143
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getMaxThumbnailDimension(Landroid/content/Context;I)I

    move-result v1

    .line 145
    .local v1, microSize:I
    if-gt p2, v1, :cond_9

    if-le p3, v1, :cond_a

    :cond_9
    const/4 v0, 0x1

    .line 149
    .local v0, kind:I
    :cond_a
    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getThumbnailHelper(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method private static getThumbnailHelper(Landroid/content/Context;Landroid/net/Uri;III)Landroid/graphics/Bitmap;
    .registers 14
    .parameter "context"
    .parameter "uri"
    .parameter "width"
    .parameter "height"
    .parameter "kind"

    .prologue
    const/4 v0, 0x0

    .line 315
    if-nez p1, :cond_4

    .line 354
    :cond_3
    :goto_3
    return-object v0

    .line 320
    :cond_4
    invoke-static {p1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isExternalMediaStoreUri(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 324
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 325
    .local v1, cr:Landroid/content/ContentResolver;
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 328
    .local v2, id:J
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 330
    .local v4, mimeType:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isImageMimeType(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3b

    .line 331
    invoke-static {v1, v2, v3, p4, v0}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 344
    .local v0, bmp:Landroid/graphics/Bitmap;
    :goto_20
    if-eqz v0, :cond_3

    .line 345
    invoke-static {v1, p1, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->rotateBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-ne v6, p2, :cond_32

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-eq v6, p3, :cond_3

    .line 348
    :cond_32
    invoke-static {v0, p2, p3}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeAndCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 350
    .local v5, resizedBitmap:Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 351
    move-object v0, v5

    goto :goto_3

    .line 333
    .end local v0           #bmp:Landroid/graphics/Bitmap;
    .end local v5           #resizedBitmap:Landroid/graphics/Bitmap;
    :cond_3b
    invoke-static {v4}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isVideoMimeType(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_46

    .line 334
    invoke-static {v1, v2, v3, p4, v0}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0       #bmp:Landroid/graphics/Bitmap;
    goto :goto_20

    .line 337
    .end local v0           #bmp:Landroid/graphics/Bitmap;
    :cond_46
    const-string v6, "MediaStoreUtils"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 338
    const-string v6, "MediaStoreUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getThumbnail: unrecognized mimeType="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", uri="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public static isExternalMediaStoreUri(Landroid/net/Uri;)Z
    .registers 6
    .parameter "uri"

    .prologue
    const/4 v3, 0x0

    .line 94
    invoke-static {p0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 95
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, path:Ljava/lang/String;
    sget-object v4, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 97
    .local v0, externalImagePrefix:Ljava/lang/String;
    sget-object v4, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, externalVideoPrefix:Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_23

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_24

    :cond_23
    const/4 v3, 0x1

    .line 100
    .end local v0           #externalImagePrefix:Ljava/lang/String;
    .end local v1           #externalVideoPrefix:Ljava/lang/String;
    .end local v2           #path:Ljava/lang/String;
    :cond_24
    return v3
.end method

.method public static isImageMimeType(Ljava/lang/String;)Z
    .registers 2
    .parameter "mimeType"

    .prologue
    .line 108
    if-eqz p0, :cond_c

    const-string v0, "image/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static isMediaStoreUri(Landroid/net/Uri;)Z
    .registers 3
    .parameter "uri"

    .prologue
    .line 74
    if-eqz p0, :cond_1c

    const-string v0, "content"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "media"

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method public static isVideoMimeType(Ljava/lang/String;)Z
    .registers 2
    .parameter "mimeType"

    .prologue
    .line 115
    if-eqz p0, :cond_c

    const-string v0, "video/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;
    .registers 7
    .parameter "resolver"
    .parameter "uri"

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 188
    .local v1, mimeType:Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4} :catch_6

    move-result-object v1

    .line 194
    :cond_5
    :goto_5
    return-object v1

    .line 189
    :catch_6
    move-exception v0

    .line 190
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "MediaStoreUtils"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 191
    const-string v2, "MediaStoreUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "safeGetMimeType failed for uri="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5
.end method

.method public static toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;
    .registers 19
    .parameter "context"
    .parameter "uri"

    .prologue
    .line 218
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 219
    .local v1, cr:Landroid/content/ContentResolver;
    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isVideoMimeType(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_12

    .line 220
    const/4 v13, 0x0

    .line 262
    :goto_11
    return-object v13

    .line 224
    :cond_12
    new-instance v14, Lcom/google/api/services/plusi/model/DataVideoStream;

    invoke-direct {v14}, Lcom/google/api/services/plusi/model/DataVideoStream;-><init>()V

    .line 225
    .local v14, videoStream:Lcom/google/api/services/plusi/model/DataVideoStream;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    .line 227
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v14, Lcom/google/api/services/plusi/model/DataVideoStream;->formatId:Ljava/lang/Integer;

    .line 229
    const/16 v16, 0x0

    .line 230
    .local v16, width:I
    const/4 v10, 0x0

    .line 231
    .local v10, height:I
    const-wide/16 v8, 0x0

    .line 232
    .local v8, durationMsec:J
    sget-object v3, Lcom/google/android/apps/plus/util/MediaStoreUtils$VideoQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 233
    .local v7, cursor:Landroid/database/Cursor;
    if-eqz v7, :cond_69

    .line 235
    :try_start_36
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_66

    .line 236
    const/4 v2, 0x1

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 238
    const/4 v2, 0x2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 239
    .local v12, resolution:Ljava/lang/String;
    if-eqz v12, :cond_66

    .line 240
    sget-object v2, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PAT_RESOLUTION:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v12}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .line 241
    .local v11, m:Ljava/util/regex/Matcher;
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_66

    .line 242
    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 243
    const/4 v2, 0x2

    invoke-virtual {v11, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_65
    .catchall {:try_start_36 .. :try_end_65} :catchall_90

    move-result v10

    .line 248
    .end local v11           #m:Ljava/util/regex/Matcher;
    .end local v12           #resolution:Ljava/lang/String;
    :cond_66
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 251
    :cond_69
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v14, Lcom/google/api/services/plusi/model/DataVideoStream;->width:Ljava/lang/Integer;

    .line 252
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v14, Lcom/google/api/services/plusi/model/DataVideoStream;->height:Ljava/lang/Integer;

    .line 254
    new-instance v15, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 255
    .local v15, videoStreamList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataVideoStream;>;"
    invoke-interface {v15, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    new-instance v13, Lcom/google/api/services/plusi/model/DataVideo;

    invoke-direct {v13}, Lcom/google/api/services/plusi/model/DataVideo;-><init>()V

    .line 259
    .local v13, videoData:Lcom/google/api/services/plusi/model/DataVideo;
    const-string v2, "FINAL"

    iput-object v2, v13, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    .line 260
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v13, Lcom/google/api/services/plusi/model/DataVideo;->durationMillis:Ljava/lang/Long;

    .line 261
    iput-object v15, v13, Lcom/google/api/services/plusi/model/DataVideo;->stream:Ljava/util/List;

    goto :goto_11

    .line 248
    .end local v13           #videoData:Lcom/google/api/services/plusi/model/DataVideo;
    .end local v15           #videoStreamList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataVideoStream;>;"
    :catchall_90
    move-exception v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v2
.end method

.method public static toVideoDataBytes(Landroid/content/Context;Landroid/net/Uri;)[B
    .registers 4
    .parameter "context"
    .parameter "uri"

    .prologue
    .line 205
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;

    move-result-object v0

    .line 206
    .local v0, videoData:Lcom/google/api/services/plusi/model/DataVideo;
    if-nez v0, :cond_8

    const/4 v1, 0x0

    :goto_7
    return-object v1

    :cond_8
    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    goto :goto_7
.end method
