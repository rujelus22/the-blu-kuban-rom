.class final Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;
.super Landroid/widget/BaseAdapter;
.source "AclFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/AclFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AudienceAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 15
    .parameter "context"
    .parameter "audienceData"

    .prologue
    const/4 v11, 0x1

    .line 96
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 88
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    .line 97
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_11
    if-ge v3, v4, :cond_22

    aget-object v5, v0, v3

    .line 98
    .local v5, user:Lcom/google/android/apps/plus/content/PersonData;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;

    invoke-direct {v7, v5}, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    .line 102
    .end local v5           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_22
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v1

    .line 103
    .local v1, hiddenUserCount:I
    if-lez v1, :cond_46

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0027

    new-array v8, v11, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v1, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 106
    .local v2, hiddenUserText:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;

    invoke-direct {v7, v11, v2}, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;-><init>(ILjava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    .end local v2           #hiddenUserText:Ljava/lang/String;
    :cond_46
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mContext:Landroid/content/Context;

    .line 110
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "position"

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 133
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .registers 3
    .parameter "position"

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;

    iget v0, v0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mType:I

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    const/4 v7, 0x0

    .line 158
    if-nez p2, :cond_3c

    .line 159
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 161
    .local v2, layoutInflater:Landroid/view/LayoutInflater;
    const v5, 0x7f030001

    invoke-virtual {v2, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 166
    .end local v2           #layoutInflater:Landroid/view/LayoutInflater;
    .local v4, view:Landroid/view/View;
    :goto_14
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;

    .line 167
    .local v1, item:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;
    invoke-virtual {v4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 169
    const v5, 0x7f090048

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    .line 170
    .local v0, avatarView:Lcom/google/android/apps/plus/views/AvatarView;
    iget v5, v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mType:I

    packed-switch v5, :pswitch_data_66

    .line 187
    :goto_2d
    const v5, 0x7f090049

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 188
    .local v3, nameView:Landroid/widget/TextView;
    iget-object v5, v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mContent:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    return-object v4

    .line 163
    .end local v0           #avatarView:Lcom/google/android/apps/plus/views/AvatarView;
    .end local v1           #item:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;
    .end local v3           #nameView:Landroid/widget/TextView;
    .end local v4           #view:Landroid/view/View;
    :cond_3c
    move-object v4, p2

    .restart local v4       #view:Landroid/view/View;
    goto :goto_14

    .line 172
    .restart local v0       #avatarView:Lcom/google/android/apps/plus/views/AvatarView;
    .restart local v1       #item:Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;
    :pswitch_3e
    iget-object v5, v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    if-eqz v5, :cond_5d

    iget-object v5, v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5d

    .line 174
    iget-object v5, v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lcom/google/android/apps/plus/fragments/AclFragment$AudienceItem;->mPerson:Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getCompressedPhotoUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_5d
    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_2d

    .line 182
    :pswitch_61
    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_2d

    .line 170
    :pswitch_data_66
    .packed-switch 0x0
        :pswitch_3e
        :pswitch_61
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 141
    const/4 v0, 0x2

    return v0
.end method
