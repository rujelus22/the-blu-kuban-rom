.class public Lcom/google/android/apps/plus/content/DbSerializer;
.super Ljava/lang/Object;
.source "DbSerializer.java"


# direct methods
.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method private static decodeUtf8([B)Ljava/lang/String;
    .registers 3
    .parameter "buf"

    .prologue
    .line 176
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_7} :catch_8

    return-object v0

    .line 179
    :catch_8
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method protected static getLongString(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .registers 4
    .parameter "bb"

    .prologue
    .line 160
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 161
    .local v1, length:I
    if-lez v1, :cond_10

    .line 162
    new-array v0, v1, [B

    .line 163
    .local v0, buf:[B
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 164
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSerializer;->decodeUtf8([B)Ljava/lang/String;

    move-result-object v2

    .line 166
    .end local v0           #buf:[B
    :goto_f
    return-object v2

    :cond_10
    const/4 v2, 0x0

    goto :goto_f
.end method

.method protected static getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .registers 4
    .parameter "bb"

    .prologue
    .line 142
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v1

    .line 143
    .local v1, length:S
    if-lez v1, :cond_10

    .line 144
    new-array v0, v1, [B

    .line 145
    .local v0, buf:[B
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 146
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSerializer;->decodeUtf8([B)Ljava/lang/String;

    move-result-object v2

    .line 148
    .end local v0           #buf:[B
    :goto_f
    return-object v2

    :cond_10
    const/4 v2, 0x0

    goto :goto_f
.end method

.method protected static putLongString(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .registers 4
    .parameter "os"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 125
    if-eqz p1, :cond_10

    .line 126
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 127
    .local v0, bytes:[B
    array-length v1, v0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 128
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 132
    .end local v0           #bytes:[B
    :goto_f
    return-void

    .line 130
    :cond_10
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_f
.end method

.method protected static putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V
    .registers 4
    .parameter "os"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    if-eqz p1, :cond_10

    .line 110
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 111
    .local v0, bytes:[B
    array-length v1, v0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 112
    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 116
    .end local v0           #bytes:[B
    :goto_f
    return-void

    .line 114
    :cond_10
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    goto :goto_f
.end method
