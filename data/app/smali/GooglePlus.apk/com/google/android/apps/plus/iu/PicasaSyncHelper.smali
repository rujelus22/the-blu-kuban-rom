.class final Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
.super Ljava/lang/Object;
.source "PicasaSyncHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/PicasaSyncHelper$EntryMetadata;,
        Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;
    }
.end annotation


# static fields
.field private static final PHOTO_PROJECTION_ID_DATE:[Ljava/lang/String;

.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static final PROJECTION_ID_ACCOUNT:[Ljava/lang/String;

.field private static final USER_TABLE_NAME:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

.field private final mEtagMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    sget-object v0, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    .line 35
    sget-object v0, Lcom/google/android/apps/plus/iu/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->USER_TABLE_NAME:Ljava/lang/String;

    .line 39
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "account"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PROJECTION_ID_ACCOUNT:[Ljava/lang/String;

    .line 40
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "date_updated"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PHOTO_PROJECTION_ID_DATE:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mEtagMap:Ljava/util/HashMap;

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mContext:Landroid/content/Context;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    .line 62
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/iu/PicasaSyncHelper;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    .registers 3
    .parameter "context"

    .prologue
    .line 53
    const-class v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->sInstance:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    if-nez v0, :cond_e

    .line 54
    new-instance v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->sInstance:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    .line 56
    :cond_e
    sget-object v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->sInstance:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 53
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;
    .registers 4
    .parameter "syncResult"
    .parameter "thread"

    .prologue
    .line 246
    new-instance v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;-><init>(Lcom/google/android/apps/plus/iu/PicasaSyncHelper;Landroid/content/SyncResult;Ljava/lang/Thread;)V

    return-object v0
.end method

.method public final findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;
    .registers 11
    .parameter "account"

    .prologue
    const/4 v5, 0x0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->USER_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "account=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 135
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1d
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_34

    sget-object v0, Lcom/google/android/apps/plus/iu/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    new-instance v1, Lcom/google/android/apps/plus/iu/UserEntry;

    invoke-direct {v1}, Lcom/google/android/apps/plus/iu/UserEntry;-><init>()V

    invoke-virtual {v0, v8, v1}, Lcom/android/gallery3d/common/EntrySchema;->cursorToObject(Landroid/database/Cursor;Lcom/android/gallery3d/common/Entry;)Lcom/android/gallery3d/common/Entry;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/iu/UserEntry;
    :try_end_30
    .catchall {:try_start_1d .. :try_end_30} :catchall_36

    .line 139
    :goto_30
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v0

    :cond_34
    move-object v0, v5

    .line 135
    goto :goto_30

    .line 139
    :catchall_36
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final syncAccounts()V
    .registers 21

    .prologue
    .line 65
    const-string v2, "PicasaSync"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 66
    const-string v2, "PicasaSync"

    const-string v3, "sync account database"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 72
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 73
    .local v18, obsoleteAccount:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    sget-object v2, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->USER_TABLE_NAME:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PROJECTION_ID_ACCOUNT:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 76
    .local v11, cursor:Landroid/database/Cursor;
    :goto_2a
    :try_start_2a
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_45

    .line 77
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 78
    .local v15, id:Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 79
    .local v9, account:Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3f
    .catchall {:try_start_2a .. :try_end_3f} :catchall_40

    goto :goto_2a

    .line 82
    .end local v9           #account:Ljava/lang/String;
    .end local v15           #id:Ljava/lang/String;
    :catchall_40
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_45
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 86
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string v3, "com.google"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v13

    .line 87
    .local v13, googleAccounts:[Landroid/accounts/Account;
    const-string v2, "PicasaSync"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_77

    .line 88
    const-string v2, "PicasaSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "accounts in DB="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_77
    if-eqz v13, :cond_dc

    .line 91
    move-object v10, v13

    .local v10, arr$:[Landroid/accounts/Account;
    array-length v0, v13

    move/from16 v17, v0

    .local v17, len$:I
    const/4 v14, 0x0

    .local v14, i$:I
    :goto_7e
    move/from16 v0, v17

    if-ge v14, v0, :cond_dc

    aget-object v9, v10, v14

    .line 93
    .local v9, account:Landroid/accounts/Account;
    iget-object v2, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_d6

    const/16 v16, 0x1

    .line 94
    .local v16, isAccountInDb:Z
    :goto_90
    const-string v2, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v9, v2}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_d9

    const/16 v19, 0x1

    .line 95
    .local v19, syncable:Z
    :goto_9a
    if-nez v16, :cond_d3

    if-eqz v19, :cond_d3

    .line 98
    const-string v2, "PicasaSync"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_bf

    .line 99
    const-string v2, "PicasaSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "add account to DB:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_bf
    sget-object v2, Lcom/google/android/apps/plus/iu/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/plus/iu/UserEntry;

    iget-object v5, v9, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/iu/UserEntry;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    .line 92
    :cond_d3
    add-int/lit8 v14, v14, 0x1

    goto :goto_7e

    .line 93
    .end local v16           #isAccountInDb:Z
    .end local v19           #syncable:Z
    :cond_d6
    const/16 v16, 0x0

    goto :goto_90

    .line 94
    .restart local v16       #isAccountInDb:Z
    :cond_d9
    const/16 v19, 0x0

    goto :goto_9a

    .line 108
    .end local v9           #account:Landroid/accounts/Account;
    .end local v10           #arr$:[Landroid/accounts/Account;
    .end local v14           #i$:I
    .end local v16           #isAccountInDb:Z
    .end local v17           #len$:I
    :cond_dc
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_144

    .line 109
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, i$:Ljava/util/Iterator;
    :goto_ea
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_144

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Map$Entry;

    .line 110
    .local v12, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "PicasaSync"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_11b

    .line 111
    const-string v2, "PicasaSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "remove account:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_11b
    invoke-interface {v12}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    :try_start_12a
    sget-object v2, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    const-string v4, "user_id=?"

    invoke-virtual {v1, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v2, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->USER_TABLE_NAME:Ljava/lang/String;

    const-string v4, "_id=?"

    invoke-virtual {v1, v2, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_13b
    .catchall {:try_start_12a .. :try_end_13b} :catchall_13f

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_ea

    :catchall_13f
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 116
    .end local v12           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v14           #i$:Ljava/util/Iterator;
    :cond_144
    return-void
.end method

.method public final syncUploadedPhotos(Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;Lcom/google/android/apps/plus/iu/UserEntry;)V
    .registers 15
    .parameter "context"
    .parameter "user"

    .prologue
    .line 149
    const-string v0, "PicasaSyncHelper.syncPhotosForUpload"

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v8

    .line 152
    .local v8, statsId:I
    :try_start_6
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->syncInterrupted()Z
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_5e

    move-result v0

    if-eqz v0, :cond_10

    .line 155
    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    .line 156
    :goto_f
    return-void

    .line 153
    :cond_10
    :try_start_10
    iget-object v9, p2, Lcom/google/android/apps/plus/iu/UserEntry;->account:Ljava/lang/String;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mDbHelper:Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_20
    .catchall {:try_start_10 .. :try_end_20} :catchall_5e

    :try_start_20
    sget-object v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->PHOTO_PROJECTION_ID_DATE:[Ljava/lang/String;

    const-string v3, "user_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p2, Lcom/google/android/apps/plus/iu/UserEntry;->id:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_38
    .catchall {:try_start_20 .. :try_end_38} :catchall_59

    move-result-object v2

    if-eqz v2, :cond_66

    :goto_3b
    :try_start_3b
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_63

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v1, 0x1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    new-instance v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$EntryMetadata;

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$EntryMetadata;-><init>(JJ)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_53
    .catchall {:try_start_3b .. :try_end_53} :catchall_54

    goto :goto_3b

    :catchall_54
    move-exception v1

    :try_start_55
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_59
    .catchall {:try_start_55 .. :try_end_59} :catchall_59

    :catchall_59
    move-exception v1

    :try_start_5a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
    :try_end_5e
    .catchall {:try_start_5a .. :try_end_5e} :catchall_5e

    .line 155
    :catchall_5e
    move-exception v0

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v0

    .line 153
    :cond_63
    :try_start_63
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_66
    .catchall {:try_start_63 .. :try_end_66} :catchall_59

    :cond_66
    :try_start_66
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-wide v4, p2, Lcom/google/android/apps/plus/iu/UserEntry;->id:J

    new-instance v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$1;

    move-object v2, p0

    move-object v3, v10

    move-object v6, v0

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$1;-><init>(Lcom/google/android/apps/plus/iu/PicasaSyncHelper;Ljava/util/ArrayList;JLandroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mEtagMap:Ljava/util/HashMap;

    invoke-virtual {v2, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v4, v3

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v11, v2

    move v2, v3

    move v3, v11

    :goto_87
    const/4 v5, 0x1

    if-gt v3, v5, :cond_a1

    iget-object v2, p1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->api:Lcom/google/android/apps/plus/iu/PicasaApi;

    invoke-virtual {v2, v9, v4, v1}, Lcom/google/android/apps/plus/iu/PicasaApi;->getUploadedPhotos(Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)I

    move-result v2

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->mEtagMap:Ljava/util/HashMap;

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-virtual {v5, v9, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v5, 0x2

    if-ne v2, v5, :cond_a1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->refreshAuthToken()V

    add-int/lit8 v3, v3, 0x1

    goto :goto_87

    :cond_a1
    move v1, v2

    packed-switch v1, :pswitch_data_e8

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a9
    :goto_a9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$EntryMetadata;

    iget-boolean v3, v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$EntryMetadata;->survived:Z

    if-nez v3, :cond_a9

    sget-object v3, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-wide v4, v1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$EntryMetadata;->id:J

    invoke-virtual {v3, v0, v4, v5}, Lcom/android/gallery3d/common/EntrySchema;->deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z

    iget-object v1, p1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->result:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v1, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v1, Landroid/content/SyncStats;->numDeletes:J

    goto :goto_a9

    :pswitch_cc
    iget-object v0, p1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->result:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_d7
    .catchall {:try_start_66 .. :try_end_d7} :catchall_5e

    .line 155
    :cond_d7
    :goto_d7
    :pswitch_d7
    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    goto/16 :goto_f

    .line 153
    :pswitch_dc
    :try_start_dc
    iget-object v0, p1, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->result:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v1, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, v0, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_e7
    .catchall {:try_start_dc .. :try_end_e7} :catchall_5e

    goto :goto_d7

    :pswitch_data_e8
    .packed-switch 0x1
        :pswitch_d7
        :pswitch_cc
        :pswitch_dc
    .end packed-switch
.end method
