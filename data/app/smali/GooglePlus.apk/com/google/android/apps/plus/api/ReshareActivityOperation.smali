.class public final Lcom/google/android/apps/plus/api/ReshareActivityOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "ReshareActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostActivityRequest;",
        "Lcom/google/api/services/plusi/model/PostActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private final mContent:Ljava/lang/String;

.field private final mReshareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "reshareId"
    .parameter "content"
    .parameter "audience"

    .prologue
    .line 48
    const-string v3, "postactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PostActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 55
    iput-object p5, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mReshareId:Ljava/lang/String;

    .line 56
    iput-object p6, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mContent:Ljava/lang/String;

    .line 57
    iput-object p7, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 58
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityResponse;

    .end local p1
    if-eqz p1, :cond_12

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    if-eqz v1, :cond_12

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_13

    :cond_12
    :goto_12
    return-void

    :cond_13
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mReshareId:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromItemId:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    goto :goto_17

    :cond_2e
    invoke-static {v4, v4, v4, v5, v5}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "DEFAULT"

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    goto :goto_12
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mReshareId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->resharedUpdateId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->updateText:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostActivityRequest;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    return-void
.end method
