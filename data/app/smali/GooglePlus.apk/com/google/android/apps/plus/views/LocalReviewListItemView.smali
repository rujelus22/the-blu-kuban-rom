.class public Lcom/google/android/apps/plus/views/LocalReviewListItemView;
.super Landroid/widget/RelativeLayout;
.source "LocalReviewListItemView.java"


# instance fields
.field private mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAuthorName:Landroid/widget/TextView;

.field private mIsFullText:Z

.field private mPublishDate:Landroid/widget/TextView;

.field private mRatingAspects:Landroid/widget/TextView;

.field private mReviewText:Landroid/widget/TextView;

.field private mTopBorder:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 2

    .prologue
    .line 59
    const v0, 0x7f0901ef

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mTopBorder:Landroid/view/View;

    .line 60
    const v0, 0x7f0901f0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    .line 61
    const v0, 0x7f0901f1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0901f2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f0901f3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0901f4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    .line 65
    return-void
.end method

.method public setAuthorAvatarOnClickListener(Landroid/view/View$OnClickListener;)V
    .registers 3
    .parameter "listener"

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    return-void
.end method

.method public setIsFullText(Z)V
    .registers 2
    .parameter "fullText"

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mIsFullText:Z

    .line 87
    return-void
.end method

.method public setReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .registers 12
    .parameter "review"

    .prologue
    const/4 v0, 0x0

    const/16 v9, 0x8

    const/4 v2, 0x0

    .line 76
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    if-eqz v1, :cond_37

    .line 77
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_cf

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileId:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 78
    :goto_19
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    if-eqz v3, :cond_db

    iget-object v3, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_db

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :cond_37
    :goto_37
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->publishDate:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_e2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :goto_49
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    if-eqz v1, :cond_e9

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    if-eqz v1, :cond_e9

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_e9

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    move-object v3, v0

    :goto_62
    if-eqz v3, :cond_12b

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v1, v2

    :goto_6a
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_ec

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_cb

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_cb

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    new-instance v6, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0f006e

    invoke-direct {v6, v7, v8}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v5, "\u00a0"

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0f006f

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v0, "\u00a0/\u00a03"

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0f0070

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_cb

    const-string v0, "  "

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_cb
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6a

    .line 77
    :cond_cf
    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_19

    .line 78
    :cond_db
    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_37

    .line 80
    :cond_e2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_49

    :cond_e9
    move-object v3, v0

    .line 81
    goto/16 :goto_62

    :cond_ec
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_125

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    :goto_fc
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->snippet:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mIsFullText:Z

    if-eqz v1, :cond_10c

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10c

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    :cond_10c
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_131

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v1, "\\<.*?>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    :goto_124
    return-void

    .line 81
    :cond_125
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_fc

    :cond_12b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_fc

    .line 82
    :cond_131
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_124
.end method

.method public setTopBorderVisible(Z)V
    .registers 4
    .parameter "visible"

    .prologue
    .line 68
    if-eqz p1, :cond_9

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mTopBorder:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 73
    :goto_8
    return-void

    .line 71
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mTopBorder:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_8
.end method
