.class public Lcom/google/android/apps/plus/phone/PostActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PostActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

.field private mShakeDetectorWasRunning:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private buildPostFragment(Lcom/google/android/apps/plus/content/EsAccount;)Z
    .registers 7
    .parameter "account"

    .prologue
    .line 242
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getPostFragmentArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 246
    .local v0, args:Landroid/os/Bundle;
    if-nez v0, :cond_a

    .line 247
    const/4 v1, 0x0

    .line 251
    :goto_9
    return v1

    .line 249
    :cond_a
    const-string v1, "post_tag"

    const-string v2, "account"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    new-instance v2, Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    const v4, 0x7f09019c

    invoke-virtual {v3, v4, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 251
    const/4 v1, 0x1

    goto :goto_9
.end method

.method private switchAccounts()V
    .registers 3

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 228
    .local v0, startIntent:Landroid/content/Intent;
    const-class v1, Lcom/google/android/apps/plus/phone/PostActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PostActivity;->startActivity(Landroid/content/Intent;)V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->finish()V

    .line 232
    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected getPostFragmentArguments()Landroid/os/Bundle;
    .registers 27

    .prologue
    .line 259
    new-instance v16, Landroid/os/Bundle;

    invoke-direct/range {v16 .. v16}, Landroid/os/Bundle;-><init>()V

    .line 261
    .local v16, args:Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v21

    .line 264
    .local v21, intent:Landroid/content/Intent;
    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    .line 265
    .local v15, action:Ljava/lang/String;
    const-string v2, "PostActivity"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 266
    const-string v2, "PostActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Intent action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :cond_2a
    const-string v2, "action"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v2, "android.intent.extra.TEXT"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6f

    .line 272
    const-string v2, "PostActivity"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_60

    .line 273
    const-string v2, "PostActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    EXTRA_TEXT: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "android.intent.extra.TEXT"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :cond_60
    const-string v2, "android.intent.extra.TEXT"

    const-string v3, "android.intent.extra.TEXT"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_6f
    const-string v2, "activity_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_ad

    .line 281
    const-string v2, "PostActivity"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9e

    .line 282
    const-string v2, "PostActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    EXTRA_ACTIVITY_ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "activity_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_9e
    const-string v2, "activity_id"

    const-string v3, "activity_id"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_ad
    const-string v2, "location"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e9

    .line 292
    const-string v2, "location"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v22

    check-cast v22, Lcom/google/android/apps/plus/content/DbLocation;

    .line 293
    .local v22, location:Lcom/google/android/apps/plus/content/DbLocation;
    const-string v2, "PostActivity"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e0

    .line 294
    const-string v2, "PostActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    EXTRA_LOCATION: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_e0
    const-string v2, "location"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 304
    .end local v22           #location:Lcom/google/android/apps/plus/content/DbLocation;
    :cond_e9
    const/16 v19, 0x0

    .line 305
    .local v19, droppedUrls:Z
    const/16 v25, 0x0

    .line 306
    .local v25, unsupported:Z
    const-string v2, "android.intent.extra.STREAM"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f6

    .line 307
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v17, attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d4

    .line 309
    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    .line 312
    .local v18, data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0xfa

    if-le v2, v3, :cond_138

    .line 313
    const v2, 0x7f080139

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/16 v5, 0xfa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/phone/PostActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 316
    const/16 v16, 0x0

    .line 382
    .end local v16           #args:Landroid/os/Bundle;
    .end local v17           #attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .end local v18           #data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    :cond_137
    :goto_137
    return-object v16

    .line 319
    .restart local v16       #args:Landroid/os/Bundle;
    .restart local v17       #attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .restart local v18       #data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    :cond_138
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, i$:Ljava/util/Iterator;
    :cond_13c
    :goto_13c
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1ed

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/os/Parcelable;

    .line 320
    .local v23, parcelable:Landroid/os/Parcelable;
    move-object/from16 v0, v23

    instance-of v2, v0, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_158

    .line 321
    check-cast v23, Lcom/google/android/apps/plus/api/MediaRef;

    .end local v23           #parcelable:Landroid/os/Parcelable;
    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13c

    .line 322
    .restart local v23       #parcelable:Landroid/os/Parcelable;
    :cond_158
    move-object/from16 v0, v23

    instance-of v2, v0, Landroid/net/Uri;

    if-eqz v2, :cond_13c

    move-object/from16 v7, v23

    .line 323
    check-cast v7, Landroid/net/Uri;

    .line 324
    .local v7, localUri:Landroid/net/Uri;
    invoke-static {v7}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_180

    .line 325
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13c

    .line 327
    :cond_180
    invoke-static {v7}, Lcom/google/android/apps/plus/util/GalleryUtils;->isGalleryContentUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_19b

    .line 328
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    .line 329
    .local v12, galleryUrl:Ljava/lang/String;
    new-instance v8, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13c

    .line 331
    .end local v12           #galleryUrl:Ljava/lang/String;
    :cond_19b
    const-string v2, "content"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c0

    .line 332
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_13c

    .line 334
    :cond_1c0
    const-string v2, "file"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d0

    .line 335
    const/16 v25, 0x1

    goto/16 :goto_13c

    .line 337
    :cond_1d0
    const/16 v19, 0x1

    goto/16 :goto_13c

    .line 342
    .end local v7           #localUri:Landroid/net/Uri;
    .end local v18           #data:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v20           #i$:Ljava/util/Iterator;
    .end local v23           #parcelable:Landroid/os/Parcelable;
    :cond_1d4
    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    .line 343
    .restart local v23       #parcelable:Landroid/os/Parcelable;
    move-object/from16 v0, v23

    instance-of v2, v0, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_22a

    .line 344
    check-cast v23, Lcom/google/android/apps/plus/api/MediaRef;

    .end local v23           #parcelable:Landroid/os/Parcelable;
    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_1ed
    :goto_1ed
    const-string v2, "android.intent.extra.STREAM"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 368
    .end local v17           #attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :cond_1f6
    const-string v2, "audience"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20f

    .line 369
    const-string v2, "audience"

    const-string v3, "audience"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 373
    :cond_20f
    if-eqz v19, :cond_137

    .line 375
    if-eqz v25, :cond_2a7

    .line 376
    const v2, 0x7f08013b

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    .line 380
    .local v24, toastText:Ljava/lang/String;
    :goto_21c
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_137

    .line 345
    .end local v24           #toastText:Ljava/lang/String;
    .restart local v17       #attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .restart local v23       #parcelable:Landroid/os/Parcelable;
    :cond_22a
    move-object/from16 v0, v23

    instance-of v2, v0, Landroid/net/Uri;

    if-eqz v2, :cond_1ed

    move-object/from16 v7, v23

    .line 346
    check-cast v7, Landroid/net/Uri;

    .line 347
    .restart local v7       #localUri:Landroid/net/Uri;
    invoke-static {v7}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_252

    .line 348
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1ed

    .line 350
    :cond_252
    invoke-static {v7}, Lcom/google/android/apps/plus/util/GalleryUtils;->isGalleryContentUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_26d

    .line 351
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    .line 352
    .restart local v12       #galleryUrl:Ljava/lang/String;
    new-instance v8, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const/4 v13, 0x0

    sget-object v14, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v8 .. v14}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1ed

    .line 354
    .end local v12           #galleryUrl:Ljava/lang/String;
    :cond_26d
    const-string v2, "content"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_292

    .line 355
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1ed

    .line 358
    :cond_292
    const-string v2, "file"

    invoke-virtual {v7}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a4

    const/16 v25, 0x1

    .line 359
    :goto_2a0
    const/16 v19, 0x1

    goto/16 :goto_1ed

    .line 358
    :cond_2a4
    const/16 v25, 0x0

    goto :goto_2a0

    .line 378
    .end local v7           #localUri:Landroid/net/Uri;
    .end local v17           #attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .end local v23           #parcelable:Landroid/os/Parcelable;
    :cond_2a7
    const v2, 0x7f08013a

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v24

    .restart local v24       #toastText:Ljava/lang/String;
    goto/16 :goto_21c
.end method

.method protected final getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 452
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f080145

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 462
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected getViewId()I
    .registers 2

    .prologue
    .line 117
    const v0, 0x7f030092

    return v0
.end method

.method public final hideInsertCameraPhotoDialog()V
    .registers 2

    .prologue
    .line 472
    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PostActivity;->dismissDialog(I)V

    .line 473
    return-void
.end method

.method public invalidateMenu()V
    .registers 2

    .prologue
    .line 216
    .line 217
    const v0, 0x7f10001b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PostActivity;->createTitlebarButtons(I)V

    .line 221
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 3
    .parameter "fragment"

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 123
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/PostFragment;

    if-eqz v0, :cond_b

    .line 124
    check-cast p1, Lcom/google/android/apps/plus/fragments/PostFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    .line 126
    :cond_b
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->onDiscard()V

    .line 210
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 427
    packed-switch p2, :pswitch_data_1a

    .line 447
    :goto_3
    return-void

    .line 430
    :pswitch_4
    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getLocationSettingActivityIntent()Landroid/content/Intent;

    move-result-object v0

    .line 431
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PostActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 437
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_c
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    goto :goto_3

    .line 443
    :pswitch_13
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLocationDialogSeenPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    goto :goto_3

    .line 427
    :pswitch_data_1a
    .packed-switch -0x3
        :pswitch_13
        :pswitch_c
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getViewId()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PostActivity;->setContentView(I)V

    .line 70
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PostActivity;->showTitlebar(Z)V

    .line 80
    if-nez p1, :cond_5c

    .line 81
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 83
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "account"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 84
    const-string v3, "account"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 85
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PostActivity;->buildPostFragment(Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v3

    if-nez v3, :cond_47

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->finish()V

    .line 114
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_2d
    :goto_2d
    return-void

    .line 91
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_2e
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 92
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_58

    .line 93
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v3

    if-eqz v3, :cond_44

    .line 94
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PostActivity;->buildPostFragment(Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v3

    if-nez v3, :cond_47

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->finish()V

    goto :goto_2d

    .line 99
    :cond_44
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->switchAccounts()V

    .line 110
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_47
    :goto_47
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v2

    .line 111
    .local v2, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v2, :cond_2d

    .line 112
    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mShakeDetectorWasRunning:Z

    goto :goto_2d

    .line 102
    .end local v2           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_58
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->switchAccounts()V

    goto :goto_47

    .line 106
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_5c
    const-string v3, "account"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_47
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter "dialogId"
    .parameter "args"

    .prologue
    .line 400
    sparse-switch p1, :sswitch_data_46

    .line 422
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 402
    :sswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 403
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v1, 0x7f080169

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 404
    const v1, 0x7f0801c6

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 405
    const v1, 0x7f0801c8

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 406
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_4

    .line 410
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_21
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 411
    .restart local v0       #builder:Landroid/app/AlertDialog$Builder;
    const v1, 0x7f08019c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 412
    const v1, 0x7f08019d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 413
    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 414
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 415
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_4

    .line 419
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_41
    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_4

    .line 400
    :sswitch_data_46
    .sparse-switch
        0x1bfb7a8 -> :sswitch_5
        0x1d71d84 -> :sswitch_21
        0x7f09003e -> :sswitch_41
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10001b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 163
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 147
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    .line 150
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_14

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 152
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_14

    .line 153
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 157
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_14
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 190
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_16

    .line 202
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 192
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->onDiscard()V

    goto :goto_9

    .line 197
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->post()Z

    goto :goto_9

    .line 190
    :sswitch_data_16
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f09029b -> :sswitch_10
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 168
    const v1, 0x7f09029b

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 169
    .local v0, postItem:Landroid/view/MenuItem;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 170
    const/4 v1, 0x1

    return v1
.end method

.method protected onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 6
    .parameter "menu"

    .prologue
    .line 182
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1d

    .line 183
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 184
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f09029b

    if-ne v2, v3, :cond_1b

    const/4 v2, 0x1

    :goto_15
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 184
    :cond_1b
    const/4 v2, 0x0

    goto :goto_15

    .line 186
    .end local v1           #item:Landroid/view/MenuItem;
    :cond_1d
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 143
    :cond_b
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 130
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_e

    .line 133
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 135
    :cond_e
    return-void
.end method

.method protected onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    if-eqz v0, :cond_9

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->onDiscard()V

    .line 178
    :cond_9
    return-void
.end method

.method public final showInsertCameraPhotoDialog()V
    .registers 2

    .prologue
    .line 467
    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PostActivity;->showDialog(I)V

    .line 468
    return-void
.end method
