.class public final Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;
.super Lcom/google/android/apps/plus/api/PlusOneOperation;
.source "ProfileOptimisticPlusOneOperation.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "profileId"
    .parameter "isPlusOne"

    .prologue
    .line 30
    const-string v5, "ENTITY"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 31
    return-void
.end method


# virtual methods
.method protected final onFailure()V
    .registers 5

    .prologue
    .line 45
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mIsPlusOne:Z

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_b
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->changePlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Z

    .line 46
    return-void

    .line 45
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected final onPopulateRequest()V
    .registers 5

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->changePlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Z

    .line 37
    return-void
.end method
