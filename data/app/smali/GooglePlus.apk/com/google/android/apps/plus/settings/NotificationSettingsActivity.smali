.class public Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "NotificationSettingsActivity.java"


# instance fields
.field private mGetNotificationsRequestId:Ljava/lang/Integer;

.field private mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->updatedEnabledStates(Z)V

    return-void
.end method

.method private setupPreferences()V
    .registers 22

    .prologue
    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v14

    .line 148
    .local v14, prefScreen:Landroid/preference/PreferenceScreen;
    if-eqz v14, :cond_9

    .line 149
    invoke-virtual {v14}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 152
    :cond_9
    const v20, 0x7f05000c

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->addPreferencesFromResource(I)V

    .line 153
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v14

    .line 155
    const v20, 0x7f080008

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    .line 157
    .local v11, notify:Landroid/preference/CheckBoxPreference;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    .line 158
    .local v4, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v4, :cond_10a

    .line 159
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 164
    :goto_39
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->hookMasterSwitch(Landroid/preference/PreferenceCategory;Landroid/preference/CheckBoxPreference;)V

    .line 165
    new-instance v20, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;

    invoke-direct/range {v20 .. v21}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 173
    const v20, 0x7f08000a

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 174
    .local v10, key:Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v16

    .line 175
    .local v16, ringtonePreference:Landroid/preference/Preference;
    const v20, 0x7f08000b

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 177
    .local v7, defaultRingtonePath:Ljava/lang/String;
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10, v7}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getRingtoneName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 178
    .local v15, ringtoneName:Ljava/lang/String;
    new-instance v20, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v10, v7}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/BaseSettingsActivity;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 180
    if-eqz v15, :cond_89

    .line 181
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 184
    :cond_89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    move-object/from16 v20, v0

    if-eqz v20, :cond_116

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    move-object/from16 v18, v0

    .line 186
    .local v18, settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    const/4 v8, 0x0

    .local v8, i:I
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getCategoriesCount()I

    move-result v5

    .line 187
    .local v5, categoriesCount:I
    :goto_9c
    if-ge v8, v5, :cond_163

    .line 188
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getCategory(I)Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    move-result-object v6

    .line 190
    .local v6, category:Lcom/google/android/apps/plus/content/NotificationSettingsCategory;
    new-instance v13, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 191
    .local v13, prefCategory:Landroid/preference/PreferenceCategory;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getDescription()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 192
    add-int/lit8 v20, v8, 0x2

    move/from16 v0, v20

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    .line 194
    invoke-virtual {v14, v13}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 196
    const/4 v9, 0x0

    .local v9, j:I
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getSettingsCount()I

    move-result v19

    .local v19, settingsCount:I
    :goto_c9
    move/from16 v0, v19

    if-ge v9, v0, :cond_113

    .line 198
    invoke-virtual {v6, v9}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getSetting(I)Lcom/google/android/apps/plus/content/NotificationSetting;

    move-result-object v17

    .line 199
    .local v17, setting:Lcom/google/android/apps/plus/content/NotificationSetting;
    new-instance v12, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    .line 201
    .local v12, pref:Landroid/preference/CheckBoxPreference;
    const v20, 0x7f030054

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setLayoutResource(I)V

    .line 202
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/NotificationSetting;->getDescription()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    .line 203
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/NotificationSetting;->isEnabled()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 204
    new-instance v20, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSetting;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 230
    invoke-virtual {v13, v12}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 197
    add-int/lit8 v9, v9, 0x1

    goto :goto_c9

    .line 161
    .end local v5           #categoriesCount:I
    .end local v6           #category:Lcom/google/android/apps/plus/content/NotificationSettingsCategory;
    .end local v7           #defaultRingtonePath:Ljava/lang/String;
    .end local v8           #i:I
    .end local v9           #j:I
    .end local v10           #key:Ljava/lang/String;
    .end local v12           #pref:Landroid/preference/CheckBoxPreference;
    .end local v13           #prefCategory:Landroid/preference/PreferenceCategory;
    .end local v15           #ringtoneName:Ljava/lang/String;
    .end local v16           #ringtonePreference:Landroid/preference/Preference;
    .end local v17           #setting:Lcom/google/android/apps/plus/content/NotificationSetting;
    .end local v18           #settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .end local v19           #settingsCount:I
    :cond_10a
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_39

    .line 187
    .restart local v5       #categoriesCount:I
    .restart local v6       #category:Lcom/google/android/apps/plus/content/NotificationSettingsCategory;
    .restart local v7       #defaultRingtonePath:Ljava/lang/String;
    .restart local v8       #i:I
    .restart local v9       #j:I
    .restart local v10       #key:Ljava/lang/String;
    .restart local v13       #prefCategory:Landroid/preference/PreferenceCategory;
    .restart local v15       #ringtoneName:Ljava/lang/String;
    .restart local v16       #ringtonePreference:Landroid/preference/Preference;
    .restart local v18       #settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .restart local v19       #settingsCount:I
    :cond_113
    add-int/lit8 v8, v8, 0x1

    goto :goto_9c

    .line 234
    .end local v5           #categoriesCount:I
    .end local v6           #category:Lcom/google/android/apps/plus/content/NotificationSettingsCategory;
    .end local v8           #i:I
    .end local v9           #j:I
    .end local v13           #prefCategory:Landroid/preference/PreferenceCategory;
    .end local v18           #settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .end local v19           #settingsCount:I
    :cond_116
    new-instance v13, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 235
    .restart local v13       #prefCategory:Landroid/preference/PreferenceCategory;
    const v20, 0x7f0800a0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 236
    const/16 v20, 0x7d0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    .line 237
    invoke-virtual {v14, v13}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 239
    new-instance v12, Landroid/preference/Preference;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 240
    .local v12, pref:Landroid/preference/Preference;
    const v20, 0x7f030054

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 241
    const v20, 0x7f0800a1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 242
    new-instance v20, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$4;

    invoke-direct/range {v20 .. v21}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$4;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 251
    invoke-virtual {v13, v12}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 254
    .end local v12           #pref:Landroid/preference/Preference;
    .end local v13           #prefCategory:Landroid/preference/PreferenceCategory;
    :cond_163
    invoke-virtual {v11}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->updatedEnabledStates(Z)V

    .line 255
    return-void
.end method

.method private updatedEnabledStates(Z)V
    .registers 8
    .parameter "enabled"

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    .line 259
    .local v4, prefScreen:Landroid/preference/PreferenceScreen;
    const/4 v1, 0x0

    .local v1, i:I
    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    .local v0, categoryCount:I
    :goto_9
    if-ge v1, v0, :cond_15

    .line 260
    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    .line 261
    .local v3, pref:Landroid/preference/Preference;
    invoke-virtual {v3, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 259
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 265
    .end local v3           #pref:Landroid/preference/Preference;
    :cond_15
    const v5, 0x7f080008

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    .line 267
    .local v2, notificationsPref:Landroid/preference/Preference;
    if-eqz v2, :cond_26

    .line 268
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 270
    :cond_26
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 69
    if-eqz p1, :cond_2b

    .line 70
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 71
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    .line 75
    :cond_19
    const-string v0, "notification_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 76
    const-string v0, "notification_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/NotificationSettingsData;

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    .line 80
    :cond_2b
    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter "dialogId"
    .parameter "args"

    .prologue
    const/4 v2, 0x0

    .line 133
    packed-switch p1, :pswitch_data_1c

    .line 143
    const/4 v0, 0x0

    :goto_5
    return-object v0

    .line 135
    :pswitch_6
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 136
    .local v0, dialog:Landroid/app/ProgressDialog;
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 137
    const v1, 0x7f080096

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 138
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_5

    .line 133
    :pswitch_data_1c
    .packed-switch 0x7f09003f
        :pswitch_6
    .end packed-switch
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onPause()V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 116
    return-void
.end method

.method public onResume()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f09003f

    .line 84
    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 89
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    if-nez v2, :cond_26

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    if-nez v2, :cond_22

    .line 91
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->getNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    .line 92
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->showDialog(I)V

    .line 110
    :cond_21
    :goto_21
    return-void

    .line 94
    :cond_22
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V

    goto :goto_21

    .line 97
    :cond_26
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_21

    .line 98
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 99
    .local v1, result:Lcom/google/android/apps/plus/service/ServiceResult;
    if-eqz v1, :cond_4f

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_4f

    .line 100
    iput-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    .line 101
    iput-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V

    .line 103
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->dismissDialog(I)V

    goto :goto_21

    .line 106
    :cond_4f
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->getNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    goto :goto_21
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 123
    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 126
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    if-eqz v0, :cond_1d

    .line 127
    const-string v0, "notification_settings"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 129
    :cond_1d
    return-void
.end method
