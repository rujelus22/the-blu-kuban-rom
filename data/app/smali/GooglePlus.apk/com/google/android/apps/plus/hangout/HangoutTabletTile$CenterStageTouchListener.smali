.class final Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;
.super Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;
.source "HangoutTabletTile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CenterStageTouchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter "context"

    .prologue
    .line 413
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    .line 414
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;-><init>(Landroid/content/Context;)V

    .line 415
    return-void
.end method


# virtual methods
.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 11
    .parameter "startEvent"
    .parameter "endEvent"
    .parameter "velocityX"
    .parameter "velocityY"

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 467
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getHeight()I

    move-result v0

    .line 468
    .local v0, height:I
    div-int/lit8 v2, v0, 0x2

    .line 469
    .local v2, topThreshold:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    .line 474
    .local v1, startY:I
    cmpl-float v4, p4, v5

    if-lez v4, :cond_31

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v4, p4, v4

    if-lez v4, :cond_31

    .line 476
    if-lt v1, v2, :cond_29

    .line 477
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideChild(Landroid/view/View;)V

    .line 505
    :goto_28
    return v3

    .line 482
    :cond_29
    if-gt v1, v2, :cond_31

    .line 483
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    goto :goto_28

    .line 491
    :cond_31
    cmpg-float v4, p4, v5

    if-gez v4, :cond_54

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    neg-float v4, v4

    cmpg-float v4, p4, v4

    if-gez v4, :cond_54

    .line 493
    if-lt v1, v2, :cond_4c

    .line 494
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showChild(Landroid/view/View;)V

    goto :goto_28

    .line 499
    :cond_4c
    if-gt v1, v2, :cond_54

    .line 500
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    goto :goto_28

    .line 505
    :cond_54
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v3

    goto :goto_28
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "e"

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 428
    const/4 v0, 0x1

    return v0
.end method

.method public final onTwoPointerSwipe(Landroid/view/MotionEvent$PointerCoords;Landroid/view/MotionEvent$PointerCoords;FF)Z
    .registers 11
    .parameter "pointerStart"
    .parameter "pointerEnd"
    .parameter "xVelocity"
    .parameter "yVelocity"

    .prologue
    const/4 v3, 0x1

    .line 439
    iget v4, p1, Landroid/view/MotionEvent$PointerCoords;->y:F

    iget v5, p2, Landroid/view/MotionEvent$PointerCoords;->y:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x437a

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3d

    .line 441
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getWidth()I

    move-result v0

    .line 442
    .local v0, width:I
    iget v4, p1, Landroid/view/MotionEvent$PointerCoords;->x:F

    float-to-int v2, v4

    .line 443
    .local v2, xStart:I
    iget v4, p2, Landroid/view/MotionEvent$PointerCoords;->x:F

    float-to-int v1, v4

    .line 444
    .local v1, xEnd:I
    if-ge v2, v1, :cond_2e

    .line 446
    div-int/lit8 v4, v0, 0x2

    if-ge v2, v4, :cond_3d

    mul-int/lit8 v4, v0, 0x7

    div-int/lit8 v4, v4, 0x8

    if-le v1, v4, :cond_3d

    .line 447
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Z)V

    .line 458
    .end local v0           #width:I
    .end local v1           #xEnd:I
    .end local v2           #xStart:I
    :goto_2d
    return v3

    .line 452
    .restart local v0       #width:I
    .restart local v1       #xEnd:I
    .restart local v2       #xStart:I
    :cond_2e
    div-int/lit8 v4, v0, 0x2

    if-le v2, v4, :cond_3d

    div-int/lit8 v4, v0, 0x8

    if-ge v1, v4, :cond_3d

    .line 453
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Z)V

    goto :goto_2d

    .line 458
    .end local v0           #width:I
    .end local v1           #xEnd:I
    .end local v2           #xStart:I
    :cond_3d
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/TwoPointerGestureDetector;->onTwoPointerSwipe(Landroid/view/MotionEvent$PointerCoords;Landroid/view/MotionEvent$PointerCoords;FF)Z

    move-result v3

    goto :goto_2d
.end method
