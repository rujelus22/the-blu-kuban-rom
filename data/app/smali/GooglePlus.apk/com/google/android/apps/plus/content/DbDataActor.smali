.class public final Lcom/google/android/apps/plus/content/DbDataActor;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbDataActor.java"


# direct methods
.method public static deserializeDataActorList([B)Ljava/util/List;
    .registers 7
    .parameter "array"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    if-nez p0, :cond_4

    .line 65
    const/4 v3, 0x0

    .line 74
    :cond_3
    return-object v3

    .line 68
    :cond_4
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 69
    .local v0, bb:Ljava/nio/ByteBuffer;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v3, returnList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 71
    .local v2, items:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_12
    if-ge v1, v2, :cond_3

    .line 72
    new-instance v4, Lcom/google/api/services/plusi/model/DataActor;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/DataActor;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataActor;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataActor;->gender:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataActor;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataActor;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataActor;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataActor;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataActor;->profileType:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbDataActor;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/api/services/plusi/model/DataActor;->profileUrl:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_12
.end method

.method public static serializeDataActorList(Ljava/util/List;)[B
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    .local p0, actorList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 46
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 48
    .local v2, dos:Ljava/io/DataOutputStream;
    if-nez p0, :cond_e

    .line 49
    const/4 v4, 0x0

    .line 59
    :goto_d
    return-object v4

    .line 52
    :cond_e
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 53
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_19
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataActor;

    .line 54
    .local v0, actor:Lcom/google/api/services/plusi/model/DataActor;
    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->gender:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbDataActor;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbDataActor;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbDataActor;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbDataActor;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->profileType:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbDataActor;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->profileUrl:Ljava/lang/String;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/content/DbDataActor;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    goto :goto_19

    .line 57
    .end local v0           #actor:Lcom/google/api/services/plusi/model/DataActor;
    :cond_44
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 58
    .local v4, result:[B
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    goto :goto_d
.end method
