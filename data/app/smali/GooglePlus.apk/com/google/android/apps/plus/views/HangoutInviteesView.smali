.class public Lcom/google/android/apps/plus/views/HangoutInviteesView;
.super Landroid/widget/FrameLayout;
.source "HangoutInviteesView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;
    }
.end annotation


# static fields
.field private static final INVITEE_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mCircleLogoView:Landroid/view/View;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCirclesView:Landroid/widget/TextView;

.field private mInviteeId:Ljava/lang/String;

.field private mInvitees:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;"
        }
    .end annotation
.end field

.field private mMultipleInviteesContainer:Landroid/widget/LinearLayout;

.field private mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

.field private mNameView:Landroid/widget/TextView;

.field private mPackedCircleIds:Ljava/lang/String;

.field private final mPersonLoaderCallbacks:Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

.field private mSingleInviteeView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->INVITEE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 70
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    .line 95
    const v0, 0x7f03003d

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->inflate(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->addView(Landroid/view/View;)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->createMultipleInviteesView()Landroid/widget/HorizontalScrollView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->addView(Landroid/view/View;)V

    .line 98
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    .line 99
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mNameView:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCirclesView:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mSingleInviteeView:Landroid/view/View;

    .line 102
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleLogoView:Landroid/view/View;

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    .line 95
    const v0, 0x7f03003d

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->inflate(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->addView(Landroid/view/View;)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->createMultipleInviteesView()Landroid/widget/HorizontalScrollView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->addView(Landroid/view/View;)V

    .line 98
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    .line 99
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mNameView:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCirclesView:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mSingleInviteeView:Landroid/view/View;

    .line 102
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleLogoView:Landroid/view/View;

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    .line 62
    new-instance v0, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    .line 95
    const v0, 0x7f03003d

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->inflate(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->addView(Landroid/view/View;)V

    .line 96
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->createMultipleInviteesView()Landroid/widget/HorizontalScrollView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->addView(Landroid/view/View;)V

    .line 98
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    .line 99
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mNameView:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCirclesView:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mSingleInviteeView:Landroid/view/View;

    .line 102
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleLogoView:Landroid/view/View;

    .line 92
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/HangoutInviteesView;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPackedCircleIds:Ljava/lang/String;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCirclesView:Landroid/widget/TextView;

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleLogoView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCirclesView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPackedCircleIds:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->getCircleNamesForPackedIds(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_27
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/HangoutInviteesView;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/HangoutInviteesView;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInviteeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .registers 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->INVITEE_PROJECTION:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/views/HangoutInviteesView;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPackedCircleIds:Ljava/lang/String;

    return-object p1
.end method

.method private createMultipleInviteesView()Landroid/widget/HorizontalScrollView;
    .registers 7

    .prologue
    const/4 v5, -0x1

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 107
    .local v1, context:Landroid/content/Context;
    new-instance v3, Landroid/widget/HorizontalScrollView;

    invoke-direct {v3, v1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    .line 108
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 110
    .local v2, layoutParams:Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3, v2}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    const v4, 0x7f0a0089

    invoke-virtual {v3, v4}, Landroid/widget/HorizontalScrollView;->setBackgroundResource(I)V

    .line 112
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 113
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesContainer:Landroid/widget/LinearLayout;

    .line 114
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v0, v3, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 116
    .local v0, containerLayoutParams:Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4, v0}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    return-object v3
.end method

.method private static getGaiaId(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;
    .registers 3
    .parameter "person"

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    :goto_17
    return-object v0

    :cond_18
    const-string v0, ""

    goto :goto_17
.end method

.method private inflate(I)Landroid/view/View;
    .registers 5
    .parameter "layoutResourceId"

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03003d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAvatarCount()I
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public setInvitees(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 15
    .parameter "invitees"
    .parameter "account"

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 156
    .local v0, context:Landroid/content/Context;
    iput-object p2, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 158
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v8

    array-length v9, v8

    const/4 v7, 0x0

    :goto_11
    if-ge v7, v9, :cond_31

    aget-object v10, v8, v7

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2e

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_2e

    iget-object v11, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2e
    add-int/lit8 v7, v7, 0x1

    goto :goto_11

    .line 160
    :cond_31
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_a6

    .line 162
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v7}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getGaiaId(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInviteeId:Ljava/lang/String;

    .line 164
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mSingleInviteeView:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 165
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 166
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 167
    .local v6, name:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInviteeId:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    .line 171
    .local v4, loaderManager:Landroid/support/v4/app/LoaderManager;
    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mPersonLoaderCallbacks:Lcom/google/android/apps/plus/views/HangoutInviteesView$PersonLoaderCallbacks;

    invoke-virtual {v4, v7, v8, v9}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 173
    new-instance v7, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v7, v0, v4, v8}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 174
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 175
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    new-instance v8, Lcom/google/android/apps/plus/views/HangoutInviteesView$1;

    invoke-direct {v8, p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView$1;-><init>(Lcom/google/android/apps/plus/views/HangoutInviteesView;)V

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 192
    .end local v4           #loaderManager:Landroid/support/v4/app/LoaderManager;
    .end local v6           #name:Ljava/lang/String;
    :cond_a5
    return-void

    .line 181
    :cond_a6
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_a5

    .line 183
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mSingleInviteeView:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 184
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 185
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 186
    const/4 v2, 0x0

    .local v2, i:I
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v5

    .local v5, n:I
    :goto_c8
    if-ge v2, v5, :cond_a5

    .line 187
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mInvitees:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/PersonData;

    .line 188
    .local v3, invitee:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v3}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getGaiaId(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, gaiaId:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesContainer:Landroid/widget/LinearLayout;

    new-instance v8, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0d00da

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v10, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v11, 0x7f0d00d5

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, v10, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v8, v10}, Lcom/google/android/apps/plus/views/AvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 186
    add-int/lit8 v2, v2, 0x1

    goto :goto_c8
.end method

.method public setName(Ljava/lang/String;)V
    .registers 4
    .parameter "name"

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mNameView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method

.method public setVisibility(I)V
    .registers 4
    .parameter "visibility"

    .prologue
    const/16 v1, 0x8

    .line 215
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 216
    if-ne p1, v1, :cond_11

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mSingleInviteeView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutInviteesView;->mMultipleInviteesView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 220
    :cond_11
    return-void
.end method
