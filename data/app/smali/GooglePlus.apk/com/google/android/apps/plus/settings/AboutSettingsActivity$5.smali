.class final Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;
.super Ljava/lang/Object;
.source "AboutSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 9
    .parameter "preference"

    .prologue
    .line 125
    iget-object v3, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 126
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_17

    .line 127
    iget-object v3, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 128
    .local v1, context:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    .line 129
    .local v2, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_TOS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v0, v3, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 132
    .end local v1           #context:Landroid/content/Context;
    .end local v2           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_17
    iget-object v3, p0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;->this$0:Lcom/google/android/apps/plus/settings/AboutSettingsActivity;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->access$100()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->startExternalActivity(Landroid/content/Intent;)V

    .line 133
    const/4 v3, 0x1

    return v3
.end method
