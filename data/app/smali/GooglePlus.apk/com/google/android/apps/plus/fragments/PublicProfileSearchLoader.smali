.class public final Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PublicProfileSearchLoader.java"


# static fields
.field public static final ABORTED:Landroid/database/MatrixCursor;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mIncludePlusPages:Z

.field private final mMinQueryLength:I

.field private volatile mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

.field private final mProjection:[Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;

.field private final mToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 41
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->ABORTED:Landroid/database/MatrixCursor;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "projection"
    .parameter "query"
    .parameter "minQueryLength"
    .parameter "includePlusPages"
    .parameter "filterNullGaiaIds"
    .parameter "token"

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mIncludePlusPages:Z

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 66
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    .line 68
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mMinQueryLength:I

    .line 69
    iput-boolean p6, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mIncludePlusPages:Z

    .line 70
    if-eqz p7, :cond_1b

    const-string v0, "gaia_id IS NOT NULL"

    :goto_15
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->setSelection(Ljava/lang/String;)V

    .line 71
    iput-object p8, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    .line 72
    return-void

    .line 70
    :cond_1b
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private abort()V
    .registers 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    .line 187
    .local v0, op:Lcom/google/android/apps/plus/network/HttpOperation;
    if-eqz v0, :cond_7

    .line 188
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->abort()V

    .line 190
    :cond_7
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    .line 191
    return-void
.end method


# virtual methods
.method public final cancelLoad()Z
    .registers 2

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->abort()V

    .line 174
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->cancelLoad()Z

    move-result v0

    return v0
.end method

.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 25

    .prologue
    .line 83
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mMinQueryLength:I

    if-ge v3, v4, :cond_22

    .line 84
    :cond_18
    new-instance v11, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v11, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 165
    :cond_21
    :goto_21
    return-object v11

    .line 87
    :cond_22
    new-instance v2, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mQuery:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mIncludePlusPages:Z

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 89
    .local v2, op:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    .line 91
    :try_start_41
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->start()V

    .line 92
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->isAborted()Z

    move-result v3

    if-eqz v3, :cond_52

    .line 93
    sget-object v11, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->ABORTED:Landroid/database/MatrixCursor;
    :try_end_4c
    .catchall {:try_start_41 .. :try_end_4c} :catchall_64

    .line 96
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    goto :goto_21

    :cond_52
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    .line 99
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_6b

    .line 100
    const-string v3, "PublicProfileSearch"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->logError(Ljava/lang/String;)V

    .line 101
    const/4 v11, 0x0

    goto :goto_21

    .line 96
    :catchall_64
    move-exception v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mOperation:Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;

    throw v3

    .line 104
    :cond_6b
    new-instance v11, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-direct {v11, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 106
    .local v11, cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->getPeopleSearchResults()Ljava/util/List;

    move-result-object v18

    .line 108
    .local v18, results:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/PeopleResult;>;"
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->getContinuationToken()Ljava/lang/String;

    move-result-object v23

    .line 109
    .local v23, token:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 110
    .local v16, resources:Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    new-array v12, v3, [Ljava/lang/Object;

    .line 111
    .local v12, firstRow:[Ljava/lang/Object;
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    aput-object v4, v12, v3

    .line 112
    const/4 v3, 0x1

    aput-object v23, v12, v3

    .line 113
    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 115
    if-eqz v18, :cond_e4

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v21

    .line 116
    .local v21, size:I
    :goto_9e
    const/16 v20, 0x0

    .local v20, rowIndex:I
    :goto_a0
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_21

    .line 117
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/api/services/plusi/model/PeopleResult;

    .line 118
    .local v17, result:Lcom/google/api/services/plusi/model/PeopleResult;
    move-object/from16 v0, v17

    iget-object v15, v0, Lcom/google/api/services/plusi/model/PeopleResult;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    .line 119
    .local v15, properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/api/services/plusi/model/PeopleResult;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    .line 120
    .local v14, memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    if-eqz v14, :cond_190

    if-eqz v15, :cond_190

    .line 121
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v19, v0

    .line 125
    .local v19, row:[Ljava/lang/Object;
    const/4 v13, 0x0

    .local v13, j:I
    :goto_c6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    array-length v3, v3

    if-ge v13, v3, :cond_18b

    .line 126
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mProjection:[Ljava/lang/String;

    aget-object v10, v3, v13

    .line 127
    .local v10, column:Ljava/lang/String;
    const-string v3, "_id"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e7

    .line 128
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v19, v13

    .line 125
    :cond_e1
    :goto_e1
    add-int/lit8 v13, v13, 0x1

    goto :goto_c6

    .line 115
    .end local v10           #column:Ljava/lang/String;
    .end local v13           #j:I
    .end local v14           #memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    .end local v15           #properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    .end local v17           #result:Lcom/google/api/services/plusi/model/PeopleResult;
    .end local v19           #row:[Ljava/lang/Object;
    .end local v20           #rowIndex:I
    .end local v21           #size:I
    :cond_e4
    const/16 v21, 0x0

    goto :goto_9e

    .line 129
    .restart local v10       #column:Ljava/lang/String;
    .restart local v13       #j:I
    .restart local v14       #memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    .restart local v15       #properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    .restart local v17       #result:Lcom/google/api/services/plusi/model/PeopleResult;
    .restart local v19       #row:[Ljava/lang/Object;
    .restart local v20       #rowIndex:I
    .restart local v21       #size:I
    :cond_e7
    const-string v3, "gaia_id"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f4

    .line 130
    iget-object v3, v14, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v3, v19, v13

    goto :goto_e1

    .line 131
    :cond_f4
    const-string v3, "person_id"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_110

    .line 132
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v19, v13

    goto :goto_e1

    .line 133
    :cond_110
    const-string v3, "name"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11d

    .line 134
    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    aput-object v3, v19, v13

    goto :goto_e1

    .line 135
    :cond_11d
    const-string v3, "profile_type"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13e

    .line 136
    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->entityInfo:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;

    if-eqz v3, :cond_136

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->entityInfo:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;->type:Ljava/lang/Integer;

    if-eqz v3, :cond_136

    .line 137
    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->entityInfo:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesEntityInfo;->type:Ljava/lang/Integer;

    aput-object v3, v19, v13

    goto :goto_e1

    .line 139
    :cond_136
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v19, v13

    goto :goto_e1

    .line 141
    :cond_13e
    const-string v3, "avatar"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14f

    .line 142
    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->photoUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v19, v13

    goto :goto_e1

    .line 143
    :cond_14f
    const-string v3, "snippet"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e1

    .line 144
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PeopleResult;->snippetHtml:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 145
    .local v22, snippet:Ljava/lang/String;
    if-nez v22, :cond_17d

    .line 146
    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    if-eqz v3, :cond_186

    .line 147
    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    if-eqz v3, :cond_181

    .line 148
    const v3, 0x7f0803fc

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 159
    :cond_17d
    :goto_17d
    aput-object v22, v19, v13

    goto/16 :goto_e1

    .line 152
    :cond_181
    iget-object v0, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_17d

    .line 155
    :cond_186
    iget-object v0, v15, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    move-object/from16 v22, v0

    goto :goto_17d

    .line 162
    .end local v10           #column:Ljava/lang/String;
    .end local v22           #snippet:Ljava/lang/String;
    :cond_18b
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 116
    .end local v13           #j:I
    .end local v19           #row:[Ljava/lang/Object;
    :cond_190
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_a0
.end method

.method public final getToken()Ljava/lang/String;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method public final onAbandon()V
    .registers 1

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->abort()V

    .line 183
    return-void
.end method
