.class public Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;
.super Landroid/support/v4/app/Fragment;
.source "OutOfBoxFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/oob/ActionCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/OutOfBoxFragment$OobEsServiceListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final DIALOG_IDS:[Ljava/lang/String;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

.field private final mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

.field private mOobFields:Landroid/view/ViewGroup;

.field private mOutOfBoxDialogInflater:Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

.field private mOutOfBoxInflater:Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

.field private mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mSignUpLayout:Landroid/view/ViewGroup;

.field private mUpgradeOrigin:Ljava/lang/String;

.field private mViewSwitcher:Landroid/widget/ViewSwitcher;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    const-class v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_25

    move v0, v1

    :goto_b
    sput-boolean v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->$assertionsDisabled:Z

    .line 80
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v3, "sending"

    aput-object v3, v0, v2

    const-string v2, "net_failure"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "event"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "server_error"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->DIALOG_IDS:[Ljava/lang/String;

    return-void

    :cond_25
    move v0, v2

    .line 56
    goto :goto_b
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 88
    new-instance v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment$OobEsServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment$OobEsServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 109
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private close()V
    .registers 3

    .prologue
    .line 555
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 557
    return-void
.end method

.method public static createInitialTag()Ljava/lang/String;
    .registers 1

    .prologue
    .line 611
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 15
    .parameter "requestId"
    .parameter "result"

    .prologue
    const v9, 0x7f08003d

    const v8, 0x7f08003a

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 210
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_15

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, p1, :cond_16

    .line 235
    :cond_15
    :goto_15
    return-void

    .line 214
    :cond_16
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 216
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v2

    .line 217
    .local v2, oob:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v3

    .line 219
    .local v3, settings:Lcom/google/android/apps/plus/content/AccountSettingsData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 220
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_34

    .line 221
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_15

    .line 224
    :cond_34
    if-eqz v2, :cond_3c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v4

    if-eqz v4, :cond_c5

    .line 225
    :cond_3c
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    .line 226
    .local v1, exception:Ljava/lang/Exception;
    instance-of v4, v1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v4, :cond_9a

    .line 227
    check-cast v1, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v1           #exception:Ljava/lang/Exception;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_174

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_55
    const v8, 0x7f0801c4

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v4, v8, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v4, p0, v7}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "server_error"

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_15

    :sswitch_70
    const v4, 0x7f08003b

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v6

    goto :goto_55

    :sswitch_79
    const v4, 0x7f080038

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v6

    goto :goto_55

    :sswitch_82
    const v4, 0x7f080039

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v5, v6

    goto :goto_55

    :sswitch_8b
    const v4, 0x7f08003e

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v4, 0x7f08003f

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_55

    .line 229
    .restart local v1       #exception:Ljava/lang/Exception;
    :cond_9a
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f080040

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v8, 0x7f0801c5

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v6, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v4, p0, v7}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "net_failure"

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_15

    .line 234
    .end local v1           #exception:Ljava/lang/Exception;
    :cond_c5
    sget-object v9, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->DIALOG_IDS:[Ljava/lang/String;

    array-length v10, v9

    move v8, v7

    :goto_c9
    if-ge v8, v10, :cond_e0

    aget-object v4, v9, v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/DialogFragment;

    if-eqz v4, :cond_dc

    invoke-virtual {v4}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_dc
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_c9

    :cond_e0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v8, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    if-eqz v8, :cond_114

    iget-object v8, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_114

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v7, :cond_104

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-static {v4, v6, v3, v7}, Lcom/google/android/apps/plus/phone/Intents;->getNextOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v6

    :cond_104
    if-eqz v6, :cond_10b

    invoke-virtual {p0, v6, v5}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_15

    :cond_10b
    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    goto/16 :goto_15

    :cond_114
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->isDialog()Z

    move-result v4

    if-eqz v4, :cond_171

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    if-lez v4, :cond_16f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    move v4, v5

    :goto_12c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_141

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_141
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getTag()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f090152

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mUpgradeOrigin:Ljava/lang/String;

    invoke-static {v8, v2, v9}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->newInstance(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    move-result-object v8

    invoke-virtual {v5, v7, v8, v6}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    if-eqz v4, :cond_16a

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_16a
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto/16 :goto_15

    :cond_16f
    move v4, v7

    goto :goto_12c

    :cond_171
    move v4, v5

    goto :goto_12c

    .line 227
    nop

    :sswitch_data_174
    .sparse-switch
        0x1 -> :sswitch_79
        0xa -> :sswitch_70
        0xc -> :sswitch_82
        0xf -> :sswitch_8b
    .end sparse-switch
.end method

.method private isDialog()Z
    .registers 2

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->dialog:Lcom/google/api/services/plusi/model/OutOfBoxDialog;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public static newInstance(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;
    .registers 7
    .parameter "account"
    .parameter "response"
    .parameter "upgradeOrigin"

    .prologue
    .line 121
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    .local v0, args:Landroid/os/Bundle;
    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 123
    const-string v2, "oob_resp"

    new-instance v3, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    invoke-direct {v3, p1}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 124
    const-string v2, "upgrade_origin"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    new-instance v1, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;-><init>()V

    .line 126
    .local v1, frag:Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->setArguments(Landroid/os/Bundle;)V

    .line 127
    return-object v1
.end method

.method private updateActionButtons()V
    .registers 12

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 441
    move v7, v8

    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v7, v6, :cond_4a

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->shouldPreventCompletionAction()Z

    move-result v10

    if-eqz v10, :cond_46

    invoke-virtual {v6}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_46

    move v3, v9

    .line 442
    .local v3, hasMissingField:Z
    :goto_20
    const/4 v4, 0x0

    .local v4, i:I
    :goto_21
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v4, v6, :cond_4e

    .line 443
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v6, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    .line 444
    .local v2, field:Lcom/google/android/apps/plus/oob/BaseFieldLayout;
    const-string v6, "CONTINUE"

    invoke-virtual {v2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getActionType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_43

    .line 445
    if-nez v3, :cond_4c

    move v6, v9

    :goto_40
    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->setActionEnabled(Z)V

    .line 442
    :cond_43
    add-int/lit8 v4, v4, 0x1

    goto :goto_21

    .line 441
    .end local v2           #field:Lcom/google/android/apps/plus/oob/BaseFieldLayout;
    .end local v3           #hasMissingField:Z
    .end local v4           #i:I
    :cond_46
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_3

    :cond_4a
    move v3, v8

    goto :goto_20

    .restart local v2       #field:Lcom/google/android/apps/plus/oob/BaseFieldLayout;
    .restart local v3       #hasMissingField:Z
    .restart local v4       #i:I
    :cond_4c
    move v6, v8

    .line 445
    goto :goto_40

    .line 449
    .end local v2           #field:Lcom/google/android/apps/plus/oob/BaseFieldLayout;
    :cond_4e
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/BottomActionBar;->getButtons()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, i$:Ljava/util/Iterator;
    :cond_58
    :goto_58
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7d

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ActionButton;

    .line 450
    .local v1, button:Lcom/google/android/apps/plus/views/ActionButton;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ActionButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    .line 451
    .local v0, action:Lcom/google/api/services/plusi/model/OutOfBoxAction;
    const-string v6, "CONTINUE"

    iget-object v7, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_58

    .line 452
    if-nez v3, :cond_7b

    move v6, v9

    :goto_77
    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/views/ActionButton;->setEnabled(Z)V

    goto :goto_58

    :cond_7b
    move v6, v8

    goto :goto_77

    .line 455
    .end local v0           #action:Lcom/google/api/services/plusi/model/OutOfBoxAction;
    .end local v1           #button:Lcom/google/android/apps/plus/views/ActionButton;
    :cond_7d
    return-void
.end method


# virtual methods
.method public final onAction(Lcom/google/api/services/plusi/model/OutOfBoxAction;)V
    .registers 6
    .parameter "action"

    .prologue
    .line 472
    const-string v0, "URL"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 473
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->url:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->viewUrl(Landroid/content/Context;Ljava/lang/String;)V

    .line 483
    :cond_13
    :goto_13
    return-void

    .line 474
    :cond_14
    const-string v0, "BACK"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 475
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    move-result v0

    if-nez v0, :cond_13

    .line 476
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_13

    .line 478
    :cond_2c
    const-string v0, "CLOSE"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 479
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_13

    .line 481
    :cond_3a
    new-instance v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    const/4 v0, 0x0

    move v1, v0

    :goto_48
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_6d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v3

    iget-object v3, v3, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v3, :cond_69

    iget-object v3, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_48

    :cond_6d
    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxAction;-><init>()V

    iput-object v0, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    goto :goto_13
.end method

.method public final onActionId(Ljava/lang/String;)V
    .registers 7
    .parameter "actionId"

    .prologue
    .line 527
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    packed-switch v2, :pswitch_data_64

    const/4 v0, 0x0

    .line 528
    .local v0, actionType:Ljava/lang/String;
    :goto_8
    const-string v2, "BACK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 529
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 530
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    .line 542
    .end local v0           #actionType:Ljava/lang/String;
    :cond_1d
    :goto_1d
    return-void

    .line 527
    :pswitch_1e
    const-string v0, "CLOSE"

    goto :goto_8

    :pswitch_21
    const-string v0, "CONTINUE"

    goto :goto_8

    :pswitch_24
    const-string v0, "URL"

    goto :goto_8

    :pswitch_27
    const-string v0, "BACK"

    goto :goto_8

    .line 532
    .restart local v0       #actionType:Ljava/lang/String;
    :cond_2a
    const-string v2, "CLOSE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_52

    .line 533
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V
    :try_end_35
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_35} :catch_36

    goto :goto_1d

    .line 538
    .end local v0           #actionType:Ljava/lang/String;
    :catch_36
    move-exception v1

    .line 539
    .local v1, e:Ljava/lang/NumberFormatException;
    const-string v2, "OutOfBoxFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to parse actionId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", not calling action on this event."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1d

    .line 536
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .restart local v0       #actionType:Ljava/lang/String;
    :cond_52
    :try_start_52
    new-instance v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    new-instance v3, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/OutOfBoxAction;-><init>()V

    iput-object v0, v3, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V
    :try_end_63
    .catch Ljava/lang/NumberFormatException; {:try_start_52 .. :try_end_63} :catch_36

    goto :goto_1d

    .line 527
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 382
    packed-switch p1, :pswitch_data_16

    .line 388
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 391
    :goto_6
    return-void

    .line 384
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_6

    .line 382
    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_7
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "account"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "oob_resp"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    .line 138
    .local v1, responseWrapper:Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "upgrade_origin"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mUpgradeOrigin:Ljava/lang/String;

    .line 140
    const v3, 0x7f030073

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 141
    .local v2, view:Landroid/view/View;
    const v3, 0x7f090153

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ViewSwitcher;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    .line 142
    const v3, 0x7f090155

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mSignUpLayout:Landroid/view/ViewGroup;

    .line 143
    const v3, 0x7f090156

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    .line 144
    const v3, 0x7f090116

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/BottomActionBar;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    .line 145
    new-instance v3, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mSignUpLayout:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOobFields:Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/google/android/apps/plus/views/BottomActionBar;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxInflater:Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

    .line 146
    new-instance v4, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const v3, 0x7f090098

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    invoke-direct {v4, v5, v3, v6, p0}, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;-><init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/ViewGroup;Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxDialogInflater:Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

    .line 150
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->isDialog()Z

    move-result v3

    if-eqz v3, :cond_cb

    .line 151
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxDialogInflater:Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->inflate()V

    .line 152
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    const v5, 0x7f090157

    invoke-virtual {v4, v5}, Landroid/widget/ViewSwitcher;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ViewSwitcher;->indexOfChild(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4, v3}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 157
    :goto_a4
    if-eqz p3, :cond_ca

    .line 158
    const-string v3, "last_request"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    .line 160
    .local v0, requestWrapper:Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;
    if-eqz v0, :cond_b6

    .line 161
    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->getRequest()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    .line 163
    :cond_b6
    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_ca

    .line 164
    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 167
    .end local v0           #requestWrapper:Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;
    :cond_ca
    return-object v2

    .line 154
    :cond_cb
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxInflater:Lcom/google/android/apps/plus/oob/OutOfBoxInflater;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mOutOfBoxResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    invoke-virtual {v3, v4, p0}, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->inflateFromResponse(Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->updateActionButtons()V

    goto :goto_a4
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 4
    .parameter "tag"

    .prologue
    .line 318
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OOB dialog not cancelable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 323
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 312
    sget-boolean v0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_12

    const-string v0, "net_failure"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 313
    :cond_12
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    .line 314
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 301
    const-string v0, "net_failure"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    if-eqz v0, :cond_11

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    .line 308
    :cond_11
    :goto_11
    return-void

    .line 305
    :cond_12
    const-string v0, "server_error"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 306
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->close()V

    goto :goto_11
.end method

.method public final onInputChanged$7c32a9fe()V
    .registers 1

    .prologue
    .line 564
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->updateActionButtons()V

    .line 565
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 205
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 207
    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2d

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 193
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    if-eqz v0, :cond_2d

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 198
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2d
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 176
    const-string v0, "last_request"

    new-instance v1, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1e

    .line 178
    const-string v0, "reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 180
    :cond_1e
    return-void
.end method

.method public final sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V
    .registers 5
    .parameter "request"

    .prologue
    .line 514
    const/4 v0, 0x0

    const v1, 0x7f080036

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "sending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mUpgradeOrigin:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    .line 516
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mLastRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    .line 517
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->sendOutOfBoxRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 518
    return-void
.end method
