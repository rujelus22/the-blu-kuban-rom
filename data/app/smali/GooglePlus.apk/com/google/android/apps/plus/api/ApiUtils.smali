.class public final Lcom/google/android/apps/plus/api/ApiUtils;
.super Ljava/lang/Object;
.source "ApiUtils.java"


# static fields
.field private static final CIRCLE_ID_PREFIXES:[Ljava/lang/String;

.field public static final EMAIL_MENTION_PREFIX_LENGTH:I

.field public static final ID_MENTION_PREFIX_LENGTH:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x2

    .line 37
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "f."

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "g."

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/api/ApiUtils;->CIRCLE_ID_PREFIXES:[Ljava/lang/String;

    .line 49
    sput v3, Lcom/google/android/apps/plus/api/ApiUtils;->ID_MENTION_PREFIX_LENGTH:I

    .line 59
    sput v3, Lcom/google/android/apps/plus/api/ApiUtils;->EMAIL_MENTION_PREFIX_LENGTH:I

    return-void
.end method

.method public static buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;
    .registers 11
    .parameter "spannable"

    .prologue
    .line 115
    const/4 v7, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v8

    const-class v9, Lcom/google/android/apps/plus/views/MentionSpan;

    invoke-interface {p0, v7, v8, v9}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/android/apps/plus/views/MentionSpan;

    .line 118
    .local v5, mentionSpans:[Lcom/google/android/apps/plus/views/MentionSpan;
    new-instance v4, Ljava/util/ArrayList;

    array-length v7, v5

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 119
    .local v4, mentionReplace:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    array-length v7, v5

    new-array v6, v7, [I

    .line 120
    .local v6, mentionStarts:[I
    array-length v7, v5

    new-array v3, v7, [I

    .line 122
    .local v3, mentionEnds:[I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1a
    array-length v7, v5

    if-ge v1, v7, :cond_8a

    .line 123
    aget-object v7, v5, v1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/MentionSpan;->getAggregateId()Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, id:Ljava/lang/String;
    const-string v7, "g:"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_56

    .line 126
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v8, Lcom/google/android/apps/plus/api/ApiUtils;->ID_MENTION_PREFIX_LENGTH:I

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    :goto_43
    aget-object v7, v5, v1

    invoke-interface {p0, v7}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    aput v7, v6, v1

    .line 138
    aget-object v7, v5, v1

    invoke-interface {p0, v7}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    aput v7, v3, v1

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_1a

    .line 128
    :cond_56
    const-string v7, "e:"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_77

    .line 130
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v8, Lcom/google/android/apps/plus/api/ApiUtils;->EMAIL_MENTION_PREFIX_LENGTH:I

    invoke-virtual {v2, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_43

    .line 134
    :cond_77
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "+"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_43

    .line 141
    .end local v2           #id:Ljava/lang/String;
    :cond_8a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 142
    .local v0, buf:Ljava/lang/StringBuilder;
    array-length v7, v5

    add-int/lit8 v1, v7, -0x1

    :goto_96
    if-ltz v1, :cond_a8

    .line 143
    aget v8, v6, v1

    aget v9, v3, v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v0, v8, v9, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    add-int/lit8 v1, v1, -0x1

    goto :goto_96

    .line 146
    :cond_a8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method

.method public static removeCircleIdNamespaces(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
    .registers 16
    .parameter "audience"

    .prologue
    .line 84
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v12

    invoke-direct {v4, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 85
    .local v4, circles:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v8, v0

    .local v8, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    move v6, v5

    .end local v0           #arr$:[Lcom/google/android/apps/plus/content/CircleData;
    .end local v5           #i$:I
    .end local v8           #len$:I
    .local v6, i$:I
    :goto_10
    if-ge v6, v8, :cond_4e

    aget-object v2, v0, v6

    .line 86
    .local v2, circle:Lcom/google/android/apps/plus/content/CircleData;
    sget-object v1, Lcom/google/android/apps/plus/api/ApiUtils;->CIRCLE_ID_PREFIXES:[Ljava/lang/String;

    .local v1, arr$:[Ljava/lang/String;
    array-length v9, v1

    .local v9, len$:I
    const/4 v5, 0x0

    .end local v6           #i$:I
    .restart local v5       #i$:I
    :goto_18
    if-ge v5, v9, :cond_44

    aget-object v10, v1, v5

    .line 87
    .local v10, prefix:Ljava/lang/String;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4b

    .line 88
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 89
    .local v7, id:Ljava/lang/String;
    new-instance v3, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v12

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v14

    invoke-direct {v3, v7, v12, v13, v14}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .end local v2           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .local v3, circle:Lcom/google/android/apps/plus/content/CircleData;
    move-object v2, v3

    .line 97
    .end local v3           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v7           #id:Ljava/lang/String;
    .end local v10           #prefix:Ljava/lang/String;
    .restart local v2       #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_44
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    .end local v5           #i$:I
    .restart local v6       #i$:I
    goto :goto_10

    .line 86
    .end local v6           #i$:I
    .restart local v5       #i$:I
    .restart local v10       #prefix:Ljava/lang/String;
    :cond_4b
    add-int/lit8 v5, v5, 0x1

    goto :goto_18

    .line 99
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v5           #i$:I
    .end local v9           #len$:I
    .end local v10           #prefix:Ljava/lang/String;
    .restart local v6       #i$:I
    :cond_4e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v12

    invoke-static {v12}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    .line 100
    .local v11, users:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    new-instance v12, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v12, v11, v4}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v12
.end method
