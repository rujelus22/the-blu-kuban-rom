.class final Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;
.super Ljava/lang/Object;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProfileLayout"
.end annotation


# instance fields
.field public details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

.field public error:Landroid/widget/TextView;

.field public header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 8
    .parameter "view"

    .prologue
    const/4 v5, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    invoke-direct {v2, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;-><init>(B)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    .line 116
    new-instance v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    invoke-direct {v2, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;-><init>(B)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    .line 117
    const v2, 0x7f090072

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    .line 119
    const v2, 0x7f0901b5

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 120
    .local v1, headerView:Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iput-object v1, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->container:Landroid/view/View;

    .line 122
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901cc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    .line 123
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v3, 0x7f0901cd

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    .line 124
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const v4, 0x7f0901ce

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto1:Lcom/google/android/apps/plus/views/EsImageView;

    .line 125
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const v4, 0x7f0901cf

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto2:Lcom/google/android/apps/plus/views/EsImageView;

    .line 126
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const v4, 0x7f0901d0

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto3:Lcom/google/android/apps/plus/views/EsImageView;

    .line 127
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const v4, 0x7f0901d1

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto4:Lcom/google/android/apps/plus/views/EsImageView;

    .line 128
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const v4, 0x7f0901d2

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto5:Lcom/google/android/apps/plus/views/EsImageView;

    .line 130
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f09012d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EsImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    .line 131
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901d4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarChoosePhotoIcon:Landroid/widget/ImageView;

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901d6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    .line 136
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901d7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->fullName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    .line 137
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901d8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->givenName:Landroid/widget/TextView;

    .line 138
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901d9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->familyName:Landroid/widget/TextView;

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    new-instance v3, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    const v4, 0x7f0901db

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;-><init>(Landroid/view/View;I)V

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    .line 141
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    new-instance v3, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    const v4, 0x7f0901dc

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;-><init>(Landroid/view/View;I)V

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    .line 142
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    new-instance v3, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    const v4, 0x7f0901dd

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;-><init>(Landroid/view/View;I)V

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v3, 0x7f0901de

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->buttons:Landroid/view/View;

    .line 145
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901df

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/CirclesButton;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    .line 146
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901e0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/CirclesButton;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    .line 148
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/CirclesButton;->setShowIcon(Z)V

    .line 149
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0900b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    .line 151
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0900a2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    .line 152
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901e1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    .line 154
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    const v2, 0x7f0901e2

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    .line 156
    const v2, 0x7f0901b3

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 157
    .local v0, detailsView:Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v0, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v3, 0x7f0901b4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    .line 160
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v3, 0x7f0901b6

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    .line 161
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v2, 0x7f0901c2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    .line 162
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const v4, 0x7f0901c3

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    .line 163
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const v4, 0x7f0901c4

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    .line 165
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const v4, 0x7f0901c5

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    .line 167
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v2, 0x7f0901c6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    .line 168
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v2, 0x7f0901c7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    .line 170
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v2, 0x7f0901c8

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    .line 171
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    const v4, 0x7f0901c9

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->map:Landroid/view/View;

    .line 172
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    const v4, 0x7f09008a

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    .line 173
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const v2, 0x7f0901ca

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    .line 174
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    const v4, 0x7f0901be

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    .line 175
    return-void
.end method
