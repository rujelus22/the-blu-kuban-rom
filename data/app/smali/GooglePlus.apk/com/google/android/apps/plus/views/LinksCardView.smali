.class public Lcom/google/android/apps/plus/views/LinksCardView;
.super Lcom/google/android/apps/plus/views/StreamCardView;
.source "LinksCardView.java"


# instance fields
.field protected mBackgroundDestRect:Landroid/graphics/Rect;

.field protected mBackgroundSrcRect:Landroid/graphics/Rect;

.field protected mImage:Lcom/google/android/apps/plus/views/MediaImage;

.field protected mImageBorderRect:Landroid/graphics/Rect;

.field protected mImageDimension:I

.field protected mImageRect:Landroid/graphics/Rect;

.field protected mLinkTitle:Ljava/lang/String;

.field protected mLinkTitleLayout:Landroid/text/StaticLayout;

.field protected mLinkUrl:Ljava/lang/String;

.field protected mLinkUrlLayout:Landroid/text/StaticLayout;

.field protected mMediaLinkUrl:Ljava/lang/String;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field protected mThumbnailUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/LinksCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/StreamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-static {p1}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->init(Landroid/content/Context;)V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    .line 59
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    .line 60
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundDestRect:Landroid/graphics/Rect;

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    .line 62
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageBorderRect:Landroid/graphics/Rect;

    .line 63
    return-void
.end method

.method public static makeLinkUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "url"

    .prologue
    .line 258
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 259
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 260
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, hostName:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_26

    .line 262
    const-string v2, "www."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 263
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 265
    :cond_21
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 268
    .end local v0           #hostName:Ljava/lang/String;
    .end local v1           #uri:Landroid/net/Uri;
    :goto_25
    return-object v2

    :cond_26
    const/4 v2, 0x0

    goto :goto_25
.end method


# virtual methods
.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .registers 29
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 209
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 210
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    .line 211
    .local v11, bitmap:Landroid/graphics/Bitmap;
    if-eqz v11, :cond_c5

    const/4 v9, 0x1

    .line 213
    .local v9, hasBitmap:Z
    :goto_12
    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getLinksTopAreaBackgroundPaint()Landroid/graphics/Paint;

    move-result-object v10

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p4

    move/from16 v8, p5

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/apps/plus/views/LinksCardView;->drawMediaTopAreaStage(Landroid/graphics/Canvas;IIZLandroid/graphics/Paint;)V

    .line 216
    if-eqz v9, :cond_38

    .line 217
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundDestRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageBorderRect:Landroid/graphics/Rect;

    move-object/from16 v10, p1

    invoke-static/range {v10 .. v15}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 221
    :cond_38
    sget v5, Lcom/google/android/apps/plus/views/LinksCardView;->sLeftBorderPadding:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    add-int v13, v5, v6

    .line 224
    .local v13, xOffset:I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v5, :cond_50

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v5, :cond_f6

    .line 225
    :cond_50
    sget v5, Lcom/google/android/apps/plus/views/LinksCardView;->sYDoublePadding:I

    add-int v5, v5, p5

    int-to-float v5, v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/LinksCardView;->getMediaHeightPercentage()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v0, v5

    move/from16 v19, v0

    .line 227
    .local v19, imageAreaHeight:I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-nez v5, :cond_c8

    move/from16 v18, v19

    .line 229
    .local v18, availableHeight:I
    :goto_65
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v5, :cond_d7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v5}, Landroid/text/TextPaint;->descent()F

    move-result v5

    :goto_77
    float-to-int v0, v5

    move/from16 v20, v0

    .line 232
    .local v20, textOffset:I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v5, :cond_e4

    const/16 v21, 0x0

    .line 233
    .local v21, titleHeight:I
    :goto_82
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-nez v5, :cond_ed

    const/16 v22, 0x0

    .line 234
    .local v22, urlHeight:I
    :goto_8a
    sub-int v5, v18, v21

    sub-int v5, v5, v22

    div-int/lit8 v5, v5, 0x2

    add-int v14, v5, v20

    .line 239
    .end local v18           #availableHeight:I
    .end local v19           #imageAreaHeight:I
    .end local v20           #textOffset:I
    .end local v21           #titleHeight:I
    .end local v22           #urlHeight:I
    .local v14, yOffset:I
    :goto_92
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    move-object/from16 v16, v0

    sget-object v5, Lcom/google/android/apps/plus/views/LinksCardView;->sTagLinkBitmaps:[Landroid/graphics/Bitmap;

    const/4 v6, 0x0

    aget-object v17, v5, v6

    move-object/from16 v12, p1

    invoke-static/range {v12 .. v17}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->drawTitleAndUrl(Landroid/graphics/Canvas;IILandroid/text/StaticLayout;Landroid/text/StaticLayout;Landroid/graphics/Bitmap;)V

    .line 242
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p4

    move/from16 v3, p5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/LinksCardView;->drawMediaTopAreaShadow(Landroid/graphics/Canvas;II)V

    .line 243
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/plus/views/LinksCardView;->drawPlusOneBar(Landroid/graphics/Canvas;)V

    .line 244
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p4

    move/from16 v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/LinksCardView;->drawMediaBottomArea$1be95c43(Landroid/graphics/Canvas;III)I

    .line 245
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/plus/views/LinksCardView;->drawWhatsHot(Landroid/graphics/Canvas;)V

    .line 246
    return p5

    .line 211
    .end local v9           #hasBitmap:Z
    .end local v13           #xOffset:I
    .end local v14           #yOffset:I
    :cond_c5
    const/4 v9, 0x0

    goto/16 :goto_12

    .line 227
    .restart local v9       #hasBitmap:Z
    .restart local v13       #xOffset:I
    .restart local v19       #imageAreaHeight:I
    :cond_c8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v18, v19, v5

    goto :goto_65

    .line 229
    .restart local v18       #availableHeight:I
    :cond_d7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-virtual {v5}, Landroid/text/TextPaint;->descent()F

    move-result v5

    goto :goto_77

    .line 232
    .restart local v20       #textOffset:I
    :cond_e4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v21

    goto :goto_82

    .line 233
    .restart local v21       #titleHeight:I
    :cond_ed
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v22

    goto :goto_8a

    .line 236
    .end local v18           #availableHeight:I
    .end local v19           #imageAreaHeight:I
    .end local v20           #textOffset:I
    .end local v21           #titleHeight:I
    :cond_f6
    const/4 v14, 0x0

    .restart local v14       #yOffset:I
    goto :goto_92
.end method

.method public final getLinkTitle()Ljava/lang/String;
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final getLinkUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaLinkUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 21
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 70
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/plus/views/StreamCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 73
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    .line 74
    .local v10, mediaBytes:[B
    if-eqz v10, :cond_6f

    .line 75
    invoke-static {v10}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v7

    .line 76
    .local v7, dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/4 v8, 0x0

    .local v8, i:I
    array-length v9, v7

    .local v9, length:I
    :goto_10
    if-ge v8, v9, :cond_6f

    .line 77
    aget-object v0, v7, v8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v11

    .line 79
    .local v11, type:I
    const/4 v0, 0x1

    if-ne v11, v0, :cond_70

    .line 80
    aget-object v0, v7, v8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v4

    .line 81
    .local v4, thumbnailUrl:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_70

    .line 82
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 87
    aget-object v0, v7, v8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaLinkUrl:Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaLinkUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/views/LinksCardView;->makeLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrl:Ljava/lang/String;

    .line 89
    iput-object v4, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 91
    aget-object v0, v7, v8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitle:Ljava/lang/String;

    .line 93
    iget v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mDisplaySizeType:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_58

    iget v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mDisplaySizeType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6f

    .line 95
    :cond_58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&google_plus:card_type=nonsquare"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 101
    .end local v4           #thumbnailUrl:Ljava/lang/String;
    .end local v7           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v8           #i:I
    .end local v9           #length:I
    .end local v11           #type:I
    :cond_6f
    return-void

    .line 76
    .restart local v7       #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .restart local v8       #i:I
    .restart local v9       #length:I
    .restart local v11       #type:I
    :cond_70
    add-int/lit8 v8, v8, 0x1

    goto :goto_10
.end method

.method protected final layoutElements(IIII)I
    .registers 24
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 164
    sget v4, Lcom/google/android/apps/plus/views/LinksCardView;->sXDoublePadding:I

    add-int v18, p3, v4

    .line 165
    .local v18, imageAreaWidth:I
    sget v4, Lcom/google/android/apps/plus/views/LinksCardView;->sYDoublePadding:I

    add-int v4, v4, p4

    int-to-float v4, v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/LinksCardView;->getMediaHeightPercentage()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v17, v0

    .line 167
    .local v17, imageAreaHeight:I
    sget v4, Lcom/google/android/apps/plus/views/LinksCardView;->sTopBorderPadding:I

    add-int v4, v4, v17

    sget v5, Lcom/google/android/apps/plus/views/LinksCardView;->sYPadding:I

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p3

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/apps/plus/views/LinksCardView;->createPlusOneBar(III)I

    .line 168
    invoke-virtual/range {p0 .. p4}, Lcom/google/android/apps/plus/views/LinksCardView;->createMediaBottomArea(IIII)I

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-nez v4, :cond_fd

    move/from16 v16, v17

    .line 172
    .local v16, availableHeight:I
    :goto_2c
    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getMaxImageDimension()I

    move-result v6

    .line 173
    .local v6, maxImageDimension:I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    if-nez v4, :cond_4d

    .line 175
    move/from16 v0, v18

    int-to-float v4, v0

    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getImageMaxWidthPercentage()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    move/from16 v0, v16

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    .line 180
    :cond_4d
    new-instance v3, Lcom/google/android/apps/plus/content/MediaImageRequest;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mThumbnailUrl:Ljava/lang/String;

    const/4 v5, 0x3

    const/4 v8, 0x1

    move v7, v6

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .line 182
    .local v3, request:Lcom/google/android/apps/plus/content/ImageRequest;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v4, :cond_66

    .line 183
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MediaImage;->cancelRequest()V

    .line 185
    :cond_66
    new-instance v4, Lcom/google/android/apps/plus/views/MediaImage;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3, v6, v6}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 188
    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    sget v10, Lcom/google/android/apps/plus/views/LinksCardView;->sLeftBorderPadding:I

    sget v11, Lcom/google/android/apps/plus/views/LinksCardView;->sTopBorderPadding:I

    sget v4, Lcom/google/android/apps/plus/views/LinksCardView;->sLeftBorderPadding:I

    add-int v12, v4, v18

    sget v4, Lcom/google/android/apps/plus/views/LinksCardView;->sTopBorderPadding:I

    add-int v13, v4, v17

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundDestRect:Landroid/graphics/Rect;

    move/from16 v7, v18

    move/from16 v8, v17

    invoke-static/range {v7 .. v15}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createBackgroundRects(IIIIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 193
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    sget v9, Lcom/google/android/apps/plus/views/LinksCardView;->sLeftBorderPadding:I

    sget v10, Lcom/google/android/apps/plus/views/LinksCardView;->sTopBorderPadding:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageBorderRect:Landroid/graphics/Rect;

    move/from16 v7, v16

    invoke-static/range {v7 .. v12}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createImageRects(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    sget v7, Lcom/google/android/apps/plus/views/LinksCardView;->sLeftBorderPadding:I

    mul-int/lit8 v7, v7, 0x2

    sub-int v7, v18, v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v4, v5, v7}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createTitle(Ljava/lang/String;II)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    .line 199
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageDimension:I

    sget v4, Lcom/google/android/apps/plus/views/LinksCardView;->sLeftBorderPadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v18, v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    sub-int/2addr v4, v8

    sget-object v8, Lcom/google/android/apps/plus/views/LinksCardView;->sTagLinkBitmaps:[Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sub-int v8, v4, v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v4, :cond_10d

    const/4 v4, 0x0

    :goto_f4
    invoke-static {v5, v7, v8, v4}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createUrl(Ljava/lang/String;III)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    .line 204
    return p4

    .line 170
    .end local v3           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v6           #maxImageDimension:I
    .end local v16           #availableHeight:I
    :cond_fd
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int v16, v17, v4

    goto/16 :goto_2c

    .line 199
    .restart local v3       #request:Lcom/google/android/apps/plus/content/ImageRequest;
    .restart local v6       #maxImageDimension:I
    .restart local v16       #availableHeight:I
    :cond_10d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    goto :goto_f4
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mThumbnailUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LinksCardView;->invalidate()V

    .line 255
    :cond_10
    return-void
.end method

.method public onRecycle()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onRecycle()V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_d

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->cancelRequest()V

    .line 111
    :cond_d
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitle:Ljava/lang/String;

    .line 112
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    .line 113
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrl:Ljava/lang/String;

    .line 114
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    .line 115
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 116
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mMediaLinkUrl:Ljava/lang/String;

    .line 117
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 118
    iput-object v1, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mBackgroundDestRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImageBorderRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 123
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 127
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onStart()V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_c

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 131
    :cond_c
    return-void
.end method

.method public final onStop()V
    .registers 4

    .prologue
    .line 135
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onStop()V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_e

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LinksCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/MediaImage;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 139
    :cond_e
    return-void
.end method
