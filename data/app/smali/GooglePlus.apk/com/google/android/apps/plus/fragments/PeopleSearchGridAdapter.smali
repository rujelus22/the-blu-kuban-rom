.class public final Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;
.super Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.source "PeopleSearchGridAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;


# instance fields
.field private mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

.field private mShowMembership:Z

.field private mViewingAsPlusPage:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 6
    .parameter "context"
    .parameter "fragmentManager"
    .parameter "loaderManager"
    .parameter "account"

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 41
    invoke-virtual {p4}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mViewingAsPlusPage:Z

    .line 42
    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;I)V
    .registers 26
    .parameter "view"
    .parameter "partition"
    .parameter "cursor"
    .parameter "position"

    .prologue
    .line 66
    packed-switch p2, :pswitch_data_1e2

    .line 179
    :goto_3
    :pswitch_3
    return-void

    :pswitch_4
    move-object/from16 v2, p1

    .line 68
    check-cast v2, Lcom/google/android/apps/plus/views/PersonCardView;

    .line 69
    .local v2, card:Lcom/google/android/apps/plus/views/PersonCardView;
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->setOnPersonCardClickListener(Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;)V

    .line 70
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->getPositionForPartition(I)I

    move-result v4

    add-int v4, v4, p4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setPosition(I)V

    .line 71
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 72
    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setContactName(Ljava/lang/String;)V

    .line 73
    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setPersonId(Ljava/lang/String;)V

    .line 74
    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 75
    .local v14, gaiaId:Ljava/lang/String;
    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 76
    .local v10, avatarUrl:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v14, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mShowMembership:Z

    if-eqz v4, :cond_aa

    .line 78
    const/4 v4, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, circles:Ljava/lang/String;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/plus/views/PersonCardView;->setPackedCircleIdsEmailAndDescription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 81
    const/4 v4, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_8a

    const/4 v11, 0x1

    .line 82
    .local v11, blocked:Z
    :goto_68
    if-eqz v11, :cond_8e

    .line 83
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setForceAvatarDefault(Z)V

    .line 84
    const/4 v4, 0x1

    const v5, 0x7f0803c9

    const/4 v6, 0x1

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setActionButtonVisible(ZII)V

    .line 86
    const/4 v4, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_8c

    const/4 v4, 0x1

    :goto_81
    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setPlusPage(Z)V

    .line 94
    :goto_84
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setDismissActionButtonVisible(Z)V

    goto/16 :goto_3

    .line 81
    .end local v11           #blocked:Z
    :cond_8a
    const/4 v11, 0x0

    goto :goto_68

    .line 86
    .restart local v11       #blocked:Z
    :cond_8c
    const/4 v4, 0x0

    goto :goto_81

    .line 88
    :cond_8e
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mViewingAsPlusPage:Z

    if-nez v4, :cond_a3

    .line 89
    const/4 v4, 0x1

    const v5, 0x7f0803c7

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setActionButtonVisible(ZII)V

    goto :goto_84

    .line 92
    :cond_a3
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setActionButtonVisible(ZII)V

    goto :goto_84

    .line 96
    .end local v3           #circles:Ljava/lang/String;
    .end local v11           #blocked:Z
    :cond_aa
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v2

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/apps/plus/views/PersonCardView;->setPackedCircleIdsEmailAndDescription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 98
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mViewingAsPlusPage:Z

    if-nez v4, :cond_c7

    .line 99
    const/4 v4, 0x1

    const v5, 0x7f0803c7

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setActionButtonVisible(ZII)V

    .line 104
    :goto_c1
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setDismissActionButtonVisible(Z)V

    goto/16 :goto_3

    .line 102
    :cond_c7
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setActionButtonVisible(ZII)V

    goto :goto_c1

    .end local v2           #card:Lcom/google/android/apps/plus/views/PersonCardView;
    .end local v10           #avatarUrl:Ljava/lang/String;
    .end local v14           #gaiaId:Ljava/lang/String;
    :pswitch_ce
    move-object/from16 v2, p1

    .line 111
    check-cast v2, Lcom/google/android/apps/plus/views/PersonCardView;

    .line 112
    .restart local v2       #card:Lcom/google/android/apps/plus/views/PersonCardView;
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->setOnPersonCardClickListener(Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;)V

    .line 113
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->getPositionForPartition(I)I

    move-result v4

    add-int v4, v4, p4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setPosition(I)V

    .line 114
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setHighlightedText(Ljava/lang/String;)V

    .line 115
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 116
    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 117
    .local v18, personId:Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->setPersonId(Ljava/lang/String;)V

    .line 118
    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 119
    .restart local v14       #gaiaId:Ljava/lang/String;
    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 120
    .local v16, lookupKey:Ljava/lang/String;
    const/4 v4, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 121
    .restart local v10       #avatarUrl:Ljava/lang/String;
    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v2, v14, v0, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setContactIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setContactName(Ljava/lang/String;)V

    .line 124
    const/16 v4, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 125
    .local v20, snippet:Ljava/lang/String;
    if-nez v20, :cond_142

    .line 126
    const/16 v4, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 127
    if-nez v20, :cond_142

    .line 128
    const/16 v4, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 132
    :cond_142
    const/4 v4, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 134
    .local v12, circleIds:Ljava/lang/String;
    const/4 v4, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 135
    .local v19, profileType:I
    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v4, v5}, Lcom/google/android/apps/plus/views/PersonCardView;->setDescription(Ljava/lang/String;ZZ)V

    .line 137
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mViewingAsPlusPage:Z

    if-nez v4, :cond_16a

    .line 138
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    const/4 v4, 0x1

    move/from16 v0, v19

    if-ne v0, v4, :cond_180

    const/4 v4, 0x1

    :goto_167
    invoke-virtual {v2, v12, v5, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setOneClickCircles(Ljava/lang/String;Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;Z)V

    .line 142
    :cond_16a
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, p4

    if-ne v0, v4, :cond_177

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->continueLoadingPublicProfiles()V

    .line 146
    :cond_177
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PersonCardView;->updateContentDescription()V

    .line 147
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setDismissActionButtonVisible(Z)V

    goto/16 :goto_3

    .line 138
    :cond_180
    const/4 v4, 0x0

    goto :goto_167

    .end local v2           #card:Lcom/google/android/apps/plus/views/PersonCardView;
    .end local v10           #avatarUrl:Ljava/lang/String;
    .end local v12           #circleIds:Ljava/lang/String;
    .end local v14           #gaiaId:Ljava/lang/String;
    .end local v16           #lookupKey:Ljava/lang/String;
    .end local v18           #personId:Ljava/lang/String;
    .end local v19           #profileType:I
    .end local v20           #snippet:Ljava/lang/String;
    :pswitch_182
    move-object/from16 v2, p1

    .line 152
    check-cast v2, Lcom/google/android/apps/plus/views/PersonCardView;

    .line 153
    .restart local v2       #card:Lcom/google/android/apps/plus/views/PersonCardView;
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->setOnPersonCardClickListener(Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/PersonCardView;->setWellFormedEmail(Ljava/lang/String;)V

    .line 155
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mAddToCirclesActionEnabled:Z

    const v5, 0x7f0803c7

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setActionButtonVisible(ZII)V

    .line 157
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PersonCardView;->updateContentDescription()V

    goto/16 :goto_3

    .line 162
    .end local v2           #card:Lcom/google/android/apps/plus/views/PersonCardView;
    :pswitch_1a2
    const/16 v15, 0x8

    .line 163
    .local v15, loadingVisibility:I
    const/16 v17, 0x8

    .line 164
    .local v17, notFoundVisibility:I
    const/16 v13, 0x8

    .line 165
    .local v13, errorVisibility:I
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_1f2

    .line 176
    :goto_1b2
    const v4, 0x7f090164

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v15}, Landroid/view/View;->setVisibility(I)V

    .line 177
    const v4, 0x7f090165

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 178
    const v4, 0x7f090166

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 167
    :pswitch_1da
    const/4 v15, 0x0

    .line 168
    goto :goto_1b2

    .line 170
    :pswitch_1dc
    const/16 v17, 0x0

    .line 171
    goto :goto_1b2

    .line 173
    :pswitch_1df
    const/4 v13, 0x0

    goto :goto_1b2

    .line 66
    nop

    :pswitch_data_1e2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_182
        :pswitch_3
        :pswitch_ce
        :pswitch_1a2
    .end packed-switch

    .line 165
    :pswitch_data_1f2
    .packed-switch 0x1
        :pswitch_1da
        :pswitch_1dc
        :pswitch_1df
    .end packed-switch
.end method

.method public final changeCircleMembers$2c8bde3e(Landroid/database/Cursor;Z)V
    .registers 4
    .parameter "cursor"
    .parameter "showMembership"

    .prologue
    .line 185
    iput-boolean p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mShowMembership:Z

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 187
    return-void
.end method

.method public final isCursorClosingEnabled()Z
    .registers 2

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method

.method protected final newView$54126883(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "context"
    .parameter "partition"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 51
    packed-switch p2, :pswitch_data_16

    .line 59
    new-instance v1, Lcom/google/android/apps/plus/views/PersonCardView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/PersonCardView;-><init>(Landroid/content/Context;)V

    :goto_8
    return-object v1

    .line 53
    :pswitch_9
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x7f03007e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_8

    .line 51
    :pswitch_data_16
    .packed-switch 0x5
        :pswitch_9
    .end packed-switch
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PersonCardView;I)V
    .registers 6
    .parameter "view"
    .parameter "action"

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v0, :cond_7

    .line 212
    packed-switch p2, :pswitch_data_32

    .line 226
    :cond_7
    :goto_7
    return-void

    .line 214
    :pswitch_8
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getWellFormedEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 215
    const-string v0, "add_email_dialog"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    goto :goto_7

    .line 217
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getContactName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    .line 222
    :pswitch_26
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onUnblockPersonAction(Ljava/lang/String;Z)V

    goto :goto_7

    .line 212
    nop

    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_8
        :pswitch_26
    .end packed-switch
.end method

.method public final onChangeCircles(Lcom/google/android/apps/plus/views/PersonCardView;)V
    .registers 5
    .parameter "view"

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getContactName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method public final onDismissButtonClick(Lcom/google/android/apps/plus/views/PersonCardView;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v0, :cond_d

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onDismissSuggestionAction(Ljava/lang/String;)V

    .line 233
    :cond_d
    return-void
.end method

.method public final onItemClick(Lcom/google/android/apps/plus/views/PersonCardView;)V
    .registers 3
    .parameter "view"

    .prologue
    .line 196
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onItemClick(I)V

    .line 197
    return-void
.end method

.method public final setCircleSpinnerAdapter(Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;)V
    .registers 2
    .parameter "circleSpinnerAdapter"

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    .line 46
    return-void
.end method
