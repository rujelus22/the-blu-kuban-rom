.class final Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PreviewCursorLoader"
.end annotation


# instance fields
.field private mCachedData:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 451
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 2

    .prologue
    .line 455
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;->mCachedData:Z

    .line 456
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final isCachedData()Z
    .registers 2

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;->mCachedData:Z

    return v0
.end method

.method public final setCachedData(Z)V
    .registers 3
    .parameter "cachedData"

    .prologue
    .line 470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;->mCachedData:Z

    .line 471
    return-void
.end method
