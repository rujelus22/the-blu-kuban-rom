.class public final Lcom/google/android/apps/plus/phone/PhotoViewAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PhotoViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoViewAdapter$2;
    }
.end annotation


# static fields
.field private static sPhotoSize:Ljava/lang/Integer;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBarHeight:I

.field private mActionState:Ljava/lang/Integer;

.field private final mCommentLoadView:Landroid/view/View;

.field private final mDefaultAlbumName:Ljava/lang/String;

.field private mDisableSpacer:Z

.field private mDobjanschiContext:Landroid/content/Context;

.field private final mEventId:Ljava/lang/String;

.field private mExpandTags:Z

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFullScreen:Z

.field private mHasScrollData:Z

.field private mList:Lcom/google/android/apps/plus/views/PhotoListView;

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mScrollData:Landroid/database/Cursor;

.field private mShowPhotoOnly:Z

.field private mStreamClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private final mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/CommentRowView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .registers 16
    .parameter "context"
    .parameter "listView"
    .parameter "account"
    .parameter "c"
    .parameter "onClickListener"
    .parameter "streamClickListener"
    .parameter "defaultAlbumName"
    .parameter "eventId"
    .parameter "commentLoadView"

    .prologue
    .line 113
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 73
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mViews:Ljava/util/List;

    .line 91
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_77

    const/4 v3, 0x1

    :goto_12
    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    .line 114
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 115
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDobjanschiContext:Landroid/content/Context;

    .line 116
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 117
    iput-object p6, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mStreamClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 118
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDefaultAlbumName:Ljava/lang/String;

    .line 119
    iput-object p8, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mEventId:Ljava/lang/String;

    .line 120
    iput-object p9, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mCommentLoadView:Landroid/view/View;

    .line 121
    new-instance v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoViewAdapter;)V

    invoke-virtual {p2, v3}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 133
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    if-nez v3, :cond_5d

    .line 134
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 135
    .local v1, metrics:Landroid/util/DisplayMetrics;
    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 137
    .local v2, wm:Landroid/view/WindowManager;
    sget-object v0, Lcom/google/android/apps/plus/util/ImageUtils;->sUseImageSize:Lcom/google/android/apps/plus/util/ImageUtils$ImageSize;

    .line 138
    .local v0, imageSize:Lcom/google/android/apps/plus/util/ImageUtils$ImageSize;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 139
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter$2;->$SwitchMap$com$google$android$apps$plus$util$ImageUtils$ImageSize:[I

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/ImageUtils$ImageSize;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_8c

    .line 149
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    .line 154
    .end local v0           #imageSize:Lcom/google/android/apps/plus/util/ImageUtils$ImageSize;
    .end local v1           #metrics:Landroid/util/DisplayMetrics;
    .end local v2           #wm:Landroid/view/WindowManager;
    :cond_5d
    :goto_5d
    check-cast p2, Lcom/google/android/apps/plus/views/PhotoListView;

    .end local p2
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    .line 155
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoListView;->getTag()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Landroid/util/SparseArray;

    if-nez v3, :cond_76

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    new-instance v4, Landroid/util/SparseArray;

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Landroid/util/SparseArray;-><init>(I)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PhotoListView;->setTag(Ljava/lang/Object;)V

    .line 156
    :cond_76
    return-void

    .line 91
    .restart local p2
    :cond_77
    const/4 v3, 0x0

    goto :goto_12

    .line 142
    .restart local v0       #imageSize:Lcom/google/android/apps/plus/util/ImageUtils$ImageSize;
    .restart local v1       #metrics:Landroid/util/DisplayMetrics;
    .restart local v2       #wm:Landroid/view/WindowManager;
    :pswitch_79
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    mul-int/lit16 v3, v3, 0x320

    div-int/lit16 v3, v3, 0x3e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    goto :goto_5d

    .line 139
    :pswitch_data_8c
    .packed-switch 0x1
        :pswitch_79
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PhotoViewAdapter;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mCommentLoadView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/PhotoViewAdapter;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mViews:Ljava/util/List;

    return-object v0
.end method

.method public static getPhotoSize()Ljava/lang/Integer;
    .registers 1

    .prologue
    .line 682
    sget-object v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    return-object v0
.end method

.method private getTag(I)Ljava/lang/Object;
    .registers 4
    .parameter "key"

    .prologue
    .line 908
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoListView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 909
    .local v0, listTags:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/Object;>;"
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method private setTag(ILjava/lang/Object;)V
    .registers 5
    .parameter "key"
    .parameter "value"

    .prologue
    .line 917
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoListView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    .line 918
    .local v0, listTags:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/Object;>;"
    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 919
    return-void
.end method

.method private shouldShowTags(Landroid/content/Context;)Z
    .registers 11
    .parameter "context"

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 871
    const v6, 0x7f090038

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 872
    .local v4, scrollView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    if-nez v6, :cond_43

    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mShowPhotoOnly:Z

    if-nez v6, :cond_43

    move v5, v8

    .line 874
    .local v5, showTags:Z
    :goto_14
    if-eqz v5, :cond_37

    .line 875
    const-string v6, "window"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 877
    .local v0, display:Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 878
    .local v1, height:I
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 879
    .local v3, orientation:I
    if-eq v3, v8, :cond_2f

    const/4 v6, 0x3

    if-ne v3, v6, :cond_45

    :cond_2f
    move v2, v8

    .line 882
    .local v2, isRotated:Z
    :goto_30
    if-eqz v2, :cond_36

    const/16 v6, 0x320

    if-lt v1, v6, :cond_47

    :cond_36
    move v5, v8

    .line 885
    .end local v0           #display:Landroid/view/Display;
    .end local v1           #height:I
    .end local v2           #isRotated:Z
    .end local v3           #orientation:I
    :cond_37
    :goto_37
    if-eqz v5, :cond_42

    if-eqz v4, :cond_42

    .line 886
    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hasTags()Z

    move-result v6

    if-eqz v6, :cond_49

    move v5, v8

    .line 888
    :cond_42
    :goto_42
    return v5

    .end local v5           #showTags:Z
    :cond_43
    move v5, v7

    .line 872
    goto :goto_14

    .restart local v0       #display:Landroid/view/Display;
    .restart local v1       #height:I
    .restart local v3       #orientation:I
    .restart local v5       #showTags:Z
    :cond_45
    move v2, v7

    .line 879
    goto :goto_30

    .restart local v2       #isRotated:Z
    :cond_47
    move v5, v7

    .line 882
    goto :goto_37

    .end local v0           #display:Landroid/view/Display;
    .end local v1           #height:I
    .end local v2           #isRotated:Z
    .end local v3           #orientation:I
    :cond_49
    move v5, v7

    .line 886
    goto :goto_42
.end method


# virtual methods
.method public final addFlaggedComment(Ljava/lang/String;)V
    .registers 3
    .parameter "commentId"

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 660
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->notifyDataSetChanged()V

    .line 661
    return-void
.end method

.method public final allowComments()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 546
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionState:Ljava/lang/Integer;

    if-nez v2, :cond_23

    .line 547
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 548
    .local v0, myCursor:Landroid/database/Cursor;
    if-eqz v0, :cond_23

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 550
    :cond_11
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v1, :cond_32

    .line 551
    const/16 v2, 0x9

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionState:Ljava/lang/Integer;

    .line 557
    .end local v0           #myCursor:Landroid/database/Cursor;
    :cond_23
    :goto_23
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionState:Ljava/lang/Integer;

    if-eqz v2, :cond_39

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionState:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_39

    :goto_31
    return v1

    .line 554
    .restart local v0       #myCursor:Landroid/database/Cursor;
    :cond_32
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_11

    goto :goto_23

    .line 557
    .end local v0           #myCursor:Landroid/database/Cursor;
    :cond_39
    const/4 v1, 0x0

    goto :goto_31
.end method

.method public final bindPhoto(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bitmap"

    .prologue
    .line 349
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 350
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_12

    .line 351
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setPhotoLoading(Z)V

    .line 352
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindPhoto(Landroid/graphics/Bitmap;)V

    .line 354
    :cond_12
    return-void
.end method

.method public final bindScroller(Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 14
    .parameter "context"
    .parameter "cursor"

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 391
    const v7, 0x7f090038

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 392
    .local v3, scrollView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    const v7, 0x7f090037

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 394
    .local v0, scrollLayout:Landroid/view/View;
    if-eqz v3, :cond_6a

    .line 395
    const v7, 0x7f090177

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 396
    .local v6, tagRoot:Landroid/view/ViewGroup;
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, p1, v7, p2, v6}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->bind(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    .line 398
    const v7, 0x7f090174

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 399
    .local v2, scrollTitleLayout:Landroid/view/View;
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    const v7, 0x7f090039

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 403
    .local v1, scrollTitle:Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->isWaitingMyApproval()Z

    move-result v7

    if-eqz v7, :cond_62

    .line 404
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0201d6

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 405
    .local v5, tag:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v5, v9, v9, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 409
    :goto_55
    invoke-virtual {v1, v10, v10, v5, v10}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 411
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->shouldShowTags(Landroid/content/Context;)Z

    move-result v4

    .line 412
    .local v4, showTags:Z
    if-eqz v4, :cond_64

    .line 413
    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 422
    .end local v1           #scrollTitle:Landroid/widget/TextView;
    .end local v2           #scrollTitleLayout:Landroid/view/View;
    .end local v4           #showTags:Z
    .end local v5           #tag:Landroid/graphics/drawable/Drawable;
    .end local v6           #tagRoot:Landroid/view/ViewGroup;
    :goto_61
    return-void

    .line 407
    .restart local v1       #scrollTitle:Landroid/widget/TextView;
    .restart local v2       #scrollTitleLayout:Landroid/view/View;
    .restart local v6       #tagRoot:Landroid/view/ViewGroup;
    :cond_62
    const/4 v5, 0x0

    .restart local v5       #tag:Landroid/graphics/drawable/Drawable;
    goto :goto_55

    .line 415
    .restart local v4       #showTags:Z
    :cond_64
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_61

    .line 419
    .end local v1           #scrollTitle:Landroid/widget/TextView;
    .end local v2           #scrollTitleLayout:Landroid/view/View;
    .end local v4           #showTags:Z
    .end local v5           #tag:Landroid/graphics/drawable/Drawable;
    .end local v6           #tagRoot:Landroid/view/ViewGroup;
    :cond_6a
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mHasScrollData:Z

    .line 420
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mScrollData:Landroid/database/Cursor;

    goto :goto_61
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 25
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 204
    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 205
    .local v10, viewType:I
    packed-switch v10, :pswitch_data_384

    .line 232
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "cannot bind view of type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 212
    :pswitch_1f
    const v3, 0x7f090066

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 234
    .end local p1
    :cond_2c
    :goto_2c
    :pswitch_2c
    return-void

    .line 216
    .restart local p1
    :pswitch_2d
    const v3, 0x7f090035

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    move-object v11, v3

    check-cast v11, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    const v3, 0x7f090038

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    const v3, 0x7f090037

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    move-object v13, v3

    check-cast v13, Landroid/view/View;

    const v3, 0x7f090036

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    move-object v14, v3

    check-cast v14, Lcom/google/android/apps/plus/views/PhotoInfoView;

    const v3, 0x7f090034

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    move-object v15, v3

    check-cast v15, Lcom/google/android/apps/plus/views/PhotoLayout;

    const/16 v3, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_1eb

    const/4 v3, 0x0

    move-object/from16 v16, v3

    :goto_76
    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const/16 v3, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v3, 0x10

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "PLACEHOLDER"

    invoke-static {v4, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_cc

    if-eqz v16, :cond_1f7

    sget-object v9, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_99
    const-string v3, "content:"

    invoke-virtual {v8, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1fb

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v4, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    move-object/from16 v0, p2

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getLocalPhotoAsync(Landroid/content/Context;Lcom/google/android/apps/plus/api/MediaRef;I)V

    :goto_c8
    const/4 v3, 0x1

    invoke-virtual {v11, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setPhotoLoading(Z)V

    :cond_cc
    const/16 v3, 0xb

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/16 v3, 0xf

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    const/4 v3, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_233

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDefaultAlbumName:Ljava/lang/String;

    :goto_f7
    const/4 v4, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_23c

    const/4 v4, 0x0

    :goto_101
    const/4 v5, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v5, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    const/16 v5, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionState:Ljava/lang/Integer;

    invoke-virtual {v12, v11}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setHeaderView(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v12, v5}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setExternalOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mHasScrollData:Z

    if-eqz v5, :cond_145

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mHasScrollData:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mScrollData:Landroid/database/Cursor;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->bindScroller(Landroid/content/Context;Landroid/database/Cursor;)V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mScrollData:Landroid/database/Cursor;

    :cond_145
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->shouldShowTags(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_245

    const/4 v5, 0x0

    :goto_150
    invoke-virtual {v13, v5}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v5, :cond_249

    const/4 v5, 0x0

    :goto_15a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PhotoListView;->getMeasuredHeight()I

    move-result v6

    sub-int v5, v6, v5

    invoke-virtual {v15, v5}, Lcom/google/android/apps/plus/views/PhotoLayout;->setFixedHeight(I)V

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    if-eqz v5, :cond_24f

    if-nez v16, :cond_24f

    const/4 v5, 0x1

    :goto_170
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mShowPhotoOnly:Z

    if-eqz v6, :cond_252

    const/4 v6, 0x0

    :goto_177
    invoke-virtual {v11, v6}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setCommentCount(I)V

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mShowPhotoOnly:Z

    if-eqz v6, :cond_255

    const/4 v6, 0x0

    :goto_181
    invoke-virtual {v11, v6}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setPlusOneCount(I)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v11, v6}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    const/4 v12, 0x0

    invoke-virtual {v11, v6, v12}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setFullScreen(ZZ)V

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setVideoBlob([B)V

    invoke-virtual {v11, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->enableImageTransforms(Z)V

    move-object/from16 v0, v17

    invoke-virtual {v14, v9, v0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setOwner(Ljava/lang/String;Ljava/lang/CharSequence;)V

    move-object/from16 v0, v18

    invoke-virtual {v14, v0, v4, v3}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setAlbum(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mEventId:Ljava/lang/String;

    invoke-virtual {v14, v3}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setEvent(Ljava/lang/String;)V

    const-wide/16 v3, 0x0

    cmp-long v3, v19, v3

    if-lez v3, :cond_1bd

    move-object/from16 v0, p2

    move-wide/from16 v1, v19

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v14, v3}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setPhotoDate(Ljava/lang/CharSequence;)V

    :cond_1bd
    invoke-virtual {v14, v7}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setCommentCount(I)V

    invoke-virtual {v14, v8}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setPlusOneCount(I)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mShowPhotoOnly:Z

    if-nez v3, :cond_1cf

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    if-eqz v3, :cond_258

    :cond_1cf
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    if-eqz v3, :cond_1de

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/views/PhotoListView;->setSelectionFromTop(II)V

    :cond_1de
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Lcom/google/android/apps/plus/views/PhotoInfoView;->hideHeaderInfoView(Z)V

    :goto_1e2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mStreamClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-virtual {v14, v3}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setExternalClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V

    goto/16 :goto_2c

    :cond_1eb
    const/16 v3, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    move-object/from16 v16, v3

    goto/16 :goto_76

    :cond_1f7
    sget-object v9, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto/16 :goto_99

    :cond_1fb
    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/util/ImageUtils;->getResizedUrl(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->sPhotoSize:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v3 .. v8}, Lcom/google/android/apps/plus/service/EsService;->getRemotePhotoAsync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;III)V

    goto/16 :goto_c8

    :cond_233
    const/4 v3, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_f7

    :cond_23c
    const/4 v4, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_101

    :cond_245
    const/16 v5, 0x8

    goto/16 :goto_150

    :cond_249
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    goto/16 :goto_15a

    :cond_24f
    const/4 v5, 0x0

    goto/16 :goto_170

    :cond_252
    move v6, v7

    goto/16 :goto_177

    :cond_255
    move v6, v8

    goto/16 :goto_181

    :cond_258
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Lcom/google/android/apps/plus/views/PhotoInfoView;->showHeaderInfoView(Z)V

    goto :goto_1e2

    .line 220
    :pswitch_25d
    const v3, 0x7f09016d

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2c

    .line 224
    :pswitch_274
    check-cast p1, Lcom/google/android/apps/plus/views/CommentRowView;

    .end local p1
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    move-object/from16 v0, p2

    invoke-static {v0, v6, v7}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v6

    const/4 v3, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_324

    const/4 v3, 0x1

    :goto_2a3
    const/4 v8, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v9, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/CommentRowView;->clear()V

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/CommentRowView;->setShowPlusOneView(Z)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/CommentRowView;->setPosition(I)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mStreamClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/CommentRowView;->setClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/views/CommentRowView;->setAuthor(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CommentRowView;->setTime(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v3}, Lcom/google/android/apps/plus/views/CommentRowView;->setContent(Ljava/lang/String;Z)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/CommentRowView;->setCommentId(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/views/CommentRowView;->setIsFlagged(Z)V

    const/16 v3, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_327

    const/4 v3, 0x0

    :goto_2f9
    if-eqz v3, :cond_30a

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/CommentRowView;->setPlusOneData(Lcom/google/api/services/plusi/model/DataPlusOne;)V

    :cond_30a
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/CommentRowView;->requestLayout()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mViews:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mViews:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2c

    :cond_324
    const/4 v3, 0x0

    goto/16 :goto_2a3

    :cond_327
    const/16 v3, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    goto :goto_2f9

    .line 228
    .restart local p1
    :pswitch_330
    const v3, 0x7f09018a

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v4, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-static {v0, v5, v4}, Lcom/google/android/apps/plus/views/PlusOneDataUtils;->getLongPlusOnesString(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPlusOne;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v3}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v5, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-eqz v5, :cond_378

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_378

    const v4, 0x7f020130

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_2c

    :cond_378
    const v4, 0x7f02012f

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_2c

    .line 205
    nop

    :pswitch_data_384
    .packed-switch 0x0
        :pswitch_2c
        :pswitch_2d
        :pswitch_25d
        :pswitch_274
        :pswitch_330
        :pswitch_1f
    .end packed-switch
.end method

.method public final clear()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 588
    const v3, 0x7f090035

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 589
    .local v1, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    const v3, 0x7f090038

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 590
    .local v2, scrollerView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    const v3, 0x7f090034

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoLayout;

    .line 592
    .local v0, photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    if-eqz v1, :cond_21

    .line 593
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->clear()V

    .line 595
    :cond_21
    if-eqz v2, :cond_26

    .line 596
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->clear()V

    .line 598
    :cond_26
    if-eqz v0, :cond_2b

    .line 599
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoLayout;->clear()V

    .line 602
    :cond_2b
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mStreamClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 603
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 604
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 605
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mScrollData:Landroid/database/Cursor;

    .line 607
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PhotoListView;->setTag(Ljava/lang/Object;)V

    .line 608
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    .line 609
    iput-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDobjanschiContext:Landroid/content/Context;

    .line 610
    return-void
.end method

.method public final getAlbumName()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x1

    .line 528
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 529
    .local v1, cursor:Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 530
    .local v0, albumName:Ljava/lang/String;
    if-eqz v1, :cond_22

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_22

    .line 532
    :cond_f
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_1c

    .line 533
    invoke-interface {v1, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_27

    const/4 v0, 0x0

    .line 536
    :cond_1c
    :goto_1c
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_f

    .line 539
    :cond_22
    if-nez v0, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDefaultAlbumName:Ljava/lang/String;

    .end local v0           #albumName:Ljava/lang/String;
    :cond_26
    return-object v0

    .line 533
    .restart local v0       #albumName:Ljava/lang/String;
    :cond_27
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1c
.end method

.method public final getCount()I
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 238
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    if-eqz v2, :cond_16

    .line 239
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCount()I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v3, :cond_14

    :goto_12
    add-int/2addr v0, v2

    .line 242
    :goto_13
    return v0

    :cond_14
    move v0, v1

    .line 239
    goto :goto_12

    .line 242
    :cond_16
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCount()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v3, :cond_20

    :goto_1e
    add-int/2addr v0, v2

    goto :goto_13

    :cond_20
    move v0, v1

    goto :goto_1e
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 277
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-nez v1, :cond_8

    if-nez p1, :cond_8

    .line 278
    const/4 v1, 0x0

    .line 282
    :goto_7
    return-object v1

    .line 281
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v1, :cond_12

    move v0, p1

    .line 282
    .local v0, newPosition:I
    :goto_d
    invoke-super {p0, v0, p2, p3}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_7

    .line 281
    .end local v0           #newPosition:I
    :cond_12
    add-int/lit8 v0, p1, -0x1

    goto :goto_d
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 4
    .parameter "position"

    .prologue
    .line 247
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-nez v1, :cond_8

    if-nez p1, :cond_8

    .line 248
    const/4 v1, 0x0

    .line 252
    :goto_7
    return-object v1

    .line 251
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v1, :cond_12

    move v0, p1

    .line 252
    .local v0, newPosition:I
    :goto_d
    invoke-super {p0, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_7

    .line 251
    .end local v0           #newPosition:I
    :cond_12
    add-int/lit8 v0, p1, -0x1

    goto :goto_d
.end method

.method public final getItemId(I)J
    .registers 5
    .parameter "position"

    .prologue
    .line 257
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-nez v1, :cond_9

    if-nez p1, :cond_9

    .line 258
    const-wide/16 v1, 0x0

    .line 262
    :goto_8
    return-wide v1

    .line 261
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v1, :cond_13

    move v0, p1

    .line 262
    .local v0, newPosition:I
    :goto_e
    invoke-super {p0, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getItemId(I)J

    move-result-wide v1

    goto :goto_8

    .line 261
    .end local v0           #newPosition:I
    :cond_13
    add-int/lit8 v0, p1, -0x1

    goto :goto_e
.end method

.method public final getItemViewType(I)I
    .registers 7
    .parameter "position"

    .prologue
    .line 186
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-nez v2, :cond_8

    if-nez p1, :cond_8

    .line 187
    const/4 v2, 0x0

    .line 199
    :goto_7
    return v2

    .line 189
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v2, :cond_1b

    move v1, p1

    .line 191
    .local v1, newPosition:I
    :goto_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 192
    .local v0, cursor:Landroid/database/Cursor;
    if-nez v0, :cond_1e

    .line 193
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 189
    .end local v0           #cursor:Landroid/database/Cursor;
    .end local v1           #newPosition:I
    :cond_1b
    add-int/lit8 v1, p1, -0x1

    goto :goto_d

    .line 195
    .restart local v0       #cursor:Landroid/database/Cursor;
    .restart local v1       #newPosition:I
    :cond_1e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_39

    .line 196
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t move cursor to position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 199
    :cond_39
    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    goto :goto_7
.end method

.method public final getMyApprovedShapeId()Ljava/lang/Long;
    .registers 3

    .prologue
    .line 170
    const v1, 0x7f090038

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 171
    .local v0, tagScroller:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    if-eqz v0, :cond_10

    .line 172
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v1

    .line 174
    :goto_f
    return-object v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method public final getPhotoImage()Landroid/graphics/Bitmap;
    .registers 3

    .prologue
    .line 375
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 376
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_10

    .line 377
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getPhoto()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 379
    :goto_f
    return-object v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method public final getPhotoRef()Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final getVideoData()[B
    .registers 3

    .prologue
    .line 520
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 521
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-nez v0, :cond_d

    const/4 v1, 0x0

    :goto_c
    return-object v1

    :cond_d
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVideoData()[B

    move-result-object v1

    goto :goto_c
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 267
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-nez v1, :cond_24

    if-nez p1, :cond_24

    .line 268
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDobjanschiContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f03008d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v3, -0x1

    iget v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    :goto_23
    return-object v1

    .line 271
    :cond_24
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v1, :cond_2e

    move v0, p1

    .line 272
    .local v0, newPosition:I
    :goto_29
    invoke-super {p0, v0, p2, p3}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_23

    .line 271
    .end local v0           #newPosition:I
    :cond_2e
    add-int/lit8 v0, p1, -0x1

    goto :goto_29
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 180
    const/4 v0, 0x6

    return v0
.end method

.method public final interceptMoveLeft(FF)Z
    .registers 6
    .parameter "origX"
    .parameter "origY"

    .prologue
    .line 565
    const v2, 0x7f090035

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 566
    .local v0, photoView:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;
    const v2, 0x7f090038

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 568
    .local v1, scrollView:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;
    if-eqz v0, :cond_1a

    invoke-interface {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;->interceptMoveLeft$2548a39(F)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_1a
    if-eqz v1, :cond_24

    invoke-interface {v1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;->interceptMoveLeft$2548a39(F)Z

    move-result v2

    if-eqz v2, :cond_24

    :cond_22
    const/4 v2, 0x1

    :goto_23
    return v2

    :cond_24
    const/4 v2, 0x0

    goto :goto_23
.end method

.method public final interceptMoveRight(FF)Z
    .registers 6
    .parameter "origX"
    .parameter "origY"

    .prologue
    .line 576
    const v2, 0x7f090035

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 577
    .local v0, photoView:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;
    const v2, 0x7f090038

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 579
    .local v1, scrollView:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;
    if-eqz v0, :cond_1a

    invoke-interface {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;->interceptMoveRight$2548a39(F)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_1a
    if-eqz v1, :cond_24

    invoke-interface {v1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;->interceptMoveRight$2548a39(F)Z

    move-result v2

    if-eqz v2, :cond_24

    :cond_22
    const/4 v2, 0x1

    :goto_23
    return v2

    :cond_24
    const/4 v2, 0x0

    goto :goto_23
.end method

.method public final isPhotoBound()Z
    .registers 3

    .prologue
    .line 488
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 489
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isPhotoBound()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    :goto_12
    return v1

    :cond_13
    const/4 v1, 0x0

    goto :goto_12
.end method

.method public final isPhotoLoading()Z
    .registers 3

    .prologue
    .line 496
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 497
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isPhotoLoading()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    :goto_12
    return v1

    :cond_13
    const/4 v1, 0x0

    goto :goto_12
.end method

.method public final isVideo()Z
    .registers 3

    .prologue
    .line 504
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 505
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideo()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    :goto_12
    return v1

    :cond_13
    const/4 v1, 0x0

    goto :goto_12
.end method

.method public final isVideoReady()Z
    .registers 3

    .prologue
    .line 512
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 513
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_13

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideoReady()Z

    move-result v1

    if-eqz v1, :cond_13

    const/4 v1, 0x1

    :goto_12
    return v1

    :cond_13
    const/4 v1, 0x0

    goto :goto_12
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 17
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 287
    const/4 v10, 0x1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 288
    .local v9, viewType:I
    const-string v10, "layout_inflater"

    invoke-virtual {p1, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 290
    .local v2, layoutInflater:Landroid/view/LayoutInflater;
    packed-switch v9, :pswitch_data_be

    .line 336
    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "cannot create view of type: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 292
    :pswitch_25
    const v10, 0x7f030083

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 293
    .local v8, view:Landroid/view/View;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v10, -0x1

    const/4 v11, -0x1

    invoke-direct {v1, v10, v11}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 295
    .local v1, layout:Landroid/widget/AbsListView$LayoutParams;
    invoke-virtual {v8, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 297
    const v10, 0x7f090171

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/PhotoLayout;

    .line 299
    .local v3, photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    const v10, 0x7f090172

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 301
    .local v4, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    const v10, 0x7f090178

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 302
    .local v0, infoView:Landroid/view/View;
    const v10, 0x7f090173

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 303
    .local v5, scrollLayout:Landroid/view/View;
    const v10, 0x7f090176

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 305
    .local v7, scrollerView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    const v10, 0x7f090175

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 307
    .local v6, scrollerTitle:Landroid/view/View;
    const v10, 0x7f090034

    invoke-direct {p0, v10, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setTag(ILjava/lang/Object;)V

    .line 308
    const v10, 0x7f090035

    invoke-direct {p0, v10, v4}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setTag(ILjava/lang/Object;)V

    .line 309
    const v10, 0x7f090036

    invoke-direct {p0, v10, v0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setTag(ILjava/lang/Object;)V

    .line 310
    const v10, 0x7f090037

    invoke-direct {p0, v10, v5}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setTag(ILjava/lang/Object;)V

    .line 311
    const v10, 0x7f090038

    invoke-direct {p0, v10, v7}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setTag(ILjava/lang/Object;)V

    .line 312
    const v10, 0x7f090039

    invoke-direct {p0, v10, v6}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setTag(ILjava/lang/Object;)V

    .line 314
    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/PhotoListView;->getMeasuredHeight()I

    move-result v11

    iget-boolean v10, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v10, :cond_9c

    const/4 v10, 0x0

    :goto_96
    sub-int v10, v11, v10

    invoke-virtual {v3, v10}, Lcom/google/android/apps/plus/views/PhotoLayout;->setFixedHeight(I)V

    .line 333
    .end local v0           #infoView:Landroid/view/View;
    .end local v1           #layout:Landroid/widget/AbsListView$LayoutParams;
    .end local v3           #photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    .end local v4           #photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    .end local v5           #scrollLayout:Landroid/view/View;
    .end local v6           #scrollerTitle:Landroid/view/View;
    .end local v7           #scrollerView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    .end local v8           #view:Landroid/view/View;
    :goto_9b
    return-object v8

    .line 314
    .restart local v0       #infoView:Landroid/view/View;
    .restart local v1       #layout:Landroid/widget/AbsListView$LayoutParams;
    .restart local v3       #photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    .restart local v4       #photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    .restart local v5       #scrollLayout:Landroid/view/View;
    .restart local v6       #scrollerTitle:Landroid/view/View;
    .restart local v7       #scrollerView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    .restart local v8       #view:Landroid/view/View;
    :cond_9c
    iget v10, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    goto :goto_96

    .line 321
    .end local v0           #infoView:Landroid/view/View;
    .end local v1           #layout:Landroid/widget/AbsListView$LayoutParams;
    .end local v3           #photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    .end local v4           #photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    .end local v5           #scrollLayout:Landroid/view/View;
    .end local v6           #scrollerTitle:Landroid/view/View;
    .end local v7           #scrollerView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    .end local v8           #view:Landroid/view/View;
    :pswitch_9f
    const v10, 0x7f030080

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    goto :goto_9b

    .line 325
    :pswitch_a8
    const v10, 0x7f030018

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    goto :goto_9b

    .line 329
    :pswitch_b1
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mCommentLoadView:Landroid/view/View;

    goto :goto_9b

    .line 333
    :pswitch_b4
    const v10, 0x7f03008c

    const/4 v11, 0x0

    invoke-virtual {v2, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    goto :goto_9b

    .line 290
    nop

    :pswitch_data_be
    .packed-switch 0x1
        :pswitch_25
        :pswitch_9f
        :pswitch_a8
        :pswitch_b4
        :pswitch_b1
    .end packed-switch
.end method

.method public final removeFlaggedComment(Ljava/lang/String;)V
    .registers 3
    .parameter "commentId"

    .prologue
    .line 667
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 668
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->notifyDataSetChanged()V

    .line 669
    return-void
.end method

.method public final resetPhotoView()V
    .registers 3

    .prologue
    .line 360
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 361
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_13

    .line 362
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setPhotoLoading(Z)V

    .line 363
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindPhoto(Landroid/graphics/Bitmap;)V

    .line 365
    :cond_13
    return-void
.end method

.method public final resetViews()V
    .registers 3

    .prologue
    .line 616
    const v1, 0x7f090035

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    .line 618
    .local v0, photoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;
    if-eqz v0, :cond_e

    .line 619
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    .line 621
    :cond_e
    return-void
.end method

.method public final setActionBarHeight(I)V
    .registers 7
    .parameter "actionBarHeight"

    .prologue
    const/4 v2, 0x0

    .line 428
    iget v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    if-eq p1, v3, :cond_2c

    const/4 v0, 0x1

    .line 429
    .local v0, heightChanged:Z
    :goto_6
    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    .line 430
    if-eqz v0, :cond_2b

    iget v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    if-lez v3, :cond_2b

    .line 431
    const v3, 0x7f090034

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoLayout;

    .line 432
    .local v1, photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    if-eqz v1, :cond_28

    .line 433
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoListView;->getMeasuredHeight()I

    move-result v3

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    if-eqz v4, :cond_2e

    :goto_23
    sub-int v2, v3, v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PhotoLayout;->setFixedHeight(I)V

    .line 436
    :cond_28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->notifyDataSetChanged()V

    .line 438
    .end local v1           #photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    :cond_2b
    return-void

    .end local v0           #heightChanged:Z
    :cond_2c
    move v0, v2

    .line 428
    goto :goto_6

    .line 433
    .restart local v0       #heightChanged:Z
    .restart local v1       #photoLayout:Lcom/google/android/apps/plus/views/PhotoLayout;
    :cond_2e
    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionBarHeight:I

    goto :goto_23
.end method

.method public final setFlaggedComments(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p1, flaggedComments:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFlaggedComments:Ljava/util/HashSet;

    .line 164
    return-void
.end method

.method public final setFullScreen$25decb5(Z)V
    .registers 4
    .parameter "fullScreen"

    .prologue
    const/4 v1, 0x0

    .line 444
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mFullScreen:Z

    .line 446
    if-eqz p1, :cond_11

    .line 448
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mExpandTags:Z

    if-eqz v0, :cond_c

    .line 449
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->toggleTags()V

    .line 451
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/plus/views/PhotoListView;->setSelectionFromTop(II)V

    .line 453
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mList:Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/PhotoListView;->disableScrolling(Z)V

    .line 454
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1e

    .line 455
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mDisableSpacer:Z

    .line 457
    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->notifyDataSetChanged()V

    .line 458
    return-void
.end method

.method public final setShowPhotoOnly(Z)V
    .registers 2
    .parameter "showPhotoOnly"

    .prologue
    .line 464
    iput-boolean p1, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mShowPhotoOnly:Z

    .line 465
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 3
    .parameter "newCursor"

    .prologue
    .line 341
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mActionState:Ljava/lang/Integer;

    .line 342
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final toggleTags()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 471
    const v2, 0x7f090038

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 472
    .local v1, scrollView:Lcom/google/android/apps/plus/views/PhotoTagScroller;
    const v2, 0x7f090039

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 474
    .local v0, scrollTitleView:Landroid/widget/TextView;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mExpandTags:Z

    if-nez v2, :cond_2e

    move v2, v3

    :goto_19
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mExpandTags:Z

    .line 475
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mExpandTags:Z

    if-eqz v2, :cond_30

    .line 476
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mExpandTags:Z

    if-nez v2, :cond_24

    move v4, v3

    :cond_24
    invoke-virtual {v1, v4, v3}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hideTags(ZZ)V

    .line 477
    const v2, 0x7f08007b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 482
    :goto_2d
    return-void

    :cond_2e
    move v2, v4

    .line 474
    goto :goto_19

    .line 479
    :cond_30
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->mExpandTags:Z

    if-nez v2, :cond_35

    move v4, v3

    :cond_35
    invoke-virtual {v1, v4, v3}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hideTags(ZZ)V

    .line 480
    const v2, 0x7f08007a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2d
.end method
