.class public Lcom/google/android/apps/plus/views/ImageCardView;
.super Lcom/google/android/apps/plus/views/StreamCardView;
.source "ImageCardView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;


# static fields
.field private static sAlbumBitmap:Landroid/graphics/Bitmap;

.field private static sImageCardViewInitialized:Z

.field protected static sImageTopAreaBackgroundPaint:Landroid/graphics/Paint;

.field private static sVideoBitmap:Landroid/graphics/Bitmap;


# instance fields
.field protected mAlbumId:Ljava/lang/String;

.field protected mDesiredHeight:I

.field protected mDesiredWidth:I

.field protected mDestRect:Landroid/graphics/Rect;

.field protected mImage:Lcom/google/android/apps/plus/views/MediaImage;

.field protected mIsAlbum:Z

.field private mIsPWA:Z

.field protected mIsVideo:Z

.field private mMediaClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field protected mMediaCount:I

.field protected mMediaLinkUrl:Ljava/lang/String;

.field protected mMediaOwnerId:Ljava/lang/String;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field protected mThumbnailUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/StreamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    sget-boolean v1, Lcom/google/android/apps/plus/views/ImageCardView;->sImageCardViewInitialized:Z

    if-nez v1, :cond_2d

    .line 71
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/ImageCardView;->sImageCardViewInitialized:Z

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 75
    .local v0, res:Landroid/content/res/Resources;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 76
    sput-object v1, Lcom/google/android/apps/plus/views/ImageCardView;->sImageTopAreaBackgroundPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a0001

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 78
    const v1, 0x7f020129

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ImageCardView;->sVideoBitmap:Landroid/graphics/Bitmap;

    .line 79
    const v1, 0x7f020128

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ImageCardView;->sAlbumBitmap:Landroid/graphics/Bitmap;

    .line 82
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_2d
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    .line 83
    return-void
.end method


# virtual methods
.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .registers 15
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v8, 0x0

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 244
    .local v6, bitmap:Landroid/graphics/Bitmap;
    if-eqz v6, :cond_62

    const/4 v4, 0x1

    .line 246
    .local v4, hasImage:Z
    :goto_f
    sget-object v5, Lcom/google/android/apps/plus/views/ImageCardView;->sImageTopAreaBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v1, p1

    move v2, p4

    move v3, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ImageCardView;->drawMediaTopAreaStage(Landroid/graphics/Canvas;IIZLandroid/graphics/Paint;)V

    .line 248
    if-eqz v4, :cond_21

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/ImageCardView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v8, v0, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 253
    :cond_21
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    if-eqz v0, :cond_64

    .line 254
    sget-object v7, Lcom/google/android/apps/plus/views/ImageCardView;->sVideoBitmap:Landroid/graphics/Bitmap;

    .line 261
    .local v7, centerBitmap:Landroid/graphics/Bitmap;
    :goto_27
    if-eqz v7, :cond_52

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v7, v0, v1, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 268
    :cond_52
    invoke-virtual {p0, p1, p4, p5}, Lcom/google/android/apps/plus/views/ImageCardView;->drawMediaTopAreaShadow(Landroid/graphics/Canvas;II)V

    .line 269
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageCardView;->drawTagBarIconAndBackground(Landroid/graphics/Canvas;II)V

    .line 270
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ImageCardView;->drawPlusOneBar(Landroid/graphics/Canvas;)V

    .line 271
    invoke-virtual {p0, p1, p2, p4, p5}, Lcom/google/android/apps/plus/views/ImageCardView;->drawMediaBottomArea$1be95c43(Landroid/graphics/Canvas;III)I

    .line 272
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ImageCardView;->drawWhatsHot(Landroid/graphics/Canvas;)V

    .line 273
    return p5

    .line 244
    .end local v4           #hasImage:Z
    .end local v7           #centerBitmap:Landroid/graphics/Bitmap;
    :cond_62
    const/4 v4, 0x0

    goto :goto_f

    .line 255
    .restart local v4       #hasImage:Z
    :cond_64
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsAlbum:Z

    if-eqz v0, :cond_6b

    .line 256
    sget-object v7, Lcom/google/android/apps/plus/views/ImageCardView;->sAlbumBitmap:Landroid/graphics/Bitmap;

    .restart local v7       #centerBitmap:Landroid/graphics/Bitmap;
    goto :goto_27

    .line 258
    .end local v7           #centerBitmap:Landroid/graphics/Bitmap;
    :cond_6b
    const/4 v7, 0x0

    .restart local v7       #centerBitmap:Landroid/graphics/Bitmap;
    goto :goto_27
.end method

.method public final getAlbumId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mAlbumId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDesiredHeight()I
    .registers 2

    .prologue
    .line 314
    iget v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDesiredHeight:I

    return v0
.end method

.method public final getDesiredWidth()I
    .registers 2

    .prologue
    .line 309
    iget v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDesiredWidth:I

    return v0
.end method

.method public final getMediaCount()I
    .registers 2

    .prologue
    .line 319
    iget v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaCount:I

    return v0
.end method

.method public final getMediaLinkUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaLinkUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method public final init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 25
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 90
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/plus/views/StreamCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 93
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 94
    .local v11, mediaBytes:[B
    if-eqz v11, :cond_156

    .line 95
    invoke-static {v11}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v8

    .line 96
    .local v8, dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaCount:I

    .line 97
    const/4 v9, 0x0

    .local v9, i:I
    array-length v10, v8

    .local v10, length:I
    :goto_17
    if-ge v9, v10, :cond_14a

    .line 98
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v15

    .line 100
    .local v15, type:I
    const/4 v1, 0x1

    if-eq v15, v1, :cond_28

    const/4 v1, 0x3

    if-eq v15, v1, :cond_28

    const/4 v1, 0x2

    if-ne v15, v1, :cond_137

    .line 102
    :cond_28
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v14

    .line 103
    .local v14, thumbnailUrl:Ljava/lang/String;
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_137

    .line 104
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaCount:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaCount:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_137

    .line 108
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaLinkUrl:Ljava/lang/String;

    .line 112
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 114
    const/4 v1, 0x2

    if-ne v15, v1, :cond_6b

    .line 115
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    .line 116
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v12

    .line 117
    .local v12, mediaUrl:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/apps/plus/util/ImageUtils;->rewriteYoutubeMediaUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 118
    .local v13, rewrittenUrl:Ljava/lang/String;
    invoke-static {v12, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6b

    .line 119
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 123
    .end local v12           #mediaUrl:Ljava/lang/String;
    .end local v13           #rewrittenUrl:Ljava/lang/String;
    :cond_6b
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getPlayerWidth()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mDesiredWidth:I

    .line 124
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getPlayerHeight()I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mDesiredHeight:I

    .line 126
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mAlbumId:Ljava/lang/String;

    .line 127
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaOwnerId:Ljava/lang/String;

    .line 129
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getPhotoId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 130
    .local v3, photoId:J
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    if-eqz v1, :cond_13b

    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 132
    .local v6, videoUri:Landroid/net/Uri;
    :goto_ad
    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    if-eqz v7, :cond_13e

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_bf
    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 135
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->isPWA()Z

    move-result v1

    if-eqz v1, :cond_142

    const-wide/16 v1, 0x0

    cmp-long v1, v3, v1

    if-eqz v1, :cond_142

    const/4 v1, 0x1

    :goto_d5
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsPWA:Z

    .line 137
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mTag:Ljava/lang/CharSequence;

    if-nez v1, :cond_108

    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_108

    .line 138
    aget-object v1, v8, v9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mTag:Ljava/lang/CharSequence;

    .line 139
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    if-eqz v1, :cond_144

    sget-object v1, Lcom/google/android/apps/plus/views/ImageCardView;->sTagVideoBitmaps:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    :goto_104
    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mTagIcon:Landroid/graphics/Bitmap;

    .line 142
    :cond_108
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    if-nez v1, :cond_137

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mDisplaySizeType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_11c

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mDisplaySizeType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_137

    .line 144
    :cond_11c
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&google_plus:card_type=nonsquare"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 97
    .end local v3           #photoId:J
    .end local v6           #videoUri:Landroid/net/Uri;
    .end local v14           #thumbnailUrl:Ljava/lang/String;
    :cond_137
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_17

    .line 130
    .restart local v3       #photoId:J
    .restart local v14       #thumbnailUrl:Ljava/lang/String;
    :cond_13b
    const/4 v6, 0x0

    goto/16 :goto_ad

    .line 132
    .restart local v6       #videoUri:Landroid/net/Uri;
    :cond_13e
    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto/16 :goto_bf

    .line 135
    :cond_142
    const/4 v1, 0x0

    goto :goto_d5

    .line 139
    :cond_144
    sget-object v1, Lcom/google/android/apps/plus/views/ImageCardView;->sTagAlbumBitmaps:[Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    goto :goto_104

    .line 149
    .end local v3           #photoId:J
    .end local v6           #videoUri:Landroid/net/Uri;
    .end local v14           #thumbnailUrl:Ljava/lang/String;
    .end local v15           #type:I
    :cond_14a
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaCount:I

    const/4 v2, 0x1

    if-le v1, v2, :cond_157

    const/4 v1, 0x1

    :goto_152
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsAlbum:Z

    .line 151
    .end local v8           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v9           #i:I
    .end local v10           #length:I
    :cond_156
    return-void

    .line 149
    .restart local v8       #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .restart local v9       #i:I
    .restart local v10       #length:I
    :cond_157
    const/4 v1, 0x0

    goto :goto_152
.end method

.method protected final layoutElements(IIII)I
    .registers 17
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 195
    sget v1, Lcom/google/android/apps/plus/views/ImageCardView;->sXDoublePadding:I

    add-int v8, p3, v1

    .line 196
    .local v8, imageWidth:I
    sget v1, Lcom/google/android/apps/plus/views/ImageCardView;->sYDoublePadding:I

    add-int v1, v1, p4

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageCardView;->getMediaHeightPercentage()F

    move-result v5

    mul-float/2addr v1, v5

    float-to-int v9, v1

    .line 197
    .local v9, imageHeight:I
    move v3, v8

    .line 198
    .local v3, fetchWidth:I
    move v4, v9

    .line 200
    .local v4, fetchHeight:I
    invoke-static {}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getScreenDisplayType()I

    move-result v1

    const/4 v5, 0x1

    if-ne v1, v5, :cond_20

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDisplaySizeType:I

    if-nez v1, :cond_20

    .line 202
    mul-int/lit8 v3, v8, 0x2

    .line 203
    mul-int/lit8 v4, v9, 0x2

    .line 206
    :cond_20
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    if-eqz v1, :cond_84

    const/4 v2, 0x2

    .line 208
    .local v2, type:I
    :goto_25
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsPWA:Z

    if-eqz v1, :cond_86

    .line 209
    new-instance v0, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/content/RemoteImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .line 214
    .local v0, request:Lcom/google/android/apps/plus/content/ImageRequest;
    :goto_30
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v1, :cond_39

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->cancelRequest()V

    .line 217
    :cond_39
    new-instance v1, Lcom/google/android/apps/plus/views/MediaImage;

    invoke-direct {v1, p0, v0, v8, v9}, Lcom/google/android/apps/plus/views/MediaImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;II)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 218
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v1, :cond_51

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ImageCardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 222
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    .line 225
    :cond_51
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsPWA:Z

    if-eqz v1, :cond_65

    .line 226
    new-instance v5, Lcom/google/android/apps/plus/views/ClickableRect;

    const/4 v11, 0x0

    move v6, p1

    move v7, p2

    move-object v10, p0

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ImageCardView;->addClickableItemAtStart(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 230
    :cond_65
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageCardView;->createTagBar(III)I

    .line 231
    sget v1, Lcom/google/android/apps/plus/views/ImageCardView;->sTopBorderPadding:I

    add-int/2addr v1, v9

    sget v5, Lcom/google/android/apps/plus/views/ImageCardView;->sYPadding:I

    sub-int/2addr v1, v5

    invoke-virtual {p0, p1, v1, p3}, Lcom/google/android/apps/plus/views/ImageCardView;->createPlusOneBar(III)I

    .line 232
    invoke-virtual/range {p0 .. p4}, Lcom/google/android/apps/plus/views/ImageCardView;->createMediaBottomArea(IIII)I

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    sget v5, Lcom/google/android/apps/plus/views/ImageCardView;->sLeftBorderPadding:I

    sget v6, Lcom/google/android/apps/plus/views/ImageCardView;->sTopBorderPadding:I

    sget v7, Lcom/google/android/apps/plus/views/ImageCardView;->sLeftBorderPadding:I

    add-int/2addr v7, v8

    sget v10, Lcom/google/android/apps/plus/views/ImageCardView;->sTopBorderPadding:I

    add-int/2addr v10, v9

    invoke-virtual {v1, v5, v6, v7, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 237
    return p4

    .line 206
    .end local v0           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v2           #type:I
    :cond_84
    const/4 v2, 0x3

    goto :goto_25

    .line 211
    .restart local v2       #type:I
    :cond_86
    new-instance v0, Lcom/google/android/apps/plus/content/MediaImageRequest;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .restart local v0       #request:Lcom/google/android/apps/plus/content/ImageRequest;
    goto :goto_30
.end method

.method public final onClickableRectClick$598f98c1()V
    .registers 7

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

    if-eqz v0, :cond_12

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mAlbumId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaOwnerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;->onMediaClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;ZLcom/google/android/apps/plus/views/StreamCardView;)V

    .line 333
    :cond_12
    return-void
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageCardView;->invalidate()V

    .line 282
    :cond_10
    return-void
.end method

.method public onRecycle()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 155
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onRecycle()V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_e

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->cancelRequest()V

    .line 161
    :cond_e
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaLinkUrl:Ljava/lang/String;

    .line 162
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mThumbnailUrl:Ljava/lang/String;

    .line 163
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 164
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 166
    iput v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDesiredWidth:I

    .line 167
    iput v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mDesiredHeight:I

    .line 168
    iput v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaCount:I

    .line 169
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mAlbumId:Ljava/lang/String;

    .line 170
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaOwnerId:Ljava/lang/String;

    .line 171
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mAlbumId:Ljava/lang/String;

    .line 172
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsVideo:Z

    .line 173
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mIsAlbum:Z

    .line 174
    return-void
.end method

.method public final onRemoteImageChanged(Lcom/google/android/apps/plus/content/ImageRequest;Landroid/graphics/Bitmap;)V
    .registers 8
    .parameter "request"
    .parameter "bitmap"

    .prologue
    .line 286
    instance-of v1, p1, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v1, :cond_9

    .line 295
    :cond_8
    :goto_8
    return-void

    :cond_9
    move-object v0, p1

    .line 289
    check-cast v0, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    .line 290
    .local v0, remoteRequest:Lcom/google/android/apps/plus/content/RemoteImageRequest;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/RemoteImageRequest;->getPhotoId()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_8

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MediaImage;->invalidate()V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ImageCardView;->invalidate()V

    goto :goto_8
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 178
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onStart()V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_c

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MediaImage;->refreshIfInvalidated()V

    .line 182
    :cond_c
    return-void
.end method

.method public final onStop()V
    .registers 4

    .prologue
    .line 186
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onStop()V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    if-eqz v0, :cond_e

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardView;->mImage:Lcom/google/android/apps/plus/views/MediaImage;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/MediaImage;->setBitmap(Landroid/graphics/Bitmap;Z)V

    .line 190
    :cond_e
    return-void
.end method
