.class public final Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "AlbumGridViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;
    }
.end annotation


# instance fields
.field private final mAlbumType:Ljava/lang/String;

.field private final mClickListener:Landroid/view/View$OnClickListener;

.field private final mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private final mLandscape:Z

.field private final mLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mSelectedMediaRefs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mThumbnailSize:I

.field private final mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;)V
    .registers 15
    .parameter "context"
    .parameter "cursor"
    .parameter "albumType"
    .parameter "gridView"
    .parameter "clickListener"
    .parameter "longClickListener"
    .parameter "viewUseListener"

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_c7

    move v0, v1

    :goto_13
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLandscape:Z

    .line 81
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLandscape:Z

    if-eqz v0, :cond_ca

    :goto_19
    invoke-virtual {p4, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    .line 83
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v2, 0x7f0d004a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {p4, v2, v2, v2, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    invoke-virtual {p4, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int v4, v0, v1

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v4, v2

    sub-int v4, v0, v4

    div-int v5, v4, v1

    div-int/2addr v4, v5

    div-int v5, v3, v1

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v2, v5

    sub-int v2, v3, v2

    div-int v1, v2, v1

    div-int v1, v2, v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/2addr v2, v1

    const-string v4, "AlbumGridViewAdapter"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_af

    const-string v4, "AlbumGridViewAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Usable width: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", usable height: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "AlbumGridViewAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Thumbnail size: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", columns: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_af
    invoke-virtual {p4, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    iput v1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mThumbnailSize:I

    .line 85
    iput-object p7, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    .line 86
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    .line 87
    iput-object p6, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 88
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 89
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mAlbumType:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;)V

    invoke-virtual {p4, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    .line 107
    return-void

    .line 79
    :cond_c7
    const/4 v0, 0x0

    goto/16 :goto_13

    :cond_ca
    move v1, v2

    .line 81
    goto/16 :goto_19
.end method

.method private createMediaRef(Ljava/lang/String;JZLjava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 13
    .parameter "ownerGaiaId"
    .parameter "photoId"
    .parameter "isVideo"
    .parameter "unsizedPhotoUrl"

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mAlbumType:Ljava/lang/String;

    const-string v1, "camera_photos"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 144
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    if-eqz p4, :cond_1c

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_17
    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 149
    :goto_1b
    return-object v0

    .line 144
    :cond_1c
    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_17

    .line 147
    :cond_1f
    iget v0, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mThumbnailSize:I

    invoke-static {v0, p5}, Lcom/google/android/apps/plus/util/ImageUtils;->getCroppedAndResizedUrl(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 149
    .local v4, photoUrl:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v1, p1

    move-wide v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    goto :goto_1b
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 22
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 158
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 160
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    .line 162
    .local v9, cursorPosition:I
    const v1, 0x7f080079

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v17, v9, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v15}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 164
    const v1, 0x7f09003d

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v15}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object/from16 v7, p1

    .line 166
    check-cast v7, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;

    .line 167
    .local v7, albumPhoto:Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;
    const/16 v1, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_149

    .line 169
    const/4 v1, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 170
    .local v2, ownerGaiaId:Ljava/lang/String;
    const/16 v1, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 171
    .local v3, photoId:J
    const/16 v1, 0xb

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_112

    const/4 v5, 0x1

    .line 172
    .local v5, isVideo:Z
    :goto_71
    const/16 v1, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .local v6, unsizedPhotoUrl:Ljava/lang/String;
    move-object/from16 v1, p0

    .line 174
    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->createMediaRef(Ljava/lang/String;JZLjava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v12

    .line 178
    .local v12, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mAlbumType:Ljava/lang/String;

    const-string v15, "camera_photos"

    invoke-static {v1, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_115

    .line 179
    new-instance v10, Lcom/google/android/apps/plus/content/LocalImageRequest;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mThumbnailSize:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mThumbnailSize:I

    invoke-direct {v10, v12, v1, v15}, Lcom/google/android/apps/plus/content/LocalImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .line 184
    .local v10, imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    :goto_98
    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setRequest(Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 185
    invoke-virtual {v7, v12}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setTag(Ljava/lang/Object;)V

    .line 188
    const/4 v1, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_124

    const/4 v14, 0x0

    .line 190
    .local v14, plusOneCount:I
    :goto_a8
    if-lez v14, :cond_12d

    .line 191
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setPlusOneCount(Ljava/lang/Integer;)V

    .line 197
    :goto_b1
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_132

    const/4 v8, 0x0

    .line 199
    .local v8, commentCount:I
    :goto_bb
    if-lez v8, :cond_13a

    .line 200
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setCommentCount(Ljava/lang/Integer;)V

    .line 206
    :goto_c4
    const/16 v1, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_13f

    const/4 v1, 0x1

    :goto_cf
    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setNotification(Z)V

    .line 208
    invoke-virtual {v7, v5}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setIsVideo(Z)V

    .line 211
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mSelectedMediaRefs:Ljava/util/HashSet;

    if-eqz v1, :cond_141

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mSelectedMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1, v12}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_141

    .line 212
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/ColumnGridView;->select(I)V

    .line 220
    .end local v2           #ownerGaiaId:Ljava/lang/String;
    .end local v3           #photoId:J
    .end local v5           #isVideo:Z
    .end local v6           #unsizedPhotoUrl:Ljava/lang/String;
    .end local v8           #commentCount:I
    .end local v10           #imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v12           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v14           #plusOneCount:I
    :goto_ec
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mLandscape:Z

    if-eqz v1, :cond_14e

    const/4 v13, 0x1

    .line 222
    .local v13, orientation:I
    :goto_f3
    new-instance v11, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v1, -0x3

    invoke-direct {v11, v13, v1}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    .line 224
    .local v11, lp:Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    if-eqz v1, :cond_111

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCount()I

    move-result v1

    if-ge v9, v1, :cond_111

    .line 227
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;

    invoke-interface {v1, v9}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;->onViewUsed(I)V

    .line 229
    :cond_111
    return-void

    .line 171
    .end local v11           #lp:Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    .end local v13           #orientation:I
    .restart local v2       #ownerGaiaId:Ljava/lang/String;
    .restart local v3       #photoId:J
    :cond_112
    const/4 v5, 0x0

    goto/16 :goto_71

    .line 181
    .restart local v5       #isVideo:Z
    .restart local v6       #unsizedPhotoUrl:Ljava/lang/String;
    .restart local v12       #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_115
    new-instance v10, Lcom/google/android/apps/plus/content/RemoteImageRequest;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mThumbnailSize:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mThumbnailSize:I

    invoke-direct {v10, v12, v1, v15}, Lcom/google/android/apps/plus/content/RemoteImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .restart local v10       #imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    goto/16 :goto_98

    .line 188
    :cond_124
    const/4 v1, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    goto/16 :goto_a8

    .line 193
    .restart local v14       #plusOneCount:I
    :cond_12d
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setPlusOneCount(Ljava/lang/Integer;)V

    goto :goto_b1

    .line 197
    :cond_132
    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    goto :goto_bb

    .line 202
    .restart local v8       #commentCount:I
    :cond_13a
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setCommentCount(Ljava/lang/Integer;)V

    goto :goto_c4

    .line 206
    :cond_13f
    const/4 v1, 0x0

    goto :goto_cf

    .line 214
    :cond_141
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v9}, Lcom/google/android/apps/plus/views/ColumnGridView;->deselect(I)V

    goto :goto_ec

    .line 217
    .end local v2           #ownerGaiaId:Ljava/lang/String;
    .end local v3           #photoId:J
    .end local v5           #isVideo:Z
    .end local v6           #unsizedPhotoUrl:Ljava/lang/String;
    .end local v8           #commentCount:I
    .end local v10           #imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v12           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v14           #plusOneCount:I
    :cond_149
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setRequest(Lcom/google/android/apps/plus/content/ImageRequest;)V

    goto :goto_ec

    .line 220
    :cond_14e
    const/4 v13, 0x2

    goto :goto_f3
.end method

.method public final getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 9
    .parameter "position"

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    .line 131
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_2e

    invoke-interface {v6, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 132
    const/4 v0, 0x5

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, ownerGaiaId:Ljava/lang/String;
    const/16 v0, 0x8

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 134
    .local v2, photoId:J
    const/16 v0, 0xb

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2c

    const/4 v4, 0x1

    .line 135
    .local v4, isVideo:Z
    :goto_20
    const/16 v0, 0x9

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .local v5, unsizedPhotoUrl:Ljava/lang/String;
    move-object v0, p0

    .line 136
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->createMediaRef(Ljava/lang/String;JZLjava/lang/String;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    .line 138
    .end local v1           #ownerGaiaId:Ljava/lang/String;
    .end local v2           #photoId:J
    .end local v4           #isVideo:Z
    .end local v5           #unsizedPhotoUrl:Ljava/lang/String;
    :goto_2b
    return-object v0

    .line 134
    .restart local v1       #ownerGaiaId:Ljava/lang/String;
    .restart local v2       #photoId:J
    :cond_2c
    const/4 v4, 0x0

    goto :goto_20

    .line 138
    .end local v1           #ownerGaiaId:Ljava/lang/String;
    .end local v2           #photoId:J
    :cond_2e
    const/4 v0, 0x0

    goto :goto_2b
.end method

.method public final getTimestampForItem(I)J
    .registers 5
    .parameter "item"

    .prologue
    const/16 v2, 0xc

    .line 120
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 121
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_19

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 122
    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_19

    .line 123
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 126
    :goto_18
    return-wide v1

    :cond_19
    const-wide/16 v1, 0x0

    goto :goto_18
.end method

.method public final hasStableIds()Z
    .registers 2

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "viewGroup"

    .prologue
    .line 236
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 237
    .local v0, layoutInflater:Landroid/view/LayoutInflater;
    const v1, 0x7f030007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public final onResume()V
    .registers 5

    .prologue
    .line 258
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onResume()V

    .line 260
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v3, :cond_26

    .line 261
    const/4 v1, 0x0

    .local v1, i:I
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    .local v2, size:I
    :goto_e
    if-ge v1, v2, :cond_21

    .line 262
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsImageView;

    .line 263
    .local v0, albumPhoto:Lcom/google/android/apps/plus/views/EsImageView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onResume()V

    .line 264
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->invalidate()V

    .line 261
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 266
    .end local v0           #albumPhoto:Lcom/google/android/apps/plus/views/EsImageView;
    :cond_21
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->onResume()V

    .line 268
    .end local v1           #i:I
    .end local v2           #size:I
    :cond_26
    return-void
.end method

.method public final onStop()V
    .registers 5

    .prologue
    .line 245
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->onStop()V

    .line 247
    const/4 v1, 0x0

    .local v1, i:I
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v2

    .local v2, size:I
    :goto_a
    if-ge v1, v2, :cond_1a

    .line 248
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsImageView;

    .line 249
    .local v0, albumPhoto:Lcom/google/android/apps/plus/views/EsImageView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onStop()V

    .line 247
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 251
    .end local v0           #albumPhoto:Lcom/google/android/apps/plus/views/EsImageView;
    :cond_1a
    return-void
.end method

.method public final setSelectedMediaRefs(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, selectedPhotoUrls:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/google/android/apps/plus/api/MediaRef;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->mSelectedMediaRefs:Ljava/util/HashSet;

    .line 117
    return-void
.end method
