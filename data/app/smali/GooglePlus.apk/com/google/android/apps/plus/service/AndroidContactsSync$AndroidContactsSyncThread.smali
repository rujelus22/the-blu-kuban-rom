.class final Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;
.super Ljava/lang/Thread;
.source "AndroidContactsSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/AndroidContactsSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AndroidContactsSyncThread"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private volatile mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private volatile mThreadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 735
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 730
    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 736
    iput-object p1, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mContext:Landroid/content/Context;

    .line 737
    const-string v0, "AndroidContactsSync"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->setName(Ljava/lang/String;)V

    .line 738
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 722
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->syncContactsForCurrentAccount()V

    return-void
.end method

.method private syncContactsForCurrentAccount()V
    .registers 4

    .prologue
    .line 768
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    new-instance v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncContactsForCurrentAccount(Landroid/content/Context;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_13} :catch_14

    .line 773
    :goto_13
    return-void

    .line 769
    :catch_14
    move-exception v0

    .line 770
    .local v0, t:Ljava/lang/Throwable;
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_13
.end method


# virtual methods
.method public final cancel()V
    .registers 3

    .prologue
    .line 804
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    .line 806
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_f

    .line 807
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 809
    :cond_f
    return-void
.end method

.method public final requestSync(Z)V
    .registers 6
    .parameter "immediate"

    .prologue
    const/4 v3, 0x0

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_18

    .line 783
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 784
    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz p1, :cond_19

    const-wide/16 v0, 0x1f4

    :goto_15
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 787
    :cond_18
    return-void

    .line 784
    :cond_19
    const-wide/16 v0, 0x1388

    goto :goto_15
.end method

.method public final run()V
    .registers 2

    .prologue
    .line 745
    const/16 v0, 0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 746
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 748
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread$1;-><init>(Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    .line 760
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->syncContactsForCurrentAccount()V

    .line 763
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 764
    return-void
.end method
