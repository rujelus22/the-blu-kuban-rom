.class public final Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "CheckMessageSentOperation.java"


# static fields
.field private static final sHandler:Landroid/os/Handler;


# instance fields
.field final mFlags:I

.field final mMessageRowId:J

.field mSendFailed:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 13
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->sHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)V
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"
    .parameter "flags"

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 33
    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mMessageRowId:J

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mSendFailed:Z

    .line 35
    iput p5, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mFlags:I

    .line 36
    return-void
.end method

.method private static recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "event"

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->sHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$2;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 119
    return-void
.end method


# virtual methods
.method public final execute()V
    .registers 7

    .prologue
    .line 41
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mMessageRowId:J

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->checkMessageSentLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)I

    move-result v0

    .line 44
    .local v0, messageStatus:I
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1c

    .line 46
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->sHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$1;-><init>(Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;)V

    const-wide/16 v3, 0x4e20

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 74
    :goto_1b
    return-void

    .line 47
    :cond_1c
    const/16 v1, 0x8

    if-ne v0, v1, :cond_3d

    .line 49
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mSendFailed:Z

    .line 50
    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mFlags:I

    packed-switch v1, :pswitch_data_58

    goto :goto_1b

    .line 57
    :pswitch_29
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_1b

    .line 52
    :pswitch_33
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_1b

    .line 65
    :cond_3d
    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mFlags:I

    packed-switch v1, :pswitch_data_60

    goto :goto_1b

    .line 72
    :pswitch_43
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_1b

    .line 67
    :pswitch_4d
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_1b

    .line 50
    nop

    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_29
        :pswitch_33
    .end packed-switch

    .line 65
    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_43
        :pswitch_4d
    .end packed-switch
.end method

.method public final getResultValue()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mSendFailed:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
