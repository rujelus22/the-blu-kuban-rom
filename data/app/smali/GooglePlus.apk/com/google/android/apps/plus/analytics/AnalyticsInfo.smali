.class public final Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
.super Ljava/lang/Object;
.source "AnalyticsInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final mCustomValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

.field private final mStartTime:J

.field private final mStartView:Lcom/google/android/apps/plus/analytics/OzViews;


# direct methods
.method constructor <init>()V
    .registers 7

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    move-object v0, p0

    move-object v2, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/analytics/OzViews;)V
    .registers 4
    .parameter "startView"

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartTime:J

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mCustomValues:Ljava/util/HashMap;

    .line 34
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;J)V
    .registers 11
    .parameter "startView"
    .parameter "endView"
    .parameter "startTime"

    .prologue
    .line 67
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V
    .registers 7
    .parameter "startView"
    .parameter "endView"
    .parameter "startTime"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/analytics/OzViews;",
            "Lcom/google/android/apps/plus/analytics/OzViews;",
            "J",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p5, customValues:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 51
    iput-object p2, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 52
    iput-wide p3, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartTime:J

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p5}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mCustomValues:Ljava/util/HashMap;

    .line 54
    return-void
.end method


# virtual methods
.method public final getEndView()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mEndView:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final getStartTimeMsec()J
    .registers 3

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartTime:J

    return-wide v0
.end method

.method public final getStartView()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->mStartView:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method
