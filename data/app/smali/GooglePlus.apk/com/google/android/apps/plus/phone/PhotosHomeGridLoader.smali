.class public final Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PhotosHomeGridLoader.java"


# static fields
.field private static final CAMERA_PHOTO_PROJECTION:[Ljava/lang/String;

.field private static final CAMERA_URI:[Landroid/net/Uri;

.field public static final PROJECTION:[Ljava/lang/String;

.field private static sRowId:J


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private final mPhotosHome:Z

.field private final mShowLocalCameraAlbum:Z

.field private mUserName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "datetaken"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->CAMERA_PHOTO_PROJECTION:[Ljava/lang/String;

    .line 47
    new-array v0, v6, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_IMAGES_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_VIDEO_URI:Landroid/net/Uri;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->CAMERA_URI:[Landroid/net/Uri;

    .line 58
    const/16 v0, 0x13

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "photo_count"

    aput-object v1, v0, v3

    const-string v1, "notification_count"

    aput-object v1, v0, v4

    const-string v1, "timestamp"

    aput-object v1, v0, v5

    const-string v1, "type"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "owner_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "stream_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "photo_id_1"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "url_1"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "photo_id_2"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "url_2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "photo_id_3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "url_3"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "photo_id_4"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "url_4"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "photo_id_5"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "url_5"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "ownerGaiaId"
    .parameter "userName"
    .parameter "photosHome"
    .parameter "showLocalCameraAlbum"

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 112
    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 117
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 118
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    .line 119
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mUserName:Ljava/lang/String;

    .line 120
    iput-boolean p5, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mPhotosHome:Z

    .line 121
    iput-boolean p6, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mShowLocalCameraAlbum:Z

    .line 122
    return-void
.end method

.method private loadCameraAlbum(Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    .registers 21
    .parameter "matrixCursor"

    .prologue
    .line 556
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 559
    .local v9, albumName:Ljava/lang/String;
    const/16 v16, 0x0

    .line 560
    .local v16, localUrl:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 561
    .local v0, resolver:Landroid/content/ContentResolver;
    const/4 v12, 0x0

    .local v12, i:I
    :goto_1a
    sget-object v1, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->CAMERA_URI:[Landroid/net/Uri;

    array-length v1, v1

    if-ge v12, v1, :cond_55

    .line 562
    sget-object v1, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->CAMERA_URI:[Landroid/net/Uri;

    aget-object v1, v1, v12

    sget-object v2, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->CAMERA_PHOTO_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "datetaken desc"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 565
    .local v15, localCursor:Landroid/database/Cursor;
    if-eqz v15, :cond_46

    :try_start_2f
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_46

    .line 566
    const/4 v1, 0x0

    invoke-interface {v15, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    .line 567
    .local v13, id:J
    sget-object v1, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->CAMERA_URI:[Landroid/net/Uri;

    aget-object v1, v1, v12

    invoke-static {v1, v13, v14}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_45
    .catchall {:try_start_2f .. :try_end_45} :catchall_4e

    move-result-object v16

    .line 570
    .end local v13           #id:J
    :cond_46
    if-eqz v15, :cond_4b

    .line 571
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 561
    :cond_4b
    add-int/lit8 v12, v12, 0x1

    goto :goto_1a

    .line 570
    :catchall_4e
    move-exception v1

    if-eqz v15, :cond_54

    .line 571
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_54
    throw v1

    .line 576
    .end local v15           #localCursor:Landroid/database/Cursor;
    :cond_55
    if-eqz v16, :cond_76

    .line 578
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "camera_photos"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x1

    new-array v10, v1, [Ljava/lang/Long;

    const/4 v1, 0x0

    const-wide/16 v17, 0x0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v10, v1

    const/4 v1, 0x1

    new-array v11, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v16, v11, v1

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V

    .line 583
    :cond_76
    return-void
.end method

.method private static logDelta(J)Ljava/lang/String;
    .registers 9
    .parameter "start"

    .prologue
    const-wide/16 v5, 0x3e8

    .line 587
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long v0, v3, p0

    .line 588
    .local v0, delta:J
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 589
    .local v2, sb:Ljava/lang/StringBuffer;
    div-long v3, v0, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 590
    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 591
    rem-long v3, v0, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 592
    const-string v3, " sec"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 593
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private processAlbumCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    .registers 23
    .parameter "albumCursor"
    .parameter "matrixCursor"

    .prologue
    .line 321
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 322
    .local v16, start:J
    const/4 v6, 0x0

    .line 323
    .local v6, lastAlbumId:Ljava/lang/String;
    const/4 v15, 0x0

    .line 324
    .local v15, rowCount:I
    const/4 v12, 0x0

    .line 325
    .local v12, albumId:Ljava/lang/String;
    const/4 v2, 0x0

    .line 326
    .local v2, count:Ljava/lang/Long;
    const/4 v4, 0x0

    .line 327
    .local v4, timestamp:Ljava/lang/Long;
    const/4 v9, 0x0

    .line 328
    .local v9, title:Ljava/lang/String;
    const/4 v7, 0x0

    .line 329
    .local v7, ownerId:Ljava/lang/String;
    const/4 v8, 0x0

    .line 330
    .local v8, streamId:Ljava/lang/String;
    const/4 v1, 0x1

    new-array v11, v1, [Ljava/lang/String;

    .line 331
    .local v11, url:[Ljava/lang/String;
    const/4 v1, 0x1

    new-array v10, v1, [Ljava/lang/Long;

    .line 333
    .local v10, photoId:[Ljava/lang/Long;
    :cond_12
    :goto_12
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_cd

    .line 338
    const/4 v1, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 339
    if-eqz v12, :cond_12

    .line 341
    invoke-static {v12, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6a

    .line 344
    if-eqz v6, :cond_30

    .line 346
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v1, p2

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V

    .line 350
    :cond_30
    move-object v6, v12

    .line 351
    const/4 v15, 0x0

    .line 352
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_88

    const/4 v2, 0x0

    .line 354
    :goto_3c
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_94

    const/4 v4, 0x0

    .line 356
    :goto_46
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_a0

    const/4 v9, 0x0

    .line 358
    :goto_50
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_a8

    const/4 v7, 0x0

    .line 360
    :goto_5a
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_b0

    const/4 v8, 0x0

    .line 363
    :goto_64
    const/4 v1, 0x1

    new-array v11, v1, [Ljava/lang/String;

    .line 364
    const/4 v1, 0x1

    new-array v10, v1, [Ljava/lang/Long;

    .line 368
    :cond_6a
    if-gtz v15, :cond_12

    .line 372
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_b8

    const/4 v14, 0x0

    .line 374
    .local v14, myUrl:Ljava/lang/String;
    :goto_77
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_c1

    const/4 v13, 0x0

    .line 376
    .local v13, myPhotoId:Ljava/lang/Long;
    :goto_81
    aput-object v14, v11, v15

    .line 377
    aput-object v13, v10, v15

    .line 378
    add-int/lit8 v15, v15, 0x1

    .line 379
    goto :goto_12

    .line 352
    .end local v13           #myPhotoId:Ljava/lang/Long;
    .end local v14           #myUrl:Ljava/lang/String;
    :cond_88
    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_3c

    .line 354
    :cond_94
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_46

    .line 356
    :cond_a0
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_50

    .line 358
    :cond_a8
    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_5a

    .line 360
    :cond_b0
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_64

    .line 372
    :cond_b8
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto :goto_77

    .line 374
    .restart local v14       #myUrl:Ljava/lang/String;
    :cond_c1
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    goto :goto_81

    .line 382
    .end local v14           #myUrl:Ljava/lang/String;
    :cond_cd
    if-eqz v6, :cond_d6

    .line 383
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v1, p2

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V

    .line 388
    :cond_d6
    const-string v1, "PhotosHomeLoader"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_f7

    .line 389
    const-string v1, "PhotosHomeLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "#processAlbumCursor; "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->logDelta(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_f7
    return-void
.end method

.method private static writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V
    .registers 17
    .parameter "cursor"
    .parameter "count"
    .parameter "notificationCount"
    .parameter "timestamp"
    .parameter "type"
    .parameter "albumId"
    .parameter "ownerId"
    .parameter "streamId"
    .parameter "title"
    .parameter "photoId"
    .parameter "url"

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v0

    .line 207
    .local v0, builder:Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;
    sget-wide v2, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->sRowId:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    sput-wide v4, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->sRowId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    .line 217
    if-eqz p9, :cond_46

    .line 218
    const/4 v1, 0x0

    .local v1, i:I
    :goto_35
    array-length v2, p9

    if-ge v1, v2, :cond_46

    .line 219
    aget-object v2, p9, v1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v2

    aget-object v3, p10, v1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    .line 218
    add-int/lit8 v1, v1, 0x1

    goto :goto_35

    .line 224
    .end local v1           #i:I
    :cond_46
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 24

    .prologue
    .line 126
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 127
    .local v16, start:J
    new-instance v15, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v15, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 129
    .local v15, returnCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_2ac

    .line 131
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mPhotosHome:Z

    if-eqz v1, :cond_2f6

    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotosHomeQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "sort_order, _id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_43e

    :try_start_39
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_43e

    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_42
    .catchall {:try_start_39 .. :try_end_42} :catchall_2ce

    const/4 v2, 0x0

    move-object/from16 v18, v2

    :goto_45
    if-nez v18, :cond_43a

    :try_start_47
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    new-instance v1, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    new-instance v6, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v9, 0x0

    const-string v10, "camerasync"

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v7, v2

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotosHomeQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "sort_order, _id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_92
    .catchall {:try_start_47 .. :try_end_92} :catchall_420

    move-result-object v12

    :goto_93
    if-eqz v12, :cond_199

    :try_start_95
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/4 v5, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    new-array v11, v6, [Ljava/lang/String;

    const/4 v6, 0x1

    new-array v10, v6, [Ljava/lang/Long;

    :cond_ab
    :goto_ab
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_192

    const/4 v6, 0x4

    invoke-interface {v12, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const-string v6, "from_my_phone"

    invoke-static {v13, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_c8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v6

    if-nez v6, :cond_ab

    :cond_c8
    invoke-static {v13, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_435

    if-eqz v5, :cond_d5

    const/4 v6, 0x0

    move-object v1, v15

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V

    :cond_d5
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v1, 0x1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_110

    const/4 v1, 0x0

    move-object v3, v1

    :goto_e1
    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_11b

    const/4 v1, 0x0

    move-object v2, v1

    :goto_ea
    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_126

    const/4 v1, 0x0

    :goto_f2
    const-string v4, "photos_of_me"

    invoke-static {v13, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_130

    const v4, 0x7f08006c

    invoke-virtual {v14, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v5, v3

    :goto_102
    const/4 v3, 0x1

    new-array v11, v3, [Ljava/lang/String;

    const/4 v3, 0x1

    new-array v10, v3, [Ljava/lang/Long;

    move-object v4, v1

    move-object v3, v2

    :goto_10a
    if-lez v6, :cond_166

    move-object v2, v5

    move v1, v6

    move-object v5, v13

    goto :goto_ab

    :cond_110
    const/4 v1, 0x1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v3, v1

    goto :goto_e1

    :cond_11b
    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v2, v1

    goto :goto_ea

    :cond_126
    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_f2

    :cond_130
    const-string v4, "from_my_phone"

    invoke-static {v13, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_14c

    const v3, 0x7f08006d

    invoke-virtual {v14, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v8, "camerasync"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v7

    const/4 v3, 0x0

    move-object v5, v3

    goto :goto_102

    :cond_14c
    const-string v4, "camera_photos"

    invoke-static {v13, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_15d

    const v4, 0x7f08006e

    invoke-virtual {v14, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v5, v3

    goto :goto_102

    :cond_15d
    const v4, 0x7f08006f

    invoke-virtual {v14, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object v5, v3

    goto :goto_102

    :cond_166
    const/4 v1, 0x7

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_181

    const/4 v1, 0x0

    move-object v2, v1

    :goto_16f
    const/4 v1, 0x6

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_188

    const/4 v1, 0x0

    :goto_177
    aput-object v2, v11, v6

    aput-object v1, v10, v6

    add-int/lit8 v1, v6, 0x1

    move-object v2, v5

    move-object v5, v13

    goto/16 :goto_ab

    :cond_181
    const/4 v1, 0x7

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_16f

    :cond_188
    const/4 v1, 0x6

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_177

    :cond_192
    if-eqz v5, :cond_199

    const/4 v6, 0x0

    move-object v1, v15

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V
    :try_end_199
    .catchall {:try_start_95 .. :try_end_199} :catchall_425

    :cond_199
    if-eqz v12, :cond_19e

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_19e
    const-string v1, "PhotosHomeLoader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1bf

    const-string v1, "PhotosHomeLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#loadPhotosHome; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->logDelta(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_1bf
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mShowLocalCameraAlbum:Z

    if-eqz v1, :cond_1ca

    .line 134
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->loadCameraAlbum(Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V

    .line 139
    :cond_1ca
    :goto_1ca
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/phone/AlbumViewQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "sort_order, timestamp DESC, album_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    const-string v1, "PhotosHomeLoader"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_21d

    const-string v4, "PhotosHomeLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "albumCursor(1): "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", count: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v3, :cond_3d8

    const-string v1, "N/A"

    :goto_212
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21d
    if-eqz v3, :cond_42c

    :try_start_21f
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_42c

    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_228
    .catchall {:try_start_21f .. :try_end_228} :catchall_3ec

    const/4 v3, 0x0

    move-object v9, v3

    :goto_22a
    if-nez v9, :cond_429

    :try_start_22c
    new-instance v3, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/phone/AlbumViewQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "sort_order, timestamp DESC, album_id"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_253
    .catchall {:try_start_22c .. :try_end_253} :catchall_415

    move-result-object v2

    :try_start_254
    const-string v1, "PhotosHomeLoader"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_27f

    const-string v3, "PhotosHomeLoader"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "albumCursor(2): "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", count: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez v2, :cond_3e2

    const-string v1, "N/A"

    :goto_274
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_27f
    :goto_27f
    if-eqz v2, :cond_286

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v15}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->processAlbumCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    :try_end_286
    .catchall {:try_start_254 .. :try_end_286} :catchall_418

    :cond_286
    if-eqz v2, :cond_28b

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_28b
    const-string v1, "PhotosHomeLoader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2ac

    const-string v1, "PhotosHomeLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#loadAlbumList; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->logDelta(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_2ac
    const-string v1, "PhotosHomeLoader"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2cd

    .line 143
    const-string v1, "PhotosHomeLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#loadInBackGround; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v16 .. v17}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->logDelta(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_2cd
    return-object v15

    .line 132
    :catchall_2ce
    move-exception v1

    :goto_2cf
    if-eqz v2, :cond_2d4

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2d4
    const-string v2, "PhotosHomeLoader"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2f5

    const-string v2, "PhotosHomeLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#loadPhotosHome; "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->logDelta(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2f5
    throw v1

    .line 136
    :cond_2f6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mUserName:Ljava/lang/String;

    if-eqz v1, :cond_1ca

    .line 137
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotosOfUserQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_432

    :try_start_321
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_432

    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_32a
    .catchall {:try_start_321 .. :try_end_32a} :catchall_3d0

    const/4 v3, 0x0

    move-object v9, v3

    :goto_32c
    if-nez v9, :cond_42f

    :try_start_32e
    new-instance v3, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/phone/PhotosOfUserQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_354
    .catchall {:try_start_32e .. :try_end_354} :catchall_41a

    move-result-object v12

    :goto_355
    if-eqz v12, :cond_3ae

    :try_start_357
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3ae

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v1, 0x1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_3b5

    const/4 v4, 0x0

    :goto_36d
    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_3bf

    const/4 v1, 0x0

    move-object v13, v1

    :goto_376
    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_3c6

    const/4 v1, 0x0

    :goto_37e
    const v3, 0x7f080070

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mUserName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    const-string v5, "photos_of_me"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Long;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    const/4 v1, 0x1

    new-array v11, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v13, v11, v1

    move-object v1, v15

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Long;[Ljava/lang/String;)V
    :try_end_3ae
    .catchall {:try_start_357 .. :try_end_3ae} :catchall_41d

    :cond_3ae
    if-eqz v12, :cond_1ca

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1ca

    :cond_3b5
    const/4 v1, 0x1

    :try_start_3b6
    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    goto :goto_36d

    :cond_3bf
    const/4 v1, 0x3

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v13, v1

    goto :goto_376

    :cond_3c6
    const/4 v1, 0x2

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_3ce
    .catchall {:try_start_3b6 .. :try_end_3ce} :catchall_41d

    move-result-object v1

    goto :goto_37e

    :catchall_3d0
    move-exception v1

    move-object v2, v3

    :goto_3d2
    if-eqz v2, :cond_3d7

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3d7
    throw v1

    .line 139
    :cond_3d8
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto/16 :goto_212

    :cond_3e2
    :try_start_3e2
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3e9
    .catchall {:try_start_3e2 .. :try_end_3e9} :catchall_418

    move-result-object v1

    goto/16 :goto_274

    :catchall_3ec
    move-exception v1

    move-object v2, v3

    :goto_3ee
    if-eqz v2, :cond_3f3

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_3f3
    const-string v2, "PhotosHomeLoader"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_414

    const-string v2, "PhotosHomeLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "#loadAlbumList; "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->logDelta(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_414
    throw v1

    :catchall_415
    move-exception v1

    move-object v2, v9

    goto :goto_3ee

    :catchall_418
    move-exception v1

    goto :goto_3ee

    .line 137
    :catchall_41a
    move-exception v1

    move-object v2, v9

    goto :goto_3d2

    :catchall_41d
    move-exception v1

    move-object v2, v12

    goto :goto_3d2

    .line 132
    :catchall_420
    move-exception v1

    move-object/from16 v2, v18

    goto/16 :goto_2cf

    :catchall_425
    move-exception v1

    move-object v2, v12

    goto/16 :goto_2cf

    :cond_429
    move-object v2, v9

    goto/16 :goto_27f

    :cond_42c
    move-object v9, v3

    goto/16 :goto_22a

    :cond_42f
    move-object v12, v9

    goto/16 :goto_355

    :cond_432
    move-object v9, v3

    goto/16 :goto_32c

    :cond_435
    move v6, v1

    move-object v13, v5

    move-object v5, v2

    goto/16 :goto_10a

    :cond_43a
    move-object/from16 v12, v18

    goto/16 :goto_93

    :cond_43e
    move-object/from16 v18, v2

    goto/16 :goto_45
.end method

.method protected final onAbandon()V
    .registers 3

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_14

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserverRegistered:Z

    .line 182
    :cond_14
    return-void
.end method

.method protected final onReset()V
    .registers 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->onAbandon()V

    .line 187
    return-void
.end method

.method protected final onStartLoading()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 152
    .local v1, resolver:Landroid/content/ContentResolver;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserverRegistered:Z

    if-nez v2, :cond_35

    .line 156
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mPhotosHome:Z

    if-eqz v2, :cond_18

    .line 157
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    .line 158
    .local v0, notificationUri:Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v1, v0, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 162
    .end local v0           #notificationUri:Landroid/net/Uri;
    :cond_18
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 164
    .restart local v0       #notificationUri:Landroid/net/Uri;
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v1, v0, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 167
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 169
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v1, v0, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 170
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->mObserverRegistered:Z

    .line 173
    .end local v0           #notificationUri:Landroid/net/Uri;
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;->forceLoad()V

    .line 174
    return-void
.end method
