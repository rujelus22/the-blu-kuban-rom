.class final Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;
.super Ljava/lang/Object;
.source "EsFragmentActivity.java"

# interfaces
.implements Landroid/view/MenuItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TitleMenuItem"
.end annotation


# instance fields
.field private mActionEnum:I

.field private mEnabled:Z

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private final mItemId:I

.field private final mResources:Landroid/content/res/Resources;

.field private mTitle:Ljava/lang/CharSequence;

.field private mVisible:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .registers 5
    .parameter "context"
    .parameter "itemId"
    .parameter "titleRes"

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mResources:Landroid/content/res/Resources;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 220
    iput p2, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mItemId:I

    .line 221
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/CharSequence;)V
    .registers 5
    .parameter "context"
    .parameter "itemId"
    .parameter "title"

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mResources:Landroid/content/res/Resources;

    .line 232
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 233
    iput p2, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mItemId:I

    .line 234
    return-void
.end method


# virtual methods
.method public final collapseActionView()Z
    .registers 2

    .prologue
    .line 436
    const/4 v0, 0x0

    return v0
.end method

.method public final expandActionView()Z
    .registers 2

    .prologue
    .line 431
    const/4 v0, 0x0

    return v0
.end method

.method public final getActionProvider()Landroid/view/ActionProvider;
    .registers 2

    .prologue
    .line 426
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getActionView()Landroid/view/View;
    .registers 2

    .prologue
    .line 238
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAlphabeticShortcut()C
    .registers 2

    .prologue
    .line 243
    const/4 v0, 0x0

    return v0
.end method

.method public final getGroupId()I
    .registers 2

    .prologue
    .line 248
    const/4 v0, 0x0

    return v0
.end method

.method public final getIcon()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 258
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId()I
    .registers 2

    .prologue
    .line 263
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mItemId:I

    return v0
.end method

.method public final getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .registers 2

    .prologue
    .line 268
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNumericShortcut()C
    .registers 2

    .prologue
    .line 273
    const/4 v0, 0x0

    return v0
.end method

.method public final getOrder()I
    .registers 2

    .prologue
    .line 278
    const/4 v0, 0x0

    return v0
.end method

.method public final getSubMenu()Landroid/view/SubMenu;
    .registers 2

    .prologue
    .line 283
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitleCondensed()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasSubMenu()Z
    .registers 2

    .prologue
    .line 298
    const/4 v0, 0x0

    return v0
.end method

.method public final isActionViewExpanded()Z
    .registers 2

    .prologue
    .line 441
    const/4 v0, 0x0

    return v0
.end method

.method public final isCheckable()Z
    .registers 2

    .prologue
    .line 303
    const/4 v0, 0x0

    return v0
.end method

.method public final isChecked()Z
    .registers 2

    .prologue
    .line 308
    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled()Z
    .registers 2

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mEnabled:Z

    return v0
.end method

.method public final isVisible()Z
    .registers 2

    .prologue
    .line 318
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mVisible:Z

    return v0
.end method

.method public final setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .registers 3
    .parameter "actionProvider"

    .prologue
    .line 421
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setActionView(I)Landroid/view/MenuItem;
    .registers 2
    .parameter "resId"

    .prologue
    .line 328
    return-object p0
.end method

.method public final setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .registers 2
    .parameter "view"

    .prologue
    .line 323
    return-object p0
.end method

.method public final setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .registers 2
    .parameter "alphaChar"

    .prologue
    .line 333
    return-object p0
.end method

.method public final setCheckable(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter "checkable"

    .prologue
    .line 338
    return-object p0
.end method

.method public final setChecked(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter "checked"

    .prologue
    .line 343
    return-object p0
.end method

.method public final setEnabled(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter "enabled"

    .prologue
    .line 348
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mEnabled:Z

    .line 349
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/MenuItem;
    .registers 3
    .parameter "iconRes"

    .prologue
    .line 360
    if-eqz p1, :cond_a

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 363
    :cond_a
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .registers 2
    .parameter "icon"

    .prologue
    .line 354
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 355
    return-object p0
.end method

.method public final setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .registers 2
    .parameter "intent"

    .prologue
    .line 368
    return-object p0
.end method

.method public final setNumericShortcut(C)Landroid/view/MenuItem;
    .registers 2
    .parameter "numericChar"

    .prologue
    .line 373
    return-object p0
.end method

.method public final setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .registers 3
    .parameter "listener"

    .prologue
    .line 446
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .registers 2
    .parameter "menuItemClickListener"

    .prologue
    .line 378
    return-object p0
.end method

.method public final setShortcut(CC)Landroid/view/MenuItem;
    .registers 3
    .parameter "numericChar"
    .parameter "alphaChar"

    .prologue
    .line 383
    return-object p0
.end method

.method public final setShowAsAction(I)V
    .registers 2
    .parameter "actionEnum"

    .prologue
    .line 388
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mActionEnum:I

    .line 389
    return-void
.end method

.method public final setShowAsActionFlags(I)Landroid/view/MenuItem;
    .registers 3
    .parameter "actionEnum"

    .prologue
    .line 416
    const/4 v0, 0x0

    return-object v0
.end method

.method public final setTitle(I)Landroid/view/MenuItem;
    .registers 3
    .parameter "title"

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 400
    return-object p0
.end method

.method public final setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 2
    .parameter "title"

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 394
    return-object p0
.end method

.method public final setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .registers 2
    .parameter "title"

    .prologue
    .line 405
    return-object p0
.end method

.method public final setVisible(Z)Landroid/view/MenuItem;
    .registers 2
    .parameter "visible"

    .prologue
    .line 410
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;->mVisible:Z

    .line 411
    return-object p0
.end method
