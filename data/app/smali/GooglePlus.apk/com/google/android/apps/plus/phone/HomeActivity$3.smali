.class final Lcom/google/android/apps/plus/phone/HomeActivity$3;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

.field final synthetic val$finishOnOk:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/HomeActivity;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 915
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->val$finishOnOk:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 934
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 938
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 930
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 918
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->val$finishOnOk:Z

    if-eqz v0, :cond_a

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    .line 926
    :goto_9
    return-void

    .line 921
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    #getter for: Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->access$100(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/HostLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNavigationBarVisible()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 922
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    #getter for: Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->access$100(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/HostLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    .line 924
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$3;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->access$202(Lcom/google/android/apps/plus/phone/HomeActivity;Landroid/os/Bundle;)Landroid/os/Bundle;

    goto :goto_9
.end method
