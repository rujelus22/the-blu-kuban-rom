.class final Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedStreamOneUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 1874
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 1874
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 1874
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    return v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v1, 0x0

    .line 2027
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_16

    .line 2099
    :cond_15
    :goto_15
    return v1

    .line 2031
    :cond_16
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const/4 v3, 0x0

    #setter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$702(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 2033
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    .line 2035
    if-eqz p2, :cond_63

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_63

    .line 2037
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)I

    move-result v2

    sparse-switch v2, :sswitch_data_80

    .line 2079
    const v0, 0x7f080163

    .line 2084
    .local v0, resId:I
    :goto_35
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_15

    .line 2039
    .end local v0           #resId:I
    :sswitch_43
    const v0, 0x7f080155

    .line 2040
    .restart local v0       #resId:I
    goto :goto_35

    .line 2044
    .end local v0           #resId:I
    :sswitch_47
    const v0, 0x7f080153

    .line 2045
    .restart local v0       #resId:I
    goto :goto_35

    .line 2049
    .end local v0           #resId:I
    :sswitch_4b
    const v0, 0x7f080159

    .line 2050
    .restart local v0       #resId:I
    goto :goto_35

    .line 2054
    .end local v0           #resId:I
    :sswitch_4f
    const v0, 0x7f08015a

    .line 2055
    .restart local v0       #resId:I
    goto :goto_35

    .line 2059
    .end local v0           #resId:I
    :sswitch_53
    const v0, 0x7f08014f

    .line 2060
    .restart local v0       #resId:I
    goto :goto_35

    .line 2064
    .end local v0           #resId:I
    :sswitch_57
    const v0, 0x7f080151

    .line 2065
    .restart local v0       #resId:I
    goto :goto_35

    .line 2069
    .end local v0           #resId:I
    :sswitch_5b
    const v0, 0x7f080152

    .line 2070
    .restart local v0       #resId:I
    goto :goto_35

    .line 2074
    .end local v0           #resId:I
    :sswitch_5f
    const v0, 0x7f08015d

    .line 2075
    .restart local v0       #resId:I
    goto :goto_35

    .line 2088
    .end local v0           #resId:I
    :cond_63
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)I

    move-result v1

    packed-switch v1, :pswitch_data_a2

    .line 2099
    :cond_6c
    :goto_6c
    const/4 v1, 0x1

    goto :goto_15

    .line 2091
    :pswitch_6e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z

    move-result v1

    if-nez v1, :cond_6c

    .line 2092
    :pswitch_76
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_6c

    .line 2037
    :sswitch_data_80
    .sparse-switch
        0x10 -> :sswitch_47
        0x11 -> :sswitch_4b
        0x12 -> :sswitch_4f
        0x13 -> :sswitch_43
        0x20 -> :sswitch_53
        0x21 -> :sswitch_57
        0x22 -> :sswitch_5b
        0x30 -> :sswitch_5f
    .end sparse-switch

    .line 2088
    :pswitch_data_a2
    .packed-switch 0x10
        :pswitch_76
        :pswitch_6e
        :pswitch_76
    .end packed-switch
.end method


# virtual methods
.method public final onCreateComment$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 10
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v6, 0x0

    .line 1908
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-nez v2, :cond_1c

    .line 1909
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v2

    if-eqz v2, :cond_18

    .line 1911
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1929
    :cond_18
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1930
    :goto_1b
    return-void

    .line 1914
    :cond_1c
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    .line 1915
    .local v1, exception:Ljava/lang/Exception;
    instance-of v2, v1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v2, :cond_18

    check-cast v1, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v1           #exception:Ljava/lang/Exception;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v2

    const/16 v3, 0xe

    if-ne v2, v3, :cond_18

    .line 1918
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    .line 1919
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const v3, 0x7f080158

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const v4, 0x7f080157

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const v5, 0x7f0801c4

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1923
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1924
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1b
.end method

.method public final onCreatePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "result"

    .prologue
    .line 1994
    if-eqz p1, :cond_19

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1995
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08015b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1997
    :cond_19
    return-void
.end method

.method public final onDeleteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1947
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1948
    return-void
.end method

.method public final onDeleteComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1965
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1966
    return-void
.end method

.method public final onDeletePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "result"

    .prologue
    .line 2002
    if-eqz p1, :cond_19

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2003
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08015c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2006
    :cond_19
    return-void
.end method

.method public final onEditActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1902
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1903
    return-void
.end method

.method public final onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1935
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1936
    return-void
.end method

.method public final onGetActivity$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "activityId"
    .parameter "result"

    .prologue
    .line 1878
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 1897
    :cond_c
    :goto_c
    return-void

    .line 1882
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2c

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne p1, v1, :cond_2c

    .line 1883
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1702(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1884
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    .line 1887
    :cond_2c
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1889
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notif_category"

    const v3, 0xffff

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1891
    .local v0, notificationCategory:I
    const/16 v1, 0x8

    if-ne v0, v1, :cond_c

    .line 1892
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$1800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    const v3, 0x7f08018d

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_c
.end method

.method public final onGetActivityAudience$6db92636(ILcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "audienceData"
    .parameter "result"

    .prologue
    .line 2011
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_12

    if-eqz p2, :cond_12

    .line 2012
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #setter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2402(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    .line 2013
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 2016
    :cond_12
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 2017
    return-void
.end method

.method public final onModerateComment$56b78e3(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "commentId"
    .parameter "isUndo"
    .parameter "result"

    .prologue
    .line 1971
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1972
    if-eqz p3, :cond_12

    .line 1973
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->removeFlaggedComment(Ljava/lang/String;)V

    .line 1978
    :cond_11
    :goto_11
    return-void

    .line 1975
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->addFlaggedComment(Ljava/lang/String;)V

    goto :goto_11
.end method

.method public final onMuteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1953
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1954
    return-void
.end method

.method public final onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "plusOne"
    .parameter "result"

    .prologue
    .line 1984
    if-eqz p2, :cond_1b

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1985
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->access$2100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_1c

    const v0, 0x7f08015b

    :goto_13
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1989
    :cond_1b
    return-void

    .line 1985
    :cond_1c
    const v0, 0x7f08015c

    goto :goto_13
.end method

.method public final onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1959
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1960
    return-void
.end method

.method public final onReshareActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1941
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1942
    return-void
.end method
