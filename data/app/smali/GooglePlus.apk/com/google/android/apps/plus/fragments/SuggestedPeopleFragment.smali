.class public Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "SuggestedPeopleFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;"
    }
.end annotation


# static fields
.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

.field private mCirclesCursor:Landroid/database/Cursor;

.field private mDataLoaded:Z

.field private final mHandler:Landroid/os/Handler;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

.field private mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

.field private mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

.field private mSuggestionScrollView:Landroid/widget/ScrollView;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 50
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contact_count"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "semantic_hints"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mHandler:Landroid/os/Handler;

    .line 81
    new-instance v0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 293
    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mDataLoaded:Z

    if-nez v0, :cond_16

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 206
    const v0, 0x7f080400

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 211
    :goto_15
    return-void

    .line 208
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 209
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->showContent(Landroid/view/View;)V

    goto :goto_15
.end method


# virtual methods
.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 372
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_d

    .line 388
    :cond_c
    :goto_c
    return-void

    .line 376
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 378
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1e

    .line 379
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 382
    :cond_1e
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 384
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801a8

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_c
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mDataLoaded:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-nez v0, :cond_a

    .line 216
    :cond_8
    const/4 v0, 0x1

    .line 219
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->isEmpty()Z

    move-result v0

    goto :goto_9
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 11
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 309
    const/4 v0, -0x1

    if-ne p2, v0, :cond_30

    if-nez p1, :cond_30

    .line 310
    const-string v0, "person_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, personId:Ljava/lang/String;
    const-string v0, "display_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 312
    .local v3, personName:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "original_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 314
    .local v4, originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selected_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 316
    .local v5, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 325
    .end local v2           #personId:Ljava/lang/String;
    .end local v3           #personName:Ljava/lang/String;
    .end local v4           #originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5           #selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_30
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 326
    return-void
.end method

.method public final onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter "personId"
    .parameter "name"
    .parameter "forSharing"

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-static {v1, v2, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultCircleId(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, defaultCircleId:Ljava/lang/String;
    if-nez v0, :cond_10

    .line 265
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :goto_f
    return-void

    .line 267
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2, p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->addPersonToCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    goto :goto_f
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x0

    .line 95
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 97
    if-eqz p1, :cond_24

    .line 98
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 99
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 101
    :cond_1a
    const-string v1, "scrollPositions"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    .line 105
    :cond_24
    new-instance v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->setNotifyOnChange(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    .line 109
    .local v0, loaderManager:Landroid/support/v4/app/LoaderManager;
    new-instance v1, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    const/4 v4, 0x3

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->setCircleSpinnerAdapter(Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->setListener(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 116
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 142
    packed-switch p1, :pswitch_data_26

    .line 154
    const/4 v0, 0x0

    :goto_5
    return-object v0

    .line 144
    :pswitch_6
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_5

    .line 149
    :pswitch_16
    new-instance v0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Z)V

    goto :goto_5

    .line 142
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_6
        :pswitch_16
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 133
    const v1, 0x7f030071

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 134
    .local v0, view:Landroid/view/View;
    const v1, 0x7f090112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    .line 135
    const v1, 0x7f090113

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestionGridView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setAdapter(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;)V

    .line 137
    return-object v0
.end method

.method public final onDismissSuggestionAction(Ljava/lang/String;)V
    .registers 6
    .parameter "personId"

    .prologue
    .line 297
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 298
    .local v0, personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const-string v3, "SIGNUP"

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->dismissSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    .line 305
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 11
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 41
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_66

    :cond_b
    :goto_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->updateView(Landroid/view/View;)V

    return-void

    :pswitch_13
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->clear()V

    if-eqz p2, :cond_44

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_44

    :cond_22
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x2

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->add(Ljava/lang/Object;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_22

    :cond_44
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->notifyDataSetChanged()V

    goto :goto_b

    :pswitch_4a
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mDataLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    if-eqz p2, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setScrollPositions(Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    goto :goto_b

    :pswitch_data_66
    .packed-switch 0x1
        :pswitch_13
        :pswitch_4a
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 180
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 248
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 250
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onPeopleListVisibilityChange(Z)V

    .line 251
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;)V
    .registers 6
    .parameter "personId"

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 257
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    .line 258
    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 230
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 232
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 233
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onPeopleListVisibilityChange(Z)V

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_32

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_32

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 238
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 239
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 243
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_32
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->updateView(Landroid/view/View;)V

    .line 244
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 122
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    if-eqz v0, :cond_21

    .line 125
    const-string v0, "scrollPositions"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getScrollPositions()Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    :cond_21
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 224
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStart()V

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->onStart()V

    .line 226
    return-void
.end method

.method protected final setCircleMembership(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 15
    .parameter "personId"
    .parameter "personName"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 333
    .local p3, originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 334
    .local v7, circlesToAdd:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 335
    .local v6, circleId:Ljava/lang/String;
    invoke-virtual {p3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 336
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 340
    .end local v6           #circleId:Ljava/lang/String;
    :cond_1f
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 341
    .local v8, circlesToRemove:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_28
    :goto_28
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 342
    .restart local v6       #circleId:Ljava/lang/String;
    invoke-virtual {p4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 343
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_28

    .line 347
    .end local v6           #circleId:Ljava/lang/String;
    :cond_3e
    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->getMembershipChangeMessageId(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v7, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 353
    return-void
.end method
