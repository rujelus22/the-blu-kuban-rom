.class public Lcom/google/android/apps/plus/views/CirclesButton;
.super Landroid/view/ViewGroup;
.source "CirclesButton.java"


# instance fields
.field private final mCircleCountText:Landroid/widget/TextView;

.field private final mCircleIcon:Landroid/graphics/drawable/Drawable;

.field private final mCircleIconSpacing:I

.field private mCircleNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mCirclesText:Landroid/widget/TextView;

.field private mDefaultTextColor:I

.field private mFixedText:Ljava/lang/String;

.field private final mLabelSpacing:I

.field private mPadding:Landroid/graphics/Rect;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private final mSb:Ljava/lang/StringBuilder;

.field private mShowIcon:Z

.field private mShowProgressIndicator:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 55
    const v0, 0x7f0f0059

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CirclesButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 11
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/16 v6, 0x10

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    .line 46
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    .line 61
    iput v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    .line 62
    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->CircleButton:[I

    invoke-virtual {p1, p2, v1, v2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 63
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    .line 65
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    .line 67
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    .line 68
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    .line 69
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    .line 70
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 74
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    .line 75
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    const v2, 0x1030046

    invoke-virtual {v1, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 78
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 80
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 82
    iget v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    if-eqz v1, :cond_84

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mDefaultTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    :cond_84
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->addView(Landroid/view/View;)V

    .line 87
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    const v2, 0x1030046

    invoke-virtual {v1, p1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 92
    iget-object v1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->addView(Landroid/view/View;)V

    .line 93
    return-void
.end method

.method private appendCirclesText(Ljava/lang/StringBuilder;I)V
    .registers 9
    .parameter "sb"
    .parameter "circleCount"

    .prologue
    .line 295
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne p2, v5, :cond_20

    .line 296
    iget-object v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    .line 317
    .local v2, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    const/4 v0, 0x0

    .local v0, i:I
    :goto_b
    if-ge v0, p2, :cond_4b

    .line 318
    if-lez v0, :cond_14

    .line 319
    const-string v5, ", "

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    :cond_14
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 303
    .end local v0           #i:I
    .end local v2           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_20
    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 304
    .restart local v2       #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_27
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, p2, :cond_a

    .line 305
    const/4 v3, 0x0

    .line 306
    .local v3, maxLength:I
    const/4 v4, -0x1

    .line 307
    .local v4, maxLengthIndex:I
    const/4 v0, 0x0

    .restart local v0       #i:I
    :goto_30
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_47

    .line 308
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    .line 309
    .local v1, length:I
    if-lt v1, v3, :cond_44

    .line 310
    move v3, v1

    .line 311
    move v4, v0

    .line 307
    :cond_44
    add-int/lit8 v0, v0, 0x1

    goto :goto_30

    .line 314
    .end local v1           #length:I
    :cond_47
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_27

    .line 324
    .end local v3           #maxLength:I
    .end local v4           #maxLengthIndex:I
    :cond_4b
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge p2, v5, :cond_58

    .line 325
    const-string v5, ",\u2026"

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    :cond_58
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter "canvas"

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    if-eqz v0, :cond_9

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 389
    :cond_9
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 390
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 20
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    .line 334
    const/4 v8, 0x0

    .line 335
    .local v8, textAndIconWidth:I
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    if-eqz v9, :cond_10

    .line 336
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    add-int/2addr v9, v10

    add-int/lit8 v8, v9, 0x0

    .line 339
    :cond_10
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getVisibility()I

    move-result v9

    if-nez v9, :cond_22

    .line 340
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    add-int/2addr v9, v10

    add-int/2addr v8, v9

    .line 343
    :cond_22
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    .line 345
    sub-int v9, p4, p2

    sub-int/2addr v9, v8

    div-int/lit8 v4, v9, 0x2

    .line 346
    .local v4, leftBound:I
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    if-ge v4, v9, :cond_38

    .line 347
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v4, v9, Landroid/graphics/Rect;->left:I

    .line 349
    :cond_38
    add-int v7, v4, v8

    .line 350
    .local v7, rightBound:I
    sub-int v9, p4, p2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    sub-int/2addr v9, v10

    if-le v7, v9, :cond_4b

    .line 351
    sub-int v9, p4, p2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    sub-int v7, v9, v10

    .line 354
    :cond_4b
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    if-eqz v9, :cond_6d

    .line 355
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 356
    .local v1, iconHeight:I
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 357
    .local v3, iconWidth:I
    sub-int v9, p5, p3

    sub-int/2addr v9, v1

    div-int/lit8 v2, v9, 0x2

    .line 358
    .local v2, iconTop:I
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    add-int v10, v4, v3

    add-int v11, v2, v1

    invoke-virtual {v9, v4, v2, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 360
    iget v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    add-int/2addr v9, v3

    add-int/2addr v4, v9

    .line 363
    .end local v1           #iconHeight:I
    .end local v2           #iconTop:I
    .end local v3           #iconWidth:I
    :cond_6d
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getVisibility()I

    move-result v9

    if-nez v9, :cond_91

    .line 364
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 365
    .local v0, countWidth:I
    sub-int/2addr v7, v0

    .line 366
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    add-int v11, v7, v0

    sub-int v12, p5, p3

    iget-object v13, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v12, v13

    invoke-virtual {v9, v7, v10, v11, v12}, Landroid/widget/TextView;->layout(IIII)V

    .line 368
    iget v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    sub-int/2addr v7, v9

    .line 371
    .end local v0           #countWidth:I
    :cond_91
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    sub-int v11, p5, p3

    iget-object v12, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v11, v12

    invoke-virtual {v9, v4, v10, v7, v11}, Landroid/widget/TextView;->layout(IIII)V

    .line 373
    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    if-eqz v9, :cond_c0

    .line 374
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v9}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v6

    .line 375
    .local v6, progressSize:I
    sub-int v9, p4, p2

    sub-int/2addr v9, v6

    div-int/lit8 v5, v9, 0x2

    .line 376
    .local v5, progressLeft:I
    iget-object v9, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    add-int v11, v5, v6

    iget-object v12, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v12, v6

    invoke-virtual {v9, v5, v10, v11, v12}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 379
    .end local v5           #progressLeft:I
    .end local v6           #progressSize:I
    :cond_c0
    return-void
.end method

.method protected onMeasure(II)V
    .registers 33
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    if-nez v25, :cond_48

    .line 154
    new-instance v25, Landroid/graphics/Rect;

    invoke-direct/range {v25 .. v25}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 156
    .local v3, background:Landroid/graphics/drawable/Drawable;
    if-eqz v3, :cond_24

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 160
    :cond_24
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getPaddingLeft()I

    move-result v17

    .line 161
    .local v17, paddingLeft:I
    if-eqz v17, :cond_36

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move/from16 v0, v17

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 165
    :cond_36
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getPaddingRight()I

    move-result v18

    .line 166
    .local v18, paddingRight:I
    if-eqz v18, :cond_48

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move/from16 v0, v18

    move-object/from16 v1, v25

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 171
    .end local v3           #background:Landroid/graphics/drawable/Drawable;
    .end local v17           #paddingLeft:I
    .end local v18           #paddingRight:I
    :cond_48
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    .line 172
    .local v15, myWidth:I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 173
    .local v6, height:I
    const/high16 v25, -0x8000

    move/from16 v0, v25

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 174
    .local v7, heightSpec:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int v24, v25, v26

    .line 176
    .local v24, widthExceptLabel:I
    const/4 v8, 0x0

    .line 177
    .local v8, iconHeight:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    move/from16 v25, v0

    if-eqz v25, :cond_99

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    .line 179
    .local v9, iconWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    .line 180
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    move/from16 v25, v0

    add-int v25, v25, v9

    add-int v24, v24, v25

    .line 184
    .end local v9           #iconWidth:I
    :cond_99
    if-nez v15, :cond_22c

    const v12, 0x7fffffff

    .line 188
    .local v12, maxLabelWidth:I
    :goto_9e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    move-object/from16 v25, v0

    if-eqz v25, :cond_230

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, -0x8000

    move/from16 v0, v26

    invoke-static {v12, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    .line 193
    .local v10, labelWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x8

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    :goto_df
    add-int v23, v10, v24

    .line 252
    .local v23, width:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v25

    move/from16 v0, v25

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    add-int v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    add-int v6, v25, v26

    .line 255
    move/from16 v0, v23

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->resolveSize(II)I

    move-result v23

    .line 256
    move/from16 v0, p2

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->resolveSize(II)I

    move-result v6

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v11, v0, Landroid/graphics/Rect;->left:I

    .line 259
    .local v11, leftBound:I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    move/from16 v25, v0

    if-eqz v25, :cond_141

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIcon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleIconSpacing:I

    move/from16 v26, v0

    add-int v25, v25, v26

    add-int v11, v11, v25

    .line 263
    :cond_141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v20, v23, v25

    .line 265
    .local v20, rightBound:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getVisibility()I

    move-result v25

    if-nez v25, :cond_1a2

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 267
    .local v5, countWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, 0x4000

    move/from16 v0, v26

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v27, v0

    sub-int v27, v6, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    const/high16 v28, 0x4000

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    .line 271
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v25, v0

    add-int v25, v25, v5

    sub-int v20, v20, v25

    .line 274
    .end local v5           #countWidth:I
    :cond_1a2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v22

    .line 275
    .local v22, textWidth:I
    sub-int v25, v20, v11

    move/from16 v0, v22

    move/from16 v1, v25

    if-le v0, v1, :cond_1b6

    .line 276
    sub-int v22, v20, v11

    .line 278
    :cond_1b6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, 0x4000

    move/from16 v0, v22

    move/from16 v1, v26

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v27, v0

    sub-int v27, v6, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v28, v0

    sub-int v27, v27, v28

    const/high16 v28, 0x4000

    invoke-static/range {v27 .. v28}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    invoke-virtual/range {v25 .. v27}, Landroid/widget/TextView;->measure(II)V

    .line 283
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    move/from16 v25, v0

    if-eqz v25, :cond_224

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    sub-int v25, v6, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mPadding:Landroid/graphics/Rect;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    const/high16 v26, 0x4000

    invoke-static/range {v25 .. v26}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    .line 286
    .local v21, spec:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, v21

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/ProgressBar;->measure(II)V

    .line 289
    .end local v21           #spec:I
    :cond_224
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1, v6}, Lcom/google/android/apps/plus/views/CirclesButton;->setMeasuredDimension(II)V

    .line 290
    return-void

    .line 184
    .end local v10           #labelWidth:I
    .end local v11           #leftBound:I
    .end local v12           #maxLabelWidth:I
    .end local v20           #rightBound:I
    .end local v22           #textWidth:I
    .end local v23           #width:I
    :cond_22c
    sub-int v12, v15, v24

    goto/16 :goto_9e

    .line 195
    .restart local v12       #maxLabelWidth:I
    :cond_230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 197
    .local v4, circleCount:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/views/CirclesButton;->appendCirclesText(Ljava/lang/StringBuilder;I)V

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v13

    .line 201
    .local v13, measuredTextWidth:I
    if-gt v13, v12, :cond_28a

    .line 202
    move v10, v13

    .line 203
    .restart local v10       #labelWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x8

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_df

    .line 204
    .end local v10           #labelWidth:I
    :cond_28a
    const/16 v25, 0x1

    move/from16 v0, v25

    if-ne v4, v0, :cond_2bd

    .line 205
    move v10, v12

    .line 208
    .restart local v10       #labelWidth:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, -0x8000

    move/from16 v0, v26

    invoke-static {v10, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v10

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x8

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_df

    .line 216
    .end local v10           #labelWidth:I
    :cond_2bd
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 219
    .local v19, resources:Landroid/content/res/Resources;
    const/4 v5, 0x0

    .line 220
    .restart local v5       #countWidth:I
    add-int/lit8 v16, v4, -0x1

    .local v16, nameCount:I
    :goto_2d3
    if-lez v16, :cond_36a

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->appendCirclesText(Ljava/lang/StringBuilder;I)V

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mSb:Ljava/lang/StringBuilder;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v13

    .line 227
    sub-int v14, v4, v16

    .line 228
    .local v14, moreCircles:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const v26, 0x7f0e001b

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    move-object/from16 v0, v19

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleCountText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    .line 233
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v25, v0

    add-int v25, v25, v13

    add-int v25, v25, v5

    move/from16 v0, v25

    if-le v0, v12, :cond_36a

    .line 234
    add-int/lit8 v16, v16, -0x1

    goto/16 :goto_2d3

    .line 238
    .end local v14           #moreCircles:I
    :cond_36a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v25, v0

    add-int v25, v25, v13

    add-int v25, v25, v5

    move/from16 v0, v25

    if-le v0, v12, :cond_397

    .line 239
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v25, v0

    sub-int v25, v12, v25

    sub-int v13, v25, v5

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    move-object/from16 v25, v0

    const/high16 v26, -0x8000

    move/from16 v0, v26

    invoke-static {v13, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v26

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->measure(II)V

    .line 247
    :cond_397
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/CirclesButton;->mLabelSpacing:I

    move/from16 v25, v0

    add-int v25, v25, v13

    add-int v10, v25, v5

    .restart local v10       #labelWidth:I
    goto/16 :goto_df
.end method

.method public setCircles(Ljava/util/ArrayList;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, circleNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    .line 143
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCircleNames:Ljava/util/ArrayList;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    .line 146
    return-void
.end method

.method public setHighlighted(Z)V
    .registers 4
    .parameter "flag"

    .prologue
    .line 132
    if-eqz p1, :cond_f

    .line 133
    const v0, 0x7f02019d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 139
    :goto_e
    return-void

    .line 136
    :cond_f
    const v0, 0x7f02019c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->setBackgroundResource(I)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_e
.end method

.method public setShowIcon(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    if-eq v0, p1, :cond_9

    .line 104
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    .line 107
    :cond_9
    return-void
.end method

.method public setShowProgressIndicator(Z)V
    .registers 6
    .parameter "flag"

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 110
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    if-eq v0, p1, :cond_37

    .line 111
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowProgressIndicator:Z

    .line 112
    if-eqz p1, :cond_38

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_25

    .line 114
    new-instance v0, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CirclesButton;->addView(Landroid/view/View;)V

    .line 119
    :cond_25
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 121
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mShowIcon:Z

    .line 122
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setHighlighted(Z)V

    .line 127
    :cond_34
    :goto_34
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    .line 129
    :cond_37
    return-void

    .line 123
    :cond_38
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_34

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mCirclesText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_34
.end method

.method public setText(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 97
    iput-object p1, p0, Lcom/google/android/apps/plus/views/CirclesButton;->mFixedText:Ljava/lang/String;

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CirclesButton;->requestLayout()V

    .line 100
    :cond_d
    return-void
.end method
