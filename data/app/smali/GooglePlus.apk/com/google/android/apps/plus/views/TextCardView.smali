.class public Lcom/google/android/apps/plus/views/TextCardView;
.super Lcom/google/android/apps/plus/views/StreamCardView;
.source "TextCardView.java"


# static fields
.field protected static sCheckinIcon:Landroid/graphics/Bitmap;

.field private static sTextCardViewInitialized:Z


# instance fields
.field private mIsCheckin:Z

.field private mWrapContent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/TextCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/StreamCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    sget-boolean v0, Lcom/google/android/apps/plus/views/TextCardView;->sTextCardViewInitialized:Z

    if-nez v0, :cond_17

    .line 46
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/views/TextCardView;->sTextCardViewInitialized:Z

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200a2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/TextCardView;->sCheckinIcon:Landroid/graphics/Bitmap;

    .line 51
    :cond_17
    return-void
.end method


# virtual methods
.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .registers 11
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 191
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/TextCardView;->drawAuthorImage$494937f0(Landroid/graphics/Canvas;)V

    .line 193
    sget v1, Lcom/google/android/apps/plus/views/TextCardView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sContentXPadding:I

    add-int/2addr v1, v2

    add-int/2addr p2, v1

    .line 194
    sget v1, Lcom/google/android/apps/plus/views/TextCardView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sContentXPadding:I

    add-int/2addr v1, v2

    sub-int/2addr p4, v1

    .line 196
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/TextCardView;->drawAuthorName(Landroid/graphics/Canvas;II)I

    move-result p3

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_2e

    .line 198
    add-int v1, p2, p4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sub-int v2, p3, v2

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sRelativeTimeYOffset:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/apps/plus/views/TextCardView;->drawRelativeTimeLayout(Landroid/graphics/Canvas;II)I

    .line 202
    :cond_2e
    sget v1, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr p3, v1

    .line 204
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContentLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_50

    .line 205
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 206
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 207
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, p3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr p3, v1

    .line 211
    :cond_50
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_6f

    .line 212
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 214
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, p3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr p3, v1

    .line 218
    :cond_6f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_8e

    .line 219
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 220
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 221
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, p3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v1, v2

    add-int/2addr p3, v1

    .line 225
    :cond_8e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_c8

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagIcon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_b0

    .line 227
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mIsCheckin:Z

    if-eqz v1, :cond_cf

    sget v1, Lcom/google/android/apps/plus/views/TextCardView;->sTagIconYPaddingCheckin:I

    :goto_9c
    add-int v0, p3, v1

    .line 229
    .local v0, iconY:I
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagIcon:Landroid/graphics/Bitmap;

    int-to-float v2, p2

    int-to-float v3, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sTagIconXPadding:I

    add-int/2addr v1, v2

    add-int/2addr p2, v1

    .line 232
    .end local v0           #iconY:I
    :cond_b0
    int-to-float v1, p2

    int-to-float v2, p3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 233
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 234
    neg-int v1, p2

    int-to-float v1, v1

    neg-int v2, p3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    sget v1, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    .line 238
    :cond_c8
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/TextCardView;->drawPlusOneBar(Landroid/graphics/Canvas;)V

    .line 239
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/TextCardView;->drawWhatsHot(Landroid/graphics/Canvas;)V

    .line 240
    return p5

    .line 227
    :cond_cf
    sget v1, Lcom/google/android/apps/plus/views/TextCardView;->sTagIconYPaddingLocation:I

    goto :goto_9c
.end method

.method protected final formatLocationName(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "locationName"

    .prologue
    .line 246
    return-object p1
.end method

.method public final init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V
    .registers 19
    .parameter "cursor"
    .parameter "displaySizeType"
    .parameter "size"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewedListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"

    .prologue
    .line 58
    invoke-super/range {p0 .. p8}, Lcom/google/android/apps/plus/views/StreamCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 61
    const/16 v6, 0xe

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 62
    .local v0, contentFlags:J
    const-wide/16 v6, 0x10

    and-long/2addr v6, v0

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_4a

    const/4 v6, 0x1

    :goto_13
    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/TextCardView;->mIsCheckin:Z

    .line 64
    const/4 v6, 0x6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    .line 65
    .local v5, mediaBytes:[B
    if-eqz v5, :cond_49

    .line 66
    invoke-static {v5}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v2

    .line 67
    .local v2, dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/4 v3, 0x0

    .local v3, i:I
    array-length v4, v2

    .local v4, length:I
    :goto_22
    if-ge v3, v4, :cond_49

    .line 68
    aget-object v6, v2, v3

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4c

    .line 69
    iget-object v6, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContent:Ljava/lang/CharSequence;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4c

    aget-object v6, v2, v3

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4c

    .line 71
    aget-object v6, v2, v3

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContent:Ljava/lang/CharSequence;

    .line 77
    .end local v2           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v3           #i:I
    .end local v4           #length:I
    :cond_49
    return-void

    .line 62
    .end local v5           #mediaBytes:[B
    :cond_4a
    const/4 v6, 0x0

    goto :goto_13

    .line 67
    .restart local v2       #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .restart local v3       #i:I
    .restart local v4       #length:I
    .restart local v5       #mediaBytes:[B
    :cond_4c
    add-int/lit8 v3, v3, 0x1

    goto :goto_22
.end method

.method protected final layoutElements(IIII)I
    .registers 10
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 119
    add-int v2, p2, p4

    invoke-virtual {p0, p1, v2, p3}, Lcom/google/android/apps/plus/views/TextCardView;->createPlusOneBar(III)I

    .line 120
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int/2addr p4, v2

    .line 122
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/views/TextCardView;->setAuthorImagePosition(II)V

    .line 124
    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sAvatarSize:I

    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sContentXPadding:I

    .line 125
    sget v2, Lcom/google/android/apps/plus/views/TextCardView;->sAvatarSize:I

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sContentXPadding:I

    add-int/2addr v2, v3

    sub-int/2addr p3, v2

    .line 127
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/plus/views/TextCardView;->createAuthorNameAndRelativeTimeLayoutOnSameLine$4868d301(II)I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int p2, v2, v3

    .line 129
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContent:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_55

    .line 130
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v0, v2, v3

    .line 132
    .local v0, maxLines:I
    if-lez v0, :cond_55

    .line 133
    sget-object v2, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContent:Ljava/lang/CharSequence;

    invoke-static {v2, v3, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContentLayout:Landroid/text/StaticLayout;

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v2, v3

    add-int/2addr p2, v2

    .line 139
    .end local v0           #maxLines:I
    :cond_55
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttribution:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_85

    .line 140
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v0, v2, v3

    .line 142
    .restart local v0       #maxLines:I
    if-lez v0, :cond_85

    .line 143
    sget-object v2, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttribution:Ljava/lang/CharSequence;

    invoke-static {v2, v3, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    .line 145
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v2, v3

    add-int/2addr p2, v2

    .line 149
    .end local v0           #maxLines:I
    :cond_85
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContent:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b5

    .line 150
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v0, v2, v3

    .line 152
    .restart local v0       #maxLines:I
    if-lez v0, :cond_b5

    .line 153
    sget-object v2, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContent:Ljava/lang/CharSequence;

    invoke-static {v2, v3, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mFillerContentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v2, v3

    add-int/2addr p2, v2

    .line 160
    .end local v0           #maxLines:I
    :cond_b5
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTag:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f5

    .line 161
    sub-int v2, p4, p2

    sget-object v3, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    div-int v0, v2, v3

    .line 163
    .restart local v0       #maxLines:I
    if-lez v0, :cond_f5

    .line 164
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mIsCheckin:Z

    if-eqz v2, :cond_12b

    sget-object v2, Lcom/google/android/apps/plus/views/TextCardView;->sCheckinIcon:Landroid/graphics/Bitmap;

    :goto_d7
    iput-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagIcon:Landroid/graphics/Bitmap;

    .line 165
    sget-object v2, Lcom/google/android/apps/plus/views/TextCardView;->sDefaultTextPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTag:Ljava/lang/CharSequence;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int v4, p3, v4

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagLayout:Landroid/text/StaticLayout;

    .line 167
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mTagLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/views/TextCardView;->sContentYPadding:I

    add-int/2addr v2, v3

    add-int/2addr p2, v2

    .line 171
    .end local v0           #maxLines:I
    :cond_f5
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mWrapContent:Z

    if-eqz v2, :cond_122

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 173
    .local v1, r:Landroid/graphics/Rect;
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 175
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_113

    .line 176
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 177
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 180
    :cond_113
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_122

    .line 181
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 182
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2, p2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 186
    .end local v1           #r:Landroid/graphics/Rect;
    :cond_122
    iget-object v2, p0, Lcom/google/android/apps/plus/views/TextCardView;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    return v2

    .line 164
    .restart local v0       #maxLines:I
    :cond_12b
    sget-object v2, Lcom/google/android/apps/plus/views/TextCardView;->sTagLocationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    goto :goto_d7
.end method

.method protected onMeasure(II)V
    .registers 16
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 81
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 82
    .local v3, widthDimension:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 83
    .local v1, heightDimensionArg:I
    invoke-static {p2}, Lcom/google/android/apps/plus/views/TextCardView;->shouldWrapContent(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/TextCardView;->mWrapContent:Z

    .line 84
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/TextCardView;->mWrapContent:Z

    if-eqz v8, :cond_4c

    move v0, v3

    .line 86
    .local v0, heightDimension:I
    :goto_13
    const/4 v5, 0x0

    .line 87
    .local v5, xPadding:I
    const/4 v7, 0x0

    .line 88
    .local v7, yPadding:I
    const/4 v4, 0x0

    .line 89
    .local v4, xDoublePadding:I
    const/4 v6, 0x0

    .line 91
    .local v6, yDoublePadding:I
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/TextCardView;->mPaddingEnabled:Z

    if-eqz v8, :cond_23

    .line 92
    sget v5, Lcom/google/android/apps/plus/views/TextCardView;->sXPadding:I

    .line 93
    sget v7, Lcom/google/android/apps/plus/views/TextCardView;->sYPadding:I

    .line 94
    sget v4, Lcom/google/android/apps/plus/views/TextCardView;->sXDoublePadding:I

    .line 95
    sget v6, Lcom/google/android/apps/plus/views/TextCardView;->sYDoublePadding:I

    .line 98
    :cond_23
    sget v8, Lcom/google/android/apps/plus/views/TextCardView;->sLeftBorderPadding:I

    add-int/2addr v8, v5

    sget v9, Lcom/google/android/apps/plus/views/TextCardView;->sTopBorderPadding:I

    add-int/2addr v9, v7

    sget v10, Lcom/google/android/apps/plus/views/TextCardView;->sLeftBorderPadding:I

    add-int/2addr v10, v4

    sget v11, Lcom/google/android/apps/plus/views/TextCardView;->sRightBorderPadding:I

    add-int/2addr v10, v11

    sub-int v10, v3, v10

    sget v11, Lcom/google/android/apps/plus/views/TextCardView;->sTopBorderPadding:I

    add-int/2addr v11, v6

    sget v12, Lcom/google/android/apps/plus/views/TextCardView;->sBottomBorderPadding:I

    add-int/2addr v11, v12

    sub-int v11, v0, v11

    invoke-virtual {p0, v8, v9, v10, v11}, Lcom/google/android/apps/plus/views/TextCardView;->layoutElements(IIII)I

    move-result v2

    .line 103
    .local v2, measuredHeight:I
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/TextCardView;->mWrapContent:Z

    if-eqz v8, :cond_4e

    .line 104
    sget v8, Lcom/google/android/apps/plus/views/TextCardView;->sTopBorderPadding:I

    add-int/2addr v8, v2

    add-int/2addr v8, v6

    sget v9, Lcom/google/android/apps/plus/views/TextCardView;->sBottomBorderPadding:I

    add-int/2addr v8, v9

    invoke-virtual {p0, v3, v8}, Lcom/google/android/apps/plus/views/TextCardView;->setMeasuredDimension(II)V

    .line 109
    :goto_4b
    return-void

    .end local v0           #heightDimension:I
    .end local v2           #measuredHeight:I
    .end local v4           #xDoublePadding:I
    .end local v5           #xPadding:I
    .end local v6           #yDoublePadding:I
    .end local v7           #yPadding:I
    :cond_4c
    move v0, v1

    .line 84
    goto :goto_13

    .line 107
    .restart local v0       #heightDimension:I
    .restart local v2       #measuredHeight:I
    .restart local v4       #xDoublePadding:I
    .restart local v5       #xPadding:I
    .restart local v6       #yDoublePadding:I
    .restart local v7       #yPadding:I
    :cond_4e
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/TextCardView;->setMeasuredDimension(II)V

    goto :goto_4b
.end method

.method public onRecycle()V
    .registers 2

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardView;->onRecycle()V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/TextCardView;->mWrapContent:Z

    .line 115
    return-void
.end method
