.class final Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;
.super Ljava/lang/Object;
.source "EventLocationFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EventLocationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 426
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .registers 7
    .parameter "location"

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    #calls: Lcom/google/android/apps/plus/fragments/EventLocationFragment;->removeLocationListener()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->access$000(Lcom/google/android/apps/plus/fragments/EventLocationFragment;)V

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EventLocationFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->access$100(Lcom/google/android/apps/plus/fragments/EventLocationFragment;DD)V

    .line 433
    return-void
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    .prologue
    .line 437
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    .prologue
    .line 441
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 445
    return-void
.end method
