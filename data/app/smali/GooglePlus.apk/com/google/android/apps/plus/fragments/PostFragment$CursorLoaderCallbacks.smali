.class final Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CursorLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 715
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 715
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    return-void
.end method

.method private setAudienceView([B)Z
    .registers 5
    .parameter "audienceBytes"

    .prologue
    .line 815
    if-eqz p1, :cond_20

    .line 816
    invoke-static {p1}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserialize([B)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 817
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AudienceView;->setDefaultAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 818
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->initLoaders(Landroid/support/v4/app/LoaderManager;)V

    .line 819
    const/4 v1, 0x1

    .line 821
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    :goto_1f
    return v1

    :cond_20
    const/4 v1, 0x0

    goto :goto_1f
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 16
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 718
    packed-switch p1, :pswitch_data_6e

    move-object v0, v4

    .line 737
    :goto_5
    return-object v0

    .line 720
    :pswitch_6
    const-string v0, "com.google.android.apps.plus"

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildPlatformAudienceUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 722
    .local v2, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/PostFragment$PlatformAudienceQuery;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 726
    .end local v2           #uri:Landroid/net/Uri;
    :pswitch_26
    new-instance v5, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/fragments/PostFragment$AccountStatusQuery;->PROJECTION:[Ljava/lang/String;

    move-object v9, v4

    move-object v10, v4

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto :goto_5

    .line 731
    :pswitch_44
    new-instance v12, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v12, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;-><init>(Landroid/content/Context;)V

    .line 732
    .local v12, loader:Lcom/google/android/apps/plus/phone/EsCursorLoader;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2000(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsApiProvider;->makePreviewUri(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->setUri(Landroid/net/Uri;)V

    .line 733
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2100(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->setSelectionArgs([Ljava/lang/String;)V

    move-object v0, v12

    .line 734
    goto :goto_5

    .line 718
    nop

    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_26
        :pswitch_44
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 9
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 715
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_e4

    .end local p1
    :cond_b
    :goto_b
    return-void

    .restart local p1
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->isEdited()Z

    move-result v0

    if-nez v0, :cond_28

    if-eqz p2, :cond_28

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->setAudienceView([B)Z

    goto :goto_b

    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    new-instance v3, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_b

    :pswitch_3a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->isEdited()Z

    move-result v0

    if-nez v0, :cond_b

    if-eqz p2, :cond_b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->setAudienceView([B)Z

    move-result v0

    if-nez v0, :cond_b

    const-string v0, "PostFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "PostFragment"

    const-string v1, "Failed to set default audience"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    :pswitch_69
    check-cast p1, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2202(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;->isCachedData()Z

    move-result v0

    if-nez v0, :cond_cf

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;->setCachedData(Z)V

    if-eqz p2, :cond_cf

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_cf

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v0, "com.google.circles.platform.result.extra.ERROR_CODE"

    const/16 v1, 0xc8

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "com.google.circles.platform.result.extra.ERROR_MESSAGE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e1

    const-string v0, "Ok"

    move-object v1, v0

    :goto_99
    const-string v0, "com.google.android.apps.content.EXTRA_ACTIVITY"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_df

    array-length v3, v0

    if-lez v3, :cond_df

    aget-object v0, v0, v5

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryActivity;

    :goto_a8
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3, v4, v1, v2}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_dc

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_b5
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v4, v5, v2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    invoke-static {v1, v3, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2300(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    :cond_cf
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2400(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_b

    :cond_dc
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_b5

    :cond_df
    move-object v0, v2

    goto :goto_a8

    :cond_e1
    move-object v1, v0

    goto :goto_99

    nop

    :pswitch_data_e4
    .packed-switch 0x1
        :pswitch_c
        :pswitch_3a
        :pswitch_69
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 827
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_14

    .line 833
    :goto_7
    return-void

    .line 829
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2400(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7

    .line 827
    :pswitch_data_14
    .packed-switch 0x3
        :pswitch_8
    .end packed-switch
.end method
