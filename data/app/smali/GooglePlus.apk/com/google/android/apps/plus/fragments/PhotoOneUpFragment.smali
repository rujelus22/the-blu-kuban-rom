.class public Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "PhotoOneUpFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;
.implements Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
.implements Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;
.implements Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;
.implements Lcom/google/android/apps/plus/views/OneUpListener;
.implements Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$MyTextWatcher;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;",
        "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;",
        "Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;",
        "Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;",
        "Lcom/google/android/apps/plus/views/OneUpListener;",
        "Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;"
    }
.end annotation


# static fields
.field private static sActionBarHeight:I

.field private static sMaxWidth:I

.field private static sResourcesLoaded:Z


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

.field private mAlbumName:Ljava/lang/String;

.field private mAllowPlusOne:Z

.field private mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

.field private mAutoPlay:Z

.field private mAutoRefreshDone:Z

.field private mBackgroundDesiredHeight:I

.field private mBackgroundDesiredWidth:I

.field private mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

.field private mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

.field private mCommentButton:Landroid/view/View;

.field private mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mDownloadable:Ljava/lang/Boolean;

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFoooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

.field private mFullScreen:Z

.field private mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mListParent:Landroid/view/View;

.field private mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

.field private mMediaCount:I

.field private mOperationType:I

.field private mPendingBytes:[B

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mReadProcessed:Z

.field private mRefreshRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

.field private mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mTagLayout:Landroid/view/View;

.field private mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTitle:Ljava/lang/String;

.field private mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    .line 276
    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    .line 283
    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    .line 1748
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 113
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/api/MediaRef;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 113
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_21

    :try_start_6
    const-string v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v2, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    :try_end_21
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_21} :catch_3b

    :cond_21
    :goto_21
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :catch_3b
    move-exception v0

    const-string v1, "StreamOneUp"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_21

    const-string v1, "StreamOneUp"

    const-string v2, "Could not add photo to the Downloads application"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$500()I
    .registers 1

    .prologue
    .line 113
    sget v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sActionBarHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "pouf_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_16
    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method private doReportComment(Ljava/lang/String;ZZ)V
    .registers 10
    .parameter "commentId"
    .parameter "delete"
    .parameter "isUndo"

    .prologue
    .line 1599
    const-string v4, "extra_comment_id"

    invoke-static {v4, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1601
    .local v1, extras:Landroid/os/Bundle;
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1603
    if-eqz p3, :cond_58

    const v4, 0x7f0803d3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1606
    .local v3, title:Ljava/lang/String;
    :goto_14
    if-eqz p3, :cond_60

    const v4, 0x7f0803d4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1609
    .local v2, question:Ljava/lang/String;
    :goto_1d
    const v4, 0x7f0801c4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1611
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v4, 0x0

    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1612
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "comment_id"

    invoke-virtual {v4, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1613
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "delete"

    invoke-virtual {v4, v5, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1614
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "is_undo"

    invoke-virtual {v4, v5, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1615
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "pouf_report_comment"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1616
    return-void

    .line 1603
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v2           #question:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    :cond_58
    const v4, 0x7f0803d0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_14

    .line 1606
    .restart local v3       #title:Ljava/lang/String;
    :cond_60
    const v4, 0x7f0803d1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1d
.end method

.method private showProgressDialog(I)V
    .registers 3
    .parameter "operationType"

    .prologue
    .line 1696
    const v0, 0x7f080164

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    .line 1697
    return-void
.end method

.method private showProgressDialog(ILjava/lang/String;)V
    .registers 6
    .parameter "operationType"
    .parameter "message"

    .prologue
    .line 1707
    iput p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    .line 1709
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, p2, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 1711
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_pending"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1712
    return-void
.end method

.method private updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 4
    .parameter "actionBar"

    .prologue
    .line 1341
    if-nez p1, :cond_3

    .line 1354
    :cond_2
    :goto_2
    return-void

    .line 1345
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_25

    .line 1346
    :cond_13
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 1351
    :goto_16
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    if-eqz v0, :cond_2

    .line 1352
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_29

    const/4 v0, 0x1

    :goto_21
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->setLoading(Z)V

    goto :goto_2

    .line 1348
    :cond_25
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_16

    .line 1352
    :cond_29
    const/4 v0, 0x0

    goto :goto_21
.end method


# virtual methods
.method public final doDownload(Landroid/content/Context;Z)V
    .registers 9
    .parameter "context"
    .parameter "fullRes"

    .prologue
    const/16 v3, 0x800

    .line 1438
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    if-nez v4, :cond_7

    .line 1472
    :goto_6
    return-void

    .line 1442
    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 1448
    .local v1, imageUrl:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5f

    .line 1449
    if-eqz p2, :cond_59

    .line 1450
    const-string v3, "d"

    invoke-static {v3, v1}, Lcom/google/android/apps/plus/phone/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1459
    .local v0, downloadUrl:Ljava/lang/String;
    :goto_1f
    if-eqz v0, :cond_67

    .line 1460
    const-string v3, "StreamOneUp"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3e

    .line 1461
    const-string v3, "StreamOneUp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Downloading image from: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1464
    :cond_3e
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAlbumName:Ljava/lang/String;

    invoke-static {p1, v3, v0, p2, v4}, Lcom/google/android/apps/plus/service/EsService;->savePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1466
    const/16 v3, 0x13

    const v4, 0x7f080084

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    goto :goto_6

    .line 1452
    .end local v0           #downloadUrl:Ljava/lang/String;
    :cond_59
    const/4 v4, 0x0

    invoke-static {v3, v1, v4}, Lcom/google/android/apps/plus/phone/FIFEUtil;->setImageUrlSize(ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #downloadUrl:Ljava/lang/String;
    goto :goto_1f

    .line 1455
    .end local v0           #downloadUrl:Ljava/lang/String;
    :cond_5f
    if-eqz p2, :cond_62

    const/4 v3, -0x1

    :cond_62
    invoke-static {v3, v1}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->setImageUrlSize(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .restart local v0       #downloadUrl:Ljava/lang/String;
    goto :goto_1f

    .line 1469
    :cond_67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080086

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1470
    .local v2, toastText:Ljava/lang/String;
    const/4 v3, 0x1

    invoke-static {p1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_6
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1290
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 914
    packed-switch p1, :pswitch_data_10

    .line 926
    :cond_3
    :goto_3
    return-void

    .line 916
    :pswitch_4
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 917
    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    goto :goto_3

    .line 914
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    .prologue
    .line 489
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onAttach(Landroid/app/Activity;)V

    .line 490
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    if-eqz v0, :cond_c

    .line 491
    check-cast p1, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    return-void

    .line 493
    .restart local p1
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity must implement PhotoOneUpCallbacks"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 16
    .parameter "view"

    .prologue
    .line 1047
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_11c

    .line 1104
    :cond_7
    :goto_7
    return-void

    .line 1049
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 1050
    .local v6, commentText:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_65

    .line 1051
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_7

    .line 1052
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 1054
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v9

    .line 1055
    .local v9, spannable:Landroid/text/Spannable;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 1056
    .local v7, context:Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v9}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v8

    .line 1058
    .local v8, postableText:Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;->newBuilder()Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->setFocusObfuscatedOwnerId(Ljava/lang/String;)Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->setPhotoId(J)Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    invoke-virtual {v1}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->build()Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;

    move-result-object v1

    invoke-static {v7, v0, v1, v8}, Lcom/google/android/apps/plus/service/EsService;->createPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1060
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto :goto_7

    .line 1063
    .end local v7           #context:Landroid/content/Context;
    .end local v8           #postableText:Ljava/lang/String;
    .end local v9           #spannable:Landroid/text/Spannable;
    :cond_65
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_7

    .line 1069
    .end local v6           #commentText:Ljava/lang/String;
    :sswitch_6c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideo()Z

    move-result v0

    if-eqz v0, :cond_b2

    .line 1070
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideoReady()Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 1071
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVideoData()[B

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getVideoViewActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)Landroid/content/Intent;

    move-result-object v10

    .line 1074
    .local v10, startIntent:Landroid/content/Intent;
    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_7

    .line 1076
    .end local v10           #startIntent:Landroid/content/Intent;
    :cond_9d
    const v0, 0x7f08007c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1077
    .local v11, toastText:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v11, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    .line 1080
    .end local v11           #toastText:Ljava/lang/String;
    :cond_b2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->toggleFullScreen()V

    goto/16 :goto_7

    .line 1086
    :sswitch_b9
    const v0, 0x7f09003c

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1087
    .local v4, shapeId:Ljava/lang/Long;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1090
    const/16 v0, 0x31

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_7

    .line 1095
    .end local v4           #shapeId:Ljava/lang/Long;
    :sswitch_ea
    const v0, 0x7f09003c

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1096
    .restart local v4       #shapeId:Ljava/lang/Long;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1099
    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_7

    .line 1047
    nop

    :sswitch_data_11c
    .sparse-switch
        0x7f0900aa -> :sswitch_8
        0x7f090184 -> :sswitch_6c
        0x7f09018f -> :sswitch_b9
        0x7f090190 -> :sswitch_ea
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, -0x1

    const/4 v4, 0x0

    .line 343
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    .line 345
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    .line 347
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 349
    .local v0, args:Landroid/os/Bundle;
    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 350
    const-string v2, "photo_ref"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 351
    const-string v2, "photo_width"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundDesiredWidth:I

    .line 352
    const-string v2, "photo_height"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundDesiredHeight:I

    .line 353
    const-string v2, "photo_count"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mMediaCount:I

    .line 354
    const-string v2, "allow_plusone"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAllowPlusOne:Z

    .line 356
    if-eqz p1, :cond_9d

    .line 357
    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 358
    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 360
    :cond_5b
    const-string v2, "refresh_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6f

    .line 361
    const-string v2, "refresh_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    .line 364
    :cond_6f
    const-string v2, "audience_data"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    .line 366
    const-string v2, "flagged_comments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 367
    .local v1, ids:[Ljava/lang/String;
    if-eqz v1, :cond_8a

    .line 368
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 371
    :cond_8a
    const-string v2, "operation_type"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    .line 372
    const-string v2, "read_processed"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mReadProcessed:Z

    .line 373
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoPlay:Z

    .line 381
    .end local v1           #ids:[Ljava/lang/String;
    :cond_9c
    :goto_9c
    return-void

    .line 376
    :cond_9d
    const-string v2, "refresh"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_b7

    const-string v2, "force_load_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_9c

    .line 378
    :cond_b7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    goto :goto_9c
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 12
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 798
    sparse-switch p1, :sswitch_data_4a

    move-object v0, v6

    .line 816
    :goto_5
    return-object v0

    .line 800
    :sswitch_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 801
    .local v1, context:Landroid/content/Context;
    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_5

    .line 806
    .end local v1           #context:Landroid/content/Context;
    :sswitch_28
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    .line 811
    .local v4, queryUri:Landroid/net/Uri;
    new-instance v2, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoTagScroller$PhotoShapeQuery;->PROJECTION:[Ljava/lang/String;

    const-string v8, "shape_id"

    move-object v7, v6

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_5

    .line 798
    :sswitch_data_4a
    .sparse-switch
        0x1efab34d -> :sswitch_6
        0x1fd2f7ba -> :sswitch_28
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 16
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const v11, 0x7f090181

    const/16 v4, 0x8

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 394
    .local v1, context:Landroid/content/Context;
    sget-boolean v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sResourcesLoaded:Z

    if-nez v5, :cond_2c

    .line 395
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 397
    .local v2, res:Landroid/content/res/Resources;
    const v5, 0x7f0d0166

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    sput v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sMaxWidth:I

    .line 398
    const v5, 0x7f0d0128

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    sput v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sActionBarHeight:I

    .line 399
    sput-boolean v8, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sResourcesLoaded:Z

    .line 402
    .end local v2           #res:Landroid/content/res/Resources;
    :cond_2c
    const v5, 0x7f030087

    invoke-virtual {p1, v5, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 405
    .local v3, view:Landroid/view/View;
    const v5, 0x7f090180

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    .line 406
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 407
    new-instance v5, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-direct {v5, v9, v7}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;-><init>(Landroid/view/View;Z)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    .line 409
    const v5, 0x102000a

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/StreamOneUpListView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    .line 411
    new-instance v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-direct {v5, v1, v6, p0, v9}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    .line 412
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    sget v9, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sMaxWidth:I

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setMaxWidth(I)V

    .line 413
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 414
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 415
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 419
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v5, :cond_1a2

    move-object v5, v6

    :goto_7f
    const v9, 0x7f09017c

    invoke-virtual {v3, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    if-nez v5, :cond_1aa

    if-eqz v9, :cond_8d

    invoke-virtual {v9, v4}, Landroid/view/View;->setVisibility(I)V

    .line 421
    :cond_8d
    :goto_8d
    const v5, 0x7f09017d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    .line 422
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    const v5, 0x7f09017e

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    .line 424
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setHeaderView(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    .line 425
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setExternalOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 428
    const v5, 0x7f090065

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    .line 429
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v5, :cond_1f4

    .line 430
    .local v4, visibility:I
    :goto_c1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    const v9, 0x7f09011b

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 431
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    const v9, 0x7f090183

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 432
    const v5, 0x7f0900a9

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 433
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    .line 434
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 435
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setLayoutListener(Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;)V

    .line 436
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    sget v9, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sMaxWidth:I

    invoke-virtual {v5, v9}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setMaxWidth(I)V

    .line 437
    new-instance v5, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-direct {v5, v9, v7}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;-><init>(Landroid/view/View;Z)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFoooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    .line 439
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v5, v9}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 441
    .local v0, circleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 442
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5, p0, v9, v6, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 445
    const v5, 0x7f0900aa

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    .line 446
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 447
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    if-lez v5, :cond_1f7

    move v5, v8

    :goto_135
    invoke-virtual {v9, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 449
    new-instance v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$MyTextWatcher;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-direct {v5, v8, v7}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$MyTextWatcher;-><init>(Landroid/view/View;B)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 450
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 451
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v8, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$1;

    invoke-direct {v8, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 464
    const v5, 0x7f090182

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    .line 465
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setBackground(Landroid/view/View;)V

    .line 466
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setScrollView(Landroid/view/View;)V

    .line 467
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setTagLayout(Landroid/view/View;)V

    .line 468
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setActionBar(Landroid/view/View;)V

    .line 470
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    const v8, 0x1efab34d

    invoke-virtual {v5, v8, v6, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 472
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "show_keyboard"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1a1

    .line 473
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v6, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    const-wide/16 v7, 0xfa

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 484
    :cond_1a1
    return-object v3

    .line 419
    .end local v0           #circleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .end local v4           #visibility:I
    :cond_1a2
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_7f

    :cond_1aa
    if-nez v9, :cond_8d

    const v5, 0x7f09017b

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewStub;

    invoke-virtual {v5}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v9

    const v5, 0x7f090164

    invoke-virtual {v9, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    const v5, 0x7f090184

    invoke-virtual {v9, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5, v10}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->init(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnImageListener(Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->enableImageTransforms(Z)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v5, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v5, v7}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    invoke-virtual {v9}, Landroid/view/View;->invalidate()V

    goto/16 :goto_8d

    :cond_1f4
    move v4, v7

    .line 429
    goto/16 :goto_c1

    .restart local v0       #circleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .restart local v4       #visibility:I
    :cond_1f7
    move v5, v7

    .line 447
    goto/16 :goto_135
.end method

.method public final onDestroyView()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 750
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 751
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    .line 752
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 754
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 755
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    .line 756
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDestroyView()V

    .line 757
    return-void
.end method

.method public final onDetach()V
    .registers 2

    .prologue
    .line 499
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    .line 500
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDetach()V

    .line 501
    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1239
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 11
    .parameter "which"
    .parameter "args"

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1243
    const-string v0, "comment_action"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 1244
    .local v7, mCommentAction:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez v7, :cond_1b

    .line 1245
    const-string v0, "StreamOneUp"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1246
    const-string v0, "StreamOneUp"

    const-string v1, "No actions for comment option dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1277
    :cond_1a
    :goto_1a
    return-void

    .line 1250
    :cond_1b
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_31

    .line 1251
    const-string v0, "StreamOneUp"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1252
    const-string v0, "StreamOneUp"

    const-string v1, "Option selected outside the action list"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1a

    .line 1257
    :cond_31
    const-string v0, "comment_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1258
    .local v3, mCommentId:Ljava/lang/String;
    const-string v0, "comment_content"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1260
    .local v4, mCommentContent:Ljava/lang/String;
    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_ca

    goto :goto_1a

    .line 1274
    :pswitch_4b
    const-string v0, "extra_comment_id"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    const v0, 0x7f08012b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f080134

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0801c4

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v5, 0x7f0801c5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_id"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_delete_comment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1a

    .line 1262
    :pswitch_8c
    const-string v0, "extra_comment_id"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/Intents;->getEditCommentActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1a

    .line 1265
    :pswitch_bb
    invoke-direct {p0, v3, v6, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto/16 :goto_1a

    .line 1268
    :pswitch_c0
    invoke-direct {p0, v3, v6, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto/16 :goto_1a

    .line 1271
    :pswitch_c5
    invoke-direct {p0, v3, v1, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto/16 :goto_1a

    .line 1260
    :pswitch_data_ca
    .packed-switch 0x21
        :pswitch_4b
        :pswitch_bb
        :pswitch_c0
        :pswitch_c5
        :pswitch_8c
    .end packed-switch
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1235
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 14
    .parameter "args"
    .parameter "tag"

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 1196
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v7

    .line 1197
    .local v7, photoId:J
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    .line 1199
    .local v2, ownerId:Ljava/lang/String;
    const-string v0, "pouf_delete_photo"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 1200
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 1201
    .local v9, photoIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1202
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 1203
    .local v6, context:Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v0, v2, v9}, Lcom/google/android/apps/plus/service/EsService;->deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1204
    const/16 v0, 0x10

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0e0005

    invoke-virtual {v1, v3, v10}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    .line 1231
    .end local v2           #ownerId:Ljava/lang/String;
    .end local v6           #context:Landroid/content/Context;
    .end local v9           #photoIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :cond_42
    :goto_42
    return-void

    .line 1207
    .restart local v2       #ownerId:Ljava/lang/String;
    :cond_43
    const-string v0, "pouf_report_photo"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 1208
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, v7, v8, v2}, Lcom/google/android/apps/plus/service/EsService;->reportPhotoAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1210
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto :goto_42

    .line 1212
    :cond_61
    const-string v0, "pouf_delete_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_89

    .line 1213
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v5, "comment_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v3, v5}, Lcom/google/android/apps/plus/service/EsService;->deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1215
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto :goto_42

    .line 1217
    :cond_89
    const-string v0, "pouf_report_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_bd

    .line 1218
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .end local v2           #ownerId:Ljava/lang/String;
    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v10, "delete"

    invoke-virtual {p1, v10, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v10, "is_undo"

    invoke-virtual {p1, v10, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->reportPhotoComment$3486cdbb(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;ZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1221
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto :goto_42

    .line 1223
    .restart local v2       #ownerId:Ljava/lang/String;
    :cond_bd
    const-string v0, "pouf_delete_tag"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 1224
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v4

    .line 1225
    .local v4, shapeId:Ljava/lang/Long;
    if-eqz v4, :cond_42

    .line 1226
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1228
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_42
.end method

.method public final onFullScreenChanged$25decb5(Z)V
    .registers 4
    .parameter "fullScreen"

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 640
    :cond_8
    :goto_8
    return-void

    .line 621
    :cond_9
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_16

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    .line 627
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFoooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_21

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFoooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    .line 630
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_2c

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    .line 633
    :cond_2c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_37

    .line 634
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    .line 637
    :cond_37
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v0, :cond_8

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    goto :goto_8
.end method

.method public final onImageLoaded$46d55081()V
    .registers 4

    .prologue
    .line 792
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 793
    .local v0, view:Landroid/view/View;
    const v1, 0x7f090164

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 794
    return-void
.end method

.method public final onImageScaled(F)V
    .registers 4
    .parameter "scale"

    .prologue
    .line 784
    const/high16 v1, 0x3f80

    cmpl-float v1, p1, v1

    if-lez v1, :cond_11

    const/4 v0, 0x1

    .line 785
    .local v0, fullScreen:Z
    :goto_7
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eq v1, v0, :cond_10

    .line 786
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->toggleFullScreen()V

    .line 788
    :cond_10
    return-void

    .line 784
    .end local v0           #fullScreen:Z
    :cond_11
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final onInterceptMoveLeft$2548a39()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 652
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 655
    :cond_9
    :goto_9
    return v0

    :cond_a
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->getTargetView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    if-ne v1, v2, :cond_9

    :cond_18
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public final onInterceptMoveRight$2548a39()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 661
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 664
    :cond_9
    :goto_9
    return v0

    :cond_a
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->getTargetView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    if-ne v1, v2, :cond_9

    :cond_18
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x5

    .line 1108
    instance-of v0, p2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    if-eqz v0, :cond_e3

    .line 1109
    check-cast p2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    .end local p2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getAuthorId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isFlagged()Z

    move-result v5

    if-eqz v1, :cond_ab

    const v5, 0x7f0803d6

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v5, 0x25

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4c
    if-nez v2, :cond_50

    if-eqz v1, :cond_63

    :cond_50
    const v1, 0x7f0803d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_63
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const v1, 0x7f0803cc

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_action"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_id"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_content"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentContent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_delete_comment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->cancelPressedState()V

    .line 1115
    :cond_aa
    :goto_aa
    return-void

    .line 1109
    :cond_ab
    if-eqz v5, :cond_c1

    const v5, 0x7f0803d2

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v5, 0x23

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4c

    :cond_c1
    const v5, 0x7f0803cf

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v2, :cond_d8

    const/16 v5, 0x24

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4c

    :cond_d8
    const/16 v5, 0x22

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4c

    .line 1111
    .restart local p2
    :cond_e3
    const-string v0, "StreamOneUp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 1112
    const-string v0, "StreamOneUp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PhotoOneUpFragment.onItemClick: Some other view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_aa
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 13
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v6, 0x6

    const/4 v9, 0x3

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 113
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_1ce

    :cond_e
    :goto_e
    return-void

    :sswitch_f
    if-eqz p2, :cond_17

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1b

    :cond_17
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    goto :goto_e

    :cond_1b
    if-eqz p2, :cond_1cb

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1cb

    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_e7

    move-object v3, v4

    :goto_2c
    if-eqz v3, :cond_1c8

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    const-string v5, "FINAL"

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f0

    move v0, v1

    :goto_43
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setVideoBlob([B)V

    const/16 v3, 0x11

    invoke-interface {p2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_f3

    move-object v3, v4

    :goto_51
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    invoke-interface {p2, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_104

    move-object v3, v4

    :goto_5a
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAlbumName:Ljava/lang/String;

    const/16 v3, 0x10

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x7

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, "PLACEHOLDER"

    invoke-static {v6, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_75

    if-nez v0, :cond_75

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    if-nez v0, :cond_10a

    :cond_75
    const-string v0, "profile"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoRefreshDone:Z

    if-nez v0, :cond_10a

    move v0, v1

    :goto_82
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoRefreshDone:Z

    const/16 v3, 0x9

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_8f

    move v2, v1

    :cond_8f
    :goto_8f
    if-nez v0, :cond_9a

    if-eqz p2, :cond_9a

    :cond_93
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v9, :cond_10d

    move v0, v1

    :cond_9a
    :goto_9a
    if-eqz v0, :cond_9f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    :cond_9f
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->setFlaggedComments(Ljava/util/HashSet;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    if-eqz v2, :cond_114

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const v2, 0x7f080141

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(I)V

    :goto_bd
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    if-eqz v0, :cond_cf

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    const v2, 0x7f090181

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setCanAnimate(Z)V

    :cond_cf
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x1fd2f7ba

    invoke-virtual {v0, v1, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_e

    :cond_e7
    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_2c

    :cond_f0
    move v0, v2

    goto/16 :goto_43

    :cond_f3
    const/16 v3, 0x11

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v1, :cond_102

    move v3, v1

    :goto_fc
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto/16 :goto_51

    :cond_102
    move v3, v2

    goto :goto_fc

    :cond_104
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5a

    :cond_10a
    move v0, v2

    goto/16 :goto_82

    :cond_10d
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_93

    goto :goto_9a

    :cond_114
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_bd

    :sswitch_11a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    const v5, 0x7f09017f

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5, v6, v7, p2, v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->bind(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->isWaitingMyApproval()Z

    move-result v0

    if-eqz v0, :cond_158

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0201d6

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v0, v2, v2, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_158
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hasTags()Z

    move-result v0

    if-eqz v0, :cond_1be

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v0, :cond_1b8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v0, v5, v7

    if-eqz v0, :cond_1b8

    move v0, v1

    :goto_171
    if-eqz v0, :cond_19b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v5, "window"

    invoke-virtual {v0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-eq v0, v1, :cond_193

    if-ne v0, v9, :cond_1ba

    :cond_193
    move v0, v1

    :goto_194
    if-eqz v0, :cond_19a

    const/16 v0, 0x320

    if-lt v5, v0, :cond_1bc

    :cond_19a
    move v0, v1

    :cond_19b
    :goto_19b
    if-eqz v0, :cond_1be

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hideTags(ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    :goto_1b0
    invoke-virtual {v3}, Landroid/view/View;->invalidate()V

    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_e

    :cond_1b8
    move v0, v2

    goto :goto_171

    :cond_1ba
    move v0, v2

    goto :goto_194

    :cond_1bc
    move v0, v2

    goto :goto_19b

    :cond_1be
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    goto :goto_1b0

    :cond_1c8
    move v0, v2

    goto/16 :goto_43

    :cond_1cb
    move v0, v2

    goto/16 :goto_8f

    :sswitch_data_1ce
    .sparse-switch
        0x1efab34d -> :sswitch_f
        0x1fd2f7ba -> :sswitch_11a
    .end sparse-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 987
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onLocationClick$75c560e7(Lcom/google/android/apps/plus/content/DbLocation;)V
    .registers 2
    .parameter "location"

    .prologue
    .line 1132
    return-void
.end method

.method public final onMeasured(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    .prologue
    .line 1010
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-ne p1, v1, :cond_10

    .line 1012
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->setContainerHeight(I)V

    .line 1035
    :cond_f
    :goto_f
    return-void

    .line 1013
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    if-ne p1, v1, :cond_f

    .line 1018
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getMeasuredHeight()I

    move-result v0

    .line 1019
    .local v0, footerHeight:I
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$4;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_f
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter "item"

    .prologue
    const v4, 0x7f0801c4

    const v6, 0x7f0801c5

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v2

    .line 611
    :goto_12
    return v0

    .line 564
    :cond_13
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_180

    .line 611
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_12

    .line 566
    :sswitch_1f
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    move v0, v3

    .line 568
    goto :goto_12

    .line 571
    :sswitch_2d
    const v0, 0x7f08006a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f08006b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_report_photo"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v0, v3

    .line 573
    goto :goto_12

    .line 576
    :sswitch_55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v4, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_85

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_85

    const-string v1, "notif_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_85

    move v2, v3

    :cond_85
    if-eqz v2, :cond_a2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v4

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_a2
    move v0, v3

    .line 578
    goto/16 :goto_12

    .line 581
    :sswitch_a5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {v0, v2, v1, v4, v3}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v3

    .line 583
    goto/16 :goto_12

    .line 586
    :sswitch_b7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-nez v0, :cond_ea

    :goto_bb
    if-eqz v1, :cond_e7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f080160

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f08015f

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v5, 0x14

    const v6, 0x7f080083

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    new-instance v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;

    invoke-direct {v5, p0, v4, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v3, [Landroid/graphics/Bitmap;

    aput-object v1, v0, v2

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_e7
    move v0, v3

    .line 588
    goto/16 :goto_12

    .line 586
    :cond_ea
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_bb

    .line 591
    :sswitch_f1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_107
    invoke-static {v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_13d

    :goto_10d
    if-nez v0, :cond_13f

    const v0, 0x7f0e0003

    :goto_112
    const v1, 0x7f0e0002

    invoke-virtual {v4, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    const v5, 0x7f0e0010

    invoke-virtual {v4, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_delete_photo"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v0, v3

    .line 593
    goto/16 :goto_12

    :cond_13b
    move-object v0, v1

    .line 591
    goto :goto_107

    :cond_13d
    move-object v0, v1

    goto :goto_10d

    :cond_13f
    const v0, 0x7f0e0004

    goto :goto_112

    .line 596
    :sswitch_143
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doDownload(Landroid/content/Context;Z)V

    move v0, v3

    .line 598
    goto/16 :goto_12

    .line 601
    :sswitch_151
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    move v0, v3

    .line 603
    goto/16 :goto_12

    .line 606
    :sswitch_157
    const v0, 0x7f080068

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f080069

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_delete_tag"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v0, v3

    .line 608
    goto/16 :goto_12

    .line 564
    :sswitch_data_180
    .sparse-switch
        0x7f090199 -> :sswitch_55
        0x7f09028d -> :sswitch_151
        0x7f09028f -> :sswitch_1f
        0x7f0902b9 -> :sswitch_2d
        0x7f0902cc -> :sswitch_a5
        0x7f0902cd -> :sswitch_b7
        0x7f0902ce -> :sswitch_157
        0x7f0902cf -> :sswitch_f1
        0x7f0902d0 -> :sswitch_143
    .end sparse-switch
.end method

.method public final onPause()V
    .registers 4

    .prologue
    .line 727
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPause()V

    .line 729
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-eqz v2, :cond_c

    .line 730
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->onStop()V

    .line 733
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-eqz v2, :cond_2c

    .line 734
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, i:I
    :goto_18
    if-ltz v0, :cond_2c

    .line 735
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 736
    .local v1, v:Landroid/view/View;
    instance-of v2, v1, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v2, :cond_29

    .line 737
    check-cast v1, Lcom/google/android/apps/plus/views/OneUpBaseView;

    .end local v1           #v:Landroid/view/View;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStop()V

    .line 734
    :cond_29
    add-int/lit8 v0, v0, -0x1

    goto :goto_18

    .line 742
    .end local v0           #i:I
    :cond_2c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V

    .line 743
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V

    .line 744
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 745
    return-void
.end method

.method public final onPlaceClick(Ljava/lang/String;)V
    .registers 6
    .parameter "placeObfuscatedId"

    .prologue
    .line 1136
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1137
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1139
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1141
    .end local v0           #intent:Landroid/content/Intent;
    :cond_14
    return-void
.end method

.method public final onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .registers 10
    .parameter "entityId"
    .parameter "plusOneData"

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    .line 1166
    .local v2, ownerId:Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    .line 1167
    .local v4, photoId:J
    invoke-static {v2, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->isPhotoPlusOnePending(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    if-nez v0, :cond_29

    .line 1168
    if-eqz p2, :cond_1a

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v0

    if-nez v0, :cond_2a

    :cond_1a
    const/4 v6, 0x1

    .line 1169
    .local v6, plusOne:Z
    :goto_1b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->photoPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JZ)I

    .line 1171
    .end local v6           #plusOne:Z
    :cond_29
    return-void

    .line 1168
    :cond_2a
    const/4 v6, 0x0

    goto :goto_1b
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 4
    .parameter "actionBar"

    .prologue
    .line 505
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    .line 507
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-nez v0, :cond_17

    .line 509
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;-><init>(Landroid/view/View;Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    .line 512
    :cond_17
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 19
    .parameter "menu"

    .prologue
    .line 516
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    if-eqz v15, :cond_13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    move-object/from16 v0, p0

    invoke-interface {v15, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v15

    if-nez v15, :cond_13

    .line 556
    :goto_12
    return-void

    .line 520
    :cond_13
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 522
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v9

    .line 523
    .local v9, photoId:J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v8

    .line 525
    .local v8, ownerId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    if-nez v15, :cond_129

    const/4 v13, 0x0

    .line 527
    .local v13, shapeId:Ljava/lang/Long;
    :goto_2d
    if-eqz v13, :cond_133

    const/4 v14, 0x1

    .line 528
    .local v14, taggedAsMe:Z
    :goto_30
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_136

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 530
    .local v12, photoUri:Landroid/net/Uri;
    :goto_46
    const-wide/16 v15, 0x0

    cmp-long v15, v9, v15

    if-nez v15, :cond_139

    invoke-static {v12}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_139

    const/4 v4, 0x1

    .line 532
    .local v4, isLocalPhoto:Z
    :goto_53
    const-wide/16 v15, 0x0

    cmp-long v15, v9, v15

    if-eqz v15, :cond_13c

    invoke-static {v12}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v15

    if-nez v15, :cond_13c

    const/4 v5, 0x1

    .line 534
    .local v5, isRemotePhoto:Z
    :goto_60
    const-wide/16 v15, 0x0

    cmp-long v15, v9, v15

    if-nez v15, :cond_13f

    if-eqz v12, :cond_13f

    const/4 v7, 0x1

    .line 535
    .local v7, onlyPhotoUrl:Z
    :goto_69
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v15, v8}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_7b

    if-nez v8, :cond_142

    invoke-static {v12}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_142

    :cond_7b
    const/4 v6, 0x1

    .line 537
    .local v6, myPhoto:Z
    :goto_7c
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v15

    const-string v16, "stream_id"

    invoke-virtual/range {v15 .. v16}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 538
    .local v11, photoStream:Ljava/lang/String;
    const-string v15, "camerasync"

    invoke-virtual {v15, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 539
    .local v3, isInstantUpload:Z
    if-nez v7, :cond_a5

    if-eqz v5, :cond_148

    if-nez v6, :cond_a5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    if-eqz v15, :cond_145

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-eqz v15, :cond_145

    const/4 v15, 0x1

    :goto_a3
    if-eqz v15, :cond_148

    :cond_a5
    const/4 v2, 0x1

    .line 540
    .local v2, allowDownload:Z
    :goto_a6
    if-eqz v6, :cond_14b

    if-nez v5, :cond_ac

    if-eqz v4, :cond_14b

    :cond_ac
    const/4 v1, 0x1

    .line 543
    .local v1, allowDelete:Z
    :goto_ad
    const v15, 0x7f090199

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 546
    const v15, 0x7f0902cc

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v15

    if-nez v15, :cond_14e

    if-nez v6, :cond_d0

    if-eqz v14, :cond_14e

    :cond_d0
    const/4 v15, 0x1

    :goto_d1
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 550
    const v15, 0x7f0902cd

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 551
    const v15, 0x7f0902cf

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 552
    const v15, 0x7f0902d0

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 553
    const v15, 0x7f0902b9

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v16

    if-nez v6, :cond_150

    if-eqz v5, :cond_150

    const/4 v15, 0x1

    :goto_108
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 554
    const v15, 0x7f0902ce

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 555
    const v15, 0x7f09028f

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    const/16 v16, 0x1

    invoke-interface/range {v15 .. v16}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_12

    .line 525
    .end local v1           #allowDelete:Z
    .end local v2           #allowDownload:Z
    .end local v3           #isInstantUpload:Z
    .end local v4           #isLocalPhoto:Z
    .end local v5           #isRemotePhoto:Z
    .end local v6           #myPhoto:Z
    .end local v7           #onlyPhotoUrl:Z
    .end local v11           #photoStream:Ljava/lang/String;
    .end local v12           #photoUri:Landroid/net/Uri;
    .end local v13           #shapeId:Ljava/lang/Long;
    .end local v14           #taggedAsMe:Z
    :cond_129
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v13

    goto/16 :goto_2d

    .line 527
    .restart local v13       #shapeId:Ljava/lang/Long;
    :cond_133
    const/4 v14, 0x0

    goto/16 :goto_30

    .line 528
    .restart local v14       #taggedAsMe:Z
    :cond_136
    const/4 v12, 0x0

    goto/16 :goto_46

    .line 530
    .restart local v12       #photoUri:Landroid/net/Uri;
    :cond_139
    const/4 v4, 0x0

    goto/16 :goto_53

    .line 532
    .restart local v4       #isLocalPhoto:Z
    :cond_13c
    const/4 v5, 0x0

    goto/16 :goto_60

    .line 534
    .restart local v5       #isRemotePhoto:Z
    :cond_13f
    const/4 v7, 0x0

    goto/16 :goto_69

    .line 535
    .restart local v7       #onlyPhotoUrl:Z
    :cond_142
    const/4 v6, 0x0

    goto/16 :goto_7c

    .line 539
    .restart local v3       #isInstantUpload:Z
    .restart local v6       #myPhoto:Z
    .restart local v11       #photoStream:Ljava/lang/String;
    :cond_145
    const/4 v15, 0x0

    goto/16 :goto_a3

    :cond_148
    const/4 v2, 0x0

    goto/16 :goto_a6

    .line 540
    .restart local v2       #allowDownload:Z
    :cond_14b
    const/4 v1, 0x0

    goto/16 :goto_ad

    .line 546
    .restart local v1       #allowDelete:Z
    :cond_14e
    const/4 v15, 0x0

    goto :goto_d1

    .line 553
    :cond_150
    const/4 v15, 0x0

    goto :goto_108
.end method

.method public final onResume()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 669
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onResume()V

    .line 671
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-eqz v4, :cond_d

    .line 672
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->onStart()V

    .line 675
    :cond_d
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-eqz v4, :cond_2d

    .line 676
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, i:I
    :goto_19
    if-ltz v0, :cond_2d

    .line 677
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 678
    .local v3, v:Landroid/view/View;
    instance-of v4, v3, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v4, :cond_2a

    .line 679
    check-cast v3, Lcom/google/android/apps/plus/views/OneUpBaseView;

    .end local v3           #v:Landroid/view/View;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStart()V

    .line 676
    :cond_2a
    add-int/lit8 v0, v0, -0x1

    goto :goto_19

    .line 684
    .end local v0           #i:I
    :cond_2d
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 685
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v4, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->addScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V

    .line 686
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v4, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V

    .line 688
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_61

    .line 689
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v4

    if-nez v4, :cond_61

    .line 690
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v2

    .line 691
    .local v2, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    #calls: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    invoke-static {v4, v5, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 695
    .end local v2           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_61
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_7c

    .line 696
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v4

    if-nez v4, :cond_7c

    .line 697
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    .line 698
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    .line 702
    :cond_7c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 703
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    .line 706
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    if-eqz v4, :cond_a0

    .line 707
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_a1

    .line 710
    const-string v4, "StreamOneUp"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9e

    .line 711
    const-string v4, "StreamOneUp"

    const-string v5, "Both a pending profile image and an existing request"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :cond_9e
    :goto_9e
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    .line 723
    :cond_a0
    return-void

    .line 714
    :cond_a1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    .line 716
    .local v1, profileBytes:[B
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/plus/service/EsService;->setProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 718
    const/16 v4, 0x15

    const v5, 0x7f080088

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    goto :goto_9e
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 761
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 763
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 764
    const-string v1, "pending_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 766
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_21

    .line 767
    const-string v1, "refresh_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 769
    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_2c

    .line 770
    const-string v1, "audience_data"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 772
    :cond_2c
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_46

    .line 773
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 774
    .local v0, ids:[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 775
    const-string v1, "flagged_comments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 778
    .end local v0           #ids:[Ljava/lang/String;
    :cond_46
    const-string v1, "operation_type"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 779
    const-string v1, "read_processed"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mReadProcessed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 780
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 4
    .parameter "args"

    .prologue
    .line 385
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 386
    const-string v0, "auto_play_music"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoPlay:Z

    .line 387
    return-void
.end method

.method public final onSkyjamBuyClick(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    .prologue
    .line 1188
    return-void
.end method

.method public final onSkyjamListenClick(Ljava/lang/String;)V
    .registers 2
    .parameter "url"

    .prologue
    .line 1192
    return-void
.end method

.method public final onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/text/style/URLSpan;Ljava/lang/String;)V
    .registers 6
    .parameter "appName"
    .parameter
    .parameter "data"
    .parameter "span"
    .parameter "authorId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/style/URLSpan;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1184
    .local p2, packageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public final onSourceAppNameClick$1b7460f0(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1179
    .local p1, packageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public final onSourceAppNameLinkEnabled()V
    .registers 1

    .prologue
    .line 1175
    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .registers 10
    .parameter "span"

    .prologue
    .line 1145
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v5

    .line 1146
    .local v5, url:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1148
    .local v0, context:Landroid/content/Context;
    const-string v6, "https://plus.google.com/s/%23"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_33

    .line 1149
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "#"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1d

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1150
    .local v4, searchKey:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v6, v4}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 1151
    .local v3, intent:Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1161
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #searchKey:Ljava/lang/String;
    :goto_32
    return-void

    .line 1153
    :cond_33
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/phone/Intents;->isProfileUrl(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4e

    .line 1154
    const-string v6, "pid="

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1155
    .local v2, gaiaId:Ljava/lang/String;
    const-string v6, "extra_gaia_id"

    invoke-static {v6, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1157
    .local v1, extras:Landroid/os/Bundle;
    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v6, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1159
    .end local v1           #extras:Landroid/os/Bundle;
    .end local v2           #gaiaId:Ljava/lang/String;
    :cond_4e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7, v5}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_32
.end method

.method public final onUserImageClick(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "gaiaId"
    .parameter "authorName"

    .prologue
    .line 1119
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1120
    .local v0, context:Landroid/content/Context;
    const-string v2, "extra_gaia_id"

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1121
    .local v1, extras:Landroid/os/Bundle;
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1122
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1124
    return-void
.end method

.method public final onViewActivated()V
    .registers 2

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->onFragmentVisible(Landroid/support/v4/app/Fragment;)V

    .line 647
    :cond_d
    return-void
.end method

.method public final recordNavigationAction()V
    .registers 1

    .prologue
    .line 1281
    return-void
.end method

.method public final refresh()V
    .registers 6

    .prologue
    .line 991
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    .line 993
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_2d

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_35

    .line 997
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getPhotoSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    .line 1005
    :cond_2d
    :goto_2d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 1006
    return-void

    .line 1000
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    goto :goto_2d
.end method

.method public final setTitle(Ljava/lang/String;)V
    .registers 2
    .parameter "title"

    .prologue
    .line 1295
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTitle:Ljava/lang/String;

    .line 1296
    return-void
.end method
