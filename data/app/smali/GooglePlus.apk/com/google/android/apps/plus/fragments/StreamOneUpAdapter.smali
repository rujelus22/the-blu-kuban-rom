.class public final Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "StreamOneUpAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/SettableItemAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$LeftoverQuery;,
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$LoadingQuery;,
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$CommentQuery;,
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$CommentCountQuery;,
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$ActivityQuery;
    }
.end annotation


# instance fields
.field private mActivityPosition:I

.field private mContainerHeight:I

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeights:Landroid/util/SparseIntArray;

.field private mLeftoverPosition:I

.field private mLoading:Z

.field private final mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

.field private final mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "oneUpListener"
    .parameter "onMeasuredListener"

    .prologue
    const/4 v1, -0x1

    .line 175
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 159
    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    .line 161
    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    .line 177
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 178
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    .line 179
    return-void
.end method


# virtual methods
.method public final addFlaggedComment(Ljava/lang/String;)V
    .registers 3
    .parameter "commentId"

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->notifyDataSetChanged()V

    .line 327
    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 13
    .parameter "view"
    .parameter "context"
    .parameter "c"

    .prologue
    const v8, 0x7f09022c

    .line 225
    const/4 v7, 0x1

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 226
    .local v5, type:I
    packed-switch v5, :pswitch_data_8c

    .line 277
    :goto_b
    return-void

    :pswitch_c
    move-object v6, p1

    .line 228
    check-cast v6, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    .line 229
    .local v6, v:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    .line 230
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 231
    invoke-virtual {v6, p3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->bind(Landroid/database/Cursor;)V

    goto :goto_b

    .line 236
    .end local v6           #v:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
    :pswitch_1d
    const/4 v7, 0x4

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 237
    .local v0, commentId:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    .local v2, isFlagged:Z
    move-object v6, p1

    .line 239
    check-cast v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    .line 240
    .local v6, v:Lcom/google/android/apps/plus/views/StreamOneUpCommentView;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    .line 241
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 242
    invoke-virtual {v6, p3, v2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->bind(Landroid/database/Cursor;Z)V

    goto :goto_b

    .end local v0           #commentId:Ljava/lang/String;
    .end local v2           #isFlagged:Z
    .end local v6           #v:Lcom/google/android/apps/plus/views/StreamOneUpCommentView;
    :pswitch_39
    move-object v6, p1

    .line 247
    check-cast v6, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;

    .line 248
    .local v6, v:Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 249
    invoke-virtual {v6, p3}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->bind(Landroid/database/Cursor;)V

    goto :goto_b

    .line 254
    .end local v6           #v:Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;
    :pswitch_45
    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLoading:Z

    if-eqz v7, :cond_58

    .line 255
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 259
    :goto_51
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 260
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    goto :goto_b

    .line 257
    :cond_58
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_51

    :pswitch_62
    move-object v6, p1

    .line 265
    check-cast v6, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;

    .line 266
    .local v6, v:Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;
    iget v4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mContainerHeight:I

    .line 267
    .local v4, leftover:I
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v7, :cond_87

    .line 268
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v7}, Landroid/util/SparseIntArray;->size()I

    move-result v7

    add-int/lit8 v1, v7, -0x1

    .local v1, i:I
    :goto_73
    if-lez v4, :cond_87

    if-ltz v1, :cond_87

    .line 269
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    .line 270
    .local v3, key:I
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v7, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v7

    sub-int/2addr v4, v7

    .line 268
    add-int/lit8 v1, v1, -0x1

    goto :goto_73

    .line 273
    .end local v1           #i:I
    .end local v3           #key:I
    :cond_87
    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->bind(I)V

    goto :goto_b

    .line 226
    nop

    :pswitch_data_8c
    .packed-switch 0x0
        :pswitch_c
        :pswitch_39
        :pswitch_1d
        :pswitch_45
        :pswitch_62
    .end packed-switch
.end method

.method public final getAclText()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 357
    iget v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    if-gez v2, :cond_6

    .line 366
    :cond_5
    :goto_5
    return-object v1

    .line 361
    :cond_6
    iget v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 362
    .local v0, c:Landroid/database/Cursor;
    if-eqz v0, :cond_5

    .line 366
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_5
.end method

.method public final getActivityAuthorId()Ljava/lang/String;
    .registers 4

    .prologue
    .line 373
    iget v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    if-ltz v1, :cond_c

    iget v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getCount()I

    move-result v2

    if-le v1, v2, :cond_e

    .line 374
    :cond_c
    const/4 v1, 0x0

    .line 378
    :goto_d
    return-object v1

    .line 377
    :cond_e
    iget v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 378
    .local v0, c:Landroid/database/Cursor;
    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_d
.end method

.method public final getItemViewType(I)I
    .registers 4
    .parameter "position"

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 183
    const/4 v0, 0x5

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "context"
    .parameter "c"
    .parameter "parent"

    .prologue
    const/4 v3, 0x0

    .line 281
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 285
    .local v0, layoutInflater:Landroid/view/LayoutInflater;
    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_42

    .line 314
    const/4 v1, 0x0

    .line 318
    .local v1, v:Landroid/view/View;
    :goto_12
    return-object v1

    .line 287
    .end local v1           #v:Landroid/view/View;
    :pswitch_13
    const v2, 0x7f0300c1

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 289
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 292
    .end local v1           #v:Landroid/view/View;
    :pswitch_1b
    const v2, 0x7f0300c2

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 295
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 298
    .end local v1           #v:Landroid/view/View;
    :pswitch_23
    const v2, 0x7f0300c3

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 300
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 303
    .end local v1           #v:Landroid/view/View;
    :pswitch_2b
    const v2, 0x7f0300c6

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 305
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 308
    .end local v1           #v:Landroid/view/View;
    :pswitch_33
    const v2, 0x7f0300c5

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 309
    .restart local v1       #v:Landroid/view/View;
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    goto :goto_12

    .line 285
    nop

    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_13
        :pswitch_1b
        :pswitch_23
        :pswitch_2b
        :pswitch_33
    .end packed-switch
.end method

.method public final removeFlaggedComment(Ljava/lang/String;)V
    .registers 3
    .parameter "commentId"

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->notifyDataSetChanged()V

    .line 335
    return-void
.end method

.method public final setContainerHeight(I)V
    .registers 2
    .parameter "height"

    .prologue
    .line 341
    iput p1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mContainerHeight:I

    .line 342
    return-void
.end method

.method public final setFlaggedComments(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p1, flaggedComments:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    .line 221
    return-void
.end method

.method public final setItemHeight(II)V
    .registers 4
    .parameter "position"
    .parameter "height"

    .prologue
    .line 346
    if-ltz p1, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    if-ne p1, v0, :cond_b

    .line 350
    :cond_a
    :goto_a
    return-void

    .line 349
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_a
.end method

.method public final setLoading(Z)V
    .registers 3
    .parameter "loading"

    .prologue
    .line 385
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLoading:Z

    if-eq v0, p1, :cond_9

    .line 386
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLoading:Z

    .line 387
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->notifyDataSetChanged()V

    .line 389
    :cond_9
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 5
    .parameter "newCursor"

    .prologue
    const/4 v2, -0x1

    .line 193
    iput v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    .line 194
    if-eqz p1, :cond_36

    .line 195
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 196
    .local v0, count:I
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    .line 197
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    .line 198
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 200
    :cond_1a
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2f

    .line 201
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    .line 205
    :goto_27
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 212
    .end local v0           #count:I
    :cond_2a
    :goto_2a
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    .line 204
    .restart local v0       #count:I
    :cond_2f
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1a

    goto :goto_27

    .line 208
    .end local v0           #count:I
    :cond_36
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    .line 209
    iput v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    goto :goto_2a
.end method
