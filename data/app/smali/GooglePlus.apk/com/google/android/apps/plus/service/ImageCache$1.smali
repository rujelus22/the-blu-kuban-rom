.class final Lcom/google/android/apps/plus/service/ImageCache$1;
.super Landroid/support/v4/util/LruCache;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/ImageCache;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Lcom/google/android/apps/plus/content/ImageRequest;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/ImageCache;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/ImageCache;I)V
    .registers 3
    .parameter
    .parameter "x0"

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageCache$1;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-direct {p0, p2}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 321
    instance-of v0, p2, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_10

    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    :goto_f
    return v0

    .restart local p2
    :cond_10
    instance-of v0, p2, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_22

    check-cast p2, Landroid/graphics/drawable/Drawable;

    .end local p2
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0xa

    goto :goto_f

    .restart local p2
    :cond_22
    const/4 v0, 0x0

    goto :goto_f
.end method
