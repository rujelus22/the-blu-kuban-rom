.class public final Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
.super Landroid/widget/BaseAdapter;
.source "SuggestionGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SuggestionCategoryAdapter"
.end annotation


# instance fields
.field private mCategory:Ljava/lang/String;

.field private mCategoryLabel:Ljava/lang/String;

.field private mCount:I

.field private mOffset:I

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;)V
    .registers 2
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->this$0:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategory:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    iput p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCount:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I

    return v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    iput p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I

    return p1
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategoryLabel:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public final getCategory()Ljava/lang/String;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategory:Ljava/lang/String;

    return-object v0
.end method

.method public final getCategoryLabel()Ljava/lang/String;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategoryLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCount:I

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 4
    .parameter "position"

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->this$0:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->this$0:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 105
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->this$0:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I

    add-int/2addr v2, p1

    invoke-virtual {v1, v2, p2, p3}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 106
    .local v0, view:Landroid/view/View;
    instance-of v1, v0, Lcom/google/android/apps/plus/views/PersonCardView;

    if-eqz v1, :cond_16

    move-object v1, v0

    .line 107
    check-cast v1, Lcom/google/android/apps/plus/views/PersonCardView;

    if-nez p1, :cond_17

    const/4 v2, 0x1

    :goto_13
    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setWideMargin(Z)V

    .line 109
    :cond_16
    return-object v0

    .line 107
    :cond_17
    const/4 v2, 0x0

    goto :goto_13
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategoryLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
