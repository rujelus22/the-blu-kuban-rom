.class public Lcom/google/android/apps/plus/service/EsService;
.super Landroid/app/Service;
.source "EsService.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;,
        Lcom/google/android/apps/plus/service/EsService$LocalImageKey;,
        Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;,
        Lcom/google/android/apps/plus/service/EsService$ImageKey;,
        Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;
    }
.end annotation


# static fields
.field private static sAccountSettingsResponses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/content/AccountSettingsData;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private static final sDismissPeopleLock:Ljava/lang/Object;

.field private static sHandler:Landroid/os/Handler;

.field private static final sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

.field private static sLastCameraMediaLocation:Ljava/lang/String;

.field private static sLastRequestId:Ljava/lang/Integer;

.field private static final sListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/service/EsServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sOutOfBoxResponses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPendingIntents:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPhotoImageCache:Lcom/google/android/apps/plus/service/PhotoCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/service/PhotoCache",
            "<",
            "Lcom/google/android/apps/plus/service/EsService$ImageKey;",
            ">;"
        }
    .end annotation
.end field

.field private static final sResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

.field private mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

.field private mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

.field private final mStopRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 400
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sDismissPeopleLock:Ljava/lang/Object;

    .line 423
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    .line 424
    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sResults:Ljava/util/Map;

    .line 426
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    .line 427
    new-instance v0, Lcom/google/android/apps/plus/service/IntentPool;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/IntentPool;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    .line 428
    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;-><init>(B)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sOutOfBoxResponses:Ljava/util/Map;

    .line 431
    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;-><init>(B)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    .line 540
    new-instance v0, Lcom/google/android/apps/plus/service/PhotoCache;

    new-instance v1, Lcom/google/android/apps/plus/service/EsService$1;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsService$1;-><init>()V

    new-instance v2, Lcom/google/android/apps/plus/service/EsService$2;

    invoke-direct {v2}, Lcom/google/android/apps/plus/service/EsService$2;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/service/PhotoCache;-><init>(Lcom/google/android/apps/plus/service/PhotoCache$CacheListener;Lcom/google/android/apps/plus/service/PhotoCache$PhotoLoader;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sPhotoImageCache:Lcom/google/android/apps/plus/service/PhotoCache;

    .line 604
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 162
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 623
    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;-><init>(Lcom/google/android/apps/plus/service/EsService;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    .line 624
    new-instance v0, Lcom/google/android/apps/plus/service/EsService$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/EsService$3;-><init>(Lcom/google/android/apps/plus/service/EsService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mStopRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;III)V
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3ef

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "media_ref"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "width"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "height"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "crop_type"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 162
    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "Notification sync"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_c
    invoke-static {p1, p2, v1, v0, v2}, Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, p3, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_18
    .catchall {:try_start_c .. :try_end_18} :catchall_2c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_18} :catch_1c

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_1b
    return-void

    :catch_1c
    move-exception v0

    :try_start_1d
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p3, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_28
    .catchall {:try_start_1d .. :try_end_28} :catchall_2c

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_1b

    :catchall_2c
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .registers 16
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 162
    const-string v0, "circles_to_add"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const-string v0, "circles_to_remove"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const-string v0, "participant_array"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v11

    move v10, v7

    :goto_15
    array-length v0, v11

    if-ge v10, v0, :cond_39

    aget-object v0, v11, v10

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/plus/util/ParticipantParcelable;

    new-instance v0, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/ParticipantParcelable;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/ParticipantParcelable;->getName()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v2, p2

    move-object v9, v8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->start()V

    const-string v1, "EsService"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_15

    :cond_39
    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;)V
    .registers 10
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 162
    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "People sync"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_b
    invoke-static {p1, p2, v1, v0, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, p4, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_17
    .catchall {:try_start_b .. :try_end_17} :catchall_2b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_17} :catch_1b

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_1a
    return-void

    :catch_1b
    move-exception v0

    :try_start_1c
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p4, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_27
    .catchall {:try_start_1c .. :try_end_27} :catchall_2b

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_1a

    :catchall_2b
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static synthetic access$200()Lcom/google/android/apps/plus/service/PhotoCache;
    .registers 1

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sPhotoImageCache:Lcom/google/android/apps/plus/service/PhotoCache;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/List;
    .registers 1

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/Map;
    .registers 1

    .prologue
    .line 162
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;Landroid/content/Intent;)V
    .registers 13
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    .prologue
    .line 162
    new-instance v6, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "Get nearby activities"

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v0, "Activities:SyncNearby"

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    :try_start_15
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPostsData;->doNearbyActivitiesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p5, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1d
    .catchall {:try_start_15 .. :try_end_1d} :catchall_37
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_1d} :catch_24

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_23
    return-void

    :catch_24
    move-exception v0

    :try_start_25
    new-instance v1, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p5, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_30
    .catchall {:try_start_25 .. :try_end_30} :catchall_37

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_23

    :catchall_37
    move-exception v0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Z)V
    .registers 20
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"

    .prologue
    .line 162
    new-instance v10, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    move/from16 v0, p8

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->setFullSync(Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Get activities for circleId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " userId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v1, "Activities:SyncStream"

    invoke-virtual {v10, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    if-eqz p8, :cond_57

    const/16 v8, 0x14

    :goto_3b
    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p8

    move-object/from16 v7, p6

    :try_start_46
    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p7

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_50
    .catchall {:try_start_46 .. :try_end_50} :catchall_72
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_50} :catch_5d

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_56
    return-void

    :cond_57
    if-nez p6, :cond_5b

    const/4 v8, 0x0

    goto :goto_3b

    :cond_5b
    const/4 v8, 0x0

    goto :goto_3b

    :catch_5d
    move-exception v1

    :try_start_5e
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v1, 0x0

    move-object/from16 v0, p7

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_6b
    .catchall {:try_start_5e .. :try_end_6b} :catchall_72

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_56

    :catchall_72
    move-exception v1

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v1
.end method

.method static accountsChanged(Landroid/content/Context;)I
    .registers 4
    .parameter "context"

    .prologue
    .line 808
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 809
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 811
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/plus/content/AccountSettingsData;)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "displayName"
    .parameter "photoUrl"
    .parameter "isPlusPage"
    .parameter "settings"

    .prologue
    .line 756
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 757
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 758
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 759
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 760
    const-string v1, "display_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 761
    const-string v1, "photo_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 762
    const-string v1, "is_plus_page"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 763
    const-string v1, "account_settings"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 765
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static addAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "upgradeOrigin"

    .prologue
    .line 730
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 731
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 732
    const-string v1, "oob_origin"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 733
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 735
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static addPeopleToCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/util/ParticipantParcelable;[Ljava/lang/String;)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "participants"
    .parameter "circlesToAdd"

    .prologue
    .line 2093
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2094
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2095
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2096
    const-string v1, "participant_array"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2097
    const-string v1, "circles_to_add"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2099
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static addPersonToCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "name"
    .parameter "circleId"

    .prologue
    const/4 v5, 0x1

    .line 2046
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2047
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "op"

    const/16 v3, 0x2be

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2048
    const-string v2, "acc"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2049
    const-string v2, "person_id"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2050
    const-string v2, "person_name"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2051
    const-string v2, "circles_to_add"

    new-array v3, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2052
    const-string v2, "fire_and_forget"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2053
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 2055
    .local v1, requestId:Ljava/lang/Integer;
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onStartAddToCircleRequest$3608be29(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 2057
    return-object v1
.end method

.method public static applyPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/lang/String;ZLjava/lang/String;)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "analytics"
    .parameter "info"
    .parameter "uri"
    .parameter "apply"
    .parameter "token"

    .prologue
    .line 2720
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2721
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x899

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2722
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2723
    const-string v1, "analytics"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2724
    const-string v1, "apiInfo"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2726
    const-string v1, "url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2727
    const-string v1, "applyPlusOne"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2728
    const-string v1, "token"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2730
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static changeNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Ljava/lang/Integer;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "settings"

    .prologue
    .line 2616
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2617
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xcc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2618
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2619
    const-string v1, "notification_settings"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2621
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static clearNetworkTransactionsData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2414
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2415
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x7d1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2416
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2418
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    .line 2419
    return-void
.end method

.method private completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    .registers 6
    .parameter "intent"
    .parameter "serviceResult"
    .parameter "resultValue"

    .prologue
    .line 5648
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/service/EsService$22;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService$22;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5657
    return-void
.end method

.method public static createCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "circleName"
    .parameter "justFollowing"

    .prologue
    .line 2153
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2154
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2155
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2156
    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2157
    const-string v1, "just_following"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2159
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static createComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentText"

    .prologue
    .line 1803
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1804
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1805
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1807
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1809
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "event"
    .parameter "audience"
    .parameter "externalId"

    .prologue
    .line 2864
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2865
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x387

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2866
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2867
    const-string v1, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2868
    const-string v1, "audience"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2869
    const-string v1, "external_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2871
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createEventComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "eventId"
    .parameter "commentText"

    .prologue
    .line 1824
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1825
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1826
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1827
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1828
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1829
    const-string v1, "content"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1831
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "commentText"

    .prologue
    .line 1240
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1241
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1242
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1243
    const-string v1, "picasa_photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1244
    const-string v1, "text"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1246
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "id"

    .prologue
    .line 1099
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1100
    .local v0, intent:Landroid/content/Intent;
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static createPostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1082
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1083
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1084
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1085
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1086
    return-object v0
.end method

.method public static createProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    .line 1575
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1576
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2ca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1577
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1578
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1580
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1004
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1005
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1006
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1007
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1009
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Ljava/lang/Integer;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 2195
    .local p2, circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2196
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2197
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2198
    const-string v1, "circle_ids"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2200
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentId"

    .prologue
    .line 1869
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1870
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1871
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1872
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1873
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1875
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "eventId"

    .prologue
    .line 2902
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2903
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x38b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2904
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2905
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2907
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I
    .registers 5
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1519
    .local p1, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1520
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1521
    const-string v1, "media_refs"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1523
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "commentId"

    .prologue
    .line 1287
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1288
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x36

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1289
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1290
    const-string v1, "photo_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1291
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1293
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1442
    .local p3, photoIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1443
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1444
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1445
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1446
    const-string v1, "photo_ids"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1448
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1128
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1129
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1130
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1131
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1132
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    .line 1645
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1646
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2cb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1647
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1648
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1650
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteReview(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "cid"

    .prologue
    .line 942
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 943
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x9c5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 944
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 945
    const-string v1, "review_place_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 947
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static disableWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 3040
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 3041
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x8fd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3042
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3044
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static dismissSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "suggestionsUi"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 2343
    .local p3, personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2344
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2345
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2346
    const-string v1, "suggestions_ui"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2347
    const-string v1, "person_ids"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2349
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method protected static doDeleteCircles$6324374e(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p2, circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 5916
    new-instance v0, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 5918
    .local v0, op:Lcom/google/android/apps/plus/api/DeleteCirclesOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->start()V

    .line 5919
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    .line 5920
    .local v6, ex:Ljava/lang/Exception;
    if-eqz v6, :cond_21

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_21

    .line 5921
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not delete circles"

    invoke-direct {v1, v2, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 5922
    :cond_21
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_40

    .line 5923
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not delete circles: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5928
    :cond_40
    new-instance v7, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v7}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    .line 5929
    .local v7, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    const-string v1, "People sync after circle deletion"

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    .line 5930
    const/4 v1, 0x1

    invoke-static {p0, p1, v7, v4, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    .line 5931
    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    .line 5933
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    .line 5934
    return-void
.end method

.method protected static doDismissSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5862
    const-string v1, "suggestions_ui"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5863
    .local v3, suggestionsUi:Ljava/lang/String;
    const-string v1, "person_ids"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 5866
    .local v4, personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {p0, p1, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->deleteFromSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    .line 5868
    sget-object v8, Lcom/google/android/apps/plus/service/EsService;->sDismissPeopleLock:Ljava/lang/Object;

    monitor-enter v8

    .line 5869
    :try_start_12
    new-instance v0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 5871
    .local v0, op:Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->start()V

    .line 5872
    const-string v1, "EsService"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    .line 5876
    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2e
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v6, "op"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v6, 0x2c7

    if-ne v1, v6, :cond_65

    add-int/lit8 v1, v2, 0x1

    :goto_4a
    move v2, v1

    goto :goto_2e

    :cond_4c
    const/4 v1, 0x1

    if-le v2, v1, :cond_51

    .line 5877
    monitor-exit v8

    .line 5885
    :goto_50
    return-void

    .line 5881
    :cond_51
    new-instance v7, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v7, p0, p1, v1, v2}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 5883
    .local v7, gspo:Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->start()V

    .line 5884
    const-string v1, "EsService"

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    .line 5885
    monitor-exit v8
    :try_end_61
    .catchall {:try_start_12 .. :try_end_61} :catchall_62

    goto :goto_50

    .end local v0           #op:Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;
    .end local v7           #gspo:Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
    :catchall_62
    move-exception v1

    monitor-exit v8

    throw v1

    .restart local v0       #op:Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;
    :cond_65
    move v1, v2

    goto :goto_4a
.end method

.method protected static doReportAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 5832
    const-string v1, "gaia_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 5833
    .local v3, gaiaId:Ljava/lang/String;
    const-string v1, "abuse_type"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 5834
    .local v4, abuseType:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;

    move-object v1, p0

    move-object v2, p1

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 5836
    .local v0, op:Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->start()V

    .line 5837
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->getException()Ljava/lang/Exception;

    move-result-object v7

    .line 5838
    .local v7, ex:Ljava/lang/Exception;
    if-eqz v7, :cond_2c

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_2c

    .line 5839
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not report abuse"

    invoke-direct {v1, v2, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 5840
    :cond_2c
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 5841
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Could not report abuse: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->getErrorCode()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5846
    :cond_4b
    new-instance v8, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    .line 5847
    .local v8, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    const-string v1, "Post-report-abuse sync"

    invoke-virtual {v8, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    .line 5848
    const/4 v1, 0x1

    invoke-static {p0, p1, v8, v5, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    .line 5850
    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 5851
    return-void
.end method

.method public static editActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "content"

    .prologue
    .line 962
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 963
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 964
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 965
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 966
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 968
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static editComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "comment"

    .prologue
    .line 1847
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1848
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1849
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1850
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1851
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1852
    const-string v1, "content"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1854
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static editPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;Ljava/lang/String;Ljava/lang/String;)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "photoId"
    .parameter "commentId"
    .parameter "commentText"

    .prologue
    .line 1264
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1265
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1266
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1267
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1268
    const-string v1, "picasa_photo_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1269
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1270
    const-string v1, "content"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1272
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static enableAndPerformWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 3024
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 3025
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x8fc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3026
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3028
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private fetchC2dmId$faab209()Z
    .registers 4

    .prologue
    .line 5731
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    const-wide/16 v1, 0x7530

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;-><init>(J)V

    .line 5733
    .local v0, c2dmClient:Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->blockingGetC2dmToken(Landroid/content/Context;)V

    .line 5734
    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->hasError()Z

    move-result v1

    if-nez v1, :cond_12

    const/4 v1, 0x1

    :goto_11
    return v1

    :cond_12
    const/4 v1, 0x0

    goto :goto_11
.end method

.method private static generateRequestId()I
    .registers 3

    .prologue
    .line 6009
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    monitor-enter v1

    .line 6010
    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    monitor-exit v1
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_19

    return v0

    .line 6011
    :catchall_19
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3
    .parameter "context"

    .prologue
    .line 2447
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_e

    .line 2448
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 2449
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_c

    .line 2450
    const/4 v1, 0x0

    .line 2456
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :goto_b
    return-object v1

    .line 2452
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_c
    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 2456
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :cond_e
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_b
.end method

.method public static getActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 900
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 901
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 902
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 903
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 905
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getActivityAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 918
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 919
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 920
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 921
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 923
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "view"
    .parameter "circleId"
    .parameter "gaiaId"
    .parameter "continuationToken"
    .parameter "fromWidget"

    .prologue
    const/4 v2, 0x0

    .line 871
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 872
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 873
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 874
    const-string v1, "view"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 875
    if-eqz p3, :cond_21

    .line 876
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 878
    :cond_21
    if-eqz p4, :cond_28

    .line 879
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 881
    :cond_28
    if-eqz p5, :cond_2f

    .line 882
    const-string v1, "cont_token"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 884
    :cond_2f
    const-string v3, "newer"

    if-nez p5, :cond_41

    const/4 v1, 0x1

    :goto_34
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 885
    const-string v1, "from_widget"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 887
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1

    :cond_41
    move v1, v2

    .line 884
    goto :goto_34
.end method

.method public static getAlbumList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    .line 1171
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1172
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1173
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1174
    if-eqz p2, :cond_1b

    .line 1175
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1178
    :cond_1b
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "albumId"
    .parameter "gaiaId"

    .prologue
    .line 1193
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1194
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1195
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1196
    const-string v1, "album_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1197
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1199
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getCreatePostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/app/PendingIntent;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "id"

    .prologue
    .line 1114
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1115
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x0

    const/high16 v2, 0x1000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/app/PendingIntent;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2499
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2500
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.google.android.apps.plus.notif.clear"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2501
    const-string v1, "op"

    const/16 v2, 0xce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2502
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2503
    const/4 v1, 0x0

    const/high16 v2, 0x1000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"

    .prologue
    .line 2743
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2744
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x385

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2745
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2746
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2747
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2749
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getEventHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2135
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2136
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x384

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2137
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2139
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static getEventInvitees(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "includeBlackList"

    .prologue
    .line 2798
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2799
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x38e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2800
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2801
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2802
    const-string v1, "include_blacklist"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2804
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getLastCameraMediaLocation()Ljava/lang/String;
    .registers 1

    .prologue
    .line 6002
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sLastCameraMediaLocation:Ljava/lang/String;

    return-object v0
.end method

.method public static getLocalPhotoAsync(Landroid/content/Context;Lcom/google/android/apps/plus/api/MediaRef;I)V
    .registers 5
    .parameter "context"
    .parameter "ref"
    .parameter "maxSize"

    .prologue
    .line 1664
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1665
    .local v0, handler:Landroid/os/Handler;
    new-instance v1, Lcom/google/android/apps/plus/service/EsService$4;

    invoke-direct {v1, p1, p2, p0}, Lcom/google/android/apps/plus/service/EsService$4;-><init>(Lcom/google/android/apps/plus/api/MediaRef;ILandroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1684
    return-void
.end method

.method public static getNearbyActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "view"
    .parameter "location"
    .parameter "continuationToken"

    .prologue
    .line 841
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 842
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 843
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 844
    const-string v1, "view"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 845
    const-string v1, "loc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 846
    if-eqz p4, :cond_25

    .line 847
    const-string v1, "cont_token"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 849
    :cond_25
    const-string v2, "newer"

    if-nez p4, :cond_32

    const/4 v1, 0x1

    :goto_2a
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 851
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1

    .line 849
    :cond_32
    const/4 v1, 0x0

    goto :goto_2a
.end method

.method public static getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "query"

    .prologue
    .line 1961
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1962
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1963
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1964
    const-string v1, "loc_query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1966
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2634
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2635
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xcd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2636
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2638
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method private static getPendingRequestId(Landroid/content/Intent;)Ljava/lang/Integer;
    .registers 14
    .parameter "intent"

    .prologue
    const/4 v12, -0x1

    .line 5956
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 5957
    .local v5, nb:Landroid/os/Bundle;
    sget-object v9, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Intent;

    .line 5958
    .local v6, pIntent:Landroid/content/Intent;
    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 5959
    .local v8, pb:Landroid/os/Bundle;
    invoke-virtual {v5}, Landroid/os/Bundle;->size()I

    move-result v9

    invoke-virtual {v8}, Landroid/os/Bundle;->size()I

    move-result v10

    if-ne v9, v10, :cond_f

    .line 5960
    const/4 v3, 0x1

    .line 5961
    .local v3, match:Z
    invoke-virtual {v8}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_32
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 5963
    .local v2, key:Ljava/lang/String;
    const-string v9, "rid"

    invoke-virtual {v9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_32

    .line 5964
    invoke-virtual {v5, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7d

    .line 5966
    const/4 v3, 0x0

    .line 5986
    .end local v2           #key:Ljava/lang/String;
    :cond_4d
    :goto_4d
    if-eqz v3, :cond_f

    .line 5987
    const-string v9, "EsService"

    const/4 v10, 0x2

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_72

    .line 5988
    const-string v9, "EsService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Op was pending: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "op"

    invoke-virtual {v6, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5990
    :cond_72
    const-string v9, "rid"

    invoke-virtual {v6, v9, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 5995
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #match:Z
    .end local v6           #pIntent:Landroid/content/Intent;
    .end local v8           #pb:Landroid/os/Bundle;
    :goto_7c
    return-object v9

    .line 5969
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #key:Ljava/lang/String;
    .restart local v3       #match:Z
    .restart local v6       #pIntent:Landroid/content/Intent;
    .restart local v8       #pb:Landroid/os/Bundle;
    :cond_7d
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 5970
    .local v7, pValue:Ljava/lang/Object;
    invoke-virtual {v5, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 5971
    .local v4, nValue:Ljava/lang/Object;
    if-nez v7, :cond_ae

    .line 5972
    const-string v9, "EsService"

    const/4 v10, 0x3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_aa

    .line 5973
    const-string v9, "EsService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "pending request id key ["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] has value null!"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5975
    :cond_aa
    if-eqz v4, :cond_32

    .line 5976
    const/4 v3, 0x0

    .line 5977
    goto :goto_4d

    .line 5979
    :cond_ae
    invoke-virtual {v7, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_32

    .line 5980
    const/4 v3, 0x0

    .line 5981
    goto :goto_4d

    .line 5995
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #key:Ljava/lang/String;
    .end local v3           #match:Z
    .end local v4           #nValue:Ljava/lang/Object;
    .end local v6           #pIntent:Landroid/content/Intent;
    .end local v7           #pValue:Ljava/lang/Object;
    .end local v8           #pb:Landroid/os/Bundle;
    :cond_b6
    const/4 v9, 0x0

    goto :goto_7c
.end method

.method public static getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "photoId"

    .prologue
    .line 1462
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1463
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1464
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1465
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1466
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1468
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getPhotoSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    .line 1481
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1482
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1483
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1484
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1486
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getPhotosHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 1423
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1424
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1425
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1427
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getPhotosOfUser(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    .line 1381
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1382
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1383
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1384
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1386
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "refresh"

    .prologue
    .line 2000
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2001
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2bf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2002
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2003
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2004
    const-string v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2006
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static getRemotePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;III)Landroid/graphics/Bitmap;
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "ref"
    .parameter "width"
    .parameter "height"
    .parameter "cropType"

    .prologue
    const/4 v6, 0x0

    .line 1760
    new-instance v3, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;

    invoke-direct {v3, p2, v6}, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;-><init>(Lcom/google/android/apps/plus/api/MediaRef;I)V

    .line 1761
    .local v3, key:Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sPhotoImageCache:Lcom/google/android/apps/plus/service/PhotoCache;

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/service/PhotoCache;->get(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Object;III)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static getRemotePhotoAsync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;III)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "ref"
    .parameter "width"
    .parameter "height"
    .parameter "cropType"

    .prologue
    .line 1705
    new-instance v7, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1706
    .local v7, handler:Landroid/os/Handler;
    new-instance v0, Lcom/google/android/apps/plus/service/EsService$5;

    const/4 v2, 0x0

    move-object v1, p2

    move-object v3, p0

    move-object v4, p1

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService$5;-><init>(Lcom/google/android/apps/plus/api/MediaRef;ILandroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;II)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1725
    return-void
.end method

.method public static getStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "streamId"
    .parameter "offset"
    .parameter "maxCount"

    .prologue
    .line 1217
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1218
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x34

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1219
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1220
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1221
    const-string v1, "stream_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1222
    const-string v1, "offset"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1223
    const-string v1, "max_count"

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1225
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "fileName"

    .prologue
    .line 2399
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2400
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x460

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2401
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2402
    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2404
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "event"

    .prologue
    .line 1591
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1592
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3f0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1593
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1594
    const-string v1, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1596
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    .line 1597
    return-void
.end method

.method public static invitePeopleToEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"
    .parameter "audience"

    .prologue
    .line 2939
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2940
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x38a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2941
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2942
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2943
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2944
    const-string v1, "audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2946
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static isOutOfBoxError(Ljava/lang/Throwable;)Z
    .registers 3
    .parameter "t"

    .prologue
    .line 4625
    instance-of v0, p0, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v0, :cond_10

    check-cast p0, Lcom/google/android/apps/plus/api/OzServerException;

    .end local p0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static isPhotoPlusOnePending(Ljava/lang/String;Ljava/lang/String;J)Z
    .registers 11
    .parameter "gaiaId"
    .parameter "albumId"
    .parameter "photoId"

    .prologue
    .line 1331
    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 1332
    .local v2, pIntent:Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 1333
    .local v3, pb:Landroid/os/Bundle;
    const-string v4, "op"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1334
    .local v1, opCode:I
    const/16 v4, 0x38

    if-ne v1, v4, :cond_a

    .line 1335
    const-string v4, "gaia_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "album_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "photo_id"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v4, v4, p2

    if-nez v4, :cond_a

    .line 1338
    const/4 v4, 0x1

    .line 1343
    .end local v1           #opCode:I
    .end local v2           #pIntent:Landroid/content/Intent;
    .end local v3           #pb:Landroid/os/Bundle;
    :goto_49
    return v4

    :cond_4a
    const/4 v4, 0x0

    goto :goto_49
.end method

.method public static isPostPlusOnePending(Ljava/lang/String;)Z
    .registers 6
    .parameter "activityId"

    .prologue
    .line 1058
    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_36

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 1059
    .local v2, pIntent:Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 1060
    .local v3, pb:Landroid/os/Bundle;
    const-string v4, "op"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1061
    .local v1, opCode:I
    const/16 v4, 0x10

    if-eq v1, v4, :cond_28

    const/16 v4, 0x11

    if-ne v1, v4, :cond_a

    .line 1062
    :cond_28
    const-string v4, "aid"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1063
    const/4 v4, 0x1

    .line 1068
    .end local v1           #opCode:I
    .end local v2           #pIntent:Landroid/content/Intent;
    .end local v3           #pb:Landroid/os/Bundle;
    :goto_35
    return v4

    :cond_36
    const/4 v4, 0x0

    goto :goto_35
.end method

.method public static isProfilePlusOnePending(Ljava/lang/String;)Z
    .registers 6
    .parameter "gaiaId"

    .prologue
    .line 1552
    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_36

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 1553
    .local v2, pIntent:Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 1554
    .local v3, pb:Landroid/os/Bundle;
    const-string v4, "op"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1555
    .local v1, opCode:I
    const/16 v4, 0x2ca

    if-eq v1, v4, :cond_28

    const/16 v4, 0x2cb

    if-ne v1, v4, :cond_a

    .line 1556
    :cond_28
    const-string v4, "gaia_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1557
    const/4 v4, 0x1

    .line 1562
    .end local v1           #opCode:I
    .end local v2           #pIntent:Landroid/content/Intent;
    .end local v3           #pb:Landroid/os/Bundle;
    :goto_35
    return v4

    :cond_36
    const/4 v4, 0x0

    goto :goto_35
.end method

.method public static isRequestPending(I)Z
    .registers 3
    .parameter "requestId"

    .prologue
    .line 665
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static localeChanged(Landroid/content/Context;)I
    .registers 4
    .parameter "context"

    .prologue
    .line 822
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 823
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 825
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static manageEventGuest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "blacklist"
    .parameter "gaiaId"
    .parameter "email"

    .prologue
    .line 2963
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2964
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3f1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2965
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2966
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2967
    const-string v1, "blacklist"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2968
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2969
    const-string v1, "email"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2971
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "activityIds"

    .prologue
    .line 1147
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1148
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "op"

    const/16 v3, 0x19

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1149
    const-string v2, "acc"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1150
    const-string v2, "aid"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1152
    invoke-static {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    .line 1153
    .local v1, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    if-eqz v1, :cond_28

    .line 1154
    const-string v2, "start_view"

    invoke-virtual {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1157
    :cond_28
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2
.end method

.method public static markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "notificationId"

    .prologue
    .line 2214
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2215
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xc9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2216
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2217
    const-string v1, "notif_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2219
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static moderateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZZ)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "delete"
    .parameter "report"
    .parameter "isUndo"

    .prologue
    .line 1892
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1893
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1894
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1895
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1896
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1897
    const-string v1, "delete"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1898
    const-string v1, "report"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1899
    const-string v1, "is_undo"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1901
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static modifyCircleProperties(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "circleId"
    .parameter "circleName"
    .parameter "justFollowing"

    .prologue
    .line 2174
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2175
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2cc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2176
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2177
    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2178
    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2179
    const-string v1, "just_following"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2181
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "muteState"

    .prologue
    .line 1023
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1024
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1025
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1026
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1027
    const-string v1, "mute_state"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1029
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "photoId"
    .parameter "shapeId"
    .parameter "approved"

    .prologue
    .line 1403
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1404
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1405
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1406
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1407
    const-string v1, "photo_id"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1408
    const-string v1, "shape_id"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1409
    const-string v1, "approved"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1411
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static notifyDeepLinkingInstall(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "packageName"

    .prologue
    .line 3078
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 3079
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xa28

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3080
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3081
    const-string v1, "package_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3083
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static photoPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JZ)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "albumId"
    .parameter "photoId"
    .parameter "plusOned"

    .prologue
    .line 1360
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1361
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x38

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1362
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1363
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1364
    const-string v1, "album_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1365
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1366
    const-string v1, "plus_oned"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1368
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "plusOneId"
    .parameter "plusOne"

    .prologue
    .line 1940
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1941
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1942
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1943
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1944
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1945
    const-string v1, "plusone_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1946
    const-string v1, "plus_oned"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1948
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static postActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Lcom/google/android/apps/plus/network/ApiaryActivity;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;)I
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "analytics"
    .parameter "info"
    .parameter "activity"
    .parameter "audience"
    .parameter "externalId"
    .parameter "content"
    .parameter
    .parameter "location"
    .parameter "deepLinkId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/analytics/AnalyticsInfo;",
            "Lcom/google/android/apps/plus/network/ApiaryApiInfo;",
            "Lcom/google/android/apps/plus/network/ApiaryActivity;",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 2682
    .local p8, attachments:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2683
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x898

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2684
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2685
    const-string v1, "analytics"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2686
    const-string v1, "apiInfo"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2688
    const-string v1, "activity"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2689
    const-string v1, "audience"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2690
    const-string v1, "external_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2691
    const-string v1, "content"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2692
    const-string v1, "loc"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2693
    const-string v1, "deep_link_id"

    invoke-virtual {v0, v1, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2696
    if-eqz p8, :cond_43

    .line 2697
    const-string v1, "media"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2700
    :cond_43
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static postOnServiceThread(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "runnable"

    .prologue
    .line 3008
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_f

    .line 3009
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    .line 3011
    :cond_f
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3012
    return-void
.end method

.method public static postOnUiThread(Ljava/lang/Runnable;)V
    .registers 3
    .parameter "runnable"

    .prologue
    .line 5940
    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_f

    .line 5941
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sHandler:Landroid/os/Handler;

    .line 5943
    :cond_f
    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 5944
    return-void
.end method

.method private processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z
    .registers 121
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "opCode"

    .prologue
    .line 4080
    sparse-switch p4, :sswitch_data_768

    .line 4610
    const/4 v4, 0x0

    .line 4613
    :goto_4
    return v4

    .line 4082
    :sswitch_5
    new-instance v14, Lcom/google/android/apps/plus/api/EventHomePageOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v14, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/EventHomePageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4084
    .local v14, op:Lcom/google/android/apps/plus/api/EventHomePageOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/EventHomePageOperation;->startThreaded()V

    .line 4613
    .end local v14           #op:Lcom/google/android/apps/plus/api/EventHomePageOperation;
    :goto_17
    :sswitch_17
    const/4 v4, 0x1

    goto :goto_4

    .line 4089
    :sswitch_19
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4090
    .local v8, eventId:Ljava/lang/String;
    new-instance v15, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/apps/plus/service/EsService$15;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v9, p3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/plus/service/EsService$15;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v15, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v15}, Ljava/lang/Thread;->start()V

    goto :goto_17

    .line 4110
    .end local v8           #eventId:Ljava/lang/String;
    :sswitch_37
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4111
    .restart local v8       #eventId:Ljava/lang/String;
    const-string v4, "pollingtoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 4112
    .local v9, pollingToken:Ljava/lang/String;
    const-string v4, "resumetoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 4113
    .local v10, resumeToken:Ljava/lang/String;
    const-string v4, "invitationtoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 4114
    .local v11, invitationToken:Ljava/lang/String;
    const-string v4, "fetchnewer"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 4115
    .local v12, fetchNewer:Z
    const-string v4, "resolvetokens"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 4117
    .local v13, noToken:Z
    new-instance v15, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/apps/plus/service/EsService$16;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v14, p3

    invoke-direct/range {v4 .. v14}, Lcom/google/android/apps/plus/service/EsService$16;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/Intent;)V

    invoke-direct {v15, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v15}, Ljava/lang/Thread;->start()V

    goto :goto_17

    .line 4138
    .end local v8           #eventId:Ljava/lang/String;
    .end local v9           #pollingToken:Ljava/lang/String;
    .end local v10           #resumeToken:Ljava/lang/String;
    .end local v11           #invitationToken:Ljava/lang/String;
    .end local v12           #fetchNewer:Z
    .end local v13           #noToken:Z
    :sswitch_7f
    const-string v4, "include_blacklist"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    .line 4140
    .local v18, includeBlackList:Z
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4141
    .restart local v8       #eventId:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v20, v0

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v17, v8

    move-object/from16 v19, p3

    invoke-direct/range {v14 .. v20}, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4144
    .local v14, op:Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4149
    .end local v8           #eventId:Ljava/lang/String;
    .end local v14           #op:Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;
    .end local v18           #includeBlackList:Z
    :sswitch_a8
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4150
    .restart local v8       #eventId:Ljava/lang/String;
    const-string v4, "photo_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v112

    .line 4151
    .local v112, photoIds:[J
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 4153
    .local v23, photoIdList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    move-object/from16 v94, v112

    .local v94, arr$:[J
    move-object/from16 v0, v112

    array-length v0, v0

    move/from16 v107, v0

    .local v107, len$:I
    const/16 v101, 0x0

    .local v101, i$:I
    :goto_c6
    move/from16 v0, v101

    move/from16 v1, v107

    if-ge v0, v1, :cond_da

    aget-wide v110, v94, v101

    .line 4154
    .local v110, photoId:J
    invoke-static/range {v110 .. v111}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4153
    add-int/lit8 v101, v101, 0x1

    goto :goto_c6

    .line 4157
    .end local v110           #photoId:J
    :cond_da
    new-instance v14, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v25, v0

    move-object/from16 v19, v14

    move-object/from16 v20, p1

    move-object/from16 v21, p2

    move-object/from16 v22, p3

    move-object/from16 v24, v8

    invoke-direct/range {v19 .. v25}, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4159
    .local v14, op:Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4164
    .end local v8           #eventId:Ljava/lang/String;
    .end local v14           #op:Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;
    .end local v23           #photoIdList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    .end local v94           #arr$:[J
    .end local v101           #i$:I
    .end local v107           #len$:I
    .end local v112           #photoIds:[J
    :sswitch_f4
    const-string v4, "circle_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 4165
    .local v27, circleName:Ljava/lang/String;
    const-string v4, "just_following"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v28

    .line 4167
    .local v28, justFollowing:Z
    new-instance v14, Lcom/google/android/apps/plus/api/CreateCircleOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v30, v0

    move-object/from16 v24, v14

    move-object/from16 v25, p1

    move-object/from16 v26, p2

    move-object/from16 v29, p3

    invoke-direct/range {v24 .. v30}, Lcom/google/android/apps/plus/api/CreateCircleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4169
    .local v14, op:Lcom/google/android/apps/plus/api/CreateCircleOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/CreateCircleOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4174
    .end local v14           #op:Lcom/google/android/apps/plus/api/CreateCircleOperation;
    .end local v27           #circleName:Ljava/lang/String;
    .end local v28           #justFollowing:Z
    :sswitch_11d
    const-string v4, "circle_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 4175
    .local v32, circleId:Ljava/lang/String;
    const-string v4, "circle_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 4176
    .restart local v27       #circleName:Ljava/lang/String;
    const-string v4, "just_following"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v28

    .line 4178
    .restart local v28       #justFollowing:Z
    new-instance v14, Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v36, v0

    move-object/from16 v29, v14

    move-object/from16 v30, p1

    move-object/from16 v31, p2

    move-object/from16 v33, v27

    move/from16 v34, v28

    move-object/from16 v35, p3

    invoke-direct/range {v29 .. v36}, Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4181
    .local v14, op:Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4186
    .end local v14           #op:Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;
    .end local v27           #circleName:Ljava/lang/String;
    .end local v28           #justFollowing:Z
    .end local v32           #circleId:Ljava/lang/String;
    :sswitch_152
    const-string v4, "circle_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v37

    .line 4188
    .local v37, circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/Thread;

    new-instance v33, Lcom/google/android/apps/plus/service/EsService$17;

    move-object/from16 v34, p0

    move-object/from16 v35, p1

    move-object/from16 v36, p2

    move-object/from16 v38, p3

    invoke-direct/range {v33 .. v38}, Lcom/google/android/apps/plus/service/EsService$17;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Landroid/content/Intent;)V

    move-object/from16 v0, v33

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_17

    .line 4207
    .end local v37           #circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_173
    const-string v4, "refresh"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v42

    .line 4208
    .local v42, refresh:Z
    new-instance v4, Ljava/lang/Thread;

    new-instance v38, Lcom/google/android/apps/plus/service/EsService$18;

    move-object/from16 v39, p0

    move-object/from16 v40, p1

    move-object/from16 v41, p2

    move-object/from16 v43, p3

    invoke-direct/range {v38 .. v43}, Lcom/google/android/apps/plus/service/EsService$18;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;)V

    move-object/from16 v0, v38

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_17

    .line 4222
    .end local v42           #refresh:Z
    :sswitch_195
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/google/android/apps/plus/service/EsService$19;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$19;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_17

    .line 4241
    :sswitch_1ac
    new-instance v14, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v14, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4243
    .local v14, op:Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4248
    .end local v14           #op:Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
    :sswitch_1c0
    new-instance v14, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v14, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4250
    .local v14, op:Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4255
    .end local v14           #op:Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;
    :sswitch_1d4
    new-instance v14, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v14, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4257
    .local v14, op:Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4262
    .end local v14           #op:Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;
    :sswitch_1e8
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->accountsChanged(Landroid/content/Context;)Ljava/util/List;

    move-result-object v113

    .line 4263
    .local v113, removedAccounts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    .line 4264
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v113

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4269
    .end local v113           #removedAccounts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_202
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsProvider;->localeChanged(Landroid/content/Context;)V

    .line 4270
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4276
    :sswitch_214
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v92

    .line 4277
    .local v92, activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v92, :cond_25b

    .line 4279
    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v105

    .line 4282
    .local v105, lastSyncTime:J
    const-wide/16 v4, 0x0

    cmp-long v4, v105, v4

    if-ltz v4, :cond_235

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v105

    const-wide/32 v6, 0x36ee80

    cmp-long v4, v4, v6

    if-lez v4, :cond_25b

    .line 4284
    :cond_235
    invoke-virtual/range {v92 .. v92}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v4

    const-string v5, "com.google.android.apps.plus.content.EsProvider"

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    invoke-static {v4, v5, v6}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 4288
    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    if-eqz v4, :cond_25b

    .line 4290
    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->wipeout(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4297
    .end local v105           #lastSyncTime:J
    :cond_25b
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/phone/InstantUpload;->isEnabled(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v114

    .line 4299
    .local v114, resultValue:Ljava/lang/Boolean;
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v114

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4304
    .end local v92           #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v114           #resultValue:Ljava/lang/Boolean;
    :sswitch_273
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4309
    :sswitch_282
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v92

    .line 4310
    .restart local v92       #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v92, :cond_290

    .line 4311
    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v92

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/content/EsProvider;->cleanupData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    .line 4313
    :cond_290
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4318
    .end local v92           #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    :sswitch_29f
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v92

    .line 4319
    .restart local v92       #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v92, :cond_2fd

    move-object/from16 v0, v92

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2fd

    .line 4321
    const-string v4, "event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v96

    .line 4322
    .local v96, clientOzEventBytes:[B
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v96

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)V

    .line 4325
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->queryLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v103

    .line 4327
    .local v103, lastAnalyticsSyncTimestamp:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v97, v4, v103

    .line 4329
    .local v97, elapsedTimeSinceLastSync:J
    const-wide/16 v4, 0x0

    cmp-long v4, v103, v4

    if-ltz v4, :cond_2d7

    const-wide/32 v4, 0x5265c0

    cmp-long v4, v97, v4

    if-lez v4, :cond_2fd

    .line 4331
    :cond_2d7
    const-string v4, "EsService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2f5

    .line 4332
    const-string v4, "EsService"

    const-string v5, "%d has passed since the last analytics syncs. Send the analytics data."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v97 .. v98}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4336
    :cond_2f5
    const-string v4, "analytics_sync"

    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4339
    .end local v96           #clientOzEventBytes:[B
    .end local v97           #elapsedTimeSinceLastSync:J
    .end local v103           #lastAnalyticsSyncTimestamp:J
    :cond_2fd
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4344
    .end local v92           #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    :sswitch_30c
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v92

    .line 4345
    .restart local v92       #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v92, :cond_371

    move-object/from16 v0, v92

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_371

    .line 4346
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/List;

    move-result-object v99

    .line 4348
    .local v99, events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    invoke-interface/range {v99 .. v99}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_371

    .line 4349
    const-string v4, "EsService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_34d

    .line 4350
    const-string v4, "EsService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v99 .. v99}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " analytics events"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4352
    :cond_34d
    new-instance v14, Lcom/google/android/apps/plus/api/PostClientLogsOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v14, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4354
    .local v14, op:Lcom/google/android/apps/plus/api/PostClientLogsOperation;
    move-object/from16 v0, v99

    invoke-virtual {v14, v0}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->setClientOzEvents(Ljava/util/List;)V

    .line 4355
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->startThreaded()V

    .line 4358
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->saveLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    goto/16 :goto_17

    .line 4363
    .end local v14           #op:Lcom/google/android/apps/plus/api/PostClientLogsOperation;
    .end local v99           #events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    :cond_371
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4368
    .end local v92           #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    :sswitch_380
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v92

    .line 4369
    .restart local v92       #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v92, :cond_3ad

    move-object/from16 v0, v92

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3ad

    .line 4370
    const-string v4, "analytics_events"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v95

    .line 4371
    .local v95, byteEvents:[B
    invoke-static/range {v95 .. v95}, Lcom/google/android/apps/plus/content/DbAnalyticsEvents;->deserializeClientOzEventList([B)Ljava/util/List;

    move-result-object v99

    .line 4373
    .restart local v99       #events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    if-eqz v99, :cond_3ad

    invoke-interface/range {v99 .. v99}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3ad

    .line 4374
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v99

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    .line 4377
    .end local v95           #byteEvents:[B
    .end local v99           #events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    :cond_3ad
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4382
    .end local v92           #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    :sswitch_3bc
    const-string v4, "search_query"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    .line 4383
    .local v46, searchQuery:Ljava/lang/String;
    const-string v4, "newer"

    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v108

    .line 4385
    .local v108, newer:Z
    if-nez v108, :cond_3f1

    .line 4386
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/SearchUtils;->getContinuationToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    .line 4392
    .local v47, continuationToken:Ljava/lang/String;
    :goto_3d9
    new-instance v14, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v49, v0

    move-object/from16 v43, v14

    move-object/from16 v44, p1

    move-object/from16 v45, p2

    move-object/from16 v48, p3

    invoke-direct/range {v43 .. v49}, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4394
    .local v14, op:Lcom/google/android/apps/plus/api/SearchActivitiesOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4389
    .end local v14           #op:Lcom/google/android/apps/plus/api/SearchActivitiesOperation;
    .end local v47           #continuationToken:Ljava/lang/String;
    :cond_3f1
    const/16 v47, 0x0

    .restart local v47       #continuationToken:Ljava/lang/String;
    goto :goto_3d9

    .line 4399
    .end local v46           #searchQuery:Ljava/lang/String;
    .end local v47           #continuationToken:Ljava/lang/String;
    .end local v108           #newer:Z
    :sswitch_3f4
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAccountsData;->uploadChangedSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4400
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4405
    :sswitch_406
    const-string v4, "filename"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v100

    .line 4407
    .local v100, fileName:Ljava/lang/String;
    :try_start_40e
    move-object/from16 v0, p1

    move-object/from16 v1, v100

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->insertCameraPhoto(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/service/EsService;->sLastCameraMediaLocation:Ljava/lang/String;
    :try_end_418
    .catch Ljava/io/FileNotFoundException; {:try_start_40e .. :try_end_418} :catch_427

    .line 4411
    :goto_418
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4409
    :catch_427
    move-exception v4

    const/4 v4, 0x0

    sput-object v4, Lcom/google/android/apps/plus/service/EsService;->sLastCameraMediaLocation:Ljava/lang/String;

    goto :goto_418

    .line 4416
    .end local v100           #fileName:Ljava/lang/String;
    :sswitch_42c
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNetworkData;->clearTransactionData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4417
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4422
    :sswitch_43e
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNetworkData;->resetStatsData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4423
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4428
    :sswitch_450
    const-string v4, "apiInfo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v59

    check-cast v59, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 4430
    .local v59, info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    const-string v4, "activity"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v53

    check-cast v53, Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 4431
    .local v53, activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    const-string v4, "external_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v56

    .line 4432
    .local v56, externalId:Ljava/lang/String;
    const-string v4, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 4433
    .local v54, content:Ljava/lang/String;
    const-string v4, "media"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v55

    .line 4435
    .local v55, attachments:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/api/MediaRef;>;"
    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v58

    check-cast v58, Lcom/google/android/apps/plus/content/AudienceData;

    .line 4437
    .local v58, audience:Lcom/google/android/apps/plus/content/AudienceData;
    const-string v4, "loc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v57

    check-cast v57, Lcom/google/android/apps/plus/content/DbLocation;

    .line 4438
    .local v57, location:Lcom/google/android/apps/plus/content/DbLocation;
    const-string v4, "deep_link_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v60

    .line 4440
    .local v60, deepLinkId:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/network/ApiaryPostOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v52, v0

    move-object/from16 v48, v14

    move-object/from16 v49, p1

    move-object/from16 v50, p2

    move-object/from16 v51, p3

    invoke-direct/range {v48 .. v60}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/ApiaryActivity;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Ljava/lang/String;)V

    .line 4443
    .local v14, op:Lcom/google/android/apps/plus/network/ApiaryPostOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/network/ApiaryPostOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4448
    .end local v14           #op:Lcom/google/android/apps/plus/network/ApiaryPostOperation;
    .end local v53           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    .end local v54           #content:Ljava/lang/String;
    .end local v55           #attachments:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .end local v56           #externalId:Ljava/lang/String;
    .end local v57           #location:Lcom/google/android/apps/plus/content/DbLocation;
    .end local v58           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v59           #info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .end local v60           #deepLinkId:Ljava/lang/String;
    :sswitch_4b0
    const-string v4, "apiInfo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v59

    check-cast v59, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 4450
    .restart local v59       #info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    const-string v4, "url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    .line 4451
    .local v66, url:Ljava/lang/String;
    const-string v4, "applyPlusOne"

    const/4 v5, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v93

    .line 4452
    .local v93, apply:Z
    const-string v4, "token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v115

    .line 4454
    .local v115, token:Ljava/lang/String;
    new-instance v65, Landroid/content/ContentValues;

    invoke-direct/range {v65 .. v65}, Landroid/content/ContentValues;-><init>()V

    .line 4455
    .local v65, cv:Landroid/content/ContentValues;
    const-string v5, "state"

    if-eqz v93, :cond_53d

    sget-object v4, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_PLUSONED:Ljava/lang/Integer;

    :goto_4de
    move-object/from16 v0, v65

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 4458
    const-string v4, "token"

    move-object/from16 v0, v65

    move-object/from16 v1, v115

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 4460
    invoke-virtual/range {v59 .. v59}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v4

    const-string v5, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "apiKey"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "clientId"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "apiVersion"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "pkg"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v64

    .line 4461
    .local v64, uri:Landroid/net/Uri;
    new-instance v4, Ljava/lang/Thread;

    new-instance v61, Lcom/google/android/apps/plus/service/EsService$20;

    move-object/from16 v62, p0

    move-object/from16 v63, p1

    move-object/from16 v67, p3

    invoke-direct/range {v61 .. v67}, Lcom/google/android/apps/plus/service/EsService$20;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v61

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_17

    .line 4455
    .end local v64           #uri:Landroid/net/Uri;
    :cond_53d
    sget-object v4, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_NOTPLUSONED:Ljava/lang/Integer;

    goto :goto_4de

    .line 4476
    .end local v59           #info:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .end local v65           #cv:Landroid/content/ContentValues;
    .end local v66           #url:Ljava/lang/String;
    .end local v93           #apply:Z
    .end local v115           #token:Ljava/lang/String;
    :sswitch_540
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4477
    .restart local v8       #eventId:Ljava/lang/String;
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v72

    .line 4478
    .local v72, ownerId:Ljava/lang/String;
    const-string v4, "rsvp_type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v73

    .line 4480
    .local v73, rsvpType:Ljava/lang/String;
    new-instance v4, Ljava/lang/Thread;

    new-instance v67, Lcom/google/android/apps/plus/service/EsService$21;

    move-object/from16 v68, p0

    move-object/from16 v69, p1

    move-object/from16 v70, p2

    move-object/from16 v71, v8

    move-object/from16 v74, p3

    invoke-direct/range {v67 .. v74}, Lcom/google/android/apps/plus/service/EsService$21;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v67

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_17

    .line 4499
    .end local v8           #eventId:Ljava/lang/String;
    .end local v72           #ownerId:Ljava/lang/String;
    .end local v73           #rsvpType:Ljava/lang/String;
    :sswitch_573
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    const-string v5, "event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v77

    check-cast v77, Lcom/google/api/services/plusi/model/PlusEvent;

    .line 4501
    .local v77, event:Lcom/google/api/services/plusi/model/PlusEvent;
    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v58

    check-cast v58, Lcom/google/android/apps/plus/content/AudienceData;

    .line 4503
    .restart local v58       #audience:Lcom/google/android/apps/plus/content/AudienceData;
    const-string v4, "external_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v56

    .line 4504
    .restart local v56       #externalId:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/api/CreateEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v81, v0

    move-object/from16 v74, v14

    move-object/from16 v75, p1

    move-object/from16 v76, p2

    move-object/from16 v78, v58

    move-object/from16 v79, v56

    move-object/from16 v80, p3

    invoke-direct/range {v74 .. v81}, Lcom/google/android/apps/plus/api/CreateEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4506
    .local v14, op:Lcom/google/android/apps/plus/api/CreateEventOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/CreateEventOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4511
    .end local v14           #op:Lcom/google/android/apps/plus/api/CreateEventOperation;
    .end local v56           #externalId:Ljava/lang/String;
    .end local v58           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v77           #event:Lcom/google/api/services/plusi/model/PlusEvent;
    :sswitch_5b3
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    const-string v5, "event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v77

    check-cast v77, Lcom/google/api/services/plusi/model/PlusEvent;

    .line 4513
    .restart local v77       #event:Lcom/google/api/services/plusi/model/PlusEvent;
    new-instance v14, Lcom/google/android/apps/plus/api/UpdateEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v79, v0

    move-object/from16 v74, v14

    move-object/from16 v75, p1

    move-object/from16 v76, p2

    move-object/from16 v78, p3

    invoke-direct/range {v74 .. v79}, Lcom/google/android/apps/plus/api/UpdateEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4515
    .local v14, op:Lcom/google/android/apps/plus/api/UpdateEventOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/UpdateEventOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4520
    .end local v14           #op:Lcom/google/android/apps/plus/api/UpdateEventOperation;
    .end local v77           #event:Lcom/google/api/services/plusi/model/PlusEvent;
    :sswitch_5dd
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4521
    .restart local v8       #eventId:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/api/DeleteEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v83, v0

    move-object/from16 v78, v14

    move-object/from16 v79, p1

    move-object/from16 v80, p2

    move-object/from16 v81, v8

    move-object/from16 v82, p3

    invoke-direct/range {v78 .. v83}, Lcom/google/android/apps/plus/api/DeleteEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4523
    .local v14, op:Lcom/google/android/apps/plus/api/DeleteEventOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/DeleteEventOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4528
    .end local v8           #eventId:Ljava/lang/String;
    .end local v14           #op:Lcom/google/android/apps/plus/api/DeleteEventOperation;
    :sswitch_5ff
    new-instance v14, Lcom/google/android/apps/plus/api/GetEventThemesOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v14, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4530
    .local v14, op:Lcom/google/android/apps/plus/api/GetEventThemesOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4535
    .end local v14           #op:Lcom/google/android/apps/plus/api/GetEventThemesOperation;
    :sswitch_613
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4536
    .restart local v8       #eventId:Ljava/lang/String;
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v72

    .line 4537
    .restart local v72       #ownerId:Ljava/lang/String;
    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v58

    check-cast v58, Lcom/google/android/apps/plus/content/AudienceData;

    .line 4539
    .restart local v58       #audience:Lcom/google/android/apps/plus/content/AudienceData;
    new-instance v14, Lcom/google/android/apps/plus/api/EventInviteOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v85, v0

    move-object/from16 v78, v14

    move-object/from16 v79, p1

    move-object/from16 v80, p2

    move-object/from16 v81, v8

    move-object/from16 v82, v72

    move-object/from16 v83, v58

    move-object/from16 v84, p3

    invoke-direct/range {v78 .. v85}, Lcom/google/android/apps/plus/api/EventInviteOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4541
    .local v14, op:Lcom/google/android/apps/plus/api/EventInviteOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/EventInviteOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4546
    .end local v8           #eventId:Ljava/lang/String;
    .end local v14           #op:Lcom/google/android/apps/plus/api/EventInviteOperation;
    .end local v58           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v72           #ownerId:Ljava/lang/String;
    :sswitch_64b
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4547
    .restart local v8       #eventId:Ljava/lang/String;
    const-string v4, "blacklist"

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v82

    .line 4548
    .local v82, blacklist:Z
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v83

    .line 4549
    .local v83, gaiaId:Ljava/lang/String;
    const-string v4, "email"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v84

    .line 4550
    .local v84, email:Ljava/lang/String;
    new-instance v14, Lcom/google/android/apps/plus/api/EventManageGuestOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v86, v0

    move-object/from16 v78, v14

    move-object/from16 v79, p1

    move-object/from16 v80, p2

    move-object/from16 v81, v8

    move-object/from16 v85, p3

    invoke-direct/range {v78 .. v86}, Lcom/google/android/apps/plus/api/EventManageGuestOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 4552
    .local v14, op:Lcom/google/android/apps/plus/api/EventManageGuestOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4557
    .end local v8           #eventId:Ljava/lang/String;
    .end local v14           #op:Lcom/google/android/apps/plus/api/EventManageGuestOperation;
    .end local v82           #blacklist:Z
    .end local v83           #gaiaId:Ljava/lang/String;
    .end local v84           #email:Ljava/lang/String;
    :sswitch_686
    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    .line 4559
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->wipeout(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    goto/16 :goto_17

    .line 4564
    :sswitch_6a5
    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    .line 4566
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4571
    :sswitch_6c4
    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v5, "timestamp"

    const-wide/16 v6, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    .line 4574
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4579
    :sswitch_6ec
    const-string v4, "package_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v109

    .line 4580
    .local v109, packageName:Ljava/lang/String;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v109

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->getByPackageName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;

    move-result-object v102

    .line 4582
    .local v102, install:Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;
    if-eqz v102, :cond_726

    .line 4583
    move-object/from16 v0, v102

    iget-object v4, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->authorName:Ljava/lang/String;

    move-object/from16 v0, v102

    iget-object v5, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->creationSource:Ljava/lang/String;

    move-object/from16 v0, v102

    iget-object v6, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->data:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v109

    invoke-static {v0, v4, v5, v1, v6}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->notifyCompletedInstall(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4585
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v109

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->removeByPackageName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 4586
    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 4589
    :cond_726
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_17

    .line 4594
    .end local v102           #install:Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;
    .end local v109           #packageName:Ljava/lang/String;
    :sswitch_735
    new-instance v14, Lcom/google/android/apps/plus/api/WriteReviewOperation;

    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v87

    check-cast v87, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v89, v0

    const-string v4, "review_to_submit"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v90

    check-cast v90, Lcom/google/android/apps/plus/content/GooglePlaceReview;

    const-string v4, "review_place_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v91

    move-object/from16 v85, v14

    move-object/from16 v86, p0

    move-object/from16 v88, p3

    invoke-direct/range {v85 .. v91}, Lcom/google/android/apps/plus/api/WriteReviewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/GooglePlaceReview;Ljava/lang/String;)V

    .line 4600
    .local v14, op:Lcom/google/android/apps/plus/api/WriteReviewOperation;
    invoke-virtual {v14}, Lcom/google/android/apps/plus/api/WriteReviewOperation;->startThreaded()V

    goto/16 :goto_17

    .line 4080
    nop

    :sswitch_data_768
    .sparse-switch
        0x4 -> :sswitch_1e8
        0x5 -> :sswitch_202
        0x1f4 -> :sswitch_173
        0x1f6 -> :sswitch_1ac
        0x1f7 -> :sswitch_1d4
        0x1f8 -> :sswitch_1c0
        0x2c4 -> :sswitch_f4
        0x2c5 -> :sswitch_152
        0x2c7 -> :sswitch_195
        0x2cc -> :sswitch_11d
        0x384 -> :sswitch_5
        0x385 -> :sswitch_19
        0x386 -> :sswitch_540
        0x387 -> :sswitch_573
        0x388 -> :sswitch_5b3
        0x389 -> :sswitch_5ff
        0x38a -> :sswitch_613
        0x38b -> :sswitch_5dd
        0x38d -> :sswitch_37
        0x38e -> :sswitch_7f
        0x38f -> :sswitch_a8
        0x3ea -> :sswitch_214
        0x3ec -> :sswitch_273
        0x3ed -> :sswitch_282
        0x3f0 -> :sswitch_29f
        0x3f1 -> :sswitch_64b
        0x3f2 -> :sswitch_30c
        0x3f3 -> :sswitch_380
        0x44c -> :sswitch_3bc
        0x456 -> :sswitch_3f4
        0x460 -> :sswitch_406
        0x7d0 -> :sswitch_43e
        0x7d1 -> :sswitch_42c
        0x898 -> :sswitch_450
        0x899 -> :sswitch_4b0
        0x8fc -> :sswitch_686
        0x8fd -> :sswitch_6a5
        0x960 -> :sswitch_6c4
        0x9c4 -> :sswitch_735
        0x9c5 -> :sswitch_17
        0xa28 -> :sswitch_6ec
    .end sparse-switch
.end method

.method private static putAccountSettingsResponse(ILcom/google/android/apps/plus/content/AccountSettingsData;)V
    .registers 4
    .parameter "requestId"
    .parameter "pages"

    .prologue
    .line 694
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    return-void
.end method

.method private static putOutOfBoxResponse(ILcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V
    .registers 4
    .parameter "requestId"
    .parameter "oobResponse"

    .prologue
    .line 681
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sOutOfBoxResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    return-void
.end method

.method public static readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"
    .parameter "pollingToken"
    .parameter "resumeToken"
    .parameter "invitationToken"
    .parameter "fetchNewer"

    .prologue
    .line 2765
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2766
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x38d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2767
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2768
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2769
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2770
    const-string v1, "pollingtoken"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2771
    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2772
    const-string v1, "invitationtoken"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2773
    const-string v1, "fetchnewer"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2775
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"
    .parameter "fetchNewer"

    .prologue
    const/4 v3, 0x1

    .line 2781
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2782
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x38d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2783
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2784
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2785
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2786
    const-string v1, "fetchnewer"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2787
    const-string v1, "resolvetokens"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2789
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 645
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 646
    return-void
.end method

.method public static removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 777
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 778
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 779
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 781
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;
    .registers 3
    .parameter "requestId"

    .prologue
    .line 702
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AccountSettingsData;

    return-object v0
.end method

.method public static removeIncompleteOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .registers 3
    .parameter "requestId"

    .prologue
    .line 712
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v0

    .line 713
    .local v0, oob:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    if-eqz v0, :cond_13

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 715
    const/4 v0, 0x0

    .line 717
    :cond_13
    return-object v0
.end method

.method public static removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .registers 3
    .parameter "requestId"

    .prologue
    .line 690
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sOutOfBoxResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-object v0
.end method

.method public static removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;
    .registers 3
    .parameter "requestId"

    .prologue
    .line 677
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sResults:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/ServiceResult;

    return-object v0
.end method

.method public static reportActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1042
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1043
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1044
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1045
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1047
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static reportPhotoAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "gaiaId"

    .prologue
    .line 1501
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1502
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1503
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1504
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1505
    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1507
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static reportPhotoComment$3486cdbb(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;ZZ)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "commentId"
    .parameter "delete"
    .parameter "isUndo"

    .prologue
    .line 1310
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1311
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1312
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1313
    const-string v1, "photo_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1314
    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1315
    const-string v1, "delete"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1316
    const-string v1, "is_undo"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1318
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static reportProfileAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "abuseType"

    .prologue
    .line 2596
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2597
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2598
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2599
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2600
    const-string v1, "abuse_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2602
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static reshareActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "reshareId"
    .parameter "content"
    .parameter "audience"

    .prologue
    .line 984
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 985
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 986
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 987
    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 988
    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 989
    const-string v1, "audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 991
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static saveLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "timestamp"

    .prologue
    .line 3058
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 3059
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x960

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3060
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3061
    const-string v1, "timestamp"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 3063
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static savePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "url"
    .parameter "isFullRes"
    .parameter "description"

    .prologue
    .line 1781
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1782
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x42

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1783
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1784
    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1785
    const-string v1, "full_res"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1786
    const-string v1, "description"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1788
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static scheduleDatabaseCleanupAlarm(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    .prologue
    .line 2528
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2529
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2530
    .local v7, intent:Landroid/content/Intent;
    const-string v1, "com.google.android.apps.plus.content.cleanup"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2531
    const-string v1, "op"

    const/16 v2, 0x3ed

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2533
    const/4 v1, 0x0

    const/high16 v2, 0x1000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2535
    .local v6, pendingIntent:Landroid/app/PendingIntent;
    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    const-wide/32 v4, 0x2932e00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 2538
    return-void
.end method

.method public static scheduleSyncAlarm(Landroid/content/Context;)V
    .registers 9
    .parameter "context"

    .prologue
    .line 2478
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2479
    .local v0, am:Landroid/app/AlarmManager;
    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2480
    .local v7, intent:Landroid/content/Intent;
    const-string v1, "com.google.android.apps.plus.content.sync"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2481
    const-string v1, "op"

    const/16 v2, 0x3ea

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2483
    const/4 v1, 0x0

    const/high16 v2, 0x1000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 2485
    .local v6, pendingIntent:Landroid/app/PendingIntent;
    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    .line 2488
    return-void
.end method

.method public static searchActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "searchQuery"
    .parameter "newer"

    .prologue
    .line 2653
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2654
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x44c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2655
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2656
    const-string v1, "search_query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2657
    const-string v1, "newer"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2659
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendEventRsvp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"
    .parameter "rsvpType"

    .prologue
    .line 2841
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2842
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x386

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2843
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2844
    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2845
    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2846
    const-string v1, "rsvp_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2848
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendOutOfBoxRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "oobRequest"

    .prologue
    .line 1980
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1981
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x258

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1982
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1983
    const-string v1, "content"

    new-instance v2, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    invoke-direct {v2, p2}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1985
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "account"

    .prologue
    .line 2465
    const-string v0, "EsService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 2466
    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "setActiveAccount: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p0, :cond_23

    const/4 v0, 0x0

    :goto_15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2469
    :cond_20
    sput-object p0, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 2470
    return-void

    .line 2466
    :cond_23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_15
.end method

.method public static setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "name"
    .parameter "circlesToAdd"
    .parameter "circlesToRemove"

    .prologue
    .line 2022
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2023
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2be

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2024
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2025
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2026
    const-string v1, "person_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2027
    const-string v1, "circles_to_add"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2028
    const-string v1, "circles_to_remove"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2030
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static setPersonBlocked(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "name"
    .parameter "blocked"

    .prologue
    .line 2574
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2575
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2576
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2577
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2578
    const-string v1, "person_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2579
    const-string v1, "blocked"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2581
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static setPersonMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "muted"

    .prologue
    .line 2552
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2553
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2554
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2555
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2556
    const-string v1, "muted"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2558
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static setProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "photoBytes"

    .prologue
    .line 1536
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 1537
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x2c9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1538
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1539
    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 1541
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sharePhotosToEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2812
    .local p3, photoIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    sget-object v3, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v4, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    .line 2813
    .local v1, intent:Landroid/content/Intent;
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    new-array v2, v3, [J

    .line 2815
    .local v2, photoIdArray:[J
    const/4 v0, 0x0

    .local v0, i:I
    :goto_f
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_24

    .line 2816
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v2, v0

    .line 2815
    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    .line 2819
    :cond_24
    const-string v3, "op"

    const/16 v4, 0x38f

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2820
    const-string v3, "acc"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2821
    const-string v3, "event_id"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2822
    const-string v3, "photo_ids"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 2824
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v3

    return v3
.end method

.method private static startCommand(Landroid/content/Context;Landroid/content/Intent;)I
    .registers 6
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 2983
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-eq v2, v3, :cond_16

    .line 2984
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "startCommand must be called on the UI thread"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2987
    :cond_16
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->generateRequestId()I

    move-result v1

    .line 2988
    .local v1, requestId:I
    const-string v2, "rid"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2990
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->getPendingRequestId(Landroid/content/Intent;)Ljava/lang/Integer;

    move-result-object v0

    .line 2991
    .local v0, pendingRequestId:Ljava/lang/Integer;
    if-eqz v0, :cond_2f

    .line 2992
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/service/IntentPool;->put(Landroid/content/Intent;)V

    .line 2994
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2999
    .end local v1           #requestId:I
    :goto_2e
    return v1

    .line 2996
    .restart local v1       #requestId:I
    :cond_2f
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2997
    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2e
.end method

.method public static syncBlockedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2325
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2326
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x1f7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2327
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2329
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static syncComplete(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "tag"

    .prologue
    .line 2514
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2515
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x3ec

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2516
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2517
    const-string v1, "content"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2519
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    .line 2520
    return-void
.end method

.method public static syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2247
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2248
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2249
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2251
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)Ljava/lang/Integer;
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "refresh"

    .prologue
    .line 2284
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2285
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2286
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2287
    const-string v1, "refresh"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2289
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static tellServerNotificationsWereRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2231
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2232
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xcb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2233
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2235
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 654
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 655
    return-void
.end method

.method private static updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "accountName"

    .prologue
    const/4 v3, 0x0

    .line 5738
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 5739
    .local v0, contentValues:Landroid/content/ContentValues;
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5740
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://com.google.android.apps.plus.content.ApiProvider/account"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 5743
    return-void
.end method

.method public static updateEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "event"

    .prologue
    .line 2884
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2885
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x388

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2886
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2887
    const-string v1, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2889
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 793
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 794
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 795
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 797
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static uploadChangedSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 2383
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2384
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x456

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2385
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2387
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    .line 2388
    return-void
.end method

.method public static uploadImageThumbnail(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/api/MediaRef;)Ljava/lang/Integer;
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "albumTitle"
    .parameter "albumLabel"
    .parameter
    .parameter "mediaRef"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    .prologue
    .line 2115
    .local p4, streamIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 2116
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "op"

    const/16 v3, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2117
    const-string v2, "acc"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2118
    const-string v2, "album_title"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2119
    const-string v2, "album_label"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2120
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {p4, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 2121
    .local v1, streamIdArray:[Ljava/lang/String;
    const-string v2, "stream_ids"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2122
    const-string v2, "media_ref"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2124
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2
.end method

.method public static writeReview(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/GoogleReviewProto;Ljava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "review"
    .parameter "cid"

    .prologue
    .line 928
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    .line 929
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 930
    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 931
    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 932
    const-string v1, "review_to_submit"

    new-instance v2, Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-direct {v2, p3}, Lcom/google/android/apps/plus/content/GooglePlaceReview;-><init>(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 933
    const-string v1, "review_place_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 935
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    .prologue
    .line 3131
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .registers 4

    .prologue
    .line 3091
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 3093
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_12

    .line 3094
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    .line 3097
    :cond_12
    new-instance v0, Lcom/google/android/apps/plus/service/ServiceThread;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    const-string v2, "ServiceThread"

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/plus/service/ServiceThread;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    .line 3098
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceThread;->start()V

    .line 3099
    return-void
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 3118
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 3120
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    if-eqz v0, :cond_f

    .line 3121
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceThread;->quit()V

    .line 3122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    .line 3124
    :cond_f
    return-void
.end method

.method public final onIntentProcessed(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    .registers 52
    .parameter "intent"
    .parameter "serviceResult"
    .parameter "resultValue"

    .prologue
    .line 4638
    move-object/from16 v23, p0

    .line 4639
    .local v23, context:Landroid/content/Context;
    const-string v10, "op"

    const/16 v45, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v32

    .line 4640
    .local v32, opCode:I
    const-string v10, "rid"

    const/16 v45, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 4641
    .local v5, requestId:I
    sparse-switch v32, :sswitch_data_e8c

    .line 5631
    const-string v10, "EsService"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "onIntentProcessed: Unhandled op code: "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-static {v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5636
    .end local p3
    :cond_37
    :goto_37
    :sswitch_37
    const-string v10, "rid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_78

    const-string v10, "rid"

    const/16 v45, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v45, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    move-object/from16 v0, v45

    invoke-interface {v0, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_63

    sget-object v45, Lcom/google/android/apps/plus/service/EsService;->sResults:Ljava/util/Map;

    move-object/from16 v0, v45

    move-object/from16 v1, p2

    invoke-interface {v0, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_63
    const-string v10, "from_pool"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_78

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/service/IntentPool;->put(Landroid/content/Intent;)V

    :cond_78
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v10

    if-nez v10, :cond_b3

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mStopRunnable:Ljava/lang/Runnable;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    invoke-virtual {v10, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mStopRunnable:Ljava/lang/Runnable;

    move-object/from16 v45, v0

    const-wide/16 v46, 0x1388

    move-object/from16 v0, v45

    move-wide/from16 v1, v46

    invoke-virtual {v10, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v10, "EsService"

    const/16 v45, 0x3

    move/from16 v0, v45

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_b3

    const-string v10, "EsService"

    const-string v45, "completeRequest: Stopping service in 5000 ms"

    move-object/from16 v0, v45

    invoke-static {v10, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5637
    :cond_b3
    return-void

    .restart local p3
    :sswitch_b4
    move-object/from16 v16, p3

    .line 4644
    check-cast v16, Lcom/google/android/apps/plus/content/EsAccount;

    .line 4645
    .local v16, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-eqz v10, :cond_c8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v10

    if-eqz v10, :cond_e3

    .line 4648
    :cond_c8
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4649
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    .line 4651
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-interface {v10, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e3

    .line 4654
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->restoreAccountSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4658
    :cond_e3
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .local v28, i$:Ljava/util/Iterator;
    :goto_e9
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4659
    .local v4, listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAccountAdded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e9

    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v16           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_fd
    move-object/from16 v16, p3

    .line 4666
    check-cast v16, Lcom/google/android/apps/plus/content/EsAccount;

    .line 4668
    .restart local v16       #account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-nez v10, :cond_114

    .line 4669
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4670
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    .line 4673
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->restoreAccountSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4676
    :cond_114
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_11a
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4677
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAccountActivated$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_11a

    .line 4683
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v16           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_12c
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4684
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_136
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_147

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 4685
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    goto :goto_136

    .line 4688
    :cond_147
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    goto/16 :goto_37

    .line 4693
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_14c
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 4694
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_156
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4695
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAccountUpgraded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_156

    .line 4703
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_172
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_178
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4704
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "circle_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "view"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v10, "loc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "newer"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v45, "max_length"

    const/16 v46, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v45

    move/from16 v0, v45

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v10, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetActivities$35a362dd(IZILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_178

    .line 4718
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_1cd
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_1d3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4719
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetActivity$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_1d3

    .line 4729
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_1f4
    if-eqz p3, :cond_224

    .line 4730
    check-cast p3, Lcom/google/android/apps/plus/api/GetAudienceOperation;

    .end local p3
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/api/GetAudienceOperation;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v22

    .line 4734
    .local v22, audience:Lcom/google/android/apps/plus/content/AudienceData;
    :goto_1fc
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_202
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4735
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetActivityAudience$6db92636(ILcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_202

    .line 4732
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v22           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v28           #i$:Ljava/util/Iterator;
    .restart local p3
    :cond_224
    const/16 v22, 0x0

    .restart local v22       #audience:Lcom/google/android/apps/plus/content/AudienceData;
    goto :goto_1fc

    .line 4744
    .end local v22           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    :sswitch_227
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_22d
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4745
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_22d

    .line 4754
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_24d
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_253
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4755
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReshareActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_253

    .line 4764
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_273
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_279
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4765
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_279

    .line 4774
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_299
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_29f
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4775
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onMuteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_29f

    .line 4784
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_2bf
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_2c5
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4785
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_2c5

    .line 4794
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_2e5
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_2eb
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4795
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetAlbumListComplete$6a63df5(I)V

    goto :goto_2eb

    .line 4803
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_302
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_308
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4804
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetAlbumComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_308

    .line 4812
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_321
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_327
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4813
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetStreamPhotosComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_327

    .line 4821
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_340
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_346
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4822
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_346

    .line 4830
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_35f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_365
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4831
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_365

    .line 4839
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_37e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_384
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4840
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_384

    .line 4848
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_39d
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_3a3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4849
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v45, "is_undo"

    const/16 v46, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v45

    move/from16 v0, v45

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v10, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_3a3

    .line 4859
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_3d4
    const-string v10, "plus_oned"

    const/16 v45, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v36

    .line 4860
    .local v36, plusOned:Z
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 4861
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_3ed
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4862
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move/from16 v0, v36

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_3ed

    .line 4868
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v36           #plusOned:Z
    :sswitch_401
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_407
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4869
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetPhotosOfUserComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_407

    .line 4877
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_420
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_426
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4878
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "photo_id"

    const-wide/16 v45, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v45

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v45

    move-wide/from16 v0, v45

    move-object/from16 v2, p2

    invoke-virtual {v4, v5, v0, v1, v2}, Lcom/google/android/apps/plus/service/EsServiceListener;->onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_426

    .line 4887
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_44d
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 4888
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_45a
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4889
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPhotosHomeComplete$6a63df5(I)V

    goto :goto_45a

    .line 4895
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_46a
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 4896
    const-string v10, "photo_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 4899
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_47e
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4900
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_47e

    .line 4906
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_490
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 4907
    const-string v10, "photo_id"

    const-wide/16 v45, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v45

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 4909
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_4a8
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4910
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4a8

    .line 4916
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_4ba
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 4917
    const-string v10, "photo_id"

    const-wide/16 v45, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v45

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v33

    .line 4919
    .local v33, photoId:J
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_4d3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4920
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-wide/from16 v0, v33

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetPhoto$4894d499(IJ)V

    goto :goto_4d3

    .line 4930
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v33           #photoId:J
    :sswitch_4e5
    const-string v10, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 4932
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_4f2
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4933
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const/4 v10, 0x0

    invoke-virtual {v4, v5, v10}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetPhotoSettings$6e3d3b8d(IZ)V

    goto :goto_4f2

    .line 4940
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_503
    const-string v10, "full_res"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 4941
    .local v7, isFullRes:Z
    const-string v10, "description"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4943
    .local v8, description:Ljava/lang/String;
    const/4 v6, 0x0

    .line 4944
    .local v6, saveToFile:Ljava/io/File;
    const/4 v9, 0x0

    .line 4945
    .local v9, mimeType:Ljava/lang/String;
    if-eqz p3, :cond_527

    move-object/from16 v42, p3

    .line 4946
    check-cast v42, Lcom/google/android/apps/plus/api/SavePhotoOperation;

    .line 4947
    .local v42, spo:Lcom/google/android/apps/plus/api/SavePhotoOperation;
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/apps/plus/api/SavePhotoOperation;->getSaveToFile()Ljava/io/File;

    move-result-object v6

    .line 4948
    invoke-virtual/range {v42 .. v42}, Lcom/google/android/apps/plus/api/SavePhotoOperation;->getContentType()Ljava/lang/String;

    move-result-object v9

    .line 4951
    .end local v42           #spo:Lcom/google/android/apps/plus/api/SavePhotoOperation;
    :cond_527
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_52d
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v10, p2

    .line 4952
    invoke-virtual/range {v4 .. v10}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_52d

    .line 4959
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v6           #saveToFile:Ljava/io/File;
    .end local v7           #isFullRes:Z
    .end local v8           #description:Ljava/lang/String;
    .end local v9           #mimeType:Ljava/lang/String;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_53f
    const-string v10, "media_refs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v29

    check-cast v29, Ljava/util/ArrayList;

    .line 4962
    .local v29, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_54f
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4963
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, v29

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_54f

    .line 4969
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v29           #mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :sswitch_563
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_569
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4970
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateComment$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_569

    .line 4978
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_589
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_58f
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4979
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateEventComment$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_58f

    .line 4986
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_5a8
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_5ae
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4987
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5ae

    .line 4997
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_5d5
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_5db
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 4998
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5db

    .line 5008
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_602
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_608
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5009
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v45, "is_undo"

    const/16 v46, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v45

    move/from16 v0, v45

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v10, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onModerateComment$56b78e3(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_608

    .line 5020
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_640
    const-string v10, "plus_oned"

    const/16 v45, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v35

    .line 5021
    .local v35, plusOne:Z
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_652
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5022
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move/from16 v0, v35

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_652

    .line 5032
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v35           #plusOne:Z
    :sswitch_67b
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 5033
    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 5034
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_68f
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5035
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreatePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_68f

    .line 5041
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_6a1
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 5042
    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 5043
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_6b5
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5044
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeletePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_6b5

    .line 5050
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_6c7
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_6cd
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5051
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "loc_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onLocationQuery$260d7f24(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_6cd

    .line 5061
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_6ed
    if-eqz p3, :cond_716

    .line 5062
    check-cast p3, Lcom/google/android/apps/plus/api/TacoTruckOperation;

    .end local p3
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/api/TacoTruckOperation;->getUploadPhotoResponse()Lcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;

    move-result-object v40

    .line 5066
    .local v40, response:Lcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;
    :goto_6f5
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_6fb
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5067
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onImageThumbnailUploaded$1c9f65a1(ILcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_6fb

    .line 5064
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v40           #response:Lcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;
    .restart local p3
    :cond_716
    const/16 v40, 0x0

    .restart local v40       #response:Lcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;
    goto :goto_6f5

    .line 5075
    .end local v40           #response:Lcom/google/wireless/tacotruck/proto/Network$UploadPhotoResponse;
    :sswitch_719
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_71f
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5076
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEventHomeRequestComplete$b5e9bbb(I)V

    goto :goto_71f

    .line 5082
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_72f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_735
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_735

    .line 5089
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_73f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_745
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5090
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReadEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_745

    .line 5096
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_757
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_75d
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5097
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetEventInviteesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_75d

    .line 5103
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_76f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_775
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5104
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSharePhotosToEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_775

    .line 5110
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_787
    if-eqz p3, :cond_7f3

    move-object/from16 v31, p3

    .line 5111
    check-cast v31, Lcom/google/android/apps/plus/api/MarkItemReadOperation;

    .line 5113
    .local v31, op:Lcom/google/android/apps/plus/api/MarkItemReadOperation;
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getErrorCode()I

    move-result v24

    .line 5114
    .local v24, errorCode:I
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getException()Ljava/lang/Exception;

    move-result-object v26

    .line 5116
    .local v26, ex:Ljava/lang/Exception;
    const/16 v10, 0xc8

    move/from16 v0, v24

    if-ne v0, v10, :cond_7f3

    if-nez v26, :cond_7f3

    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->isNotificationType()Z

    move-result v10

    if-nez v10, :cond_7f3

    .line 5117
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getItemIds()Ljava/util/List;

    move-result-object v20

    .line 5118
    .local v20, activityIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v20, :cond_7f3

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_7f3

    .line 5119
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_7b3
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7f3

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 5120
    .local v19, activityId:Ljava/lang/String;
    const-string v10, "extra_activity_id"

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v27

    .line 5122
    .local v27, extras:Landroid/os/Bundle;
    const-string v10, "start_view"

    sget-object v45, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/apps/plus/analytics/OzViews;->ordinal()I

    move-result v45

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v44

    .line 5124
    .local v44, viewOrdinal:I
    invoke-static/range {v44 .. v44}, Lcom/google/android/apps/plus/analytics/OzViews;->valueOf(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v43

    .line 5125
    .local v43, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v45, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v23

    move-object/from16 v1, v45

    move-object/from16 v2, v43

    move-object/from16 v3, v27

    invoke-static {v0, v10, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    goto :goto_7b3

    .line 5134
    .end local v19           #activityId:Ljava/lang/String;
    .end local v20           #activityIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v24           #errorCode:I
    .end local v26           #ex:Ljava/lang/Exception;
    .end local v27           #extras:Landroid/os/Bundle;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v31           #op:Lcom/google/android/apps/plus/api/MarkItemReadOperation;
    .end local v43           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    .end local v44           #viewOrdinal:I
    :cond_7f3
    const/16 p2, 0x0

    .line 5135
    goto/16 :goto_37

    .line 5144
    :sswitch_7f7
    const/16 p2, 0x0

    .line 5145
    goto/16 :goto_37

    .line 5149
    :sswitch_7fb
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_801
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5150
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-virtual {v4, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onChangeNotificationsRequestComplete$6a63df5(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_801

    .line 5159
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_81d
    if-eqz p3, :cond_849

    move-object/from16 v31, p3

    .line 5160
    check-cast v31, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;

    .line 5162
    .local v31, op:Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;->getNotificationSettings()Lcom/google/android/apps/plus/content/NotificationSettingsData;

    move-result-object v41

    .line 5166
    .end local v31           #op:Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;
    .local v41, settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    :goto_827
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_82d
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5167
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v41

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetNotificationSettings$434dcfc8(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V

    goto :goto_82d

    .line 5164
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v41           #settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    :cond_849
    const/16 v41, 0x0

    .restart local v41       #settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    goto :goto_827

    .line 5175
    .end local v41           #settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    :sswitch_84c
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_852
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_86b

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5176
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSyncNotifications$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_852

    .line 5181
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    :cond_86b
    const/16 p2, 0x0

    .line 5182
    goto/16 :goto_37

    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_86f
    move-object/from16 v16, p3

    .line 5187
    check-cast v16, Lcom/google/android/apps/plus/content/EsAccount;

    .line 5188
    .restart local v16       #account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v16, :cond_87b

    .line 5189
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 5190
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    .line 5192
    :cond_87b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_881
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5193
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onOobRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_881

    .line 5200
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v16           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_89a
    const-string v10, "media_ref"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v37

    check-cast v37, Lcom/google/android/apps/plus/api/MediaRef;

    .line 5201
    .local v37, ref:Lcom/google/android/apps/plus/api/MediaRef;
    const-string v10, "width"

    const/16 v45, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 5202
    .local v13, width:I
    const-string v10, "height"

    const/16 v45, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    .line 5203
    .local v14, height:I
    const-string v10, "crop_type"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 5204
    .local v15, cropType:I
    check-cast p3, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;

    .end local p3
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 5205
    .local v12, bitmap:Landroid/graphics/Bitmap;
    new-instance v11, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;

    move-object/from16 v0, v37

    invoke-direct {v11, v0, v15}, Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;-><init>(Lcom/google/android/apps/plus/api/MediaRef;I)V

    .line 5206
    .local v11, key:Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;
    if-eqz v12, :cond_8e0

    .line 5207
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sPhotoImageCache:Lcom/google/android/apps/plus/service/PhotoCache;

    invoke-virtual/range {v10 .. v15}, Lcom/google/android/apps/plus/service/PhotoCache;->downloadComplete(Ljava/lang/Object;Landroid/graphics/Bitmap;III)V

    .line 5219
    :cond_8dc
    const/16 p2, 0x0

    .line 5220
    goto/16 :goto_37

    .line 5212
    :cond_8e0
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sPhotoImageCache:Lcom/google/android/apps/plus/service/PhotoCache;

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/service/PhotoCache;->downloadFailed(Ljava/lang/Object;)V

    .line 5213
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_8eb
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8dc

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5214
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const/4 v10, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v4, v0, v10, v15}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPhotoImageLoaded$b81653(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;I)V

    goto :goto_8eb

    .line 5224
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v11           #key:Lcom/google/android/apps/plus/service/EsService$RemoteImageKey;
    .end local v12           #bitmap:Landroid/graphics/Bitmap;
    .end local v13           #width:I
    .end local v14           #height:I
    .end local v15           #cropType:I
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v37           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    .restart local p3
    :sswitch_8fe
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_904
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_91c

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 5225
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_904

    .line 5230
    :cond_91c
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    .line 5233
    const/16 p2, 0x0

    .line 5234
    goto/16 :goto_37

    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_923
    move-object/from16 v39, p3

    .line 5238
    check-cast v39, Ljava/util/List;

    .line 5239
    .local v39, removedAccounts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v39, :cond_989

    .line 5240
    const-string v10, "EsService"

    const/16 v45, 0x3

    move/from16 v0, v45

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_960

    .line 5241
    invoke-interface/range {v39 .. v39}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_939
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_960

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    .line 5242
    .local v38, removedAccount:Ljava/lang/String;
    const-string v10, "EsService"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "OP_ACCOUNTS_CHANGED removed: "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-static {v10, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_939

    .line 5246
    .end local v28           #i$:Ljava/util/Iterator;
    .end local v38           #removedAccount:Ljava/lang/String;
    :cond_960
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v10, :cond_989

    .line 5247
    invoke-interface/range {v39 .. v39}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :cond_968
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_989

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 5248
    .local v16, account:Ljava/lang/String;
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_968

    .line 5249
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 5250
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    .line 5258
    .end local v16           #account:Ljava/lang/String;
    .end local v28           #i$:Ljava/util/Iterator;
    :cond_989
    const/16 p2, 0x0

    .line 5259
    goto/16 :goto_37

    .line 5266
    .end local v39           #removedAccounts:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_98d
    const/16 p2, 0x0

    .line 5267
    goto/16 :goto_37

    .line 5271
    :sswitch_991
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_997
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5272
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_997

    .line 5281
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_9b7
    const-string v10, "analytics_sync"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v30

    .line 5283
    .local v30, needToSyncAnalytics:Z
    if-eqz v30, :cond_9f4

    .line 5284
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v45, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v46, Lcom/google/android/apps/plus/service/EsService;

    move-object/from16 v0, v45

    move-object/from16 v1, v23

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v45

    const-string v46, "op"

    const/16 v47, 0x3f2

    invoke-virtual/range {v45 .. v47}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v46, "acc"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-object/from16 v0, v23

    move-object/from16 v1, v45

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    .line 5289
    :cond_9f4
    const-string v10, "analytics_sync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a05

    .line 5290
    const-string v10, "analytics_sync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 5294
    :cond_a05
    const/16 p2, 0x0

    .line 5295
    goto/16 :goto_37

    .line 5299
    .end local v30           #needToSyncAnalytics:Z
    :sswitch_a09
    if-eqz p3, :cond_abb

    move-object/from16 v31, p3

    .line 5300
    check-cast v31, Lcom/google/android/apps/plus/api/PostClientLogsOperation;

    .line 5302
    .local v31, op:Lcom/google/android/apps/plus/api/PostClientLogsOperation;
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getErrorCode()I

    move-result v24

    .line 5303
    .restart local v24       #errorCode:I
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getException()Ljava/lang/Exception;

    move-result-object v26

    .line 5305
    .restart local v26       #ex:Ljava/lang/Exception;
    const/16 v10, 0xc8

    move/from16 v0, v24

    if-ne v0, v10, :cond_a1f

    if-eqz v26, :cond_ad9

    .line 5306
    :cond_a1f
    const-string v10, "EsService"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "PostClientLogsOperation failed ex "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v45

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " errorCode "

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    move-object/from16 v0, v45

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-static {v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5309
    invoke-virtual/range {v31 .. v31}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getClientOzEvents()Ljava/util/List;

    move-result-object v25

    .line 5310
    .local v25, events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    if-eqz v25, :cond_abb

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_abb

    .line 5311
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v45, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v46, Lcom/google/android/apps/plus/service/EsService;

    move-object/from16 v0, v45

    move-object/from16 v1, v23

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v45

    const-string v46, "op"

    const/16 v47, 0x3f3

    invoke-virtual/range {v45 .. v47}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v46, "acc"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_a7b
    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/plus/content/DbAnalyticsEvents;->serializeClientOzEventList(Ljava/util/List;)[B

    move-result-object v10

    const-string v46, "analytics_events"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    move-object/from16 v0, v23

    move-object/from16 v1, v45

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I
    :try_end_a8f
    .catch Ljava/io/IOException; {:try_start_a7b .. :try_end_a8f} :catch_abf

    .line 5313
    :goto_a8f
    const-string v10, "EsService"

    const/16 v45, 0x3

    move/from16 v0, v45

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_abb

    .line 5314
    const-string v10, "EsService"

    new-instance v45, Ljava/lang/StringBuilder;

    const-string v46, "Insert "

    invoke-direct/range {v45 .. v46}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v46

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v45

    const-string v46, " analytics events back to the database"

    invoke-virtual/range {v45 .. v46}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v45

    invoke-virtual/range {v45 .. v45}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, v45

    invoke-static {v10, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5326
    .end local v24           #errorCode:I
    .end local v25           #events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    .end local v26           #ex:Ljava/lang/Exception;
    .end local v31           #op:Lcom/google/android/apps/plus/api/PostClientLogsOperation;
    :cond_abb
    :goto_abb
    const/16 p2, 0x0

    .line 5327
    goto/16 :goto_37

    .line 5311
    .restart local v24       #errorCode:I
    .restart local v25       #events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    .restart local v26       #ex:Ljava/lang/Exception;
    .restart local v31       #op:Lcom/google/android/apps/plus/api/PostClientLogsOperation;
    :catch_abf
    move-exception v10

    const-string v45, "EsService"

    new-instance v46, Ljava/lang/StringBuilder;

    const-string v47, "insertAnalyticsEvents: Failed to serialize the analytics events. "

    invoke-direct/range {v46 .. v47}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v46

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v45

    invoke-static {v0, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a8f

    .line 5319
    .end local v25           #events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    :cond_ad9
    const-string v10, "EsService"

    const/16 v45, 0x3

    move/from16 v0, v45

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_abb

    .line 5320
    const-string v10, "EsService"

    const-string v45, "PostClientLogsOperation was successful"

    move-object/from16 v0, v45

    invoke-static {v10, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_abb

    .line 5332
    .end local v24           #errorCode:I
    .end local v26           #ex:Ljava/lang/Exception;
    .end local v31           #op:Lcom/google/android/apps/plus/api/PostClientLogsOperation;
    :sswitch_aef
    const/16 p2, 0x0

    .line 5333
    goto/16 :goto_37

    .line 5337
    :sswitch_af3
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_af9
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5338
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetCircleMemebershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_af9

    .line 5345
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_b12
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_b18
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5346
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onRemovePeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_b18

    .line 5353
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_b31
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_b37
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5354
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAddPeopleToCirclesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_b37

    .line 5361
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_b50
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_b56
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5362
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "muted"

    const/16 v45, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v10, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetMutedRequestComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_b56

    .line 5370
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_b7b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_b81
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5371
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetBlockedRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_b81

    .line 5378
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_b9a
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_ba0
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5379
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportAbuseRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_ba0

    .line 5386
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_bb9
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_bbf
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5387
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateCircleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_bbf

    .line 5394
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_bd8
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_bde
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5395
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onModifyCirclePropertiesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_bde

    .line 5402
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_bf7
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_bfd
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5403
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteCirclesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_bfd

    .line 5410
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_c16
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_c1c
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5411
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDismissSuggestedPeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_c1c

    .line 5419
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_c35
    const/16 p2, 0x0

    .line 5420
    goto/16 :goto_37

    .line 5424
    :sswitch_c39
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v18

    .line 5425
    .local v18, activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v18, :cond_c56

    if-eqz p3, :cond_c56

    .line 5429
    check-cast p3, Ljava/lang/Boolean;

    .end local p3
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_c5a

    sget-object v10, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_c4b
    const/16 v45, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v45

    invoke-static {v0, v1, v10, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 5435
    :cond_c56
    const/16 p2, 0x0

    .line 5436
    goto/16 :goto_37

    .line 5429
    :cond_c5a
    sget-object v10, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_c4b

    .line 5440
    .end local v18           #activeAccount:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local p3
    :sswitch_c5d
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_c63
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5441
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCircleSyncComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_c63

    .line 5450
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_c7c
    const/16 p2, 0x0

    .line 5451
    goto/16 :goto_37

    .line 5457
    :sswitch_c80
    const/16 p2, 0x0

    .line 5458
    goto/16 :goto_37

    .line 5464
    :sswitch_c84
    const/16 p2, 0x0

    .line 5465
    goto/16 :goto_37

    .line 5469
    :sswitch_c88
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_c8e
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5470
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetProfilePhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_c8e

    .line 5477
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_ca7
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_cad
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5478
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_cad

    .line 5485
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_cc6
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_ccc
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5486
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_ccc

    .line 5493
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_ce5
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_ceb
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5494
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSearchActivitiesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_ceb

    .line 5501
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_d04
    const/16 p2, 0x0

    .line 5502
    goto/16 :goto_37

    .line 5506
    :sswitch_d08
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_d0e
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5507
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_d0e

    .line 5515
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_d27
    const/16 p2, 0x0

    .line 5516
    goto/16 :goto_37

    .line 5520
    :sswitch_d2b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_d31
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_d43

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5521
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPostActivityResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_d31

    .line 5524
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    :cond_d43
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-nez v10, :cond_d70

    sget-object v17, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 5526
    .local v17, action:Lcom/google/android/apps/plus/analytics/OzActions;
    :goto_d4b
    const-string v10, "analytics"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    .line 5528
    .local v21, analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/plus/content/EsAccount;

    .line 5529
    .local v16, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v21, :cond_37

    if-eqz v17, :cond_37

    .line 5530
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto/16 :goto_37

    .line 5524
    .end local v16           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v17           #action:Lcom/google/android/apps/plus/analytics/OzActions;
    .end local v21           #analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    :cond_d70
    sget-object v17, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_d4b

    .line 5536
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_d73
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_d79
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_d8b

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5537
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPlusOneApplyResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_d79

    .line 5540
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    :cond_d8b
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-nez v10, :cond_db8

    sget-object v17, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 5542
    .restart local v17       #action:Lcom/google/android/apps/plus/analytics/OzActions;
    :goto_d93
    const-string v10, "analytics"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    .line 5544
    .restart local v21       #analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/plus/content/EsAccount;

    .line 5545
    .restart local v16       #account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v21, :cond_37

    if-eqz v17, :cond_37

    .line 5546
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    move-object/from16 v2, v21

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto/16 :goto_37

    .line 5540
    .end local v16           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v17           #action:Lcom/google/android/apps/plus/analytics/OzActions;
    .end local v21           #analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    :cond_db8
    sget-object v17, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_d93

    .line 5552
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_dbb
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_dc1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5553
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_dc1

    .line 5559
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_dd3
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_dd9
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5560
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_dd9

    .line 5566
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_deb
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_df1
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5567
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onUpdateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_df1

    .line 5573
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_e03
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_e09
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5574
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e09

    .line 5580
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_e1b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_e21
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_e21

    .line 5587
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_e2b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_e31
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5588
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEventInviteComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e31

    .line 5594
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_e43
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_e49
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5595
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEventManageGuestComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e49

    .line 5617
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_e5b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_e61
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5618
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onWriteReviewComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e61

    .line 5624
    .end local v4           #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    .end local v28           #i$:Ljava/util/Iterator;
    :sswitch_e73
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v28

    .restart local v28       #i$:Ljava/util/Iterator;
    :goto_e79
    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_37

    invoke-interface/range {v28 .. v28}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 5625
    .restart local v4       #listener:Lcom/google/android/apps/plus/service/EsServiceListener;
    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteReviewComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e79

    .line 4641
    nop

    :sswitch_data_e8c
    .sparse-switch
        0x1 -> :sswitch_b4
        0x3 -> :sswitch_12c
        0x4 -> :sswitch_923
        0x5 -> :sswitch_98d
        0x6 -> :sswitch_14c
        0x7 -> :sswitch_fd
        0xb -> :sswitch_1cd
        0xc -> :sswitch_1f4
        0xe -> :sswitch_227
        0x10 -> :sswitch_67b
        0x11 -> :sswitch_6a1
        0x12 -> :sswitch_299
        0x13 -> :sswitch_2bf
        0x14 -> :sswitch_273
        0x15 -> :sswitch_24d
        0x16 -> :sswitch_172
        0x17 -> :sswitch_172
        0x19 -> :sswitch_787
        0x1e -> :sswitch_563
        0x1f -> :sswitch_589
        0x20 -> :sswitch_5a8
        0x21 -> :sswitch_5d5
        0x22 -> :sswitch_602
        0x23 -> :sswitch_640
        0x29 -> :sswitch_6c7
        0x32 -> :sswitch_2e5
        0x33 -> :sswitch_302
        0x34 -> :sswitch_321
        0x35 -> :sswitch_340
        0x36 -> :sswitch_37e
        0x37 -> :sswitch_39d
        0x38 -> :sswitch_3d4
        0x39 -> :sswitch_401
        0x3a -> :sswitch_420
        0x3c -> :sswitch_44d
        0x3d -> :sswitch_46a
        0x3e -> :sswitch_4ba
        0x3f -> :sswitch_53f
        0x40 -> :sswitch_35f
        0x41 -> :sswitch_4e5
        0x42 -> :sswitch_503
        0x43 -> :sswitch_490
        0xc9 -> :sswitch_7f7
        0xca -> :sswitch_84c
        0xcb -> :sswitch_7f7
        0xcc -> :sswitch_7fb
        0xcd -> :sswitch_81d
        0xce -> :sswitch_7f7
        0x1f4 -> :sswitch_c5d
        0x1f6 -> :sswitch_c7c
        0x1f7 -> :sswitch_c84
        0x1f8 -> :sswitch_c80
        0x258 -> :sswitch_86f
        0x2be -> :sswitch_af3
        0x2bf -> :sswitch_991
        0x2c1 -> :sswitch_b50
        0x2c2 -> :sswitch_b7b
        0x2c3 -> :sswitch_b9a
        0x2c4 -> :sswitch_bb9
        0x2c5 -> :sswitch_bf7
        0x2c6 -> :sswitch_b12
        0x2c7 -> :sswitch_c16
        0x2c8 -> :sswitch_b31
        0x2c9 -> :sswitch_c88
        0x2ca -> :sswitch_ca7
        0x2cb -> :sswitch_cc6
        0x2cc -> :sswitch_bd8
        0x320 -> :sswitch_6ed
        0x384 -> :sswitch_719
        0x385 -> :sswitch_72f
        0x386 -> :sswitch_dbb
        0x387 -> :sswitch_dd3
        0x388 -> :sswitch_deb
        0x389 -> :sswitch_e1b
        0x38a -> :sswitch_e2b
        0x38b -> :sswitch_e03
        0x38d -> :sswitch_73f
        0x38e -> :sswitch_757
        0x38f -> :sswitch_76f
        0x3ea -> :sswitch_c39
        0x3ec -> :sswitch_8fe
        0x3ed -> :sswitch_c35
        0x3ef -> :sswitch_89a
        0x3f0 -> :sswitch_9b7
        0x3f1 -> :sswitch_e43
        0x3f2 -> :sswitch_a09
        0x3f3 -> :sswitch_aef
        0x44c -> :sswitch_ce5
        0x456 -> :sswitch_d04
        0x460 -> :sswitch_d08
        0x7d0 -> :sswitch_d27
        0x7d1 -> :sswitch_d27
        0x898 -> :sswitch_d2b
        0x899 -> :sswitch_d73
        0x8fc -> :sswitch_37
        0x8fd -> :sswitch_37
        0x960 -> :sswitch_37
        0x9c4 -> :sswitch_e5b
        0x9c5 -> :sswitch_e73
        0xa28 -> :sswitch_37
    .end sparse-switch
.end method

.method public final onServiceThreadEnd()V
    .registers 1

    .prologue
    .line 4622
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    .prologue
    .line 3106
    if-eqz p1, :cond_7

    .line 3107
    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/ServiceThread;->put(Landroid/content/Intent;)V

    .line 3110
    :cond_7
    const/4 v0, 0x2

    return v0
.end method

.method public final processIntent(Landroid/content/Intent;)V
    .registers 21
    .parameter "intent"

    .prologue
    .line 3139
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/service/EsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 3140
    .local v4, context:Landroid/content/Context;
    const-string v3, "op"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 3141
    .local v17, opCode:I
    const-string v3, "rid"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 3142
    .local v18, requestId:I
    const-string v3, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/EsAccount;

    .line 3146
    .local v5, account:Lcom/google/android/apps/plus/content/EsAccount;
    sparse-switch v17, :sswitch_data_a7c

    const/4 v3, 0x0

    :goto_24
    if-eqz v3, :cond_a59

    .line 3157
    :cond_26
    :goto_26
    return-void

    .line 3146
    :sswitch_27
    :try_start_27
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/service/EsService;->fetchC2dmId$faab209()Z

    move-result v3

    if-nez v3, :cond_45

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getStickyC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_45

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v6, 0x0

    const-string v7, "Failed to get C2DM registration."

    const/4 v8, 0x0

    invoke-direct {v3, v6, v7, v8}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v5}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    :goto_43
    const/4 v3, 0x1

    goto :goto_24

    :cond_45
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "webupdates"

    invoke-static {v4, v3, v6}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v6

    if-nez v6, :cond_d2

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasPlusPages()Z

    move-result v6

    if-eqz v6, :cond_c2

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v6

    move/from16 v0, v18

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->putAccountSettingsResponse(ILcom/google/android/apps/plus/content/AccountSettingsData;)V

    move-object v8, v5

    :goto_6f
    if-eqz v8, :cond_ee

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v6

    if-eqz v6, :cond_e0

    const-string v3, "oob_origin"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v9, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v9}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    iput-object v3, v9, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    new-instance v6, Lcom/google/android/apps/plus/api/OutOfBoxOperation;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v7, v4

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->start()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v3

    move/from16 v0, v18

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/service/EsService;->putOutOfBoxResponse(ILcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3, v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v8}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_aa
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_aa} :catch_ab

    goto :goto_43

    .line 3153
    :catch_ab
    move-exception v16

    .line 3154
    .local v16, ex:Ljava/lang/Exception;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    .line 3155
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v16

    invoke-direct {v3, v6, v7, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_26

    .line 3146
    .end local v16           #ex:Ljava/lang/Exception;
    :cond_c2
    :try_start_c2
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->getAccountByName(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_6f

    :cond_d2
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v6

    if-eqz v6, :cond_de

    move-object v8, v5

    goto :goto_6f

    :cond_de
    const/4 v8, 0x0

    goto :goto_6f

    :cond_e0
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v8}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_ee
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_fd
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "display_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "photo_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v3, "is_plus_page"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    const-string v3, "account_settings"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/AccountSettingsData;

    if-eqz v15, :cond_17b

    new-instance v6, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, -0x1

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/plus/content/EsAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    new-instance v7, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/4 v10, 0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v8, v4

    move-object v9, v6

    invoke-direct/range {v7 .. v12}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v3

    if-nez v3, :cond_14f

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v3

    if-eqz v3, :cond_15e

    :cond_14f
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3, v7}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_15e
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->getAccountByName(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v7}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_17b
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AccountSettingsData;->isChild()Z

    move-result v14

    move-object v10, v4

    move-object v11, v8

    move-object v13, v9

    invoke-static/range {v10 .. v15}, Lcom/google/android/apps/plus/content/EsAccountsData;->insertAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-static {v4, v7, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    invoke-static {v4, v7, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveServerSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v7}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_1a5
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-static {v4, v3, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->deactivateAccount(Landroid/content/Context;Ljava/lang/String;Z)V

    const/4 v3, 0x0

    invoke-static {v4, v3}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_1c0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/service/EsService;->fetchC2dmId$faab209()Z

    move-result v3

    if-nez v3, :cond_1df

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getStickyC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1df

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v6, 0x0

    const-string v7, "Failed to get C2DM registration."

    const/4 v8, 0x0

    invoke-direct {v3, v6, v7, v8}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_1df
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_1f1
    const-string v3, "loc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/DbLocation;

    const-string v3, "cont_token"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$7;

    move-object/from16 v7, p0

    move-object v8, v4

    move-object v9, v5

    move-object/from16 v12, p1

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/plus/service/EsService$7;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_218
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v3, "circle_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v3, "cont_token"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v3, "view"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    const-string v3, "from_widget"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v15

    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$8;

    move-object/from16 v7, p0

    move-object v8, v4

    move-object v9, v5

    move-object/from16 v14, p1

    invoke-direct/range {v6 .. v15}, Lcom/google/android/apps/plus/service/EsService$8;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Z)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_257
    new-instance v3, Lcom/google/android/apps/plus/api/GetActivityOperation;

    const-string v6, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/GetActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_270
    new-instance v3, Lcom/google/android/apps/plus/api/GetAudienceOperation;

    const-string v6, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/GetAudienceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetAudienceOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_288
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v3, Lcom/google/android/apps/plus/api/EditActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/EditActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/EditActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_2a8
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "audience"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v3, Lcom/google/android/apps/plus/api/ReshareActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/ReshareActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_2d2
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v9, 0x1

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    new-instance v6, Ljava/lang/Thread;

    new-instance v7, Lcom/google/android/apps/plus/service/EsService$6;

    invoke-direct {v7, v4, v5}, Lcom/google/android/apps/plus/service/EsService$6;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {v6, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_2f8
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v9, 0x0

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_311
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/DeleteActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/DeleteActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/DeleteActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_329
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "mute_state"

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    new-instance v3, Lcom/google/android/apps/plus/api/MuteActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MuteActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MuteActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_34a
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const-string v9, "SPAM"

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_364
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/MarkItemReadOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_381
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_399
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v3, "album_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_3b9
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v3, "stream_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v8, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_3d9
    const-string v3, "picasa_photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;

    const-string v6, "text"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;->getPhotoId()J

    move-result-wide v6

    invoke-virtual {v3}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;->getFocusObfuscatedOwnerId()Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v10, p1

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_403
    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/4 v8, 0x0

    const/16 v3, 0x23

    invoke-virtual {v9, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-lez v3, :cond_421

    const/4 v6, 0x0

    invoke-virtual {v9, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    :cond_421
    new-instance v3, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_431
    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/DeleteCommentOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/DeleteCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_449
    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "delete"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v3, "is_undo"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    new-instance v3, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v8, 0x0

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_474
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "album_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "photo_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v9, "plus_oned"

    const/4 v10, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    if-nez v3, :cond_4ab

    const-wide/16 v9, 0x0

    :goto_49b
    new-instance v3, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v12, p1

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;JZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :cond_4ab
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    goto :goto_49b

    :sswitch_4b0
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_4c8
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "photo_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v3, "shape_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v3, "approved"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    new-instance v3, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v12, p1

    invoke-direct/range {v3 .. v13}, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;JZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_4fd
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v10

    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$9;

    move-object/from16 v7, p0

    move-object v8, v4

    move-object v9, v5

    move-object/from16 v11, p1

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/plus/service/EsService$9;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_516
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v3, "photo_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    new-instance v3, Lcom/google/android/apps/plus/api/DeletePhotosOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/DeletePhotosOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_537
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v3, "photo_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    new-instance v3, Lcom/google/android/apps/plus/api/GetPhotoOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/GetPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;JLjava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_559
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_568
    const-string v3, "url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v3, Lcom/google/android/apps/plus/api/SavePhotoOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/SavePhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_580
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "photo_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    new-instance v3, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v9, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_5a2
    const-string v3, "media_refs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v6, "content_uri IN ("

    invoke-direct {v9, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5c1
    :goto_5c1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5e7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v10

    if-eqz v10, :cond_5c1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v3, "?,"

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5c1

    :cond_5e7
    const/4 v6, 0x1

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_62c

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v10, v3, -0x1

    const-string v11, ")"

    invoke-virtual {v9, v10, v3, v11}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v10, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->uploadsUri:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v11, v10, v9, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_616
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_62c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-static {v8, v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->deleteLocalFileAndMediaStore(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    and-int/2addr v6, v3

    goto :goto_616

    :cond_62c
    move v3, v6

    if-eqz v3, :cond_63e

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    :goto_634
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_63e
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v3, v6, v7, v8}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_634

    :sswitch_647
    const-string v3, "data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v10

    new-instance v3, Lcom/google/android/apps/plus/api/UploadMediaOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v8

    const-string v9, "profile"

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_665
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v9, 0x1

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_67e
    const-string v3, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v9, 0x0

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_697
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v3, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_6b7
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v3, "event_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v3, Lcom/google/android/apps/plus/api/PostEventCommentOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/PostEventCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_6df
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v3, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_707
    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v3, Lcom/google/android/apps/plus/api/DeleteCommentOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/DeleteCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_71f
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "delete"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v3, "is_undo"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    new-instance v3, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_751
    const-string v3, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "plus_oned"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    new-instance v3, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_77a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    if-eqz v3, :cond_791

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->isAborted()Z

    move-result v3

    if-nez v3, :cond_791

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->abort()V

    :cond_791
    const-string v3, "loc_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/api/LocationQuery;

    new-instance v3, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v9, 0x1

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/LocationQuery;Z)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_7b4
    const-string v3, "media_ref"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    move-object v6, v0

    const-string v3, "width"

    const/16 v7, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v3, "height"

    const/16 v8, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string v3, "crop_type"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    new-instance v3, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v10, p1

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/api/DownloadPicasaPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;IIILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_7f1
    const-string v3, "notif_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/content/EsNotificationData;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/plus/api/MarkItemReadOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x1

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_817
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsNotificationData;->markAllNotificationsAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsNotificationData;->getLatestNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D

    move-result-wide v8

    const-wide/16 v6, 0x0

    cmpl-double v3, v8, v6

    if-lez v3, :cond_837

    new-instance v3, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;D)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;->startThreaded()V

    goto/16 :goto_43

    :cond_837
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_846
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsNotificationData;->markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_85b
    const-string v3, "notification_settings"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/content/NotificationSettingsData;

    new-instance v3, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v7, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_875
    new-instance v3, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    invoke-direct {v3, v4, v5, v0, v6}, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_885
    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$10;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v6, v0, v4, v5, v1}, Lcom/google/android/apps/plus/service/EsService$10;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_898
    const-string v3, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->getRequest()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/api/OutOfBoxOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->start()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v6

    move/from16 v0, v18

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->putOutOfBoxResponse(ILcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    if-eqz v6, :cond_934

    iget-object v7, v6, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    if-eqz v7, :cond_934

    iget-object v6, v6, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_934

    const-string v3, "EsService"

    const/4 v6, 0x3

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8d7

    const-string v3, "EsService"

    const-string v6, "Get account info after signup"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8d7
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "webupdates"

    invoke-static {v4, v3, v6}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v6

    if-nez v6, :cond_8f7

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    if-eqz v6, :cond_906

    :cond_8f7
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_906
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasPlusPages()Z

    move-result v6

    if-eqz v6, :cond_924

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v6

    move/from16 v0, v18

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->putAccountSettingsResponse(ILcom/google/android/apps/plus/content/AccountSettingsData;)V

    move-object v6, v5

    :goto_916
    new-instance v7, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v7, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :cond_924
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->getAccountByName(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_916

    :cond_934
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v3}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_43

    :sswitch_943
    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$11;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v6, v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService$11;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_956
    const-string v3, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "muted"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    new-instance v3, Lcom/google/android/apps/plus/api/MuteUserOperation;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MuteUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MuteUserOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_977
    const-string v3, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v3, "person_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v3, "blocked"

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    new-instance v3, Lcom/google/android/apps/plus/api/SetBlockedOperation;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v9, p1

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/api/SetBlockedOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SetBlockedOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_9a0
    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$12;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v6, v0, v4, v5, v1}, Lcom/google/android/apps/plus/service/EsService$12;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_9b3
    const-string v3, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v3, "person_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v3, "circles_to_add"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    const-string v3, "circles_to_remove"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const-string v3, "fire_and_forget"

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    new-instance v3, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v11, p1

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->startThreaded()V

    goto/16 :goto_43

    :sswitch_9ec
    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$13;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v6, v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/EsService$13;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_9ff
    new-instance v3, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$14;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v6, v0, v4, v5, v1}, Lcom/google/android/apps/plus/service/EsService$14;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v3, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_43

    :sswitch_a12
    const-string v3, "media_ref"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/api/MediaRef;

    const-string v3, "album_title"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v3, "album_label"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v3, "stream_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_a3a
    array-length v7, v6

    if-ge v3, v7, :cond_a45

    aget-object v7, v6, v3

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_a3a

    :cond_a45
    new-instance v6, Lcom/google/android/apps/plus/api/TacoTruckOperation;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    invoke-direct {v6, v4, v5, v0, v3}, Lcom/google/android/apps/plus/api/TacoTruckOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    move-object v7, v4

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/apps/plus/api/TacoTruckOperation;->uploadPhotoThumbnail(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/api/MediaRef;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/TacoTruckOperation;->startThreaded()V

    goto/16 :goto_43

    .line 3149
    :cond_a59
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    invoke-direct {v0, v4, v5, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z

    move-result v3

    if-nez v3, :cond_26

    .line 3152
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unsupported op code: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_a7c
    .catch Ljava/lang/Exception; {:try_start_c2 .. :try_end_a7c} :catch_ab

    .line 3146
    :sswitch_data_a7c
    .sparse-switch
        0x1 -> :sswitch_27
        0x3 -> :sswitch_1a5
        0x6 -> :sswitch_1c0
        0x7 -> :sswitch_fd
        0xb -> :sswitch_257
        0xc -> :sswitch_270
        0xe -> :sswitch_288
        0x10 -> :sswitch_2d2
        0x11 -> :sswitch_2f8
        0x12 -> :sswitch_329
        0x13 -> :sswitch_34a
        0x14 -> :sswitch_311
        0x15 -> :sswitch_2a8
        0x16 -> :sswitch_1f1
        0x17 -> :sswitch_218
        0x19 -> :sswitch_364
        0x1e -> :sswitch_697
        0x1f -> :sswitch_6b7
        0x20 -> :sswitch_6df
        0x21 -> :sswitch_707
        0x22 -> :sswitch_71f
        0x23 -> :sswitch_751
        0x29 -> :sswitch_77a
        0x32 -> :sswitch_381
        0x33 -> :sswitch_399
        0x34 -> :sswitch_3b9
        0x35 -> :sswitch_3d9
        0x36 -> :sswitch_431
        0x37 -> :sswitch_449
        0x38 -> :sswitch_474
        0x39 -> :sswitch_4b0
        0x3a -> :sswitch_4c8
        0x3c -> :sswitch_4fd
        0x3d -> :sswitch_516
        0x3e -> :sswitch_537
        0x3f -> :sswitch_5a2
        0x40 -> :sswitch_403
        0x41 -> :sswitch_559
        0x42 -> :sswitch_568
        0x43 -> :sswitch_580
        0xc9 -> :sswitch_7f1
        0xca -> :sswitch_885
        0xcb -> :sswitch_817
        0xcc -> :sswitch_85b
        0xcd -> :sswitch_875
        0xce -> :sswitch_846
        0x258 -> :sswitch_898
        0x2be -> :sswitch_9b3
        0x2bf -> :sswitch_943
        0x2c1 -> :sswitch_956
        0x2c2 -> :sswitch_977
        0x2c3 -> :sswitch_9a0
        0x2c6 -> :sswitch_9ec
        0x2c8 -> :sswitch_9ff
        0x2c9 -> :sswitch_647
        0x2ca -> :sswitch_665
        0x2cb -> :sswitch_67e
        0x320 -> :sswitch_a12
        0x3ef -> :sswitch_7b4
    .end sparse-switch
.end method
