.class public Lcom/google/android/apps/plus/api/MediaRef;
.super Ljava/lang/Object;
.source "MediaRef.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDisplayName:Ljava/lang/String;

.field private final mLocalUri:Landroid/net/Uri;

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private final mPhotoId:J

.field private final mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 128
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/api/MediaRef$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/api/MediaRef;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "in"

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, uriString:Ljava/lang/String;
    if-eqz v0, :cond_32

    .line 92
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    .line 96
    :goto_21
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->valueOf(I)Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    .line 98
    return-void

    .line 94
    :cond_32
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    goto :goto_21
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .registers 15
    .parameter "ownerGaiaId"
    .parameter "photoId"
    .parameter "url"
    .parameter "localUri"
    .parameter "type"

    .prologue
    .line 68
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 69
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V
    .registers 9
    .parameter "ownerGaiaId"
    .parameter "photoId"
    .parameter "url"
    .parameter "localUri"
    .parameter "displayName"
    .parameter "type"

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    .line 74
    iput-wide p2, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    .line 75
    iput-object p4, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    .line 76
    iput-object p5, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    .line 78
    iput-object p7, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    .line 79
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 9
    .parameter "o"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 314
    instance-of v3, p1, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v3, :cond_11

    move-object v0, p1

    .line 315
    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    .line 320
    .local v0, other:Lcom/google/android/apps/plus/api/MediaRef;
    iget-wide v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    iget-wide v5, v0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    cmp-long v3, v3, v5

    if-eqz v3, :cond_12

    .line 332
    .end local v0           #other:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_11
    :goto_11
    return v1

    .line 322
    .restart local v0       #other:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_12
    iget-object v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 324
    iget-object v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v3, :cond_32

    if-eqz v4, :cond_32

    invoke-virtual {v3, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    :goto_28
    if-eqz v3, :cond_11

    .line 326
    iget-object v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iget-object v4, v0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v3, v4, :cond_11

    move v1, v2

    .line 329
    goto :goto_11

    .line 324
    :cond_32
    if-nez v3, :cond_38

    if-nez v4, :cond_38

    move v3, v2

    goto :goto_28

    :cond_38
    move v3, v1

    goto :goto_28
.end method

.method public final getLocalUri()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getOwnerGaiaId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final getPhotoId()J
    .registers 3

    .prologue
    .line 257
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    return-wide v0
.end method

.method public final getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;
    .registers 2

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final hasLocalUri()Z
    .registers 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final hasPhotoId()Z
    .registers 5

    .prologue
    .line 306
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasUrl()Z
    .registers 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public hashCode()I
    .registers 7

    .prologue
    .line 359
    iget-wide v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    iget-wide v3, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v0, v1

    .line 361
    .local v0, result:I
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_14

    .line 362
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 365
    :cond_14
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 366
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 369
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v1, :cond_2a

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    .line 372
    :cond_2a
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 380
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    if-nez v0, :cond_58

    const-string v0, ""

    :goto_19
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [g-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", p-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_58
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    goto :goto_19
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "out"
    .parameter "flags"

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mOwnerGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mPhotoId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    if-eqz v0, :cond_2b

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mLocalUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    :goto_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MediaRef;->mType:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    return-void

    .line 111
    :cond_2b
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1c
.end method
