.class final Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;
.super Ljava/lang/Object;
.source "EsSyncAdapterService.java"

# interfaces
.implements Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SyncListener"
.end annotation


# instance fields
.field private final mSyncResult:Landroid/content/SyncResult;


# direct methods
.method public constructor <init>(Landroid/content/SyncResult;)V
    .registers 2
    .parameter "syncResult"

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;->mSyncResult:Landroid/content/SyncResult;

    .line 285
    return-void
.end method


# virtual methods
.method public final onOperationComplete(Lcom/google/android/apps/plus/network/HttpOperation;)V
    .registers 10
    .parameter "op"

    .prologue
    const/16 v7, 0x2f

    const-wide/16 v5, 0x1

    .line 292
    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v0

    .line 293
    .local v0, errorCode:I
    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    .line 295
    .local v1, ex:Ljava/lang/Exception;
    const-string v2, "EsSyncAdapterService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 296
    const-string v2, "EsSyncAdapterService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sync operation complete: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_3d
    if-eqz v1, :cond_69

    .line 301
    instance-of v2, v1, Landroid/accounts/AuthenticatorException;

    if-eqz v2, :cond_4d

    .line 302
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 314
    :cond_4c
    :goto_4c
    return-void

    .line 303
    :cond_4d
    instance-of v2, v1, Landroid/accounts/OperationCanceledException;

    if-nez v2, :cond_4c

    .line 304
    instance-of v2, v1, Ljava/io/IOException;

    if-eqz v2, :cond_5f

    .line 305
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_4c

    .line 307
    :cond_5f
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_4c

    .line 309
    :cond_69
    const/16 v2, 0x191

    if-ne v0, v2, :cond_77

    .line 310
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_4c

    .line 311
    :cond_77
    invoke-virtual {p1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 312
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;->mSyncResult:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_4c
.end method
