.class public final Lcom/google/android/apps/plus/analytics/EsAnalytics;
.super Ljava/lang/Object;
.source "EsAnalytics.java"


# direct methods
.method private static getDisplayTextForExtras(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 6
    .parameter "extras"

    .prologue
    .line 99
    if-nez p0, :cond_5

    .line 100
    const-string v3, "none"

    .line 107
    :goto_4
    return-object v3

    .line 103
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_45

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 105
    .local v1, key:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_12

    .line 107
    .end local v1           #key:Ljava/lang/String;
    :cond_45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_4
.end method

.method public static postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 5
    .parameter "inputContext"
    .parameter "account"
    .parameter "analytics"
    .parameter "action"

    .prologue
    .line 258
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 259
    return-void
.end method

.method public static postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    .registers 12
    .parameter "inputContext"
    .parameter "account"
    .parameter "analytics"
    .parameter "action"
    .parameter "extras"

    .prologue
    .line 239
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 240
    .local v1, context:Landroid/content/Context;
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 250
    return-void
.end method

.method public static recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "action"
    .parameter "startView"

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    .line 120
    return-void
.end method

.method public static recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;J)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "action"
    .parameter "startView"
    .parameter "startTime"

    .prologue
    const/4 v3, 0x0

    .line 146
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v4, p4

    move-object v8, v3

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)V

    .line 147
    return-void
.end method

.method private static recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)V
    .registers 20
    .parameter "context"
    .parameter "account"
    .parameter "action"
    .parameter "startView"
    .parameter "startTime"
    .parameter "endTime"
    .parameter "extras"

    .prologue
    .line 176
    const/4 v4, 0x0

    move-object v2, p2

    move-object v3, p3

    move-wide v5, p4

    move-wide/from16 v7, p6

    move-object/from16 v9, p8

    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOzEvent(Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientOzEvent;

    move-result-object v10

    .line 178
    .local v10, event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    const-string v2, "EsAnalytics"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_60

    .line 179
    const-string v2, "EsAnalytics"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "recordActionEvent action: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/apps/plus/analytics/OzActions;->getName(Lcom/google/android/apps/plus/analytics/OzActions;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " startView: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p3}, Lcom/google/android/apps/plus/analytics/OzViews;->getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " startTime: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " endTime: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " extras: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->getDisplayTextForExtras(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_60
    invoke-static {p0, p1, v10}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V

    .line 186
    return-void
.end method

.method public static recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "action"
    .parameter "startView"
    .parameter "extras"

    .prologue
    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .local v4, now:J
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v6, v4

    move-object v8, p4

    .line 133
    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)V

    .line 134
    return-void
.end method

.method public static recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "info"
    .parameter "action"

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)J
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "info"
    .parameter "action"
    .parameter "extras"

    .prologue
    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 83
    .local v5, endTime:J
    const-string v0, "EsAnalytics"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 84
    const-string v0, "EsAnalytics"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recordEvent action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p3}, Lcom/google/android/apps/plus/analytics/OzActions;->getName(Lcom/google/android/apps/plus/analytics/OzActions;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->getStartView()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/analytics/OzViews;->getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->getEndView()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/analytics/OzViews;->getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startTime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->getStartTimeMsec()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " endTime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " extras: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->getDisplayTextForExtras(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_6f
    invoke-virtual {p2}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->getStartView()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->getEndView()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;->getStartTimeMsec()J

    move-result-wide v3

    move-object v0, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOzEvent(Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientOzEvent;

    move-result-object v8

    .line 94
    .local v8, event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    invoke-static {p0, p1, v8}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V

    .line 95
    return-wide v5
.end method

.method private static recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "event"

    .prologue
    .line 41
    if-eqz p1, :cond_1c

    if-eqz p2, :cond_1c

    .line 42
    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V

    .line 44
    const-string v0, "EsAnalytics"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 45
    if-eqz p2, :cond_1c

    iget-object v0, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    if-eqz v0, :cond_1c

    iget-object v0, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    if-nez v0, :cond_1d

    .line 48
    :cond_1c
    :goto_1c
    return-void

    .line 45
    :cond_1d
    const-string v0, "EsAnalytics"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "logAction clientTimeMsec: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->clientTimeMsec:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " totalTimeMs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    if-eqz v0, :cond_6d

    const-string v1, "EsAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "> startView namespace: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " typeNum: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6d
    iget-object v0, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    if-eqz v0, :cond_97

    const-string v1, "EsAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "> endView namespace: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " typeNum: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_97
    iget-object v0, p2, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    if-eqz v0, :cond_1c

    const-string v1, "EsAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "> action namespace: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " typeNum: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1c
.end method

.method public static recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .registers 21
    .parameter "context"
    .parameter "account"
    .parameter "startView"
    .parameter "endView"
    .parameter "startTime"
    .parameter "endTime"
    .parameter "startViewExtras"
    .parameter "endViewExtras"

    .prologue
    .line 205
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 206
    .local v11, now:J
    if-nez p4, :cond_a

    .line 207
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    .line 209
    :cond_a
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p5

    .line 213
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 214
    .local v10, extras:Landroid/os/Bundle;
    if-eqz p6, :cond_22

    invoke-virtual/range {p6 .. p6}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_22

    .line 215
    const-string v1, "extra_start_view_extras"

    move-object/from16 v0, p6

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 218
    :cond_22
    if-eqz p7, :cond_31

    invoke-virtual/range {p7 .. p7}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_31

    .line 219
    const-string v1, "extra_end_view_extras"

    move-object/from16 v0, p7

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 222
    :cond_31
    const/4 v1, 0x0

    invoke-virtual/range {p4 .. p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual/range {p5 .. p5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v10}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_93

    const/4 v8, 0x0

    :goto_41
    move-object v2, p2

    move-object/from16 v3, p3

    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createClientOzEvent(Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientOzEvent;

    move-result-object v9

    .line 224
    .local v9, event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    const-string v1, "EsAnalytics"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8f

    .line 225
    const-string v1, "EsAnalytics"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "recordNavigationEvent startView: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/google/android/apps/plus/analytics/OzViews;->getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " endView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/analytics/OzViews;->getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " startTime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " endTime: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_8f
    invoke-static {p0, p1, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V

    .line 231
    return-void

    .end local v9           #event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    :cond_93
    move-object v8, v10

    .line 222
    goto :goto_41
.end method
