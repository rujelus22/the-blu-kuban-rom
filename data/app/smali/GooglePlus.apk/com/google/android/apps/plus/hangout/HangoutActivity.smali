.class public Lcom/google/android/apps/plus/hangout/HangoutActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "HangoutActivity.java"

# interfaces
.implements Landroid/text/Html$ImageGetter;
.implements Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;,
        Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;,
        Lcom/google/android/apps/plus/hangout/HangoutActivity$HangoutParticipantPresenceListener;
    }
.end annotation


# instance fields
.field private hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private final hangoutParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private mShakeDetectorWasRunning:Z

.field private mSkipGreenRoom:Z

.field private mSkipMinorWarning:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 89
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mSkipGreenRoom:Z

    .line 90
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutActivity$HangoutParticipantPresenceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutActivity$HangoutParticipantPresenceListener;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    .line 188
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/HangoutActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->displayParticipantsInTray()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/hangout/HangoutActivity;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mSkipMinorWarning:Z

    return v0
.end method

.method private canTransfer()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 642
    :try_start_1
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :try_end_c
    .catch Ljava/lang/LinkageError; {:try_start_1 .. :try_end_c} :catch_1b

    move-result-object v0

    .line 646
    .local v0, appState:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_SWITCH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_1a

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v2, :cond_1a

    const/4 v1, 0x1

    .end local v0           #appState:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :cond_1a
    :goto_1a
    return v1

    .line 644
    :catch_1b
    move-exception v2

    goto :goto_1a
.end method

.method private displayParticipantsInTray()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V

    .line 562
    return-void
.end method


# virtual methods
.method public final blockPerson(Ljava/io/Serializable;)V
    .registers 3
    .parameter "callbackData"

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->blockPerson(Ljava/io/Serializable;)V

    .line 551
    return-void
.end method

.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 8
    .parameter "source"

    .prologue
    const/4 v5, 0x0

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 253
    .local v2, res:Landroid/content/res/Resources;
    const-string v3, "block_icon"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 255
    const v3, 0x7f020149

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 261
    .local v1, icon:Landroid/graphics/Bitmap;
    :goto_14
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 262
    .local v0, drawable:Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 264
    .end local v0           #drawable:Landroid/graphics/drawable/BitmapDrawable;
    .end local v1           #icon:Landroid/graphics/Bitmap;
    :goto_24
    return-object v0

    .line 256
    :cond_25
    const-string v3, "exit_icon"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 257
    const v3, 0x7f020078

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .restart local v1       #icon:Landroid/graphics/Bitmap;
    goto :goto_14

    .line 259
    .end local v1           #icon:Landroid/graphics/Bitmap;
    :cond_35
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public final getGreenRoomParticipantListActivityIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 605
    .local p1, participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, p1}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutParticipantListActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 607
    .local v0, intent:Landroid/content/Intent;
    return-object v0
.end method

.method public final getHangoutNotificationIntent()Landroid/content/Intent;
    .registers 11

    .prologue
    .line 577
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    .line 580
    .local v2, nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 581
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x0

    .line 582
    .local v1, invitedAudience:Lcom/google/android/apps/plus/content/AudienceData;
    const-string v4, "audience"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 583
    const-string v4, "audience"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .end local v1           #invitedAudience:Lcom/google/android/apps/plus/content/AudienceData;
    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    .line 585
    .restart local v1       #invitedAudience:Lcom/google/android/apps/plus/content/AudienceData;
    :cond_1d
    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHangoutInfo()Lcom/google/android/apps/plus/service/Hangout$Info;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mSkipGreenRoom:Z

    new-instance v3, Landroid/content/Intent;

    const-class v7, Lcom/google/android/apps/plus/hangout/HangoutActivity;

    invoke-direct {v3, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unique"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "account"

    invoke-virtual {v3, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v4, "hangout_info"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v4, "hangout_skip_green_room"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v1, :cond_5a

    const-string v4, "audience"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 588
    .local v3, notificationIntent:Landroid/content/Intent;
    :cond_5a
    const-string v4, "hangout_skip_minor_warning"

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mSkipMinorWarning:Z

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 589
    return-object v3
.end method

.method public final getParticipantListActivityIntent()Landroid/content/Intent;
    .registers 10

    .prologue
    .line 615
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMembersOrderedByEntry()Ljava/util/List;

    move-result-object v4

    .line 617
    .local v4, meetingMembers:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/hangout/MeetingMember;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 618
    .local v6, participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_19
    :goto_19
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 619
    .local v3, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v7

    if-nez v7, :cond_19

    .line 620
    const-string v0, ""

    .line 623
    .local v0, fullName:Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getVCard()Lcom/google/android/apps/plus/hangout/VCard;

    move-result-object v7

    if-eqz v7, :cond_3b

    .line 624
    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getVCard()Lcom/google/android/apps/plus/hangout/VCard;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/VCard;->getFullName()Ljava/lang/String;

    move-result-object v0

    .line 626
    :cond_3b
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/apps/plus/service/Hangout;->getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v5

    .line 631
    .local v5, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_19

    .line 634
    .end local v0           #fullName:Ljava/lang/String;
    .end local v3           #meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    .end local v5           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_5b
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-static {p0, v7, v6}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutParticipantListActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v2

    .line 636
    .local v2, intent:Landroid/content/Intent;
    return-object v2
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 542
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 652
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onActivityResult(IILandroid/content/Intent;)V

    .line 653
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 654
    return-void
.end method

.method public final onBlockCompleted(Z)V
    .registers 2
    .parameter "success"

    .prologue
    .line 558
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, -0x1

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v12, 0x0

    .line 272
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 273
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutActivity.onCreate: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 278
    :try_start_19
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :try_end_24
    .catch Ljava/lang/LinkageError; {:try_start_19 .. :try_end_24} :catch_104

    .line 287
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 288
    .local v7, intent:Landroid/content/Intent;
    const-string v0, "account"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isChild()Z

    move-result v0

    if-eqz v0, :cond_156

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenMinorHangoutWarningDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_156

    const-string v0, "hangout_skip_minor_warning"

    invoke-virtual {v7, v0, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_156

    move v10, v4

    .line 292
    .local v10, needMinorNotification:Z
    :goto_4b
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_159

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE_INTERSTITIAL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_159

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenReportAbusetWarningDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_159

    move v9, v4

    .line 296
    .local v9, needAbuseNotification:Z
    :goto_64
    if-nez v10, :cond_15c

    if-nez v9, :cond_15c

    const-string v0, "hangout_skip_green_room"

    invoke-virtual {v7, v0, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_15c

    move v0, v4

    :goto_71
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mSkipGreenRoom:Z

    .line 298
    const-string v0, "hangout_info"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 301
    const-string v0, "hangout_participants"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    .line 306
    .local v3, participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v0, :cond_15f

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-eq v0, v1, :cond_9d

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Transfer:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v0, v1, :cond_15f

    .line 309
    :cond_9d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x680080

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 319
    :goto_a7
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_b1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    .line 320
    .local v6, actionBar:Landroid/app/ActionBar;
    :cond_b1
    if-eqz v6, :cond_b6

    .line 321
    invoke-virtual {v6, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 325
    :cond_b6
    invoke-static {p0}, Lcom/google/android/apps/plus/service/Hangout;->isAdvancedUiSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_164

    .line 326
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    .line 334
    :cond_c3
    :goto_c3
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v8, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 336
    .local v8, layoutParams:Landroid/view/ViewGroup$LayoutParams;
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {p0, v0, v8}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mSkipGreenRoom:Z

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setHangoutInfo(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;Ljava/util/ArrayList;ZZ)V

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreate(Landroid/os/Bundle;)V

    .line 341
    if-eqz v10, :cond_f0

    .line 342
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$MinorWarningDialog;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity;)V

    invoke-virtual {v0, v12}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "warning"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 344
    :cond_f0
    if-eqz v9, :cond_103

    .line 345
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity;)V

    invoke-virtual {v0, v12}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "warning"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 347
    .end local v3           #participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    .end local v6           #actionBar:Landroid/app/ActionBar;
    .end local v7           #intent:Landroid/content/Intent;
    .end local v8           #layoutParams:Landroid/view/ViewGroup$LayoutParams;
    .end local v9           #needAbuseNotification:Z
    .end local v10           #needMinorNotification:Z
    :cond_103
    :goto_103
    return-void

    .line 280
    :catch_104
    move-exception v0

    new-instance v11, Landroid/view/View;

    invoke-direct {v11, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 281
    .local v11, view:Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v11, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 282
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->setContentView(Landroid/view/View;)V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "showError: message=%s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v12

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x1080027

    invoke-static {v6, v0, v1, v6, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_103

    .end local v11           #view:Landroid/view/View;
    .restart local v7       #intent:Landroid/content/Intent;
    :cond_156
    move v10, v12

    .line 289
    goto/16 :goto_4b

    .restart local v10       #needMinorNotification:Z
    :cond_159
    move v9, v12

    .line 292
    goto/16 :goto_64

    .restart local v9       #needAbuseNotification:Z
    :cond_15c
    move v0, v12

    .line 296
    goto/16 :goto_71

    .line 315
    .restart local v3       #participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    :cond_15f
    invoke-static {}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->stopRingActivity()V

    goto/16 :goto_a7

    .line 328
    .restart local v6       #actionBar:Landroid/app/ActionBar;
    :cond_164
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    .line 330
    if-eqz v6, :cond_c3

    .line 331
    invoke-virtual {v6}, Landroid/app/ActionBar;->hide()V

    goto/16 :goto_c3
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 8
    .parameter "menu"

    .prologue
    const/4 v5, 0x0

    .line 448
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 450
    .local v1, inflater:Landroid/view/MenuInflater;
    const v4, 0x7f10000e

    invoke-virtual {v1, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 451
    const v4, 0x7f100011

    invoke-virtual {v1, v4, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 452
    const v4, 0x7f0902b1

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 453
    .local v3, transferItem:Landroid/view/MenuItem;
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->canTransfer()Z

    move-result v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 456
    const v4, 0x7f090290

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 457
    .local v2, item:Landroid/view/MenuItem;
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 458
    const v4, 0x7f09028f

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 459
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 461
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isDebuggable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_45

    .line 462
    const-string v4, "Debug"

    invoke-interface {p1, v4}, Landroid/view/Menu;->addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v0

    .line 463
    .local v0, debugMenu:Landroid/view/Menu;
    const v4, 0x7f10000b

    invoke-virtual {v1, v4, v0}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 466
    .end local v0           #debugMenu:Landroid/view/Menu;
    :cond_45
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v4, :cond_4e

    .line 467
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v4, p1, v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 469
    :cond_4e
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    return v4
.end method

.method public final onMeetingMediaStarted()V
    .registers 1

    .prologue
    .line 598
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    .prologue
    .line 435
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onNewIntent:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 436
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->setIntent(Landroid/content/Intent;)V

    .line 437
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 10
    .parameter "item"

    .prologue
    const/4 v5, 0x1

    .line 492
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 493
    .local v2, itemId:I
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 534
    :goto_d
    return v5

    .line 496
    :cond_e
    const v6, 0x102002c

    if-ne v2, v6, :cond_19

    .line 497
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_d

    .line 501
    :cond_19
    const v6, 0x7f090290

    if-ne v2, v6, :cond_38

    .line 502
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0802df

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 503
    .local v1, helpUrl:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 504
    .local v3, launchBrowser:Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_d

    .line 507
    .end local v1           #helpUrl:Ljava/lang/String;
    .end local v3           #launchBrowser:Landroid/content/Intent;
    :cond_38
    const v6, 0x7f09028f

    if-ne v2, v6, :cond_46

    .line 508
    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 509
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    goto :goto_d

    .line 512
    :cond_46
    const v6, 0x7f0902a5

    if-ne v2, v6, :cond_65

    .line 516
    new-instance v4, Lcom/google/android/apps/plus/hangout/crash/CrashReport;

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;-><init>(Z)V

    .line 518
    .local v4, report:Lcom/google/android/apps/plus/hangout/crash/CrashReport;
    :try_start_50
    new-instance v6, Ljava/lang/Exception;

    const-string v7, "Dummy exception for testing crash reports"

    invoke-direct {v6, v7}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_58
    .catch Ljava/lang/Exception; {:try_start_50 .. :try_end_58} :catch_58

    .line 519
    :catch_58
    move-exception v0

    .line 520
    .local v0, ex:Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->computeJavaCrashSignature(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->generateReport(Ljava/lang/String;)Z

    .line 522
    const/4 v6, 0x0

    invoke-virtual {v4, p0, v6}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->send(Landroid/app/Activity;Z)V

    goto :goto_d

    .line 525
    .end local v0           #ex:Ljava/lang/Exception;
    .end local v4           #report:Lcom/google/android/apps/plus/hangout/crash/CrashReport;
    :cond_65
    const v6, 0x7f0902a6

    if-ne v2, v6, :cond_72

    .line 526
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommApp;->raiseNetworkError()V

    goto :goto_d

    .line 529
    :cond_72
    const v6, 0x7f0902b1

    if-ne v2, v6, :cond_7d

    .line 530
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/HangoutTile;->transfer()V

    goto :goto_d

    .line 534
    :cond_7d
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    goto :goto_d
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_e

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onTilePause()V

    .line 390
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPause()V

    .line 393
    :cond_e
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 394
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 477
    const v1, 0x7f0902b1

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 478
    .local v0, transferItem:Landroid/view/MenuItem;
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->canTransfer()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 480
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v1, :cond_17

    .line 481
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 483
    :cond_17
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 375
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_11

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onResume()V

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onTileResume()V

    .line 381
    :cond_11
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 3
    .parameter "outState"

    .prologue
    .line 401
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_c

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 406
    :cond_c
    return-void
.end method

.method public onStart()V
    .registers 4

    .prologue
    .line 354
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    .line 356
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v1, :cond_1b

    .line 357
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStart()V

    .line 358
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onTileStart()V

    .line 359
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->displayParticipantsInTray()V

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTile;->addParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V

    .line 364
    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 365
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_2b

    .line 366
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mShakeDetectorWasRunning:Z

    .line 368
    :cond_2b
    return-void
.end method

.method protected onStop()V
    .registers 4

    .prologue
    .line 413
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStop()V

    .line 415
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v1, :cond_18

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->hangoutParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTile;->removeParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V

    .line 417
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onTileStop()V

    .line 418
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStop()V

    .line 422
    :cond_18
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_29

    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 424
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_29

    .line 425
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 428
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_29
    return-void
.end method

.method public final stopHangoutTile()V
    .registers 1

    .prologue
    .line 569
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity;->finish()V

    .line 570
    return-void
.end method
