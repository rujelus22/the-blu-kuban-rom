.class public Lcom/google/android/apps/plus/views/EventDetailsHeaderView;
.super Landroid/view/ViewGroup;
.source "EventDetailsHeaderView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sAvatarOverlap:I

.field private static sAvatarSize:I

.field private static sCollapseText:Ljava/lang/String;

.field private static sExpandText:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sOnAirColor:I

.field private static sOnAirDrawable:Landroid/graphics/drawable/Drawable;

.field private static sOnAirText:Ljava/lang/String;

.field private static sPadding:I

.field private static sPrivatePublicColor:I

.field private static sPrivateText:Ljava/lang/String;

.field private static sPublicText:Ljava/lang/String;

.field private static sSecondaryPadding:I

.field private static sTypeSize:F


# instance fields
.field private mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

.field private mChevronResId:I

.field private mExpandCollapseChevronView:Landroid/widget/ImageView;

.field private mExpandCollapseTextView:Landroid/widget/TextView;

.field private mExpandCollapseView:Landroid/view/View;

.field private mOnAirWrap:Z

.field private mOnClickListener:Landroid/view/View$OnClickListener;

.field private mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

.field private mTitleView:Landroid/widget/TextView;

.field private mTypeView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sInitialized:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 11
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const v6, 0x7f0d009e

    const v5, 0x7f020155

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 72
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    sget-boolean v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sInitialized:Z

    if-nez v1, :cond_8d

    .line 75
    sput-boolean v4, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sInitialized:Z

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0d009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 79
    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    int-to-float v1, v1

    const v2, 0x7f0d009b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarOverlap:I

    .line 81
    const v1, 0x7f0d0092

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    .line 82
    const v1, 0x7f0d00a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sSecondaryPadding:I

    .line 84
    const v1, 0x7f0800fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirText:Ljava/lang/String;

    .line 85
    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirColor:I

    .line 86
    const v1, 0x7f0800ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivateText:Ljava/lang/String;

    .line 87
    const v1, 0x7f080100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPublicText:Ljava/lang/String;

    .line 88
    const v1, 0x7f0a0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivatePublicColor:I

    .line 89
    const v1, 0x7f0d00a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sTypeSize:F

    .line 90
    const v1, 0x7f020034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirDrawable:Landroid/graphics/drawable/Drawable;

    .line 92
    const v1, 0x7f0801d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    .line 93
    const v1, 0x7f0801d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sCollapseText:Ljava/lang/String;

    .line 96
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_8d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 98
    .restart local v0       #res:Landroid/content/res/Resources;
    new-instance v1, Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventThemeView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/EventThemeView;->setFadeIn(Z)V

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 102
    new-instance v1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 107
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    const v2, 0x7f0a0060

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 114
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 116
    iput v5, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mChevronResId:I

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 119
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 123
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const v2, 0x7f0d009f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 130
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    const v2, 0x7f0a0061

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 139
    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Landroid/view/View$OnClickListener;ZLcom/google/android/apps/plus/views/EventActionListener;)V
    .registers 8
    .parameter "event"
    .parameter "expandCollapseListener"
    .parameter "isCardLayout"
    .parameter "actionListener"

    .prologue
    const/4 v2, 0x0

    .line 268
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeView(Landroid/view/View;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeView(Landroid/view/View;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->removeView(Landroid/view/View;)V

    .line 276
    if-eqz p2, :cond_36

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 280
    if-eqz p3, :cond_36

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->addView(Landroid/view/View;)V

    .line 285
    :cond_36
    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 286
    iput-object p4, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 288
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v0, :cond_70

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    if-eqz v0, :cond_70

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sOnAirDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 303
    :goto_68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sTypeSize:F

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 305
    return-void

    .line 294
    :cond_70
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    if-eqz v0, :cond_99

    .line 295
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_96

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPublicText:Ljava/lang/String;

    :goto_80
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivatePublicColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_68

    .line 295
    :cond_96
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPrivateText:Ljava/lang/String;

    goto :goto_80

    .line 300
    :cond_99
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_68
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 317
    instance-of v0, p1, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_14

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    check-cast p1, Lcom/google/android/apps/plus/views/AvatarView;

    .end local p1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onAvatarClicked(Ljava/lang/String;)V

    .line 322
    :cond_13
    :goto_13
    return-void

    .line 319
    .restart local p1
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_13

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_13
.end method

.method protected onLayout(ZIIII)V
    .registers 21
    .parameter "changed"
    .parameter "l"
    .parameter "t"
    .parameter "r"
    .parameter "b"

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->getMeasuredWidth()I

    move-result v10

    .line 219
    .local v10, width:I
    int-to-float v11, v10

    const v12, 0x40570a3d

    div-float/2addr v11, v12

    float-to-int v7, v11

    .line 220
    .local v7, themeHeight:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13, v10, v7}, Lcom/google/android/apps/plus/views/EventThemeView;->layout(IIII)V

    .line 222
    sget v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    .line 223
    .local v0, avatarX:I
    sget v11, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarOverlap:I

    sub-int v1, v7, v11

    .line 224
    .local v1, avatarY:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    sget v12, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    add-int/2addr v12, v0

    sget v13, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    add-int/2addr v13, v1

    invoke-virtual {v11, v0, v1, v12, v13}, Lcom/google/android/apps/plus/views/AvatarView;->layout(IIII)V

    .line 226
    sget v11, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sget v12, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    add-int/2addr v11, v12

    sget v12, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int v6, v11, v12

    .line 228
    .local v6, textX:I
    sget v11, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int v8, v7, v11

    .line 229
    .local v8, topInfoY:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v6

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v8

    invoke-virtual {v11, v6, v8, v12, v13}, Landroid/widget/TextView;->layout(IIII)V

    .line 232
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getVisibility()I

    move-result v11

    if-nez v11, :cond_65

    .line 233
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    add-int v9, v8, v11

    .line 234
    .local v9, typeY:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v6

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v9

    invoke-virtual {v11, v6, v9, v12, v13}, Landroid/widget/TextView;->layout(IIII)V

    .line 240
    .end local v9           #typeY:I
    :cond_65
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v11, :cond_c8

    .line 241
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    .line 243
    .local v2, expandCollapseHeight:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getBaseline()I

    move-result v11

    add-int/2addr v11, v8

    sub-int v4, v11, v2

    .line 244
    .local v4, expandCollapseY:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v11}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v11

    sub-int v11, v10, v11

    sget v12, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sSecondaryPadding:I

    sub-int v3, v11, v12

    .line 247
    .local v3, expandCollapseX:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v12}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v3

    add-int v13, v4, v2

    invoke-virtual {v11, v3, v4, v12, v13}, Landroid/widget/ImageView;->layout(IIII)V

    .line 253
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v11

    sub-int v11, v3, v11

    sget v12, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sub-int v5, v11, v12

    .line 255
    .local v5, expandTextLeft:I
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v5

    iget-object v13, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v8

    invoke-virtual {v11, v5, v8, v12, v13}, Landroid/widget/TextView;->layout(IIII)V

    .line 260
    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    sget v12, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    sget v13, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    iget-object v14, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v13, v14

    iget-object v14, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    add-int/2addr v14, v7

    invoke-virtual {v11, v12, v7, v13, v14}, Landroid/view/View;->layout(IIII)V

    .line 264
    .end local v2           #expandCollapseHeight:I
    .end local v3           #expandCollapseX:I
    .end local v4           #expandCollapseY:I
    .end local v5           #expandTextLeft:I
    :cond_c8
    return-void
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 147
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 149
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 150
    .local v7, width:I
    if-nez v7, :cond_d

    .line 151
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 154
    :cond_d
    int-to-float v8, v7

    const v9, 0x40570a3d

    div-float/2addr v8, v9

    float-to-int v3, v8

    .line 155
    .local v3, themeHeight:I
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    const/high16 v9, 0x4000

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/high16 v10, 0x4000

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/EventThemeView;->measure(II)V

    .line 158
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    const/high16 v10, 0x4000

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    const/high16 v11, 0x4000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    .line 161
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/AvatarView;->getMeasuredWidth()I

    move-result v9

    sub-int v9, v7, v9

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    mul-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sSecondaryPadding:I

    sub-int/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 164
    .local v1, maxTextWidth:I
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v8, :cond_73

    .line 165
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/ImageView;->measure(II)V

    .line 169
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v9

    sget v10, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int/2addr v9, v10

    sub-int v9, v1, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 173
    :cond_73
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    const/high16 v9, -0x8000

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    .line 176
    const/4 v8, 0x0

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    sub-int v9, v1, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 177
    .local v5, typeViewMaxWidth:I
    const/4 v6, 0x0

    .line 178
    .local v6, typeViewWidth:I
    const/4 v4, 0x0

    .line 179
    .local v4, typeViewHeight:I
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnAirWrap:Z

    .line 181
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getVisibility()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_dc

    .line 182
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    .line 186
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    if-le v8, v5, :cond_134

    const/4 v8, 0x1

    :goto_b5
    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnAirWrap:Z

    .line 189
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnAirWrap:Z

    if-eqz v8, :cond_cc

    .line 190
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    const/high16 v9, -0x8000

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    .line 195
    :cond_cc
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int v6, v8, v9

    .line 196
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTypeView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 199
    :cond_dc
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v9

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnAirWrap:Z

    if-eqz v10, :cond_e8

    const/4 v6, 0x0

    .end local v6           #typeViewWidth:I
    :cond_e8
    invoke-static {v9, v6}, Ljava/lang/Math;->max(II)I

    move-result v9

    sub-int v9, v1, v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 202
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    const/high16 v9, -0x8000

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    .line 205
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sPadding:I

    add-int v2, v8, v9

    .line 206
    .local v2, textInfoHeight:I
    sget v8, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarOverlap:I

    sub-int/2addr v8, v9

    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v8

    add-int v0, v3, v8

    .line 208
    .local v0, height:I
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseView:Landroid/view/View;

    sget v9, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sAvatarSize:I

    sub-int v9, v7, v9

    const/high16 v10, 0x4000

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    sub-int v10, v0, v3

    const/high16 v11, 0x4000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->measure(II)V

    .line 212
    invoke-virtual {p0, v7, v0}, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->setMeasuredDimension(II)V

    .line 213
    return-void

    .line 186
    .end local v0           #height:I
    .end local v2           #textInfoHeight:I
    .restart local v6       #typeViewWidth:I
    :cond_134
    const/4 v8, 0x0

    goto :goto_b5
.end method

.method public onRecycle()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->onRecycle()V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 313
    return-void
.end method

.method public setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V
    .registers 3
    .parameter "theme"

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mThemeImageView:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/EventThemeView;->setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V

    .line 143
    return-void
.end method

.method public setExpandState(Z)V
    .registers 4
    .parameter "expanded"

    .prologue
    .line 325
    if-eqz p1, :cond_21

    const v0, 0x7f020157

    :goto_5
    iput v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mChevronResId:I

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_25

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sCollapseText:Ljava/lang/String;

    :goto_d
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseChevronView:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mChevronResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_20

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/views/EventActionListener;->onExpansionToggled(Z)V

    .line 334
    :cond_20
    return-void

    .line 325
    :cond_21
    const v0, 0x7f020155

    goto :goto_5

    .line 328
    :cond_25
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->sExpandText:Ljava/lang/String;

    goto :goto_d
.end method

.method public setLayoutType(Z)V
    .registers 4
    .parameter "cardLayout"

    .prologue
    .line 340
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsHeaderView;->mExpandCollapseTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_9

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 341
    return-void

    .line 340
    :cond_9
    const/16 v0, 0x8

    goto :goto_5
.end method
