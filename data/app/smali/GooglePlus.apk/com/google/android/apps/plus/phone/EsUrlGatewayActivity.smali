.class public abstract Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EsUrlGatewayActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;
    }
.end annotation


# static fields
.field private static sKnownUnsupportedUri:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/net/Uri;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mActivityId:Ljava/lang/String;

.field protected mAlbumId:Ljava/lang/String;

.field protected mDesktopActivityId:Ljava/lang/String;

.field private mEventCreatorId:Ljava/lang/String;

.field private mEventId:Ljava/lang/String;

.field private mEventInvitationToken:Ljava/lang/String;

.field protected mGaiaId:Ljava/lang/String;

.field protected mHangoutDomain:Ljava/lang/String;

.field protected mHangoutId:Ljava/lang/String;

.field protected mHangoutServiceId:Ljava/lang/String;

.field protected mName:Ljava/lang/String;

.field protected mPhotoId:J

.field protected mProfileId:Ljava/lang/String;

.field protected mProfileIdValidated:Z

.field protected mRequestType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 114
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 749
    return-void
.end method

.method private static parseLong(Ljava/lang/String;)J
    .registers 3
    .parameter "string"

    .prologue
    .line 740
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-wide v0

    .line 742
    :goto_4
    return-wide v0

    :catch_5
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_4
.end method

.method private parseUri(Landroid/net/Uri;)V
    .registers 16
    .parameter "uri"

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 193
    :goto_5
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    .line 194
    .local v4, segments:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lt v6, v10, :cond_74

    const-string v6, "_"

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_74

    const-string v6, "notifications"

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_74

    .line 196
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lt v7, v12, :cond_a2

    const-string v7, "emlink"

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a2

    const-string v6, "path"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_a2

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_60

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "/"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_60
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "http://plus.google.com"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    goto :goto_5

    .line 200
    :cond_74
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 203
    .local v5, size:I
    if-lt v5, v10, :cond_8c

    const-string v6, "u"

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8c

    .line 204
    invoke-interface {v4, v10, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 205
    add-int/lit8 v5, v5, -0x2

    .line 208
    :cond_8c
    if-lez v5, :cond_1b7

    const-string v6, "photos"

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b7

    .line 209
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v9, :cond_a3

    iput v12, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 294
    .end local v5           #size:I
    :cond_a2
    :goto_a2
    return-void

    .line 209
    .restart local v5       #size:I
    :cond_a3
    if-ne v6, v10, :cond_b8

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v7, "fromphone"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a2

    const/16 v6, 0xb

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto :goto_a2

    :cond_b8
    if-ne v6, v12, :cond_f2

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "of"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d5

    const/16 v6, 0xc

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto :goto_a2

    :cond_d5
    const-string v8, "posts"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e4

    const/16 v7, 0xd

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto :goto_a2

    :cond_e4
    const-string v8, "albums"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a2

    const/4 v7, 0x7

    iput v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto :goto_a2

    :cond_f2
    if-ne v6, v13, :cond_140

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v4, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const-string v9, "albums"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_116

    const-string v9, "album"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a2

    :cond_116
    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    const-string v6, "profile"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_126

    const/16 v6, 0x10

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_126
    const-string v6, "posts"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_134

    const/16 v6, 0xd

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_134
    iput-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v6, :cond_a2

    const/16 v6, 0xe

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_140
    const/4 v7, 0x5

    if-ne v6, v7, :cond_a2

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v4, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v4, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    const-string v10, "albums"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a2

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    const-string v6, "profile"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_181

    invoke-static {v9}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    iget-wide v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a2

    const/16 v6, 0x11

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_181
    const-string v6, "posts"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19d

    invoke-static {v9}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    iget-wide v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a2

    const/16 v6, 0x12

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_19d
    iput-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-static {v9}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    if-eqz v6, :cond_a2

    iget-wide v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a2

    const/16 v6, 0xf

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 213
    :cond_1b7
    if-lez v5, :cond_253

    const-string v6, "hangouts"

    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_253

    .line 215
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lt v6, v10, :cond_a2

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7, v4}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v8, "hangouts"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a2

    invoke-interface {v7, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v8, "_"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1f2

    invoke-interface {v7, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1f2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v6

    if-ne v6, v9, :cond_209

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v6, 0x14

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_209
    if-ne v6, v10, :cond_a2

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "google.com"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_233

    sget-boolean v8, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v8, :cond_233

    const-string v6, "google.com"

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    invoke-static {v7}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v6, 0x16

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_233
    const-string v8, "lite"

    invoke-virtual {v8, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_246

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v6, 0x17

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    :cond_246
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/16 v6, 0x15

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 219
    :cond_253
    if-nez v5, :cond_259

    .line 220
    iput v9, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 224
    :cond_259
    invoke-interface {v4, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 226
    .local v1, segment0:Ljava/lang/String;
    const-string v6, "settings"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_a2

    .line 234
    if-ne v5, v9, :cond_2c7

    .line 235
    const-string v6, "stream"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_275

    .line 236
    iput v10, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 237
    :cond_275
    const-string v6, "me"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_281

    .line 238
    iput v13, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 239
    :cond_281
    const-string v6, "circles"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_28f

    .line 240
    const/16 v6, 0x8

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 241
    :cond_28f
    const-string v6, "hot"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_29f

    const-string v6, "explore"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2a5

    .line 242
    :cond_29f
    const/16 v6, 0x19

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 243
    :cond_2a5
    const-string v6, "events"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2b3

    .line 244
    const/16 v6, 0x1c

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 245
    :cond_2b3
    const-string v6, "+"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2bf

    .line 246
    iput v9, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 248
    :cond_2bf
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    .line 249
    const/16 v6, 0x13

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 251
    :cond_2c7
    if-ne v5, v10, :cond_332

    .line 252
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 253
    .local v2, segment1:Ljava/lang/String;
    const-string v6, "posts"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2df

    const-string v6, "stream"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2e6

    .line 254
    :cond_2df
    const/4 v6, 0x5

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 255
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 256
    :cond_2e6
    const-string v6, "about"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2f5

    .line 257
    const/4 v6, 0x6

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 258
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 259
    :cond_2f5
    const-string v6, "photos"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_304

    .line 260
    const/4 v6, 0x7

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 261
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 262
    :cond_304
    const-string v6, "circles"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_31a

    const-string v6, "find"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_31a

    .line 263
    const/16 v6, 0x1a

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    goto/16 :goto_a2

    .line 264
    :cond_31a
    const-string v6, "events"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a2

    .line 265
    const/16 v6, 0x1b

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 266
    const-string v6, "gpinv"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    .line 267
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 269
    .end local v2           #segment1:Ljava/lang/String;
    :cond_332
    if-ne v5, v12, :cond_a2

    .line 270
    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 271
    .restart local v2       #segment1:Ljava/lang/String;
    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 273
    .local v3, segment2:Ljava/lang/String;
    const-string v6, "posts"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_352

    .line 274
    const/16 v6, 0x9

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 275
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    .line 276
    iput-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mDesktopActivityId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 277
    :cond_352
    const-string v6, "digest"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_361

    .line 279
    const/4 v6, 0x5

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 280
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 281
    :cond_361
    const-string v6, "notifications"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_385

    const-string v6, "all"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_385

    .line 282
    const-string v6, "mute"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, mute:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a2

    .line 284
    const/16 v6, 0x18

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 285
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    goto/16 :goto_a2

    .line 287
    .end local v0           #mute:Ljava/lang/String;
    :cond_385
    const-string v6, "events"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a2

    .line 288
    const/16 v6, 0x1b

    iput v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 289
    const-string v6, "gpinv"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    .line 290
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    .line 291
    iput-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    goto/16 :goto_a2
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 735
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isReadyToRedirect()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 439
    iget v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    packed-switch v2, :pswitch_data_1e

    .line 477
    :cond_7
    :goto_7
    :pswitch_7
    return v0

    .line 454
    :pswitch_8
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_7

    .line 468
    :pswitch_e
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileIdValidated:Z

    if-nez v2, :cond_7

    :cond_16
    move v0, v1

    goto :goto_7

    .line 474
    :pswitch_18
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    if-nez v2, :cond_7

    move v0, v1

    goto :goto_7

    .line 439
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_7
        :pswitch_8
        :pswitch_e
        :pswitch_7
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_18
        :pswitch_7
    .end packed-switch
.end method

.method protected final launchBrowser(Landroid/net/Uri;)V
    .registers 12
    .parameter "uri"

    .prologue
    .line 687
    sget-object v7, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/HashSet;

    .line 688
    .local v6, unsupported:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/net/Uri;>;"
    if-nez v6, :cond_20

    .line 689
    new-instance v6, Ljava/util/HashSet;

    .end local v6           #unsupported:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/net/Uri;>;"
    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 690
    .restart local v6       #unsupported:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/net/Uri;>;"
    sget-object v7, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 693
    :cond_20
    invoke-virtual {v6, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 695
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 696
    .local v2, intent:Landroid/content/Intent;
    const/high16 v7, 0x8

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 697
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 699
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 700
    .local v3, pm:Landroid/content/pm/PackageManager;
    const/high16 v7, 0x1

    invoke-virtual {v3, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 701
    .local v5, ris:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v5, :cond_67

    .line 703
    const/4 v1, 0x0

    .local v1, i:I
    :goto_3f
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_67

    .line 704
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 705
    .local v4, ri:Landroid/content/pm/ResolveInfo;
    iget-object v0, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 706
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    if-eqz v0, :cond_6b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getPackageName()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6b

    .line 707
    new-instance v7, Landroid/content/ComponentName;

    iget-object v8, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v9, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 713
    .end local v0           #ai:Landroid/content/pm/ActivityInfo;
    .end local v1           #i:I
    .end local v4           #ri:Landroid/content/pm/ResolveInfo;
    :cond_67
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    .line 714
    return-void

    .line 703
    .restart local v0       #ai:Landroid/content/pm/ActivityInfo;
    .restart local v1       #i:I
    .restart local v4       #ri:Landroid/content/pm/ResolveInfo;
    :cond_6b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3f
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, intent:Landroid/content/Intent;
    if-eqz p1, :cond_98

    .line 126
    const-string v2, "request_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    .line 127
    const-string v2, "profile_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    .line 128
    const-string v2, "profile_id_validated"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileIdValidated:Z

    .line 129
    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    .line 130
    const-string v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    .line 131
    const-string v2, "activity_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mDesktopActivityId:Ljava/lang/String;

    .line 132
    const-string v2, "album_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    .line 133
    const-string v2, "photo_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    .line 134
    const-string v2, "hangout_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    .line 135
    const-string v2, "hangout_domain"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    .line 136
    const-string v2, "service-id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    .line 137
    const-string v2, "event_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    .line 138
    const-string v2, "event_creator_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    .line 139
    const-string v2, "event_invitation_token"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    .line 156
    :goto_79
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 157
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_97

    .line 158
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 159
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->startActivity(Landroid/content/Intent;)V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    .line 163
    :cond_97
    :goto_97
    return-void

    .line 142
    :cond_98
    const-string v2, "customAppUri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b0

    .line 143
    const-string v2, "customAppUri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 148
    .local v1, uri:Landroid/net/Uri;
    :goto_aa
    if-nez v1, :cond_b5

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    goto :goto_97

    .line 145
    .end local v1           #uri:Landroid/net/Uri;
    :cond_b0
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #uri:Landroid/net/Uri;
    goto :goto_aa

    .line 153
    :cond_b5
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->parseUri(Landroid/net/Uri;)V

    goto :goto_79
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 171
    const-string v0, "request_type"

    iget v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    const-string v0, "profile_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "profile_id_validated"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mProfileIdValidated:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 174
    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v0, "album_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v0, "photo_id"

    iget-wide v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 179
    const-string v0, "hangout_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v0, "hangout_domain"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v0, "service-id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v0, "event_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v0, "event_creator_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v0, "event_invitation_token"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method protected final redirect()V
    .registers 13

    .prologue
    .line 481
    iget v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mRequestType:I

    packed-switch v1, :pswitch_data_2fc

    .line 658
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    .line 660
    :goto_8
    return-void

    .line 483
    :pswitch_9
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_8

    .line 487
    :pswitch_13
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v3, 0x5

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_8

    .line 491
    :pswitch_30
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v3, 0x5

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "people_view_type"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_8

    .line 495
    :pswitch_4e
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_8

    .line 499
    :pswitch_58
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "circle_id"

    const-string v3, "v.whatshot"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto :goto_8

    .line 503
    :pswitch_7c
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 509
    :pswitch_8f
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 515
    :pswitch_a2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 522
    :pswitch_af
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 527
    :pswitch_cd
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "g:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedProfileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 532
    :pswitch_eb
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const v2, 0x7f08006d

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "camerasync"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 541
    :pswitch_119
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const v2, 0x7f080070

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 549
    :pswitch_145
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const v2, 0x7f08007f

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "posts"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 558
    :pswitch_16f
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 566
    :pswitch_18e
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 575
    :pswitch_1b3
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 578
    .local v0, photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAlbumId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 587
    .end local v0           #photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    :pswitch_1e3
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 590
    .restart local v0       #photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 600
    .end local v0           #photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    :pswitch_213
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mPhotoId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 603
    .restart local v0       #photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    const-string v2, "posts"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 613
    .end local v0           #photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    :pswitch_243
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 621
    :pswitch_25c
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 629
    :pswitch_275
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutServiceId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 637
    :pswitch_28f
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$RoomType;->WITH_EXTRAS:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutDomain:Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mHangoutId:Ljava/lang/String;

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Url:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 645
    :pswitch_2a8
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mActivityId:Ljava/lang/String;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/StreamOneUpActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "account"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "activity_id"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mute"

    const/4 v2, 0x1

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 649
    :pswitch_2cd
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 653
    :pswitch_2eb
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventCreatorId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mEventInvitationToken:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->redirect(Landroid/content/Intent;)V

    goto/16 :goto_8

    .line 481
    :pswitch_data_2fc
    .packed-switch 0x1
        :pswitch_9
        :pswitch_4e
        :pswitch_8f
        :pswitch_7c
        :pswitch_af
        :pswitch_af
        :pswitch_cd
        :pswitch_13
        :pswitch_a2
        :pswitch_5
        :pswitch_eb
        :pswitch_119
        :pswitch_145
        :pswitch_16f
        :pswitch_1b3
        :pswitch_18e
        :pswitch_1e3
        :pswitch_213
        :pswitch_af
        :pswitch_243
        :pswitch_275
        :pswitch_28f
        :pswitch_25c
        :pswitch_2a8
        :pswitch_58
        :pswitch_30
        :pswitch_2eb
        :pswitch_2cd
    .end packed-switch
.end method

.method protected redirect(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    .prologue
    .line 717
    const/high16 v0, 0x200

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 718
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->startActivity(Landroid/content/Intent;)V

    .line 719
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->finish()V

    .line 720
    return-void
.end method

.method protected final redirectToBrowser()V
    .registers 8

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 669
    .local v4, uri:Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 670
    .local v2, scheme:Ljava/lang/String;
    const-string v5, "http"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_20

    const-string v5, "content"

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 671
    :cond_20
    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "https"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 674
    :cond_2e
    sget-object v5, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->sKnownUnsupportedUri:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/HashSet;

    .line 675
    .local v3, unsupported:Ljava/util/HashSet;,"Ljava/util/HashSet<Landroid/net/Uri;>;"
    if-eqz v3, :cond_48

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_48

    .line 676
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->launchBrowser(Landroid/net/Uri;)V

    .line 684
    :goto_47
    return-void

    .line 678
    :cond_48
    new-instance v1, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;-><init>()V

    .line 679
    .local v1, dialog:Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 680
    .local v0, args:Landroid/os/Bundle;
    const-string v5, "url"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 681
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;->setArguments(Landroid/os/Bundle;)V

    .line 682
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "unsupported"

    invoke-virtual {v1, v5, v6}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity$UnrecognizedLinkDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_47
.end method
