.class public Lcom/google/android/apps/plus/views/PhotoInfoView;
.super Landroid/view/View;
.source "PhotoInfoView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ItemClickListener;


# static fields
.field private static sAlbumPaint:Landroid/text/TextPaint;

.field private static sAlbumRightMargin:I

.field private static final sAvatarRect:Landroid/graphics/Rect;

.field private static sAvatarRightMargin:I

.field private static sCommentBitmap:Landroid/graphics/Bitmap;

.field private static sCommentCountLeftMargin:I

.field private static sCommentCountPaint:Landroid/text/TextPaint;

.field private static sCommentCountTextWidth:I

.field private static sDatePaint:Landroid/text/TextPaint;

.field private static sDateRightMargin:I

.field private static sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

.field private static sInfoAvatarHeight:I

.field private static sInfoAvatarWidth:I

.field private static sInfoBackground:Landroid/graphics/drawable/Drawable;

.field private static sInfoRightPadding:I

.field private static sInitialized:Z

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sPlusOneBitmap:Landroid/graphics/Bitmap;

.field private static sPlusOneBottomMargin:I

.field private static sPlusOneCountLeftMargin:I

.field private static sPlusOneCountPaint:Landroid/text/TextPaint;

.field private static sPlusOneCountTextWidth:I

.field private static sPlusoneByMeBitmap:Landroid/graphics/Bitmap;


# instance fields
.field private mAlbumContent:Ljava/lang/CharSequence;

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mAlbumStream:Ljava/lang/String;

.field private mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

.field private mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private mCommentText:Ljava/lang/String;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mDate:Ljava/lang/CharSequence;

.field private mEventId:Ljava/lang/String;

.field private mFixedHeight:I

.field private mHide:Z

.field private mOwnerId:Ljava/lang/String;

.field private mOwnerName:Ljava/lang/CharSequence;

.field private mPlusOneText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 97
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAvatarRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 102
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->initialize()V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->initialize()V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 112
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->initialize()V

    .line 114
    return-void
.end method

.method private getInfoBoxHeight()I
    .registers 4

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_a

    .line 465
    const/4 v0, 0x0

    .line 469
    .local v0, height:I
    :goto_9
    return v0

    .line 467
    .end local v0           #height:I
    :cond_a
    sget v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarHeight:I

    .restart local v0       #height:I
    goto :goto_9
.end method

.method private initialize()V
    .registers 10

    .prologue
    const v8, 0x7f0d006b

    const v7, 0x7f0d006a

    const v6, 0x7f0a0043

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 520
    sget-boolean v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInitialized:Z

    if-nez v2, :cond_188

    .line 521
    sput-boolean v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInitialized:Z

    .line 523
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 524
    .local v0, context:Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 527
    .local v1, resources:Landroid/content/res/Resources;
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    .line 528
    const v2, 0x7f02012a

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    .line 530
    const v2, 0x7f02012b

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    .line 531
    const v2, 0x7f020130

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusoneByMeBitmap:Landroid/graphics/Bitmap;

    .line 534
    const v2, 0x7f020195

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoBackground:Landroid/graphics/drawable/Drawable;

    .line 536
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 537
    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 538
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0042

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 539
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 540
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 541
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v2, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 543
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 544
    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 545
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0044

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 546
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    const v3, 0x7f0d006c

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 547
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    const v3, 0x7f0d006c

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 549
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 550
    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 551
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 552
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 553
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, v2, Landroid/text/TextPaint;->linkColor:I

    .line 554
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 556
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 557
    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 558
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0045

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 559
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountPaint:Landroid/text/TextPaint;

    const v3, 0x7f0d006d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 561
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountPaint:Landroid/text/TextPaint;

    const v3, 0x7f0d006d

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 564
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 565
    sput-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 566
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0046

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 567
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    const v3, 0x7f0d006e

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 569
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    const v3, 0x7f0d006e

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 573
    const v2, 0x7f0d0062

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 574
    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarHeight:I

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarWidth:I

    .line 575
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAvatarRect:Landroid/graphics/Rect;

    sget v3, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarWidth:I

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarHeight:I

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 577
    const v2, 0x7f0d0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoRightPadding:I

    .line 578
    const v2, 0x7f0d0063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAvatarRightMargin:I

    .line 580
    const v2, 0x7f0d0066

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDateRightMargin:I

    .line 581
    const v2, 0x7f0d0061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumRightMargin:I

    .line 582
    const v2, 0x7f0d0064

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountLeftMargin:I

    .line 584
    const v2, 0x7f0d0068

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountLeftMargin:I

    .line 586
    const v2, 0x7f0d0067

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBottomMargin:I

    .line 588
    const v2, 0x7f0d0065

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountTextWidth:I

    .line 590
    const v2, 0x7f0d0069

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountTextWidth:I

    .line 593
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #resources:Landroid/content/res/Resources;
    :cond_188
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter "event"

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v0, v4

    .line 251
    .local v0, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    .line 252
    .local v1, y:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_6c

    :pswitch_14
    move v2, v3

    .line 295
    :goto_15
    return v2

    .line 254
    :pswitch_16
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v4, :cond_2a

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v4, v0, v1, v3}, Lcom/google/android/apps/plus/views/ClickableUserImage;->handleEvent(III)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 255
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->invalidate()V

    goto :goto_15

    .line 258
    :cond_2a
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v4, :cond_3e

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4, v0, v1, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->handleEvent(III)Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 260
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->invalidate()V

    goto :goto_15

    :cond_3e
    move v2, v3

    .line 265
    goto :goto_15

    .line 269
    :pswitch_40
    iput-object v5, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 271
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v4, :cond_4b

    .line 272
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->handleEvent(III)Z

    .line 275
    :cond_4b
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v4, :cond_54

    .line 276
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->handleEvent(III)Z

    .line 279
    :cond_54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->invalidate()V

    move v2, v3

    .line 280
    goto :goto_15

    .line 284
    :pswitch_59
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v4, :cond_69

    .line 285
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v4, 0x3

    invoke-interface {v3, v0, v1, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    .line 286
    iput-object v5, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    .line 287
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->invalidate()V

    goto :goto_15

    :cond_69
    move v2, v3

    .line 290
    goto :goto_15

    .line 252
    nop

    :pswitch_data_6c
    .packed-switch 0x0
        :pswitch_16
        :pswitch_40
        :pswitch_14
        :pswitch_59
    .end packed-switch
.end method

.method public final hideHeaderInfoView(Z)V
    .registers 4
    .parameter "animate"

    .prologue
    .line 304
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mHide:Z

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 307
    .local v0, currentAnimation:Landroid/view/animation/Animation;
    if-eqz v0, :cond_c

    .line 308
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 311
    :cond_c
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setVisibility(I)V

    .line 334
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 30
    .parameter "canvas"

    .prologue
    .line 338
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 340
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getInfoBoxHeight()I

    move-result v20

    .line 341
    .local v20, infoBoxHeight:I
    if-lez v20, :cond_2f3

    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getHeight()I

    move-result v2

    sub-int v21, v2, v20

    .line 346
    .local v21, infoBoxY:I
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoBackground:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getWidth()I

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getHeight()I

    move-result v6

    move/from16 v0, v21

    invoke-virtual {v2, v4, v0, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 347
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 349
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBottomMargin:I

    add-int/2addr v2, v4

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int v18, v2, v4

    .line 351
    .local v18, countHeight:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getWidth()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoRightPadding:I

    sub-int v25, v2, v4

    .line 352
    .local v25, xPos:I
    sub-int v2, v20, v18

    div-int/lit8 v2, v2, 0x2

    add-int v27, v21, v2

    .line 355
    .local v27, yPos:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    if-eqz v2, :cond_93

    .line 356
    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountTextWidth:I

    sub-int v25, v25, v2

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    move/from16 v0, v25

    int-to-float v4, v0

    move/from16 v0, v27

    int-to-float v5, v0

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 360
    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneCountLeftMargin:I

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/2addr v2, v4

    sub-int v25, v25, v2

    .line 361
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v25

    int-to-float v4, v0

    move/from16 v0, v27

    int-to-float v5, v0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 363
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getWidth()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoRightPadding:I

    sub-int v25, v2, v4

    .line 364
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sPlusOneBottomMargin:I

    add-int/2addr v2, v4

    add-int v27, v27, v2

    .line 368
    :cond_93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    if-eqz v2, :cond_ce

    .line 369
    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountTextWidth:I

    sub-int v25, v25, v2

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    move/from16 v0, v25

    int-to-float v4, v0

    move/from16 v0, v27

    int-to-float v5, v0

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v6}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    sub-float/2addr v5, v6

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 373
    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentCountLeftMargin:I

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/2addr v2, v4

    sub-int v25, v25, v2

    .line 374
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sCommentBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v25

    int-to-float v4, v0

    move/from16 v0, v27

    int-to-float v5, v0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 377
    :cond_ce
    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDateRightMargin:I

    sub-int v26, v25, v2

    .line 380
    .local v26, xPosEnd:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v2, :cond_2f4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_2f4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v17

    .line 382
    .local v17, avatar:Landroid/graphics/Bitmap;
    :goto_ea
    const/4 v2, 0x0

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAvatarRect:Landroid/graphics/Rect;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 384
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v2, :cond_10e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v2

    if-eqz v2, :cond_10e

    .line 385
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    .line 389
    :cond_10e
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v23, v0

    .line 390
    .local v23, nameHeight:I
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v16, v0

    .line 392
    .local v16, albumHeight:I
    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarWidth:I

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAvatarRightMargin:I

    add-int v25, v2, v4

    .line 393
    add-int v2, v23, v16

    sub-int v2, v20, v2

    div-int/lit8 v2, v2, 0x2

    add-int v27, v21, v2

    .line 395
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    if-eqz v2, :cond_18d

    .line 396
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    float-to-int v0, v2

    move/from16 v24, v0

    .line 398
    .local v24, ownerNameWidth:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    .line 399
    .local v3, drawText:Ljava/lang/CharSequence;
    add-int v2, v25, v24

    move/from16 v0, v26

    if-le v2, v0, :cond_171

    .line 400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    sub-int v5, v26, v25

    int-to-float v5, v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v4, v5, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 403
    :cond_171
    const/4 v4, 0x0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move/from16 v0, v25

    int-to-float v6, v0

    move/from16 v0, v27

    int-to-float v2, v0

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v8}, Landroid/text/TextPaint;->ascent()F

    move-result v8

    sub-float v7, v2, v8

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoInfoView;->sNamePaint:Landroid/text/TextPaint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 405
    add-int v27, v27, v23

    .line 408
    .end local v3           #drawText:Ljava/lang/CharSequence;
    .end local v24           #ownerNameWidth:I
    :cond_18d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumContent:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2b1

    .line 409
    sub-int v22, v26, v25

    .line 410
    .local v22, maxWidth:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1ba

    .line 411
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v19

    .line 412
    .local v19, dateWidth:F
    move/from16 v0, v22

    int-to-float v2, v0

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumRightMargin:I

    int-to-float v4, v4

    add-float v4, v4, v19

    sub-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v22, v0

    .line 415
    .end local v19           #dateWidth:F
    :cond_1ba
    sget-object v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumContent:Ljava/lang/CharSequence;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumContent:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    float-to-int v15, v2

    .line 417
    .local v15, albumContentWidth:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumContent:Ljava/lang/CharSequence;

    .line 418
    .restart local v3       #drawText:Ljava/lang/CharSequence;
    move/from16 v0, v22

    if-le v15, v0, :cond_1e5

    .line 419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumContent:Ljava/lang/CharSequence;

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    move/from16 v0, v22

    int-to-float v5, v0

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v2, v4, v5, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 423
    :cond_1e5
    move/from16 v0, v22

    invoke-static {v15, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 424
    .local v7, albumNameWidth:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v2, :cond_1fb

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getWidth()I

    move-result v2

    if-eq v2, v7, :cond_283

    .line 425
    :cond_1fb
    new-instance v4, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getContext()Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mEventId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumStream:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerId:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v2, :cond_2f8

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "eid="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_22a
    const-string v5, "<a href=\"#~loop:svt=album&amp;"

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "&amp;aname="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "&amp;oid="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\">"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "</a>"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f80

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v12, p0

    invoke-direct/range {v4 .. v12}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    .line 428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move/from16 v0, v25

    move/from16 v1, v27

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    .line 431
    :cond_283
    move/from16 v0, v25

    int-to-float v2, v0

    move/from16 v0, v27

    int-to-float v4, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 433
    move/from16 v0, v25

    neg-int v2, v0

    int-to-float v2, v2

    move/from16 v0, v27

    neg-int v4, v0

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 434
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumName:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getWidth()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumRightMargin:I

    add-int/2addr v2, v4

    add-int v25, v25, v2

    .line 437
    .end local v3           #drawText:Ljava/lang/CharSequence;
    .end local v7           #albumNameWidth:I
    .end local v15           #albumContentWidth:I
    .end local v22           #maxWidth:I
    :cond_2b1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2f3

    .line 438
    move/from16 v0, v27

    int-to-float v2, v0

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sAlbumPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v27, v0

    .line 439
    move/from16 v0, v27

    int-to-float v2, v0

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    add-float/2addr v2, v4

    float-to-int v0, v2

    move/from16 v27, v0

    .line 441
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v11

    move/from16 v0, v25

    int-to-float v12, v0

    move/from16 v0, v27

    int-to-float v2, v0

    sget-object v4, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float v13, v2, v4

    sget-object v14, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v8, p1

    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 445
    .end local v16           #albumHeight:I
    .end local v17           #avatar:Landroid/graphics/Bitmap;
    .end local v18           #countHeight:I
    .end local v21           #infoBoxY:I
    .end local v23           #nameHeight:I
    .end local v25           #xPos:I
    .end local v26           #xPosEnd:I
    .end local v27           #yPos:I
    :cond_2f3
    return-void

    .line 380
    .restart local v18       #countHeight:I
    .restart local v21       #infoBoxY:I
    .restart local v25       #xPos:I
    .restart local v26       #xPosEnd:I
    .restart local v27       #yPos:I
    :cond_2f4
    sget-object v17, Lcom/google/android/apps/plus/views/PhotoInfoView;->sDefaultAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_ea

    .line 425
    .restart local v3       #drawText:Ljava/lang/CharSequence;
    .restart local v7       #albumNameWidth:I
    .restart local v15       #albumContentWidth:I
    .restart local v16       #albumHeight:I
    .restart local v17       #avatar:Landroid/graphics/Bitmap;
    .restart local v22       #maxWidth:I
    .restart local v23       #nameHeight:I
    :cond_2f8
    if-eqz v6, :cond_30f

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "sid="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_22a

    :cond_30f
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "aid="

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_22a
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getInfoBoxHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    .line 450
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    if-lez v0, :cond_1f

    .line 451
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    const/high16 v1, -0x8000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    .line 453
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mFixedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setMeasuredDimension(II)V

    .line 457
    :goto_1e
    return-void

    .line 455
    :cond_1f
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_1e
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .registers 3
    .parameter "span"

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    if-eqz v0, :cond_9

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/views/ItemClickListener;->onSpanClick(Landroid/text/style/URLSpan;)V

    .line 239
    :cond_9
    return-void
.end method

.method public final onUserImageClick(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "gaiaId"
    .parameter "authorName"

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    if-eqz v0, :cond_9

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/plus/views/ItemClickListener;->onUserImageClick(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_9
    return-void
.end method

.method public setAlbum(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "albumId"
    .parameter "albumStream"
    .parameter "albumName"

    .prologue
    .line 139
    iput-object p3, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumContent:Ljava/lang/CharSequence;

    .line 140
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumId:Ljava/lang/String;

    .line 141
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAlbumStream:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setCommentCount(I)V
    .registers 5
    .parameter "commentCount"

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    .line 156
    .local v0, oldCommentText:Ljava/lang/String;
    if-gez p1, :cond_5

    .line 171
    :cond_4
    :goto_4
    return-void

    .line 160
    :cond_5
    if-nez p1, :cond_16

    .line 161
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    .line 168
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 169
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->invalidate()V

    goto :goto_4

    .line 162
    :cond_16
    const/16 v1, 0x63

    if-le p1, v1, :cond_28

    .line 163
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    goto :goto_a

    .line 165
    :cond_28
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mCommentText:Ljava/lang/String;

    goto :goto_a
.end method

.method public setEvent(Ljava/lang/String;)V
    .registers 2
    .parameter "eventId"

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mEventId:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setExternalClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V
    .registers 2
    .parameter "clickListener"

    .prologue
    .line 198
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 199
    return-void
.end method

.method public setOwner(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .registers 10
    .parameter "gaiaId"
    .parameter "name"

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 120
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerId:Ljava/lang/String;

    .line 121
    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    if-nez v0, :cond_20

    move-object v4, v3

    .line 124
    .local v4, nameString:Ljava/lang/String;
    :goto_b
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableUserImage;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerId:Ljava/lang/String;

    move-object v1, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    sget v1, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarWidth:I

    sget v2, Lcom/google/android/apps/plus/views/PhotoInfoView;->sInfoAvatarHeight:I

    invoke-virtual {v0, v6, v6, v1, v2}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    .line 126
    return-void

    .line 122
    .end local v4           #nameString:Ljava/lang/String;
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mOwnerName:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_b
.end method

.method public setPhotoDate(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "date"

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mDate:Ljava/lang/CharSequence;

    .line 133
    return-void
.end method

.method public setPlusOneCount(I)V
    .registers 5
    .parameter "plusOneCount"

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    .line 178
    .local v0, oldPlusOneText:Ljava/lang/String;
    if-gez p1, :cond_5

    .line 195
    :cond_4
    :goto_4
    return-void

    .line 182
    :cond_5
    if-nez p1, :cond_16

    .line 183
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    .line 192
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->invalidate()V

    goto :goto_4

    .line 185
    :cond_16
    const/16 v1, 0x63

    if-le p1, v1, :cond_28

    .line 186
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    goto :goto_a

    .line 188
    :cond_28
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mPlusOneText:Ljava/lang/String;

    goto :goto_a
.end method

.method public final showHeaderInfoView(Z)V
    .registers 4
    .parameter "animate"

    .prologue
    const/4 v1, 0x0

    .line 205
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoInfoView;->mHide:Z

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoInfoView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 208
    .local v0, currentAnimation:Landroid/view/animation/Animation;
    if-eqz v0, :cond_c

    .line 209
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 212
    :cond_c
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoInfoView;->setVisibility(I)V

    .line 218
    return-void
.end method
