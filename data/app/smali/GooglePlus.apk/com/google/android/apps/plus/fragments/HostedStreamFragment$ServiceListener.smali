.class public final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedStreamFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreatePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "result"

    .prologue
    .line 343
    if-eqz p1, :cond_19

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f08015b

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 346
    :cond_19
    return-void
.end method

.method public final onDeletePostPlusOne$63505a2b(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "result"

    .prologue
    .line 351
    if-eqz p1, :cond_19

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f08015c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 355
    :cond_19
    return-void
.end method

.method public final onGetActivities$35a362dd(IZILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 11
    .parameter "requestId"
    .parameter "newer"
    .parameter "streamLength"
    .parameter "result"

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 298
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    if-eqz p4, :cond_47

    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_47

    const/4 v0, 0x1

    :goto_e
    iput-boolean v0, v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    .line 299
    const-string v0, "HostedStreamFrag"

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 300
    const-string v0, "HostedStreamFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onGetActivities - mError="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :cond_30
    if-eqz p2, :cond_7e

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_49

    .line 338
    :cond_46
    :goto_46
    return-void

    :cond_47
    move v0, v1

    .line 298
    goto :goto_e

    .line 309
    :cond_49
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 326
    :cond_4d
    :goto_4d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_5c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v0, :cond_73

    .line 327
    :cond_5c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-nez v0, :cond_67

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    #setter for: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$002(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;I)I

    .line 332
    :cond_67
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0, v1, v4, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 336
    :cond_73
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    goto :goto_46

    .line 311
    :cond_7e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v0, :cond_4d

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/HostedStreamFragment;

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    .line 320
    const-string v0, "HostedStreamFrag"

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 321
    const-string v0, "HostedStreamFrag"

    const-string v1, "onGetActivities - mPreloadRequested=false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4d
.end method
