.class public final Lcom/google/android/apps/plus/oob/OutOfBoxMessages;
.super Ljava/lang/Object;
.source "OutOfBoxMessages.java"


# direct methods
.method public static areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter "o1"
    .parameter "o2"

    .prologue
    .line 76
    if-nez p0, :cond_8

    .line 77
    if-nez p1, :cond_6

    const/4 v0, 0x1

    .line 79
    :goto_5
    return v0

    .line 77
    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    .line 79
    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public static copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .registers 3
    .parameter "input"

    .prologue
    .line 57
    if-nez p0, :cond_4

    .line 58
    const/4 v0, 0x0

    .line 69
    :goto_3
    return-object v0

    .line 60
    :cond_4
    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxInputField;-><init>()V

    .line 61
    .local v0, newInput:Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->id:Ljava/lang/String;

    .line 62
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    .line 64
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    .line 65
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    .line 66
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->valueOption:Ljava/util/List;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->valueOption:Ljava/util/List;

    .line 67
    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->helpText:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->helpText:Ljava/lang/String;

    goto :goto_3
.end method
