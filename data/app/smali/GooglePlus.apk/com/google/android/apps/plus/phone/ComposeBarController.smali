.class public final Lcom/google/android/apps/plus/phone/ComposeBarController;
.super Ljava/lang/Object;
.source "ComposeBarController.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;


# static fields
.field private static sCumulativeTouchSlop:I


# instance fields
.field private mCurrentOffset:F

.field private mCurrentTouchDelta:I

.field private mLandscape:Z

.field private final mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

.field private mState:I

.field private mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Z)V
    .registers 5
    .parameter "view"
    .parameter "landscape"

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    .line 38
    new-instance v0, Lcom/google/android/apps/plus/phone/ComposeBarController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/ComposeBarController$1;-><init>(Lcom/google/android/apps/plus/phone/ComposeBarController;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    .line 82
    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    .line 84
    sget v0, Lcom/google/android/apps/plus/phone/ComposeBarController;->sCumulativeTouchSlop:I

    if-nez v0, :cond_23

    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d01c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/google/android/apps/plus/phone/ComposeBarController;->sCumulativeTouchSlop:I

    .line 88
    :cond_23
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/ComposeBarController;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/ComposeBarController;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 18
    iput p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/ComposeBarController;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateVisibility()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/phone/ComposeBarController;F)F
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 18
    iput p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/ComposeBarController;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 18
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/ComposeBarController;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    return-object v0
.end method

.method private startAnimation(FI)V
    .registers 6
    .parameter "finalDelta"
    .parameter "time"

    .prologue
    const/4 v2, 0x0

    .line 183
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v1, :cond_21

    .line 184
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    invoke-direct {v0, v1, p1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 188
    .local v0, anim:Landroid/view/animation/Animation;
    :goto_c
    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 189
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 190
    if-lez p2, :cond_1b

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 193
    :cond_1b
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 194
    return-void

    .line 186
    .end local v0           #anim:Landroid/view/animation/Animation;
    :cond_21
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    invoke-direct {v0, v2, v2, v1, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v0       #anim:Landroid/view/animation/Animation;
    goto :goto_c
.end method

.method private updateVisibility()V
    .registers 4

    .prologue
    .line 198
    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    if-nez v1, :cond_37

    .line 199
    const/4 v0, 0x0

    .line 209
    .local v0, visible:Z
    :goto_5
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    if-eqz v0, :cond_6f

    const/4 v1, 0x0

    :goto_a
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 212
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    const v2, 0x7f09006a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    const v2, 0x7f09006b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    const v2, 0x7f09006c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 216
    return-void

    .line 201
    .end local v0           #visible:Z
    :cond_37
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v1, :cond_51

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    if-lez v1, :cond_51

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-lez v1, :cond_6b

    :cond_51
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-nez v1, :cond_6d

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-lez v1, :cond_6d

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_6d

    .line 203
    :cond_6b
    const/4 v0, 0x0

    .restart local v0       #visible:Z
    goto :goto_5

    .line 205
    .end local v0           #visible:Z
    :cond_6d
    const/4 v0, 0x1

    .restart local v0       #visible:Z
    goto :goto_5

    .line 209
    :cond_6f
    const/16 v1, 0x8

    goto :goto_a
.end method


# virtual methods
.method public final onScroll(Lcom/google/android/apps/plus/views/ColumnGridView;IIIII)V
    .registers 15
    .parameter "view"
    .parameter "firstItem"
    .parameter "visibleOffset"
    .parameter "viewItemCount"
    .parameter "totalItemCount"
    .parameter "scrollDelta"

    .prologue
    .line 133
    if-lt p3, p4, :cond_3

    .line 179
    :goto_2
    return-void

    .line 137
    :cond_3
    if-gez p6, :cond_9

    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    if-gtz v6, :cond_f

    :cond_9
    if-lez p6, :cond_12

    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    if-gez v6, :cond_12

    .line 139
    :cond_f
    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    .line 141
    :cond_12
    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    add-int/2addr v6, p6

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    .line 143
    add-int v2, p2, p3

    .line 144
    .local v2, firstVisibleIndex:I
    invoke-virtual {p1, p3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 145
    .local v1, firstView:Landroid/view/View;
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v6, :cond_55

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v6

    neg-int v0, v6

    .line 146
    .local v0, currentScrollOffset:I
    :goto_26
    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    if-lez v6, :cond_5b

    const/4 v4, 0x1

    .line 148
    .local v4, scrolledBack:Z
    :goto_2b
    if-nez v2, :cond_68

    .line 149
    const/4 v6, 0x2

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    .line 151
    move v5, v0

    .line 152
    .local v5, translation:I
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v6, :cond_5d

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 153
    .local v3, maxTranslation:I
    :goto_3b
    if-le v5, v3, :cond_64

    .line 154
    move v5, v3

    .line 159
    :cond_3e
    :goto_3e
    if-eqz v4, :cond_47

    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_51

    .line 160
    :cond_47
    neg-int v6, v5

    int-to-float v6, v6

    const/4 v7, 0x0

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    .line 161
    neg-int v6, v5

    int-to-float v6, v6

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    .line 178
    .end local v3           #maxTranslation:I
    .end local v5           #translation:I
    :cond_51
    :goto_51
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateVisibility()V

    goto :goto_2

    .line 145
    .end local v0           #currentScrollOffset:I
    .end local v4           #scrolledBack:Z
    :cond_55
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    neg-int v0, v6

    goto :goto_26

    .line 146
    .restart local v0       #currentScrollOffset:I
    :cond_5b
    const/4 v4, 0x0

    goto :goto_2b

    .line 152
    .restart local v4       #scrolledBack:Z
    .restart local v5       #translation:I
    :cond_5d
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v3

    goto :goto_3b

    .line 155
    .restart local v3       #maxTranslation:I
    :cond_64
    if-gez v5, :cond_3e

    .line 156
    const/4 v5, 0x0

    goto :goto_3e

    .line 163
    .end local v3           #maxTranslation:I
    .end local v5           #translation:I
    :cond_68
    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    sget v7, Lcom/google/android/apps/plus/phone/ComposeBarController;->sCumulativeTouchSlop:I

    neg-int v7, v7

    if-le v6, v7, :cond_75

    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    sget v7, Lcom/google/android/apps/plus/phone/ComposeBarController;->sCumulativeTouchSlop:I

    if-lt v6, v7, :cond_51

    .line 165
    :cond_75
    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    if-eqz v6, :cond_7e

    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_87

    :cond_7e
    if-eqz v4, :cond_87

    .line 166
    const/4 v6, 0x0

    const/16 v7, 0xc8

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    goto :goto_51

    .line 167
    :cond_87
    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_91

    iget v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_51

    :cond_91
    if-nez v4, :cond_51

    .line 168
    iget-boolean v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v6, :cond_af

    .line 169
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    const/16 v7, 0xc8

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    .line 170
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    goto :goto_51

    .line 172
    :cond_af
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    const/16 v7, 0xc8

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    .line 173
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    neg-int v6, v6

    int-to-float v6, v6

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    goto :goto_51
.end method

.method public final onScrollStateChanged(Lcom/google/android/apps/plus/views/ColumnGridView;I)V
    .registers 11
    .parameter "view"
    .parameter "scrollState"

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 92
    const/4 v4, 0x1

    if-eq p2, v4, :cond_7

    .line 93
    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    .line 96
    :cond_7
    if-eq p2, v7, :cond_43

    .line 101
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_43

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getFirstVisiblePosition()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getVisibleOffset()I

    move-result v5

    add-int/2addr v4, v5

    if-nez v4, :cond_43

    .line 103
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 104
    .local v1, firstView:Landroid/view/View;
    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v4, :cond_44

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    neg-int v0, v4

    .line 107
    .local v0, currentScrollOffset:I
    :goto_27
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    .line 108
    iput v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    .line 110
    move v3, v0

    .line 111
    .local v3, translation:I
    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v4, :cond_4a

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 112
    .local v2, maxTranslation:I
    :goto_39
    if-le v3, v2, :cond_51

    .line 113
    move v3, v2

    .line 119
    :cond_3c
    :goto_3c
    neg-int v4, v3

    int-to-float v4, v4

    iput v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateVisibility()V

    .line 124
    .end local v0           #currentScrollOffset:I
    .end local v1           #firstView:Landroid/view/View;
    .end local v2           #maxTranslation:I
    .end local v3           #translation:I
    :cond_43
    return-void

    .line 104
    .restart local v1       #firstView:Landroid/view/View;
    :cond_44
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    neg-int v0, v4

    goto :goto_27

    .line 111
    .restart local v0       #currentScrollOffset:I
    .restart local v3       #translation:I
    :cond_4a
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v2

    goto :goto_39

    .line 114
    .restart local v2       #maxTranslation:I
    :cond_51
    if-gez v3, :cond_3c

    .line 115
    const/4 v3, 0x0

    goto :goto_3c
.end method
