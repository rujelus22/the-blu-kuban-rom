.class public Lcom/google/android/apps/plus/views/OneUpBaseView;
.super Landroid/view/View;
.source "OneUpBaseView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;
    }
.end annotation


# instance fields
.field mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method


# virtual methods
.method public onStart()V
    .registers 1

    .prologue
    .line 58
    return-void
.end method

.method public onStop()V
    .registers 1

    .prologue
    .line 64
    return-void
.end method

.method public setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V
    .registers 2
    .parameter "measuredListener"

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpBaseView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    .line 52
    return-void
.end method
