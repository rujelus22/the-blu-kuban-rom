.class final Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;
.super Landroid/support/v4/util/LruCache;
.source "EsFragmentPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FragmentCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/support/v4/app/Fragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;I)V
    .registers 4
    .parameter
    .parameter "size"

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;->this$0:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;

    .line 188
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Landroid/support/v4/util/LruCache;-><init>(I)V

    .line 189
    return-void
.end method


# virtual methods
.method protected final bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 186
    check-cast p3, Landroid/support/v4/app/Fragment;

    .end local p3
    check-cast p4, Landroid/support/v4/app/Fragment;

    .end local p4
    if-nez p1, :cond_a

    if-eqz p4, :cond_13

    if-eq p3, p4, :cond_13

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;->this$0:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;

    #getter for: Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Landroid/support/v4/app/FragmentTransaction;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->access$000(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_13
    return-void
.end method
