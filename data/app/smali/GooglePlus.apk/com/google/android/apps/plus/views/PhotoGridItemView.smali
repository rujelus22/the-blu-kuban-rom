.class public Lcom/google/android/apps/plus/views/PhotoGridItemView;
.super Landroid/view/View;
.source "PhotoGridItemView.java"

# interfaces
.implements Landroid/graphics/drawable/Drawable$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;,
        Lcom/google/android/apps/plus/views/PhotoGridItemView$OnMeasuredListener;,
        Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;
    }
.end annotation


# static fields
.field private static sCountLeftPadding:I

.field private static sCountPaint:Landroid/text/TextPaint;

.field private static sCountRightPadding:I

.field private static sInfoHeight:I

.field private static sInfoPaint:Landroid/graphics/Paint;

.field private static sInitialized:Z

.field private static sItemHeight:I

.field private static sItemInnerPadding:I

.field private static sLoadingImage:Landroid/graphics/Bitmap;

.field private static sNameLeftPadding:I

.field private static sNamePaint:Landroid/text/TextPaint;

.field private static sNameRightPadding:I


# instance fields
.field private mAlbumCountText:Ljava/lang/CharSequence;

.field private mAlbumCountWidth:I

.field private mAlbumNameText:Ljava/lang/CharSequence;

.field private mHandler:Landroid/os/Handler;

.field private final mLoadingRect:[Landroid/graphics/Rect;

.field private mMaxCoverCount:I

.field private mMeasuredListener:Lcom/google/android/apps/plus/views/PhotoGridItemView$OnMeasuredListener;

.field private final mPhoto:[Landroid/graphics/Bitmap;

.field private mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

.field private final mPhotoRect:[Landroid/graphics/Rect;

.field private final mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter "context"
    .parameter "attrs"

    .prologue
    const v5, 0x7f0d003f

    const v4, 0x7f0d003e

    const/4 v3, 0x1

    .line 110
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mHandler:Landroid/os/Handler;

    .line 90
    new-array v0, v3, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhoto:[Landroid/graphics/Bitmap;

    .line 91
    new-array v0, v3, [Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    .line 92
    new-array v0, v3, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    .line 93
    new-array v0, v3, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    .line 111
    sget-boolean v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInitialized:Z

    if-nez v0, :cond_d9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020104

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sLoadingImage:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    const v2, 0x7f0a002c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountPaint:Landroid/text/TextPaint;

    const v2, 0x7f0a002d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountPaint:Landroid/text/TextPaint;

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInfoPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInfoPaint:Landroid/graphics/Paint;

    const v2, 0x7f0a002b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    const v1, 0x7f0d0041

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNameLeftPadding:I

    const v1, 0x7f0d0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNameRightPadding:I

    const v1, 0x7f0d0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountLeftPadding:I

    const v1, 0x7f0d0044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountRightPadding:I

    const v1, 0x7f0d003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    const v1, 0x7f0d0040

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInfoHeight:I

    const v1, 0x7f0d003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    sput-boolean v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInitialized:Z

    .line 112
    :cond_d9
    return-void
.end method

.method private createPhotoRect()V
    .registers 35

    .prologue
    .line 386
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getMeasuredWidth()I

    move-result v18

    .line 389
    .local v18, measuredWidth:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_5
    if-gtz v6, :cond_2e

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    aput-object v27, v25, v26

    .line 391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    aput-object v27, v25, v26

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    aput-object v27, v25, v26

    .line 389
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 395
    :cond_2e
    if-nez v18, :cond_31

    .line 521
    :goto_30
    return-void

    .line 400
    :cond_31
    sget-object v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sLoadingImage:Landroid/graphics/Bitmap;

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    .line 401
    .local v14, loadingHeight:I
    sget-object v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sLoadingImage:Landroid/graphics/Bitmap;

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .line 402
    .local v17, loadingWidth:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMaxCoverCount:I

    move/from16 v25, v0

    packed-switch v25, :pswitch_data_432

    goto :goto_30

    .line 404
    :pswitch_47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v18

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v27, v25, v26

    .line 405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    const/16 v29, 0x0

    sget v30, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v18

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 407
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    sub-int v25, v25, v14

    div-int/lit8 v16, v25, 0x2

    .line 408
    .local v16, loadingTop:I
    sub-int v25, v18, v17

    div-int/lit8 v15, v25, 0x2

    .line 409
    .local v15, loadingLeft:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v15, v17

    add-int v29, v16, v14

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v15, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    goto :goto_30

    .line 424
    .end local v15           #loadingLeft:I
    .end local v16           #loadingTop:I
    :pswitch_a3
    move/from16 v0, v18

    mul-int/lit16 v0, v0, 0x29b

    move/from16 v25, v0

    move/from16 v0, v25

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v25, v0

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v23, v25, v26

    .line 425
    .local v23, width1:I
    sub-int v25, v18, v23

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v24, v25, v26

    .line 426
    .local v24, width2:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v19, v23, v25

    .line 427
    .local v19, photo2Left:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v27, v25, v26

    .line 428
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    new-instance v27, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v24

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v27, v25, v26

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    const/16 v29, 0x0

    sget v30, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v23

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    sget v29, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v19

    move/from16 v2, v28

    move/from16 v3, v18

    move/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 432
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    sub-int v25, v25, v14

    div-int/lit8 v16, v25, 0x2

    .line 433
    .restart local v16       #loadingTop:I
    sub-int v25, v23, v17

    div-int/lit8 v7, v25, 0x2

    .line 434
    .local v7, loading1Left:I
    sub-int v25, v24, v17

    div-int/lit8 v25, v25, 0x2

    add-int v9, v19, v25

    .line 435
    .local v9, loading2Left:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v7, v17

    add-int v29, v16, v14

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v7, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v9, v17

    add-int v29, v16, v14

    move-object/from16 v0, v27

    move/from16 v1, v16

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v9, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    goto/16 :goto_30

    .line 454
    .end local v7           #loading1Left:I
    .end local v9           #loading2Left:I
    .end local v16           #loadingTop:I
    .end local v19           #photo2Left:I
    .end local v23           #width1:I
    .end local v24           #width2:I
    :pswitch_16f
    move/from16 v0, v18

    mul-int/lit16 v0, v0, 0x29b

    move/from16 v25, v0

    move/from16 v0, v25

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v25, v0

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v23, v25, v26

    .line 455
    .restart local v23       #width1:I
    sub-int v25, v18, v23

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v24, v25, v26

    .line 456
    .restart local v24       #width2:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    div-int/lit8 v25, v25, 0x2

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v5, v25, v26

    .line 457
    .local v5, height2:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v19, v23, v25

    .line 458
    .restart local v19       #photo2Left:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v21, v5, v25

    .line 459
    .local v21, photo3Top:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v27, v25, v26

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    new-instance v29, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v0, v29

    move/from16 v1, v24

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v29, v27, v28

    aput-object v29, v25, v26

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    const/16 v29, 0x0

    sget v30, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v23

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v19

    move/from16 v2, v28

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    new-instance v27, Landroid/graphics/Rect;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v19

    move/from16 v2, v21

    move/from16 v3, v18

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 465
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    sub-int v25, v25, v14

    div-int/lit8 v8, v25, 0x2

    .line 466
    .local v8, loading1Top:I
    sub-int v25, v5, v14

    div-int/lit8 v10, v25, 0x2

    .line 467
    .local v10, loading2Top:I
    add-int v25, v10, v5

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v12, v25, v26

    .line 468
    .local v12, loading3Top:I
    sub-int v25, v23, v17

    div-int/lit8 v7, v25, 0x2

    .line 469
    .restart local v7       #loading1Left:I
    sub-int v25, v24, v17

    div-int/lit8 v25, v25, 0x2

    add-int v9, v19, v25

    .line 470
    .restart local v9       #loading2Left:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v7, v17

    add-int v29, v8, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v7, v8, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v9, v17

    add-int v29, v10, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v9, v10, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v9, v17

    add-int v29, v12, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v9, v12, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    goto/16 :goto_30

    .line 489
    .end local v5           #height2:I
    .end local v7           #loading1Left:I
    .end local v8           #loading1Top:I
    .end local v9           #loading2Left:I
    .end local v10           #loading2Top:I
    .end local v12           #loading3Top:I
    .end local v19           #photo2Left:I
    .end local v21           #photo3Top:I
    .end local v23           #width1:I
    .end local v24           #width2:I
    :pswitch_283
    move/from16 v0, v18

    mul-int/lit16 v0, v0, 0x29b

    move/from16 v25, v0

    move/from16 v0, v25

    div-int/lit16 v0, v0, 0x3e8

    move/from16 v25, v0

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v23, v25, v26

    .line 490
    .restart local v23       #width1:I
    sub-int v25, v18, v23

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v25, v25, v26

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v25, v25, v26

    div-int/lit8 v24, v25, 0x2

    .line 492
    .restart local v24       #width2:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    div-int/lit8 v25, v25, 0x2

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    sub-int v5, v25, v26

    .line 493
    .restart local v5       #height2:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v19, v23, v25

    .line 494
    .restart local v19       #photo2Left:I
    add-int v25, v19, v24

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v20, v25, v26

    .line 495
    .local v20, photo3Left:I
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v22, v5, v25

    .line 496
    .local v22, photo4Top:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v23

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v27, v25, v26

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v29, v0

    const/16 v30, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoSize:[Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v31, v0

    const/16 v32, 0x4

    new-instance v33, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    move-object/from16 v0, v33

    move/from16 v1, v24

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;-><init>(II)V

    aput-object v33, v31, v32

    aput-object v33, v29, v30

    aput-object v33, v27, v28

    aput-object v33, v25, v26

    .line 499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    const/16 v29, 0x0

    sget v30, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v23

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 500
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    add-int v29, v19, v24

    move-object/from16 v0, v27

    move/from16 v1, v19

    move/from16 v2, v28

    move/from16 v3, v29

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    new-instance v27, Landroid/graphics/Rect;

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v20

    move/from16 v2, v28

    move/from16 v3, v18

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x3

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v19, v24

    sget v29, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v19

    move/from16 v2, v22

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x4

    new-instance v27, Landroid/graphics/Rect;

    sget v28, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    move-object/from16 v0, v27

    move/from16 v1, v20

    move/from16 v2, v22

    move/from16 v3, v18

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 505
    sget v25, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    sub-int v25, v25, v14

    div-int/lit8 v8, v25, 0x2

    .line 506
    .restart local v8       #loading1Top:I
    sub-int v25, v5, v14

    div-int/lit8 v10, v25, 0x2

    .line 507
    .restart local v10       #loading2Top:I
    add-int v25, v10, v5

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v13, v25, v26

    .line 508
    .local v13, loading4Top:I
    sub-int v25, v23, v17

    div-int/lit8 v7, v25, 0x2

    .line 509
    .restart local v7       #loading1Left:I
    sub-int v25, v24, v17

    div-int/lit8 v25, v25, 0x2

    add-int v9, v19, v25

    .line 510
    .restart local v9       #loading2Left:I
    add-int v25, v9, v24

    sget v26, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemInnerPadding:I

    add-int v11, v25, v26

    .line 511
    .local v11, loading3Left:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v7, v17

    add-int v29, v8, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v7, v8, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    new-instance v29, Landroid/graphics/Rect;

    add-int v30, v9, v17

    add-int v31, v10, v14

    move-object/from16 v0, v29

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v9, v10, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v29, v27, v28

    aput-object v29, v25, v26

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x2

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v11, v17

    add-int v29, v10, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v11, v10, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x3

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v9, v17

    add-int v29, v13, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v9, v13, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    .line 519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    move-object/from16 v25, v0

    const/16 v26, 0x4

    new-instance v27, Landroid/graphics/Rect;

    add-int v28, v11, v17

    add-int v29, v13, v14

    move-object/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v11, v13, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    aput-object v27, v25, v26

    goto/16 :goto_30

    .line 402
    nop

    :pswitch_data_432
    .packed-switch 0x1
        :pswitch_47
        :pswitch_a3
        :pswitch_16f
        :pswitch_16f
        :pswitch_283
    .end packed-switch
.end method


# virtual methods
.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 2
    .parameter "who"

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->invalidate()V

    .line 119
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 23
    .parameter "canvas"

    .prologue
    .line 342
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getMeasuredWidth()I

    move-result v18

    .line 343
    .local v18, measuredWidth:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    if-eqz v1, :cond_2b

    .line 348
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    .local v13, arr$:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;
    array-length v0, v13

    move/from16 v16, v0

    .local v16, len$:I
    const/4 v15, 0x0

    .local v15, i$:I
    :goto_12
    move/from16 v0, v16

    if-ge v15, v0, :cond_2b

    aget-object v14, v13, v15

    .line 349
    .local v14, drawableInfo:Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;
    if-eqz v14, :cond_28

    .line 350
    iget-object v1, v14, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 351
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v14, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawn:Ljava/lang/Boolean;

    .line 348
    :cond_28
    add-int/lit8 v15, v15, 0x1

    goto :goto_12

    .line 357
    .end local v13           #arr$:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;
    .end local v14           #drawableInfo:Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;
    .end local v15           #i$:I
    .end local v16           #len$:I
    :cond_2b
    sget v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    sget v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInfoHeight:I

    sub-int v20, v1, v3

    .line 358
    .local v20, yPos:I
    const/4 v2, 0x0

    move/from16 v0, v20

    int-to-float v3, v0

    move/from16 v0, v18

    int-to-float v4, v0

    sget v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    int-to-float v5, v1

    sget-object v6, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInfoPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 361
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float v19, v1, v3

    .line 362
    .local v19, textHeight:F
    move/from16 v0, v20

    int-to-float v1, v0

    sget v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sInfoHeight:I

    int-to-float v3, v3

    sub-float v3, v3, v19

    const/high16 v4, 0x4000

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    float-to-int v0, v1

    move/from16 v20, v0

    .line 363
    move/from16 v0, v20

    int-to-float v1, v0

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v1, v3

    float-to-int v0, v1

    move/from16 v20, v0

    .line 365
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumNameText:Ljava/lang/CharSequence;

    if-eqz v1, :cond_a6

    .line 366
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountWidth:I

    sub-int v1, v18, v1

    sget v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNameLeftPadding:I

    sub-int/2addr v1, v3

    sget v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNameRightPadding:I

    sub-int/2addr v1, v3

    sget v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountLeftPadding:I

    sub-int/2addr v1, v3

    sget v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountRightPadding:I

    sub-int v17, v1, v3

    .line 368
    .local v17, maxTextWidth:I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumNameText:Ljava/lang/CharSequence;

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    move/from16 v0, v17

    int-to-float v4, v0

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v3, v4, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 370
    .local v2, drawText:Ljava/lang/CharSequence;
    sget v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNameLeftPadding:I

    int-to-float v5, v1

    .line 372
    .local v5, drawX:F
    const/4 v3, 0x0

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    move/from16 v0, v20

    int-to-float v6, v0

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sNamePaint:Landroid/text/TextPaint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 375
    .end local v2           #drawText:Ljava/lang/CharSequence;
    .end local v5           #drawX:F
    .end local v17           #maxTextWidth:I
    :cond_a6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    if-eqz v1, :cond_ce

    .line 376
    sget v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountRightPadding:I

    sub-int v1, v18, v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountWidth:I

    sub-int/2addr v1, v3

    int-to-float v5, v1

    .line 377
    .restart local v5       #drawX:F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v9

    move/from16 v0, v20

    int-to-float v11, v0

    sget-object v12, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountPaint:Landroid/text/TextPaint;

    move-object/from16 v6, p1

    move v10, v5

    invoke-virtual/range {v6 .. v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 379
    .end local v5           #drawX:F
    :cond_ce
    return-void
.end method

.method protected onMeasure(II)V
    .registers 7
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 268
    sget v2, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    const/high16 v3, 0x4000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 269
    .local v0, fixedHeightSpec:I
    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getMeasuredWidth()I

    move-result v1

    .line 272
    .local v1, measuredWidth:I
    sget v2, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sItemHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->setMeasuredDimension(II)V

    .line 274
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->createPhotoRect()V

    .line 276
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMeasuredListener:Lcom/google/android/apps/plus/views/PhotoGridItemView$OnMeasuredListener;

    if-eqz v2, :cond_1d

    .line 277
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMeasuredListener:Lcom/google/android/apps/plus/views/PhotoGridItemView$OnMeasuredListener;

    .line 279
    :cond_1d
    return-void
.end method

.method public scheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;J)V
    .registers 6
    .parameter "who"
    .parameter "what"
    .parameter "when"

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p2, p2, p3, p4}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 124
    return-void
.end method

.method public setAlbumCount(Ljava/lang/Integer;)V
    .registers 7
    .parameter "albumCount"

    .prologue
    const/4 v4, 0x0

    .line 144
    if-nez p1, :cond_c

    .line 145
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    .line 146
    iput v4, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountWidth:I

    .line 159
    :goto_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->invalidate()V

    .line 160
    return-void

    .line 148
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 149
    .local v0, res:Landroid/content/res/Resources;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_31

    .line 150
    const v1, 0x7f080078

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    .line 155
    :goto_1f
    sget-object v1, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sCountPaint:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-virtual {v1, v2, v4, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountWidth:I

    goto :goto_8

    .line 152
    :cond_31
    const/high16 v1, 0x7f0e

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumCountText:Ljava/lang/CharSequence;

    goto :goto_1f
.end method

.method public setAlbumName(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "name"

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mAlbumNameText:Ljava/lang/CharSequence;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->invalidate()V

    .line 138
    return-void
.end method

.method public setCoverCount(II)V
    .registers 5
    .parameter "coverCount"
    .parameter "albumCount"

    .prologue
    const/4 v1, 0x1

    .line 167
    if-ltz p1, :cond_5

    if-le p1, v1, :cond_6

    .line 189
    :cond_5
    :goto_5
    return-void

    .line 175
    :cond_6
    if-eqz p2, :cond_34

    .line 176
    const/16 v0, 0xa

    if-ge p2, v0, :cond_1c

    .line 178
    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMaxCoverCount:I

    .line 187
    :goto_12
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMaxCoverCount:I

    new-array v0, v0, [Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    .line 188
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->createPhotoRect()V

    goto :goto_5

    .line 179
    :cond_1c
    const/16 v0, 0x14

    if-ge p2, v0, :cond_28

    .line 180
    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMaxCoverCount:I

    goto :goto_12

    .line 181
    :cond_28
    const/16 v0, 0x32

    if-ge p2, v0, :cond_34

    .line 182
    const/4 v0, 0x3

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMaxCoverCount:I

    goto :goto_12

    .line 184
    :cond_34
    const/4 v0, 0x5

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMaxCoverCount:I

    goto :goto_12
.end method

.method public setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 4
    .parameter "refs"

    .prologue
    .line 229
    if-eqz p1, :cond_7

    array-length v0, p1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_7

    .line 233
    :goto_6
    return-void

    .line 232
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->setTag(Ljava/lang/Object;)V

    goto :goto_6
.end method

.method public setOnMeasureListener(Lcom/google/android/apps/plus/views/PhotoGridItemView$OnMeasuredListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mMeasuredListener:Lcom/google/android/apps/plus/views/PhotoGridItemView$OnMeasuredListener;

    .line 259
    return-void
.end method

.method public setPhoto(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;)V
    .registers 16
    .parameter "ref"
    .parameter "photo"

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/api/MediaRef;

    .line 199
    .local v0, allRefs:[Lcom/google/android/apps/plus/api/MediaRef;
    if-nez v0, :cond_b

    .line 220
    :cond_a
    :goto_a
    return-void

    .line 204
    :cond_b
    const/4 v3, 0x0

    .line 205
    .local v3, invalidate:Z
    const/4 v6, 0x0

    .line 206
    .local v6, position:I
    move-object v1, v0

    .local v1, arr$:[Lcom/google/android/apps/plus/api/MediaRef;
    array-length v5, v0

    .local v5, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_10
    if-ge v2, v5, :cond_c1

    aget-object v4, v1, v2

    .line 207
    .local v4, itemRef:Lcom/google/android/apps/plus/api/MediaRef;
    if-eqz p1, :cond_1c

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_95

    .line 208
    :cond_1c
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhoto:[Landroid/graphics/Bitmap;

    aput-object p2, v7, v6

    .line 209
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoRect:[Landroid/graphics/Rect;

    aget-object v8, v7, v6

    if-eqz v8, :cond_94

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhoto:[Landroid/graphics/Bitmap;

    aget-object v9, v7, v6

    const/4 v7, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aget-object v10, v10, v6

    if-eqz v10, :cond_3d

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aget-object v10, v10, v6

    iget-object v10, v10, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mLoadingDrawable:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_9b

    :cond_3d
    if-eqz v9, :cond_9b

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-direct {v7, v10, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aget-object v8, v8, v6

    if-eqz v8, :cond_7a

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aget-object v8, v8, v6

    iget-object v8, v8, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawn:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_7a

    const/4 v8, 0x2

    new-array v8, v8, [Landroid/graphics/drawable/Drawable;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aget-object v9, v9, v6

    iget-object v9, v9, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawable:Landroid/graphics/drawable/Drawable;

    aput-object v9, v8, v11

    aput-object v7, v8, v12

    new-instance v7, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v7, v8}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, p0}, Landroid/graphics/drawable/TransitionDrawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {v7, v12}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    const/16 v8, 0xfa

    invoke-virtual {v7, v8}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_7a
    new-instance v8, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    invoke-direct {v8, v11}, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;-><init>(B)V

    iput-object v7, v8, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iput-object v7, v8, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mLoadingDrawable:Ljava/lang/Boolean;

    move-object v7, v8

    :cond_88
    :goto_88
    if-eqz v7, :cond_94

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawn:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aput-object v7, v8, v6

    .line 210
    :cond_94
    const/4 v3, 0x1

    .line 213
    :cond_95
    add-int/lit8 v6, v6, 0x1

    .line 206
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_10

    .line 209
    :cond_9b
    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mPhotoDrawables:[Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    aget-object v8, v8, v6

    if-nez v8, :cond_88

    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoGridItemView;->sLoadingImage:Landroid/graphics/Bitmap;

    invoke-direct {v8, v7, v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mLoadingRect:[Landroid/graphics/Rect;

    aget-object v7, v7, v6

    invoke-virtual {v8, v7}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    new-instance v7, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;

    invoke-direct {v7, v11}, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;-><init>(B)V

    iput-object v8, v7, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    iput-object v8, v7, Lcom/google/android/apps/plus/views/PhotoGridItemView$DrawableInfo;->mLoadingDrawable:Ljava/lang/Boolean;

    goto :goto_88

    .line 217
    .end local v4           #itemRef:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_c1
    if-eqz v3, :cond_a

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoGridItemView;->invalidate()V

    goto/16 :goto_a
.end method

.method public unscheduleDrawable(Landroid/graphics/drawable/Drawable;Ljava/lang/Runnable;)V
    .registers 4
    .parameter "who"
    .parameter "what"

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p2, p1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 129
    return-void
.end method
