.class final Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;
.super Landroid/os/AsyncTask;
.source "PhotoViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$toastError:Ljava/lang/String;

.field final synthetic val$toastSuccess:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1283
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->val$toastSuccess:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->val$toastError:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs doInBackground([Landroid/graphics/Bitmap;)Ljava/lang/Boolean;
    .registers 6
    .parameter "bitmaps"

    .prologue
    .line 1294
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v1

    .line 1296
    .local v1, manager:Landroid/app/WallpaperManager;
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v1, v2}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1298
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_12} :catch_13

    .line 1302
    .end local v1           #manager:Landroid/app/WallpaperManager;
    :goto_12
    return-object v2

    .line 1299
    :catch_13
    move-exception v0

    .line 1300
    .local v0, e:Ljava/io/IOException;
    const-string v2, "PhotoViewFragment"

    const-string v3, "Exception setting wallpaper"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1302
    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_12
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 1283
    check-cast p1, [Landroid/graphics/Bitmap;

    .end local p1
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->doInBackground([Landroid/graphics/Bitmap;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5
    .parameter "x0"

    .prologue
    .line 1283
    check-cast p1, Ljava/lang/Boolean;

    .end local p1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->val$toastSuccess:Ljava/lang/String;

    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->val$activity:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hideProgressDialog()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$500(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V

    return-void

    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->val$toastError:Ljava/lang/String;

    goto :goto_a
.end method
