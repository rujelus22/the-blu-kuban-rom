.class public final Lcom/google/android/apps/plus/phone/SignOnManager;
.super Ljava/lang/Object;
.source "SignOnManager.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mActivity:Landroid/support/v4/app/FragmentActivity;

.field private final mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mIntent:Landroid/content/Intent;

.field private mIsResumed:Z

.field private mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/SignOnManager;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getPendingInstantUploadCount()I

    move-result v0

    return v0
.end method

.method private doSignOut(Z)V
    .registers 7
    .parameter "downgrading"

    .prologue
    .line 338
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 339
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    .line 340
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_25

    .line 341
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 344
    :cond_25
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 346
    if-nez p1, :cond_44

    .line 347
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 348
    .local v0, startIntent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 351
    .end local v0           #startIntent:Landroid/content/Intent;
    :cond_44
    return-void
.end method

.method private getPendingInstantUploadCount()I
    .registers 16

    .prologue
    const/4 v2, 0x0

    .line 357
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 360
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->instantUploadUri:Landroid/net/Uri;

    .local v1, iuQueryUri:Landroid/net/Uri;
    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    .line 363
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 365
    .local v14, iuCursor:Landroid/database/Cursor;
    const/4 v13, 0x0

    .line 367
    .local v13, instantUploadCount:I
    if-eqz v14, :cond_1e

    :try_start_13
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 368
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1d
    .catchall {:try_start_13 .. :try_end_1d} :catchall_5e

    move-result v13

    .line 371
    :cond_1e
    if-eqz v14, :cond_23

    .line 372
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 377
    :cond_23
    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->uploadAllUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    .line 378
    .local v9, builder:Landroid/net/Uri$Builder;
    const-string v3, "account"

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 379
    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .local v4, duQueryUri:Landroid/net/Uri;
    move-object v3, v0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    .line 381
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 383
    .local v12, duCursor:Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 384
    .local v11, demandUploadProgress:I
    const/4 v10, 0x0

    .line 386
    .local v10, demandUploadCount:I
    if-eqz v12, :cond_55

    :try_start_45
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_55

    .line 387
    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 388
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_54
    .catchall {:try_start_45 .. :try_end_54} :catchall_65

    move-result v10

    .line 391
    :cond_55
    if-eqz v12, :cond_5a

    .line 392
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 396
    :cond_5a
    sub-int v2, v10, v11

    add-int/2addr v2, v13

    return v2

    .line 371
    .end local v4           #duQueryUri:Landroid/net/Uri;
    .end local v9           #builder:Landroid/net/Uri$Builder;
    .end local v10           #demandUploadCount:I
    .end local v11           #demandUploadProgress:I
    .end local v12           #duCursor:Landroid/database/Cursor;
    :catchall_5e
    move-exception v2

    if-eqz v14, :cond_64

    .line 372
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_64
    throw v2

    .line 391
    .restart local v4       #duQueryUri:Landroid/net/Uri;
    .restart local v9       #builder:Landroid/net/Uri$Builder;
    .restart local v10       #demandUploadCount:I
    .restart local v11       #demandUploadProgress:I
    .restart local v12       #duCursor:Landroid/database/Cursor;
    :catchall_65
    move-exception v2

    if-eqz v12, :cond_6b

    .line 392
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_6b
    throw v2
.end method

.method private setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 4
    .parameter "account"

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eq v0, p1, :cond_13

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 228
    :cond_13
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 229
    return-void
.end method

.method private switchAccounts()V
    .registers 4

    .prologue
    .line 207
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_15

    .line 208
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    .line 210
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 213
    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 214
    .local v0, startIntent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 215
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 216
    return-void
.end method


# virtual methods
.method protected final continueSignOut(IZ)V
    .registers 12
    .parameter "pendingInstantUploadCount"
    .parameter "downgrading"

    .prologue
    .line 273
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v5, "SignOnManager.progress_dialog"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/DialogFragment;

    .line 275
    .local v3, progressDialog:Landroid/support/v4/app/DialogFragment;
    if-eqz v3, :cond_f

    .line 277
    :try_start_c
    invoke-virtual {v3}, Landroid/support/v4/app/DialogFragment;->dismiss()V
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_f} :catch_6b

    .line 284
    :cond_f
    :goto_f
    if-nez p2, :cond_67

    if-lez p1, :cond_67

    .line 285
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0007

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, p1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 290
    .local v2, message:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    const v5, 0x7f080131

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    const v6, 0x7f0801c4

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    const v7, 0x7f0801c5

    invoke-virtual {v6, v7}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v2, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 296
    .local v0, alertDialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    .line 298
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 299
    .local v1, args:Landroid/os/Bundle;
    if-nez v1, :cond_57

    .line 300
    new-instance v1, Landroid/os/Bundle;

    .end local v1           #args:Landroid/os/Bundle;
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 302
    .restart local v1       #args:Landroid/os/Bundle;
    :cond_57
    const-string v4, "downgrade_account"

    invoke-virtual {v1, v4, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 303
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    .line 305
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v5, "SignOnManager.confirm_signoff"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 310
    .end local v0           #alertDialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v1           #args:Landroid/os/Bundle;
    .end local v2           #message:Ljava/lang/String;
    :goto_66
    return-void

    .line 308
    :cond_67
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnManager;->doSignOut(Z)V

    goto :goto_66

    :catch_6b
    move-exception v4

    goto :goto_f
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final isSignedIn()Z
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final onActivityResult$6eb84b56(II)Z
    .registers 6
    .parameter "requestCode"
    .parameter "resultCode"

    .prologue
    .line 179
    packed-switch p1, :pswitch_data_30

    .line 198
    const/4 v1, 0x0

    :goto_4
    return v1

    .line 181
    :pswitch_5
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 182
    const/4 v1, -0x1

    if-ne p2, v1, :cond_2c

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 185
    .local v0, startIntent:Landroid/content/Intent;
    if-eqz v0, :cond_23

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 194
    .end local v0           #startIntent:Landroid/content/Intent;
    :goto_21
    const/4 v1, 0x1

    goto :goto_4

    .line 189
    .restart local v0       #startIntent:Landroid/content/Intent;
    :cond_23
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_21

    .line 192
    .end local v0           #startIntent:Landroid/content/Intent;
    :cond_2c
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_21

    .line 179
    :pswitch_data_30
    .packed-switch 0x3ff
        :pswitch_5
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;Landroid/content/Intent;)V
    .registers 15
    .parameter "savedInstanceState"
    .parameter "intent"

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 85
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    .line 88
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 90
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_11

    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    .line 123
    :cond_10
    :goto_10
    return-void

    .line 92
    :cond_11
    if-nez p1, :cond_65

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "run_oob"

    invoke-virtual {v6, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_65

    .line 94
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "intent"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    .line 96
    .local v5, startIntent:Landroid/content/Intent;
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "network_oob"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    .line 98
    .local v3, oob:Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "plus_pages"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/AccountSettingsData;

    .line 100
    .local v4, settings:Lcom/google/android/apps/plus/content/AccountSettingsData;
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v3, :cond_53

    invoke-virtual {v3}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v6

    :goto_43
    invoke-static {v8, v0, v6, v4, v7}, Lcom/google/android/apps/plus/phone/Intents;->getOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 103
    .local v2, nextIntent:Landroid/content/Intent;
    if-eqz v2, :cond_55

    .line 104
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 105
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    const/16 v7, 0x3ff

    invoke-virtual {v6, v2, v7}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_10

    .end local v2           #nextIntent:Landroid/content/Intent;
    :cond_53
    move-object v6, v7

    .line 100
    goto :goto_43

    .line 107
    .restart local v2       #nextIntent:Landroid/content/Intent;
    :cond_55
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 108
    if-eqz v5, :cond_10

    .line 109
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 110
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_10

    .line 113
    .end local v2           #nextIntent:Landroid/content/Intent;
    .end local v3           #oob:Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;
    .end local v4           #settings:Lcom/google/android/apps/plus/content/AccountSettingsData;
    .end local v5           #startIntent:Landroid/content/Intent;
    :cond_65
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v6

    if-nez v6, :cond_6f

    .line 114
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_10

    .line 116
    :cond_6f
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 117
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v6

    if-eqz v6, :cond_10

    if-nez p1, :cond_10

    .line 118
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080048

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, message:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v6, v1, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_10
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 327
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 331
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 323
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 5
    .parameter "args"
    .parameter "tag"

    .prologue
    const/4 v1, 0x0

    .line 314
    if-eqz p1, :cond_d

    .line 315
    const-string v0, "downgrade_account"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->doSignOut(Z)V

    .line 319
    :goto_c
    return-void

    .line 317
    :cond_d
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->doSignOut(Z)V

    goto :goto_c
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    .line 173
    return-void
.end method

.method public final onResume()Z
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 131
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v3, "SignOnManager.confirm_signoff"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    .line 134
    .local v0, alertDialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    if-eqz v0, :cond_10

    .line 135
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    .line 139
    :cond_10
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_28

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 141
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 143
    const/4 v1, 0x1

    .line 148
    .local v1, signInJustCompleted:Z
    :goto_1d
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_2e

    .line 150
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_2a

    .line 151
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    .line 165
    :cond_27
    :goto_27
    return v1

    .line 145
    .end local v1           #signInJustCompleted:Z
    :cond_28
    const/4 v1, 0x0

    .restart local v1       #signInJustCompleted:Z
    goto :goto_1d

    .line 153
    :cond_2a
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_27

    .line 155
    :cond_2e
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_40

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_27

    .line 159
    :cond_40
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    .line 160
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v2

    if-nez v2, :cond_27

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initiateConnection(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_27
.end method

.method public final signOut(Z)V
    .registers 6
    .parameter "downgrading"

    .prologue
    .line 244
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    const v3, 0x7f080130

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 246
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v2, "SignOnManager.progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 248
    new-instance v1, Lcom/google/android/apps/plus/phone/SignOnManager$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/phone/SignOnManager$1;-><init>(Lcom/google/android/apps/plus/phone/SignOnManager;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/SignOnManager$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 267
    return-void
.end method
