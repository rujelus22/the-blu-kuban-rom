.class final Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;
.super Landroid/os/HandlerThread;
.source "ImageCache.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoaderThread"
.end annotation


# instance fields
.field private mLoaderThreadHandler:Landroid/os/Handler;

.field private mPreloadRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mPreloadStatus:I

.field private final mRequests:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/content/ImageRequest;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/apps/plus/service/ImageCache;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/ImageCache;)V
    .registers 3
    .parameter

    .prologue
    .line 1063
    iput-object p1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    .line 1064
    const-string v0, "ImageCache"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1052
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    .line 1053
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    .line 1061
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    .line 1065
    return-void
.end method

.method private continuePreloading()V
    .registers 5

    .prologue
    const/4 v1, 0x2

    .line 1095
    iget v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    if-ne v0, v1, :cond_6

    .line 1106
    :cond_5
    :goto_5
    return-void

    .line 1099
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->ensureHandler()V

    .line 1100
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1104
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_5
.end method

.method private ensureHandler()V
    .registers 3

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    if-nez v0, :cond_f

    .line 1069
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    .line 1071
    :cond_f
    return-void
.end method

.method private loadImagesFromDatabase(Z)V
    .registers 18
    .parameter "preloading"

    .prologue
    .line 1321
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v13}, Ljava/util/HashSet;->size()I

    move-result v3

    .line 1322
    .local v3, count:I
    if-nez v3, :cond_b

    .line 1403
    :goto_a
    return-void

    .line 1328
    :cond_b
    if-nez p1, :cond_2e

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_2e

    .line 1329
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-interface {v13, v14}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 1330
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_2e

    .line 1331
    const/4 v13, 0x2

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    .line 1335
    :cond_2e
    const/4 v1, 0x0

    .line 1336
    .local v1, avatarRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    const/4 v9, 0x0

    .line 1337
    .local v9, mediaRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/CachedImageRequest;>;"
    const/4 v10, 0x0

    .line 1339
    .local v10, remoteRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/ImageRequest;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v13}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :goto_39
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_72

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/content/ImageRequest;

    .line 1340
    .local v11, request:Lcom/google/android/apps/plus/content/ImageRequest;
    instance-of v13, v11, Lcom/google/android/apps/plus/content/AvatarRequest;

    if-eqz v13, :cond_56

    .line 1341
    if-nez v1, :cond_50

    .line 1342
    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #avatarRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1344
    .restart local v1       #avatarRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    :cond_50
    check-cast v11, Lcom/google/android/apps/plus/content/AvatarRequest;

    .end local v11           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_39

    .line 1345
    .restart local v11       #request:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_56
    instance-of v13, v11, Lcom/google/android/apps/plus/content/MediaImageRequest;

    if-eqz v13, :cond_67

    .line 1346
    if-nez v9, :cond_61

    .line 1347
    new-instance v9, Ljava/util/ArrayList;

    .end local v9           #mediaRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/CachedImageRequest;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1349
    .restart local v9       #mediaRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/CachedImageRequest;>;"
    :cond_61
    check-cast v11, Lcom/google/android/apps/plus/content/MediaImageRequest;

    .end local v11           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_39

    .line 1351
    .restart local v11       #request:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_67
    if-nez v10, :cond_6e

    .line 1352
    new-instance v10, Ljava/util/ArrayList;

    .end local v10           #remoteRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/ImageRequest;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1354
    .restart local v10       #remoteRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/ImageRequest;>;"
    :cond_6e
    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_39

    .line 1358
    .end local v11           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_72
    if-eqz v9, :cond_b2

    .line 1359
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/google/android/apps/plus/service/ImageCache;->access$800(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v9}, Lcom/google/android/apps/plus/content/EsMediaCache;->loadMedia(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/Map;

    move-result-object v8

    .line 1362
    .local v8, images:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/android/apps/plus/content/CachedImageRequest;[B>;"
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_88
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_b2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1363
    .local v5, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/google/android/apps/plus/content/CachedImageRequest;[B>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/content/CachedImageRequest;

    .line 1364
    .local v11, request:Lcom/google/android/apps/plus/content/CachedImageRequest;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [B

    const/4 v15, 0x1

    move/from16 v0, p1

    invoke-static {v14, v11, v13, v15, v0}, Lcom/google/android/apps/plus/service/ImageCache;->access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V

    .line 1365
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v13, v11}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_88

    .line 1369
    .end local v5           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/google/android/apps/plus/content/CachedImageRequest;[B>;"
    .end local v8           #images:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/android/apps/plus/content/CachedImageRequest;[B>;"
    .end local v11           #request:Lcom/google/android/apps/plus/content/CachedImageRequest;
    :cond_b2
    if-eqz v1, :cond_f2

    .line 1370
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/google/android/apps/plus/service/ImageCache;->access$800(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v1}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatars(Landroid/content/Context;Ljava/util/List;)Ljava/util/Map;

    move-result-object v2

    .line 1373
    .local v2, avatars:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_c8
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_f2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 1374
    .local v4, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 1375
    .local v11, request:Lcom/google/android/apps/plus/content/AvatarRequest;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [B

    const/4 v15, 0x1

    move/from16 v0, p1

    invoke-static {v14, v11, v13, v15, v0}, Lcom/google/android/apps/plus/service/ImageCache;->access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V

    .line 1376
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v13, v11}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_c8

    .line 1385
    .end local v2           #avatars:Ljava/util/Map;,"Ljava/util/Map<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    .end local v4           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    .end local v11           #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    :cond_f2
    if-eqz v10, :cond_11d

    .line 1386
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 1387
    .local v12, requestCount:I
    const/4 v6, 0x0

    .local v6, i:I
    :goto_f9
    if-ge v6, v12, :cond_11d

    .line 1388
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/content/ImageRequest;

    .line 1391
    .local v11, request:Lcom/google/android/apps/plus/content/ImageRequest;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mPendingRequests:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v13}, Lcom/google/android/apps/plus/service/ImageCache;->access$900(Lcom/google/android/apps/plus/service/ImageCache;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_11a

    .line 1392
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/google/android/apps/plus/service/ImageCache;->access$800(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v11}, Lcom/google/android/apps/plus/service/RemoteImageLoader;->downloadImage(Landroid/content/Context;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 1387
    :cond_11a
    add-int/lit8 v6, v6, 0x1

    goto :goto_f9

    .line 1398
    .end local v6           #i:I
    .end local v11           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v12           #requestCount:I
    :cond_11d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v13}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_125
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_13d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/content/ImageRequest;

    .line 1399
    .restart local v11       #request:Lcom/google/android/apps/plus/content/ImageRequest;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move/from16 v0, p1

    invoke-static {v13, v11, v14, v15, v0}, Lcom/google/android/apps/plus/service/ImageCache;->access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V

    goto :goto_125

    .line 1402
    .end local v11           #request:Lcom/google/android/apps/plus/content/ImageRequest;
    :cond_13d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v13

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_a
.end method

.method private preloadAvatarsInBackground()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 1196
    iget v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    if-ne v3, v5, :cond_7

    .line 1243
    :goto_6
    return-void

    .line 1200
    :cond_7
    iget v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    if-nez v3, :cond_1c

    .line 1201
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_19

    .line 1202
    iput v5, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    .line 1206
    :goto_15
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->continuePreloading()V

    goto :goto_6

    .line 1204
    :cond_19
    iput v6, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    goto :goto_15

    .line 1210
    :cond_1c
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$200(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/support/v4/util/LruCache;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/util/LruCache;->size()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCacheRedZoneBytes:I
    invoke-static {v4}, Lcom/google/android/apps/plus/service/ImageCache;->access$300(Lcom/google/android/apps/plus/service/ImageCache;)I

    move-result v4

    if-le v3, v4, :cond_31

    .line 1211
    iput v5, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    goto :goto_6

    .line 1215
    :cond_31
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    .line 1217
    const/4 v0, 0x0

    .line 1218
    .local v0, count:I
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    .line 1219
    .local v1, preloadSize:I
    :cond_3d
    :goto_3d
    if-lez v1, :cond_6c

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    const/16 v4, 0x19

    if-ge v3, v4, :cond_6c

    .line 1220
    add-int/lit8 v1, v1, -0x1

    .line 1221
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 1222
    .local v2, request:Lcom/google/android/apps/plus/content/AvatarRequest;
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1224
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$200(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/support/v4/util/LruCache;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3d

    .line 1225
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1226
    add-int/lit8 v0, v0, 0x1

    goto :goto_3d

    .line 1230
    .end local v2           #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    :cond_6c
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->loadImagesFromDatabase(Z)V

    .line 1232
    if-nez v1, :cond_73

    .line 1233
    iput v5, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    .line 1236
    :cond_73
    const-string v3, "ImageCache"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a4

    .line 1237
    const-string v3, "ImageCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Preloaded "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " avatars. Cache size (bytes): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;
    invoke-static {v5}, Lcom/google/android/apps/plus/service/ImageCache;->access$200(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/support/v4/util/LruCache;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/util/LruCache;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    :cond_a4
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->continuePreloading()V

    goto/16 :goto_6
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .registers 12
    .parameter "msg"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1156
    :try_start_2
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_fc

    :cond_7
    :goto_7
    move v2, v4

    .line 1185
    :goto_8
    return v2

    .line 1159
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1160
    .local v0, requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1161
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadRequests:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1162
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mPreloadStatus:I

    .line 1163
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->preloadAvatarsInBackground()V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_1d} :catch_1e

    goto :goto_7

    .line 1182
    .end local v0           #requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    :catch_1e
    move-exception v1

    .line 1183
    .local v1, t:Ljava/lang/Throwable;
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    move v2, v5

    .line 1185
    goto :goto_8

    .line 1166
    .end local v1           #t:Ljava/lang/Throwable;
    :pswitch_2c
    :try_start_2c
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->preloadAvatarsInBackground()V

    goto :goto_7

    .line 1169
    :pswitch_30
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$600(Lcom/google/android/apps/plus/service/ImageCache;Ljava/util/HashSet;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7f

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_4d
    :goto_4d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_74

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/ImageRequest;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mImageHolderCache:Landroid/support/v4/util/LruCache;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$200(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/support/v4/util/LruCache;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;

    if-eqz v3, :cond_4d

    iget-object v7, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v3, v3, Lcom/google/android/apps/plus/service/ImageCache$ImageHolder;->bytes:[B

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v7, v2, v3, v8, v9}, Lcom/google/android/apps/plus/service/ImageCache;->access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_4d

    :cond_74
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_7

    :cond_7f
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$700(Lcom/google/android/apps/plus/service/ImageCache;Ljava/util/HashSet;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mRequests:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_92

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->loadImagesFromDatabase(Z)V

    :cond_92
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->continuePreloading()V

    goto/16 :goto_7

    .line 1172
    :pswitch_97
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    const/4 v6, 0x3

    invoke-virtual {v3, v6, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_7

    .line 1175
    :pswitch_b1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v6, v2, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->request:Lcom/google/android/apps/plus/content/MediaImageRequest;

    iget-object v7, v2, Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;->imageBytes:[B

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v3, v6, v7, v8, v9}, Lcom/google/android/apps/plus/service/ImageCache;->access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    const/4 v6, 0x4

    invoke-virtual {v3, v6, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_7

    .line 1178
    :pswitch_d6
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    iget-object v6, v2, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;->request:Lcom/google/android/apps/plus/content/ImageRequest;

    iget-object v7, v2, Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;->imageBytes:[B

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {v3, v6, v7, v8, v9}, Lcom/google/android/apps/plus/service/ImageCache;->access$500(Lcom/google/android/apps/plus/service/ImageCache;Lcom/google/android/apps/plus/content/ImageRequest;[BZZ)V

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    const/4 v6, 0x5

    invoke-virtual {v3, v6, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->this$0:Lcom/google/android/apps/plus/service/ImageCache;

    #getter for: Lcom/google/android/apps/plus/service/ImageCache;->mMainThreadHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/google/android/apps/plus/service/ImageCache;->access$400(Lcom/google/android/apps/plus/service/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_f9
    .catch Ljava/lang/Throwable; {:try_start_2c .. :try_end_f9} :catch_1e

    goto/16 :goto_7

    .line 1156
    nop

    :pswitch_data_fc
    .packed-switch 0x0
        :pswitch_9
        :pswitch_2c
        :pswitch_30
        :pswitch_97
        :pswitch_b1
        :pswitch_d6
    .end packed-switch
.end method

.method public final notifyAvatarChange(Ljava/lang/String;)V
    .registers 5
    .parameter "gaiaId"

    .prologue
    .line 1122
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->ensureHandler()V

    .line 1123
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1124
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1125
    return-void
.end method

.method public final notifyMediaImageChange(Lcom/google/android/apps/plus/service/ImageCache$MediaImageChangeNotification;)V
    .registers 5
    .parameter "notification"

    .prologue
    .line 1132
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->ensureHandler()V

    .line 1133
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1135
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1136
    return-void
.end method

.method public final notifyRemoteImageChange(Lcom/google/android/apps/plus/service/ImageCache$RemoteImageChangeNotification;)V
    .registers 5
    .parameter "notification"

    .prologue
    .line 1143
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->ensureHandler()V

    .line 1144
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1146
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1147
    return-void
.end method

.method public final requestLoading()V
    .registers 3

    .prologue
    .line 1112
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->ensureHandler()V

    .line 1113
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1114
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1115
    return-void
.end method

.method public final startPreloading(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1081
    .local p1, requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->ensureHandler()V

    .line 1083
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageCache$LoaderThread;->mLoaderThreadHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1085
    return-void
.end method
