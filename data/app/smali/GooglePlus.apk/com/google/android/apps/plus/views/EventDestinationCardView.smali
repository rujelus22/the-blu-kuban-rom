.class public Lcom/google/android/apps/plus/views/EventDestinationCardView;
.super Lcom/google/android/apps/plus/views/CardView;
.source "EventDestinationCardView.java"


# instance fields
.field private mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mIgnoreHeight:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance v0, Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setPaddingEnabled(Z)V

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setFocusable(Z)V

    .line 34
    return-void
.end method


# virtual methods
.method public final bindData(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)V
    .registers 5
    .parameter "account"
    .parameter "event"

    .prologue
    .line 52
    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-virtual {v0, p1, p0, p2, v1}, Lcom/google/android/apps/plus/views/EventCardDrawer;->bind(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/views/CardView;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V

    .line 54
    return-void
.end method

.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .registers 8
    .parameter "canvas"
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    add-int v1, p3, p5

    invoke-virtual {v0, p3, v1, p1}, Lcom/google/android/apps/plus/views/EventCardDrawer;->draw$680d9d43(IILandroid/graphics/Canvas;)I

    move-result v0

    return v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .registers 12

    .prologue
    const v10, 0x7f0803f1

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 100
    .local v1, res:Landroid/content/res/Resources;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .local v3, sb:Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v7, 0x0

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/util/EventDateUtils;->getDateRange(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;

    move-result-object v4

    .line 107
    .local v4, startTimeText:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 109
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v5, :cond_49

    .line 110
    const v6, 0x7f0803f0

    new-array v7, v8, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-eqz v5, :cond_5b

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    :goto_40
    aput-object v5, v7, v9

    invoke-virtual {v1, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, locationText:Ljava/lang/String;
    invoke-static {v3, v0}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 116
    .end local v0           #locationText:Ljava/lang/String;
    :cond_49
    const/4 v2, 0x0

    .line 117
    .local v2, rsvpText:Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpStatus(Lcom/google/api/services/plusi/model/PlusEvent;)I

    move-result v5

    packed-switch v5, :pswitch_data_9a

    .line 135
    :goto_53
    invoke-static {v3, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 110
    .end local v2           #rsvpText:Ljava/lang/String;
    :cond_5b
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto :goto_40

    .line 119
    .restart local v2       #rsvpText:Ljava/lang/String;
    :pswitch_62
    new-array v5, v8, [Ljava/lang/Object;

    const v6, 0x7f0800e6

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v1, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 121
    goto :goto_53

    .line 123
    :pswitch_72
    new-array v5, v8, [Ljava/lang/Object;

    const v6, 0x7f0800e8

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v1, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 125
    goto :goto_53

    .line 127
    :pswitch_82
    new-array v5, v8, [Ljava/lang/Object;

    const v6, 0x7f0800e7

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v1, v10, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 129
    goto :goto_53

    .line 131
    :pswitch_92
    const v5, 0x7f0800e5

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_53

    .line 117
    :pswitch_data_9a
    .packed-switch 0x0
        :pswitch_92
        :pswitch_62
        :pswitch_82
        :pswitch_72
    .end packed-switch
.end method

.method public final getEvent()Lcom/google/api/services/plusi/model/PlusEvent;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-object v0
.end method

.method protected final layoutElements(IIII)I
    .registers 11
    .parameter "x"
    .parameter "y"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    move v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layout(IIZII)I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 83
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onAttachedToWindow()V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->attach()V

    .line 85
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onDetachedFromWindow()V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->detach()V

    .line 91
    return-void
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 58
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 59
    .local v3, widthDimension:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 60
    .local v1, heightDimensionArg:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    if-nez v4, :cond_3e

    const/4 v4, 0x1

    :goto_f
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    .line 61
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    if-eqz v4, :cond_40

    move v0, v3

    .line 63
    .local v0, heightDimension:I
    :goto_16
    sget v4, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sLeftBorderPadding:I

    sget v5, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sTopBorderPadding:I

    sget v6, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sLeftBorderPadding:I

    sget v7, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sRightBorderPadding:I

    add-int/2addr v6, v7

    sub-int v6, v3, v6

    sget v7, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sTopBorderPadding:I

    sget v8, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sBottomBorderPadding:I

    add-int/2addr v7, v8

    sub-int v7, v0, v7

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->layoutElements(IIII)I

    move-result v2

    .line 67
    .local v2, measuredHeight:I
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    if-eqz v4, :cond_3a

    sget v4, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sTopBorderPadding:I

    add-int/2addr v4, v2

    sget v5, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sBottomBorderPadding:I

    add-int/2addr v4, v5

    sget v5, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sYPadding:I

    add-int v0, v4, v5

    .end local v0           #heightDimension:I
    :cond_3a
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setMeasuredDimension(II)V

    .line 69
    return-void

    .line 60
    .end local v2           #measuredHeight:I
    :cond_3e
    const/4 v4, 0x0

    goto :goto_f

    :cond_40
    move v0, v1

    .line 61
    goto :goto_16
.end method

.method public onRecycle()V
    .registers 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onRecycle()V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->clear()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 49
    return-void
.end method
