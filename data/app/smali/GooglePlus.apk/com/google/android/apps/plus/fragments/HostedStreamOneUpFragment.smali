.class public Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "HostedStreamOneUpFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;
.implements Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;
.implements Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;
.implements Lcom/google/android/apps/plus/views/OneUpListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$MyTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;",
        "Lcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;",
        "Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;",
        "Lcom/google/android/apps/plus/views/OneUpListener;"
    }
.end annotation


# static fields
.field private static sAvatarMarginTop:I

.field private static sMaxWidth:I

.field private static sMinExposureLand:I

.field private static sMinExposurePort:I

.field private static sResourcesLoaded:Z


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActivityId:Ljava/lang/String;

.field private mActivityRequestId:Ljava/lang/Integer;

.field private mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

.field private mAlbumId:Ljava/lang/String;

.field private mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

.field private mAutoPlay:Z

.field private mBackgroundDesiredHeight:I

.field private mBackgroundDesiredWidth:I

.field private mBackgroundLinkUrl:Ljava/lang/String;

.field private mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mCommentButton:Landroid/view/View;

.field private mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mDeepLinkLogged:Z

.field private mEditableText:Ljava/lang/String;

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

.field private mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

.field private mIsActivityMuted:Z

.field private mIsActivityPublic:Z

.field private mIsActivityResharable:Z

.field private mIsMyActivity:Ljava/lang/Boolean;

.field private mLinkTitle:Ljava/lang/String;

.field private mLinkUrl:Ljava/lang/String;

.field private mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

.field private mListParent:Landroid/view/View;

.field private mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

.field private mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

.field private mMediaCount:I

.field private mMuteProcessed:Z

.field private mOperationType:I

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mPlusOnedByData:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mReadProcessed:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

.field private mShowAlbumOverlay:Z

.field private mSourceAuthorId:Ljava/lang/String;

.field private mSourcePackageName:Ljava/lang/String;

.field private mTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    .line 220
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    .line 227
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    .line 1874
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 109
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1300()I
    .registers 1

    .prologue
    .line 109
    sget v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMaxWidth:I

    return v0
.end method

.method static synthetic access$1400()I
    .registers 1

    .prologue
    .line 109
    sget v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sAvatarMarginTop:I

    return v0
.end method

.method static synthetic access$1500()I
    .registers 1

    .prologue
    .line 109
    sget v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMinExposureLand:I

    return v0
.end method

.method static synthetic access$1600()I
    .registers 1

    .prologue
    .line 109
    sget v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMinExposurePort:I

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "hsouf_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_16

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_16
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2402(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    return-void
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private bindStage(Landroid/database/Cursor;)V
    .registers 35
    .parameter "c"

    .prologue
    .line 870
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v32

    .line 871
    .local v32, view:Landroid/view/View;
    if-nez v32, :cond_7

    .line 993
    :cond_6
    :goto_6
    return-void

    .line 877
    :cond_7
    if-eqz p1, :cond_6

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 883
    const/16 v7, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v22

    .line 884
    .local v22, hangoutDataBytes:[B
    if-eqz v22, :cond_79

    .line 885
    invoke-static {}, Lcom/google/api/services/plusi/model/HangoutDataJson;->getInstance()Lcom/google/api/services/plusi/model/HangoutDataJson;

    move-result-object v7

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/google/api/services/plusi/model/HangoutDataJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/api/services/plusi/model/HangoutData;

    .line 887
    .local v21, hangoutData:Lcom/google/api/services/plusi/model/HangoutData;
    const/4 v7, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 888
    .local v18, authorName:Ljava/lang/String;
    const/4 v7, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 890
    .local v17, authorId:Ljava/lang/String;
    const v7, 0x7f09017c

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .line 891
    .local v30, stage:Landroid/view/View;
    if-nez v30, :cond_4d

    .line 893
    const v7, 0x7f09022a

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewStub;

    invoke-virtual {v7}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v30

    .line 896
    :cond_4d
    const v7, 0x7f09022d

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;

    .line 898
    .local v23, hangoutView:Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;
    if-eqz v23, :cond_6

    .line 902
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    move-object/from16 v3, v17

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->bind(Lcom/google/api/services/plusi/model/HangoutData;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/OneUpListener;)V

    .line 904
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    const v8, 0x7f090181

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    goto :goto_6

    .line 909
    .end local v17           #authorId:Ljava/lang/String;
    .end local v18           #authorName:Ljava/lang/String;
    .end local v21           #hangoutData:Lcom/google/api/services/plusi/model/HangoutData;
    .end local v23           #hangoutView:Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;
    .end local v30           #stage:Landroid/view/View;
    :cond_79
    const/4 v5, 0x0

    .line 910
    .local v5, bgMediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    const/16 v19, 0x0

    .line 912
    .local v19, backgroundLinkUrl:Ljava/lang/String;
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMediaCount:I

    .line 914
    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v26

    .line 915
    .local v26, mediaBytes:[B
    if-eqz v26, :cond_193

    .line 916
    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v20

    .line 917
    .local v20, dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    const/16 v24, 0x0

    .local v24, i:I
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v29, v0

    .local v29, size:I
    :goto_95
    move/from16 v0, v24

    move/from16 v1, v29

    if-ge v0, v1, :cond_193

    .line 918
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v31

    .line 919
    .local v31, type:I
    sparse-switch v31, :sswitch_data_1d2

    .line 917
    :cond_a4
    :goto_a4
    add-int/lit8 v24, v24, 0x1

    goto :goto_95

    .line 923
    :sswitch_a7
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMediaCount:I

    add-int/lit8 v7, v7, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMediaCount:I

    .line 924
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v7, :cond_a4

    if-nez v5, :cond_a4

    .line 925
    const/4 v7, 0x2

    move/from16 v0, v31

    if-ne v0, v7, :cond_11c

    const/16 v25, 0x1

    .line 926
    .local v25, isVideo:Z
    :goto_c0
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v9

    .line 927
    .local v9, thumbnailUrl:Ljava/lang/String;
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v27

    .line 928
    .local v27, mediaUrl:Ljava/lang/String;
    if-eqz v25, :cond_da

    .line 929
    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/plus/util/ImageUtils;->rewriteYoutubeMediaUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 931
    .local v28, rewrittenUrl:Ljava/lang/String;
    invoke-static/range {v27 .. v28}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_da

    .line 932
    move-object/from16 v9, v28

    .line 936
    .end local v28           #rewrittenUrl:Ljava/lang/String;
    :cond_da
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAlbumId:Ljava/lang/String;

    .line 937
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v6

    .line 939
    .local v6, ownerId:Ljava/lang/String;
    new-instance v5, Lcom/google/android/apps/plus/api/MediaRef;

    .end local v5           #bgMediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getPhotoId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/String;)J

    move-result-wide v7

    if-eqz v25, :cond_11f

    invoke-static/range {v27 .. v27}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    :goto_fc
    if-eqz v25, :cond_121

    sget-object v11, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_100
    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 944
    .restart local v5       #bgMediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v19

    .line 946
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getTitlePlaintext()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkTitle:Ljava/lang/String;

    .line 947
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/views/LinksCardView;->makeLinkUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkUrl:Ljava/lang/String;

    goto :goto_a4

    .line 925
    .end local v6           #ownerId:Ljava/lang/String;
    .end local v9           #thumbnailUrl:Ljava/lang/String;
    .end local v25           #isVideo:Z
    .end local v27           #mediaUrl:Ljava/lang/String;
    :cond_11c
    const/16 v25, 0x0

    goto :goto_c0

    .line 939
    .end local v5           #bgMediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    .restart local v6       #ownerId:Ljava/lang/String;
    .restart local v9       #thumbnailUrl:Ljava/lang/String;
    .restart local v25       #isVideo:Z
    .restart local v27       #mediaUrl:Ljava/lang/String;
    :cond_11f
    const/4 v10, 0x0

    goto :goto_fc

    :cond_121
    sget-object v11, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_100

    .line 954
    .end local v6           #ownerId:Ljava/lang/String;
    .end local v9           #thumbnailUrl:Ljava/lang/String;
    .end local v25           #isVideo:Z
    .end local v27           #mediaUrl:Ljava/lang/String;
    .restart local v5       #bgMediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    :sswitch_124
    const v7, 0x7f09017c

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .line 955
    .restart local v30       #stage:Landroid/view/View;
    if-nez v30, :cond_13e

    .line 957
    const v7, 0x7f090229

    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewStub;

    invoke-virtual {v7}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v30

    .line 960
    :cond_13e
    const v7, 0x7f09022f

    move-object/from16 v0, v30

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;

    .line 962
    .local v10, skyjamView:Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;
    if-eqz v10, :cond_6

    .line 966
    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getAlbum()Ljava/lang/String;

    move-result-object v11

    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getSongTitle()Ljava/lang/String;

    move-result-object v12

    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v13

    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getPlayerUrl()Ljava/lang/String;

    move-result-object v14

    aget-object v7, v20, v24

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v10 .. v16}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    const v8, 0x7f090181

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    .line 973
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    if-eqz v7, :cond_6

    .line 974
    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->startAutoPlay()V

    .line 975
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput-boolean v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    goto/16 :goto_6

    .line 983
    .end local v10           #skyjamView:Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;
    .end local v20           #dbMediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v24           #i:I
    .end local v29           #size:I
    .end local v30           #stage:Landroid/view/View;
    .end local v31           #type:I
    :cond_193
    if-eqz v5, :cond_6

    .line 984
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 985
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundLinkUrl:Ljava/lang/String;

    .line 986
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v7, :cond_1bb

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v7

    if-nez v7, :cond_1bb

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v7, v8, :cond_1c6

    .line 988
    :cond_1bb
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->bindStageMedia(Landroid/view/View;)Z

    goto/16 :goto_6

    .line 990
    :cond_1c6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v7

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->bindStageLink(Landroid/view/View;)Z

    goto/16 :goto_6

    .line 919
    nop

    :sswitch_data_1d2
    .sparse-switch
        0x1 -> :sswitch_a7
        0x2 -> :sswitch_a7
        0x3 -> :sswitch_a7
        0xb -> :sswitch_124
        0xd -> :sswitch_124
    .end sparse-switch
.end method

.method private bindStageLink(Landroid/view/View;)Z
    .registers 11
    .parameter "view"

    .prologue
    const/4 v8, 0x0

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v0, :cond_18

    const/4 v6, 0x0

    .line 833
    .local v6, backgroundUrl:Ljava/lang/String;
    :goto_6
    const v0, 0x7f09017c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 834
    .local v7, stage:Landroid/view/View;
    if-nez v6, :cond_1f

    .line 835
    if-eqz v7, :cond_16

    .line 836
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_16
    move v0, v8

    .line 859
    :goto_17
    return v0

    .line 831
    .end local v6           #backgroundUrl:Ljava/lang/String;
    .end local v7           #stage:Landroid/view/View;
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 841
    .restart local v6       #backgroundUrl:Ljava/lang/String;
    .restart local v7       #stage:Landroid/view/View;
    :cond_1f
    if-nez v7, :cond_70

    .line 843
    const v0, 0x7f09022b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v7

    .line 845
    const v0, 0x7f090164

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 847
    const v0, 0x7f090184

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OneUpLinkView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    .line 848
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v0, v1, :cond_72

    const/4 v2, 0x3

    .line 850
    .local v2, type:I
    :goto_4e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkUrl:Ljava/lang/String;

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/OneUpLinkView;->init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 853
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    const v1, 0x7f090181

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    .line 856
    invoke-virtual {v7}, Landroid/view/View;->invalidate()V

    .line 859
    .end local v2           #type:I
    :cond_70
    const/4 v0, 0x1

    goto :goto_17

    .line 848
    :cond_72
    const/4 v2, 0x2

    goto :goto_4e
.end method

.method private bindStageMedia(Landroid/view/View;)Z
    .registers 11
    .parameter "view"

    .prologue
    const/4 v8, 0x0

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v0, :cond_18

    const/4 v6, 0x0

    .line 795
    .local v6, backgroundUrl:Ljava/lang/String;
    :goto_6
    const v0, 0x7f09017c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 796
    .local v7, stage:Landroid/view/View;
    if-nez v6, :cond_1f

    .line 797
    if-eqz v7, :cond_16

    .line 798
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_16
    move v0, v8

    .line 822
    :goto_17
    return v0

    .line 793
    .end local v6           #backgroundUrl:Ljava/lang/String;
    .end local v7           #stage:Landroid/view/View;
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    goto :goto_6

    .line 803
    .restart local v6       #backgroundUrl:Ljava/lang/String;
    .restart local v7       #stage:Landroid/view/View;
    :cond_1f
    if-nez v7, :cond_70

    .line 805
    const v0, 0x7f09017b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v7

    .line 807
    const v0, 0x7f090164

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 809
    const v0, 0x7f090184

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OneUpImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    .line 810
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v0, v1, :cond_72

    const/4 v2, 0x3

    .line 812
    .local v2, type:I
    :goto_4e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundDesiredWidth:I

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundDesiredHeight:I

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/OneUpImageView;->init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpBackgroundView$BackgroundViewLoadedListener;II)V

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/OneUpImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 816
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    const v1, 0x7f090181

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    .line 819
    invoke-virtual {v7}, Landroid/view/View;->invalidate()V

    .line 822
    .end local v2           #type:I
    :cond_70
    const/4 v0, 0x1

    goto :goto_17

    .line 810
    :cond_72
    const/4 v2, 0x2

    goto :goto_4e
.end method

.method private doReportComment(ZLjava/lang/String;Z)V
    .registers 10
    .parameter "delete"
    .parameter "commentId"
    .parameter "isUndo"

    .prologue
    .line 1694
    const-string v4, "extra_comment_id"

    invoke-static {v4, p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1696
    .local v1, extras:Landroid/os/Bundle;
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1697
    if-eqz p3, :cond_58

    const v4, 0x7f0803d3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1700
    .local v3, title:Ljava/lang/String;
    :goto_14
    if-eqz p3, :cond_60

    const v4, 0x7f0803d4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1703
    .local v2, question:Ljava/lang/String;
    :goto_1d
    const v4, 0x7f0801c4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1705
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v4, 0x0

    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1706
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "comment_id"

    invoke-virtual {v4, v5, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "delete"

    invoke-virtual {v4, v5, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1708
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "is_undo"

    invoke-virtual {v4, v5, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1709
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "hsouf_report_comment"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1710
    return-void

    .line 1697
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v2           #question:Ljava/lang/String;
    .end local v3           #title:Ljava/lang/String;
    :cond_58
    const v4, 0x7f0803d0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_14

    .line 1700
    .restart local v3       #title:Ljava/lang/String;
    :cond_60
    const v4, 0x7f0803d1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1d
.end method

.method private showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 12
    .parameter "audienceData"

    .prologue
    .line 1780
    const-string v7, "StreamOneUp"

    const/4 v8, 0x3

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5e

    .line 1781
    const-string v7, "StreamOneUp"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Hidden count: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1783
    const-string v7, "StreamOneUp"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Audience users: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v1

    .local v1, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v5, v1

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_3f
    if-ge v4, v5, :cond_5e

    aget-object v6, v1, v4

    .line 1785
    .local v6, user:Lcom/google/android/apps/plus/content/PersonData;
    const-string v7, "StreamOneUp"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Users: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    add-int/lit8 v4, v4, 0x1

    goto :goto_3f

    .line 1789
    .end local v1           #arr$:[Lcom/google/android/apps/plus/content/PersonData;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v6           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_5e
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getAclText()Ljava/lang/String;

    move-result-object v0

    .line 1790
    .local v0, aclText:Ljava/lang/String;
    new-instance v3, Lcom/google/android/apps/plus/fragments/AclFragment;

    invoke-direct {v3}, Lcom/google/android/apps/plus/fragments/AclFragment;-><init>()V

    .line 1791
    .local v3, fragment:Lcom/google/android/apps/plus/fragments/AclFragment;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1792
    .local v2, bundle:Landroid/os/Bundle;
    const-string v7, "account"

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1793
    const-string v7, "audience"

    invoke-virtual {v2, v7, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1794
    const-string v7, "acl_display"

    invoke-virtual {v2, v7, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1795
    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/fragments/AclFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1796
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "hsouf_audience"

    invoke-virtual {v3, v7, v8}, Lcom/google/android/apps/plus/fragments/AclFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1797
    return-void
.end method

.method private showPlusOnePeople(Ljava/lang/String;I)V
    .registers 7
    .parameter "plusOneId"
    .parameter "totalPlusOnes"

    .prologue
    .line 1801
    new-instance v1, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;-><init>()V

    .line 1802
    .local v1, fragment:Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1803
    .local v0, bundle:Landroid/os/Bundle;
    const-string v2, "account"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1804
    const-string v2, "plus_one_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805
    const-string v2, "total_plus_ones"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1806
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1807
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hsouf_plus_ones"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1808
    return-void
.end method

.method private showProgressDialog(I)V
    .registers 6
    .parameter "operationType"

    .prologue
    .line 1817
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    .line 1819
    const/16 v1, 0x30

    if-ne p1, v1, :cond_1d

    const v1, 0x7f080096

    :goto_9
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1822
    .local v0, message:Ljava/lang/String;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hsouf_pending"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1824
    return-void

    .line 1819
    .end local v0           #message:Ljava/lang/String;
    :cond_1d
    const v1, 0x7f080164

    goto :goto_9
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 1833
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1828
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onActionButtonClicked(I)V
    .registers 7
    .parameter "actionId"

    .prologue
    .line 455
    if-nez p1, :cond_48

    .line 456
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityPublic:Z

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getReshareActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 458
    .local v0, intent:Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityPublic:Z

    if-eqz v1, :cond_44

    .line 459
    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_RESHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    const v1, 0x7f0801a0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0801a1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801a2

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/fragments/ConfirmIntentDialog;->newInstance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "reshare_activity"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 466
    .end local v0           #intent:Landroid/content/Intent;
    :goto_43
    return-void

    .line 461
    .restart local v0       #intent:Landroid/content/Intent;
    :cond_44
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_43

    .line 464
    .end local v0           #intent:Landroid/content/Intent;
    :cond_48
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onActionButtonClicked(I)V

    goto :goto_43
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 11
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1542
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2e

    .line 1543
    const/4 v3, -0x1

    if-ne p2, v3, :cond_2f

    .line 1544
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1545
    .local v1, activity:Landroid/app/Activity;
    if-eqz v1, :cond_2e

    .line 1547
    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1548
    .local v2, context:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 1549
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourceAuthorId:Ljava/lang/String;

    invoke-static {v2, v0, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1551
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1559
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #activity:Landroid/app/Activity;
    .end local v2           #context:Landroid/content/Context;
    :cond_2e
    :goto_2e
    return-void

    .line 1555
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_NOT_STARTED_ON_PLAY_STORE:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto :goto_2e
.end method

.method public final onBackgroundViewLoaded(Lcom/google/android/apps/plus/views/OneUpBackgroundView;)V
    .registers 12
    .parameter "backgroundView"

    .prologue
    const/16 v8, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 657
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/OneUpBackgroundView;->getId()I

    move-result v7

    const v9, 0x7f090184

    if-ne v7, v9, :cond_5b

    .line 658
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v4

    .line 659
    .local v4, view:Landroid/view/View;
    const v7, 0x7f090164

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 661
    instance-of v7, p1, Lcom/google/android/apps/plus/views/OneUpImageView;

    if-eqz v7, :cond_5b

    .line 662
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v7, :cond_5c

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v7

    sget-object v9, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v7, v9, :cond_5c

    move v2, v5

    .line 664
    .local v2, isVideo:Z
    :goto_2e
    const v7, 0x7f090185

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 665
    .local v3, mediaOverlay:Landroid/view/View;
    if-eqz v2, :cond_5e

    move v7, v6

    :goto_38
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 667
    if-nez v2, :cond_60

    iget v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMediaCount:I

    if-le v7, v5, :cond_60

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowAlbumOverlay:Z

    if-eqz v7, :cond_60

    move v1, v5

    .line 668
    .local v1, isAlbum:Z
    :goto_46
    const v5, 0x7f09022e

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EsImageView;

    .line 670
    .local v0, albumOverlay:Lcom/google/android/apps/plus/views/EsImageView;
    if-eqz v1, :cond_62

    :goto_51
    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    .line 671
    if-eqz v1, :cond_5b

    .line 672
    const/16 v5, 0x7d0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/EsImageView;->startFadeOut(I)V

    .line 676
    .end local v0           #albumOverlay:Lcom/google/android/apps/plus/views/EsImageView;
    .end local v1           #isAlbum:Z
    .end local v2           #isVideo:Z
    .end local v3           #mediaOverlay:Landroid/view/View;
    .end local v4           #view:Landroid/view/View;
    :cond_5b
    return-void

    .restart local v4       #view:Landroid/view/View;
    :cond_5c
    move v2, v6

    .line 662
    goto :goto_2e

    .restart local v2       #isVideo:Z
    .restart local v3       #mediaOverlay:Landroid/view/View;
    :cond_5e
    move v7, v8

    .line 665
    goto :goto_38

    :cond_60
    move v1, v6

    .line 667
    goto :goto_46

    .restart local v0       #albumOverlay:Lcom/google/android/apps/plus/views/EsImageView;
    .restart local v1       #isAlbum:Z
    :cond_62
    move v6, v8

    .line 670
    goto :goto_51
.end method

.method public final onCancelRequested()V
    .registers 7

    .prologue
    .line 1636
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-nez v2, :cond_c

    .line 1637
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1651
    :goto_b
    return-void

    .line 1639
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1640
    .local v0, content:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_24

    .line 1641
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_b

    .line 1643
    :cond_24
    const v2, 0x7f080132

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080133

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801c6

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c8

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    .line 1647
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1648
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hsouf_cancel_edits"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_b
.end method

.method public onClick(Landroid/view/View;)V
    .registers 25
    .parameter "view"

    .prologue
    .line 1085
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v20

    sparse-switch v20, :sswitch_data_1b8

    .line 1165
    .end local p1
    :cond_7
    :goto_7
    return-void

    .line 1087
    .restart local p1
    :sswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 1088
    .local v4, commentText:Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_81

    .line 1089
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    move-object/from16 v20, v0

    if-nez v20, :cond_7

    .line 1090
    const-string v20, "extra_activity_id"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 1092
    .local v7, extras:Landroid/os/Bundle;
    sget-object v20, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1094
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v15

    .line 1095
    .local v15, spannable:Landroid/text/Spannable;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 1096
    .local v5, context:Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    invoke-static {v15}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v13

    .line 1098
    .local v13, postableText:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-static {v5, v0, v1, v13}, Lcom/google/android/apps/plus/service/EsService;->createComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1100
    const/16 v20, 0x20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_7

    .line 1103
    .end local v5           #context:Landroid/content/Context;
    .end local v7           #extras:Landroid/os/Bundle;
    .end local v13           #postableText:Ljava/lang/String;
    .end local v15           #spannable:Landroid/text/Spannable;
    :cond_81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setEnabled(Z)V

    goto/16 :goto_7

    .line 1110
    .end local v4           #commentText:Ljava/lang/String;
    :sswitch_8e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v20, v0

    if-nez v20, :cond_e9

    .line 1111
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 1112
    check-cast p1, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    .end local p1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getLastTouchEvent()Landroid/view/MotionEvent;

    move-result-object v6

    .line 1113
    .local v6, event:Landroid/view/MotionEvent;
    if-eqz v6, :cond_7

    .line 1114
    invoke-virtual {v6}, Landroid/view/MotionEvent;->getRawX()F

    move-result v18

    .line 1115
    .local v18, x:F
    invoke-virtual {v6}, Landroid/view/MotionEvent;->getRawY()F

    move-result v19

    .line 1116
    .local v19, y:F
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v20

    const v21, 0x7f09017c

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 1117
    .local v16, stage:Landroid/view/View;
    if-eqz v16, :cond_7

    .line 1118
    const v20, 0x7f09022d

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;

    .line 1120
    .local v8, hangoutView:Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;
    if-eqz v8, :cond_d1

    .line 1121
    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;->processClick(FF)V

    .line 1124
    :cond_d1
    const v20, 0x7f09022f

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;

    .line 1126
    .local v14, skyjamView:Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;
    if-eqz v14, :cond_7

    .line 1127
    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v14, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->processClick(FF)V

    goto/16 :goto_7

    .line 1135
    .end local v6           #event:Landroid/view/MotionEvent;
    .end local v8           #hangoutView:Lcom/google/android/apps/plus/views/StreamOneUpHangoutView;
    .end local v14           #skyjamView:Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;
    .end local v16           #stage:Landroid/view/View;
    .end local v18           #x:F
    .end local v19           #y:F
    .restart local p1
    :cond_e9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v20

    sget-object v21, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_171

    const/4 v9, 0x1

    .line 1136
    .local v9, isImage:Z
    :goto_fc
    if-eqz v9, :cond_194

    .line 1137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v11

    .line 1139
    .local v11, photoId:J
    const-wide/16 v20, 0x0

    cmp-long v20, v11, v20

    if-eqz v20, :cond_173

    .line 1140
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v3

    .line 1142
    .local v3, builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v10

    .line 1145
    .local v10, ownerGaiaId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAlbumId:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAlbumId:Ljava/lang/String;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setRefreshAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1151
    const-string v20, "extra_gaia_id"

    move-object/from16 v0, v20

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 1153
    .restart local v7       #extras:Landroid/os/Bundle;
    sget-object v20, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1155
    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_7

    .line 1135
    .end local v3           #builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    .end local v7           #extras:Landroid/os/Bundle;
    .end local v9           #isImage:Z
    .end local v10           #ownerGaiaId:Ljava/lang/String;
    .end local v11           #photoId:J
    :cond_171
    const/4 v9, 0x0

    goto :goto_fc

    .line 1156
    .restart local v9       #isImage:Z
    .restart local v11       #photoId:J
    :cond_173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundLinkUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_7

    .line 1158
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundLinkUrl:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-static/range {v20 .. v22}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1161
    .end local v11           #photoId:J
    :cond_194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v17

    .line 1162
    .local v17, videoUrl:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1085
    nop

    :sswitch_data_1b8
    .sparse-switch
        0x7f0900aa -> :sswitch_8
        0x7f090181 -> :sswitch_8e
        0x7f090184 -> :sswitch_8e
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 287
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    .line 289
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    .line 291
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 293
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 294
    const-string v2, "activity_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    .line 295
    const-string v2, "album_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAlbumId:Ljava/lang/String;

    .line 296
    const-string v2, "photo_ref"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    .line 297
    const-string v2, "photo_link_url"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundLinkUrl:Ljava/lang/String;

    .line 298
    const-string v2, "link_title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkTitle:Ljava/lang/String;

    .line 299
    const-string v2, "link_url"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkUrl:Ljava/lang/String;

    .line 300
    const-string v2, "photo_width"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundDesiredWidth:I

    .line 301
    const-string v2, "photo_height"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundDesiredHeight:I

    .line 302
    const-string v2, "photo_count"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMediaCount:I

    .line 304
    if-nez p1, :cond_e5

    const/4 v2, 0x1

    :goto_6b
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mShowAlbumOverlay:Z

    .line 306
    if-eqz p1, :cond_e7

    .line 307
    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_83

    .line 308
    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 310
    :cond_83
    const-string v2, "activity_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_97

    .line 311
    const-string v2, "activity_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    .line 314
    :cond_97
    const-string v2, "audience_data"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    .line 316
    const-string v2, "flagged_comments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 317
    .local v0, ids:[Ljava/lang/String;
    if-eqz v0, :cond_b2

    .line 318
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 321
    :cond_b2
    const-string v2, "operation_type"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    .line 322
    const-string v2, "mute_processed"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    .line 323
    const-string v2, "read_processed"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    .line 324
    const-string v2, "deep_link_logged"

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mDeepLinkLogged:Z

    .line 325
    const-string v2, "source_package_name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    .line 326
    const-string v2, "source_author_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourceAuthorId:Ljava/lang/String;

    .line 327
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    .line 334
    .end local v0           #ids:[Ljava/lang/String;
    :cond_e4
    :goto_e4
    return-void

    :cond_e5
    move v2, v3

    .line 304
    goto :goto_6b

    .line 330
    :cond_e7
    const-string v2, "refresh"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_e4

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->refresh()V

    goto :goto_e4
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 7
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 680
    packed-switch p1, :pswitch_data_18

    .line 687
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 682
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 683
    .local v0, context:Landroid/content/Context;
    new-instance v1, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_4

    .line 680
    nop

    :pswitch_data_18
    .packed-switch 0x1efab34d
        :pswitch_5
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 14
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 354
    .local v1, context:Landroid/content/Context;
    sget-boolean v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sResourcesLoaded:Z

    if-nez v4, :cond_39

    .line 355
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 357
    .local v2, res:Landroid/content/res/Resources;
    const v4, 0x7f0d0166

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    sput v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMaxWidth:I

    .line 358
    const v4, 0x7f0d0165

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    sput v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMinExposureLand:I

    .line 360
    const v4, 0x7f0d0164

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    sput v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMinExposurePort:I

    .line 362
    const v4, 0x7f0d016a

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    sput v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sAvatarMarginTop:I

    .line 364
    sput-boolean v5, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sResourcesLoaded:Z

    .line 367
    .end local v2           #res:Landroid/content/res/Resources;
    :cond_39
    const v4, 0x7f0300c4

    invoke-virtual {p1, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 370
    .local v3, view:Landroid/view/View;
    const v4, 0x7f090180

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    .line 371
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    const v7, 0x7f090181

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 373
    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/StreamOneUpListView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    .line 375
    new-instance v4, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-direct {v4, v1, v9, p0, v7}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    .line 376
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setLoading(Z)V

    .line 377
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    sget v7, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMaxWidth:I

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setMaxWidth(I)V

    .line 378
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 379
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 380
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 384
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v4, :cond_9c

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v4

    if-nez v4, :cond_9c

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v4, v7, :cond_143

    .line 386
    :cond_9c
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->bindStageMedia(Landroid/view/View;)Z

    .line 392
    :goto_9f
    const v4, 0x7f090065

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    .line 393
    const v4, 0x7f0900a9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 394
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    .line 395
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setLayoutListener(Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;)V

    .line 397
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    sget v7, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->sMaxWidth:I

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setMaxWidth(I)V

    .line 399
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v4, v7}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 401
    .local v0, circleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 402
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v4, p0, v7, v8, v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 405
    const v4, 0x7f0900aa

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    .line 406
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 407
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-lez v4, :cond_148

    move v4, v5

    :goto_ff
    invoke-virtual {v7, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 409
    new-instance v4, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$MyTextWatcher;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$MyTextWatcher;-><init>(Landroid/view/View;B)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 410
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 411
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    const v5, 0x1efab34d

    invoke-virtual {v4, v5, v9, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 427
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "show_keyboard"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_142

    .line 428
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    const-wide/16 v6, 0xfa

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 439
    :cond_142
    return-object v3

    .line 388
    .end local v0           #circleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    :cond_143
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->bindStageLink(Landroid/view/View;)Z

    goto/16 :goto_9f

    .restart local v0       #circleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    :cond_148
    move v4, v6

    .line 407
    goto :goto_ff
.end method

.method public final onDestroyView()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 618
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 619
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    .line 621
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 624
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentButton:Landroid/view/View;

    .line 625
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDestroyView()V

    .line 626
    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1578
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 19
    .parameter "which"
    .parameter "args"

    .prologue
    .line 1582
    const-string v1, "comment_action"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    .line 1583
    .local v13, mCommentAction:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez v13, :cond_12

    .line 1584
    const-string v1, "StreamOneUp"

    const-string v2, "No actions for comment option dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1627
    :goto_11
    return-void

    .line 1587
    :cond_12
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v1

    move/from16 v0, p1

    if-lt v0, v1, :cond_22

    .line 1588
    const-string v1, "StreamOneUp"

    const-string v2, "Option selected outside the action list"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 1592
    :cond_22
    const-string v1, "comment_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1593
    .local v4, mCommentId:Ljava/lang/String;
    const-string v1, "comment_content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1594
    .local v10, mCommentContent:Ljava/lang/String;
    const-string v1, "plus_one_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1595
    .local v5, plusOneId:Ljava/lang/String;
    const-string v1, "plus_one_by_me"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v14

    .line 1596
    .local v14, plusOneByMe:Z
    const-string v1, "plus_one_count"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 1598
    .local v15, plusOneCount:I
    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sparse-switch v1, :sswitch_data_128

    goto :goto_11

    .line 1621
    :sswitch_5a
    const-string v1, "extra_comment_id"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    const v1, 0x7f08012b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f080134

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v6, 0x7f0801c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v2, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "hsouf_delete_comment"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 1600
    :sswitch_a9
    if-eqz v5, :cond_ad

    if-nez v14, :cond_c4

    .line 1601
    :cond_ad
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/service/EsService;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_11

    .line 1604
    :cond_c4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/service/EsService;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_11

    .line 1609
    :sswitch_da
    const-string v1, "extra_comment_id"

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v9, v4

    invoke-static/range {v6 .. v12}, Lcom/google/android/apps/plus/phone/Intents;->getEditCommentActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_11

    .line 1612
    :sswitch_105
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->doReportComment(ZLjava/lang/String;Z)V

    goto/16 :goto_11

    .line 1615
    :sswitch_10e
    const/4 v1, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->doReportComment(ZLjava/lang/String;Z)V

    goto/16 :goto_11

    .line 1618
    :sswitch_117
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->doReportComment(ZLjava/lang/String;Z)V

    goto/16 :goto_11

    .line 1624
    :sswitch_120
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v15}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showPlusOnePeople(Ljava/lang/String;I)V

    goto/16 :goto_11

    .line 1598
    nop

    :sswitch_data_128
    .sparse-switch
        0x21 -> :sswitch_5a
        0x22 -> :sswitch_105
        0x23 -> :sswitch_10e
        0x24 -> :sswitch_117
        0x25 -> :sswitch_a9
        0x26 -> :sswitch_da
        0x40 -> :sswitch_120
    .end sparse-switch
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 8
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1563
    const-string v1, "hsouf_source_app_install"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 1564
    const-string v1, "url_span"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/text/style/URLSpan;

    .line 1565
    .local v0, span:Landroid/text/style/URLSpan;
    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1566
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1574
    .end local v0           #span:Landroid/text/style/URLSpan;
    :cond_2b
    :goto_2b
    return-void

    .line 1569
    .restart local v0       #span:Landroid/text/style/URLSpan;
    :cond_2c
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->onSpanClick(Landroid/text/style/URLSpan;)V

    .line 1570
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_DECLINED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto :goto_2b
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 12
    .parameter "args"
    .parameter "tag"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1511
    const-string v0, "hsouf_delete_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1512
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1513
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    .line 1538
    :cond_25
    :goto_25
    return-void

    .line 1514
    :cond_26
    const-string v0, "hsouf_delete_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 1515
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1517
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_25

    .line 1518
    :cond_50
    const-string v0, "hsouf_report_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_86

    .line 1519
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "delete"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v8, "is_undo"

    invoke-virtual {p1, v8, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->moderateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1522
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_25

    .line 1523
    :cond_86
    const-string v0, "hsouf_mute_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 1524
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-nez v3, :cond_af

    :goto_9e
    invoke-static {v0, v1, v2, v5}, Lcom/google/android/apps/plus/service/EsService;->muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1526
    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_25

    :cond_af
    move v5, v6

    .line 1524
    goto :goto_9e

    .line 1527
    :cond_b1
    const-string v0, "hsouf_report_activity"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 1528
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->reportActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1529
    const/16 v0, 0x12

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_25

    .line 1530
    :cond_d6
    const-string v0, "hsouf_cancel_edits"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e7

    .line 1531
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_25

    .line 1532
    :cond_e7
    const-string v0, "hsouf_source_app_install"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 1533
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getBackgroundInstallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 1534
    .local v7, intent:Landroid/content/Intent;
    invoke-virtual {p0, v7, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1535
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_ACCEPTED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_25
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 16
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x5

    .line 1175
    instance-of v0, p2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    if-eqz v0, :cond_142

    .line 1176
    check-cast p2, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    .end local p2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getAuthorId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getActivityAuthorId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneByMe()Z

    move-result v6

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneCount()I

    move-result v8

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isFlagged()Z

    move-result v9

    if-nez v9, :cond_5a

    if-eqz v6, :cond_104

    const v0, 0x7f0803ce

    :goto_4a
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x25

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5a
    if-eqz v2, :cond_109

    const v0, 0x7f0803d6

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x26

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_6f
    if-nez v3, :cond_73

    if-eqz v2, :cond_86

    :cond_73
    const v0, 0x7f0803d5

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_86
    if-eqz v7, :cond_9d

    if-lez v8, :cond_9d

    const v0, 0x7f0803d7

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x40

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9d
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const v1, 0x7f0803cc

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_action"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_id"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_content"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentContent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "plus_one_id"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "plus_one_by_me"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "plus_one_count"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneCount()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "hsouf_delete_comment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->cancelPressedState()V

    .line 1182
    :cond_103
    :goto_103
    return-void

    .line 1176
    :cond_104
    const v0, 0x7f0803cd

    goto/16 :goto_4a

    :cond_109
    if-eqz v9, :cond_120

    const v0, 0x7f0803d2

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x23

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6f

    :cond_120
    const v0, 0x7f0803cf

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_137

    const/16 v0, 0x24

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6f

    :cond_137
    const/16 v0, 0x22

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_6f

    .line 1178
    .restart local p2
    :cond_142
    const-string v0, "StreamOneUp"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_103

    .line 1179
    const-string v0, "StreamOneUp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HostedStreamOneUpFragment.onItemClick: Some other view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_103
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 8
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 109
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_124

    :cond_b
    :goto_b
    return-void

    :pswitch_c
    if-eqz p2, :cond_14

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_47

    :cond_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->refresh()V

    :cond_17
    :goto_17
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->bindStage(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setFlaggedComments(Ljava/util/HashSet;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "mute"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_b

    :cond_47
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    if-nez v0, :cond_5a

    const-string v0, "extra_activity_id"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    :cond_5a
    const/16 v0, 0x8

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsMyActivity:Ljava/lang/Boolean;

    const/16 v0, 0xd

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_10a

    move v0, v1

    :goto_80
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityResharable:Z

    const/16 v0, 0xb

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_10d

    move v0, v1

    :goto_8b
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityPublic:Z

    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_110

    move v0, v1

    :goto_96
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    const/4 v0, 0x6

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_bc

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_bc

    new-instance v3, Landroid/util/Pair;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    :cond_bc
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->invalidateActionBar()V

    const/16 v0, 0xc

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_112

    move v0, v1

    :goto_c8
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    if-eqz v0, :cond_114

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const v3, 0x7f080141

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(I)V

    :goto_d7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    if-eqz v0, :cond_e9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListParent:Landroid/view/View;

    const v3, 0x7f090181

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setCanAnimate(Z)V

    :cond_e9
    const/16 v0, 0x14

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_11b

    const/16 v0, 0x13

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mEditableText:Ljava/lang/String;

    :goto_fd
    const/16 v0, 0x10

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_17

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->refresh()V

    goto/16 :goto_17

    :cond_10a
    move v0, v2

    goto/16 :goto_80

    :cond_10d
    move v0, v2

    goto/16 :goto_8b

    :cond_110
    move v0, v2

    goto :goto_96

    :cond_112
    move v0, v2

    goto :goto_c8

    :cond_114
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_d7

    :cond_11b
    const/16 v0, 0x12

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mEditableText:Ljava/lang/String;

    goto :goto_fd

    :pswitch_data_124
    .packed-switch 0x1efab34d
        :pswitch_c
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 998
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onLocationClick$75c560e7(Lcom/google/android/apps/plus/content/DbLocation;)V
    .registers 3
    .parameter "location"

    .prologue
    .line 1297
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/MapUtils;->showActivityOnMap(Landroid/content/Context;Lcom/google/android/apps/plus/content/DbLocation;)V

    .line 1298
    return-void
.end method

.method public final onMeasured(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    .prologue
    .line 1034
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-ne p1, v1, :cond_10

    .line 1036
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setContainerHeight(I)V

    .line 1073
    :cond_f
    :goto_f
    return-void

    .line 1037
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    if-ne p1, v1, :cond_f

    .line 1038
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getMeasuredHeight()I

    move-result v0

    .line 1043
    .local v0, footerHeight:I
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_f
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter "item"

    .prologue
    const v5, 0x7f0801c5

    const v4, 0x7f0801c4

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 502
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 504
    .local v0, context:Landroid/content/Context;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_13c

    .line 548
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_1b
    return v1

    .line 506
    :sswitch_1c
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 507
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    move v1, v3

    .line 509
    goto :goto_1b

    .line 512
    :sswitch_2a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/MapUtils;->showActivityOnMap(Landroid/content/Context;Lcom/google/android/apps/plus/content/DbLocation;)V

    move v1, v3

    .line 514
    goto :goto_1b

    .line 520
    :sswitch_35
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mEditableText:Ljava/lang/String;

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/phone/EditPostActivity;

    invoke-direct {v5, v0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "android.intent.action.EDIT"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "account"

    invoke-virtual {v5, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "activity_id"

    invoke-virtual {v5, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v5, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    move v1, v3

    .line 523
    goto :goto_1b

    .line 526
    :sswitch_5b
    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REMOVE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    const v1, 0x7f080123

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08013c

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {v1, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "hsouf_delete_activity"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v1, v3

    .line 528
    goto :goto_1b

    .line 531
    :sswitch_90
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    if-eqz v1, :cond_a7

    .line 532
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showPlusOnePeople(Ljava/lang/String;I)V

    :cond_a7
    move v1, v3

    .line 535
    goto/16 :goto_1b

    .line 538
    :sswitch_aa
    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    const v1, 0x7f08012a

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08013f

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {v1, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "activity_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "hsouf_report_activity"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v1, v3

    .line 540
    goto/16 :goto_1b

    .line 543
    :sswitch_eb
    const-string v1, "extra_activity_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_MUTE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-eqz v1, :cond_134

    const v1, 0x7f080129

    :goto_ff
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-eqz v1, :cond_138

    const v1, 0x7f08013e

    :goto_10a
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v1, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {v1, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "activity_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "hsouf_mute_activity"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v1, v3

    .line 545
    goto/16 :goto_1b

    .line 543
    :cond_134
    const v1, 0x7f080128

    goto :goto_ff

    :cond_138
    const v1, 0x7f08013d

    goto :goto_10a

    .line 504
    :sswitch_data_13c
    .sparse-switch
        0x7f09028f -> :sswitch_1c
        0x7f0902b9 -> :sswitch_aa
        0x7f0902bc -> :sswitch_35
        0x7f0902bd -> :sswitch_2a
        0x7f0902be -> :sswitch_eb
        0x7f0902bf -> :sswitch_5b
        0x7f0902c2 -> :sswitch_90
    .end sparse-switch
.end method

.method public final onPause()V
    .registers 4

    .prologue
    .line 594
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPause()V

    .line 596
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    if-eqz v2, :cond_c

    .line 597
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/OneUpImageView;->onStop()V

    .line 600
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    if-eqz v2, :cond_15

    .line 601
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/OneUpLinkView;->onStop()V

    .line 604
    :cond_15
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-eqz v2, :cond_35

    .line 605
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, i:I
    :goto_21
    if-ltz v0, :cond_35

    .line 606
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 607
    .local v1, v:Landroid/view/View;
    instance-of v2, v1, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v2, :cond_32

    .line 608
    check-cast v1, Lcom/google/android/apps/plus/views/OneUpBaseView;

    .end local v1           #v:Landroid/view/View;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStop()V

    .line 605
    :cond_32
    add-int/lit8 v0, v0, -0x1

    goto :goto_21

    .line 613
    .end local v0           #i:I
    :cond_35
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 614
    return-void
.end method

.method public final onPlaceClick(Ljava/lang/String;)V
    .registers 6
    .parameter "placeObfuscatedId"

    .prologue
    .line 1302
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1303
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1305
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1307
    .end local v0           #intent:Landroid/content/Intent;
    :cond_14
    return-void
.end method

.method public final onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .registers 5
    .parameter "entityId"
    .parameter "plusOneData"

    .prologue
    .line 1369
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->isPostPlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1370
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1371
    .local v0, activity:Landroid/app/Activity;
    if-eqz p2, :cond_18

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1372
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    .line 1377
    .end local v0           #activity:Landroid/app/Activity;
    :cond_17
    :goto_17
    return-void

    .line 1374
    .restart local v0       #activity:Landroid/app/Activity;
    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    goto :goto_17
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 5
    .parameter "actionBar"

    .prologue
    .line 444
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 445
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 446
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityResharable:Z

    if-eqz v0, :cond_14

    .line 447
    const/4 v0, 0x0

    const v1, 0x7f020093

    const v2, 0x7f080124

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 450
    :cond_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    .line 451
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 5
    .parameter "menu"

    .prologue
    const/4 v2, 0x1

    .line 470
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 472
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLocationData:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v1, :cond_12

    .line 473
    const v1, 0x7f0902bd

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 476
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsMyActivity:Ljava/lang/Boolean;

    if-eqz v1, :cond_32

    .line 477
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsMyActivity:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4b

    .line 478
    const v1, 0x7f0902bc

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 479
    const v1, 0x7f0902bf

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 493
    :cond_32
    :goto_32
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPlusOnedByData:Landroid/util/Pair;

    if-eqz v1, :cond_40

    .line 494
    const v1, 0x7f0902c2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 497
    :cond_40
    const v1, 0x7f09028f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 498
    return-void

    .line 481
    :cond_4b
    const v1, 0x7f0902b9

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 483
    const v1, 0x7f0902be

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 484
    .local v0, item:Landroid/view/MenuItem;
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 485
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mIsActivityMuted:Z

    if-eqz v1, :cond_6a

    .line 486
    const v1, 0x7f080129

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_32

    .line 488
    :cond_6a
    const v1, 0x7f080128

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_32
.end method

.method public final onResume()V
    .registers 6

    .prologue
    .line 554
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onResume()V

    .line 556
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    if-eqz v3, :cond_c

    .line 557
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mImageView:Lcom/google/android/apps/plus/views/OneUpImageView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/OneUpImageView;->onStart()V

    .line 560
    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    if-eqz v3, :cond_15

    .line 561
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mLinkView:Lcom/google/android/apps/plus/views/OneUpLinkView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/OneUpLinkView;->onStart()V

    .line 564
    :cond_15
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-eqz v3, :cond_35

    .line 565
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, i:I
    :goto_21
    if-ltz v0, :cond_35

    .line 566
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 567
    .local v2, v:Landroid/view/View;
    instance-of v3, v2, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v3, :cond_32

    .line 568
    check-cast v2, Lcom/google/android/apps/plus/views/OneUpBaseView;

    .end local v2           #v:Landroid/view/View;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStart()V

    .line 565
    :cond_32
    add-int/lit8 v0, v0, -0x1

    goto :goto_21

    .line 573
    .end local v0           #i:I
    :cond_35
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 575
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_5f

    .line 576
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v3

    if-nez v3, :cond_5f

    .line 577
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 578
    .local v1, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    #calls: Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;->access$400(Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 582
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_5f
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_7b

    .line 583
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v3

    if-nez v3, :cond_7b

    .line 584
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    .line 585
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    .line 589
    :cond_7b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    .line 590
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 630
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 632
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 633
    const-string v1, "pending_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 635
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_21

    .line 636
    const-string v1, "activity_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 638
    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_2c

    .line 639
    const-string v1, "audience_data"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 641
    :cond_2c
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_46

    .line 642
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 643
    .local v0, ids:[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 644
    const-string v1, "flagged_comments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 647
    .end local v0           #ids:[Ljava/lang/String;
    :cond_46
    const-string v1, "operation_type"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mOperationType:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 648
    const-string v1, "mute_processed"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mMuteProcessed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 649
    const-string v1, "read_processed"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mReadProcessed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 650
    const-string v1, "deep_link_logged"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mDeepLinkLogged:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 651
    const-string v1, "source_package_name"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    const-string v1, "source_author_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourceAuthorId:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 4
    .parameter "args"

    .prologue
    .line 338
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 339
    const-string v0, "auto_play_music"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAutoPlay:Z

    .line 340
    return-void
.end method

.method public final onSkyjamBuyClick(Ljava/lang/String;)V
    .registers 6
    .parameter "url"

    .prologue
    .line 1481
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1482
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1483
    .local v1, intent:Landroid/content/Intent;
    const/high16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1484
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1485
    const-string v2, "com.android.vending"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1486
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_2b

    .line 1487
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1489
    :cond_2b
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1490
    return-void
.end method

.method public final onSkyjamListenClick(Ljava/lang/String;)V
    .registers 6
    .parameter "url"

    .prologue
    .line 1494
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1495
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.music.SHARED_PLAY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1496
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "url"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1497
    const-string v2, "authAccount"

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1499
    const-string v2, "accountType"

    const-string v3, "com.google"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1500
    const-string v2, "com.google.android.music"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1501
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_49

    .line 1502
    new-instance v1, Landroid/content/Intent;

    .end local v1           #intent:Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1503
    .restart local v1       #intent:Landroid/content/Intent;
    const/high16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1504
    const-string v2, "market://details?id=com.google.android.music"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1506
    :cond_49
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1507
    return-void
.end method

.method public final onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/text/style/URLSpan;Ljava/lang/String;)V
    .registers 16
    .parameter "appName"
    .parameter
    .parameter "data"
    .parameter "span"
    .parameter "authorId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/text/style/URLSpan;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1423
    .local p2, packageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 1424
    .local v3, packageInstalled:Z
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1425
    .local v4, packageManager:Landroid/content/pm/PackageManager;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_11
    :goto_11
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1426
    .local v5, packageName:Ljava/lang/String;
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPackageInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1427
    const/4 v3, 0x1

    .line 1428
    invoke-static {v4, v5, p3}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getContinueIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1430
    .local v2, intent:Landroid/content/Intent;
    if-eqz v2, :cond_11

    .line 1432
    :try_start_2a
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1433
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v9, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V
    :try_end_3e
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2a .. :try_end_3e} :catch_3f

    .line 1477
    .end local v2           #intent:Landroid/content/Intent;
    .end local v5           #packageName:Ljava/lang/String;
    :goto_3e
    return-void

    .line 1437
    .restart local v2       #intent:Landroid/content/Intent;
    .restart local v5       #packageName:Ljava/lang/String;
    :catch_3f
    move-exception v6

    const-string v6, "StreamOneUp"

    const/4 v7, 0x5

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 1438
    const-string v6, "StreamOneUp"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Failed to start deep linked Activity with "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 1445
    .end local v2           #intent:Landroid/content/Intent;
    .end local v5           #packageName:Ljava/lang/String;
    :cond_5e
    invoke-virtual {p4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9a

    .line 1446
    if-eqz v3, :cond_7f

    .line 1449
    invoke-virtual {p0, p4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->onSpanClick(Landroid/text/style/URLSpan;)V

    .line 1450
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_UNRESOLVED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v9, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto :goto_3e

    .line 1454
    :cond_7f
    invoke-static {v4}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPlayStoreInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v6

    if-nez v6, :cond_9a

    .line 1455
    invoke-virtual {p0, p4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->onSpanClick(Landroid/text/style/URLSpan;)V

    .line 1456
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v9, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto :goto_3e

    .line 1462
    :cond_9a
    const/4 v6, 0x0

    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourcePackageName:Ljava/lang/String;

    .line 1463
    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mSourceAuthorId:Ljava/lang/String;

    .line 1467
    const/4 v6, 0x0

    const v7, 0x7f080401

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0801c6

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    const v9, 0x7f0801c8

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1471
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v6, 0x0

    invoke-virtual {v0, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1472
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "url_span"

    invoke-virtual {v6, v7, p4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1473
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "hsouf_source_app_install"

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1475
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v8, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_OFFERED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v9, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto/16 :goto_3e
.end method

.method public final onSourceAppNameClick$1b7460f0(Ljava/util/List;)V
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, packageNames:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1390
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1391
    .local v2, packageManager:Landroid/content/pm/PackageManager;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1392
    .local v3, packageName:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPackageInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 1393
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getLaunchIntent(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1394
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CONSUMED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v7, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1418
    .end local v3           #packageName:Ljava/lang/String;
    :goto_3c
    return-void

    .line 1402
    :cond_3d
    invoke-static {v2}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->isPlayStoreInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v4

    if-nez v4, :cond_73

    .line 1403
    const v4, 0x7f080402

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x104000a

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v4, v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1408
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1409
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "hsouf_source_app_install_unavailable"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1410
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CLICKED_BUT_PLAY_STORE_NOT_INSTALLED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v7, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto :goto_3c

    .line 1414
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_73
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->getInstallIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1415
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v7, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    goto :goto_3c
.end method

.method public final onSourceAppNameLinkEnabled()V
    .registers 2

    .prologue
    .line 1381
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mDeepLinkLogged:Z

    if-nez v0, :cond_c

    .line 1382
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_ATTRIBUTION_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 1383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mDeepLinkLogged:Z

    .line 1385
    :cond_c
    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .registers 15
    .parameter "span"

    .prologue
    const/4 v12, 0x1

    .line 1311
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v9

    .line 1312
    .local v9, url:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual {v10}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 1314
    .local v2, context:Landroid/content/Context;
    const-string v10, "acl:"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_ae

    .line 1315
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    if-nez v10, :cond_a8

    .line 1316
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-nez v10, :cond_4e

    .line 1317
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getAclText()Ljava/lang/String;

    move-result-object v0

    .line 1319
    .local v0, aclText:Ljava/lang/String;
    const v10, 0x7f0803f4

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_4f

    .line 1320
    const v10, 0x7f0803f8

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1337
    .local v1, audienceDescription:Ljava/lang/String;
    :goto_37
    if-eqz v1, :cond_4e

    .line 1338
    const v10, 0x7f0801c4

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v0, v1, v10, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v3

    .line 1340
    .local v3, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    const-string v11, "hsouf_audience"

    invoke-virtual {v3, v10, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1365
    .end local v0           #aclText:Ljava/lang/String;
    .end local v1           #audienceDescription:Ljava/lang/String;
    .end local v3           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_4e
    :goto_4e
    return-void

    .line 1321
    .restart local v0       #aclText:Ljava/lang/String;
    :cond_4f
    const v10, 0x7f0803f5

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_64

    .line 1323
    const v10, 0x7f0803f9

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1       #audienceDescription:Ljava/lang/String;
    goto :goto_37

    .line 1324
    .end local v1           #audienceDescription:Ljava/lang/String;
    :cond_64
    const v10, 0x7f0803f6

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_79

    .line 1326
    const v10, 0x7f0803fa

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1       #audienceDescription:Ljava/lang/String;
    goto :goto_37

    .line 1327
    .end local v1           #audienceDescription:Ljava/lang/String;
    :cond_79
    const v10, 0x7f0803f7

    invoke-virtual {v2, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v0, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_93

    .line 1329
    const v10, 0x7f0803fb

    new-array v11, v12, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v0, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1       #audienceDescription:Ljava/lang/String;
    goto :goto_37

    .line 1331
    .end local v1           #audienceDescription:Ljava/lang/String;
    :cond_93
    const/4 v1, 0x0

    .line 1332
    .restart local v1       #audienceDescription:Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v10, v11}, Lcom/google/android/apps/plus/service/EsService;->getActivityAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1334
    const/16 v10, 0x30

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showProgressDialog(I)V

    goto :goto_37

    .line 1345
    .end local v0           #aclText:Ljava/lang/String;
    .end local v1           #audienceDescription:Ljava/lang/String;
    :cond_a8
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    goto :goto_4e

    .line 1347
    :cond_ae
    const-string v10, "+1:"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_d4

    .line 1348
    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 1349
    .local v7, parts:[Ljava/lang/String;
    if-eqz v7, :cond_4e

    array-length v10, v7

    const/4 v11, 0x3

    if-ne v10, v11, :cond_4e

    .line 1350
    aget-object v10, v7, v12

    const/4 v11, 0x2

    aget-object v11, v7, v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-direct {p0, v10, v11}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->showPlusOnePeople(Ljava/lang/String;I)V

    goto/16 :goto_4e

    .line 1352
    .end local v7           #parts:[Ljava/lang/String;
    :cond_d4
    const-string v10, "https://plus.google.com/s/%23"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_fc

    .line 1353
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "#"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v11, 0x1d

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1354
    .local v8, searchKey:Ljava/lang/String;
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v10, v8}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 1355
    .local v6, intent:Landroid/content/Intent;
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_4e

    .line 1357
    .end local v6           #intent:Landroid/content/Intent;
    .end local v8           #searchKey:Ljava/lang/String;
    :cond_fc
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/phone/Intents;->isProfileUrl(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_115

    .line 1358
    invoke-static {v9}, Lcom/google/android/apps/plus/phone/Intents;->getPersonIdFromProfileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1359
    .local v5, gaiaId:Ljava/lang/String;
    const-string v10, "extra_gaia_id"

    invoke-static {v10, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 1361
    .local v4, extras:Landroid/os/Bundle;
    sget-object v10, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v10, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1363
    .end local v4           #extras:Landroid/os/Bundle;
    .end local v5           #gaiaId:Ljava/lang/String;
    :cond_115
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v10, v11, v9}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto/16 :goto_4e
.end method

.method public final onUserImageClick(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "gaiaId"
    .parameter "authorName"

    .prologue
    .line 1253
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1254
    .local v0, context:Landroid/content/Context;
    const-string v2, "extra_gaia_id"

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1255
    .local v1, extras:Landroid/os/Bundle;
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1256
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->startActivity(Landroid/content/Intent;)V

    .line 1258
    return-void
.end method

.method public final refresh()V
    .registers 4

    .prologue
    .line 1005
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    .line 1007
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_1d

    .line 1008
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    .line 1011
    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->updateProgressIndicator()V

    .line 1012
    return-void
.end method

.method protected final updateProgressIndicator()V
    .registers 4

    .prologue
    .line 1020
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    .line 1021
    .local v0, actionBar:Lcom/google/android/apps/plus/views/HostActionBar;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    if-eqz v1, :cond_26

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_26

    .line 1022
    :cond_14
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 1027
    :goto_17
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    if-eqz v1, :cond_25

    .line 1028
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamOneUpFragment;->mActivityRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2a

    const/4 v1, 0x1

    :goto_22
    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->setLoading(Z)V

    .line 1030
    :cond_25
    return-void

    .line 1024
    :cond_26
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_17

    .line 1028
    :cond_2a
    const/4 v1, 0x0

    goto :goto_22
.end method
