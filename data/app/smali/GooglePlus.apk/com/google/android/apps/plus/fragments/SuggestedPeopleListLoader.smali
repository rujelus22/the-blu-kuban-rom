.class public final Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "SuggestedPeopleListLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mFirstRun:Z

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mProjection:[Ljava/lang/String;

.field private final mRefreshDataOnStart:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Z)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "projection"
    .parameter "refreshDataOnStart"

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mFirstRun:Z

    .line 41
    iput-boolean p4, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mRefreshDataOnStart:Z

    .line 42
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->setUri(Landroid/net/Uri;)V

    .line 43
    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 45
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mProjection:[Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mProjection:[Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mFirstRun:Z

    if-eqz v1, :cond_20

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mRefreshDataOnStart:Z

    if-eqz v1, :cond_20

    const/4 v1, 0x1

    :goto_12
    invoke-static {v3, v4, v5, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;ZZ)Landroid/database/Cursor;

    move-result-object v0

    .line 56
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_1f

    .line 57
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mFirstRun:Z

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 61
    :cond_1f
    return-object v0

    .end local v0           #cursor:Landroid/database/Cursor;
    :cond_20
    move v1, v2

    .line 53
    goto :goto_12
.end method
