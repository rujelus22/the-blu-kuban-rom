.class public Lcom/google/android/apps/plus/api/ApiaryErrorResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ApiaryErrorResponse.java"


# static fields
.field public static final AGE_RESTRICTED:Ljava/lang/String; = "AGE_RESTRICTED"

.field public static final APP_UPGRADE_REQUIRED:Ljava/lang/String; = "APP_UPGRADE_REQUIRED"

.field public static final BAD_PROFILE:Ljava/lang/String; = "BAD_PROFILE"

.field public static final ES_BLOCKED_FOR_DOMAIN_BY_ADMIN:Ljava/lang/String; = "ES_BLOCKED_FOR_DOMAIN_BY_ADMIN"

.field public static final ES_STREAM_POST_RESTRICTIONS_NOT_SUPPORTED:Ljava/lang/String; = "ES_STREAM_POST_RESTRICTIONS_NOT_SUPPORTED"

.field public static final FORBIDDEN:Ljava/lang/String; = "FORBIDDEN"

.field public static final HAS_PLUSONE_OPT_IN_REQUIRED:Ljava/lang/String; = "HAS_PLUSONE_OPT_IN_REQUIRED"

.field public static final INVALID_ACTION_TOKEN:Ljava/lang/String; = "INVALID_ACTION_TOKEN"

.field public static final INVALID_CREDENTIALS:Ljava/lang/String; = "INVALID_CREDENTIALS"

.field public static final INVALID_VALUE:Ljava/lang/String; = "INVALID_VALUE"

.field public static final JSON:Lcom/google/android/apps/plus/json/EsJson; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/api/ApiaryErrorResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final NETWORK_ERROR:Ljava/lang/String; = "NETWORK_ERROR"

.field public static final NOT_FOUND:Ljava/lang/String; = "NOT_FOUND"

.field public static final OUT_OF_BOX_REQUIRED:Ljava/lang/String; = "OUT_OF_BOX_REQUIRED"

.field public static final PERMISSION_ERROR:Ljava/lang/String; = "PERMISSION_ERROR"

.field public static final SERVICE_UNAVAILABLE:Ljava/lang/String; = "SERVICE_UNAVAILABLE"


# instance fields
.field public error:Lcom/google/android/apps/plus/api/ApiaryError;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/plus/api/ApiaryError;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "error"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method


# virtual methods
.method public getErrorMessage()Ljava/lang/String;
    .registers 6

    .prologue
    .line 69
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    if-eqz v4, :cond_30

    .line 70
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    if-eqz v4, :cond_f

    .line 71
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    .line 83
    :goto_e
    return-object v4

    .line 73
    :cond_f
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    if-eqz v4, :cond_30

    .line 74
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v3, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    .line 75
    .local v3, nestedErrors:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/ApiaryError;>;"
    const/4 v0, 0x0

    .local v0, i:I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, n:I
    :goto_1e
    if-ge v0, v1, :cond_30

    .line 76
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryError;

    .line 77
    .local v2, nestedError:Lcom/google/android/apps/plus/api/ApiaryError;
    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    if-eqz v4, :cond_2d

    .line 78
    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->message:Ljava/lang/String;

    goto :goto_e

    .line 75
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 83
    .end local v0           #i:I
    .end local v1           #n:I
    .end local v2           #nestedError:Lcom/google/android/apps/plus/api/ApiaryError;
    .end local v3           #nestedErrors:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/ApiaryError;>;"
    :cond_30
    const/4 v4, 0x0

    goto :goto_e
.end method

.method public getErrorType()Ljava/lang/String;
    .registers 6

    .prologue
    .line 48
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    if-eqz v4, :cond_30

    .line 49
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    if-eqz v4, :cond_f

    .line 50
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    .line 62
    :goto_e
    return-object v4

    .line 52
    :cond_f
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v4, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    if-eqz v4, :cond_30

    .line 53
    iget-object v4, p0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->error:Lcom/google/android/apps/plus/api/ApiaryError;

    iget-object v3, v4, Lcom/google/android/apps/plus/api/ApiaryError;->errors:Ljava/util/ArrayList;

    .line 54
    .local v3, nestedErrors:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/ApiaryError;>;"
    const/4 v0, 0x0

    .local v0, i:I
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, n:I
    :goto_1e
    if-ge v0, v1, :cond_30

    .line 55
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/ApiaryError;

    .line 56
    .local v2, nestedError:Lcom/google/android/apps/plus/api/ApiaryError;
    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    if-eqz v4, :cond_2d

    .line 57
    iget-object v4, v2, Lcom/google/android/apps/plus/api/ApiaryError;->reason:Ljava/lang/String;

    goto :goto_e

    .line 54
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    goto :goto_1e

    .line 62
    .end local v0           #i:I
    .end local v1           #n:I
    .end local v2           #nestedError:Lcom/google/android/apps/plus/api/ApiaryError;
    .end local v3           #nestedErrors:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/ApiaryError;>;"
    :cond_30
    const/4 v4, 0x0

    goto :goto_e
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/json/EsJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
