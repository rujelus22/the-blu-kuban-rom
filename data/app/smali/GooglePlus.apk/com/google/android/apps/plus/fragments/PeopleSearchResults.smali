.class public Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
.super Ljava/lang/Object;
.source "PeopleSearchResults.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mContacts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field private mCursorValid:Z

.field private final mGaiaIdsAndCircles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mGaiaIdsAndCirclesLoaded:Z

.field private mHasMoreResults:Z

.field private mIncludePeopleInCircles:Z

.field private final mLocalProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalProfilesLoaded:Z

.field private mMyPersonId:Ljava/lang/String;

.field private mNextId:J

.field private final mPublicProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mQuery:Ljava/lang/String;

.field private mToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 46
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "lookup_key"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "matched_email"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "email"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "phone"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "snippet"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->PROJECTION:[Ljava/lang/String;

    .line 523
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    .line 140
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    .line 143
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 13
    .parameter "in"

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 473
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    .line 140
    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    .line 143
    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    .line 147
    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    .line 474
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    .line 475
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    .line 476
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    .line 477
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_76

    move v0, v9

    :goto_40
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    .line 478
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_78

    :goto_48
    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    .line 479
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 480
    .local v8, publicCount:I
    const/4 v7, 0x0

    .local v7, i:I
    :goto_4f
    if-ge v7, v8, :cond_7a

    .line 481
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, personId:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 483
    .local v2, gaiaId:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 484
    .local v3, name:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 485
    .local v4, profileType:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 486
    .local v5, avatarUrl:Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 487
    .local v6, snippet:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 480
    add-int/lit8 v7, v7, 0x1

    goto :goto_4f

    .end local v1           #personId:Ljava/lang/String;
    .end local v2           #gaiaId:Ljava/lang/String;
    .end local v3           #name:Ljava/lang/String;
    .end local v4           #profileType:I
    .end local v5           #avatarUrl:Ljava/lang/String;
    .end local v6           #snippet:Ljava/lang/String;
    .end local v7           #i:I
    .end local v8           #publicCount:I
    :cond_76
    move v0, v10

    .line 477
    goto :goto_40

    :cond_78
    move v9, v10

    .line 478
    goto :goto_48

    .line 490
    .restart local v7       #i:I
    .restart local v8       #publicCount:I
    :cond_7a
    return-void
.end method

.method public static onFinishContacts()V
    .registers 0

    .prologue
    .line 259
    return-void
.end method


# virtual methods
.method public final addContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "personId"
    .parameter "lookupKey"
    .parameter "name"
    .parameter "email"
    .parameter "phoneNumber"
    .parameter "phoneType"

    .prologue
    .line 255
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 256
    return-void
.end method

.method public final addGaiaIdAndCircles(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "gaiaId"
    .parameter "packedCircleIds"

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    return-void
.end method

.method public final addLocalProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 21
    .parameter "personId"
    .parameter "gaiaId"
    .parameter "name"
    .parameter "profileType"
    .parameter "avatarUrl"
    .parameter "packedCircleIds"
    .parameter "email"
    .parameter "phoneNumber"
    .parameter "phoneType"

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 242
    :goto_8
    return-void

    .line 240
    :cond_9
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public final addPublicProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "personId"
    .parameter "gaiaId"
    .parameter "name"
    .parameter "profileType"
    .parameter "avatarUrl"
    .parameter "snippet"

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 270
    :goto_8
    return-void

    .line 267
    :cond_9
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    goto :goto_8
.end method

.method public describeContents()I
    .registers 2

    .prologue
    .line 520
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public final getCursor()Landroid/database/Cursor;
    .registers 25

    .prologue
    .line 277
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    .line 470
    :goto_e
    return-object v17

    .line 281
    :cond_f
    new-instance v17, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v18, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->PROJECTION:[Ljava/lang/String;

    invoke-direct/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    .line 282
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    .line 284
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_34

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    move/from16 v17, v0

    if-nez v17, :cond_3b

    .line 285
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    goto :goto_e

    .line 291
    :cond_3b
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 292
    .local v10, gaiaIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 293
    .local v14, names:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 295
    .local v6, emails:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1b9

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :cond_5c
    :goto_5c
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_fe

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;

    .line 299
    .local v15, profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;
    iget-object v9, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->gaiaId:Ljava/lang/String;

    .line 300
    .local v9, gaiaId:Ljava/lang/String;
    iget-object v13, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->email:Ljava/lang/String;

    .line 302
    .local v13, matchedEmail:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->personId:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    aput-object v9, v18, v19

    const/16 v19, 0x4

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x5

    iget v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->profileType:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->avatarUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x7

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->packedCircleIds:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x8

    aput-object v13, v18, v19

    const/16 v19, 0x9

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xa

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->phoneNumber:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0xb

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->phoneType:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0xc

    const/16 v20, 0x0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 318
    invoke-virtual {v10, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 319
    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 320
    if-eqz v13, :cond_5c

    .line 321
    invoke-virtual {v6, v13}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5c

    .line 330
    .end local v9           #gaiaId:Ljava/lang/String;
    .end local v13           #matchedEmail:Ljava/lang/String;
    .end local v15           #profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;
    :cond_fe
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_108
    :goto_108
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1b9

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    .line 332
    .local v15, profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;
    iget-object v9, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->gaiaId:Ljava/lang/String;

    .line 333
    .restart local v9       #gaiaId:Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_108

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 339
    .local v3, circles:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_108

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->personId:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    aput-object v9, v18, v19

    const/16 v19, 0x4

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x5

    iget v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->profileType:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->avatarUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x7

    aput-object v3, v18, v19

    const/16 v19, 0x8

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x9

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xa

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xb

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xc

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->snippet:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 359
    invoke-virtual {v10, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 360
    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_108

    .line 364
    .end local v3           #circles:Ljava/lang/String;
    .end local v9           #gaiaId:Ljava/lang/String;
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v15           #profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;
    :cond_1b9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_2d9

    .line 372
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 373
    .local v12, mapByEmail:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11       #i$:Ljava/util/Iterator;
    :cond_1d4
    :goto_1d4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2d9

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;

    .line 376
    .local v4, contact:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;
    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1d4

    .line 377
    iget-object v5, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->email:Ljava/lang/String;

    .line 383
    .local v5, email:Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1d4

    .line 384
    invoke-virtual {v12, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    .line 388
    .local v7, existing:[Ljava/lang/Object;
    if-eqz v7, :cond_24d

    .line 389
    const/16 v17, 0x4

    aget-object v8, v7, v17

    check-cast v8, Ljava/lang/String;

    .line 392
    .local v8, existingName:Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_20e

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1d4

    .line 398
    :cond_20e
    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_1d4

    .line 399
    const/16 v17, 0x1

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->personId:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    .line 400
    const/16 v17, 0x2

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->lookupKey:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    .line 401
    const/16 v17, 0x4

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    .line 404
    const/16 v17, 0xa

    aget-object v17, v7, v17

    if-nez v17, :cond_23e

    .line 405
    const/16 v17, 0xa

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneNumber:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    .line 407
    :cond_23e
    const/16 v17, 0xb

    aget-object v17, v7, v17

    if-nez v17, :cond_1d4

    .line 408
    const/16 v17, 0xb

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneType:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    goto :goto_1d4

    .line 416
    .end local v8           #existingName:Ljava/lang/String;
    :cond_24d
    const/16 v17, 0xd

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x1

    add-long v20, v20, v18

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->personId:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x2

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->lookupKey:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x3

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x4

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x5

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x6

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x7

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x8

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x9

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->email:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0xa

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneNumber:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0xb

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneType:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0xc

    const/16 v18, 0x0

    aput-object v18, v16, v17

    .line 432
    .local v16, row:[Ljava/lang/Object;
    move-object/from16 v0, v16

    invoke-virtual {v12, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1d4

    .line 441
    .end local v4           #contact:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;
    .end local v5           #email:Ljava/lang/String;
    .end local v7           #existing:[Ljava/lang/Object;
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #mapByEmail:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/Object;>;"
    .end local v16           #row:[Ljava/lang/Object;
    :cond_2d9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11       #i$:Ljava/util/Iterator;
    :cond_2e3
    :goto_2e3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_384

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    .line 443
    .restart local v15       #profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;
    iget-object v9, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->gaiaId:Ljava/lang/String;

    .line 444
    .restart local v9       #gaiaId:Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2e3

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2e3

    .line 450
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->personId:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    aput-object v9, v18, v19

    const/16 v19, 0x4

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x5

    iget v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->profileType:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->avatarUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x7

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x8

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x9

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xa

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xb

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xc

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->snippet:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2e3

    .line 470
    .end local v9           #gaiaId:Ljava/lang/String;
    .end local v15           #profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;
    :cond_384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    goto/16 :goto_e
.end method

.method public final getPublicProfileCount()I
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getQuery()Ljava/lang/String;
    .registers 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method public final hasMoreResults()Z
    .registers 2

    .prologue
    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    return v0
.end method

.method public final isParcelable()Z
    .registers 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_13

    const/4 v0, 0x1

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final onFinishGaiaIdsAndCircles()V
    .registers 2

    .prologue
    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    .line 225
    return-void
.end method

.method public final onFinishLocalProfiles()V
    .registers 2

    .prologue
    .line 245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    .line 246
    return-void
.end method

.method public final onStartContacts()V
    .registers 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 250
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    .line 251
    return-void
.end method

.method public final onStartGaiaIdsAndCircles()V
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    .line 217
    return-void
.end method

.method public final onStartLocalProfiles()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 229
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    .line 230
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    .line 231
    return-void
.end method

.method public final setHasMoreResults(Z)V
    .registers 2
    .parameter "hasMoreResults"

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    .line 176
    return-void
.end method

.method public final setIncludePeopleInCircles(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    .line 212
    return-void
.end method

.method public final setMyProfile(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public final setQueryString(Ljava/lang/String;)V
    .registers 4
    .parameter "queryString"

    .prologue
    const/4 v1, 0x0

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 205
    :goto_9
    return-void

    .line 199
    :cond_a
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 202
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    .line 203
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    goto :goto_9
.end method

.method public final setToken(Ljava/lang/String;)V
    .registers 2
    .parameter "token"

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 9
    .parameter "dest"
    .parameter "flags"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 497
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 498
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 499
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 500
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    if-eqz v3, :cond_55

    move v3, v4

    :goto_16
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 501
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    if-eqz v3, :cond_57

    :goto_1d
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 502
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 503
    .local v2, publicCount:I
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 504
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2a
    if-ge v0, v2, :cond_59

    .line 505
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    .line 506
    .local v1, profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;
    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->personId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 507
    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->gaiaId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 508
    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 509
    iget v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->profileType:I

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 510
    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->avatarUrl:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 511
    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->snippet:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 504
    add-int/lit8 v0, v0, 0x1

    goto :goto_2a

    .end local v0           #i:I
    .end local v1           #profile:Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;
    .end local v2           #publicCount:I
    :cond_55
    move v3, v5

    .line 500
    goto :goto_16

    :cond_57
    move v4, v5

    .line 501
    goto :goto_1d

    .line 513
    .restart local v0       #i:I
    .restart local v2       #publicCount:I
    :cond_59
    return-void
.end method
