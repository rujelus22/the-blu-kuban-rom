.class public final Lcom/google/android/apps/plus/content/EsAnalyticsData;
.super Ljava/lang/Object;
.source "EsAnalyticsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
    }
.end annotation


# static fields
.field private static isTabletDevice:Ljava/lang/Boolean;

.field private static final runtime:Ljava/lang/Runtime;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 79
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    return-void
.end method

.method public static bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p2, events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_40

    move-result-object v1

    .line 121
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 123
    :try_start_b
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 124
    .local v4, values:Landroid/content/ContentValues;
    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v0

    .line 125
    .local v0, clientOzEventJson:Lcom/google/api/services/plusi/model/ClientOzEventJson;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_39

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ClientOzEvent;

    .line 126
    .local v2, event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    const-string v5, "event_data"

    invoke-virtual {v0, v2}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 127
    const-string v5, "analytics_events"

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_33
    .catchall {:try_start_b .. :try_end_33} :catchall_34

    goto :goto_18

    .line 131
    .end local v0           #clientOzEventJson:Lcom/google/api/services/plusi/model/ClientOzEventJson;
    .end local v2           #event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #values:Landroid/content/ContentValues;
    :catchall_34
    move-exception v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5

    .line 129
    .restart local v0       #clientOzEventJson:Lcom/google/api/services/plusi/model/ClientOzEventJson;
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #values:Landroid/content/ContentValues;
    :cond_39
    :try_start_39
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3c
    .catchall {:try_start_39 .. :try_end_3c} :catchall_34

    .line 131
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 132
    .end local v0           #clientOzEventJson:Lcom/google/api/services/plusi/model/ClientOzEventJson;
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #values:Landroid/content/ContentValues;
    :goto_3f
    return-void

    .line 118
    :catch_40
    move-exception v5

    goto :goto_3f
.end method

.method public static createClientOzEvent(Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JJLandroid/os/Bundle;)Lcom/google/api/services/plusi/model/ClientOzEvent;
    .registers 41
    .parameter "action"
    .parameter "startView"
    .parameter "endView"
    .parameter "startTime"
    .parameter "endTime"
    .parameter "extras"

    .prologue
    .line 270
    new-instance v4, Lcom/google/api/services/plusi/model/ClientOzEvent;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/ClientOzEvent;-><init>()V

    .line 271
    .local v4, clientOzEvent:Lcom/google/api/services/plusi/model/ClientOzEvent;
    const/16 v18, 0x0

    .line 274
    .local v18, namespace:Ljava/lang/String;
    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->clientTimeMsec:Ljava/lang/Long;

    .line 276
    new-instance v22, Lcom/google/api/services/plusi/model/OzEvent;

    invoke-direct/range {v22 .. v22}, Lcom/google/api/services/plusi/model/OzEvent;-><init>()V

    .line 277
    .local v22, ozEvent:Lcom/google/api/services/plusi/model/OzEvent;
    new-instance v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/FavaDiagnostics;-><init>()V

    .line 280
    .local v10, favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;
    const-wide/16 v30, 0x0

    cmp-long v30, p3, v30

    if-lez v30, :cond_5b6

    cmp-long v30, p5, p3

    if-ltz v30, :cond_5b6

    .line 281
    sub-long v30, p5, p3

    move-wide/from16 v0, v30

    long-to-int v0, v0

    move/from16 v30, v0

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    .line 287
    :goto_32
    if-eqz p0, :cond_96

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/analytics/OzActions;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 289
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    if-eqz v30, :cond_96

    .line 290
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 292
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_96

    .line 293
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "Action name: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " namespace: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " typeNum: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_96
    if-eqz p1, :cond_118

    .line 304
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewData()Lcom/google/api/services/plusi/model/OutputData;

    move-result-object v24

    .line 305
    .local v24, startViewData:Lcom/google/api/services/plusi/model/OutputData;
    if-eqz v24, :cond_a4

    .line 306
    move-object/from16 v0, v24

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/OzEvent;->startViewData:Lcom/google/api/services/plusi/model/OutputData;

    .line 309
    :cond_a4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/analytics/OzViews;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 310
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    if-eqz v30, :cond_118

    .line 311
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_118

    .line 312
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "StartView name: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " namespace: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " typeNum: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " filterType: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getFilterType(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " tab: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getTab(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    .end local v24           #startViewData:Lcom/google/api/services/plusi/model/OutputData;
    :cond_118
    if-eqz p2, :cond_1a2

    .line 325
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewData()Lcom/google/api/services/plusi/model/OutputData;

    move-result-object v8

    .line 326
    .local v8, endViewData:Lcom/google/api/services/plusi/model/OutputData;
    if-eqz v8, :cond_124

    .line 327
    move-object/from16 v0, v22

    iput-object v8, v0, Lcom/google/api/services/plusi/model/OzEvent;->endViewData:Lcom/google/api/services/plusi/model/OutputData;

    .line 330
    :cond_124
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/analytics/OzViews;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 331
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    if-eqz v30, :cond_1a2

    .line 332
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 333
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_1a2

    .line 334
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "EndView name: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " namespace: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " typeNum: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " filterType: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getFilterType(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " tab: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getTab(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    .end local v8           #endViewData:Lcom/google/api/services/plusi/model/OutputData;
    :cond_1a2
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->actionType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    if-nez v30, :cond_1ae

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->endView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    if-eqz v30, :cond_20a

    .line 347
    :cond_1ae
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    if-nez v30, :cond_20a

    .line 348
    if-nez v18, :cond_5c2

    .line 349
    sget-object v30, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/analytics/OzViews;->getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 359
    :goto_1c0
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_20a

    .line 360
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "StartView name: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " namespace: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " typeNum: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    :cond_20a
    if-eqz p7, :cond_623

    .line 370
    const-string v30, "extra_start_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_25e

    .line 371
    const-string v30, "extra_start_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v25

    .line 372
    .local v25, startViewExtras:Landroid/os/Bundle;
    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getGaiaId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v13

    .line 373
    .local v13, gaiaId:Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_25e

    .line 374
    new-instance v30, Lcom/google/api/services/plusi/model/ClientOutputData;

    invoke-direct/range {v30 .. v30}, Lcom/google/api/services/plusi/model/ClientOutputData;-><init>()V

    move-object/from16 v0, v30

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->startViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    .line 375
    iget-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->startViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v0, v13}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->setUserInfo(Lcom/google/api/services/plusi/model/ClientOutputData;Ljava/lang/String;)V

    .line 376
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_25e

    .line 377
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: start view target gaiaId: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    .end local v13           #gaiaId:Ljava/lang/String;
    .end local v25           #startViewExtras:Landroid/os/Bundle;
    :cond_25e
    const-string v30, "extra_end_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_2b0

    .line 383
    const-string v30, "extra_end_view_extras"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v9

    .line 384
    .local v9, endViewExtras:Landroid/os/Bundle;
    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->getGaiaId(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v13

    .line 386
    .restart local v13       #gaiaId:Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_2b0

    .line 387
    new-instance v30, Lcom/google/api/services/plusi/model/ClientOutputData;

    invoke-direct/range {v30 .. v30}, Lcom/google/api/services/plusi/model/ClientOutputData;-><init>()V

    move-object/from16 v0, v30

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->endViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    .line 388
    iget-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->endViewData:Lcom/google/api/services/plusi/model/ClientOutputData;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    invoke-static {v0, v13}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->setUserInfo(Lcom/google/api/services/plusi/model/ClientOutputData;Ljava/lang/String;)V

    .line 389
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_2b0

    .line 390
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: end view target gaiaId: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    .end local v9           #endViewExtras:Landroid/os/Bundle;
    .end local v13           #gaiaId:Ljava/lang/String;
    :cond_2b0
    const-string v30, "extra_gaia_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_30f

    .line 396
    const-string v30, "extra_gaia_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 398
    .restart local v13       #gaiaId:Ljava/lang/String;
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_30f

    .line 399
    new-instance v30, Lcom/google/api/services/plusi/model/ClientActionData;

    invoke-direct/range {v30 .. v30}, Lcom/google/api/services/plusi/model/ClientActionData;-><init>()V

    move-object/from16 v0, v30

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->actionData:Lcom/google/api/services/plusi/model/ClientActionData;

    .line 400
    new-instance v26, Ljava/util/ArrayList;

    const/16 v30, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 401
    .local v26, targetGaiaIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, v26

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    iget-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->actionData:Lcom/google/api/services/plusi/model/ClientActionData;

    move-object/from16 v30, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ClientActionData;->obfuscatedGaiaId:Ljava/util/List;

    .line 403
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_30f

    .line 404
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: target gaiaId: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    .end local v13           #gaiaId:Ljava/lang/String;
    .end local v26           #targetGaiaIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_30f
    const-string v30, "extra_activity_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_363

    const-string v30, "extra_comment_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_363

    const-string v30, "extra_notification_read"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_363

    const-string v30, "extra_notification_types"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_363

    const-string v30, "extra_coalescing_codes"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_363

    const-string v30, "extra_num_unread_notifi"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_363

    const-string v30, "extra_prev_num_unread_noti"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_36e

    .line 415
    :cond_363
    new-instance v30, Lcom/google/api/services/plusi/model/ActionTarget;

    invoke-direct/range {v30 .. v30}, Lcom/google/api/services/plusi/model/ActionTarget;-><init>()V

    move-object/from16 v0, v30

    move-object/from16 v1, v22

    iput-object v0, v1, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    .line 418
    :cond_36e
    const-string v30, "extra_activity_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_3b4

    .line 419
    const-string v30, "extra_activity_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 420
    .local v3, activityId:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_3b4

    .line 421
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iput-object v3, v0, Lcom/google/api/services/plusi/model/ActionTarget;->activityId:Ljava/lang/String;

    .line 422
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_3b4

    .line 423
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: activityId: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    .end local v3           #activityId:Ljava/lang/String;
    :cond_3b4
    const-string v30, "extra_comment_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_3fa

    .line 429
    const-string v30, "extra_comment_id"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 430
    .local v7, commentId:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_3fa

    .line 431
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iput-object v7, v0, Lcom/google/api/services/plusi/model/ActionTarget;->commentId:Ljava/lang/String;

    .line 432
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_3fa

    .line 433
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: commentId: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    .end local v7           #commentId:Ljava/lang/String;
    :cond_3fa
    const-string v30, "extra_notification_read"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_456

    .line 439
    const-string v30, "extra_notification_read"

    const/16 v31, 0x0

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    .line 440
    .local v15, isRead:Z
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    move-object/from16 v31, v0

    if-nez v15, :cond_5e7

    const/16 v30, 0x1

    :goto_41e
    invoke-static/range {v30 .. v30}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActionTarget;->isUnreadNotification:Ljava/lang/Boolean;

    .line 441
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_456

    .line 442
    const-string v31, "EsAnalyticsData"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v30, "createClientOzEvent: isUnreadNotification: "

    move-object/from16 v0, v32

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v15, :cond_5eb

    const/16 v30, 0x1

    :goto_443
    move-object/from16 v0, v32

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    .end local v15           #isRead:Z
    :cond_456
    const-string v30, "extra_num_unread_notifi"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_49e

    .line 447
    const-string v30, "extra_num_unread_notifi"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v21

    .line 448
    .local v21, nunUnread:I
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    move-object/from16 v30, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActionTarget;->numUnreadNotifications:Ljava/lang/Integer;

    .line 449
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_49e

    .line 450
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: numUnreadNotifications: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    .end local v21           #nunUnread:I
    :cond_49e
    const-string v30, "extra_prev_num_unread_noti"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_4e6

    .line 455
    const-string v30, "extra_prev_num_unread_noti"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v23

    .line 456
    .local v23, prevNunUnread:I
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    move-object/from16 v30, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActionTarget;->previousNumUnreadNotifications:Ljava/lang/Integer;

    .line 457
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_4e6

    .line 458
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: previousNumUnreadNotifications: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v31

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    .end local v23           #prevNunUnread:I
    :cond_4e6
    const-string v30, "extra_notification_types"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_623

    const-string v30, "extra_coalescing_codes"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_623

    .line 465
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 467
    .local v16, logMessage:Ljava/lang/StringBuilder;
    const-string v30, "extra_notification_types"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v29

    .line 469
    .local v29, types:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-string v30, "extra_coalescing_codes"

    move-object/from16 v0, p7

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 472
    .local v6, coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v29, :cond_623

    if-eqz v6, :cond_623

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v30

    if-nez v30, :cond_623

    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v30

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_623

    .line 474
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 476
    .local v20, notificationTypes:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/NotificationTypes;>;"
    const/4 v14, 0x0

    .local v14, i:I
    :goto_535
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v30

    move/from16 v0, v30

    if-ge v14, v0, :cond_5f5

    .line 477
    new-instance v19, Lcom/google/api/services/plusi/model/NotificationTypes;

    invoke-direct/range {v19 .. v19}, Lcom/google/api/services/plusi/model/NotificationTypes;-><init>()V

    .line 480
    .local v19, notificationType:Lcom/google/api/services/plusi/model/NotificationTypes;
    new-instance v28, Ljava/util/ArrayList;

    const/16 v30, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 481
    .local v28, typeList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v0, v29

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/Integer;

    .line 482
    .local v27, type:Ljava/lang/Integer;
    if-nez v27, :cond_5ef

    const/16 v30, 0x0

    :goto_559
    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 483
    move-object/from16 v0, v28

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/api/services/plusi/model/NotificationTypes;->type:Ljava/util/List;

    .line 486
    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 487
    .local v5, coalescingCode:Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_57a

    .line 488
    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/api/services/plusi/model/NotificationTypes;->coalescingCode:Ljava/lang/String;

    .line 491
    :cond_57a
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "("

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v31, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ":"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ") "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v16

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 492
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 476
    add-int/lit8 v14, v14, 0x1

    goto :goto_535

    .line 283
    .end local v5           #coalescingCode:Ljava/lang/String;
    .end local v6           #coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14           #i:I
    .end local v16           #logMessage:Ljava/lang/StringBuilder;
    .end local v19           #notificationType:Lcom/google/api/services/plusi/model/NotificationTypes;
    .end local v20           #notificationTypes:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/NotificationTypes;>;"
    .end local v27           #type:Ljava/lang/Integer;
    .end local v28           #typeList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v29           #types:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_5b6
    const/16 v30, 0x0

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v30

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->totalTimeMs:Ljava/lang/Integer;

    goto/16 :goto_32

    .line 352
    :cond_5c2
    new-instance v30, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    invoke-direct/range {v30 .. v30}, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;-><init>()V

    move-object/from16 v0, v30

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 355
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    .line 356
    iget-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->startView:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    goto/16 :goto_1c0

    .line 440
    .restart local v15       #isRead:Z
    :cond_5e7
    const/16 v30, 0x0

    goto/16 :goto_41e

    .line 442
    :cond_5eb
    const/16 v30, 0x0

    goto/16 :goto_443

    .line 482
    .end local v15           #isRead:Z
    .restart local v6       #coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14       #i:I
    .restart local v16       #logMessage:Ljava/lang/StringBuilder;
    .restart local v19       #notificationType:Lcom/google/api/services/plusi/model/NotificationTypes;
    .restart local v20       #notificationTypes:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/NotificationTypes;>;"
    .restart local v27       #type:Ljava/lang/Integer;
    .restart local v28       #typeList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v29       #types:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_5ef
    invoke-virtual/range {v27 .. v27}, Ljava/lang/Integer;->intValue()I

    move-result v30

    goto/16 :goto_559

    .line 494
    .end local v19           #notificationType:Lcom/google/api/services/plusi/model/NotificationTypes;
    .end local v27           #type:Ljava/lang/Integer;
    .end local v28           #typeList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_5f5
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_617

    .line 495
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "createClientOzEvent: notificationTypes: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    :cond_617
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OzEvent;->actionTarget:Lcom/google/api/services/plusi/model/ActionTarget;

    move-object/from16 v30, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/api/services/plusi/model/ActionTarget;->notificationTypes:Ljava/util/List;

    .line 505
    .end local v6           #coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14           #i:I
    .end local v16           #logMessage:Ljava/lang/StringBuilder;
    .end local v20           #notificationTypes:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/NotificationTypes;>;"
    .end local v29           #types:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_623
    sget-boolean v30, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v30, :cond_6bc

    .line 506
    new-instance v17, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;

    invoke-direct/range {v17 .. v17}, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;-><init>()V

    .line 507
    .local v17, memoryStats:Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;
    sget-object v30, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v30

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->jsHeapSizeLimit:Ljava/lang/Long;

    .line 508
    sget-object v30, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v30

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->totalJsHeapSize:Ljava/lang/Long;

    .line 509
    sget-object v30, Lcom/google/android/apps/plus/content/EsAnalyticsData;->runtime:Ljava/lang/Runtime;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v11

    .line 510
    .local v11, freeMemory:J
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->totalJsHeapSize:Ljava/lang/Long;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    sub-long v30, v30, v11

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->usedJsHeapSize:Ljava/lang/Long;

    .line 511
    const-string v30, "EsAnalyticsData"

    const/16 v31, 0x3

    invoke-static/range {v30 .. v31}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v30

    if-eqz v30, :cond_6b8

    .line 512
    const-string v30, "EsAnalyticsData"

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "MemoryStats Max: "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->jsHeapSizeLimit:Ljava/lang/Long;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " Total: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->totalJsHeapSize:Ljava/lang/Long;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " Used: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;->usedJsHeapSize:Ljava/lang/Long;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " Free: "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v0, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :cond_6b8
    move-object/from16 v0, v17

    iput-object v0, v10, Lcom/google/api/services/plusi/model/FavaDiagnostics;->memoryStats:Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;

    .line 530
    .end local v11           #freeMemory:J
    .end local v17           #memoryStats:Lcom/google/api/services/plusi/model/FavaDiagnosticsMemoryStats;
    :cond_6bc
    move-object/from16 v0, v22

    iput-object v10, v0, Lcom/google/api/services/plusi/model/OzEvent;->favaDiagnostics:Lcom/google/api/services/plusi/model/FavaDiagnostics;

    .line 531
    move-object/from16 v0, v22

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ClientOzEvent;->ozEvent:Lcom/google/api/services/plusi/model/OzEvent;

    .line 532
    return-object v4
.end method

.method public static createClientOzExtension(Landroid/content/Context;)Lcom/google/api/services/plusi/model/ClientOzExtension;
    .registers 14
    .parameter "context"

    .prologue
    const-wide/16 v11, 0x0

    const/4 v7, 0x0

    .line 196
    new-instance v2, Lcom/google/api/services/plusi/model/ClientOzExtension;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/ClientOzExtension;-><init>()V

    .line 197
    .local v2, extension:Lcom/google/api/services/plusi/model/ClientOzExtension;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->sendTimeMsec:Ljava/lang/Long;

    .line 200
    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    if-eqz v6, :cond_69

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    :goto_1c
    if-eqz v6, :cond_d7

    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_TABLET:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    .line 202
    .local v0, clientId:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
    :goto_20
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->value()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientId:Ljava/lang/String;

    .line 203
    const-string v6, "EsAnalyticsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_55

    .line 204
    const-string v6, "EsAnalyticsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Set the client id to "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->value()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_55
    :try_start_55
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 209
    .local v4, packageManager:Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 210
    .local v5, packageName:Ljava/lang/String;
    iput-object v5, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->callingApplication:Ljava/lang/String;

    .line 212
    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 213
    .local v3, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v6, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v6, v2, Lcom/google/api/services/plusi/model/ClientOzExtension;->clientVersion:Ljava/lang/String;
    :try_end_68
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_55 .. :try_end_68} :catch_db

    .line 219
    .end local v3           #packageInfo:Landroid/content/pm/PackageInfo;
    .end local v4           #packageManager:Landroid/content/pm/PackageManager;
    .end local v5           #packageName:Ljava/lang/String;
    :goto_68
    return-object v2

    .line 200
    .end local v0           #clientId:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
    :cond_69
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sput-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    if-nez v6, :cond_85

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_1c

    :cond_85
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    if-nez v6, :cond_92

    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_1c

    :cond_92
    invoke-virtual {v6, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v6, v8, Landroid/util/DisplayMetrics;->xdpi:F

    float-to-double v9, v6

    cmpl-double v6, v9, v11

    if-eqz v6, :cond_a3

    iget v6, v8, Landroid/util/DisplayMetrics;->ydpi:F

    float-to-double v9, v6

    cmpl-double v6, v9, v11

    if-nez v6, :cond_ab

    :cond_a3
    sget-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto/16 :goto_1c

    :cond_ab
    iget v6, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v6, v6

    iget v9, v8, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v6, v9

    float-to-double v9, v6

    iget v6, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v6, v6

    iget v8, v8, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v6, v8

    float-to-double v11, v6

    mul-double v8, v9, v9

    mul-double v10, v11, v11

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    const-wide/high16 v10, 0x4014

    cmpl-double v6, v8, v10

    if-ltz v6, :cond_d5

    const/4 v6, 0x1

    :goto_c9
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    sput-object v6, Lcom/google/android/apps/plus/content/EsAnalyticsData;->isTabletDevice:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto/16 :goto_1c

    :cond_d5
    move v6, v7

    goto :goto_c9

    :cond_d7
    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;->ANDROID_OS:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;

    goto/16 :goto_20

    .line 215
    .restart local v0       #clientId:Lcom/google/android/apps/plus/content/EsAnalyticsData$ClientId;
    :catch_db
    move-exception v1

    .line 216
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_68
.end method

.method public static createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 4
    .parameter "key"
    .parameter "value"

    .prologue
    .line 595
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 596
    :cond_c
    const/4 v0, 0x0

    .line 600
    :goto_d
    return-object v0

    .line 598
    :cond_e
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 599
    .local v0, extras:Landroid/os/Bundle;
    invoke-virtual {v0, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_d
.end method

.method private static getFilterType(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;
    .registers 2
    .parameter "viewData"

    .prologue
    .line 585
    if-eqz p0, :cond_6

    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->filterType:Ljava/lang/Integer;

    if-nez v0, :cond_8

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->filterType:Ljava/lang/Integer;

    goto :goto_7
.end method

.method private static getGaiaId(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 2
    .parameter "extras"

    .prologue
    .line 536
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    const-string v0, "extra_gaia_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private static getTab(Lcom/google/api/services/plusi/model/OutputData;)Ljava/lang/Integer;
    .registers 2
    .parameter "viewData"

    .prologue
    .line 590
    if-eqz p0, :cond_6

    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->tab:Ljava/lang/Integer;

    if-nez v0, :cond_8

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return-object v0

    :cond_8
    iget-object v0, p0, Lcom/google/api/services/plusi/model/OutputData;->tab:Ljava/lang/Integer;

    goto :goto_7
.end method

.method public static insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)V
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "eventBytes"

    .prologue
    .line 95
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_19

    move-result-object v0

    .line 100
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 101
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "event_data"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 102
    const-string v2, "analytics_events"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 103
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v1           #values:Landroid/content/ContentValues;
    :goto_18
    return-void

    .line 97
    :catch_19
    move-exception v2

    goto :goto_18
.end method

.method public static queryLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 573
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 576
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT last_analytics_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_e} :catch_10

    move-result-wide v1

    .line 580
    :goto_f
    return-wide v1

    :catch_10
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_f
.end method

.method public static removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/List;
    .registers 16
    .parameter "context"
    .parameter "account"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 152
    .local v12, events:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    :try_start_5
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_c} :catch_67

    move-result-object v0

    .line 157
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 159
    const/4 v1, 0x1

    :try_start_11
    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "event_data"

    aput-object v3, v2, v1

    .line 162
    .local v2, columns:[Ljava/lang/String;
    const-string v1, "analytics_events"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_22
    .catchall {:try_start_11 .. :try_end_22} :catchall_62

    move-result-object v9

    .line 164
    .local v9, cursor:Landroid/database/Cursor;
    if-nez v9, :cond_29

    .line 184
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v2           #columns:[Ljava/lang/String;
    .end local v9           #cursor:Landroid/database/Cursor;
    :goto_28
    return-object v12

    .line 168
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2       #columns:[Ljava/lang/String;
    .restart local v9       #cursor:Landroid/database/Cursor;
    :cond_29
    :try_start_29
    const-string v1, "event_data"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 169
    .local v8, columnIndex:I
    const/4 v13, 0x0

    .local v13, position:I
    :goto_30
    invoke-interface {v9, v13}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 170
    invoke-interface {v9, v8}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 171
    .local v11, eventData:[B
    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v1

    invoke-virtual {v1, v11}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/ClientOzEvent;

    .line 172
    .local v10, event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    if-eqz v10, :cond_49

    .line 173
    invoke-interface {v12, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_49
    .catchall {:try_start_29 .. :try_end_49} :catchall_5d

    .line 169
    :cond_49
    add-int/lit8 v13, v13, 0x1

    goto :goto_30

    .line 177
    .end local v10           #event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    .end local v11           #eventData:[B
    :cond_4c
    :try_start_4c
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 180
    const-string v1, "analytics_events"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 181
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_59
    .catchall {:try_start_4c .. :try_end_59} :catchall_62

    .line 184
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_28

    .line 177
    .end local v8           #columnIndex:I
    .end local v13           #position:I
    :catchall_5d
    move-exception v1

    :try_start_5e
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_62
    .catchall {:try_start_5e .. :try_end_62} :catchall_62

    .line 184
    .end local v2           #columns:[Ljava/lang/String;
    .end local v9           #cursor:Landroid/database/Cursor;
    :catchall_62
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1

    .line 154
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    :catch_67
    move-exception v1

    goto :goto_28
.end method

.method public static saveLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "timestamp"

    .prologue
    const/4 v4, 0x0

    .line 557
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 560
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 561
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "last_analytics_sync_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 562
    const-string v2, "account_status"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 563
    return-void
.end method

.method private static setUserInfo(Lcom/google/api/services/plusi/model/ClientOutputData;Ljava/lang/String;)V
    .registers 5
    .parameter "viewData"
    .parameter "gaiaId"

    .prologue
    .line 540
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1c

    if-eqz p0, :cond_1c

    .line 541
    new-instance v0, Lcom/google/api/services/plusi/model/ClientUserInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientUserInfo;-><init>()V

    .line 542
    .local v0, userInfo:Lcom/google/api/services/plusi/model/ClientUserInfo;
    iput-object p1, v0, Lcom/google/api/services/plusi/model/ClientUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    .line 543
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/api/services/plusi/model/ClientOutputData;->userInfo:Ljava/util/List;

    .line 544
    iget-object v1, p0, Lcom/google/api/services/plusi/model/ClientOutputData;->userInfo:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546
    .end local v0           #userInfo:Lcom/google/api/services/plusi/model/ClientUserInfo;
    :cond_1c
    return-void
.end method
