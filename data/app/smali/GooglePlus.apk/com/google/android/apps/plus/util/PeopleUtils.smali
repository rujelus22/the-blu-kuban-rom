.class public final Lcom/google/android/apps/plus/util/PeopleUtils;
.super Ljava/lang/Object;
.source "PeopleUtils.java"


# direct methods
.method public static in(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z
    .registers 9
    .parameter "set"
    .parameter "target"

    .prologue
    const/4 v5, 0x0

    .line 56
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v3, v0

    .local v3, len$:I
    const/4 v2, 0x0

    .local v2, i$:I
    :goto_7
    if-ge v2, v3, :cond_19

    aget-object v4, v0, v2

    .line 57
    .local v4, user:Lcom/google/android/apps/plus/content/PersonData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v6

    if-nez v6, :cond_16

    .line 66
    .end local v0           #arr$:[Lcom/google/android/apps/plus/content/PersonData;
    .end local v4           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_15
    :goto_15
    return v5

    .line 56
    .restart local v0       #arr$:[Lcom/google/android/apps/plus/content/PersonData;
    .restart local v4       #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_16
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 61
    .end local v4           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v3, v0

    const/4 v2, 0x0

    :goto_1f
    if-ge v2, v3, :cond_30

    aget-object v1, v0, v2

    .line 62
    .local v1, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_1f

    .line 66
    .end local v1           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_30
    const/4 v5, 0x1

    goto :goto_15
.end method

.method public static in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z
    .registers 8
    .parameter "others"
    .parameter "target"

    .prologue
    .line 29
    move-object v0, p0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v2, p0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_1a

    aget-object v3, v0, v1

    .line 30
    .local v3, other:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    .line 31
    const/4 v4, 0x1

    .line 34
    .end local v3           #other:Lcom/google/android/apps/plus/content/CircleData;
    :goto_16
    return v4

    .line 29
    .restart local v3       #other:Lcom/google/android/apps/plus/content/CircleData;
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 34
    .end local v3           #other:Lcom/google/android/apps/plus/content/CircleData;
    :cond_1a
    const/4 v4, 0x0

    goto :goto_16
.end method

.method public static in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z
    .registers 7
    .parameter "others"
    .parameter "target"

    .prologue
    .line 43
    move-object v0, p0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v2, p0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_12

    aget-object v3, v0, v1

    .line 44
    .local v3, other:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->isSamePerson(Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 45
    const/4 v4, 0x1

    .line 48
    .end local v3           #other:Lcom/google/android/apps/plus/content/PersonData;
    :goto_e
    return v4

    .line 43
    .restart local v3       #other:Lcom/google/android/apps/plus/content/PersonData;
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 48
    .end local v3           #other:Lcom/google/android/apps/plus/content/PersonData;
    :cond_12
    const/4 v4, 0x0

    goto :goto_e
.end method

.method public static isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z
    .registers 2
    .parameter "audience"

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method
