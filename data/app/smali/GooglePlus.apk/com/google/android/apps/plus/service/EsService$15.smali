.class final Lcom/google/android/apps/plus/service/EsService$15;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/EsService;->processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$eventId:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 4090
    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$15;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$eventId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 4093
    const/16 v2, 0xa

    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 4096
    :try_start_6
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$eventId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->getEventFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z

    move-result v1

    .line 4098
    .local v1, success:Z
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$15;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$intent:Landroid/content/Intent;

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4, v1}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Z)V

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_1d} :catch_1e

    .line 4103
    .end local v1           #success:Z
    :goto_1d
    return-void

    .line 4099
    :catch_1e
    move-exception v0

    .line 4100
    .local v0, ex:Ljava/lang/Exception;
    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$15;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$15;->val$intent:Landroid/content/Intent;

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->access$1000(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_1d
.end method
