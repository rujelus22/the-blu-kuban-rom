.class public Lcom/google/android/apps/plus/phone/AddedToCircleActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "AddedToCircleActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v1, 0x7f030006

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->setContentView(I)V

    .line 28
    if-nez p1, :cond_1e

    .line 29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notif_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    .local v0, notificationId:Ljava/lang/String;
    if-eqz v0, :cond_1e

    .line 35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    .line 41
    .end local v0           #notificationId:Ljava/lang/String;
    :cond_1e
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->showTitlebar(ZZ)V

    .line 48
    const v1, 0x7f0801a3

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    .line 78
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_12

    .line 86
    const/4 v1, 0x0

    :goto_8
    return v1

    .line 80
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 81
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 82
    const/4 v1, 0x1

    goto :goto_8

    .line 78
    :pswitch_data_12
    .packed-switch 0x102002c
        :pswitch_9
    .end packed-switch
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->finish()V

    .line 62
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 70
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/AddedToCircleActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 71
    return-void
.end method
