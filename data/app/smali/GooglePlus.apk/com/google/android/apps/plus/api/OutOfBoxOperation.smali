.class public final Lcom/google/android/apps/plus/api/OutOfBoxOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "OutOfBoxOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;",
        "Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

.field private mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "request"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v6, 0x0

    .line 40
    const-string v3, "mobileoutofboxflow"

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 43
    iput-object p3, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    .line 44
    return-void
.end method


# virtual methods
.method public final getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mResponse:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    .end local p1
    const-string v0, "NATIVE_ANDROID"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->clientType:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    :goto_10
    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->mRequest:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    return-void

    :cond_1f
    const-string v0, "DEFAULT"

    goto :goto_10
.end method
