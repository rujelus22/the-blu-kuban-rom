.class public final Lcom/google/android/apps/plus/views/EventCardDrawer;
.super Ljava/lang/Object;
.source "EventCardDrawer.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;


# static fields
.field private static sAuthorBitmap:Landroid/graphics/Bitmap;

.field private static sAvatarSize:I

.field private static sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sEventAttendingBitmap:Landroid/graphics/Bitmap;

.field private static sEventCardPadding:I

.field private static sEventInfoBackgroundPaint:Landroid/graphics/Paint;

.field private static sEventInfoTextPaint:Landroid/text/TextPaint;

.field private static sEventMaybeBitmap:Landroid/graphics/Bitmap;

.field private static sEventNameTextPaint:Landroid/text/TextPaint;

.field private static sEventNotAttendingBitmap:Landroid/graphics/Bitmap;

.field private static sEventTextLineSpacing:I

.field private static sGreenRsvpBannerBitmap:Landroid/graphics/Bitmap;

.field private static sGreyRsvpBannerBitmap:Landroid/graphics/Bitmap;

.field private static sHangoutBitmap:Landroid/graphics/Bitmap;

.field private static sHangoutTitle:Ljava/lang/String;

.field private static sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private static sInitialized:Z

.field private static sLocationBitmap:Landroid/graphics/Bitmap;

.field private static sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sOnAirPaint:Landroid/text/TextPaint;

.field private static sOnAirTitle:Ljava/lang/String;

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sRibbonHeightPercentOverlap:F

.field private static sStatusGoingPaint:Landroid/text/TextPaint;

.field private static sStatusInvitedPaint:Landroid/text/TextPaint;

.field private static sStatusMaybePaint:Landroid/text/TextPaint;

.field private static sStatusNotGoingPaint:Landroid/text/TextPaint;


# instance fields
.field mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAttribution:Ljava/lang/CharSequence;

.field private mAttributionLayoutCorner:Landroid/graphics/Point;

.field mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

.field private mBound:Z

.field mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

.field private mCreatorLayout:Landroid/text/StaticLayout;

.field private mDateLayout:Landroid/text/StaticLayout;

.field private mDateLayoutCorner:Landroid/graphics/Point;

.field mDividerLines:[F

.field mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mIgnoreHeight:Z

.field private mLocationIcon:Landroid/graphics/Bitmap;

.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayout:Landroid/text/StaticLayout;

.field private mLocationLayoutCorner:Landroid/graphics/Point;

.field private mNameLayout:Landroid/text/StaticLayout;

.field private mNameLayoutCorner:Landroid/graphics/Point;

.field private mRsvpBanner:Landroid/graphics/Bitmap;

.field private mRsvpBannerRect:Landroid/graphics/Rect;

.field private mRsvpIcon:Landroid/graphics/Bitmap;

.field private mRsvpIconRect:Landroid/graphics/Rect;

.field private mRsvpLayout:Landroid/text/StaticLayout;

.field private mRsvpLayoutCorner:Landroid/graphics/Point;

.field mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

.field private mThemeImageRect:Landroid/graphics/Rect;

.field mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

.field private mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 11
    .parameter "view"

    .prologue
    const v8, 0x7f0d00a5

    const v7, 0x7f0d0098

    const v6, 0x7f0d0097

    const v5, 0x7f0d0096

    const/4 v4, 0x1

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    sget-boolean v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sInitialized:Z

    if-nez v2, :cond_1d3

    .line 135
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 136
    .local v0, resources:Landroid/content/res/Resources;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 137
    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    .line 138
    const v2, 0x7f0d0138

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAvatarSize:I

    .line 139
    const v2, 0x7f0d0095

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sRibbonHeightPercentOverlap:F

    .line 141
    const v2, 0x7f0d0092

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventCardPadding:I

    .line 142
    const v2, 0x7f0d0093

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventTextLineSpacing:I

    .line 144
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAuthorBitmap:Landroid/graphics/Bitmap;

    .line 147
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 148
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    const v3, 0x7f0a004e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 150
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 152
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 153
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 154
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a004d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 155
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 157
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 158
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 161
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    .line 162
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 163
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0105

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 164
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 166
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-static {v2, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 169
    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    .line 170
    .local v1, statusPaint:Landroid/text/TextPaint;
    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 171
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 172
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 174
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 175
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusInvitedPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0103

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 177
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusInvitedPaint:Landroid/text/TextPaint;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 180
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 181
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusGoingPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0101

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 183
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusGoingPaint:Landroid/text/TextPaint;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 186
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 187
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusNotGoingPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0102

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 189
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusNotGoingPaint:Landroid/text/TextPaint;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 192
    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2, v1}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 193
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusMaybePaint:Landroid/text/TextPaint;

    const v3, 0x7f0a0104

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 195
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusMaybePaint:Landroid/text/TextPaint;

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 198
    new-instance v2, Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 199
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    const v3, 0x7f0a004f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 200
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 202
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    invoke-static {v2, v8}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    .line 204
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 205
    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sDividerPaint:Landroid/graphics/Paint;

    const v3, 0x7f0a0100

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 207
    sget-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sDividerPaint:Landroid/graphics/Paint;

    const v3, 0x7f0d0094

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 211
    const v2, 0x7f020158

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventAttendingBitmap:Landroid/graphics/Bitmap;

    .line 213
    const v2, 0x7f020161

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNotAttendingBitmap:Landroid/graphics/Bitmap;

    .line 215
    const v2, 0x7f020160

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventMaybeBitmap:Landroid/graphics/Bitmap;

    .line 218
    const v2, 0x7f020163

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

    .line 220
    const v2, 0x7f020164

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreenRsvpBannerBitmap:Landroid/graphics/Bitmap;

    .line 222
    const v2, 0x7f020165

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreyRsvpBannerBitmap:Landroid/graphics/Bitmap;

    .line 225
    const v2, 0x7f020169

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sLocationBitmap:Landroid/graphics/Bitmap;

    .line 226
    const v2, 0x7f02015e

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutBitmap:Landroid/graphics/Bitmap;

    .line 229
    const v2, 0x7f080357

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutTitle:Ljava/lang/String;

    .line 230
    const v2, 0x7f0800fe

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirTitle:Ljava/lang/String;

    .line 232
    const v2, 0x7f020034

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v2, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    .line 235
    sput-boolean v4, Lcom/google/android/apps/plus/views/EventCardDrawer;->sInitialized:Z

    .line 238
    .end local v0           #resources:Landroid/content/res/Resources;
    .end local v1           #statusPaint:Landroid/text/TextPaint;
    :cond_1d3
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    .line 240
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    .line 241
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    .line 242
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    .line 243
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    .line 245
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    .line 246
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    .line 247
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    .line 248
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    .line 249
    const/4 v2, 0x4

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    .line 250
    return-void
.end method

.method private static drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "layout"
    .parameter "corner"
    .parameter "canvas"

    .prologue
    .line 337
    iget v0, p1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 338
    invoke-virtual {p0, p2}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 339
    iget v0, p1, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 340
    return-void
.end method

.method private static layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .registers 18
    .parameter "left"
    .parameter "top"
    .parameter "width"
    .parameter "text"
    .parameter "textCorner"
    .parameter "paint"
    .parameter "ellipsize"

    .prologue
    .line 331
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v0, p0

    move v1, p1

    move v2, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v9, p5

    move/from16 v10, p6

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final attach()V
    .registers 2

    .prologue
    .line 654
    sget-object v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 655
    sget-object v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->registerAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 656
    return-void
.end method

.method public final bind(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/views/CardView;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V
    .registers 12
    .parameter "account"
    .parameter "view"
    .parameter "event"
    .parameter "clickListener"

    .prologue
    const/4 v4, 0x0

    .line 262
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/views/EventCardDrawer;->bind(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/views/CardView;Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V

    .line 263
    return-void
.end method

.method public final bind(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/views/CardView;Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;Ljava/lang/CharSequence;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;)V
    .registers 14
    .parameter "account"
    .parameter "view"
    .parameter "event"
    .parameter "authorGaiaId"
    .parameter "attribution"
    .parameter "clickListener"

    .prologue
    const/4 v3, 0x0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->clear()V

    .line 269
    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_3f

    const/4 v0, 0x1

    :goto_b
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    .line 272
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-eqz v0, :cond_3e

    .line 273
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 274
    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->theme:Lcom/google/api/services/plusi/model/Theme;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeImage(Lcom/google/api/services/plusi/model/Theme;)Lcom/google/api/services/plusi/model/ThemeImage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 279
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableUserImage;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    if-eqz p4, :cond_41

    move-object v2, p4

    :goto_2d
    const/4 v6, 0x2

    move-object v4, v3

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableUserImage;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableUserImage$UserImageClickListener;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 282
    iput-object p5, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CardView;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 285
    :cond_3e
    return-void

    .line 270
    :cond_3f
    const/4 v0, 0x0

    goto :goto_b

    .line 279
    :cond_41
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    goto :goto_2d
.end method

.method public final clear()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 291
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-nez v0, :cond_7

    .line 327
    :goto_6
    return-void

    .line 295
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CardView;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    .line 296
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    .line 297
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    .line 298
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    .line 299
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    .line 300
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 301
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    .line 302
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    .line 303
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 312
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    .line 313
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    .line 314
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    .line 315
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    .line 316
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    .line 317
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    .line 319
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v3, v3}, Landroid/graphics/Point;->set(II)V

    .line 325
    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    .line 326
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    goto :goto_6
.end method

.method public final detach()V
    .registers 2

    .prologue
    .line 659
    sget-object v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterAvatarChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnAvatarChangeListener;)V

    .line 660
    sget-object v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->sImageCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->unregisterMediaImageChangeListener(Lcom/google/android/apps/plus/service/ImageCache$OnMediaImageChangeListener;)V

    .line 661
    return-void
.end method

.method public final draw$680d9d43(IILandroid/graphics/Canvas;)I
    .registers 16
    .parameter "top"
    .parameter "height"
    .parameter "canvas"

    .prologue
    .line 556
    move v5, p1

    .line 557
    .local v5, returnY:I
    add-int v3, p1, p2

    .line 559
    .local v3, bottom:I
    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-nez v8, :cond_8

    .line 650
    .end local p1
    :goto_7
    return p1

    .line 564
    .restart local p1
    :cond_8
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    if-eqz v8, :cond_2b

    .line 565
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/RemoteImage;->refreshIfInvalidated()V

    .line 566
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/RemoteImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 568
    .local v2, bitmap:Landroid/graphics/Bitmap;
    if-eqz v2, :cond_2b

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    if-le v8, v3, :cond_23

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_2b

    .line 569
    :cond_23
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    sget-object v10, Lcom/google/android/apps/plus/views/EventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p3, v2, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 574
    .end local v2           #bitmap:Landroid/graphics/Bitmap;
    :cond_2b
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v8, :cond_60

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    if-le v8, v3, :cond_3d

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_60

    .line 575
    :cond_3d
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 577
    .local v1, avatarBitmap:Landroid/graphics/Bitmap;
    if-nez v1, :cond_47

    .line 578
    sget-object v1, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAuthorBitmap:Landroid/graphics/Bitmap;

    .line 581
    :cond_47
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/ClickableUserImage;->getRect()Landroid/graphics/Rect;

    move-result-object v9

    sget-object v10, Lcom/google/android/apps/plus/views/EventCardDrawer;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p3, v1, v8, v9, v10}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 583
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableUserImage;->isClicked()Z

    move-result v8

    if-eqz v8, :cond_60

    .line 584
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v8, p3}, Lcom/google/android/apps/plus/views/ClickableUserImage;->drawSelectionRect(Landroid/graphics/Canvas;)V

    .line 588
    .end local v1           #avatarBitmap:Landroid/graphics/Bitmap;
    :cond_60
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_a9

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    invoke-virtual {v11}, Landroid/text/StaticLayout;->getHeight()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    if-le v8, v3, :cond_85

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_a9

    .line 592
    :cond_85
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    const/4 v11, 0x0

    invoke-virtual {p3, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 595
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    invoke-static {v8, v9, p3}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 597
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    if-eqz v8, :cond_a2

    .line 598
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    const/4 v11, 0x0

    invoke-virtual {p3, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 602
    :cond_a2
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    sget-object v9, Lcom/google/android/apps/plus/views/EventCardDrawer;->sDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {p3, v8, v9}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    .line 606
    :cond_a9
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    if-le v8, v3, :cond_ba

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_c1

    .line 607
    :cond_ba
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    invoke-static {v8, v9, p3}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 611
    :cond_c1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    add-int v4, v8, v9

    .line 613
    .local v4, dateLabelBottom:I
    if-le v4, v3, :cond_d3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_db

    .line 614
    :cond_d3
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    invoke-static {v8, v9, p3}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 615
    move v5, v4

    .line 619
    :cond_db
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v8, :cond_f3

    .line 620
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v8

    iget v7, v8, Landroid/graphics/Rect;->bottom:I

    .line 622
    .local v7, typeLabelBottom:I
    if-le v7, v3, :cond_ed

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_f3

    .line 623
    :cond_ed
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v8, p3}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    .line 624
    move v5, v7

    .line 629
    .end local v7           #typeLabelBottom:I
    :cond_f3
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_121

    .line 630
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    add-int/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 633
    .local v6, sectionBottom:I
    if-le v6, v3, :cond_110

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_121

    .line 634
    :cond_110
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    invoke-static {v8, v9, p3}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 635
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    const/4 v11, 0x0

    invoke-virtual {p3, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 636
    move v5, v6

    .line 641
    .end local v6           #sectionBottom:I
    :cond_121
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    if-eqz v8, :cond_13f

    .line 642
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    invoke-virtual {v9}, Landroid/text/StaticLayout;->getHeight()I

    move-result v9

    add-int v0, v8, v9

    .line 644
    .local v0, attributeBottom:I
    if-le v0, v3, :cond_137

    iget-boolean v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    if-eqz v8, :cond_13f

    .line 645
    :cond_137
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    invoke-static {v8, v9, p3}, Lcom/google/android/apps/plus/views/EventCardDrawer;->drawTextLayout(Landroid/text/StaticLayout;Landroid/graphics/Point;Landroid/graphics/Canvas;)V

    .line 646
    move v5, v0

    .end local v0           #attributeBottom:I
    :cond_13f
    move p1, v5

    .line 650
    goto/16 :goto_7
.end method

.method public final layout(IIZII)I
    .registers 34
    .parameter "left"
    .parameter "top"
    .parameter "ignoreHeight"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 521
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mBound:Z

    if-nez v3, :cond_e

    .line 522
    :cond_c
    const/4 v3, 0x0

    .line 552
    :goto_d
    return v3

    .line 524
    :cond_e
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/EventCardDrawer;->mIgnoreHeight:Z

    .line 526
    sget v8, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventCardPadding:I

    .line 527
    .local v8, padding:I
    mul-int/lit8 v22, v8, 0x2

    .line 528
    .local v22, doublePadding:I
    sget v26, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventTextLineSpacing:I

    .line 530
    .local v26, lineSpacing:I
    move/from16 v0, p4

    int-to-float v3, v0

    const v4, 0x40570a3d

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v27

    .line 535
    .local v27, themeHeight:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    if-nez v3, :cond_67

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    if-eqz v3, :cond_67

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImageRect:Landroid/graphics/Rect;

    add-int v4, p1, p4

    add-int v5, p2, v27

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeInfo:Lcom/google/api/services/plusi/model/ThemeImage;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ThemeImage;->url:Ljava/lang/String;

    move/from16 v0, p4

    move/from16 v1, v27

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->getCenterCroppedAndResizedUrl(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/plus/content/EventThemeImageRequest;

    invoke-direct {v4, v3}, Lcom/google/android/apps/plus/content/EventThemeImageRequest;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/apps/plus/views/RemoteImage;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-direct {v3, v5, v4}, Lcom/google/android/apps/plus/views/RemoteImage;-><init>(Landroid/view/View;Lcom/google/android/apps/plus/content/ImageRequest;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/RemoteImage;->load()V

    .line 537
    :cond_67
    sget v20, Lcom/google/android/apps/plus/views/EventCardDrawer;->sAvatarSize:I

    .line 538
    .local v20, avatarSizeLength:I
    add-int v19, p1, v8

    .line 539
    .local v19, avatarLeft:I
    add-int v3, p2, v27

    sub-int v21, v3, v22

    .line 541
    .local v21, avatarTop:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    add-int v4, v19, v20

    add-int v5, v21, v20

    move/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/apps/plus/views/ClickableUserImage;->setRect(IIII)V

    .line 544
    add-int v3, v19, v20

    add-int v2, v3, v8

    .line 545
    .local v2, infoLeft:I
    add-int v24, p2, v27

    .line 546
    .local v24, infoTop:I
    add-int v3, p1, p4

    sub-int/2addr v3, v2

    sub-int v25, v3, v22

    .line 547
    .local v25, infoWidth:I
    const/4 v9, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/CardView;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsEventData;->canRsvp(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v4

    if-nez v4, :cond_1b5

    const/4 v3, 0x0

    :goto_9e
    add-int v3, v3, v24

    add-int v10, v3, v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v12, v3, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayoutCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNameTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x1

    move v9, v2

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    if-eqz v4, :cond_ea

    add-int v10, v3, v26

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttribution:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAttributionLayoutCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x0

    move v9, v2

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mCreatorLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v10

    :cond_ea
    add-int v10, v3, v26

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/CardView;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayoutCorner:Landroid/graphics/Point;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v15, 0x1

    move v9, v2

    move/from16 v11, v25

    invoke-static/range {v9 .. v15}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layoutTextLabel(IIILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDateLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v3, v10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v4, :cond_168

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    if-eqz v4, :cond_168

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_168

    add-int v18, v3, v26

    new-instance v9, Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mContainingCardView:Lcom/google/android/apps/plus/views/CardView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/CardView;->getContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirTitle:Ljava/lang/String;

    sget-object v13, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirPaint:Landroid/text/TextPaint;

    sget-object v14, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v15, Lcom/google/android/apps/plus/views/EventCardDrawer;->sOnAirNinePatch:Landroid/graphics/drawable/NinePatchDrawable;

    const/16 v16, 0x0

    move/from16 v17, v2

    invoke-direct/range {v9 .. v18}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mTypeLabel:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int v3, v3, v18

    :cond_168
    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v4, :cond_2c8

    iget-object v5, v4, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-eqz v5, :cond_2c4

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    :goto_17f
    sget-object v5, Lcom/google/android/apps/plus/views/EventCardDrawer;->sLocationBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    move-object v9, v4

    :cond_186
    :goto_186
    if-eqz v9, :cond_1ad

    add-int v3, v3, v26

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIconRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayoutCorner:Landroid/graphics/Point;

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventInfoTextPaint:Landroid/text/TextPaint;

    const/4 v12, 0x1

    move/from16 v4, v25

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    :cond_1ad
    sub-int v23, v3, v24

    .line 552
    .local v23, infoCalculatedHeight:I
    add-int v3, v24, v23

    sub-int v3, v3, p2

    goto/16 :goto_d

    .line 547
    .end local v23           #infoCalculatedHeight:I
    :cond_1b5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpStatus(Lcom/google/api/services/plusi/model/PlusEvent;)I

    move-result v4

    packed-switch v4, :pswitch_data_2d4

    :goto_1c0
    sget v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sRibbonHeightPercentOverlap:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v13

    sub-int v3, v24, v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    add-int v5, v2, v25

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v6, v2, v25

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int v3, v25, v3

    sub-int v4, v3, v8

    add-int v3, v24, v8

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    const/4 v12, 0x1

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayoutCorner:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v3, v5

    sub-int v5, v3, v24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2c1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIconRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int v3, v3, v24

    :goto_232
    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int v3, v3, v24

    add-int/2addr v3, v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v6, 0x0

    int-to-float v7, v2

    aput v7, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v6, 0x1

    int-to-float v7, v3

    aput v7, v5, v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v6, 0x2

    add-int/2addr v4, v2

    int-to-float v4, v4

    aput v4, v5, v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mDividerLines:[F

    const/4 v5, 0x3

    int-to-float v6, v3

    aput v6, v4, v5

    sub-int v3, v3, v24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBannerRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v13

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/16 :goto_9e

    :pswitch_26b
    const v4, 0x7f0800e6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusGoingPaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventAttendingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreenRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_1c0

    :pswitch_282
    const v4, 0x7f0800e8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusNotGoingPaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventNotAttendingBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sGreyRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_1c0

    :pswitch_299
    const v4, 0x7f0800e7

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusMaybePaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sEventMaybeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpIcon:Landroid/graphics/Bitmap;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_1c0

    :pswitch_2b0
    const v4, 0x7f0800e5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget-object v11, Lcom/google/android/apps/plus/views/EventCardDrawer;->sStatusInvitedPaint:Landroid/text/TextPaint;

    sget-object v3, Lcom/google/android/apps/plus/views/EventCardDrawer;->sBlueRsvpBannerBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mRsvpBanner:Landroid/graphics/Bitmap;

    goto/16 :goto_1c0

    :cond_2c1
    const/4 v3, 0x0

    goto/16 :goto_232

    :cond_2c4
    iget-object v4, v4, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto/16 :goto_17f

    :cond_2c8
    if-eqz v5, :cond_186

    sget-object v9, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutTitle:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/EventCardDrawer;->sHangoutBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mLocationIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_186

    :pswitch_data_2d4
    .packed-switch 0x0
        :pswitch_2b0
        :pswitch_26b
        :pswitch_299
        :pswitch_282
    .end packed-switch
.end method

.method public final onAvatarChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "gaiaId"

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    if-eqz v0, :cond_9

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mAvatar:Lcom/google/android/apps/plus/views/ClickableUserImage;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableUserImage;->onAvatarChanged(Ljava/lang/String;)V

    .line 677
    :cond_9
    return-void
.end method

.method public final onMediaImageChanged(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 665
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mEventInfo:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/RemoteImage;->getRequest()Lcom/google/android/apps/plus/content/ImageRequest;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/MediaImageRequest;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Lcom/google/android/apps/plus/content/MediaImageRequest;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 668
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardDrawer;->mThemeImage:Lcom/google/android/apps/plus/views/RemoteImage;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/RemoteImage;->invalidate()V

    .line 670
    :cond_1b
    return-void
.end method
