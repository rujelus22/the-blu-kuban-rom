.class public abstract Lcom/google/android/apps/plus/views/ScrollableViewGroup;
.super Landroid/view/ViewGroup;
.source "ScrollableViewGroup.java"


# static fields
.field private static final sInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mFlingVelocity:F

.field private mFlingable:Z

.field private mIsBeingDragged:Z

.field private mLastPosition:[F

.field private final mLimits:[I

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mReceivedDown:Z

.field private mScrollDirection:I

.field private mScrollEnabled:Z

.field protected mScroller:Landroid/widget/Scroller;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVertical:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/apps/plus/views/ScrollableViewGroup$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 33
    new-array v1, v4, [F

    fill-array-data v1, :array_4a

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    .line 34
    new-array v1, v4, [I

    fill-array-data v1, :array_52

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    .line 38
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    .line 39
    iput v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    .line 41
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    .line 42
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    .line 43
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    .line 44
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    .line 45
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 75
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->setFocusable(Z)V

    .line 77
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 78
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    .line 79
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    .line 80
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    .line 81
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    .line 63
    return-void

    .line 33
    :array_4a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 34
    :array_52
    .array-data 0x4
        0x1t 0x0t 0x0t 0x80t
        0xfft 0xfft 0xfft 0x7ft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 8
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-array v1, v4, [F

    fill-array-data v1, :array_4a

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    .line 34
    new-array v1, v4, [I

    fill-array-data v1, :array_52

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    .line 38
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    .line 39
    iput v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    .line 41
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    .line 42
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    .line 43
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    .line 44
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    .line 45
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 75
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->setFocusable(Z)V

    .line 77
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 78
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    .line 79
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    .line 80
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    .line 81
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    .line 67
    return-void

    .line 33
    :array_4a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 34
    :array_52
    .array-data 0x4
        0x1t 0x0t 0x0t 0x80t
        0xfft 0xfft 0xfft 0x7ft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter "context"
    .parameter "attrs"
    .parameter "defState"

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-array v1, v4, [F

    fill-array-data v1, :array_4a

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    .line 34
    new-array v1, v4, [I

    fill-array-data v1, :array_52

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    .line 38
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    .line 39
    iput v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    .line 41
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    .line 42
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    .line 43
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    .line 44
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    .line 45
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    .line 75
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->setFocusable(Z)V

    .line 77
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 78
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    .line 79
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    .line 80
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    .line 81
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    .line 71
    return-void

    .line 33
    :array_4a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 34
    :array_52
    .array-data 0x4
        0x1t 0x0t 0x0t 0x80t
        0xfft 0xfft 0xfft 0x7ft
    .end array-data
.end method

.method private clampToScrollLimits(I)I
    .registers 5
    .parameter "value"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 319
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget v0, v0, v1

    if-ge p1, v0, :cond_d

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget p1, v0, v1

    .line 325
    .end local p1
    :cond_c
    :goto_c
    return p1

    .line 322
    .restart local p1
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget v0, v0, v2

    if-le p1, v0, :cond_c

    .line 323
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget p1, v0, v2

    goto :goto_c
.end method

.method private shouldStartDrag(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter "event"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    if-nez v0, :cond_7

    .line 142
    :cond_6
    :goto_6
    return v1

    .line 113
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    if-eqz v0, :cond_e

    .line 114
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    goto :goto_6

    .line 118
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_7c

    :pswitch_15
    goto :goto_6

    .line 122
    :pswitch_16
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_26

    .line 124
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->startDrag()V

    move v1, v2

    .line 125
    goto :goto_6

    .line 127
    :cond_26
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    goto :goto_6

    .line 134
    :pswitch_29
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v3, v3, v1

    sub-float/2addr v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-gtz v4, :cond_4a

    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_6e

    :cond_4a
    move v0, v2

    :goto_4b
    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-gtz v4, :cond_5a

    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_70

    :cond_5a
    move v3, v2

    :goto_5b
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v4, :cond_74

    if-eqz v3, :cond_72

    if-nez v0, :cond_72

    move v0, v2

    :goto_64
    if-eqz v0, :cond_6

    .line 135
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    .line 136
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->startDrag()V

    move v1, v2

    .line 137
    goto :goto_6

    :cond_6e
    move v0, v1

    .line 134
    goto :goto_4b

    :cond_70
    move v3, v1

    goto :goto_5b

    :cond_72
    move v0, v1

    goto :goto_64

    :cond_74
    if-eqz v0, :cond_7a

    if-nez v3, :cond_7a

    move v0, v2

    goto :goto_64

    :cond_7a
    move v0, v1

    goto :goto_64

    .line 118
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_29
    .end packed-switch
.end method

.method private startDrag()V
    .registers 2

    .prologue
    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    .line 211
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    .line 212
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 214
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .registers 11
    .parameter "child"

    .prologue
    const v8, 0x7f090054

    .line 344
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 345
    .local v6, panel:Landroid/view/View;
    if-eqz v6, :cond_3c

    .line 346
    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 347
    .local v5, index:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getChildCount()I

    move-result v3

    .line 349
    .local v3, childrenCount:I
    const/4 v0, 0x0

    .line 350
    .local v0, added:Z
    const/4 v4, 0x0

    .local v4, i:I
    :goto_19
    if-ge v4, v3, :cond_33

    .line 351
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 352
    .local v2, childPanel:Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 353
    .local v1, childIndex:I
    if-le v1, v5, :cond_39

    .line 354
    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->addView(Landroid/view/View;I)V

    .line 355
    const/4 v0, 0x1

    .line 360
    .end local v1           #childIndex:I
    .end local v2           #childPanel:Landroid/view/View;
    :cond_33
    if-nez v0, :cond_38

    .line 361
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 366
    .end local v0           #added:Z
    .end local v3           #childrenCount:I
    .end local v4           #i:I
    .end local v5           #index:I
    :cond_38
    :goto_38
    return-void

    .line 350
    .restart local v0       #added:Z
    .restart local v1       #childIndex:I
    .restart local v2       #childPanel:Landroid/view/View;
    .restart local v3       #childrenCount:I
    .restart local v4       #i:I
    .restart local v5       #index:I
    :cond_39
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    .line 364
    .end local v0           #added:Z
    .end local v1           #childIndex:I
    .end local v2           #childPanel:Landroid/view/View;
    .end local v3           #childrenCount:I
    .end local v4           #i:I
    .end local v5           #index:I
    :cond_3c
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_38
.end method

.method public computeScroll()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 280
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 281
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v3, :cond_3d

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    .line 282
    .local v0, current:I
    :goto_13
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(I)V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->invalidate()V

    .line 285
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v3, :cond_44

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    .line 286
    .local v2, target:I
    :goto_23
    if-ne v0, v2, :cond_2a

    .line 287
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->abortAnimation()V

    .line 290
    :cond_2a
    iget v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_3c

    .line 291
    iget v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4b

    const/4 v1, 0x1

    .line 292
    .local v1, direction:I
    :goto_37
    iput v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    .line 293
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onScrollFinished(I)V

    .line 296
    .end local v0           #current:I
    .end local v1           #direction:I
    .end local v2           #target:I
    :cond_3c
    return-void

    .line 281
    :cond_3d
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    goto :goto_13

    .line 285
    .restart local v0       #current:I
    :cond_44
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getFinalX()I

    move-result v2

    goto :goto_23

    .line 291
    .restart local v2       #target:I
    :cond_4b
    const/4 v1, -0x1

    goto :goto_37
.end method

.method public final getScroll()I
    .registers 2

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_9

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollY()I

    move-result v0

    .line 257
    :goto_8
    return v0

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollX()I

    move-result v0

    goto :goto_8
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter "event"

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->shouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onScrollFinished(I)V
    .registers 2
    .parameter "direction"

    .prologue
    .line 268
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter "event"

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    .line 152
    .local v9, action:I
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    if-eqz v0, :cond_17

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_12

    .line 154
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 156
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 159
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    if-nez v0, :cond_34

    .line 160
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->shouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 161
    const/4 v0, 0x1

    .line 191
    :goto_22
    return v0

    .line 164
    :cond_23
    const/4 v0, 0x1

    if-ne v9, v0, :cond_32

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    if-eqz v0, :cond_32

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->performClick()Z

    move-result v0

    goto :goto_22

    .line 168
    :cond_32
    const/4 v0, 0x1

    goto :goto_22

    .line 171
    :cond_34
    packed-switch v9, :pswitch_data_102

    .line 191
    :goto_37
    const/4 v0, 0x1

    goto :goto_22

    .line 173
    :pswitch_39
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_61

    const/4 v0, 0x1

    :goto_3e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v1, v1, v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v0, v2, v0

    sub-float v10, v1, v0

    .line 174
    .local v10, delta:F
    const/high16 v0, -0x4080

    cmpg-float v0, v10, v0

    if-gez v0, :cond_63

    .line 175
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    .line 180
    :cond_54
    :goto_54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScroll()I

    move-result v0

    float-to-int v1, v10

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(I)V

    .line 187
    .end local v10           #delta:F
    :cond_5d
    :goto_5d
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    goto :goto_37

    .line 173
    :cond_61
    const/4 v0, 0x0

    goto :goto_3e

    .line 176
    .restart local v10       #delta:F
    :cond_63
    const/high16 v0, 0x3f80

    cmpl-float v0, v10, v0

    if-lez v0, :cond_54

    .line 177
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    goto :goto_54

    .line 186
    .end local v10           #delta:F
    :pswitch_6d
    const/4 v0, 0x3

    if-ne v9, v0, :cond_d9

    const/4 v0, 0x1

    :goto_71
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    if-nez v0, :cond_fc

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    if-eqz v0, :cond_fc

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_fc

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_db

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_94
    iget v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_a3

    iget v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_f6

    :cond_a3
    neg-float v4, v0

    iput v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollY()I

    move-result v2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_e2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x0

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v11, 0x1

    aget v8, v8, v11

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    :goto_c5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->invalidate()V

    :goto_c8
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    if-eqz v0, :cond_5d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_5d

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    goto :goto_5d

    :cond_d9
    const/4 v0, 0x0

    goto :goto_71

    :cond_db
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_94

    :cond_e2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    float-to-int v3, v4

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_c5

    :cond_f6
    iget v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onScrollFinished(I)V

    goto :goto_c8

    :cond_fc
    iget v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onScrollFinished(I)V

    goto :goto_c8

    .line 171
    :pswitch_data_102
    .packed-switch 0x1
        :pswitch_6d
        :pswitch_39
        :pswitch_6d
    .end packed-switch
.end method

.method protected final scrollTo(I)V
    .registers 4
    .parameter "position"

    .prologue
    const/4 v1, 0x0

    .line 261
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_d

    .line 262
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(II)V

    .line 266
    :goto_c
    return-void

    .line 264
    :cond_d
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_c
.end method

.method public setFlingable(Z)V
    .registers 2
    .parameter "value"

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    .line 94
    return-void
.end method

.method public setScrollEnabled(Z)V
    .registers 2
    .parameter "value"

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    .line 98
    return-void
.end method

.method public setScrollLimits(II)V
    .registers 5
    .parameter "start"
    .parameter "end"

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 273
    return-void
.end method

.method public setVertical(Z)V
    .registers 2
    .parameter "value"

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    .line 86
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .registers 3
    .parameter "originalView"

    .prologue
    .line 200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 201
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public final smoothScrollTo(I)V
    .registers 14
    .parameter "position"

    .prologue
    const/16 v5, 0x1f4

    const/4 v1, 0x0

    .line 329
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result p1

    .line 330
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScroll()I

    move-result v0

    sub-int v4, p1, v0

    .line 331
    .local v4, delta:I
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_1f

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollY()I

    move-result v2

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 336
    :goto_1b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->invalidate()V

    .line 337
    return-void

    .line 334
    :cond_1f
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollX()I

    move-result v7

    move v8, v1

    move v9, v4

    move v10, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_1b
.end method

.method protected final updatePosition(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "event"

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    aput v2, v0, v1

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    aput v2, v0, v1

    .line 207
    return-void
.end method
