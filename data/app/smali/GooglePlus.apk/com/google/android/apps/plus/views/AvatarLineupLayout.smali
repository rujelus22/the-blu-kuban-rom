.class public Lcom/google/android/apps/plus/views/AvatarLineupLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "AvatarLineupLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sAvatarLineupItemPadding:I

.field private static sAvatarLineupItemSize:I

.field private static sDescriptionFontColor:I

.field private static sDescriptionFontSize:F

.field private static sInitialized:Z


# instance fields
.field private mAvatars:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/AvatarView;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mOverflowText:Landroid/widget/TextView;

.field private mPeople:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTotalPeopleCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 12
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v6, 0x1

    .line 52
    sget-boolean v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sInitialized:Z

    if-nez v0, :cond_2f

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 54
    .local v7, resources:Landroid/content/res/Resources;
    const v0, 0x7f0d00b0

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemPadding:I

    .line 56
    const v0, 0x7f0d00b1

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    .line 59
    const v0, 0x7f0d00b2

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontSize:F

    .line 61
    const v0, 0x7f0a0005

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontColor:I

    .line 63
    sput-boolean v6, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sInitialized:Z

    .line 66
    .end local v7           #resources:Landroid/content/res/Resources;
    :cond_2f
    sget v3, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontSize:F

    sget v4, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sDescriptionFontColor:I

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->addView(Landroid/view/View;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    .line 70
    return-void
.end method


# virtual methods
.method public final bind(Ljava/util/ArrayList;Lcom/google/android/apps/plus/views/EventActionListener;I)V
    .registers 8
    .parameter
    .parameter "listener"
    .parameter "total"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsEventData$EventPerson;",
            ">;",
            "Lcom/google/android/apps/plus/views/EventActionListener;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 167
    .local p1, people:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/EsEventData$EventPerson;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v0, gaiaIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    .line 170
    .local v2, person:Lcom/google/android/apps/plus/content/EsEventData$EventPerson;
    iget-object v3, v2, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 173
    .end local v2           #person:Lcom/google/android/apps/plus/content/EsEventData$EventPerson;
    :cond_1b
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    .line 174
    iput-object p2, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 175
    iput p3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->requestLayout()V

    .line 178
    return-void
.end method

.method public final bindIds(Ljava/util/ArrayList;Lcom/google/android/apps/plus/views/EventActionListener;I)V
    .registers 4
    .parameter
    .parameter "listener"
    .parameter "total"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/plus/views/EventActionListener;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, people:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    .line 189
    iput-object p2, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 190
    iput p3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->requestLayout()V

    .line 193
    return-void
.end method

.method public final clear()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 206
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 208
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 210
    .local v0, count:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_d
    if-ge v1, v0, :cond_25

    .line 211
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/AvatarView;

    .line 212
    .local v2, view:Lcom/google/android/apps/plus/views/AvatarView;
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    .line 213
    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->removeView(Landroid/view/View;)V

    .line 210
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 217
    .end local v2           #view:Lcom/google/android/apps/plus/views/AvatarView;
    :cond_25
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 219
    iput-object v4, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    .line 220
    return-void
.end method

.method protected measureChildren(II)V
    .registers 26
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    .line 86
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    .line 87
    .local v17, totalWidth:I
    sget v18, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    sget v19, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemPadding:I

    add-int v14, v18, v19

    .line 88
    .local v14, singleItemWidth:I
    div-int v8, v17, v14

    .line 89
    .local v8, maxAvatars:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 91
    .local v13, peopleCount:I
    const/4 v7, 0x0

    .line 94
    .local v7, currentX:I
    if-lt v8, v13, :cond_23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v0, v13, :cond_128

    .line 96
    :cond_23
    const/4 v9, 0x0

    .line 97
    .local v9, maxTextWidth:I
    const/4 v5, 0x0

    .line 100
    .local v5, continueRemoving:Z
    :cond_25
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mTotalPeopleCount:I

    move/from16 v18, v0

    sub-int v11, v18, v13

    .line 101
    .local v11, otherCount:I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0e0006

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v11, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 103
    .local v12, overflowText:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    sget v20, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v21, -0x8000

    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->measure(II)V

    .line 106
    mul-int v18, v13, v14

    sub-int v9, v17, v18

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_125

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v18

    move/from16 v0, v18

    if-ge v9, v0, :cond_125

    const/4 v5, 0x1

    .line 111
    :goto_9b
    if-eqz v5, :cond_9f

    .line 112
    add-int/lit8 v13, v13, -0x1

    .line 114
    :cond_9f
    if-nez v5, :cond_25

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const/high16 v19, -0x8000

    move/from16 v0, v19

    invoke-static {v9, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    sget v20, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v21, -0x8000

    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->measure(II)V

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    mul-int v19, v14, v13

    const/16 v20, 0x0

    sget v21, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v22

    sub-int v21, v21, v22

    div-int/lit8 v21, v21, 0x2

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->max(II)I

    move-result v20

    invoke-static/range {v18 .. v20}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->setCorner(Landroid/view/View;II)V

    .line 126
    .end local v5           #continueRemoving:Z
    .end local v9           #maxTextWidth:I
    .end local v11           #otherCount:I
    .end local v12           #overflowText:Ljava/lang/String;
    :goto_e6
    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    sub-int v19, v13, v19

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 128
    .local v4, additionalViews:I
    move v3, v4

    .local v3, addedViewCount:I
    :goto_f9
    if-lez v3, :cond_134

    .line 129
    new-instance v10, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->getContext()Landroid/content/Context;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    .line 130
    .local v10, newView:Lcom/google/android/apps/plus/views/AvatarView;
    move-object/from16 v0, p0

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    .line 132
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->addView(Landroid/view/View;)V

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    add-int/lit8 v3, v3, -0x1

    goto :goto_f9

    .line 108
    .end local v3           #addedViewCount:I
    .end local v4           #additionalViews:I
    .end local v10           #newView:Lcom/google/android/apps/plus/views/AvatarView;
    .restart local v5       #continueRemoving:Z
    .restart local v9       #maxTextWidth:I
    .restart local v11       #otherCount:I
    .restart local v12       #overflowText:Ljava/lang/String;
    :cond_125
    const/4 v5, 0x0

    goto/16 :goto_9b

    .line 122
    .end local v5           #continueRemoving:Z
    .end local v9           #maxTextWidth:I
    .end local v11           #otherCount:I
    .end local v12           #overflowText:Ljava/lang/String;
    :cond_128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mOverflowText:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const/16 v19, 0x8

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_e6

    .line 137
    .restart local v3       #addedViewCount:I
    .restart local v4       #additionalViews:I
    :cond_134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 139
    .local v16, totalAvatarCount:I
    const/4 v6, 0x0

    .local v6, currentIndex:I
    :goto_13f
    move/from16 v0, v16

    if-ge v6, v0, :cond_1aa

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mAvatars:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/views/AvatarView;

    .line 142
    .local v15, targetView:Lcom/google/android/apps/plus/views/AvatarView;
    if-ge v6, v13, :cond_19b

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mPeople:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 145
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    .line 146
    sget v18, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v19, 0x4000

    invoke-static/range {v18 .. v19}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v18

    sget v19, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    const/high16 v20, 0x4000

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    .line 150
    if-lez v6, :cond_198

    sget v18, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemPadding:I

    :goto_188
    add-int v7, v7, v18

    .line 151
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-static {v15, v7, v0}, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->setCorner(Landroid/view/View;II)V

    .line 152
    sget v18, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->sAvatarLineupItemSize:I

    add-int v7, v7, v18

    .line 139
    :goto_195
    add-int/lit8 v6, v6, 0x1

    goto :goto_13f

    .line 150
    :cond_198
    const/16 v18, 0x0

    goto :goto_188

    .line 154
    :cond_19b
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 155
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_195

    .line 158
    .end local v15           #targetView:Lcom/google/android/apps/plus/views/AvatarView;
    :cond_1aa
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 200
    instance-of v0, p1, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_13

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AvatarLineupLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    check-cast p1, Lcom/google/android/apps/plus/views/AvatarView;

    .end local p1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onAvatarClicked(Ljava/lang/String;)V

    .line 203
    :cond_13
    return-void
.end method
