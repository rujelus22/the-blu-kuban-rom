.class final Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "PostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 262
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    return-void
.end method


# virtual methods
.method public final onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$100(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$100(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_23

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->insertCameraPhoto(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$200(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$102(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 270
    :cond_23
    return-void
.end method

.method public final onPostActivityResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$300(Lcom/google/android/apps/plus/fragments/PostFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 275
    return-void
.end method
