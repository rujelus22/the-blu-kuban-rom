.class public final Lcom/google/android/apps/plus/hangout/Compatibility;
.super Ljava/lang/Object;
.source "Compatibility.java"


# direct methods
.method public static getCameraOrientation(Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)I
    .registers 5
    .parameter "cameraProperties"

    .prologue
    .line 94
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "HTC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_52

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "PC36100"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_46

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "myTouch_4G"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_46

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "HTC Glacier"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_46

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "ADR6400L"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_46

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "HTC Incredible S S710e"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_46

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "A9191"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_52

    :cond_46
    const/4 v2, 0x1

    :goto_47
    if-eqz v2, :cond_54

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->isFrontFacing()Z

    move-result v2

    if-eqz v2, :cond_54

    .line 95
    const/16 v0, 0x10e

    .line 105
    :goto_51
    return v0

    .line 94
    :cond_52
    const/4 v2, 0x0

    goto :goto_47

    .line 100
    :cond_54
    :try_start_54
    sget-object v2, Lcom/google/android/apps/plus/util/Property;->HANGOUT_CAMERA_ORIENTATION:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, orientationOverride:Ljava/lang/String;
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 102
    .local v0, orientation:I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Using camera orientation of: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V
    :try_end_70
    .catch Ljava/lang/NumberFormatException; {:try_start_54 .. :try_end_70} :catch_71

    goto :goto_51

    .line 105
    .end local v0           #orientation:I
    .end local v1           #orientationOverride:Ljava/lang/String;
    :catch_71
    move-exception v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->getOrientation()I

    move-result v0

    goto :goto_51
.end method

.method public static getSupportedPreviewSizes(Landroid/hardware/Camera$Parameters;Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)Ljava/util/List;
    .registers 8
    .parameter "params"
    .parameter "cameraProperties"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/hardware/Camera$Parameters;",
            "Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    .line 75
    .local v2, reportedSizes:Ljava/util/List;,"Ljava/util/List<Landroid/hardware/Camera$Size;>;"
    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v5, "motorola"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4c

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v5, "DROID3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4c

    const/4 v4, 0x1

    :goto_19
    if-eqz v4, :cond_4e

    .line 76
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->isFrontFacing()Z

    move-result v4

    if-eqz v4, :cond_4e

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 79
    .local v0, filteredSizes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Camera$Size;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_30
    :goto_30
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Camera$Size;

    .line 80
    .local v3, size:Landroid/hardware/Camera$Size;
    iget v4, v3, Landroid/hardware/Camera$Size;->width:I

    const/16 v5, 0xf0

    if-ne v4, v5, :cond_48

    iget v4, v3, Landroid/hardware/Camera$Size;->height:I

    const/16 v5, 0xa0

    if-eq v4, v5, :cond_30

    .line 81
    :cond_48
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_30

    .line 75
    .end local v0           #filteredSizes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/hardware/Camera$Size;>;"
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #size:Landroid/hardware/Camera$Size;
    :cond_4c
    const/4 v4, 0x0

    goto :goto_19

    :cond_4e
    move-object v0, v2

    .line 88
    :cond_4f
    return-object v0
.end method
