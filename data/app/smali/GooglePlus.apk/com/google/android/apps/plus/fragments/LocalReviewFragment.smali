.class public Lcom/google/android/apps/plus/fragments/LocalReviewFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "LocalReviewFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mContainer:Landroid/view/ViewGroup;

.field private mContext:Landroid/app/Activity;

.field private mPersonId:Ljava/lang/String;

.field private mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

.field private mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

.field private mReviewIndex:I

.field private mReviewType:I


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 179
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 2
    .parameter "activity"

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onAttach(Landroid/app/Activity;)V

    .line 77
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContext:Landroid/app/Activity;

    .line 78
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 10
    .parameter "v"

    .prologue
    .line 189
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 191
    .local v2, id:I
    sparse-switch v2, :sswitch_data_48

    .line 201
    :cond_7
    :goto_7
    return-void

    :sswitch_8
    move-object v0, p1

    .line 193
    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    .line 194
    .local v0, authorAvatar:Lcom/google/android/apps/plus/views/AvatarView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, 0x0

    invoke-static {v5, v6, v4, v7}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_7

    .line 198
    .end local v0           #authorAvatar:Lcom/google/android/apps/plus/views/AvatarView;
    :sswitch_24
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    iget-object v1, v4, Lcom/google/api/services/plusi/model/AuthorProto;->profileId:Ljava/lang/String;

    .line 199
    .local v1, gaiaId:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://maps.google.com/maps?q=*+by:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 200
    .local v3, url:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContext:Landroid/app/Activity;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_7

    .line 191
    nop

    :sswitch_data_48
    .sparse-switch
        0x7f090121 -> :sswitch_24
        0x7f0901f0 -> :sswitch_8
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 70
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "person_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/16 v2, 0x64

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 72
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    const-string v0, "LocalReviewFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 95
    const-string v0, "LocalReviewFragment"

    const-string v1, "Loader<ProfileAndContactData> onCreateLoader()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_10
    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "person_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 83
    const v1, 0x7f030059

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    const v2, 0x7f0901f0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    .line 86
    .local v0, authorAvatar:Lcom/google/android/apps/plus/views/AvatarView;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    .line 87
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    return-object v1
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 11
    .parameter "x0"
    .parameter "x1"

    .prologue
    const v7, 0x7f090121

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/16 v6, 0x8

    .line 38
    check-cast p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    .end local p2
    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewType:I

    if-nez v0, :cond_bf

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    :cond_21
    :goto_21
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/FrontendPaperProto;->title:Lcom/google/api/services/plusi/model/TitleProto;

    if-eqz v1, :cond_e4

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Page;->localInfo:Lcom/google/api/services/plusi/model/LocalEntityInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocalEntityInfo;->paper:Lcom/google/api/services/plusi/model/FrontendPaperProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/FrontendPaperProto;->title:Lcom/google/api/services/plusi/model/TitleProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/TitleProto;->linkedTitle:Lcom/google/api/services/plusi/model/PlacePageLink;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    :goto_39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    const v2, 0x7f0803b6

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    const v2, 0x7f09011f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setTopBorderVisible(Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setIsFullText(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setAuthorAvatarOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    const v2, 0x7f0803b7

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    const v1, 0x7f09011e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    const v2, 0x7f090120

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    const v4, 0x7f090122

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mContainer:Landroid/view/ViewGroup;

    const v5, 0x7f090124

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_bf
    iget v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewType:I

    if-ne v0, v4, :cond_cd

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getYourReview(Lcom/google/api/services/plusi/model/SimpleProfile;)Lcom/google/api/services/plusi/model/GoogleReviewProto;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    goto/16 :goto_21

    :cond_cd
    iget v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_21

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mProfile:Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsLocalPageData;->getCircleReviews(Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReview:Lcom/google/api/services/plusi/model/GoogleReviewProto;

    goto/16 :goto_21

    :cond_e4
    const/4 v0, 0x0

    goto/16 :goto_39
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, arg0:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;>;"
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 3
    .parameter "args"

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 59
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 60
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mPersonId:Ljava/lang/String;

    .line 61
    const-string v0, "local_review_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewType:I

    .line 62
    const-string v0, "local_review_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/LocalReviewFragment;->mReviewIndex:I

    .line 63
    return-void
.end method

.method public final recordNavigationAction()V
    .registers 1

    .prologue
    .line 175
    return-void
.end method
