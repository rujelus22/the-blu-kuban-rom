.class final Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;
.super Ljava/lang/Object;
.source "HostedProfileFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 224
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 225
    const-string v0, "HostedProfileFragment"

    const-string v1, "Loader<ProfileAndContactData> onCreateLoader()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_10
    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "person_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 220
    check-cast p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    .end local p2
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "HostedProfileFragment"

    const-string v1, "Loader<ProfileAndContactData> onLoadFinished()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$302(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_27

    iget v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_27

    iget v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-ne v0, v3, :cond_33

    :cond_27
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$400(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Z

    move-result v0

    if-eqz v0, :cond_61

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v0, :cond_61

    :cond_33
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iput-boolean v3, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mError:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMapLoaderActive:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$502(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-nez v0, :cond_5d

    const v0, 0x7f0801ab

    :goto_4b
    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->showError(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$700(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    :cond_5c
    :goto_5c
    return-void

    :cond_5d
    const v0, 0x7f0802a6

    goto :goto_4b

    :cond_61
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mError:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setProfileData(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v1

    #setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$802(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isBlocked()Z

    move-result v1

    #setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$902(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isMuted()Z

    move-result v1

    #setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1002(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_f9

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v0, :cond_f9

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    if-eqz v0, :cond_f9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v1, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/User;->locations:Lcom/google/api/services/plusi/model/Locations;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/Locations;->locationMapUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/String;)V

    :goto_ba
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->onAsyncData()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1300(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)Z

    move-result v0

    if-eqz v0, :cond_d6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refreshProfile()V

    :cond_d6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1400(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    move-result-object v0

    if-eqz v0, :cond_5c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1400(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    move-result-object v0

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mProfileCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->access$1500(Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;)Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v0

    if-eqz v0, :cond_5c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1400(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    move-result-object v0

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->mProfileCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->access$1500(Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;)Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->requery()Z

    goto/16 :goto_5c

    :cond_f9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMapLoaderActive:Z
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$502(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z

    goto :goto_ba
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 290
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;>;"
    return-void
.end method
