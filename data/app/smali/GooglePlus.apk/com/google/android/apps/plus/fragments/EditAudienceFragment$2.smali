.class final Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;
.super Ljava/lang/Object;
.source "EditAudienceFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    #calls: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$500(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v6

    if-eq v1, v4, :cond_36

    :cond_1f
    :goto_1f
    if-nez v0, :cond_35

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->access$600(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->notifyDataSetChanged()V

    .line 318
    :cond_35
    return-void

    .line 314
    :cond_36
    if-ne v5, v6, :cond_1f

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    array-length v6, v5

    move v1, v0

    :goto_43
    if-ge v1, v6, :cond_51

    aget-object v7, v5, v1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_43

    :cond_51
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    array-length v6, v5

    move v1, v0

    :goto_57
    if-ge v1, v6, :cond_68

    aget-object v7, v5, v1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1f

    add-int/lit8 v1, v1, 0x1

    goto :goto_57

    :cond_68
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v3

    array-length v5, v3

    move v1, v0

    :goto_73
    if-ge v1, v5, :cond_81

    aget-object v6, v3, v1

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_73

    :cond_81
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_87
    if-ge v1, v3, :cond_98

    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    add-int/lit8 v1, v1, 0x1

    goto :goto_87

    :cond_98
    const/4 v0, 0x1

    goto :goto_1f
.end method
