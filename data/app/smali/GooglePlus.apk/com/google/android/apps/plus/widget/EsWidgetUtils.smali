.class public final Lcom/google/android/apps/plus/widget/EsWidgetUtils;
.super Ljava/lang/Object;
.source "EsWidgetUtils.java"


# direct methods
.method public static loadCircleId(Landroid/content/Context;I)Ljava/lang/String;
    .registers 6
    .parameter "context"
    .parameter "appWidgetId"

    .prologue
    .line 53
    const-string v2, "com.google.android.apps.plus.widget.EsWidgetUtils"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 54
    .local v1, prefs:Landroid/content/SharedPreferences;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "circleId_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, circleId:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 56
    const/4 v0, 0x0

    .line 58
    :cond_22
    return-object v0
.end method

.method public static saveCircleInfo(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "appWidgetId"
    .parameter "circleId"
    .parameter "circleName"

    .prologue
    .line 39
    const-string v1, "com.google.android.apps.plus.widget.EsWidgetUtils"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 41
    .local v0, prefs:Landroid/content/SharedPreferences$Editor;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "circleId_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez p2, :cond_1e

    const-string p2, ""

    .end local p2
    :cond_1e
    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 43
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "circleName_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez p3, :cond_34

    const-string p3, ""

    .end local p3
    :cond_34
    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 45
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-ge v1, v2, :cond_41

    .line 46
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 50
    :goto_40
    return-void

    .line 48
    :cond_41
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_40
.end method

.method public static updateAllWidgets$1a552341(Landroid/content/Context;)V
    .registers 10
    .parameter "context"

    .prologue
    const/4 v8, 0x0

    .line 102
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 103
    .local v2, appWidgetManager:Landroid/appwidget/AppWidgetManager;
    new-instance v6, Landroid/content/ComponentName;

    const-class v7, Lcom/google/android/apps/plus/widget/EsWidgetProvider;

    invoke-direct {v6, p0, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v6}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v1

    .line 105
    .local v1, appWidgetIds:[I
    move-object v3, v1

    .local v3, arr$:[I
    array-length v5, v1

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_13
    if-ge v4, v5, :cond_2c

    aget v0, v3, v4

    .line 108
    .local v0, appWidgetId:I
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 109
    const-string v6, "v.all.circles"

    invoke-static {p0, v0, v6, v8}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->saveCircleInfo(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_26
    invoke-static {p0, v0, v8}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;)V

    .line 105
    add-int/lit8 v4, v4, 0x1

    goto :goto_13

    .line 113
    .end local v0           #appWidgetId:I
    :cond_2c
    return-void
.end method
