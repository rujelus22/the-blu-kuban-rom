.class public Lcom/google/android/apps/plus/content/AccountSettingsData;
.super Ljava/lang/Object;
.source "AccountSettingsData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/AccountSettingsData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIsChild:Z

.field private mPlusPageIds:[Ljava/lang/String;

.field private mPlusPageNames:[Ljava/lang/String;

.field private mPlusPagePhotoUrls:[Ljava/lang/String;

.field private mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

.field private mUserDisplayName:Ljava/lang/String;

.field private mUserGaiaId:Ljava/lang/String;

.field private mWarmWelcomeTimestamp:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 195
    new-instance v0, Lcom/google/android/apps/plus/content/AccountSettingsData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/AccountSettingsData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/AccountSettingsData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "parcel"

    .prologue
    const/4 v2, 0x1

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_4e

    move v1, v2

    :goto_17
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, shareboxSettingsJson:Ljava/lang/String;
    if-eqz v0, :cond_50

    invoke-static {}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->getInstance()Lcom/google/api/services/plusi/model/ShareboxSettingsJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/ShareboxSettings;

    :goto_29
    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_3b

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    .line 160
    :cond_3b
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    .line 163
    return-void

    .line 150
    .end local v0           #shareboxSettingsJson:Ljava/lang/String;
    :cond_4e
    const/4 v1, 0x0

    goto :goto_17

    .line 153
    .restart local v0       #shareboxSettingsJson:Ljava/lang/String;
    :cond_50
    const/4 v1, 0x0

    goto :goto_29
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/AccountSettingsData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;)V
    .registers 4
    .parameter "settingsResponse"

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    if-eqz v1, :cond_2d

    .line 43
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    .line 45
    .local v0, user:Lcom/google/api/services/plusi/model/MobileSettingsUser;
    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    if-eqz v1, :cond_3c

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3c

    const/4 v1, 0x1

    :goto_16
    iput-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    .line 47
    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    if-eqz v1, :cond_28

    .line 48
    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    .line 49
    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->displayName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    .line 52
    :cond_28
    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileSettingsUser;->plusPageInfo:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/content/AccountSettingsData;->setPlusPages(Ljava/util/List;)V

    .line 55
    .end local v0           #user:Lcom/google/api/services/plusi/model/MobileSettingsUser;
    :cond_2d
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    if-eqz v1, :cond_37

    .line 56
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobilePreference;->wwMainFlowAckTimestampMsec:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    .line 59
    :cond_37
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    iput-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    .line 60
    return-void

    .line 45
    .restart local v0       #user:Lcom/google/api/services/plusi/model/MobileSettingsUser;
    :cond_3c
    const/4 v1, 0x0

    goto :goto_16
.end method

.method private setPlusPages(Ljava/util/List;)V
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, pages:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;>;"
    if-eqz p1, :cond_30

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 64
    .local v1, numPages:I
    :goto_6
    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    .line 65
    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    .line 66
    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    .line 68
    const/4 v0, 0x0

    .local v0, i:I
    :goto_13
    if-ge v0, v1, :cond_32

    .line 69
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    .line 70
    .local v2, page:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;
    iget-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->displayName:Ljava/lang/String;

    aput-object v4, v3, v0

    .line 71
    iget-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v4, v3, v0

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    iget-object v4, v2, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoUrl:Ljava/lang/String;

    aput-object v4, v3, v0

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 63
    .end local v0           #i:I
    .end local v1           #numPages:I
    .end local v2           #page:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;
    :cond_30
    const/4 v1, 0x0

    goto :goto_6

    .line 74
    .restart local v0       #i:I
    .restart local v1       #numPages:I
    :cond_32
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 189
    const/4 v0, 0x0

    return v0
.end method

.method public final getNumPlusPages()I
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public final getPlusPageId(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getPlusPageName(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getPlusPagePhotoUrl(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getShareboxSettings()Lcom/google/api/services/plusi/model/ShareboxSettings;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    return-object v0
.end method

.method public final getUserDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserGaiaId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final getUserPhotoUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getWarmWelcomeTimestamp()Ljava/lang/Long;
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    return-object v0
.end method

.method public final isChild()Z
    .registers 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 8
    .parameter "dest"
    .parameter "flags"

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserGaiaId:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 168
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mUserDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 169
    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mIsChild:Z

    if-eqz v1, :cond_45

    move v1, v2

    :goto_11
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    if-eqz v1, :cond_47

    invoke-static {}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->getInstance()Lcom/google/api/services/plusi/model/ShareboxSettingsJson;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mShareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    invoke-virtual {v1, v4}, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 173
    .local v0, shareboxSettingsJson:Ljava/lang/String;
    :goto_22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    if-eqz v1, :cond_49

    .line 176
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mWarmWelcomeTimestamp:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 182
    :goto_35
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageNames:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPageIds:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AccountSettingsData;->mPlusPagePhotoUrls:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 185
    return-void

    .end local v0           #shareboxSettingsJson:Ljava/lang/String;
    :cond_45
    move v1, v3

    .line 169
    goto :goto_11

    .line 171
    :cond_47
    const/4 v0, 0x0

    goto :goto_22

    .line 179
    .restart local v0       #shareboxSettingsJson:Ljava/lang/String;
    :cond_49
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_35
.end method
