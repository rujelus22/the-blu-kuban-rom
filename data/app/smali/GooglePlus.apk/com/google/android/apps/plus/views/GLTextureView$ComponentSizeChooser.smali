.class Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;
.super Lcom/google/android/apps/plus/views/GLTextureView$BaseConfigChooser;
.source "GLTextureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/GLTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComponentSizeChooser"
.end annotation


# instance fields
.field protected mAlphaSize:I

.field protected mBlueSize:I

.field protected mDepthSize:I

.field protected mGreenSize:I

.field protected mRedSize:I

.field protected mStencilSize:I

.field private mValue:[I

.field final synthetic this$0:Lcom/google/android/apps/plus/views/GLTextureView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/GLTextureView;IIIIII)V
    .registers 12
    .parameter
    .parameter "redSize"
    .parameter "greenSize"
    .parameter "blueSize"
    .parameter "alphaSize"
    .parameter "depthSize"
    .parameter "stencilSize"

    .prologue
    const/4 v3, 0x1

    .line 908
    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    .line 909
    const/16 v0, 0xd

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x3024

    aput v2, v0, v1

    aput p2, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0x3023

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p3, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x3022

    aput v2, v0, v1

    const/4 v1, 0x5

    aput p4, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x3021

    aput v2, v0, v1

    const/4 v1, 0x7

    aput p5, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x3025

    aput v2, v0, v1

    const/16 v1, 0x9

    aput p6, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x3026

    aput v2, v0, v1

    const/16 v1, 0xb

    aput p7, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x3038

    aput v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/GLTextureView$BaseConfigChooser;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;[I)V

    .line 917
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mValue:[I

    .line 918
    iput p2, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mRedSize:I

    .line 919
    iput p3, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mGreenSize:I

    .line 920
    iput p4, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mBlueSize:I

    .line 921
    iput p5, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mAlphaSize:I

    .line 922
    iput p6, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mDepthSize:I

    .line 923
    iput p7, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mStencilSize:I

    .line 924
    return-void
.end method

.method private findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I
    .registers 8
    .parameter "egl"
    .parameter "display"
    .parameter "config"
    .parameter "attribute"
    .parameter "defaultValue"

    .prologue
    const/4 v0, 0x0

    .line 955
    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mValue:[I

    invoke-interface {p1, p2, p3, p4, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 956
    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mValue:[I

    aget v0, v1, v0

    .line 958
    :cond_d
    return v0
.end method


# virtual methods
.method public final chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLConfig;
    .registers 20
    .parameter "egl"
    .parameter "display"
    .parameter "configs"

    .prologue
    .line 929
    move-object/from16 v8, p3

    .local v8, arr$:[Ljavax/microedition/khronos/egl/EGLConfig;
    move-object/from16 v0, p3

    array-length v13, v0

    .local v13, len$:I
    const/4 v12, 0x0

    .local v12, i$:I
    :goto_6
    if-ge v12, v13, :cond_80

    aget-object v4, v8, v12

    .line 930
    .local v4, config:Ljavax/microedition/khronos/egl/EGLConfig;
    const/16 v5, 0x3025

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v10

    .line 932
    .local v10, d:I
    const/16 v5, 0x3026

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v15

    .line 934
    .local v15, s:I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mDepthSize:I

    if-lt v10, v1, :cond_7d

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mStencilSize:I

    if-lt v15, v1, :cond_7d

    .line 935
    const/16 v5, 0x3024

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v14

    .line 937
    .local v14, r:I
    const/16 v5, 0x3023

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v11

    .line 939
    .local v11, g:I
    const/16 v5, 0x3022

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v9

    .line 941
    .local v9, b:I
    const/16 v5, 0x3021

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->findConfigAttrib(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;II)I

    move-result v7

    .line 943
    .local v7, a:I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mRedSize:I

    if-ne v14, v1, :cond_7d

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mGreenSize:I

    if-ne v11, v1, :cond_7d

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mBlueSize:I

    if-ne v9, v1, :cond_7d

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;->mAlphaSize:I

    if-ne v7, v1, :cond_7d

    .line 949
    .end local v4           #config:Ljavax/microedition/khronos/egl/EGLConfig;
    .end local v7           #a:I
    .end local v9           #b:I
    .end local v10           #d:I
    .end local v11           #g:I
    .end local v14           #r:I
    .end local v15           #s:I
    :goto_7c
    return-object v4

    .line 929
    .restart local v4       #config:Ljavax/microedition/khronos/egl/EGLConfig;
    .restart local v10       #d:I
    .restart local v15       #s:I
    :cond_7d
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    .line 949
    .end local v4           #config:Ljavax/microedition/khronos/egl/EGLConfig;
    .end local v10           #d:I
    .end local v15           #s:I
    :cond_80
    const/4 v4, 0x0

    goto :goto_7c
.end method
