.class public interface abstract Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;
.super Ljava/lang/Object;
.source "SuggestionGridAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SuggestionGridAdapterListener"
.end annotation


# virtual methods
.method public abstract onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onDismissSuggestionAction(Ljava/lang/String;)V
.end method

.method public abstract onPersonSelected(Ljava/lang/String;)V
.end method
