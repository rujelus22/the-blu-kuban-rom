.class public final Lcom/google/android/apps/plus/api/SearchActivitiesOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SearchActivitiesOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SearchQueryRequest;",
        "Lcom/google/api/services/plusi/model/SearchQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private final mQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "query"
    .parameter "continuationToken"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 40
    const-string v3, "searchquery"

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 10
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 29
    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    if-eqz v0, :cond_3c

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/SearchUtils;->getSearchKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ActivityResults;->stream:Lcom/google/api/services/plusi/model/Stream;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    const-string v4, "DEFAULT"

    iget-object v5, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/ActivityResults;->shownActivitiesBlob:Ljava/lang/String;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsPostsData;->updateStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/SearchResults;->activityResults:Lcom/google/api/services/plusi/model/ActivityResults;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/ActivityResults;->shownActivitiesBlob:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/util/SearchUtils;->insertSearchResults(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3b
    return-void

    :cond_3c
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, v7, v7}, Lcom/google/android/apps/plus/util/SearchUtils;->insertSearchResults(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3b
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 29
    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;

    .end local p1
    new-instance v0, Lcom/google/api/services/plusi/model/SearchQuery;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchQuery;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mQuery:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->queryText:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    const-string v1, "RECENT"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->sort:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    const-string v1, "TACOS"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->filter:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/ActivityRequestData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ActivityRequestData;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_30

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->shownActivitiesBlob:Ljava/lang/String;

    :cond_30
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    new-instance v1, Lcom/google/api/services/plusi/model/ActivityFilters;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ActivityFilters;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    new-instance v1, Lcom/google/api/services/plusi/model/FieldRequestOptions;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/FieldRequestOptions;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->fieldRequestOptions:Lcom/google/api/services/plusi/model/FieldRequestOptions;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/FieldRequestOptions;->includeLegacyMediaData:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    new-instance v1, Lcom/google/api/services/plusi/model/UpdateFilter;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/UpdateFilter;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->activityRequestData:Lcom/google/api/services/plusi/model/ActivityRequestData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityRequestData;->activityFilters:Lcom/google/api/services/plusi/model/ActivityFilters;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ActivityFilters;->updateFilter:Lcom/google/api/services/plusi/model/UpdateFilter;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPostsData;->getStreamNamespaces(Z)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/UpdateFilter;->includeNamespace:Ljava/util/List;

    return-void
.end method
