.class public Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;
.super Ljava/lang/Object;
.source "ApiaryHttpRequestConfiguration.java"

# interfaces
.implements Lcom/google/android/apps/plus/network/HttpRequestConfiguration;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mBackendOverrideUrl:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mScope:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "scope"
    .parameter "backendOverrideUrl"

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mScope:Ljava/lang/String;

    .line 35
    iput-object p4, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V
    .registers 10
    .parameter "method"

    .prologue
    .line 43
    const-string v5, "Accept-Encoding"

    const-string v6, "gzip"

    invoke-virtual {p1, v5, v6}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v5, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v5, "User-Agent"

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->getUserAgentHeader(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v5, "Content-Type"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->getContentType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v5, :cond_63

    .line 55
    :try_start_2c
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mScope:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory;->getAuthData(Ljava/lang/String;)Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;

    move-result-object v0

    .line 56
    .local v0, authData:Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v5, v6}, Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;->getAuthToken(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, authToken:Ljava/lang/String;
    invoke-interface {v0, v2}, Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;->getAuthTime(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_49
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_49} :catch_9f

    move-result-object v1

    .line 62
    .local v1, authTime:Ljava/lang/String;
    const-string v5, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Bearer "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v5, "X-Auth-Time"

    invoke-virtual {p1, v5, v1}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .end local v0           #authData:Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;
    .end local v1           #authTime:Ljava/lang/String;
    .end local v2           #authToken:Ljava/lang/String;
    :cond_63
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getStickyC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, c2dmRegistrationId:Ljava/lang/String;
    if-eqz v3, :cond_70

    .line 69
    const-string v5, "X-Android-App-ID"

    invoke-virtual {p1, v5, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_70
    iget-object v5, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9e

    .line 73
    const-string v5, "HttpTransaction"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_97

    .line 74
    const-string v5, "HttpTransaction"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Setting backend override url "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :cond_97
    const-string v5, "X-Google-Backend-Override"

    iget-object v6, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mBackendOverrideUrl:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Lorg/apache/http/client/methods/HttpRequestBase;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_9e
    return-void

    .line 58
    .end local v3           #c2dmRegistrationId:Ljava/lang/String;
    :catch_9f
    move-exception v4

    .line 59
    .local v4, e:Ljava/lang/Exception;
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Cannot obtain authentication token"

    invoke-direct {v5, v6, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
.end method

.method protected getContentType()Ljava/lang/String;
    .registers 2

    .prologue
    .line 109
    const-string v0, "application/json"

    return-object v0
.end method

.method protected getUserAgentHeader(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter "context"

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/network/UserAgent;->from(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " (gzip)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final invalidateAuthToken()V
    .registers 5

    .prologue
    .line 85
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_15

    .line 89
    :try_start_4
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mScope:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory;->getAuthData(Ljava/lang/String;)Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/plus/network/ApiaryAuthDataFactory$ApiaryAuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_15} :catch_16

    .line 95
    :cond_15
    return-void

    .line 91
    :catch_16
    move-exception v0

    .line 92
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot invalidate authentication token"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
