.class public Lcom/google/android/apps/plus/phone/PlusOneActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PlusOneActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

.field private mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

.field private mInsert:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PlusOneActivity;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordExitedAction()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/PlusOneActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mInsert:Z

    return v0
.end method

.method private recordErrorAndFinish()V
    .registers 4

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->finish()V

    .line 154
    return-void
.end method

.method private recordExitedAction()V
    .registers 2

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mInsert:Z

    if-eqz v0, :cond_a

    .line 232
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PLUSONE_CONFIRMED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 238
    :goto_9
    return-void

    .line 236
    :cond_a
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_UNDO_PLUSONE_CANCELED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_9
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onAttachedToWindow()V
    .registers 3

    .prologue
    .line 216
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachedToWindow()V

    .line 218
    const v1, 0x7f09023d

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 219
    .local v0, progressBarView:Landroid/widget/ProgressBar;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 220
    return-void
.end method

.method public onBackPressed()V
    .registers 1

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordExitedAction()V

    .line 225
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    .line 226
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 205
    packed-switch p2, :pswitch_data_c

    .line 212
    :goto_3
    return-void

    .line 207
    :pswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->setResult(I)V

    .line 208
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->finish()V

    goto :goto_3

    .line 205
    :pswitch_data_c
    .packed-switch -0x3
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 25
    .parameter "savedInstanceState"

    .prologue
    .line 52
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    const v1, 0x7f030090

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->setContentView(I)V

    .line 56
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->showTitlebar(Z)V

    .line 57
    const v1, 0x7f080029

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 59
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v19

    .line 60
    .local v19, intent:Landroid/content/Intent;
    new-instance v15, Landroid/os/Bundle;

    invoke-direct {v15}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v15, args:Landroid/os/Bundle;
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 65
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 147
    :goto_3c
    return-void

    .line 69
    :cond_3d
    const-string v1, "PlusOneFragment#mAccount"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v15, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 72
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v16

    .line 73
    .local v16, callingPackage:Ljava/lang/String;
    const-string v1, "from_signup"

    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    .line 74
    .local v17, fromSignup:Z
    const-string v1, "calling_package"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 75
    .local v4, thirdPartyPackageName:Ljava/lang/String;
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v10, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "com.google.circles.platform.intent.extra.APIKEY"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "com.google.circles.platform.intent.extra.CLIENTID"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "com.google.circles.platform.intent.extra.APIVERSION"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    const/4 v8, 0x0

    move-object v12, v6

    move-object v13, v1

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 78
    if-nez p1, :cond_ae

    .line 79
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 84
    :cond_ae
    if-eqz v17, :cond_c2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_c7

    .line 86
    :cond_c2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordErrorAndFinish()V

    goto/16 :goto_3c

    .line 90
    :cond_c7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v20

    .line 91
    .local v20, source:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    if-eqz v20, :cond_103

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_103

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getCertificate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_103

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_103

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_103

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_108

    .line 96
    :cond_103
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordErrorAndFinish()V

    goto/16 :goto_3c

    .line 99
    :cond_108
    const-string v1, "PlusOneFragment#mApiaryApiInfo"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {v15, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 101
    const-string v1, "com.google.circles.platform.intent.extra.TOKEN"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 103
    .local v21, token:Ljava/lang/String;
    const-string v1, "com.google.circles.platform.intent.extra.ENTITY"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 105
    .local v22, url:Ljava/lang/String;
    const-string v1, "com.google.circles.platform.intent.extra.ACTION"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 108
    .local v14, action:Ljava/lang/String;
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13b

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13b

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_140

    .line 109
    :cond_13b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordErrorAndFinish()V

    goto/16 :goto_3c

    .line 112
    :cond_140
    const-string v1, "delete"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1cb

    const/4 v1, 0x1

    :goto_149
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mInsert:Z

    .line 114
    const-string v1, "PlusOneFragment#mToken"

    move-object/from16 v0, v21

    invoke-virtual {v15, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v1, "PlusOneFragment#mUrl"

    move-object/from16 v0, v22

    invoke-virtual {v15, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "PlusOneFragment#mInsert"

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mInsert:Z

    invoke-virtual {v15, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PlusOneActivity#Fragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    .line 120
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    if-nez v1, :cond_1a3

    .line 121
    new-instance v1, Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    .line 122
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    invoke-virtual {v1, v15}, Lcom/google/android/apps/plus/fragments/PlusOneFragment;->setArguments(Landroid/os/Bundle;)V

    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v18

    .line 124
    .local v18, ft:Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f090192

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PlusOneFragment;

    const-string v3, "PlusOneActivity#Fragment"

    move-object/from16 v0, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 125
    invoke-virtual/range {v18 .. v18}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 129
    .end local v18           #ft:Landroid/support/v4/app/FragmentTransaction;
    :cond_1a3
    const v1, 0x7f090191

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/phone/PlusOneActivity$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/phone/PlusOneActivity$1;-><init>(Lcom/google/android/apps/plus/phone/PlusOneActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    const v1, 0x7f090192

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/phone/PlusOneActivity$2;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/phone/PlusOneActivity$2;-><init>(Lcom/google/android/apps/plus/phone/PlusOneActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3c

    .line 112
    :cond_1cb
    const/4 v1, 0x0

    goto/16 :goto_149
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter "dialogId"
    .parameter "args"

    .prologue
    .line 188
    packed-switch p1, :pswitch_data_22

    .line 197
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 190
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 191
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v1, 0x7f08017e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 193
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_4

    .line 188
    nop

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 165
    :cond_b
    return-void
.end method

.method protected final showTitlebar(ZZ)V
    .registers 8
    .parameter "showAnimation"
    .parameter "enableUp"

    .prologue
    const/4 v4, 0x0

    .line 245
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->showTitlebar(ZZ)V

    .line 247
    const v2, 0x7f090236

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 248
    .local v1, titleLayout:Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d011a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 250
    .local v0, paddingLeft:I
    invoke-virtual {v1, v0, v4, v4, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 251
    return-void
.end method
