.class public Lcom/google/android/apps/plus/views/EventActionButtonLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventActionButtonLayout.java"


# static fields
.field private static sSpacing:I


# instance fields
.field private mImage:Landroid/widget/ImageView;

.field private mText:Landroid/widget/TextView;

.field private sInitialized:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v3, 0x1

    .line 42
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sInitialized:Z

    if-nez v2, :cond_14

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 44
    .local v1, resources:Landroid/content/res/Resources;
    const v2, 0x7f0d00b5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sSpacing:I

    .line 46
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sInitialized:Z

    .line 48
    .end local v1           #resources:Landroid/content/res/Resources;
    :cond_14
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setClickable(Z)V

    .line 49
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setFocusable(Z)V

    .line 50
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setWillNotDraw(Z)V

    .line 52
    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    .local v0, a:Landroid/content/res/TypedArray;
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 55
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 57
    new-instance v2, Landroid/widget/TextView;

    const v3, 0x7f0f0023

    invoke-direct {v2, p1, p2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    .line 58
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->addView(Landroid/view/View;)V

    .line 59
    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    .line 60
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->addView(Landroid/view/View;)V

    .line 61
    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .registers 4
    .parameter "text"
    .parameter "image"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method

.method protected onMeasure(II)V
    .registers 13
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x1

    const/high16 v5, -0x8000

    const/4 v9, 0x0

    .line 81
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 82
    .local v2, width:I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 83
    .local v1, height:I
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    add-int/lit8 v4, v2, 0x0

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4, v5, v1, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->measure(Landroid/view/View;IIII)V

    .line 87
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-static {v3, v9, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setCorner(Landroid/view/View;II)V

    .line 88
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->sSpacing:I

    add-int/2addr v3, v4

    add-int/lit8 v0, v3, 0x0

    .line 90
    .local v0, currentLeft:I
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    sub-int v4, v2, v0

    invoke-static {v9, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v4, v5, v1, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->measure(Landroid/view/View;IIII)V

    .line 92
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-static {v3, v0, v9}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setCorner(Landroid/view/View;II)V

    .line 93
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v0, v3

    .line 95
    new-array v3, v6, [Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    aput-object v4, v3, v7

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->verticallyCenter(I[Landroid/view/View;)V

    .line 97
    sub-int v3, v2, v0

    div-int/lit8 v5, v3, 0x2

    new-array v6, v6, [Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mText:Landroid/widget/TextView;

    aput-object v3, v6, v9

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->mImage:Landroid/widget/ImageView;

    aput-object v3, v6, v7

    array-length v3, v6

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    move v4, v3

    :goto_62
    if-ltz v4, :cond_7f

    aget-object v7, v6, v4

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    if-eqz v3, :cond_7b

    iget v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    add-int/2addr v8, v5

    iput v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    iget v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    add-int/2addr v8, v9

    iput v8, v3, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    invoke-virtual {v7, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_7b
    add-int/lit8 v3, v4, -0x1

    move v4, v3

    goto :goto_62

    .line 100
    :cond_7f
    invoke-static {v2, p1}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->resolveSize(II)I

    move-result v3

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->resolveSize(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setMeasuredDimension(II)V

    .line 102
    return-void
.end method
