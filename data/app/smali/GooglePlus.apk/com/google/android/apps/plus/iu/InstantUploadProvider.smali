.class public Lcom/google/android/apps/plus/iu/InstantUploadProvider;
.super Landroid/content/ContentProvider;
.source "InstantUploadProvider.java"


# static fields
.field private static final PHOTO_TABLE_NAME:Ljava/lang/String;

.field private static final PROJECTION_PHOTO_ID:[Ljava/lang/String;

.field private static final SETTINGS_LOCK:Ljava/lang/Object;

.field private static final SETTING_DEFAULTS:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final SETTING_DEPRECATED:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final UPLOAD_RECORD_TABLE:Ljava/lang/String;

.field private static final UPLOAD_TASK_TABLE:Ljava/lang/String;


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 113
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    .line 114
    sget-object v0, Lcom/google/android/apps/plus/iu/UploadedEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_RECORD_TABLE:Ljava/lang/String;

    .line 116
    sget-object v0, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    .line 117
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PROJECTION_PHOTO_ID:[Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    .line 210
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    .line 211
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    .line 213
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "auto_upload_account_name"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "auto_upload_account_type"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "auto_upload_enabled"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 217
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "video_upload_wifi_only"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_roaming"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "sync_on_battery"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_share_eventid"

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    const-string v1, "instant_share_endtime"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 224
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    const-string v1, "sync_photo_on_mobile"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 85
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 124
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    return-void
.end method

.method private cancelUploads(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 15
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 387
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    sget-object v3, Lcom/android/gallery3d/common/Entry;->ID_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 390
    .local v8, cursor:Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 391
    .local v7, count:I
    :goto_e
    :try_start_e
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 392
    const-string v0, "_id"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 393
    .local v9, id:J
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelTask(J)V
    :try_end_29
    .catchall {:try_start_e .. :try_end_29} :catchall_30

    .line 394
    add-int/lit8 v7, v7, 0x1

    .line 395
    goto :goto_e

    .line 398
    .end local v9           #id:J
    :cond_2c
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return v7

    :catchall_30
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static disableInstantShare(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    .prologue
    .line 427
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 430
    .local v0, resolver:Landroid/content/ContentResolver;
    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 431
    :try_start_7
    const-string v1, "com.google.android.picasasync.instant_share_eventid"

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 434
    monitor-exit v2
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_f

    return-void

    :catchall_f
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private queryPhotos(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 32
    .parameter "uri"
    .parameter "localContentUris"

    .prologue
    .line 289
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v20

    .line 290
    .local v20, helper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    const-string v4, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 291
    .local v11, account:Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;

    move-result-object v28

    .line 292
    .local v28, userEntry:Lcom/google/android/apps/plus/iu/UserEntry;
    new-instance v26, Landroid/content/SyncResult;

    invoke-direct/range {v26 .. v26}, Landroid/content/SyncResult;-><init>()V

    .line 293
    .local v26, result:Landroid/content/SyncResult;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->createSyncContext(Landroid/content/SyncResult;Ljava/lang/Thread;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;

    move-result-object v27

    .line 295
    .local v27, syncContext:Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;
    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->setAccount(Ljava/lang/String;)Z

    .line 296
    move-object/from16 v0, v20

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->syncUploadedPhotos(Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;Lcom/google/android/apps/plus/iu/UserEntry;)V

    .line 298
    new-instance v24, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "uri"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "photoid"

    aput-object v6, v4, v5

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 300
    .local v24, matrixCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    const/4 v4, 0x1

    new-array v0, v4, [J

    move-object/from16 v16, v0

    .line 301
    .local v16, dataLength:[J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    .line 302
    .local v14, context:Landroid/content/Context;
    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    .line 303
    .local v25, resolver:Landroid/content/ContentResolver;
    invoke-static {v14}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/PicasaDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 305
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    move-object/from16 v12, p2

    .local v12, arr$:[Ljava/lang/String;
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v22, v0

    .local v22, len$:I
    const/16 v21, 0x0

    .local v21, i$:I
    :goto_67
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_eb

    aget-object v23, v12, v21

    .line 306
    .local v23, localContentUri:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v13

    .line 307
    .local v13, contentUri:Landroid/net/Uri;
    const/4 v15, 0x0

    .line 310
    .local v15, cursor:Landroid/database/Cursor;
    :try_start_74
    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-static {v4, v0}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v18

    .line 312
    .local v18, fingerprint:Lcom/android/gallery3d/common/Fingerprint;
    invoke-virtual/range {v18 .. v18}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v19

    .line 316
    .local v19, hash:I
    sget-object v4, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PHOTO_TABLE_NAME:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->PROJECTION_PHOTO_ID:[Ljava/lang/String;

    const-string v6, "fingerprint_hash=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 320
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_b8

    .line 321
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v23, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-interface {v15, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_b8
    .catchall {:try_start_74 .. :try_end_b8} :catchall_e4
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_b8} :catch_c0

    .line 328
    :cond_b8
    if-eqz v15, :cond_bd

    .line 329
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 305
    .end local v18           #fingerprint:Lcom/android/gallery3d/common/Fingerprint;
    .end local v19           #hash:I
    :cond_bd
    :goto_bd
    add-int/lit8 v21, v21, 0x1

    goto :goto_67

    .line 323
    :catch_c0
    move-exception v17

    .line 324
    .local v17, e:Ljava/lang/Exception;
    :try_start_c1
    const-string v4, "iu.InstantUploadProvider"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_de

    .line 325
    const-string v4, "iu.InstantUploadProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "problem retrieving photo id for uri:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_de
    .catchall {:try_start_c1 .. :try_end_de} :catchall_e4

    .line 328
    :cond_de
    if-eqz v15, :cond_bd

    .line 329
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    goto :goto_bd

    .line 328
    .end local v17           #e:Ljava/lang/Exception;
    :catchall_e4
    move-exception v4

    if-eqz v15, :cond_ea

    .line 329
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    :cond_ea
    throw v4

    .line 334
    .end local v13           #contentUri:Landroid/net/Uri;
    .end local v15           #cursor:Landroid/database/Cursor;
    .end local v23           #localContentUri:Ljava/lang/String;
    :cond_eb
    return-object v24
.end method

.method private querySettings$7be7850c([Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "projection"

    .prologue
    .line 233
    sget-object v7, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v7

    .line 234
    :try_start_3
    new-instance v2, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 235
    .local v2, cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    array-length v6, p1

    new-array v1, v6, [Ljava/lang/Object;

    .line 236
    .local v1, columnValues:[Ljava/lang/Object;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 237
    .local v5, resolver:Landroid/content/ContentResolver;
    const/4 v3, 0x0

    .local v3, i:I
    array-length v4, p1

    .local v4, n:I
    :goto_15
    if-ge v3, v4, :cond_6e

    .line 238
    aget-object v0, p1, v3

    .line 239
    .local v0, column:Ljava/lang/String;
    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4c

    .line 240
    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_34

    .line 241
    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v1, v3

    .line 237
    :cond_31
    :goto_31
    add-int/lit8 v3, v3, 0x1

    goto :goto_15

    .line 244
    :cond_34
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unknown column: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_49
    .catchall {:try_start_3 .. :try_end_49} :catchall_49

    .line 254
    .end local v0           #column:Ljava/lang/String;
    .end local v1           #columnValues:[Ljava/lang/Object;
    .end local v2           #cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .end local v3           #i:I
    .end local v4           #n:I
    .end local v5           #resolver:Landroid/content/ContentResolver;
    :catchall_49
    move-exception v6

    monitor-exit v7

    throw v6

    .line 247
    .restart local v0       #column:Ljava/lang/String;
    .restart local v1       #columnValues:[Ljava/lang/Object;
    .restart local v2       #cursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .restart local v3       #i:I
    .restart local v4       #n:I
    .restart local v5       #resolver:Landroid/content/ContentResolver;
    :cond_4c
    :try_start_4c
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "com.google.android.picasasync."

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v1, v3

    .line 248
    aget-object v6, v1, v3

    if-nez v6, :cond_31

    .line 249
    sget-object v6, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v1, v3

    goto :goto_31

    .line 252
    .end local v0           #column:Ljava/lang/String;
    :cond_6e
    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 253
    monitor-exit v7
    :try_end_72
    .catchall {:try_start_4c .. :try_end_72} :catchall_49

    return-object v2
.end method

.method private querySingleUpload(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 12
    .parameter "tableName"
    .parameter "uri"
    .parameter "projection"

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 266
    new-array v4, v2, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v1

    .line 267
    .local v4, whereArgs:[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getUploadsDatabaseHelper()Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v3, "_id=?"

    move-object v1, p1

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private queryUploads(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 16
    .parameter "tableName"
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    const/4 v5, 0x0

    .line 259
    const-string v0, "limit"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 260
    .local v8, limit:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getUploadsDatabaseHelper()Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, v5

    move-object v7, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private updateSettings(Landroid/content/ContentValues;)Z
    .registers 15
    .parameter "values"

    .prologue
    const/4 v10, 0x0

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 439
    .local v1, context:Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 440
    .local v6, resolver:Landroid/content/ContentResolver;
    const/4 v0, 0x0

    .line 443
    .local v0, changed:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v8

    .line 446
    .local v8, uploadsManager:Lcom/google/android/apps/plus/iu/UploadsManager;
    sget-object v11, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTINGS_LOCK:Ljava/lang/Object;

    monitor-enter v11

    .line 447
    :try_start_11
    invoke-virtual {p1}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :cond_19
    :goto_19
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 448
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v9, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5b

    .line 449
    sget-object v9, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEPRECATED:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_19

    .line 450
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v9, "unknown setting: "

    invoke-direct {v12, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v10, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_58
    .catchall {:try_start_11 .. :try_end_58} :catchall_58

    .line 470
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v3           #i$:Ljava/util/Iterator;
    :catchall_58
    move-exception v9

    monitor-exit v11

    throw v9

    .line 455
    .restart local v2       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v3       #i$:Ljava/util/Iterator;
    :cond_5b
    :try_start_5b
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v9, "com.google.android.picasasync."

    invoke-direct {v12, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 456
    .local v7, systemKey:Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_86

    move-object v4, v10

    .line 457
    .local v4, newValue:Ljava/lang/String;
    :goto_77
    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 458
    .local v5, oldValue:Ljava/lang/String;
    invoke-static {v5, v4}, Lcom/android/gallery3d/common/Utils;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_19

    .line 459
    invoke-static {v6, v7, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 460
    const/4 v0, 0x1

    goto :goto_19

    .line 456
    .end local v4           #newValue:Ljava/lang/String;
    .end local v5           #oldValue:Ljava/lang/String;
    :cond_86
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_77

    .line 467
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v7           #systemKey:Ljava/lang/String;
    :cond_8f
    if-eqz v0, :cond_94

    .line 468
    invoke-virtual {v8}, Lcom/google/android/apps/plus/iu/UploadsManager;->reloadSystemSettings()V

    .line 470
    :cond_94
    monitor-exit v11
    :try_end_95
    .catchall {:try_start_5b .. :try_end_95} :catchall_58

    .line 471
    if-eqz v0, :cond_a5

    .line 472
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v9

    const-wide/16 v11, 0x0

    invoke-virtual {v9, v11, v12}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V

    .line 473
    sget-object v9, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->settingsUri:Landroid/net/Uri;

    invoke-virtual {v6, v9, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 475
    :cond_a5
    return v0
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .registers 7
    .parameter "context"
    .parameter "info"

    .prologue
    .line 133
    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    .line 134
    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "uploads"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "upload_records"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "upload_all"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "iu"

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "uploads/#"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "settings"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "upload_records/#"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "photos"

    const/16 v3, 0x12

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 10
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_9a

    .line 382
    :pswitch_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported uri:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 373
    :pswitch_20
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->cancelUploads(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 380
    :goto_24
    return v0

    .line 375
    :pswitch_25
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    sget-object v1, Lcom/android/gallery3d/common/Entry;->ID_PROJECTION:[Ljava/lang/String;

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->querySingleUpload(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_35

    move v0, v2

    goto :goto_24

    :cond_35
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelTask(J)V

    move v0, v3

    goto :goto_24

    .line 377
    :pswitch_50
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_63

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->cancelUploadExistingPhotos(Ljava/lang/String;)V

    :cond_63
    move v0, v2

    .line 378
    goto :goto_24

    .line 380
    :pswitch_65
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->SETTING_DEFAULTS:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_74
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_74

    :cond_90
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateSettings(Landroid/content/ContentValues;)Z

    move-result v0

    if-eqz v0, :cond_98

    move v0, v3

    goto :goto_24

    :cond_98
    move v0, v2

    goto :goto_24

    .line 371
    :pswitch_data_9a
    .packed-switch 0x5
        :pswitch_20
        :pswitch_25
        :pswitch_b
        :pswitch_b
        :pswitch_50
        :pswitch_b
        :pswitch_65
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 5
    .parameter "uri"

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_34

    .line 164
    :pswitch_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :pswitch_1e
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.upload"

    .line 162
    :goto_20
    return-object v0

    .line 152
    :pswitch_21
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.upload_record"

    goto :goto_20

    .line 154
    :pswitch_24
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.upload_all"

    goto :goto_20

    .line 156
    :pswitch_27
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.apps.plus.iu.iu"

    goto :goto_20

    .line 158
    :pswitch_2a
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.iu.upload"

    goto :goto_20

    .line 160
    :pswitch_2d
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.iu.upload_record"

    goto :goto_20

    .line 162
    :pswitch_30
    const-string v0, "vnd.android.cursor.item/vnd.google.android.apps.plus.iu.photos_content_uri"

    goto :goto_20

    .line 148
    nop

    :pswitch_data_34
    .packed-switch 0x5
        :pswitch_1e
        :pswitch_2a
        :pswitch_21
        :pswitch_2d
        :pswitch_24
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_27
        :pswitch_30
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 9
    .parameter "uri"
    .parameter "values"

    .prologue
    .line 339
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "INSERT "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v2

    .line 341
    .local v2, statsId:I
    :try_start_13
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    sparse-switch v3, :sswitch_data_66

    .line 350
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unsupported uri:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_31
    .catchall {:try_start_13 .. :try_end_31} :catchall_31

    .line 353
    :catchall_31
    move-exception v3

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v3

    .line 343
    :sswitch_36
    :try_start_36
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v3

    invoke-static {p2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->addManualUpload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)J

    move-result-wide v0

    .line 345
    .local v0, id:J
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->getUploadUri(J)Landroid/net/Uri;
    :try_end_49
    .catchall {:try_start_36 .. :try_end_49} :catchall_31

    move-result-object v3

    .line 353
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    .end local v0           #id:J
    :goto_4d
    return-object v3

    .line 347
    :sswitch_4e
    :try_start_4e
    const-string v3, "account"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->uploadExistingPhotos(Ljava/lang/String;)V

    .line 348
    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->uploadAllUri:Landroid/net/Uri;
    :try_end_61
    .catchall {:try_start_4e .. :try_end_61} :catchall_31

    .line 353
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    goto :goto_4d

    .line 341
    nop

    :sswitch_data_66
    .sparse-switch
        0x5 -> :sswitch_36
        0x9 -> :sswitch_4e
    .end sparse-switch
.end method

.method public onCreate()Z
    .registers 2

    .prologue
    .line 128
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 15
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "QUERY "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v8

    .line 173
    .local v8, statsId:I
    :try_start_17
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_a0

    :pswitch_20
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_35
    .catchall {:try_start_17 .. :try_end_35} :catchall_35

    .line 177
    :catchall_35
    move-exception v0

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v0

    .line 173
    :pswitch_3a
    :try_start_3a
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 174
    .local v7, cursor:Landroid/database/Cursor;
    :goto_46
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementQueryResultCount(I)V
    :try_end_4d
    .catchall {:try_start_3a .. :try_end_4d} :catchall_35

    .line 177
    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    return-object v7

    .line 173
    .end local v7           #cursor:Landroid/database/Cursor;
    :pswitch_51
    :try_start_51
    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_RECORD_TABLE:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryUploads(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :pswitch_5e
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_TASK_TABLE:Ljava/lang/String;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->querySingleUpload(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :pswitch_65
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->UPLOAD_RECORD_TABLE:Ljava/lang/String;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->querySingleUpload(Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :pswitch_6c
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->querySettings$7be7850c([Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :pswitch_71
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getUploadAllStatus(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :pswitch_84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstantUploadStatus()Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :pswitch_91
    if-eqz p2, :cond_98

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->queryPhotos(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    goto :goto_46

    :cond_98
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "projection must include list of local uris"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a0
    .catchall {:try_start_51 .. :try_end_a0} :catchall_35

    :pswitch_data_a0
    .packed-switch 0x5
        :pswitch_3a
        :pswitch_5e
        :pswitch_51
        :pswitch_65
        :pswitch_71
        :pswitch_20
        :pswitch_6c
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_84
        :pswitch_91
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 8
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_28

    .line 418
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported uri:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :pswitch_1e
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateSettings(Landroid/content/ContentValues;)Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x0

    goto :goto_25

    .line 413
    :pswitch_data_28
    .packed-switch 0xb
        :pswitch_1e
    .end packed-switch
.end method
