.class public final Lcom/google/android/apps/plus/content/EsPhotosDataApiary;
.super Lcom/google/android/apps/plus/content/EsPhotosData;
.source "EsPhotosDataApiary.java"


# static fields
.field private static final PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

.field private static sPhotosFromPostsAlbumName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 240
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    return-void
.end method

.method public static deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "commentId"

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x4

    .line 1023
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1025
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1026
    .local v6, startTime:J
    const/4 v8, 0x1

    new-array v0, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v0, v8

    .line 1028
    .local v0, commentArgs:[Ljava/lang/String;
    :try_start_14
    const-string v8, "SELECT photo_id FROM photo_comment WHERE comment_id = ?"

    invoke-static {v1, v8, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_19
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_14 .. :try_end_19} :catch_6a

    move-result-wide v3

    .line 1040
    .local v3, photoId:J
    :try_start_1a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1043
    const-string v8, "photo_comment"

    const-string v9, "comment_id = ?"

    invoke-virtual {v1, v8, v9, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 1046
    .local v5, removeCount:I
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    neg-int v9, v5

    invoke-static {v1, v8, v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateCommentCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 1048
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_30
    .catchall {:try_start_1a .. :try_end_30} :catchall_89

    .line 1050
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1051
    const-string v8, "EsPhotosData"

    invoke-static {v8, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_53

    .line 1052
    const-string v8, "EsPhotosData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "[DELETE_PHOTO_COMMENT], duration: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    :cond_53
    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v8, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 1060
    .local v2, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    invoke-virtual {v8, v2, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1061
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v8, v9, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1062
    .end local v2           #notifyUri:Landroid/net/Uri;
    .end local v3           #photoId:J
    .end local v5           #removeCount:I
    :cond_69
    :goto_69
    return-void

    .line 1033
    :catch_6a
    move-exception v8

    const-string v8, "EsPhotosData"

    const/4 v9, 0x5

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_69

    .line 1034
    const-string v8, "EsPhotosData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "WARNING: could not find photo for the comment: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_69

    .line 1050
    .restart local v3       #photoId:J
    :catchall_89
    move-exception v8

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1051
    const-string v9, "EsPhotosData"

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_ad

    .line 1052
    const-string v9, "EsPhotosData"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "[DELETE_PHOTO_COMMENT], duration: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_ad
    throw v8
.end method

.method private static deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .registers 6
    .parameter "db"
    .parameter "photoId"

    .prologue
    .line 1935
    const-string v0, "photo_plusone"

    const-string v1, "photo_id=?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1937
    return-void
.end method

.method public static deletePhotos$43585934(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 26
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 607
    .local p2, photoList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    if-eqz p2, :cond_8

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    if-nez v19, :cond_9

    .line 688
    :cond_8
    :goto_8
    return-void

    .line 612
    :cond_9
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    .line 614
    .local v6, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    .line 617
    .local v17, startTime:J
    :try_start_15
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 620
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 621
    .local v3, albumDelta:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 622
    .local v16, sb:Ljava/lang/StringBuilder;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v15, v0, [Ljava/lang/String;

    .line 623
    .local v15, removedIds:[Ljava/lang/String;
    const-string v19, "photo_id IN("

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    add-int/lit8 v9, v19, -0x1

    .local v9, i:I
    :goto_39
    if-ltz v9, :cond_f1

    .line 625
    move-object/from16 v0, p2

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v19

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    .line 627
    .local v14, photoId:Ljava/lang/String;
    const-string v19, "EsPhotosData"

    const/16 v20, 0x3

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_6b

    .line 628
    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, ">> deletePhoto photo id: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    :cond_6b
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v11, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    aput-object v14, v11, v19
    :try_end_75
    .catchall {:try_start_15 .. :try_end_75} :catchall_ca

    .line 634
    .local v11, photoArgs:[Ljava/lang/String;
    :try_start_75
    const-string v19, "SELECT album_id FROM photo WHERE photo_id = ?"

    move-object/from16 v0, v19

    invoke-static {v6, v0, v11}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 635
    .local v4, albumId:Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    .line 636
    .local v12, photoCount:Ljava/lang/Integer;
    if-nez v12, :cond_8b

    .line 637
    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 639
    :cond_8b
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_9a
    .catchall {:try_start_75 .. :try_end_9a} :catchall_ca
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_75 .. :try_end_9a} :catch_a8

    .line 648
    .end local v4           #albumId:Ljava/lang/String;
    .end local v12           #photoCount:Ljava/lang/Integer;
    :cond_9a
    :goto_9a
    :try_start_9a
    const-string v19, "?,"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    aput-object v14, v15, v9

    .line 624
    add-int/lit8 v9, v9, -0x1

    goto :goto_39

    .line 640
    :catch_a8
    move-exception v8

    .line 642
    .local v8, e:Landroid/database/sqlite/SQLiteDoneException;
    const-string v19, "EsPhotosData"

    const/16 v20, 0x5

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_9a

    .line 643
    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Album not found for photo: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c9
    .catchall {:try_start_9a .. :try_end_c9} :catchall_ca

    goto :goto_9a

    .line 679
    .end local v3           #albumDelta:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v8           #e:Landroid/database/sqlite/SQLiteDoneException;
    .end local v9           #i:I
    .end local v11           #photoArgs:[Ljava/lang/String;
    .end local v14           #photoId:Ljava/lang/String;
    .end local v15           #removedIds:[Ljava/lang/String;
    .end local v16           #sb:Ljava/lang/StringBuilder;
    :catchall_ca
    move-exception v19

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 680
    const-string v20, "EsPhotosData"

    const/16 v21, 0x4

    invoke-static/range {v20 .. v21}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v20

    if-eqz v20, :cond_f0

    .line 681
    const-string v20, "EsPhotosData"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "[DELETE_PHOTOS], duration: "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f0
    throw v19

    .line 651
    .restart local v3       #albumDelta:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v9       #i:I
    .restart local v15       #removedIds:[Ljava/lang/String;
    .restart local v16       #sb:Ljava/lang/StringBuilder;
    :cond_f1
    :try_start_f1
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 652
    const-string v19, ")"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 656
    .local v5, albumValues:Landroid/content/ContentValues;
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, i$:Ljava/util/Iterator;
    :cond_114
    :goto_114
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_184

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 657
    .restart local v4       #albumId:Ljava/lang/String;
    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v2, v0, [Ljava/lang/String;

    const/16 v19, 0x0

    aput-object v4, v2, v19

    .line 658
    .local v2, albumArgs:[Ljava/lang/String;
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I
    :try_end_133
    .catchall {:try_start_f1 .. :try_end_133} :catchall_ca

    move-result v7

    .line 661
    .local v7, delta:I
    :try_start_134
    const-string v19, "SELECT photo_count FROM album WHERE photo_count NOT NULL AND album_id = ?"

    move-object/from16 v0, v19

    invoke-static {v6, v0, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v19

    int-to-long v0, v7

    move-wide/from16 v21, v0

    add-long v12, v19, v21

    .line 664
    .local v12, photoCount:J
    const-string v19, "photo_count"

    const-wide/16 v20, 0x0

    move-wide/from16 v0, v20

    invoke-static {v0, v1, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 665
    const-string v19, "album"

    const-string v20, "album_id = ?"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v5, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_161
    .catchall {:try_start_134 .. :try_end_161} :catchall_ca
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_134 .. :try_end_161} :catch_162

    goto :goto_114

    .line 668
    .end local v12           #photoCount:J
    :catch_162
    move-exception v19

    :try_start_163
    const-string v19, "EsPhotosData"

    const/16 v20, 0x4

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_114

    .line 669
    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "Photo count not found; album id: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_114

    .line 675
    .end local v2           #albumArgs:[Ljava/lang/String;
    .end local v4           #albumId:Ljava/lang/String;
    .end local v7           #delta:I
    :cond_184
    const-string v19, "photo"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1, v15}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 677
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_194
    .catchall {:try_start_163 .. :try_end_194} :catchall_ca

    .line 679
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 680
    const-string v19, "EsPhotosData"

    const/16 v20, 0x4

    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v19

    if-eqz v19, :cond_1b9

    .line 681
    const-string v19, "EsPhotosData"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "[DELETE_PHOTOS], duration: "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :cond_1b9
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    sget-object v20, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_8
.end method

.method private static deletePhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;)V
    .registers 9
    .parameter "db"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1796
    .local p1, photoMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    if-lez v4, :cond_5e

    .line 1797
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1798
    .local v3, sb:Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1800
    .local v2, removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v4, "photo_id IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1802
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 1803
    .local v1, photoId:Ljava/lang/Long;
    const-string v4, "?,"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1804
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 1806
    .end local v1           #photoId:Ljava/lang/Long;
    :cond_3e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1807
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1809
    const-string v5, "photo"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {p0, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1812
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v2           #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3           #sb:Ljava/lang/StringBuilder;
    :cond_5e
    return-void
.end method

.method private static getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;
    .registers 3
    .parameter "album"

    .prologue
    .line 2004
    :try_start_0
    iget-object v0, p0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 2005
    iget-object v0, p0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_7} :catch_8

    .line 2009
    :goto_7
    return-object v0

    :catch_8
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_7
.end method

.method private static getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;
    .registers 8
    .parameter "album"
    .parameter "indent"

    .prologue
    .line 2287
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2288
    .local v2, sb:Ljava/lang/StringBuilder;
    if-lez p1, :cond_12

    .line 2289
    const/4 v0, 0x0

    .local v0, i:I
    :goto_8
    if-ge v0, p1, :cond_12

    .line 2290
    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2289
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 2293
    .end local v0           #i:I
    :cond_12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2294
    .local v1, indentString:Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2296
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ALBUM [id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", owner: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2307
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    if-eqz v3, :cond_5e

    .line 2308
    const-string v3, ",\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "       type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2315
    :cond_5e
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    if-eqz v3, :cond_77

    .line 2316
    const-string v3, ",\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "       title: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2323
    :cond_77
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v3, :cond_8c

    .line 2324
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    add-int/lit8 v5, p1, 0x2

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCoverPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2327
    :cond_8c
    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2329
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getCommentContentValues(Lcom/google/api/services/plusi/model/DataComment;Ljava/lang/String;)Landroid/content/ContentValues;
    .registers 9
    .parameter "comment"
    .parameter "photoId"

    .prologue
    .line 1397
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1398
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "photo_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    const-string v3, "comment_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1400
    const-string v3, "author_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataComment;->user:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1401
    const-string v3, "content"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataComment;->text:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->timestamp:Ljava/lang/String;

    if-eqz v3, :cond_3b

    .line 1403
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->timestamp:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3

    const-wide v5, 0x408f400000000000L

    mul-double/2addr v3, v5

    double-to-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1404
    .local v0, createTime:Ljava/lang/Long;
    const-string v3, "create_time"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1406
    .end local v0           #createTime:Ljava/lang/Long;
    :cond_3b
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->lastUpdateTimestamp:Ljava/lang/Long;

    if-eqz v3, :cond_46

    .line 1407
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataComment;->lastUpdateTimestamp:Ljava/lang/Long;

    .line 1408
    .local v1, updateTime:Ljava/lang/Long;
    const-string v3, "update_time"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1410
    .end local v1           #updateTime:Ljava/lang/Long;
    :cond_46
    iget-object v3, p0, Lcom/google/api/services/plusi/model/DataComment;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v3, :cond_57

    .line 1411
    const-string v3, "plusone_data"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataComment;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1413
    :cond_57
    return-object v2
.end method

.method private static getCoverPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;
    .registers 15
    .parameter "photo"
    .parameter "indent"

    .prologue
    const-wide/16 v7, 0x0

    .line 2334
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2335
    .local v4, sb:Ljava/lang/StringBuilder;
    if-lez p1, :cond_14

    .line 2336
    const/4 v1, 0x0

    .local v1, i:I
    :goto_a
    if-ge v1, p1, :cond_14

    .line 2337
    const/16 v9, 0x20

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2336
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 2340
    .end local v1           #i:I
    :cond_14
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2341
    .local v3, indentString:Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2343
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    if-nez v9, :cond_66

    move-wide v5, v7

    .line 2345
    .local v5, timestamp:J
    :goto_21
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "COVER PHOTO [id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", owner: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2351
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-nez v9, :cond_74

    const-string v9, "N/A"

    :goto_3c
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2352
    cmp-long v7, v5, v7

    if-eqz v7, :cond_57

    .line 2353
    const-string v2, "MMM dd, yyyy h:mmaa"

    .line 2354
    .local v2, inFormat:Ljava/lang/CharSequence;
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7, v5, v6}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2355
    .local v0, dateString:Ljava/lang/CharSequence;
    const-string v7, ", date: "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2359
    .end local v0           #dateString:Ljava/lang/CharSequence;
    .end local v2           #inFormat:Ljava/lang/CharSequence;
    :cond_57
    const-string v7, "]"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2360
    const-string v7, "\n"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2362
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 2343
    .end local v5           #timestamp:J
    :cond_66
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v9}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v9

    const-wide v11, 0x408f400000000000L

    mul-double/2addr v9, v11

    double-to-long v5, v9

    goto :goto_21

    .line 2351
    .restart local v5       #timestamp:J
    :cond_74
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    goto :goto_3c
.end method

.method private static getFingerPrint(Lcom/google/api/services/plusi/model/DataPhoto;)[B
    .registers 4
    .parameter "photo"

    .prologue
    .line 2038
    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    if-eqz v2, :cond_29

    .line 2039
    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2040
    .local v1, stream:Ljava/lang/String;
    const-string v2, "cs_01_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2041
    sget v2, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->FINGERPRINT_STREAM_PREFIX_LENGTH:I

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->hexToBytes(Ljava/lang/CharSequence;)[B

    move-result-object v2

    .line 2045
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #stream:Ljava/lang/String;
    :goto_28
    return-object v2

    :cond_29
    const/4 v2, 0x0

    goto :goto_28
.end method

.method private static getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;
    .registers 17
    .parameter "photo"
    .parameter "indent"

    .prologue
    .line 2125
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2126
    .local v5, sb:Ljava/lang/StringBuilder;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2132
    .local v3, indentString:Ljava/lang/String;
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2134
    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 2135
    .local v4, plusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;
    iget-object v10, p0, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    .line 2136
    .local v10, videoData:Lcom/google/api/services/plusi/model/DataVideo;
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    if-nez v11, :cond_108

    const-wide/16 v11, 0x0

    :goto_17
    double-to-long v11, v11

    const-wide/16 v13, 0x3e8

    mul-long v8, v11, v13

    .line 2138
    .local v8, timestamp:J
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "PHOTO [id: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", owner: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-nez v11, :cond_110

    const-string v11, "N/A"

    :goto_38
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", version: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2148
    const-wide/16 v11, 0x0

    cmp-long v11, v8, v11

    if-eqz v11, :cond_61

    .line 2149
    const-string v2, "MMM dd, yyyy h:mmaa"

    .line 2150
    .local v2, inFormat:Ljava/lang/CharSequence;
    new-instance v11, Ljava/util/Date;

    invoke-direct {v11, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2151
    .local v0, dateString:Ljava/lang/CharSequence;
    const-string v11, ", date: "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 2155
    .end local v0           #dateString:Ljava/lang/CharSequence;
    .end local v2           #inFormat:Ljava/lang/CharSequence;
    :cond_61
    const-string v11, ", \n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2157
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "      title: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->title:Ljava/lang/String;

    if-nez v11, :cond_116

    const-string v11, "N/A"

    :goto_76
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "video? "

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v10, :cond_11a

    const/4 v11, 0x1

    :goto_8a
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", comments: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    if-nez v11, :cond_11d

    const/4 v11, 0x0

    :goto_a1
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "+1s: "

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_125

    iget-object v11, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    :goto_b6
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v11, "by me: "

    invoke-direct {v13, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_128

    iget-object v11, v4, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    :goto_d3
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2171
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    if-eqz v11, :cond_12b

    .line 2172
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->streamId:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_e8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_12b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2173
    .local v7, stream:Ljava/lang/String;
    const-string v11, ", \n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "      stream: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_e8

    .line 2136
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v7           #stream:Ljava/lang/String;
    .end local v8           #timestamp:J
    :cond_108
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v11

    goto/16 :goto_17

    .line 2138
    .restart local v8       #timestamp:J
    :cond_110
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    goto/16 :goto_38

    .line 2157
    :cond_116
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->title:Ljava/lang/String;

    goto/16 :goto_76

    :cond_11a
    const/4 v11, 0x0

    goto/16 :goto_8a

    :cond_11d
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    goto/16 :goto_a1

    :cond_125
    const-string v11, "0"

    goto :goto_b6

    :cond_128
    const-string v11, "false"

    goto :goto_d3

    .line 2181
    :cond_12b
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    if-eqz v11, :cond_13f

    .line 2182
    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    const/4 v13, 0x2

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2185
    :cond_13f
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    if-eqz v11, :cond_164

    .line 2186
    iget-object v11, p0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_149
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_164

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/api/services/plusi/model/DataShape;

    .line 2187
    .local v6, shape:Lcom/google/api/services/plusi/model/DataShape;
    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x2

    invoke-static {v6, v12}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getShapeOutput(Lcom/google/api/services/plusi/model/DataShape;I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_149

    .line 2191
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v6           #shape:Lcom/google/api/services/plusi/model/DataShape;
    :cond_164
    const-string v11, "]"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2192
    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2193
    const-string v11, "\n"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2195
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11
.end method

.method private static getShapeOutput(Lcom/google/api/services/plusi/model/DataShape;I)Ljava/lang/String;
    .registers 13
    .parameter "shape"
    .parameter "indent"

    .prologue
    const/4 v10, 0x0

    .line 2200
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2201
    .local v4, sb:Ljava/lang/StringBuilder;
    if-lez p1, :cond_13

    .line 2202
    const/4 v2, 0x0

    .local v2, i:I
    :goto_9
    if-ge v2, p1, :cond_13

    .line 2203
    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2202
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 2206
    .end local v2           #i:I
    :cond_13
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2207
    .local v3, indentString:Ljava/lang/String;
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2209
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataShape;->bounds:Lcom/google/api/services/plusi/model/DataRect32;

    .line 2210
    .local v1, bounds:Lcom/google/api/services/plusi/model/DataRect32;
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "SHAPE [("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%d, %d, %d, %d"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v1, Lcom/google/api/services/plusi/model/DataRect32;->upperLeft:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataPoint32;->x:Ljava/lang/Long;

    aput-object v8, v7, v10

    const/4 v8, 0x1

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataRect32;->upperLeft:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPoint32;->y:Ljava/lang/Long;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataRect32;->lowerRight:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPoint32;->x:Ljava/lang/Long;

    aput-object v9, v7, v8

    const/4 v8, 0x3

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataRect32;->lowerRight:Lcom/google/api/services/plusi/model/DataPoint32;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataPoint32;->y:Ljava/lang/Long;

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "), "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "subjectId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    if-nez v5, :cond_10b

    const-string v5, "N/A"

    :goto_60
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", status: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/api/services/plusi/model/DataShape;->status:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2223
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iget-object v7, p0, Lcom/google/api/services/plusi/model/DataShape;->viewerCanEdit:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/google/api/services/plusi/model/DataShape;->viewerCanApprove:Ljava/lang/Boolean;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v5, :cond_99

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_99

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_111

    const-string v5, ""

    :goto_90
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "COMMENT"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_99
    if-eqz v6, :cond_b2

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_b2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_115

    const-string v5, ""

    :goto_a9
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "TAG"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b2
    if-eqz v7, :cond_cb

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_cb

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_118

    const-string v5, ""

    :goto_c2
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "EDIT"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_cb
    if-eqz v8, :cond_e4

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_e4

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-nez v5, :cond_11b

    const-string v5, ""

    :goto_db
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "APPROVE"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2225
    .local v0, actionState:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_101

    .line 2226
    const-string v5, ", \n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "       state: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2234
    :cond_101
    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2236
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 2210
    .end local v0           #actionState:Ljava/lang/String;
    :cond_10b
    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    goto/16 :goto_60

    .line 2223
    :cond_111
    const-string v5, "|"

    goto/16 :goto_90

    :cond_115
    const-string v5, "|"

    goto :goto_a9

    :cond_118
    const-string v5, "|"

    goto :goto_c2

    :cond_11b
    const-string v5, "|"

    goto :goto_db
.end method

.method private static insertAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;)V
    .registers 16
    .parameter "db"
    .parameter "album"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, notificationList:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1505
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/Long;

    move-result-object v0

    .line 1506
    .local v0, albumRowId:Ljava/lang/Long;
    if-nez v0, :cond_19

    .line 1508
    const-string v5, "EsPhotosData"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1509
    const-string v5, "EsPhotosData"

    const-string v6, "Could not insert album row"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1526
    :cond_18
    :goto_18
    return-void

    .line 1514
    :cond_19
    const-string v7, "UPDATES_ALBUMS"

    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1515
    .local v2, isUpdateAlbum:Z
    iget-object v7, p1, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_56

    move v1, v5

    .line 1516
    .local v1, isAggregateAlbum:Z
    :goto_2e
    if-eqz v2, :cond_32

    if-eqz v1, :cond_3e

    .line 1517
    :cond_32
    iget-object v7, p1, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    if-eqz v7, :cond_3e

    iget-object v10, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    if-nez v10, :cond_58

    .line 1520
    :cond_3e
    :goto_3e
    if-eqz p2, :cond_18

    .line 1521
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 1522
    .local v4, uriBuilder:Landroid/net/Uri$Builder;
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1524
    .local v3, notificationUri:Landroid/net/Uri;
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_18

    .end local v1           #isAggregateAlbum:Z
    .end local v3           #notificationUri:Landroid/net/Uri;
    .end local v4           #uriBuilder:Landroid/net/Uri$Builder;
    :cond_56
    move v1, v6

    .line 1515
    goto :goto_2e

    .line 1517
    .restart local v1       #isAggregateAlbum:Z
    :cond_58
    const-string v10, "album_cover"

    const-string v11, "album_key=?"

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v6

    invoke-virtual {p0, v10, v11, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "album_key"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "url"

    iget-object v8, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "width"

    iget-object v8, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "height"

    iget-object v8, v7, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "size"

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataPhoto;->fileSize:Ljava/lang/Long;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "album_cover"

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {p0, v6, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_3e
.end method

.method private static insertAlbumListInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 15
    .parameter "db"
    .parameter
    .parameter
    .parameter
    .parameter "syncState"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, albumList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataAlbum;>;"
    .local p2, albumMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .local p3, notificationList:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    const/4 v9, 0x3

    .line 1467
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 1468
    .local v1, albumCount:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_6
    if-ge v3, v1, :cond_5e

    .line 1469
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataAlbum;

    .line 1471
    .local v0, album:Lcom/google/api/services/plusi/model/DataAlbum;
    const-string v6, "EsPhotosData"

    invoke-static {v6, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 1472
    const-string v6, "EsPhotosData"

    const/4 v7, 0x0

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 1475
    :cond_20
    if-eqz p4, :cond_25

    .line 1476
    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    .line 1479
    :cond_25
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v2

    .line 1480
    .local v2, albumId:Ljava/lang/String;
    invoke-interface {p2, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 1481
    .local v5, oldEntity:Ljava/lang/Long;
    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    .line 1483
    .local v4, newEntity:Ljava/lang/Long;
    if-eqz v5, :cond_3b

    if-eqz v4, :cond_41

    invoke-virtual {v5, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_41

    .line 1484
    :cond_3b
    invoke-static {p0, v0, p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;)V

    .line 1468
    :cond_3e
    :goto_3e
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 1486
    :cond_41
    const-string v6, "EsPhotosData"

    invoke-static {v6, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3e

    .line 1487
    const-string v6, "EsPhotosData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Album not updated; id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3e

    .line 1491
    .end local v0           #album:Lcom/google/api/services/plusi/model/DataAlbum;
    .end local v2           #albumId:Ljava/lang/String;
    .end local v4           #newEntity:Ljava/lang/Long;
    .end local v5           #oldEntity:Ljava/lang/Long;
    :cond_5e
    return-void
.end method

.method public static insertAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;Ljava/lang/Boolean;)V
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "album"
    .parameter
    .parameter "isDownloadable"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .prologue
    .line 319
    .local p4, photos:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataPhoto;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 321
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    .line 323
    .local v15, startTime:J
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v10

    .line 324
    .local v10, albumId:Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 325
    .local v8, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-static {v1, v10, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCurrentAlbumMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v5

    .line 327
    .local v5, photoMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Long;>;"
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 328
    const/4 v2, 0x3

    const-string v3, "EsPhotosData"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumOutput(Lcom/google/api/services/plusi/model/DataAlbum;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_35
    :try_start_35
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 334
    if-eqz p2, :cond_3d

    .line 335
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    .line 338
    :cond_3d
    move-object/from16 v0, p3

    invoke-static {v1, v0, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;)V

    .line 341
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 342
    .local v7, albumValues:Landroid/content/ContentValues;
    const-string v2, "album_id"

    invoke-virtual {v7, v2, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v6, "photos_in_album"

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p3

    move-object/from16 v9, p2

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 349
    invoke-static {v1, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;)V

    .line 351
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5f
    .catchall {:try_start_35 .. :try_end_5f} :catchall_d0

    .line 353
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 354
    const-string v2, "EsPhotosData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9d

    .line 355
    const-string v3, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "[INSERT_ALBUM_PHOTOS], album ID: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", num photos: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p4, :cond_ce

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    :goto_84
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", duration: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_9d
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    .line 364
    .local v14, resolver:Landroid/content/ContentResolver;
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v17

    .line 365
    .local v17, uriBuilder:Landroid/net/Uri$Builder;
    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v12

    .line 367
    .local v12, notificationUri:Landroid/net/Uri;
    const/4 v2, 0x0

    invoke-virtual {v14, v12, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 368
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :goto_bd
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_113

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/net/Uri;

    .line 369
    .local v13, notifyUri:Landroid/net/Uri;
    const/4 v2, 0x0

    invoke-virtual {v14, v13, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_bd

    .line 355
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #notificationUri:Landroid/net/Uri;
    .end local v13           #notifyUri:Landroid/net/Uri;
    .end local v14           #resolver:Landroid/content/ContentResolver;
    .end local v17           #uriBuilder:Landroid/net/Uri$Builder;
    :cond_ce
    const/4 v2, 0x0

    goto :goto_84

    .line 353
    .end local v7           #albumValues:Landroid/content/ContentValues;
    :catchall_d0
    move-exception v2

    move-object v3, v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 354
    const-string v2, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_110

    .line 355
    const-string v4, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "[INSERT_ALBUM_PHOTOS], album ID: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", num photos: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz p4, :cond_111

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    :goto_f7
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ", duration: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_110
    throw v3

    :cond_111
    const/4 v2, 0x0

    goto :goto_f7

    .line 371
    .restart local v7       #albumValues:Landroid/content/ContentValues;
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v12       #notificationUri:Landroid/net/Uri;
    .restart local v14       #resolver:Landroid/content/ContentResolver;
    .restart local v17       #uriBuilder:Landroid/net/Uri$Builder;
    :cond_113
    return-void
.end method

.method public static insertAlbums(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .registers 24
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "ownerId"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 700
    .local p4, aggregateAlbums:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataAlbum;>;"
    .local p5, nonAggregateAlbums:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataAlbum;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 702
    .local v5, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 704
    .local v12, startTime:J
    move-object/from16 v0, p3

    invoke-static {v5, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumEntityMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 705
    .local v4, albumMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 706
    .local v7, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    .line 709
    .local v2, albumCount:I
    :try_start_18
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 711
    if-eqz p4, :cond_2a

    .line 712
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v2, v14, 0x0

    .line 713
    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-static {v5, v0, v4, v7, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumListInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 716
    :cond_2a
    if-eqz p5, :cond_38

    .line 717
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v14

    add-int/2addr v2, v14

    .line 718
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v5, v0, v4, v7, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumListInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/util/Map;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 723
    :cond_38
    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v9

    .line 724
    .local v9, removedAlbumCount:I
    if-lez v9, :cond_bf

    .line 725
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 726
    .local v10, removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 728
    .local v11, sb:Ljava/lang/StringBuilder;
    const-string v14, "album_type == \'ALL_OTHERS\' AND album_id IN("

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 729
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_55
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_9d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 730
    .local v3, albumId:Ljava/lang/String;
    const-string v14, "?,"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 731
    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_69
    .catchall {:try_start_18 .. :try_end_69} :catchall_6a

    goto :goto_55

    .line 743
    .end local v3           #albumId:Ljava/lang/String;
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v9           #removedAlbumCount:I
    .end local v10           #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11           #sb:Ljava/lang/StringBuilder;
    :catchall_6a
    move-exception v14

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 744
    const-string v15, "EsPhotosData"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_9c

    .line 745
    const-string v15, "EsPhotosData"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "[INSERT_ALBUM_LIST], num albums: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", duration: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9c
    throw v14

    .line 733
    .restart local v6       #i$:Ljava/util/Iterator;
    .restart local v9       #removedAlbumCount:I
    .restart local v10       #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v11       #sb:Ljava/lang/StringBuilder;
    :cond_9d
    :try_start_9d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->length()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 734
    const-string v14, ")"

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 736
    const-string v15, "album"

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/String;

    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v5, v15, v0, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 741
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v10           #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v11           #sb:Ljava/lang/StringBuilder;
    :cond_bf
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_c2
    .catchall {:try_start_9d .. :try_end_c2} :catchall_6a

    .line 743
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 744
    const-string v14, "EsPhotosData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_f0

    .line 745
    const-string v14, "EsPhotosData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "[INSERT_ALBUM_LIST], num albums: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", duration: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    :cond_f0
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .restart local v6       #i$:Ljava/util/Iterator;
    :goto_f4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_109

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    .line 753
    .local v8, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v8, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_f4

    .line 755
    .end local v8           #notifyUri:Landroid/net/Uri;
    :cond_109
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_111
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_12f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 756
    .restart local v3       #albumId:Ljava/lang/String;
    sget-object v14, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_ALBUM_URI:Landroid/net/Uri;

    invoke-static {v14, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_111

    .line 758
    .end local v3           #albumId:Ljava/lang/String;
    :cond_12f
    if-gtz v2, :cond_137

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v14

    if-lez v14, :cond_14a

    .line 759
    :cond_137
    sget-object v14, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_OWNER_URI:Landroid/net/Uri;

    move-object/from16 v0, p3

    invoke-static {v14, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v15, v14, v0}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 761
    :cond_14a
    return-void
.end method

.method public static insertEventPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/String;Ljava/util/Map;Ljava/util/List;)V
    .registers 16
    .parameter "db"
    .parameter "photo"
    .parameter "eventId"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 527
    .local p3, seenAlbum:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .local p4, notificationList:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 529
    .local v9, startTime:J
    :try_start_4
    const-string v0, "EsPhotosData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 530
    const/4 v0, 0x3

    const-string v1, "EsPhotosData"

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_18
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;)Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_44

    .line 536
    const-string v0, "EsPhotosData"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_44

    .line 537
    const-string v0, "EsPhotosData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not insert row for event photo; id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    :cond_44
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 543
    .local v8, eventValues:Landroid/content/ContentValues;
    const-string v0, "event_id"

    invoke-virtual {v8, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    const-string v0, "photo_id"

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 546
    const/4 v0, 0x2

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p2, v7, v0

    const/4 v0, 0x1

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v1, v7, v0

    .line 547
    .local v7, eventArgs:[Ljava/lang/String;
    const-string v0, "SELECT count(*) FROM photos_in_event WHERE event_id=? AND photo_id=?"

    invoke-static {p0, v0, v7}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a6

    .line 548
    const-string v0, "photos_in_event"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_7a
    .catchall {:try_start_4 .. :try_end_7a} :catchall_ae

    .line 554
    :goto_7a
    const-string v0, "EsPhotosData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 555
    const-string v0, "EsPhotosData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[INSERT_EVENT_PHOTO], event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    :cond_a5
    return-void

    .line 550
    :cond_a6
    :try_start_a6
    const-string v0, "photos_in_event"

    const-string v1, "event_id=? AND photo_id=?"

    invoke-virtual {p0, v0, v8, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_ad
    .catchall {:try_start_a6 .. :try_end_ad} :catchall_ae

    goto :goto_7a

    .line 554
    .end local v7           #eventArgs:[Ljava/lang/String;
    .end local v8           #eventValues:Landroid/content/ContentValues;
    :catchall_ae
    move-exception v0

    const-string v1, "EsPhotosData"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_da

    .line 555
    const-string v1, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[INSERT_EVENT_PHOTO], event: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_da
    throw v0
.end method

.method public static insertMediaPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/MediaItem;)V
    .registers 15
    .parameter "db"
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    .line 573
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 575
    .local v11, startTime:J
    const-string v0, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 576
    const-string v0, "EsPhotosData"

    const-string v3, ">>>>> media photos"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    :cond_15
    new-instance v3, Lcom/google/api/services/plusi/model/DataUser;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/DataUser;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaItem;->ownerObfuscatedId:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    new-instance v4, Lcom/google/api/services/plusi/model/DataAlbum;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/DataAlbum;-><init>()V

    const-string v0, "UPDATES_ALBUMS"

    iput-object v0, v4, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaItem;->albumId:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    iput-object v3, v4, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaItem;->albumHtml:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    new-instance v5, Lcom/google/api/services/plusi/model/DataImage;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataImage;-><init>()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaItem;->playerUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5d

    const-string v7, "http:"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5d

    const-string v7, "https:"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5d

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "http:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_5d
    iput-object v0, v5, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    new-instance v10, Lcom/google/api/services/plusi/model/DataPhoto;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/DataPhoto;-><init>()V

    iput-object v4, v10, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaItem;->caption:Ljava/lang/String;

    iput-object v0, v10, Lcom/google/api/services/plusi/model/DataPhoto;->caption:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaItem;->photoId:Ljava/lang/String;

    iput-object v0, v10, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    iput-object v5, v10, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iput-object v3, v10, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    .line 580
    .local v10, photo:Lcom/google/api/services/plusi/model/DataPhoto;
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 581
    .local v1, photoList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataPhoto;>;"
    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 583
    iget-object v0, v10, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    .line 586
    .local v9, albumId:Ljava/lang/String;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 587
    .local v6, albumValues:Landroid/content/ContentValues;
    const-string v0, "album_id"

    invoke-virtual {v6, v0, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v5, "photos_in_album"

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v7, v2

    move-object v8, v2

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 593
    const-string v0, "EsPhotosData"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 594
    const-string v0, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[INSERT_MEDIA_PHOTO], duration: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    :cond_b4
    return-void
.end method

.method private static insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/Long;
    .registers 10
    .parameter "db"
    .parameter "album"

    .prologue
    .line 1823
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "album_id"

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "owner_id"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_26

    const-string v4, "title"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->title:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_26
    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->timestampSeconds:Ljava/lang/String;

    if-eqz v4, :cond_40

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->timestampSeconds:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    const-wide v6, 0x408f400000000000L

    mul-double/2addr v4, v6

    double-to-long v4, v4

    const-string v6, "timestamp"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_40
    const-string v4, "album_type"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    if-eqz v4, :cond_52

    const-string v4, "entity_version"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_52
    const-string v4, "ALL_OTHERS"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10a

    const-string v4, "UPDATES_ALBUMS"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c9

    const-string v4, "stream_id"

    const-string v5, "posts"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sort_order"

    const/16 v5, 0x28

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v4, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->sPhotosFromPostsAlbumName:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_87

    const-string v4, "title"

    sget-object v5, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->sPhotosFromPostsAlbumName:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_87
    :goto_87
    const-string v4, "photo_count"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_8c
    :goto_8c
    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v4, :cond_a7

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    if-eqz v4, :cond_a7

    const-string v4, "cover_photo_id"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->cover:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1824
    .local v3, values:Landroid/content/ContentValues;
    :cond_a7
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v0

    .line 1826
    .local v0, albumId:Ljava/lang/String;
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    .line 1828
    .local v1, rowId:Ljava/lang/Long;
    if-nez v1, :cond_127

    .line 1829
    const-string v4, "album"

    const/4 v5, 0x0

    const/4 v6, 0x4

    invoke-virtual {p0, v4, v5, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1831
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-nez v4, :cond_c8

    .line 1832
    const/4 v1, 0x0

    .line 1841
    :cond_c8
    :goto_c8
    return-object v1

    .line 1823
    .end local v0           #albumId:Ljava/lang/String;
    .end local v1           #rowId:Ljava/lang/Long;
    .end local v3           #values:Landroid/content/ContentValues;
    :cond_c9
    const-string v4, "BUNCH_ALBUMS"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e6

    const-string v4, "stream_id"

    const-string v5, "messenger"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sort_order"

    const/16 v5, 0x32

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_87

    :cond_e6
    const-string v4, "PROFILE_PHOTOS"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_103

    const-string v4, "stream_id"

    const-string v5, "profile"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sort_order"

    const/16 v5, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_87

    :cond_103
    const-string v4, "stream_id"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_87

    :cond_10a
    const-string v4, "sort_order"

    const/16 v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "stream_id"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    if-eqz v4, :cond_8c

    const-string v4, "photo_count"

    iget-object v5, p1, Lcom/google/api/services/plusi/model/DataAlbum;->photoCount:Ljava/lang/Integer;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_8c

    .line 1835
    .restart local v0       #albumId:Ljava/lang/String;
    .restart local v1       #rowId:Ljava/lang/Long;
    .restart local v3       #values:Landroid/content/ContentValues;
    :cond_127
    const-string v4, "album"

    const-string v5, "album_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {p0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 1837
    .local v2, rowsUpdated:I
    if-nez v2, :cond_c8

    .line 1838
    const/4 v1, 0x0

    goto :goto_c8
.end method

.method private static insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z
    .registers 13
    .parameter "db"
    .parameter "commentId"
    .parameter "values"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1950
    const-string v7, "SELECT count(*) FROM photo_comment WHERE comment_id=?"

    new-array v8, v5, [Ljava/lang/String;

    aput-object p1, v8, v6

    invoke-static {p0, v7, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 1953
    .local v0, rowCount:J
    const-wide/16 v7, 0x0

    cmp-long v7, v0, v7

    if-nez v7, :cond_23

    .line 1954
    const-string v7, "photo_comment"

    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {p0, v7, v8, p2, v9}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 1956
    .local v2, rowId:J
    const-wide/16 v7, -0x1

    cmp-long v7, v2, v7

    if-eqz v7, :cond_21

    .line 1960
    .end local v2           #rowId:J
    :cond_20
    :goto_20
    return v5

    .restart local v2       #rowId:J
    :cond_21
    move v5, v6

    .line 1956
    goto :goto_20

    .line 1958
    .end local v2           #rowId:J
    :cond_23
    const-string v7, "photo_comment"

    const-string v8, "comment_id=?"

    new-array v9, v5, [Ljava/lang/String;

    aput-object p1, v9, v6

    invoke-virtual {p0, v7, p2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1960
    .local v4, rowsUpdated:I
    if-nez v4, :cond_20

    move v5, v6

    goto :goto_20
.end method

.method private static insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z
    .registers 15
    .parameter "db"
    .parameter "plusOne"
    .parameter "photoId"

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 1912
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    iget-object v6, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-nez v6, :cond_5a

    move v6, v7

    :goto_c
    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-nez v8, :cond_61

    move v8, v7

    :goto_11
    const-string v10, "plusone_data"

    sget-object v11, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    invoke-virtual {v11, p1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "plusone_by_me"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v10, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v6, "plusone_count"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v6, "plusone_id"

    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "photo_id"

    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1914
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "SELECT count(*) FROM photo_plusone WHERE photo_id=?"

    new-array v8, v9, [Ljava/lang/String;

    aput-object p2, v8, v7

    invoke-static {p0, v6, v8}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    .line 1917
    .local v0, rowCount:J
    const-wide/16 v10, 0x0

    cmp-long v6, v0, v10

    if-nez v6, :cond_68

    .line 1918
    const-string v6, "photo_plusone"

    const/4 v8, 0x0

    const/4 v10, 0x4

    invoke-virtual {p0, v6, v8, v5, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    .line 1920
    .local v2, rowId:J
    const-wide/16 v10, -0x1

    cmp-long v6, v2, v10

    if-eqz v6, :cond_59

    move v7, v9

    .line 1924
    .end local v2           #rowId:J
    :cond_59
    :goto_59
    return v7

    .line 1912
    .end local v0           #rowCount:J
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_5a
    iget-object v6, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    goto :goto_c

    :cond_61
    iget-object v8, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    goto :goto_11

    .line 1922
    .restart local v0       #rowCount:J
    .restart local v5       #values:Landroid/content/ContentValues;
    :cond_68
    const-string v6, "photo_plusone"

    const-string v8, "photo_id=?"

    new-array v10, v9, [Ljava/lang/String;

    aput-object p2, v10, v7

    invoke-virtual {p0, v6, v5, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 1924
    .local v4, rowsUpdated:I
    if-eqz v4, :cond_59

    move v7, v9

    goto :goto_59
.end method

.method public static insertPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "photo"
    .parameter "isDownloadable"

    .prologue
    .line 259
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 261
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 262
    .local v9, startTime:J
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v6, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :try_start_11
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 267
    const-string v1, "EsPhotosData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 268
    const/4 v1, 0x3

    const-string v2, "EsPhotosData"

    const/4 v3, 0x0

    invoke-static {p3, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_28
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 277
    .local v5, seenUser:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/google/api/services/plusi/model/DataUser;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v1, p3

    move-object/from16 v2, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;)Ljava/lang/Long;

    move-result-object v1

    if-nez v1, :cond_57

    .line 280
    const-string v1, "EsPhotosData"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_57

    .line 281
    const-string v1, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not insert row for photo of me; id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    :cond_57
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 286
    .local v11, userList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/DataUser;>;"
    invoke-static {v0, v11}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 288
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_62
    .catchall {:try_start_11 .. :try_end_62} :catchall_ab

    .line 290
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 292
    const-string v1, "EsPhotosData"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_92

    .line 293
    const-string v1, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[INSERT_PHOTO], photo ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_92
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, i$:Ljava/util/Iterator;
    :goto_96
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_dd

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/Uri;

    .line 301
    .local v8, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v8, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_96

    .line 290
    .end local v5           #seenUser:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/google/api/services/plusi/model/DataUser;>;"
    .end local v7           #i$:Ljava/util/Iterator;
    .end local v8           #notifyUri:Landroid/net/Uri;
    .end local v11           #userList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/DataUser;>;"
    :catchall_ab
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 292
    const-string v2, "EsPhotosData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_dc

    .line 293
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[INSERT_PHOTO], photo ID: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p3, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", duration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_dc
    throw v1

    .line 304
    .restart local v5       #seenUser:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/google/api/services/plusi/model/DataUser;>;"
    .restart local v7       #i$:Ljava/util/Iterator;
    .restart local v11       #userList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/DataUser;>;"
    :cond_dd
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 305
    return-void
.end method

.method private static insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;)Ljava/lang/Long;
    .registers 42
    .parameter "db"
    .parameter "photo"
    .parameter "isDownloadable"
    .parameter "updateShapes"
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            "Ljava/lang/Boolean;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/api/services/plusi/model/DataUser;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/lang/Long;"
        }
    .end annotation

    .prologue
    .line 1628
    .local p4, seenAlbum:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    .local p5, seenUser:Ljava/util/Set;,"Ljava/util/Set<Lcom/google/api/services/plusi/model/DataUser;>;"
    .local p6, pendingNotification:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    if-nez v2, :cond_2a

    .line 1629
    const-string v2, "EsPhotosData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1630
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot add photo that has no album; photo id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    :cond_27
    const/16 v26, 0x0

    .line 1786
    :cond_29
    :goto_29
    return-object v26

    .line 1635
    :cond_2a
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    .line 1636
    .local v11, album:Lcom/google/api/services/plusi/model/DataAlbum;
    invoke-static {v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getAlbumId(Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/String;

    move-result-object v12

    .line 1638
    .local v12, albumId:Ljava/lang/String;
    if-eqz p4, :cond_97

    move-object/from16 v0, p4

    invoke-interface {v0, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    move-object/from16 v24, v2

    .line 1639
    .local v24, oldEntity:Ljava/lang/Long;
    :goto_3e
    if-nez v24, :cond_5d

    .line 1642
    :try_start_40
    const-string v2, "SELECT entity_version FROM album WHERE album_id = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v11, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v24

    .line 1644
    if-eqz p4, :cond_5d

    .line 1645
    move-object/from16 v0, p4

    move-object/from16 v1, v24

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5d
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_40 .. :try_end_5d} :catch_9a

    .line 1656
    :cond_5d
    :goto_5d
    iget-object v0, v11, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    move-object/from16 v23, v0

    .line 1658
    .local v23, newEntity:Ljava/lang/Long;
    if-eqz v24, :cond_6f

    if-eqz v23, :cond_b4

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b4

    .line 1659
    :cond_6f
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdateAlbumRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataAlbum;)Ljava/lang/Long;

    move-result-object v13

    .line 1660
    .local v13, albumRowId:Ljava/lang/Long;
    if-nez v13, :cond_ab

    .line 1663
    const-string v2, "EsPhotosData"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_94

    .line 1664
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not insert album row; album id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1666
    :cond_94
    const/16 v26, 0x0

    goto :goto_29

    .line 1638
    .end local v13           #albumRowId:Ljava/lang/Long;
    .end local v23           #newEntity:Ljava/lang/Long;
    .end local v24           #oldEntity:Ljava/lang/Long;
    :cond_97
    const/16 v24, 0x0

    goto :goto_3e

    .line 1649
    .restart local v24       #oldEntity:Ljava/lang/Long;
    :catch_9a
    move-exception v2

    const/16 v24, 0x0

    .line 1651
    if-eqz p4, :cond_5d

    .line 1652
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-interface {v0, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5d

    .line 1668
    .restart local v13       #albumRowId:Ljava/lang/Long;
    .restart local v23       #newEntity:Ljava/lang/Long;
    :cond_ab
    if-eqz p4, :cond_b4

    .line 1669
    move-object/from16 v0, p4

    move-object/from16 v1, v23

    invoke-interface {v0, v12, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674
    .end local v13           #albumRowId:Ljava/lang/Long;
    :cond_b4
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "photo_id"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "plus_one_key"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "album_id"

    invoke-virtual {v3, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "url"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "title"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->title:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->caption:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_100

    const-string v2, "description"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->caption:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_100
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->viewerCanComment:Ljava/lang/Boolean;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->viewerCanTag:Ljava/lang/Boolean;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    if-eqz v2, :cond_2ae

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2ae

    const/4 v2, 0x2

    :goto_11b
    or-int/lit8 v7, v2, 0x0

    if-eqz v4, :cond_2b1

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2b1

    const/4 v2, 0x4

    :goto_126
    or-int v4, v7, v2

    if-eqz v5, :cond_2b4

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2b4

    const/16 v2, 0x8

    :goto_132
    or-int/2addr v4, v2

    if-eqz v6, :cond_2b7

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2b7

    const/16 v2, 0x10

    :goto_13d
    or-int/2addr v2, v4

    const-string v4, "action_state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    if-eqz v2, :cond_194

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    if-nez v2, :cond_2ba

    const/4 v2, 0x0

    :goto_154
    if-eqz v2, :cond_2c4

    const-string v4, "comment_count"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "EsPhotosData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_194

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v2, v4, :cond_194

    const-string v4, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WARN: comment mismatch; total: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", actual: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_194
    :goto_194
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-eqz v2, :cond_1a5

    const-string v2, "owner_id"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a5
    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    if-eqz v4, :cond_2cf

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_2cf

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->timestampSeconds:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    :cond_1c6
    :goto_1c6
    if-eqz v2, :cond_1d8

    const-string v4, "timestamp"

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_1d8
    const-string v2, "entity_version"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getFingerPrint(Lcom/google/api/services/plusi/model/DataPhoto;)[B

    move-result-object v2

    if-eqz v2, :cond_1ec

    const-string v4, "fingerprint"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    :cond_1ec
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v2, :cond_203

    const-string v2, "video_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataPhoto;->video:Lcom/google/api/services/plusi/model/DataVideo;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/DataVideoJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_203
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadStatus:Ljava/lang/String;

    if-eqz v2, :cond_212

    const-string v2, "upload_status"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadStatus:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_212
    if-eqz p2, :cond_21b

    const-string v2, "downloadable"

    move-object/from16 v0, p2

    invoke-virtual {v3, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_21b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v26

    if-nez v26, :cond_2f1

    const-string v2, "photo"

    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_241

    const/16 v26, 0x0

    .line 1676
    .local v26, photoRowId:Ljava/lang/Long;
    :cond_241
    :goto_241
    if-eqz p5, :cond_24c

    .line 1677
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1684
    :cond_24c
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v2, :cond_30b

    .line 1685
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z

    .line 1691
    :goto_25f
    const/16 v17, 0x0

    .line 1692
    .local v17, commentsAdded:Z
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 1693
    .local v19, existingComments:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v3, "photo_comment"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "comment_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "update_time"

    aput-object v5, v4, v2

    const-string v5, "photo_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 1698
    .local v18, cursor:Landroid/database/Cursor;
    :goto_28b
    :try_start_28b
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_316

    .line 1699
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2a8
    .catchall {:try_start_28b .. :try_end_2a8} :catchall_2a9

    goto :goto_28b

    .line 1702
    :catchall_2a9
    move-exception v2

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v2

    .line 1674
    .end local v17           #commentsAdded:Z
    .end local v18           #cursor:Landroid/database/Cursor;
    .end local v19           #existingComments:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v26           #photoRowId:Ljava/lang/Long;
    :cond_2ae
    const/4 v2, 0x0

    goto/16 :goto_11b

    :cond_2b1
    const/4 v2, 0x0

    goto/16 :goto_126

    :cond_2b4
    const/4 v2, 0x0

    goto/16 :goto_132

    :cond_2b7
    const/4 v2, 0x0

    goto/16 :goto_13d

    :cond_2ba
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    goto/16 :goto_154

    :cond_2c4
    const-string v2, "comment_count"

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->totalComments:Ljava/lang/Integer;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_194

    :cond_2cf
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    if-eqz v4, :cond_1c6

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1c6

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->uploadTimestampSeconds:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_1c6

    :cond_2f1
    const-string v2, "photo"

    const-string v4, "photo_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_241

    const/16 v26, 0x0

    goto/16 :goto_241

    .line 1687
    .restart local v26       #photoRowId:Ljava/lang/Long;
    :cond_30b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto/16 :goto_25f

    .line 1702
    .restart local v17       #commentsAdded:Z
    .restart local v18       #cursor:Landroid/database/Cursor;
    .restart local v19       #existingComments:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_316
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 1705
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    if-eqz v2, :cond_36a

    .line 1706
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->comment:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, i$:Ljava/util/Iterator;
    :cond_327
    :goto_327
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_36a

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/DataComment;

    .line 1707
    .local v14, comment:Lcom/google/api/services/plusi/model/DataComment;
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Ljava/lang/Long;

    .line 1708
    .local v32, updateTimestamp:Ljava/lang/Long;
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v14, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCommentContentValues(Lcom/google/api/services/plusi/model/DataComment;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v34

    .line 1711
    .local v34, values:Landroid/content/ContentValues;
    if-eqz v32, :cond_351

    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->lastUpdateTimestamp:Ljava/lang/Long;

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_327

    .line 1713
    :cond_351
    if-eqz p5, :cond_35a

    .line 1714
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->user:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v0, p5

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1716
    :cond_35a
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 1717
    const/16 v17, 0x1

    goto :goto_327

    .line 1723
    .end local v14           #comment:Lcom/google/api/services/plusi/model/DataComment;
    .end local v21           #i$:Ljava/util/Iterator;
    .end local v32           #updateTimestamp:Ljava/lang/Long;
    .end local v34           #values:Landroid/content/ContentValues;
    :cond_36a
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_3d0

    .line 1724
    new-instance v27, Ljava/util/ArrayList;

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    move-result v2

    move-object/from16 v0, v27

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1725
    .local v27, removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    .line 1727
    .local v28, sb:Ljava/lang/StringBuilder;
    const-string v2, "comment_id IN("

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1728
    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .restart local v21       #i$:Ljava/util/Iterator;
    :goto_38f
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3a8

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1729
    .local v15, commentId:Ljava/lang/String;
    const-string v2, "?,"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1730
    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_38f

    .line 1732
    .end local v15           #commentId:Ljava/lang/String;
    :cond_3a8
    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1733
    const-string v2, ")"

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1735
    const-string v3, "photo_comment"

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1739
    .end local v21           #i$:Ljava/util/Iterator;
    .end local v27           #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v28           #sb:Ljava/lang/StringBuilder;
    :cond_3d0
    if-nez v17, :cond_3d8

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashMap;->size()I

    move-result v2

    if-lez v2, :cond_3eb

    .line 1740
    :cond_3d8
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 1742
    .local v16, commentNotifyUri:Landroid/net/Uri;
    if-eqz p6, :cond_3eb

    .line 1743
    move-object/from16 v0, p6

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1748
    .end local v16           #commentNotifyUri:Landroid/net/Uri;
    :cond_3eb
    const-string v2, "photo_shape"

    const-string v3, "photo_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1751
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    if-eqz v2, :cond_504

    .line 1752
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v30

    .line 1753
    .local v30, shapeCount:I
    const/16 v20, 0x0

    .local v20, i:I
    :goto_40e
    move/from16 v0, v20

    move/from16 v1, v30

    if-ge v0, v1, :cond_4f1

    .line 1754
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->shape:Ljava/util/List;

    move/from16 v0, v20

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/api/services/plusi/model/DataShape;

    .line 1757
    .local v29, shape:Lcom/google/api/services/plusi/model/DataShape;
    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    move-object/from16 v33, v0

    .line 1758
    .local v33, user:Lcom/google/api/services/plusi/model/DataUser;
    if-eqz v33, :cond_4d6

    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_4d6

    move-object/from16 v0, v33

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    if-eqz v2, :cond_4d6

    const-string v2, "0"

    move-object/from16 v0, v33

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4d6

    const/16 v22, 0x1

    .line 1760
    .local v22, isValidUser:Z
    :goto_442
    if-eqz v22, :cond_4d2

    .line 1761
    if-eqz p5, :cond_44d

    .line 1765
    move-object/from16 v0, p5

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1769
    :cond_44d
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataShape;->relativeBounds:Lcom/google/api/services/plusi/model/DataRectRelative;

    if-eqz v4, :cond_46d

    const-string v4, "bounds"

    invoke-static {}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->getInstance()Lcom/google/api/services/plusi/model/DataRectRelativeJson;

    move-result-object v5

    move-object/from16 v0, v29

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataShape;->relativeBounds:Lcom/google/api/services/plusi/model/DataRectRelative;

    invoke-virtual {v5, v6}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_46d
    const-string v4, "creator_id"

    move-object/from16 v0, v29

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataShape;->creator:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "photo_id"

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "shape_id"

    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataShape;->id:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "status"

    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataShape;->status:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v29

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    if-eqz v2, :cond_4a8

    const-string v2, "subject_id"

    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataShape;->user:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4a8
    if-eqz v3, :cond_4d2

    const-string v2, "SELECT count(*) FROM photo_shape WHERE shape_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v29

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataShape;->id:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-static {v0, v2, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_4da

    const-string v2, "photo_shape"

    const/4 v4, 0x0

    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4d2

    .line 1753
    :cond_4d2
    :goto_4d2
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_40e

    .line 1758
    .end local v22           #isValidUser:Z
    :cond_4d6
    const/16 v22, 0x0

    goto/16 :goto_442

    .line 1769
    .restart local v22       #isValidUser:Z
    :cond_4da
    const-string v2, "photo_shape"

    const-string v4, "shape_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, v29

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataShape;->id:Ljava/lang/String;

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_4d2

    goto :goto_4d2

    .line 1771
    .end local v22           #isValidUser:Z
    .end local v29           #shape:Lcom/google/api/services/plusi/model/DataShape;
    .end local v33           #user:Lcom/google/api/services/plusi/model/DataUser;
    :cond_4f1
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v31

    .line 1773
    .local v31, shapeNotifyUri:Landroid/net/Uri;
    if-eqz p6, :cond_504

    .line 1774
    move-object/from16 v0, p6

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1779
    .end local v20           #i:I
    .end local v30           #shapeCount:I
    .end local v31           #shapeNotifyUri:Landroid/net/Uri;
    :cond_504
    if-eqz v26, :cond_29

    .line 1780
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v25

    .line 1782
    .local v25, photoNotifyUri:Landroid/net/Uri;
    if-eqz p6, :cond_29

    .line 1783
    move-object/from16 v0, p6

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_29
.end method

.method private static insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 26
    .parameter "db"
    .parameter
    .parameter "isDownloadable"
    .parameter "collectionAlbum"
    .parameter
    .parameter "collectionTable"
    .parameter "collectionValues"
    .parameter
    .parameter "syncState"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/google/api/services/plusi/model/DataAlbum;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/content/ContentValues;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1554
    .local p1, photos:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataPhoto;>;"
    .local p4, collectionPhotoMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    .local p7, notificationList:Ljava/util/List;,"Ljava/util/List<Landroid/net/Uri;>;"
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1555
    .local v7, seenAlbum:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1557
    .local v8, seenUser:Ljava/util/HashSet;,"Ljava/util/HashSet<Lcom/google/api/services/plusi/model/DataUser;>;"
    if-eqz p3, :cond_17

    .line 1558
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataAlbum;->entityVersion:Ljava/lang/Long;

    invoke-interface {v7, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1561
    :cond_17
    if-eqz p1, :cond_91

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    .line 1562
    .local v13, photoCount:I
    :goto_1d
    const/4 v10, 0x0

    .local v10, i:I
    :goto_1e
    if-ge v10, v13, :cond_ca

    .line 1563
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataPhoto;

    .line 1565
    .local v4, photo:Lcom/google/api/services/plusi/model/DataPhoto;
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 1566
    const/4 v3, 0x3

    const-string v5, "EsPhotosData"

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotoOutput(Lcom/google/api/services/plusi/model/DataPhoto;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 1569
    :cond_3c
    if-eqz p8, :cond_41

    .line 1570
    invoke-virtual/range {p8 .. p8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementSubCount()V

    .line 1573
    :cond_41
    iget-object v3, v4, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    .line 1574
    .local v14, photoId:Ljava/lang/Long;
    if-eqz p4, :cond_93

    move-object/from16 v0, p4

    invoke-interface {v0, v14}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    move-object v12, v3

    .line 1576
    .local v12, oldEntity:Ljava/lang/Long;
    :goto_56
    iget-object v3, v4, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    if-eqz v3, :cond_95

    iget-object v11, v4, Lcom/google/api/services/plusi/model/DataPhoto;->entityVersion:Ljava/lang/Long;

    .line 1579
    .local v11, newEntity:Ljava/lang/Long;
    :goto_5c
    if-eqz v12, :cond_64

    invoke-virtual {v12, v11}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_97

    .line 1581
    :cond_64
    const/4 v6, 0x1

    move-object/from16 v3, p0

    move-object/from16 v5, p2

    move-object/from16 v9, p7

    invoke-static/range {v3 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;ZLjava/util/Map;Ljava/util/Set;Ljava/util/List;)Ljava/lang/Long;

    move-result-object v15

    .line 1583
    .local v15, rowId:Ljava/lang/Long;
    if-nez v15, :cond_b4

    .line 1585
    const-string v3, "EsPhotosData"

    const/4 v5, 0x5

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8e

    .line 1586
    const-string v3, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not insert row for photo of me; id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1562
    .end local v15           #rowId:Ljava/lang/Long;
    :cond_8e
    :goto_8e
    add-int/lit8 v10, v10, 0x1

    goto :goto_1e

    .line 1561
    .end local v4           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .end local v10           #i:I
    .end local v11           #newEntity:Ljava/lang/Long;
    .end local v12           #oldEntity:Ljava/lang/Long;
    .end local v13           #photoCount:I
    .end local v14           #photoId:Ljava/lang/Long;
    :cond_91
    const/4 v13, 0x0

    goto :goto_1d

    .line 1574
    .restart local v4       #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .restart local v10       #i:I
    .restart local v13       #photoCount:I
    .restart local v14       #photoId:Ljava/lang/Long;
    :cond_93
    const/4 v12, 0x0

    goto :goto_56

    .line 1576
    .restart local v12       #oldEntity:Ljava/lang/Long;
    :cond_95
    const/4 v11, 0x0

    goto :goto_5c

    .line 1591
    .restart local v11       #newEntity:Ljava/lang/Long;
    :cond_97
    const-string v3, "EsPhotosData"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_b4

    .line 1592
    const-string v3, "EsPhotosData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Photo not updated; id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1597
    :cond_b4
    if-nez v12, :cond_8e

    if-eqz p5, :cond_8e

    .line 1598
    const-string v3, "photo_id"

    move-object/from16 v0, p6

    invoke-virtual {v0, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1599
    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_8e

    .line 1603
    .end local v4           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .end local v11           #newEntity:Ljava/lang/Long;
    .end local v12           #oldEntity:Ljava/lang/Long;
    .end local v14           #photoId:Ljava/lang/Long;
    :cond_ca
    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1604
    .local v16, userList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/api/services/plusi/model/DataUser;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 1605
    return-void
.end method

.method public static insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .registers 25
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "streamId"
    .parameter "ownerGaiaId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 386
    .local p5, photos:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataPhoto;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 388
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    .line 389
    .local v17, startTime:J
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .local v9, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCurrentStreamMap(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 393
    .local v6, photoMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/lang/Long;>;"
    :try_start_19
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 395
    if-eqz p2, :cond_21

    .line 396
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    .line 400
    :cond_21
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 401
    .local v8, streamValues:Landroid/content/ContentValues;
    const-string v3, "stream_id"

    move-object/from16 v0, p3

    invoke-virtual {v8, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v7, "photos_in_stream"

    move-object/from16 v3, p5

    move-object/from16 v10, p2

    invoke-static/range {v2 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 408
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v3

    if-lez v3, :cond_e6

    .line 409
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 410
    .local v16, sb:Ljava/lang/StringBuilder;
    new-instance v15, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-direct {v15, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 412
    .local v15, removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "stream_id=? AND photo_id IN("

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :goto_60
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    .line 417
    .local v13, photoId:J
    const-string v3, "?,"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-static {v13, v14}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7e
    .catchall {:try_start_19 .. :try_end_7e} :catchall_7f

    goto :goto_60

    .line 429
    .end local v8           #streamValues:Landroid/content/ContentValues;
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v13           #photoId:J
    .end local v15           #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16           #sb:Ljava/lang/StringBuilder;
    :catchall_7f
    move-exception v3

    move-object v4, v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 430
    const-string v3, "EsPhotosData"

    const/4 v5, 0x4

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c1

    .line 431
    const-string v5, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "[INSERT_STREAM_PHOTOS], stream: "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", num photos: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-eqz p5, :cond_144

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    :goto_a8
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, ", duration: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c1
    throw v4

    .line 420
    .restart local v8       #streamValues:Landroid/content/ContentValues;
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v15       #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v16       #sb:Ljava/lang/StringBuilder;
    :cond_c2
    :try_start_c2
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 421
    const-string v3, ")"

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    const-string v4, "photos_in_stream"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v15, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 427
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v15           #removedIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16           #sb:Ljava/lang/StringBuilder;
    :cond_e6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e9
    .catchall {:try_start_c2 .. :try_end_e9} :catchall_7f

    .line 429
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 430
    const-string v3, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_129

    .line 431
    const-string v4, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "[INSERT_STREAM_PHOTOS], stream: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", num photos: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz p5, :cond_142

    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v3

    :goto_110
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", duration: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_129
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11       #i$:Ljava/util/Iterator;
    :goto_12d
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_147

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/net/Uri;

    .line 440
    .local v12, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v12, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_12d

    .line 431
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #notifyUri:Landroid/net/Uri;
    :cond_142
    const/4 v3, 0x0

    goto :goto_110

    .end local v8           #streamValues:Landroid/content/ContentValues;
    :cond_144
    const/4 v3, 0x0

    goto/16 :goto_a8

    .line 443
    .restart local v8       #streamValues:Landroid/content/ContentValues;
    .restart local v11       #i$:Ljava/util/Iterator;
    :cond_147
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_STREAM_ID_AND_OWNER_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p3

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 446
    .restart local v12       #notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v12, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 447
    return-void
.end method

.method public static insertUserPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter
    .parameter
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 461
    .local p3, approvedPhotos:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataPhoto;>;"
    .local p4, unapprovedPhotos:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataPhoto;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 463
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 464
    .local v13, startTime:J
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 466
    .local v8, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p3, :cond_cb

    const/4 v10, 0x0

    .line 467
    .local v10, approvedSize:I
    :goto_14
    if-nez p4, :cond_d1

    const/4 v15, 0x0

    .line 470
    .local v15, unapprovedSize:I
    :goto_17
    :try_start_17
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 473
    const-string v2, "photos_of_user"

    const-string v3, "photo_of_user_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p5, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 476
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 477
    const-string v2, "EsPhotosData"

    const-string v3, ">>>>> approved photos"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    :cond_37
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 481
    .local v7, collectionValues:Landroid/content/ContentValues;
    const-string v2, "photo_of_user_id"

    move-object/from16 v0, p5

    invoke-virtual {v7, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "photos_of_user"

    move-object/from16 v2, p3

    move-object/from16 v9, p2

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 487
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5f

    .line 488
    const-string v2, "EsPhotosData"

    const-string v3, ">>>>> unapproved photos"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_5f
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "photos_of_user"

    move-object/from16 v2, p4

    move-object/from16 v9, p2

    invoke-static/range {v1 .. v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhotosInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/Boolean;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/Map;Ljava/lang/String;Landroid/content/ContentValues;Ljava/util/List;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    .line 495
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6e
    .catchall {:try_start_17 .. :try_end_6e} :catchall_d7

    .line 497
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 498
    const-string v2, "EsPhotosData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b2

    .line 499
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[INSERT_USER_PHOTOS], userId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", approved: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", unapproved: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", duration: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    :cond_b2
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :goto_b6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11d

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/net/Uri;

    .line 509
    .local v12, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v12, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_b6

    .line 466
    .end local v7           #collectionValues:Landroid/content/ContentValues;
    .end local v10           #approvedSize:I
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #notifyUri:Landroid/net/Uri;
    .end local v15           #unapprovedSize:I
    :cond_cb
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v10

    goto/16 :goto_14

    .line 467
    .restart local v10       #approvedSize:I
    :cond_d1
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v15

    goto/16 :goto_17

    .line 497
    .restart local v15       #unapprovedSize:I
    :catchall_d7
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 498
    const-string v3, "EsPhotosData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_11c

    .line 499
    const-string v3, "EsPhotosData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[INSERT_USER_PHOTOS], userId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", approved: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", unapproved: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11c
    throw v2

    .line 512
    .restart local v7       #collectionValues:Landroid/content/ContentValues;
    .restart local v11       #i$:Ljava/util/Iterator;
    :cond_11d
    if-gtz v10, :cond_121

    if-lez v15, :cond_131

    .line 513
    :cond_121
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_OF_USER_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p5

    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 514
    .restart local v12       #notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v12, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 516
    .end local v12           #notifyUri:Landroid/net/Uri;
    :cond_131
    return-void
.end method

.method public static setPhotosFromPostsAlbumName(Ljava/lang/String;)V
    .registers 1
    .parameter "photosFromPostsAlbumName"

    .prologue
    .line 246
    sput-object p0, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->sPhotosFromPostsAlbumName:Ljava/lang/String;

    .line 247
    return-void
.end method

.method static syncTopLevel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "syncState"

    .prologue
    .line 1201
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1202
    const/4 v15, 0x0

    .line 1259
    :goto_7
    return v15

    .line 1205
    :cond_8
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1206
    const-string v2, "EsPhotosData"

    const-string v3, "    #syncTopLevel(); start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1209
    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    .line 1210
    .local v5, gaiaId:Ljava/lang/String;
    const-string v2, "Photos:TopLevel"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 1212
    const/4 v15, 0x1

    .line 1214
    .local v15, success:Z
    new-instance v1, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1216
    .local v1, op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 1217
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_62

    .line 1218
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed user photo; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1220
    const/4 v15, 0x0

    .line 1223
    :cond_62
    new-instance v1, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    .end local v1           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1225
    .restart local v1       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 1226
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_9f

    .line 1227
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed photo albums; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    const/4 v15, 0x0

    .line 1232
    :cond_9f
    new-instance v1, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    .end local v1           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    const-string v10, "camerasync"

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v6, v1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1235
    .restart local v1       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 1236
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_e1

    .line 1237
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed camera photos; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1239
    const/4 v15, 0x0

    .line 1242
    :cond_e1
    new-instance v1, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    .end local v1           #op:Lcom/google/android/apps/plus/network/HttpOperation;
    const-string v10, "posts"

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v6, v1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object v11, v5

    invoke-direct/range {v6 .. v14}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1245
    .restart local v1       #op:Lcom/google/android/apps/plus/network/HttpOperation;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    .line 1246
    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_123

    .line 1247
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    #syncTopLevel(); failed post photos; code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    const/4 v15, 0x0

    .line 1252
    :cond_123
    if-eqz v15, :cond_135

    .line 1253
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_135

    .line 1254
    const-string v2, "EsPhotosData"

    const-string v3, "    #syncTopLevel(); completed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    :cond_135
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    goto/16 :goto_7
.end method

.method public static updateInstantUploadCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter "cover"

    .prologue
    .line 1134
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1136
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 1137
    .local v7, startTime:J
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1140
    .local v3, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :try_start_11
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1142
    new-instance v9, Landroid/content/ContentValues;

    const/4 v10, 0x6

    invoke-direct {v9, v10}, Landroid/content/ContentValues;-><init>(I)V

    .line 1144
    .local v9, values:Landroid/content/ContentValues;
    const-string v10, "type"

    const-string v11, "from_my_phone"

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    const-string v10, "sort_order"

    const/16 v11, 0x1e

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1146
    const-string v10, "photo_count"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1149
    const-string v10, "from_my_phone"

    invoke-static {v1, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotosHomeRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v5

    .line 1150
    .local v5, rowId:J
    const-wide/16 v10, -0x1

    cmp-long v10, v5, v10

    if-eqz v10, :cond_ef

    .line 1151
    const-string v10, "photo_home"

    const-string v11, "type=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "from_my_phone"

    aput-object v14, v12, v13

    invoke-virtual {v1, v10, v9, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1160
    :goto_4c
    const-string v10, "photo_home_cover"

    const-string v11, "photo_home_key=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v1, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1164
    if-eqz p2, :cond_af

    .line 1166
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 1167
    const-string v10, "photo_home_key"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1168
    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7e

    .line 1169
    const-string v10, "photo_id"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    :cond_7e
    const-string v10, "url"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1172
    const-string v10, "width"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1173
    const-string v10, "height"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1174
    const-string v10, "size"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->fileSize:Ljava/lang/Long;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1175
    const-string v10, "photo_home_cover"

    const/4 v11, 0x0

    const/4 v12, 0x4

    invoke-virtual {v1, v10, v11, v9, v12}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1179
    :cond_af
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b2
    .catchall {:try_start_11 .. :try_end_b2} :catchall_f9

    .line 1181
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1182
    const-string v10, "EsPhotosData"

    const/4 v11, 0x4

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_d6

    .line 1183
    const-string v10, "EsPhotosData"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[INSERT_COVER_INSTANT_UPLOAD], duration: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1189
    :cond_d6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_da
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_11f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 1190
    .local v4, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v4, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_da

    .line 1155
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #notifyUri:Landroid/net/Uri;
    :cond_ef
    :try_start_ef
    const-string v10, "photo_home"

    const/4 v11, 0x0

    const/4 v12, 0x4

    invoke-virtual {v1, v10, v11, v9, v12}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_f6
    .catchall {:try_start_ef .. :try_end_f6} :catchall_f9

    move-result-wide v5

    goto/16 :goto_4c

    .line 1181
    .end local v5           #rowId:J
    .end local v9           #values:Landroid/content/ContentValues;
    :catchall_f9
    move-exception v10

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1182
    const-string v11, "EsPhotosData"

    const/4 v12, 0x4

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_11e

    .line 1183
    const-string v11, "EsPhotosData"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[INSERT_COVER_INSTANT_UPLOAD], duration: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11e
    throw v10

    .line 1192
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v5       #rowId:J
    .restart local v9       #values:Landroid/content/ContentValues;
    :cond_11f
    return-void
.end method

.method public static updatePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/Comment;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "comment"

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x3

    const/4 v10, 0x0

    .line 977
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 979
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 980
    .local v4, startTime:J
    const/4 v7, 0x1

    new-array v0, v7, [Ljava/lang/String;

    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    aput-object v7, v0, v10

    .line 982
    .local v0, commentArgs:[Ljava/lang/String;
    :try_start_16
    const-string v7, "SELECT photo_id FROM photo_comment WHERE comment_id = ?"

    invoke-static {v1, v7, v0}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;
    :try_end_1f
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_16 .. :try_end_1f} :catch_f6

    move-result-object v3

    .line 994
    .local v3, photoId:Ljava/lang/String;
    const-string v7, "EsPhotosData"

    invoke-static {v7, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5d

    .line 995
    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "COMMENT [id: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", content: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p2, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v11, v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 998
    :cond_5d
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "photo_id"

    invoke-virtual {v6, v7, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "comment_id"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "author_id"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "content"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    if-eqz v7, :cond_87

    const-string v7, "create_time"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_87
    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->updatedTimestampUsec:Ljava/lang/Long;

    if-eqz v7, :cond_92

    const-string v7, "update_time"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/Comment;->updatedTimestampUsec:Ljava/lang/Long;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_92
    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v7, :cond_a3

    const-string v7, "plusone_data"

    sget-object v8, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    iget-object v9, p2, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-virtual {v8, v9}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    .local v6, values:Landroid/content/ContentValues;
    :cond_a3
    iget-object v7, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-static {v1, v7, v6}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    .line 1001
    const-string v7, "EsPhotosData"

    const/4 v8, 0x4

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_df

    .line 1002
    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "[UPDATE_PHOTO_COMMENTS], photo ID: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", comment ID: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", duration: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    :cond_df
    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v7, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1011
    .local v2, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    invoke-virtual {v7, v2, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1012
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1013
    .end local v2           #notifyUri:Landroid/net/Uri;
    .end local v3           #photoId:Ljava/lang/String;
    .end local v6           #values:Landroid/content/ContentValues;
    :cond_f5
    :goto_f5
    return-void

    .line 988
    :catch_f6
    move-exception v7

    const-string v7, "EsPhotosData"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_f5

    .line 989
    const-string v7, "EsPhotosData"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "WARNING: could not find photo for the comment: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p2, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f5
.end method

.method public static updatePhotoCommentList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V
    .registers 20
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataComment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 923
    .local p3, commentList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataComment;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 924
    .local v5, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-nez p3, :cond_93

    const/4 v2, 0x0

    .line 926
    .local v2, commentCount:I
    :goto_8
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 928
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 930
    .local v7, startTime:J
    :try_start_14
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 932
    const/4 v4, 0x0

    .local v4, i:I
    :goto_18
    if-ge v4, v2, :cond_99

    .line 933
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataComment;

    .line 934
    .local v1, comment:Lcom/google/api/services/plusi/model/DataComment;
    move-object/from16 v0, p2

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getCommentContentValues(Lcom/google/api/services/plusi/model/DataComment;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v10

    .line 936
    .local v10, values:Landroid/content/ContentValues;
    const-string v11, "EsPhotosData"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_68

    .line 937
    const/4 v11, 0x3

    const-string v12, "EsPhotosData"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "COMMENT [id: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v1, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", content: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v1, Lcom/google/api/services/plusi/model/DataComment;->text:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "]"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->writeToLog(ILjava/lang/String;Ljava/lang/String;)V

    .line 941
    :cond_68
    iget-object v11, v1, Lcom/google/api/services/plusi/model/DataComment;->id:Ljava/lang/String;

    invoke-static {v3, v11, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoCommentRow$1e3cff3e(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)Z

    move-result v11

    if-eqz v11, :cond_76

    .line 943
    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-static {v3, v0, v11}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateCommentCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 947
    :cond_76
    sget-object v11, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p2

    invoke-static {v11, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 949
    .local v6, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v6, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 950
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    sget-object v12, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_90
    .catchall {:try_start_14 .. :try_end_90} :catchall_ee

    .line 932
    add-int/lit8 v4, v4, 0x1

    goto :goto_18

    .line 924
    .end local v1           #comment:Lcom/google/api/services/plusi/model/DataComment;
    .end local v2           #commentCount:I
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v4           #i:I
    .end local v6           #notifyUri:Landroid/net/Uri;
    .end local v7           #startTime:J
    .end local v10           #values:Landroid/content/ContentValues;
    :cond_93
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    goto/16 :goto_8

    .line 953
    .restart local v2       #commentCount:I
    .restart local v3       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v4       #i:I
    .restart local v7       #startTime:J
    :cond_99
    :try_start_99
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_9c
    .catchall {:try_start_99 .. :try_end_9c} :catchall_ee

    .line 955
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 956
    const-string v11, "EsPhotosData"

    const/4 v12, 0x4

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_d6

    .line 957
    const-string v11, "EsPhotosData"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[INSERT_PHOTO_COMMENTS], photo ID: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", num comments: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", duration: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 965
    :cond_d6
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 966
    .local v9, uriCount:I
    const/4 v4, 0x0

    :goto_db
    if-ge v4, v9, :cond_12a

    .line 967
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/Uri;

    const/4 v13, 0x0

    invoke-virtual {v12, v11, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 966
    add-int/lit8 v4, v4, 0x1

    goto :goto_db

    .line 955
    .end local v4           #i:I
    .end local v9           #uriCount:I
    :catchall_ee
    move-exception v11

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 956
    const-string v12, "EsPhotosData"

    const/4 v13, 0x4

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_129

    .line 957
    const-string v12, "EsPhotosData"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "[INSERT_PHOTO_COMMENTS], photo ID: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", num comments: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", duration: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_129
    throw v11

    .line 969
    .restart local v4       #i:I
    .restart local v9       #uriCount:I
    :cond_12a
    return-void
.end method

.method public static updatePhotoPlusOne$55b1eb27(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "addPlusOne"

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 770
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 771
    const-string v3, "EsPhotosData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, ">> updatePlusOne; photo id: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_96

    const-string v2, ""

    :goto_1d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " +1\'d"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 775
    :cond_2e
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 778
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "photo_plusone"

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "plusone_data"

    aput-object v4, v3, v7

    const-string v4, "photo_id=?"

    new-array v5, v5, [Ljava/lang/String;

    aput-object p2, v5, v7

    move-object v7, v6

    move-object v8, v6

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 783
    .local v9, cursor:Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 785
    .local v11, plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    :try_start_4b
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_67

    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_67

    .line 786
    sget-object v2, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->PLUS_ONE_JSON:Lcom/google/api/services/plusi/model/DataPlusOneJson;

    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/api/services/plusi/model/DataPlusOne;

    move-object v11, v0

    .line 789
    :cond_67
    if-nez v11, :cond_9b

    if-eqz p3, :cond_99

    new-instance v6, Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/DataPlusOne;-><init>()V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v6, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v6, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    move-object v11, v6

    .line 790
    :goto_7f
    if-eqz v11, :cond_dd

    .line 791
    invoke-static {v1, v11, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z

    .line 797
    :goto_84
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 798
    .local v10, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v10, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_92
    .catchall {:try_start_4b .. :try_end_92} :catchall_b8

    .line 800
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 801
    return-void

    .line 771
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v9           #cursor:Landroid/database/Cursor;
    .end local v10           #notifyUri:Landroid/net/Uri;
    .end local v11           #plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    :cond_96
    const-string v2, " (un)"

    goto :goto_1d

    .restart local v1       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v9       #cursor:Landroid/database/Cursor;
    .restart local v11       #plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    :cond_99
    move-object v11, v6

    .line 789
    goto :goto_7f

    :cond_9b
    if-eqz p3, :cond_bd

    const/4 v2, 0x1

    :try_start_9e
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    iget-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    iget-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    :cond_b4
    :goto_b4
    const/4 v2, 0x0

    iput-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;
    :try_end_b7
    .catchall {:try_start_9e .. :try_end_b7} :catchall_b8

    goto :goto_7f

    .line 800
    :catchall_b8
    move-exception v2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v2

    .line 789
    :cond_bd
    const/4 v2, 0x0

    :try_start_be
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    iget-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_b4

    iget-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    iget-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v11, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    goto :goto_b4

    .line 793
    :cond_dd
    invoke-static {v1, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_e0
    .catchall {:try_start_be .. :try_end_e0} :catchall_b8

    goto :goto_84
.end method

.method public static updatePhotoPlusOne$95d6774(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "plusOne"

    .prologue
    .line 811
    const-string v2, "EsPhotosData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 812
    const-string v2, "EsPhotosData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">> updatePlusOne; photo id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 818
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    if-eqz p3, :cond_39

    .line 819
    invoke-static {v0, p3, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertOrUpdatePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/DataPlusOne;Ljava/lang/String;)Z

    .line 825
    :goto_2a
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 826
    .local v1, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 827
    return-void

    .line 821
    .end local v1           #notifyUri:Landroid/net/Uri;
    :cond_39
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoPlusOneRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    goto :goto_2a
.end method

.method public static updatePhotoShapeApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJZ)V
    .registers 25
    .parameter "context"
    .parameter "account"
    .parameter "photoId"
    .parameter "shapeId"
    .parameter "approve"

    .prologue
    .line 840
    const-string v14, "EsPhotosData"

    const/4 v15, 0x3

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_2b

    .line 841
    const-string v14, "EsPhotosData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, ">> updatePhotoShape photo id: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v15, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", approved? "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p6

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    :cond_2b
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 846
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 849
    .local v8, startTime:J
    :try_start_37
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_10f

    .line 852
    const-wide/16 v4, 0x0

    .line 854
    .local v4, notificationCount:J
    :try_start_3c
    const-string v14, "SELECT notification_count FROM photo_home WHERE type = \'photos_of_me\'"

    const/4 v15, 0x0

    invoke-static {v2, v14, v15}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_42
    .catchall {:try_start_3c .. :try_end_42} :catchall_10f
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_3c .. :try_end_42} :catch_fc

    move-result-wide v4

    .line 862
    :cond_43
    :goto_43
    const-wide/16 v14, 0x0

    cmp-long v14, v4, v14

    if-lez v14, :cond_66

    .line 863
    :try_start_49
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 864
    .local v13, values:Landroid/content/ContentValues;
    const-string v14, "notification_count"

    const-wide/16 v15, 0x1

    sub-long v15, v4, v15

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 865
    const-string v14, "photo_home"

    const-string v15, "type = \'photos_of_me\'"

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v2, v14, v13, v15, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 869
    .end local v13           #values:Landroid/content/ContentValues;
    :cond_66
    if-nez p6, :cond_8f

    .line 870
    const/4 v14, 0x1

    new-array v10, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v10, v14
    :try_end_72
    .catchall {:try_start_49 .. :try_end_72} :catchall_10f

    .line 871
    .local v10, subjectArgs:[Ljava/lang/String;
    const/4 v11, 0x0

    .line 874
    .local v11, subjectGaiaId:Ljava/lang/String;
    :try_start_73
    const-string v14, "SELECT subject_id FROM photo_shape WHERE shape_id = ?"

    invoke-static {v2, v14, v10}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_78
    .catchall {:try_start_73 .. :try_end_78} :catchall_10f
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_73 .. :try_end_78} :catch_136

    move-result-object v11

    .line 881
    :goto_79
    if-eqz v11, :cond_8f

    .line 882
    const/4 v14, 0x2

    :try_start_7c
    new-array v12, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v11, v12, v14

    const/4 v14, 0x1

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v12, v14

    .line 884
    .local v12, userArgs:[Ljava/lang/String;
    const-string v14, "photos_of_user"

    const-string v15, "photo_of_user_id=? AND photo_id=?"

    invoke-virtual {v2, v14, v15, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 890
    .end local v10           #subjectArgs:[Ljava/lang/String;
    .end local v11           #subjectGaiaId:Ljava/lang/String;
    .end local v12           #userArgs:[Ljava/lang/String;
    :cond_8f
    const/4 v14, 0x1

    new-array v7, v14, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v7, v14

    .line 892
    .local v7, shapeArgs:[Ljava/lang/String;
    if-eqz p6, :cond_13e

    .line 893
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 894
    .restart local v13       #values:Landroid/content/ContentValues;
    const-string v14, "status"

    const-string v15, "ACCEPTED"

    invoke-virtual {v13, v14, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    const-string v14, "photo_shape"

    const-string v15, "shape_id = ? AND status = \'PENDING\'"

    invoke-virtual {v2, v14, v13, v15, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 900
    .end local v13           #values:Landroid/content/ContentValues;
    :goto_ae
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b1
    .catchall {:try_start_7c .. :try_end_b1} :catchall_10f

    .line 902
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 903
    const-string v14, "EsPhotosData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_d5

    .line 904
    const-string v14, "EsPhotosData"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "[UPDATE_SHAPE_APPROVAL], duration: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    :cond_d5
    sget-object v14, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-wide/from16 v0, p2

    invoke-static {v14, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 912
    .local v6, notifyUri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v6, v15}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 913
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_HOME_URI:Landroid/net/Uri;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 914
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    sget-object v15, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_URI:Landroid/net/Uri;

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 915
    return-void

    .line 855
    .end local v6           #notifyUri:Landroid/net/Uri;
    .end local v7           #shapeArgs:[Ljava/lang/String;
    :catch_fc
    move-exception v3

    .line 857
    .local v3, e:Landroid/database/sqlite/SQLiteDoneException;
    :try_start_fd
    const-string v14, "EsPhotosData"

    const/4 v15, 0x4

    invoke-static {v14, v15}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_43

    .line 858
    const-string v14, "EsPhotosData"

    const-string v15, "Notification count not found; have you sync\'d?"

    invoke-static {v14, v15, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_10d
    .catchall {:try_start_fd .. :try_end_10d} :catchall_10f

    goto/16 :goto_43

    .line 902
    .end local v3           #e:Landroid/database/sqlite/SQLiteDoneException;
    .end local v4           #notificationCount:J
    :catchall_10f
    move-exception v14

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 903
    const-string v15, "EsPhotosData"

    const/16 v16, 0x4

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v15

    if-eqz v15, :cond_135

    .line 904
    const-string v15, "EsPhotosData"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "[UPDATE_SHAPE_APPROVAL], duration: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_135
    throw v14

    .line 876
    .restart local v4       #notificationCount:J
    .restart local v10       #subjectArgs:[Ljava/lang/String;
    .restart local v11       #subjectGaiaId:Ljava/lang/String;
    :catch_136
    move-exception v3

    .line 878
    .restart local v3       #e:Landroid/database/sqlite/SQLiteDoneException;
    :try_start_137
    const-string v14, "EsPhotosData"

    invoke-static {v14, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_79

    .line 897
    .end local v3           #e:Landroid/database/sqlite/SQLiteDoneException;
    .end local v10           #subjectArgs:[Ljava/lang/String;
    .end local v11           #subjectGaiaId:Ljava/lang/String;
    .restart local v7       #shapeArgs:[Ljava/lang/String;
    :cond_13e
    const-string v14, "photo_shape"

    const-string v15, "shape_id = ? AND status = \'PENDING\'"

    invoke-virtual {v2, v14, v15, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_145
    .catchall {:try_start_137 .. :try_end_145} :catchall_10f

    goto/16 :goto_ae
.end method

.method public static updatePhotosOfYouCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter "cover"

    .prologue
    .line 1069
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1071
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 1072
    .local v7, startTime:J
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1075
    .local v3, notificationList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :try_start_11
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1077
    new-instance v9, Landroid/content/ContentValues;

    const/4 v10, 0x6

    invoke-direct {v9, v10}, Landroid/content/ContentValues;-><init>(I)V

    .line 1079
    .local v9, values:Landroid/content/ContentValues;
    const-string v10, "type"

    const-string v11, "photos_of_me"

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1080
    const-string v10, "sort_order"

    const/16 v11, 0x14

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1081
    const-string v10, "photo_count"

    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1084
    const-string v10, "photos_of_me"

    invoke-static {v1, v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getPhotosHomeRowId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v5

    .line 1085
    .local v5, rowId:J
    const-wide/16 v10, -0x1

    cmp-long v10, v5, v10

    if-eqz v10, :cond_ef

    .line 1086
    const-string v10, "photo_home"

    const-string v11, "type=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "photos_of_me"

    aput-object v14, v12, v13

    invoke-virtual {v1, v10, v9, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1095
    :goto_4c
    const-string v10, "photo_home_cover"

    const-string v11, "photo_home_key=?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v1, v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1100
    if-eqz p2, :cond_af

    .line 1101
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    .line 1102
    const-string v10, "photo_home_key"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1103
    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_7e

    .line 1104
    const-string v10, "photo_id"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1106
    :cond_7e
    const-string v10, "url"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    const-string v10, "width"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->width:Ljava/lang/Integer;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1108
    const-string v10, "height"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v11, v11, Lcom/google/api/services/plusi/model/DataImage;->height:Ljava/lang/Integer;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1109
    const-string v10, "size"

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/google/api/services/plusi/model/DataPhoto;->fileSize:Ljava/lang/Long;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1110
    const-string v10, "photo_home_cover"

    const/4 v11, 0x0

    const/4 v12, 0x4

    invoke-virtual {v1, v10, v11, v9, v12}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1114
    :cond_af
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_b2
    .catchall {:try_start_11 .. :try_end_b2} :catchall_f9

    .line 1116
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1117
    const-string v10, "EsPhotosData"

    const/4 v11, 0x4

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_d6

    .line 1118
    const-string v10, "EsPhotosData"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[INSERT_COVER_PHOTOS_OF_YOU], duration: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    :cond_d6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_da
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_11f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 1125
    .local v4, notifyUri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v4, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_da

    .line 1090
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v4           #notifyUri:Landroid/net/Uri;
    :cond_ef
    :try_start_ef
    const-string v10, "photo_home"

    const/4 v11, 0x0

    const/4 v12, 0x4

    invoke-virtual {v1, v10, v11, v9, v12}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J
    :try_end_f6
    .catchall {:try_start_ef .. :try_end_f6} :catchall_f9

    move-result-wide v5

    goto/16 :goto_4c

    .line 1116
    .end local v5           #rowId:J
    .end local v9           #values:Landroid/content/ContentValues;
    :catchall_f9
    move-exception v10

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1117
    const-string v11, "EsPhotosData"

    const/4 v12, 0x4

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_11e

    .line 1118
    const-string v11, "EsPhotosData"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[INSERT_COVER_PHOTOS_OF_YOU], duration: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->getDeltaTime(J)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11e
    throw v10

    .line 1127
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v5       #rowId:J
    .restart local v9       #values:Landroid/content/ContentValues;
    :cond_11f
    return-void
.end method
