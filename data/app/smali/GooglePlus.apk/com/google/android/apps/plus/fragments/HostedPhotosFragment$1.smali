.class final Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedPhotosFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 223
    return-void
.end method

.method public final onGetAlbumComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 205
    return-void
.end method

.method public final onGetPhotosOfUserComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 211
    return-void
.end method

.method public final onGetStreamPhotosComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 217
    return-void
.end method

.method public final onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 228
    .local p2, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p3}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 231
    return-void
.end method

.method public final onReadEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 199
    return-void
.end method
