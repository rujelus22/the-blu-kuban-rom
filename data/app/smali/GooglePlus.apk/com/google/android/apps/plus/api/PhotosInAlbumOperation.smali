.class public final Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PhotosInAlbumOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;",
        "Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mCollectionId:Ljava/lang/String;

.field private final mCoverOnly:Z

.field private mIsAlbum:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "collectionId"
    .parameter "ownerId"
    .parameter "coverOnly"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 56
    const-string v3, "photosinalbum"

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PhotosInAlbumResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 59
    iput-object p4, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    .line 60
    iput-object p5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    .line 61
    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCoverOnly:Z

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    .line 65
    :try_start_1b
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mIsAlbum:Z
    :try_end_23
    .catch Ljava/lang/NumberFormatException; {:try_start_1b .. :try_end_23} :catch_24

    .line 70
    :goto_23
    return-void

    :catch_24
    move-exception v0

    goto :goto_23
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "collectionId"
    .parameter "ownerId"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 49
    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, p5

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 13
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->onStartResultProcessing()V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->photo:Ljava/util/List;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v5, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumResponse;->isDownloadable:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    const-string v1, "camerasync"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2b

    if-eqz v4, :cond_3f

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3f

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPhoto;

    :goto_24
    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->updateInstantUploadCover(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataPhoto;)V

    :cond_2b
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCoverOnly:Z

    if-nez v0, :cond_3e

    if-eqz v3, :cond_3e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mIsAlbum:Z

    if-eqz v0, :cond_41

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataAlbum;Ljava/util/List;Ljava/lang/Boolean;)V

    :cond_3e
    :goto_3e
    return-void

    :cond_3f
    const/4 v0, 0x0

    goto :goto_24

    :cond_41
    const-string v0, "CAMERA_SYNC"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5a

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "camerasync"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_3e

    :cond_5a
    const-string v0, "UPDATES_ALBUMS"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_73

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "posts"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_3e

    :cond_73
    const-string v0, "PROFILE_PHOTOS"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8c

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "profile"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_3e

    :cond_8c
    const-string v0, "BUNCH_ALBUMS"

    iget-object v1, v3, Lcom/google/api/services/plusi/model/DataAlbum;->albumType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    iget-object v5, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const-string v8, "messenger"

    iget-object v9, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    move-object v10, v4

    invoke-static/range {v5 .. v10}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_3e
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 7
    .parameter "x0"

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 27
    check-cast p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;

    .end local p1
    new-instance v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;-><init>()V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnAlbumInfo:Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnComments:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnDownloadability:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2a

    move v0, v1

    :cond_2a
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnOwnerInfo:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPhotos:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPlusOnes:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnShapes:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnVideoUrls:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCollectionId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->collectionId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->ownerId:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;->mCoverOnly:Z

    if-eqz v0, :cond_5c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->maxResults:Ljava/lang/Integer;

    :cond_5c
    return-void
.end method
