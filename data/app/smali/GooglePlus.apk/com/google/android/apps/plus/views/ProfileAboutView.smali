.class public Lcom/google/android/apps/plus/views/ProfileAboutView;
.super Lcom/google/android/apps/plus/views/EsScrollView;
.source "ProfileAboutView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$Item;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;
    }
.end annotation


# static fields
.field private static final SCRAPBOOK_PHOTO_IDS:[I

.field private static final sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

.field private static sPlusOneStandardTextColor:I

.field private static sPlusOneTextSize:F

.field private static sPlusOnedByMeTextColor:I


# instance fields
.field private mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

.field private mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

.field private final mInflater:Landroid/view/LayoutInflater;

.field mIsExpanded:Z

.field private mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

.field private mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

.field private mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 366
    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    .line 652
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/google/android/apps/plus/views/ProfileAboutView;->SCRAPBOOK_PHOTO_IDS:[I

    return-void

    :array_10
    .array-data 0x4
        0xcet 0x1t 0x9t 0x7ft
        0xcft 0x1t 0x9t 0x7ft
        0xd0t 0x1t 0x9t 0x7ft
        0xd1t 0x1t 0x9t 0x7ft
        0xd2t 0x1t 0x9t 0x7ft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    .prologue
    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;)V

    .line 372
    sget-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    .line 376
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    .line 393
    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    if-nez v1, :cond_3f

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 395
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    .line 396
    const v1, 0x7f0a00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    .line 397
    const v1, 0x7f0d0132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    .line 380
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_3f
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 383
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 372
    sget-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    .line 376
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    .line 393
    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    if-nez v1, :cond_3f

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 395
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    .line 396
    const v1, 0x7f0a00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    .line 397
    const v1, 0x7f0d0132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    .line 384
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_3f
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 387
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 372
    sget-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    .line 376
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    .line 393
    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    if-nez v1, :cond_3f

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 395
    .local v0, res:Landroid/content/res/Resources;
    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    .line 396
    const v1, 0x7f0a00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    .line 397
    const v1, 0x7f0d0132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    .line 388
    .end local v0           #res:Landroid/content/res/Resources;
    :cond_3f
    return-void
.end method

.method private addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V
    .registers 12
    .parameter "review"
    .parameter "parent"
    .parameter "reviewType"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1206
    const v2, 0x7f09008a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1207
    .local v0, container:Landroid/widget/LinearLayout;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03009b

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1208
    .local v1, view:Landroid/view/View;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    const v2, 0x7f09011f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/LocalReviewListItemView;

    if-nez v6, :cond_49

    move v3, v4

    :goto_24
    if-nez v3, :cond_4b

    move v3, v4

    :goto_27
    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setTopBorderVisible(Z)V

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setAuthorAvatarOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Integer;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1209
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1210
    return-void

    :cond_49
    move v3, v5

    .line 1208
    goto :goto_24

    :cond_4b
    move v3, v5

    goto :goto_27
.end method

.method private bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "view"
    .parameter "iconResId"
    .parameter "text"
    .parameter "label"

    .prologue
    .line 1334
    const v3, 0x1020006

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1335
    .local v0, iconView:Landroid/widget/ImageView;
    if-eqz p2, :cond_2e

    .line 1336
    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1340
    :goto_e
    const v3, 0x1020014

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1341
    .local v1, text1:Landroid/widget/TextView;
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1342
    const v3, 0x1020015

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1343
    .local v2, text2:Landroid/widget/TextView;
    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1344
    invoke-static {p1, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1345
    return-void

    .line 1338
    .end local v1           #text1:Landroid/widget/TextView;
    .end local v2           #text2:Landroid/widget/TextView;
    :cond_2e
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_e
.end method

.method private bindExpandArea()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1274
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showExpandButtonText:Z

    if-eqz v2, :cond_15

    .line 1275
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v2, :cond_24

    const v1, 0x7f0801d5

    .line 1276
    .local v1, label:I
    :goto_e
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1278
    .end local v1           #label:I
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v2, :cond_28

    const v0, 0x7f020157

    .line 1280
    .local v0, drawable:I
    :goto_1c
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v2, v3, v3, v0, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1281
    return-void

    .line 1275
    .end local v0           #drawable:I
    :cond_24
    const v1, 0x7f0801d4

    goto :goto_e

    .line 1278
    :cond_28
    const v0, 0x7f020155

    goto :goto_1c
.end method

.method private bindIntroductionView(Landroid/view/View;Ljava/lang/String;)V
    .registers 9
    .parameter "view"
    .parameter "value"

    .prologue
    const/4 v5, 0x0

    .line 1295
    move-object v2, p1

    check-cast v2, Landroid/widget/TextView;

    .line 1296
    .local v2, textView:Landroid/widget/TextView;
    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;)V

    invoke-static {p2, v3, v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    .line 1297
    .local v1, text:Landroid/text/Spanned;
    :goto_e
    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v3

    if-lez v3, :cond_2a

    invoke-interface {v1, v5}, Landroid/text/Spanned;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_2a

    .line 1298
    const/4 v3, 0x1

    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v4

    invoke-interface {v1, v3, v4}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .end local v1           #text:Landroid/text/Spanned;
    check-cast v1, Landroid/text/Spanned;

    .restart local v1       #text:Landroid/text/Spanned;
    goto :goto_e

    .line 1300
    :cond_2a
    :goto_2a
    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v3

    if-lez v3, :cond_4d

    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Landroid/text/Spanned;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_4d

    .line 1301
    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v5, v3}, Landroid/text/Spanned;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .end local v1           #text:Landroid/text/Spanned;
    check-cast v1, Landroid/text/Spanned;

    .restart local v1       #text:Landroid/text/Spanned;
    goto :goto_2a

    .line 1303
    :cond_4d
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1304
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1305
    invoke-virtual {v2}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v0

    .line 1306
    .local v0, mm:Landroid/text/method/MovementMethod;
    instance-of v3, v0, Landroid/text/method/LinkMovementMethod;

    if-nez v3, :cond_62

    .line 1307
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1309
    :cond_62
    return-void
.end method

.method private bindLinkView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "view"
    .parameter "label"
    .parameter "favIconUrl"
    .parameter "type"

    .prologue
    .line 1384
    const v1, 0x1020006

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    .line 1385
    const v1, 0x1020014

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1386
    const v1, 0x1020015

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1387
    .local v0, typeView:Landroid/widget/TextView;
    if-eqz p4, :cond_32

    .line 1388
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1389
    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1390
    invoke-static {p1, p2, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1395
    :goto_31
    return-void

    .line 1392
    :cond_32
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1393
    invoke-virtual {p1, p2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_31
.end method

.method private bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V
    .registers 4
    .parameter "view"
    .parameter "resourceId"

    .prologue
    .line 1287
    invoke-virtual {p1, p2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(I)V

    .line 1288
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1289
    return-void
.end method

.method private enableAvatarChangePhotoIcon(Z)V
    .registers 4
    .parameter "enabled"

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarChoosePhotoIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_b

    const/4 v0, 0x0

    :goto_7
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 620
    return-void

    .line 619
    :cond_b
    const/16 v0, 0x8

    goto :goto_7
.end method

.method private static enableDivider(Landroid/view/View;Z)V
    .registers 4
    .parameter "view"
    .parameter "enabled"

    .prologue
    .line 771
    const v1, 0x7f090061

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 772
    .local v0, divider:Landroid/view/View;
    if-eqz v0, :cond_f

    .line 773
    if-eqz p1, :cond_10

    const/4 v1, 0x0

    :goto_c
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 775
    :cond_f
    return-void

    .line 773
    :cond_10
    const/16 v1, 0x8

    goto :goto_c
.end method

.method private getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;
    .registers 10
    .parameter "parent"
    .parameter "view"
    .parameter "layoutResId"
    .parameter "labelResId"
    .parameter "value"

    .prologue
    .line 1352
    if-nez p2, :cond_9

    .line 1353
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const/4 v3, 0x0

    invoke-virtual {v2, p3, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1355
    :cond_9
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1356
    .local v0, text1:Landroid/widget/TextView;
    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1357
    const v2, 0x1020015

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1358
    .local v1, text2:Landroid/widget/TextView;
    invoke-direct {p0, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1359
    invoke-direct {p0, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p5, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1360
    return-object p2
.end method

.method private getString(I)Ljava/lang/String;
    .registers 3
    .parameter "resourceId"

    .prologue
    .line 1256
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initProfileLayout()V
    .registers 4

    .prologue
    .line 402
    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    .line 405
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoToLoading()V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setAvatarToLoading()V

    .line 408
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 410
    return-void
.end method

.method private static setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .registers 5
    .parameter "view"
    .parameter "text1"
    .parameter "text2"

    .prologue
    .line 1262
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1263
    .local v0, contentDescription:Ljava/lang/StringBuilder;
    if-eqz p2, :cond_15

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_15

    .line 1264
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1265
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1267
    :cond_15
    if-eqz p1, :cond_20

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_20

    .line 1268
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1270
    :cond_20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1271
    return-void
.end method

.method private updateGenericListSectionDividers(Landroid/view/ViewGroup;)V
    .registers 7
    .parameter "group"

    .prologue
    .line 1057
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1058
    .local v0, count:I
    if-nez v0, :cond_7

    .line 1075
    :goto_6
    return-void

    .line 1063
    :cond_7
    const/4 v1, 0x0

    .line 1066
    .local v1, i:I
    :goto_8
    add-int/lit8 v2, v1, 0x1

    .end local v1           #i:I
    .local v2, i:I
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1067
    .local v3, view:Landroid/view/View;
    if-eq v2, v0, :cond_16

    .line 1068
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    move v1, v2

    .line 1072
    .end local v2           #i:I
    .restart local v1       #i:I
    goto :goto_8

    .line 1074
    .end local v1           #i:I
    .restart local v2       #i:I
    :cond_16
    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    goto :goto_6
.end method


# virtual methods
.method public final addAddress(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "address"
    .parameter "type"

    .prologue
    const/4 v2, 0x0

    .line 866
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03009f

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 868
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_3d

    const/4 v1, 0x1

    :goto_19
    if-eqz v1, :cond_3f

    const v1, 0x7f02019e

    :goto_1e
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 869
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 871
    const v1, 0x7f0901c5

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 872
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 873
    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 874
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 875
    return-void

    :cond_3d
    move v1, v2

    .line 868
    goto :goto_19

    :cond_3f
    move v1, v2

    goto :goto_1e
.end method

.method public final addCircleReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .registers 5
    .parameter "review"

    .prologue
    .line 1195
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v2, 0x7f0901c0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1196
    .local v0, parent:Landroid/view/View;
    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V

    .line 1197
    return-void
.end method

.method public final addEmail(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "email"
    .parameter "type"

    .prologue
    const/4 v2, 0x0

    .line 823
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0300a3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 825
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_3d

    const/4 v1, 0x1

    :goto_19
    if-eqz v1, :cond_3f

    const v1, 0x7f0201a1

    :goto_1e
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 826
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 828
    const v1, 0x7f0901c3

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 829
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 830
    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 831
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 832
    return-void

    :cond_3d
    move v1, v2

    .line 825
    goto :goto_19

    :cond_3f
    move v1, v2

    goto :goto_1e
.end method

.method public final addLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "url"
    .parameter "label"
    .parameter "favIconUrl"
    .parameter "type"

    .prologue
    const/4 v4, 0x0

    .line 1102
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030098

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1103
    .local v0, view:Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_37

    .end local p4
    :goto_18
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindLinkView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1106
    const v1, 0x7f0901be

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 1107
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1108
    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1109
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1110
    return-void

    .line 1103
    .restart local p4
    :cond_37
    const/4 p4, 0x0

    goto :goto_18
.end method

.method public final addLocalReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .registers 5
    .parameter "review"

    .prologue
    .line 1200
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v2, 0x7f0901c1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1201
    .local v0, parent:Landroid/view/View;
    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V

    .line 1202
    return-void
.end method

.method public final addLocation(Ljava/lang/String;Z)V
    .registers 9
    .parameter "address"
    .parameter "isCurrent"

    .prologue
    const/4 v3, 0x0

    .line 1045
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03009d

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v5, v5, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1046
    .local v1, view:Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1048
    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;-><init>(Ljava/lang/String;Z)V

    .line 1049
    .local v0, item:Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;
    const v2, 0x1020006

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->current:Z

    if-eqz v2, :cond_4b

    move v2, v3

    :goto_26
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x1020014

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->address:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->address:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1051
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1052
    new-instance v2, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1053
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1054
    return-void

    .line 1049
    :cond_4b
    const/4 v2, 0x4

    goto :goto_26
.end method

.method public final addPhoneNumber(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 12
    .parameter "phoneNumber"
    .parameter "type"
    .parameter "isSmsIntentRegistered"

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x0

    .line 839
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f0300a0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 840
    .local v2, view:Landroid/view/View;
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-nez v3, :cond_5f

    const/4 v3, 0x1

    :goto_1b
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_61

    const v3, 0x7f0201a8

    :goto_24
    invoke-direct {p0, v2, v3, v5, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    .line 841
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 843
    const v3, 0x7f0901c4

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 844
    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 845
    new-instance v3, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 846
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 848
    const v3, 0x7f090203

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 849
    .local v1, sendSmsButton:Landroid/view/View;
    const v3, 0x7f090204

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 850
    .local v0, divider:Landroid/view/View;
    if-eqz p3, :cond_63

    .line 851
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 852
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 853
    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 854
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 859
    :goto_5e
    return-void

    .end local v0           #divider:Landroid/view/View;
    .end local v1           #sendSmsButton:Landroid/view/View;
    :cond_5f
    move v3, v4

    .line 840
    goto :goto_1b

    :cond_61
    move v3, v4

    goto :goto_24

    .line 856
    .restart local v0       #divider:Landroid/view/View;
    .restart local v1       #sendSmsButton:Landroid/view/View;
    :cond_63
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 857
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5e
.end method

.method public final addYourReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .registers 5
    .parameter "review"

    .prologue
    .line 1190
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v2, 0x7f0901bf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1191
    .local v0, parent:Landroid/view/View;
    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V

    .line 1192
    return-void
.end method

.method public final clearAddresses()V
    .registers 2

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 863
    return-void
.end method

.method public final clearAllReviews()V
    .registers 6

    .prologue
    const v4, 0x7f09008a

    .line 1176
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901bf

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1177
    .local v1, parent:Landroid/view/View;
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1178
    .local v0, container:Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1180
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901c0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1181
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #container:Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 1182
    .restart local v0       #container:Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1184
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901c1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1185
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0           #container:Landroid/widget/LinearLayout;
    check-cast v0, Landroid/widget/LinearLayout;

    .line 1186
    .restart local v0       #container:Landroid/widget/LinearLayout;
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1187
    return-void
.end method

.method public final clearEmails()V
    .registers 2

    .prologue
    .line 819
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 820
    return-void
.end method

.method public final clearLinks()V
    .registers 2

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1099
    return-void
.end method

.method public final clearLocations()V
    .registers 2

    .prologue
    .line 1041
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1042
    return-void
.end method

.method public final clearPhoneNumbers()V
    .registers 2

    .prologue
    .line 835
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 836
    return-void
.end method

.method public final enableContactSection(Z)V
    .registers 5
    .parameter "enabled"

    .prologue
    .line 778
    if-eqz p1, :cond_1e

    .line 779
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 781
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 783
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v1, 0x7f0801da

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 787
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_1d
    return-void

    .line 785
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1d
.end method

.method public final enableHompageSection(Z)V
    .registers 6
    .parameter "enabled"

    .prologue
    .line 1156
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901bc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1157
    .local v1, view:Landroid/view/View;
    if-eqz p1, :cond_21

    const/4 v2, 0x0

    :goto_e
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1159
    const v2, 0x7f0901bd

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1161
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f08024c

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1162
    return-void

    .line 1157
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :cond_21
    const/16 v2, 0x8

    goto :goto_e
.end method

.method public final enableLinksSection(Z)V
    .registers 5
    .parameter "enabled"

    .prologue
    .line 1082
    if-eqz p1, :cond_1e

    .line 1083
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1085
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1087
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v1, 0x7f08024c

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1091
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_1d
    return-void

    .line 1089
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1d
.end method

.method public final enableLocalDetailsSection(Z)V
    .registers 6
    .parameter "enabled"

    .prologue
    .line 1136
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901b9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1137
    .local v1, view:Landroid/view/View;
    if-eqz p1, :cond_23

    const/4 v2, 0x0

    :goto_e
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1138
    if-eqz p1, :cond_22

    .line 1139
    const v2, 0x7f0901ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1141
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f0803aa

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1143
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :cond_22
    return-void

    .line 1137
    :cond_23
    const/16 v2, 0x8

    goto :goto_e
.end method

.method public final enableLocalEditorialReviewsSection(Z)V
    .registers 5
    .parameter "enabled"

    .prologue
    .line 1123
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v2, 0x7f0901b8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1124
    .local v0, view:Landroid/view/View;
    if-eqz p1, :cond_12

    const/4 v1, 0x0

    :goto_e
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1125
    return-void

    .line 1124
    :cond_12
    const/16 v1, 0x8

    goto :goto_e
.end method

.method public final enableLocalReviewsSection(Z)V
    .registers 6
    .parameter "enabled"

    .prologue
    .line 1240
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901c1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1241
    .local v1, view:Landroid/view/View;
    if-eqz p1, :cond_21

    .line 1242
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1244
    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1246
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f0803a7

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1250
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_20
    return-void

    .line 1248
    :cond_21
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_20
.end method

.method public final enableLocalYourActivitySection(Z)V
    .registers 6
    .parameter "enabled"

    .prologue
    .line 1213
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901bf

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1214
    .local v1, view:Landroid/view/View;
    if-eqz p1, :cond_21

    .line 1215
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1217
    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1219
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f0803a8

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1223
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_20
    return-void

    .line 1221
    :cond_21
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_20
.end method

.method public final enableLocalYourCirclesActivitySection(Z)V
    .registers 6
    .parameter "enabled"

    .prologue
    .line 1226
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901c0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1227
    .local v1, view:Landroid/view/View;
    if-eqz p1, :cond_21

    .line 1228
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1230
    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1232
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f0803a9

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1237
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_20
    return-void

    .line 1235
    :cond_21
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_20
.end method

.method public final enableLocationsSection(Z)V
    .registers 5
    .parameter "enabled"

    .prologue
    .line 1025
    if-eqz p1, :cond_1e

    .line 1026
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1028
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 1030
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v1, 0x7f08024b

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 1034
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_1d
    return-void

    .line 1032
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1d
.end method

.method public final enablePersonalSection(Z)V
    .registers 5
    .parameter "enabled"

    .prologue
    .line 878
    if-eqz p1, :cond_1e

    .line 879
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 881
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 883
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v1, 0x7f080243

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 887
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_1d
    return-void

    .line 885
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1d
.end method

.method public final enableWorkEducationSection(Z)V
    .registers 5
    .parameter "enabled"

    .prologue
    .line 951
    if-eqz p1, :cond_1e

    .line 952
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 954
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    const v2, 0x7f0901b5

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 956
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v1, 0x7f080248

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 960
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    :goto_1d
    return-void

    .line 958
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1d
.end method

.method public final init(Z)V
    .registers 4
    .parameter "isExpanded"

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    if-nez v0, :cond_7

    .line 414
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->initProfileLayout()V

    .line 417
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 425
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 426
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter "v"

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 438
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 440
    .local v1, id:I
    const v5, 0x7f0901e2

    if-ne v1, v5, :cond_2a

    .line 441
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v5, :cond_2f

    .line 442
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v5, v5, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 443
    iput-boolean v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 444
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    .line 445
    const v5, 0x7f0803e8

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 452
    :goto_27
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->requestLayout()V

    .line 455
    :cond_2a
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    if-nez v5, :cond_46

    .line 530
    :goto_2e
    return-void

    .line 447
    :cond_2f
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v5, v5, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 448
    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 449
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    .line 450
    const v5, 0x7f0803e9

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_27

    .line 459
    :cond_46
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    sparse-switch v5, :sswitch_data_112

    goto :goto_2e

    .line 511
    :sswitch_4e
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Integer;

    aget-object v5, v5, v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 512
    .local v4, reviewType:I
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/Integer;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 513
    .local v3, reviewIndex:I
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v5, v4, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalReviewClicked(II)V

    goto :goto_2e

    .line 461
    .end local v3           #reviewIndex:I
    .end local v4           #reviewType:I
    :sswitch_6c
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onAvatarClicked()V

    goto :goto_2e

    .line 465
    :sswitch_72
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;

    .line 466
    .local v2, location:Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iget-object v6, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->address:Ljava/lang/String;

    invoke-interface {v5, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocationClicked(Ljava/lang/String;)V

    goto :goto_2e

    .line 470
    .end local v2           #location:Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;
    :sswitch_80
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEmailClicked(Ljava/lang/String;)V

    goto :goto_2e

    .line 474
    :sswitch_8c
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onPhoneNumberClicked(Ljava/lang/String;)V

    goto :goto_2e

    .line 478
    :sswitch_98
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onSendTextClicked(Ljava/lang/String;)V

    goto :goto_2e

    .line 482
    :sswitch_a4
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onAddressClicked(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 486
    :sswitch_b1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLinkClicked(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 491
    :sswitch_be
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCirclesButtonClicked()V

    goto/16 :goto_2e

    .line 495
    :sswitch_c5
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalMapClicked(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 499
    :sswitch_d2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalDirectionsClicked(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 503
    :sswitch_df
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalCallClicked(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 507
    :sswitch_ec
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onZagatExplanationClicked()V

    goto/16 :goto_2e

    :sswitch_f3
    move-object v0, p1

    .line 517
    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    .line 518
    .local v0, authorAvatar:Lcom/google/android/apps/plus/views/AvatarView;
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onReviewAuthorAvatarClicked(Ljava/lang/String;)V

    goto/16 :goto_2e

    .line 522
    .end local v0           #authorAvatar:Lcom/google/android/apps/plus/views/AvatarView;
    :sswitch_101
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onPlusOneClicked()V

    goto/16 :goto_2e

    .line 526
    :sswitch_108
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    invoke-interface {v5, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onExpandClicked(Z)V

    goto/16 :goto_2e

    .line 459
    nop

    :sswitch_data_112
    .sparse-switch
        0x7f09011f -> :sswitch_4e
        0x7f09012d -> :sswitch_6c
        0x7f0901be -> :sswitch_b1
        0x7f0901c3 -> :sswitch_80
        0x7f0901c4 -> :sswitch_8c
        0x7f0901c5 -> :sswitch_a4
        0x7f0901dd -> :sswitch_72
        0x7f0901df -> :sswitch_be
        0x7f0901e0 -> :sswitch_be
        0x7f0901e1 -> :sswitch_101
        0x7f0901e2 -> :sswitch_108
        0x7f0901e3 -> :sswitch_c5
        0x7f0901e5 -> :sswitch_d2
        0x7f0901e7 -> :sswitch_df
        0x7f0901f0 -> :sswitch_f3
        0x7f0901ff -> :sswitch_ec
        0x7f090203 -> :sswitch_98
    .end sparse-switch
.end method

.method protected onFinishInflate()V
    .registers 2

    .prologue
    .line 430
    invoke-super {p0}, Lcom/google/android/apps/plus/views/EsScrollView;->onFinishInflate()V

    .line 431
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->initProfileLayout()V

    .line 432
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setVerticalFadingEdgeEnabled(Z)V

    .line 433
    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setFadingEdgeLength(I)V

    .line 434
    return-void
.end method

.method public onRecycle()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 534
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    if-eqz v0, :cond_5b

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto1:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto2:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto3:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto4:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto5:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EsImageView;->onRecycle()V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 552
    :cond_5b
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    .line 553
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    .line 554
    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    .line 555
    return-void
.end method

.method public setAddedByCount(Ljava/lang/Integer;)V
    .registers 10
    .parameter "count"

    .prologue
    const/4 v7, 0x0

    .line 607
    if-eqz p1, :cond_32

    .line 608
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 609
    .local v0, formatter:Ljava/text/NumberFormat;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e001f

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 611
    .local v1, text:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 612
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    .end local v0           #formatter:Ljava/text/NumberFormat;
    .end local v1           #text:Ljava/lang/String;
    :goto_31
    return-void

    .line 614
    :cond_32
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_31
.end method

.method public setAvatarToDefault(Z)V
    .registers 4
    .parameter "enableChoosePhotoIcon"

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 629
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableAvatarChangePhotoIcon(Z)V

    .line 630
    return-void
.end method

.method public setAvatarToLoading()V
    .registers 3

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    const v1, 0x7f0201a0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setImageResource(I)V

    .line 624
    return-void
.end method

.method public setAvatarUrl(Ljava/lang/String;Z)V
    .registers 4
    .parameter "url"
    .parameter "enableChoosePhotoIcon"

    .prologue
    .line 633
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    .line 634
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/EsImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 635
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableAvatarChangePhotoIcon(Z)V

    .line 636
    return-void
.end method

.method public setBirthday(Ljava/lang/String;)V
    .registers 10
    .parameter "birthday"

    .prologue
    const/16 v7, 0x3e9

    .line 930
    if-eqz p1, :cond_4b

    .line 932
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-nez v0, :cond_16

    .line 933
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    .line 935
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    const v3, 0x7f0300a2

    const v4, 0x7f080247

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;

    move-result-object v6

    .line 937
    .local v6, view:Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-nez v0, :cond_42

    .line 938
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v6, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    .line 939
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 940
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 942
    :cond_42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 948
    .end local v6           #view:Landroid/view/View;
    :cond_4a
    :goto_4a
    return-void

    .line 944
    :cond_4b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-eqz v0, :cond_4a

    .line 945
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4a
.end method

.method public setCircles(Ljava/util/ArrayList;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, circleNames:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v2, 0x8

    .line 1582
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1583
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CirclesButton;->setCircles(Ljava/util/ArrayList;)V

    .line 1584
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1585
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1586
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1587
    return-void
.end method

.method public setCoverPhotoToDefault()V
    .registers 3

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    const v1, 0x7f020066

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setImageResource(I)V

    .line 650
    return-void
.end method

.method public setCoverPhotoToLoading()V
    .registers 3

    .prologue
    .line 644
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EsImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 645
    return-void
.end method

.method public setCoverPhotoUrl(Ljava/lang/String;)V
    .registers 3
    .parameter "url"

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/EsImageView;->setUrl(Ljava/lang/String;)V

    .line 640
    return-void
.end method

.method public setDisplayPolicies(Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;)V
    .registers 6
    .parameter "policy"

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 558
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    .line 560
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->hideButtons:Z

    if-eqz v2, :cond_2a

    .line 561
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->buttons:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 566
    :goto_12
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showDetailsAlways:Z

    if-eqz v2, :cond_32

    .line 567
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 568
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    .line 571
    :goto_23
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 574
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->requestLayout()V

    .line 575
    return-void

    .line 563
    :cond_2a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->buttons:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_12

    .line 570
    :cond_32
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    .line 571
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v3, :cond_3f

    move-object v1, v2

    goto :goto_23

    :cond_3f
    move v0, v1

    move-object v1, v2

    goto :goto_23
.end method

.method public setEducation(Ljava/lang/String;)V
    .registers 5
    .parameter "institution"

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 684
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    if-eqz v0, :cond_2b

    .line 688
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 695
    :goto_2a
    return-void

    .line 690
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2a

    .line 693
    :cond_35
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2a
.end method

.method public setEducationLocations(Ljava/lang/String;)V
    .registers 10
    .parameter "locations"

    .prologue
    const/16 v7, 0x3eb

    .line 1004
    if-eqz p1, :cond_4b

    .line 1005
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    if-nez v0, :cond_16

    .line 1006
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    .line 1008
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    const v3, 0x7f03009e

    const v4, 0x7f08024a

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;

    move-result-object v6

    .line 1010
    .local v6, view:Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    if-nez v0, :cond_42

    .line 1011
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v6, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    .line 1012
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 1013
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1015
    :cond_42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1022
    .end local v6           #view:Landroid/view/View;
    :cond_4a
    :goto_4a
    return-void

    .line 1018
    :cond_4b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    if-eqz v0, :cond_4a

    .line 1019
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4a
.end method

.method public setEmployer(Ljava/lang/String;)V
    .registers 5
    .parameter "employer"

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 670
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 671
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    if-eqz v0, :cond_2b

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 681
    :goto_2a
    return-void

    .line 676
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2a

    .line 679
    :cond_35
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2a
.end method

.method public setEmploymentLocations(Ljava/lang/String;)V
    .registers 10
    .parameter "locations"

    .prologue
    const/16 v7, 0x3ea

    .line 983
    if-eqz p1, :cond_4b

    .line 984
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    if-nez v0, :cond_16

    .line 985
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    .line 987
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    const v3, 0x7f03009e

    const v4, 0x7f080249

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;

    move-result-object v6

    .line 989
    .local v6, view:Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    if-nez v0, :cond_42

    .line 990
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v6, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    .line 991
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 992
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workEducationSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 994
    :cond_42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1001
    .end local v6           #view:Landroid/view/View;
    :cond_4a
    :goto_4a
    return-void

    .line 997
    :cond_4b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    if-eqz v0, :cond_4a

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4a
.end method

.method public setGender(Ljava/lang/String;)V
    .registers 10
    .parameter "gender"

    .prologue
    const/16 v7, 0x3e8

    .line 910
    if-eqz p1, :cond_4b

    .line 911
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-nez v0, :cond_16

    .line 912
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    .line 914
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    const v3, 0x7f0300a2

    const v4, 0x7f080244

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;

    move-result-object v6

    .line 916
    .local v6, view:Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-nez v0, :cond_42

    .line 917
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v6, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    .line 918
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 921
    :cond_42
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 927
    .end local v6           #view:Landroid/view/View;
    :cond_4a
    :goto_4a
    return-void

    .line 923
    :cond_4b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-eqz v0, :cond_4a

    .line 924
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4a
.end method

.method public setHomepage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "url"
    .parameter "text"
    .parameter "favIconUrl"

    .prologue
    const/4 v4, 0x0

    .line 1165
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901bc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1166
    .local v0, parent:Landroid/view/View;
    const v2, 0x7f0901be

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1167
    .local v1, view:Landroid/view/View;
    const/4 v2, 0x0

    invoke-direct {p0, v1, p2, p3, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindLinkView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 1170
    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1171
    new-instance v2, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1172
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1173
    return-void
.end method

.method public setIntroduction(Ljava/lang/String;)V
    .registers 6
    .parameter "intro"

    .prologue
    .line 756
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    .line 757
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 759
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    const v3, 0x7f0901b5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 761
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f0801d9

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 763
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    const v3, 0x7f09008a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 764
    .local v1, view:Landroid/view/View;
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindIntroductionView(Landroid/view/View;Ljava/lang/String;)V

    .line 768
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    .end local v1           #view:Landroid/view/View;
    :goto_2f
    return-void

    .line 766
    :cond_30
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2f
.end method

.method public setIsExpanded(Z)V
    .registers 2
    .parameter "isExpanded"

    .prologue
    .line 736
    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    .line 737
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    .line 738
    return-void
.end method

.method public setLocalActions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "fullName"
    .parameter "phone"
    .parameter "mapsCid"
    .parameter "localPageAddress"

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 1114
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v3, 0x7f0901b7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1115
    .local v1, view:Landroid/view/View;
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1117
    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;

    invoke-direct {v0, p2, p1, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    .local v0, localActionsItem:Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;
    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->mapsCid:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://maps.google.com/maps?cid="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v2, :cond_38

    const-string v4, "&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->address:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "http://maps.google.com/maps?daddr="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_56

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_56
    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0901e3

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0901e5

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0901e6

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0901e7

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->phone:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a0

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->phone:Ljava/lang/String;

    invoke-virtual {v7, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1120
    :goto_9f
    return-void

    .line 1119
    :cond_a0
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_9f
.end method

.method public setLocalDetails(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter
    .parameter "phone"
    .parameter "openingHoursSummary"
    .parameter "openingHoursFull"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1147
    .local p1, knownForTerms:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v4, 0x7f0901b9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1148
    .local v1, parent:Landroid/view/View;
    const v3, 0x7f0901bb

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1149
    .local v2, view:Landroid/view/View;
    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    .local v0, item:Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;
    const v3, 0x7f0901e9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const v3, 0x7f0901eb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const v3, 0x7f0901ed

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->knownForTerms:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_61

    const/4 v3, 0x1

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->knownForTerms:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v3

    :goto_41
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_61

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_5e

    if-nez v4, :cond_5a

    const-string v4, " \u00b7 "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5a
    const/4 v4, 0x0

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5e
    move v3, v4

    move v4, v3

    goto :goto_41

    :cond_61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_b3

    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0901ea

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_77
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->phone:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b9

    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0901ec

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->phone:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_91
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursFull:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_bf

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursFull:Ljava/lang/String;

    move-object v4, v3

    :goto_9c
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c3

    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    const v3, 0x7f0901ee

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1153
    :goto_b2
    return-void

    .line 1152
    :cond_b3
    const/16 v3, 0x8

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_77

    :cond_b9
    const/16 v3, 0x8

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_91

    :cond_bf
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursSummary:Ljava/lang/String;

    move-object v4, v3

    goto :goto_9c

    :cond_c3
    const/16 v3, 0x8

    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_b2
.end method

.method public setLocalEditorialReviews(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 18
    .parameter "scores"
    .parameter "reviewText"
    .parameter "priceLabel"
    .parameter "priceValue"
    .parameter "reviewCount"

    .prologue
    .line 1129
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const v2, 0x7f0901b8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 1130
    .local v6, view:Landroid/view/View;
    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;-><init>(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1132
    .local v0, item:Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;
    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    const-string v2, "ZAGAT_OFFICIAL"

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->source:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    const v1, 0x7f0901f6

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v1, 0x7f0901f7

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/4 v1, 0x4

    new-array v9, v1, [Landroid/view/View;

    const/4 v1, 0x0

    const v2, 0x7f0901f8

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x1

    const v2, 0x7f0901fb

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x2

    const v2, 0x7f0901fc

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x3

    const v2, 0x7f0901fd

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const v1, 0x7f090200

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0901fe

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v7, :cond_c4

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/16 v3, 0x8

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_7f
    const/4 v3, 0x0

    move v5, v3

    :goto_81
    const/4 v3, 0x4

    if-ge v5, v3, :cond_109

    if-ge v5, v8, :cond_ce

    aget-object v3, v9, v5

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    aget-object v3, v9, v5

    const v4, 0x7f0901f9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v3, v9, v5

    const v4, 0x7f0901fa

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_c0
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_81

    :cond_c4
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7f

    :cond_ce
    if-ne v5, v8, :cond_101

    if-eqz v7, :cond_101

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    if-eqz v3, :cond_101

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    if-eqz v3, :cond_101

    aget-object v3, v9, v5

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    aget-object v3, v9, v5

    const v4, 0x7f0901f9

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v3, v9, v5

    const v4, 0x7f0901fa

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_c0

    :cond_101
    aget-object v3, v9, v5

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_c0

    :cond_109
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    if-eqz v3, :cond_15f

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    if-nez v3, :cond_15f

    const/4 v3, 0x1

    :goto_112
    if-eqz v7, :cond_161

    if-eqz v3, :cond_161

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_11f
    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->editorialText:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1b8

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    const v3, 0x7f0803a6

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v5

    const v7, 0x7f0f006c

    invoke-direct {v4, v5, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->editorialText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_154
    const v1, 0x7f0901ff

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1133
    return-void

    .line 1132
    :cond_15f
    const/4 v3, 0x0

    goto :goto_112

    :cond_161
    if-nez v7, :cond_1b1

    iget v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    if-gtz v4, :cond_169

    if-eqz v3, :cond_1b1

    :cond_169
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    if-lez v5, :cond_192

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0e0020

    iget v8, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_192
    if-eqz v3, :cond_1a4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_19f

    const-string v3, " \u00b7 "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_19f
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1a4
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11f

    :cond_1b1
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_11f

    :cond_1b8
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_154
.end method

.method public setLocation(Ljava/lang/String;Z)V
    .registers 7
    .parameter "location"
    .parameter "singleLine"

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 698
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_60

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 701
    if-eqz p2, :cond_37

    .line 702
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 708
    :goto_27
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    if-eqz v0, :cond_56

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    :goto_36
    return-void

    .line 704
    :cond_37
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 705
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 706
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_27

    .line 711
    :cond_56
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_36

    .line 714
    :cond_60
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_36
.end method

.method public setLocationMap(Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter "bitmap"

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->map:Landroid/view/View;

    const v1, 0x7f090205

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1038
    return-void
.end method

.method public setName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "fullName"
    .parameter "givenName"
    .parameter "familyName"

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 594
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_37

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_37

    const/4 v0, 0x1

    .line 595
    .local v0, useFullName:Z
    :goto_10
    if-eqz v0, :cond_39

    .line 596
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->fullName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 601
    :goto_19
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->fullName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    if-eqz v0, :cond_48

    move v1, v2

    :goto_20
    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setVisibility(I)V

    .line 602
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->givenName:Landroid/widget/TextView;

    if-eqz v0, :cond_4a

    move v1, v3

    :goto_2a
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 603
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->familyName:Landroid/widget/TextView;

    if-eqz v0, :cond_4c

    :goto_33
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 604
    return-void

    .end local v0           #useFullName:Z
    :cond_37
    move v0, v2

    .line 594
    goto :goto_10

    .line 598
    .restart local v0       #useFullName:Z
    :cond_39
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->givenName:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->familyName:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_19

    :cond_48
    move v1, v3

    .line 601
    goto :goto_20

    :cond_4a
    move v1, v2

    .line 602
    goto :goto_2a

    :cond_4c
    move v3, v2

    .line 603
    goto :goto_33
.end method

.method public setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 578
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    .line 579
    return-void
.end method

.method public setPlusOneData(Ljava/lang/String;Z)V
    .registers 5
    .parameter "plusOnes"
    .parameter "plusOneByMe"

    .prologue
    .line 719
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    .line 720
    .local v0, plusOneButton:Landroid/widget/Button;
    if-eqz p1, :cond_27

    .line 721
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 722
    if-eqz p2, :cond_1b

    .line 723
    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 724
    const v1, 0x7f02019d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 729
    :goto_16
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 733
    :goto_1a
    return-void

    .line 726
    :cond_1b
    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 727
    const v1, 0x7f02019c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_16

    .line 731
    :cond_27
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1a
.end method

.method public setScrapbookAlbumUrls(Ljava/lang/Long;[Ljava/lang/String;)V
    .registers 8
    .parameter "albumId"
    .parameter "urls"

    .prologue
    .line 656
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/EsImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/EsImageView;->setVisibility(I)V

    .line 657
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 658
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    invoke-virtual {v3, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 660
    array-length v3, p2

    const/4 v4, 0x5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 661
    .local v2, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1f
    if-ge v0, v2, :cond_3c

    .line 662
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    sget-object v4, Lcom/google/android/apps/plus/views/ProfileAboutView;->SCRAPBOOK_PHOTO_IDS:[I

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ProfileImageView;

    .line 664
    .local v1, imageView:Lcom/google/android/apps/plus/views/ProfileImageView;
    sget-object v3, Lcom/google/android/apps/plus/views/ProfileImageView$SizePolicy;->SQUARE_TO_LONGEST_DIMENSION:Lcom/google/android/apps/plus/views/ProfileImageView$SizePolicy;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/ProfileImageView;->setSizePolicy(Lcom/google/android/apps/plus/views/ProfileImageView$SizePolicy;)V

    .line 665
    aget-object v3, p2, v0

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/ProfileImageView;->setUrl(Ljava/lang/String;)V

    .line 661
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 667
    .end local v1           #imageView:Lcom/google/android/apps/plus/views/ProfileImageView;
    :cond_3c
    return-void
.end method

.method public setTagLine(Ljava/lang/String;)V
    .registers 6
    .parameter "tagLine"

    .prologue
    .line 741
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_30

    .line 742
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 744
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    const v3, 0x7f0901b5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    .line 746
    .local v0, headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    const v2, 0x7f0801d8

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;I)V

    .line 748
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    const v3, 0x7f09008a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 749
    .local v1, view:Landroid/view/View;
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindIntroductionView(Landroid/view/View;Ljava/lang/String;)V

    .line 753
    .end local v0           #headerView:Lcom/google/android/apps/plus/views/SectionHeaderView;
    .end local v1           #view:Landroid/view/View;
    :goto_2f
    return-void

    .line 751
    :cond_30
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2f
.end method

.method public final showAddToCircles(Z)V
    .registers 5
    .parameter "isPlusPage"

    .prologue
    const/16 v2, 0x8

    .line 1593
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1594
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1595
    if-eqz p1, :cond_30

    .line 1596
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const v1, 0x7f08020a

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    .line 1600
    :goto_21
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1601
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1602
    return-void

    .line 1598
    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const v1, 0x7f080209

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    goto :goto_21
.end method

.method public final showBlocked()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    .line 1608
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1609
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1610
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1611
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1612
    return-void
.end method

.method public final showError(ZLjava/lang/String;)V
    .registers 6
    .parameter "enabled"
    .parameter "errorText"

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 582
    if-eqz p1, :cond_22

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 584
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 585
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 591
    :goto_21
    return-void

    .line 588
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_21
.end method

.method public final showNone()V
    .registers 3

    .prologue
    const/16 v1, 0x8

    .line 1628
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1629
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1630
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1631
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1632
    return-void
.end method

.method public final showProgress()V
    .registers 3

    .prologue
    const/16 v1, 0x8

    .line 1618
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1619
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    .line 1620
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1621
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1622
    return-void
.end method

.method public final updateContactSectionDividers()V
    .registers 5

    .prologue
    .line 790
    const/4 v1, 0x0

    .line 794
    .local v1, lastOne:Landroid/view/View;
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 795
    .local v0, count:I
    if-lez v0, :cond_15

    .line 796
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 799
    :cond_15
    if-nez v1, :cond_2b

    .line 800
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 801
    if-lez v0, :cond_2b

    .line 802
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 806
    :cond_2b
    if-nez v1, :cond_41

    .line 807
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 808
    if-lez v0, :cond_41

    .line 809
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 813
    :cond_41
    if-eqz v1, :cond_47

    .line 814
    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 816
    :cond_47
    return-void
.end method

.method public final updateLinksSectionDividers()V
    .registers 2

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateGenericListSectionDividers(Landroid/view/ViewGroup;)V

    .line 1095
    return-void
.end method

.method public final updateLocationsSectionDividers()V
    .registers 2

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateGenericListSectionDividers(Landroid/view/ViewGroup;)V

    .line 1079
    return-void
.end method

.method public final updatePersonalSectionDividers()V
    .registers 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 890
    const/4 v0, 0x0

    .line 892
    .local v0, lastOne:Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_1f

    .line 894
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 895
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    .line 898
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-eqz v1, :cond_3a

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_3a

    .line 900
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 901
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    .line 904
    :cond_3a
    if-eqz v0, :cond_40

    .line 905
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 907
    :cond_40
    return-void
.end method

.method public final updateWorkEducationDividers()V
    .registers 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 963
    const/4 v0, 0x0

    .line 965
    .local v0, lastOne:Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_1f

    .line 967
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 968
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->employment:Landroid/view/View;

    .line 971
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    if-eqz v1, :cond_3a

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_3a

    .line 973
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 974
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->education:Landroid/view/View;

    .line 977
    :cond_3a
    if-eqz v0, :cond_40

    .line 978
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    .line 980
    :cond_40
    return-void
.end method
