.class abstract Lcom/google/android/apps/plus/api/EsOperation;
.super Lcom/google/android/apps/plus/network/HttpOperation;
.source "EsOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/EsOperation$1;
    }
.end annotation


# instance fields
.field protected final mRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/EsRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "method"
    .parameter "url"
    .parameter "outputStream"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 62
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    .line 63
    return-void
.end method

.method protected static createESRequestUrl(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Ljava/lang/String;
    .registers 3
    .parameter "s"

    .prologue
    .line 287
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ES_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/api/EsOperation;->appendLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isFullSync(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/api/EsOperation;->appendSyncParameter(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final addRequest(Lcom/google/wireless/tacotruck/proto/Network$Request$Type;Lcom/google/protobuf/MessageLite;)V
    .registers 5
    .parameter "type"
    .parameter "message"

    .prologue
    .line 266
    new-instance v0, Lcom/google/android/apps/plus/api/EsRequest;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/api/EsRequest;-><init>(Lcom/google/wireless/tacotruck/proto/Network$Request$Type;Lcom/google/protobuf/MessageLite;)V

    .line 267
    .local v0, request:Lcom/google/android/apps/plus/api/EsRequest;
    iget-object v1, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    return-void
.end method

.method protected final createPostData()Lorg/apache/http/HttpEntity;
    .registers 8

    .prologue
    .line 80
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$BatchRequest;->newBuilder()Lcom/google/wireless/tacotruck/proto/Network$BatchRequest$Builder;

    move-result-object v4

    const/4 v2, 0x0

    move v3, v2

    :goto_6
    iget-object v2, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_3e

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/EsRequest;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$Request;->newBuilder()Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;->setRequestId(Ljava/lang/String;)Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EsRequest;->getType()Lcom/google/wireless/tacotruck/proto/Network$Request$Type;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;->setRequestType(Lcom/google/wireless/tacotruck/proto/Network$Request$Type;)Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/EsRequest;->getMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/MessageLite;->toByteString()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;->setRequestBody(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;

    invoke-virtual {v5}, Lcom/google/wireless/tacotruck/proto/Network$Request$Builder;->build()Lcom/google/wireless/tacotruck/proto/Network$Request;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/wireless/tacotruck/proto/Network$BatchRequest$Builder;->addRequest(Lcom/google/wireless/tacotruck/proto/Network$Request;)Lcom/google/wireless/tacotruck/proto/Network$BatchRequest$Builder;

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    :cond_3e
    iget-object v2, p0, Lcom/google/android/apps/plus/api/EsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ME_AT"

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/network/AuthData;->getActionToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "HttpTransaction"

    const/4 v5, 0x3

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_67

    const-string v3, "HttpTransaction"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Using action token: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_67
    invoke-virtual {v4, v2}, Lcom/google/wireless/tacotruck/proto/Network$BatchRequest$Builder;->setActionToken(Ljava/lang/String;)Lcom/google/wireless/tacotruck/proto/Network$BatchRequest$Builder;

    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$BatchRequest$Builder;->build()Lcom/google/wireless/tacotruck/proto/Network$BatchRequest;

    move-result-object v0

    .line 81
    .local v0, batchRequest:Lcom/google/wireless/tacotruck/proto/Network$BatchRequest;
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Network$BatchRequest;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 82
    .local v1, data:Lorg/apache/http/entity/ByteArrayEntity;
    const-string v2, "application/octet-stream"

    invoke-virtual {v1, v2}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    .line 83
    return-object v1
.end method

.method public getName()Ljava/lang/String;
    .registers 3

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "NO OP"

    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/EsRequest;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/EsRequest;->getType()Lcom/google/wireless/tacotruck/proto/Network$Request$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Network$Request$Type;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method protected handleOneResponse(Lcom/google/wireless/tacotruck/proto/Network$Response;Lcom/google/protobuf/MessageLite;)V
    .registers 3
    .parameter "response"
    .parameter "originalRequest"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    return-void
.end method

.method protected final isAuthenticationError(Ljava/lang/Exception;)Z
    .registers 4
    .parameter "e"

    .prologue
    .line 218
    instance-of v0, p1, Lcom/google/android/apps/plus/api/ServerException;

    if-eqz v0, :cond_16

    .line 219
    sget-object v1, Lcom/google/android/apps/plus/api/EsOperation$1;->$SwitchMap$com$google$wireless$tacotruck$proto$Network$Response$ErrorCode:[I

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/api/ServerException;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ServerException;->getErrorCode()Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1e

    .line 224
    :cond_16
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v0

    :goto_1a
    return v0

    .line 221
    :pswitch_1b
    const/4 v0, 0x1

    goto :goto_1a

    .line 219
    nop

    :pswitch_data_1e
    .packed-switch 0x4
        :pswitch_1b
    .end packed-switch
.end method

.method protected final isImmediatelyRetryableError(Ljava/lang/Exception;)Z
    .registers 4
    .parameter "e"

    .prologue
    .line 206
    instance-of v0, p1, Lcom/google/android/apps/plus/api/ServerException;

    if-eqz v0, :cond_16

    .line 207
    sget-object v1, Lcom/google/android/apps/plus/api/EsOperation$1;->$SwitchMap$com$google$wireless$tacotruck$proto$Network$Response$ErrorCode:[I

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/api/ServerException;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ServerException;->getErrorCode()Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1e

    .line 213
    :cond_16
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isImmediatelyRetryableError(Ljava/lang/Exception;)Z

    move-result v0

    :goto_1a
    return v0

    .line 210
    :pswitch_1b
    const/4 v0, 0x1

    goto :goto_1a

    .line 207
    nop

    :pswitch_data_1e
    .packed-switch 0x3
        :pswitch_1b
        :pswitch_1b
    .end packed-switch
.end method

.method public onHttpCookie(Lorg/apache/http/cookie/Cookie;)V
    .registers 5
    .parameter "cookie"

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpCookie(Lorg/apache/http/cookie/Cookie;)V

    .line 145
    const-string v0, "ME_AT"

    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 146
    invoke-interface {p1}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ME_AT"

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/network/AuthData;->setActionToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_1e
    return-void
.end method

.method protected final onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .registers 14
    .parameter "inputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-static {p1}, Lcom/google/wireless/tacotruck/proto/Network$BatchResponse;->parseFrom(Ljava/io/InputStream;)Lcom/google/wireless/tacotruck/proto/Network$BatchResponse;

    move-result-object v0

    .line 94
    .local v0, batchResponse:Lcom/google/wireless/tacotruck/proto/Network$BatchResponse;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/EsOperation;->onStartResultProcessing()V

    .line 98
    invoke-virtual {v0}, Lcom/google/wireless/tacotruck/proto/Network$BatchResponse;->getResponseCount()I

    move-result v5

    .line 99
    .local v5, responseCount:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_c
    if-ge v1, v5, :cond_b2

    .line 100
    invoke-virtual {v0, v1}, Lcom/google/wireless/tacotruck/proto/Network$BatchResponse;->getResponse(I)Lcom/google/wireless/tacotruck/proto/Network$Response;

    move-result-object v4

    .line 101
    .local v4, response:Lcom/google/wireless/tacotruck/proto/Network$Response;
    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->hasRequestType()Z

    move-result v6

    if-nez v6, :cond_20

    new-instance v6, Ljava/net/ProtocolException;

    const-string v7, "Received a response without request type"

    invoke-direct {v6, v7}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_20
    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->hasErrorCode()Z

    move-result v6

    if-eqz v6, :cond_ae

    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->getErrorCode()Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;

    move-result-object v6

    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->getRequestType()Lcom/google/wireless/tacotruck/proto/Network$Request$Type;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/tacotruck/proto/Network$Request$Type;->name()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;->getNumber()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "HttpTransaction"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Error for request type: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", error number: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v6}, Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;->getNumber()I

    move-result v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ", error code: "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v7, Lcom/google/android/apps/plus/api/EsOperation$1;->$SwitchMap$com$google$wireless$tacotruck$proto$Network$Response$ErrorCode:[I

    invoke-virtual {v6}, Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_e6

    new-instance v7, Lcom/google/android/apps/plus/api/ServerException;

    invoke-direct {v7, v6, v8}, Lcom/google/android/apps/plus/api/ServerException;-><init>(Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;Ljava/lang/String;)V

    throw v7

    :pswitch_98
    iget-object v7, p0, Lcom/google/android/apps/plus/api/EsOperation;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/apps/plus/service/AndroidNotification;->showUpgradeRequiredNotification(Landroid/content/Context;)V

    new-instance v7, Lcom/google/android/apps/plus/api/ServerException;

    invoke-direct {v7, v6, v8}, Lcom/google/android/apps/plus/api/ServerException;-><init>(Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;Ljava/lang/String;)V

    throw v7

    :pswitch_a3
    iget-object v7, p0, Lcom/google/android/apps/plus/api/EsOperation;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidNotification;->showOutOfBoxRequiredNotification$faab20d()V

    new-instance v7, Lcom/google/android/apps/plus/api/ServerException;

    invoke-direct {v7, v6, v8}, Lcom/google/android/apps/plus/api/ServerException;-><init>(Lcom/google/wireless/tacotruck/proto/Network$Response$ErrorCode;Ljava/lang/String;)V

    throw v7

    .line 99
    :cond_ae
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_c

    .line 104
    .end local v4           #response:Lcom/google/wireless/tacotruck/proto/Network$Response;
    :cond_b2
    const/4 v1, 0x0

    :goto_b3
    if-ge v1, v5, :cond_e4

    .line 128
    invoke-virtual {v0, v1}, Lcom/google/wireless/tacotruck/proto/Network$BatchResponse;->getResponse(I)Lcom/google/wireless/tacotruck/proto/Network$Response;

    move-result-object v4

    .line 130
    .restart local v4       #response:Lcom/google/wireless/tacotruck/proto/Network$Response;
    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->hasIsMasterResponse()Z

    move-result v6

    if-eqz v6, :cond_df

    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->getIsMasterResponse()Z

    move-result v6

    if-eqz v6, :cond_df

    .line 131
    invoke-virtual {v4}, Lcom/google/wireless/tacotruck/proto/Network$Response;->getRequestId()Ljava/lang/String;

    move-result-object v3

    .line 132
    .local v3, requestId:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/plus/api/EsOperation;->mRequests:Ljava/util/List;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/api/EsRequest;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/EsRequest;->getMessage()Lcom/google/protobuf/MessageLite;

    move-result-object v2

    .line 134
    .local v2, originalRequest:Lcom/google/protobuf/MessageLite;
    invoke-virtual {p0, v4, v2}, Lcom/google/android/apps/plus/api/EsOperation;->handleOneResponse(Lcom/google/wireless/tacotruck/proto/Network$Response;Lcom/google/protobuf/MessageLite;)V

    .line 127
    .end local v2           #originalRequest:Lcom/google/protobuf/MessageLite;
    .end local v3           #requestId:Ljava/lang/String;
    :goto_dc
    add-int/lit8 v1, v1, 0x1

    goto :goto_b3

    .line 136
    :cond_df
    const/4 v6, 0x0

    invoke-virtual {p0, v4, v6}, Lcom/google/android/apps/plus/api/EsOperation;->handleOneResponse(Lcom/google/wireless/tacotruck/proto/Network$Response;Lcom/google/protobuf/MessageLite;)V

    goto :goto_dc

    .line 140
    .end local v4           #response:Lcom/google/wireless/tacotruck/proto/Network$Response;
    :cond_e4
    return-void

    .line 101
    nop

    :pswitch_data_e6
    .packed-switch 0x1
        :pswitch_98
        :pswitch_a3
    .end packed-switch
.end method
