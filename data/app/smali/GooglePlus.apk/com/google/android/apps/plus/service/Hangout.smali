.class public Lcom/google/android/apps/plus/service/Hangout;
.super Ljava/lang/Object;
.source "Hangout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/Hangout$1;,
        Lcom/google/android/apps/plus/service/Hangout$SupportStatus;,
        Lcom/google/android/apps/plus/service/Hangout$RoomType;,
        Lcom/google/android/apps/plus/service/Hangout$ApplicationEventListener;,
        Lcom/google/android/apps/plus/service/Hangout$Info;,
        Lcom/google/android/apps/plus/service/Hangout$LaunchSource;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CONSUMER_HANGOUT_DOMAIN:Ljava/lang/String;

.field private static final HANGOUT_URL_PATTERN:Ljava/util/regex/Pattern;

.field private static sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

.field private static sCachedIsCreationSupported:Z

.field private static sCachedStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

.field private static sHangoutCreationSupportCacheIsDirty:Z

.field private static sHangoutSupportStatusCacheIsDirty:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 34
    const-class v0, Lcom/google/android/apps/plus/service/Hangout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_1d

    move v0, v1

    :goto_a
    sput-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->$assertionsDisabled:Z

    .line 151
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/service/Hangout;->CONSUMER_HANGOUT_DOMAIN:Ljava/lang/String;

    .line 160
    const-string v0, "http s? ://plus.google.com/hangouts/(    \\p{Alnum}+)"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/Hangout;->HANGOUT_URL_PATTERN:Ljava/util/regex/Pattern;

    .line 167
    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    .line 168
    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    return-void

    .line 34
    :cond_1d
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 220
    return-void
.end method

.method public static enterGreenRoomFromStream(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/HangoutData;)V
    .registers 27
    .parameter "account"
    .parameter "context"
    .parameter "inviterId"
    .parameter "inviterName"
    .parameter "hangoutData"

    .prologue
    .line 440
    sget-boolean v2, Lcom/google/android/apps/plus/service/Hangout;->$assertionsDisabled:Z

    if-nez v2, :cond_10

    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/plus/service/Hangout;->isInProgress(Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v2

    if-nez v2, :cond_10

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    .line 441
    :cond_10
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 444
    .local v12, participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "g:"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/service/Hangout;->getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/api/services/plusi/model/HangoutData;->occupant:Ljava/util/List;

    if-eqz v2, :cond_92

    .line 451
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/api/services/plusi/model/HangoutData;->occupant:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, i$:Ljava/util/Iterator;
    :goto_51
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_92

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/api/services/plusi/model/HangoutOccupant;

    .line 452
    .local v20, occupant:Lcom/google/api/services/plusi/model/HangoutOccupant;
    move-object/from16 v0, v20

    iget-object v14, v0, Lcom/google/api/services/plusi/model/HangoutOccupant;->obfuscatedGaiaId:Ljava/lang/String;

    .line 453
    .local v14, gaiaId:Ljava/lang/String;
    move-object/from16 v0, v20

    iget-object v13, v0, Lcom/google/api/services/plusi/model/HangoutOccupant;->name:Ljava/lang/String;

    .line 454
    .local v13, fullName:Ljava/lang/String;
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "g:"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-static {v13}, Lcom/google/android/apps/plus/service/Hangout;->getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v21

    .line 459
    .local v21, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_51

    .line 463
    .end local v13           #fullName:Ljava/lang/String;
    .end local v14           #gaiaId:Ljava/lang/String;
    .end local v17           #i$:Ljava/util/Iterator;
    .end local v20           #occupant:Lcom/google/api/services/plusi/model/HangoutOccupant;
    .end local v21           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_92
    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->fromHangoutData(Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v4

    .line 464
    .local v4, roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/api/services/plusi/model/HangoutData;->roomDomain:Ljava/lang/String;

    if-eqz v2, :cond_123

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/HangoutData;->roomDomain:Ljava/lang/String;

    .line 465
    .local v5, roomDomain:Ljava/lang/String;
    :goto_a0
    const-string v2, "Hangout"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_d9

    .line 466
    const-string v2, "Hangout"

    const-string v3, "Launching for HangoutData:\nroomId=%s\nroomDomain=%s\nurl=%s\ntype=%s\nsubject=%s\nbroadcastId=%s"

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/google/api/services/plusi/model/HangoutData;->roomId:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    aput-object v5, v6, v7

    const/4 v7, 0x2

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/google/api/services/plusi/model/HangoutData;->url:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object v4, v6, v7

    const/4 v7, 0x4

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/google/api/services/plusi/model/HangoutData;->subject:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    move-object/from16 v0, p4

    iget-object v8, v0, Lcom/google/api/services/plusi/model/HangoutData;->broadcastId:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :cond_d9
    const/4 v6, 0x0

    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/google/api/services/plusi/model/HangoutData;->roomId:Ljava/lang/String;

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Stream:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v18

    .line 479
    .local v18, intent:Landroid/content/Intent;
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v15

    .line 480
    .local v15, gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;
    invoke-virtual {v15}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v2

    if-eqz v2, :cond_11b

    .line 481
    invoke-virtual {v15}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommService;->getNotificationIntent()Landroid/content/Intent;

    move-result-object v19

    .line 482
    .local v19, notificationIntent:Landroid/content/Intent;
    if-eqz v19, :cond_11b

    .line 483
    const-string v2, "hangout_info"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v16

    check-cast v16, Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 485
    .local v16, hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    if-eqz v16, :cond_11b

    #getter for: Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/service/Hangout$Info;->access$000(Lcom/google/android/apps/plus/service/Hangout$Info;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/api/services/plusi/model/HangoutData;->roomId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11b

    .line 486
    move-object/from16 v18, v19

    .line 490
    .end local v16           #hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    .end local v19           #notificationIntent:Landroid/content/Intent;
    :cond_11b
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 491
    return-void

    .line 464
    .end local v5           #roomDomain:Ljava/lang/String;
    .end local v15           #gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;
    .end local v18           #intent:Landroid/content/Intent;
    :cond_123
    const/4 v5, 0x0

    goto/16 :goto_a0
.end method

.method public static getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "fullName"

    .prologue
    .line 422
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 423
    .local v0, indexOfSpace:I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_a

    .line 426
    .end local p0
    :goto_9
    return-object p0

    .restart local p0
    :cond_a
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_9
.end method

.method public static getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "hangoutData"

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-static {p1}, Lcom/google/android/apps/plus/service/Hangout;->updateCacheDirtyFlags(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 256
    sget-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    if-eqz v1, :cond_14

    .line 257
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-ge v1, v3, :cond_21

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->OS_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    :goto_10
    sput-object v1, Lcom/google/android/apps/plus/service/Hangout;->sCachedStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    .line 258
    sput-boolean v2, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    .line 261
    :cond_14
    if-eqz p2, :cond_87

    .line 262
    invoke-static {p2}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->fromHangoutData(Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v0

    .line 263
    .local v0, roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;
    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->UNKNOWN:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    if-ne v0, v1, :cond_78

    .line 264
    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->TYPE_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    .line 274
    .end local v0           #roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;
    :goto_20
    return-object v1

    .line 257
    :cond_21
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v4

    if-eqz v4, :cond_47

    array-length v1, v4

    if-lez v1, :cond_47

    array-length v5, v4

    move v3, v2

    move v1, v2

    :goto_31
    if-ge v3, v5, :cond_48

    aget-object v6, v4, v3

    iget-object v7, v6, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-nez v7, :cond_42

    iget v1, v6, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    shr-int/lit8 v1, v1, 0x10

    int-to-short v1, v1

    const/4 v6, 0x2

    if-lt v1, v6, :cond_45

    const/4 v1, 0x1

    :cond_42
    :goto_42
    add-int/lit8 v3, v3, 0x1

    goto :goto_31

    :cond_45
    move v1, v2

    goto :goto_42

    :cond_47
    move v1, v2

    :cond_48
    if-nez v1, :cond_4d

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->DEVICE_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_10

    :cond_4d
    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string v3, "armeabi-v7a"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_64

    sget-object v1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v3, "armeabi-v7a"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_64

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->DEVICE_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_10

    :cond_64
    if-eqz p1, :cond_72

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_72

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    if-eqz v1, :cond_75

    :cond_72
    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->ACCOUNT_NOT_CONFIGURED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_10

    :cond_75
    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_10

    .line 267
    .restart local v0       #roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;
    :cond_78
    invoke-static {p2}, Lcom/google/android/apps/plus/service/Hangout;->isViewOnlyHangoutOnAir(Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v1

    if-eqz v1, :cond_87

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_87

    .line 270
    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->TYPE_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_20

    .line 274
    .end local v0           #roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;
    :cond_87
    sget-object v1, Lcom/google/android/apps/plus/service/Hangout;->sCachedStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_20
.end method

.method public static isAdvancedUiSupported(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 521
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_3c

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_ADVANCED_HANGOUTS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_3c

    new-instance v2, Landroid/view/TextureView;

    invoke-direct {v2, p0}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/view/TextureView;->getLayerType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3c

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "samsung"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3a

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v3, "google"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3a

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-gt v2, v3, :cond_3a

    move v2, v0

    :goto_37
    if-nez v2, :cond_3c

    :goto_39
    return v0

    :cond_3a
    move v2, v1

    goto :goto_37

    :cond_3c
    move v0, v1

    goto :goto_39
.end method

.method public static isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Z
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "hangoutData"

    .prologue
    const/4 v1, 0x0

    .line 323
    invoke-static {p1}, Lcom/google/android/apps/plus/service/Hangout;->updateCacheDirtyFlags(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 324
    sget-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    if-eqz v0, :cond_30

    .line 325
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-ne v0, v2, :cond_36

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v2

    if-eqz v2, :cond_36

    array-length v3, v2

    move v0, v1

    :goto_1d
    if-ge v0, v3, :cond_36

    aget-object v4, v2, v0

    const-string v5, "android.hardware.camera.front"

    iget-object v4, v4, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_33

    const/4 v0, 0x1

    :goto_2c
    sput-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->sCachedIsCreationSupported:Z

    .line 327
    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    .line 330
    :cond_30
    sget-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->sCachedIsCreationSupported:Z

    return v0

    .line 325
    :cond_33
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    :cond_36
    move v0, v1

    goto :goto_2c
.end method

.method public static isInProgress(Lcom/google/api/services/plusi/model/HangoutData;)Z
    .registers 2
    .parameter "hangoutData"

    .prologue
    .line 431
    if-eqz p0, :cond_c

    iget-object v0, p0, Lcom/google/api/services/plusi/model/HangoutData;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static isViewOnlyHangoutOnAir(Lcom/google/api/services/plusi/model/HangoutData;)Z
    .registers 5
    .parameter "hangoutData"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 506
    if-eqz p0, :cond_3a

    const-string v2, "BROADCAST"

    iget-object v3, p0, Lcom/google/api/services/plusi/model/HangoutData;->type:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_18

    const-string v2, "OPEN_BROADCAST"

    iget-object v3, p0, Lcom/google/api/services/plusi/model/HangoutData;->type:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3a

    :cond_18
    move v2, v0

    :goto_19
    if-eqz v2, :cond_3c

    iget-object v2, p0, Lcom/google/api/services/plusi/model/HangoutData;->broadcastDetails:Lcom/google/api/services/plusi/model/HangoutDataBroadcastDetails;

    if-eqz v2, :cond_3c

    iget-object v2, p0, Lcom/google/api/services/plusi/model/HangoutData;->broadcastDetails:Lcom/google/api/services/plusi/model/HangoutDataBroadcastDetails;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/HangoutDataBroadcastDetails;->youtubeLiveId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3c

    iget-object v2, p0, Lcom/google/api/services/plusi/model/HangoutData;->roomId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3c

    iget-object v2, p0, Lcom/google/api/services/plusi/model/HangoutData;->isViewOnly:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 512
    .local v0, viewOnlyHangout:Z
    :goto_39
    return v0

    .end local v0           #viewOnlyHangout:Z
    :cond_3a
    move v2, v1

    .line 506
    goto :goto_19

    :cond_3c
    move v0, v1

    goto :goto_39
.end method

.method private static updateCacheDirtyFlags(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 3
    .parameter "account"

    .prologue
    const/4 v1, 0x1

    .line 361
    sget-object v0, Lcom/google/android/apps/plus/service/Hangout;->sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/android/apps/plus/service/Hangout;->sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 363
    :cond_d
    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    .line 364
    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    .line 367
    sput-object p0, Lcom/google/android/apps/plus/service/Hangout;->sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

    .line 369
    :cond_13
    return-void
.end method
