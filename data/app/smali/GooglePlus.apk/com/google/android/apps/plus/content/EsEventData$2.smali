.class final Lcom/google/android/apps/plus/content/EsEventData$2;
.super Ljava/lang/Object;
.source "EsEventData.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/content/EsEventData;->enableInstantShareInternal(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$activeISEventId:Ljava/lang/String;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$eventId:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1410
    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$activeISEventId:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$eventId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 1413
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    .line 1415
    .local v0, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$activeISEventId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1416
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_OFF:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1419
    :cond_17
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$eventId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_28

    .line 1420
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsEventData$2;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_PARTY_MODE_ON:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1423
    :cond_28
    return-void
.end method
