.class public final Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "ReportAbuseActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;",
        "Lcom/google/api/services/plusi/model/ReportAbuseActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAbuseType:Ljava/lang/String;

.field private final mActivityId:Ljava/lang/String;

.field private final mCommentId:Ljava/lang/String;

.field private final mDeleteComment:Z

.field private final mIsUndo:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "abuseType"

    .prologue
    .line 110
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V

    .line 111
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "deleteComments"
    .parameter "abuseType"
    .parameter "isUndo"

    .prologue
    .line 63
    const-string v4, "reportabuseactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/ReportAbuseActivityRequestJson;

    move-result-object v5

    invoke-static {}, Lcom/google/api/services/plusi/model/ReportAbuseActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/ReportAbuseActivityResponseJson;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 71
    iput-object p5, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mActivityId:Ljava/lang/String;

    .line 72
    iput-object p6, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    .line 73
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAbuseType:Ljava/lang/String;

    .line 74
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mDeleteComment:Z

    .line 75
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mIsUndo:Z

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "deleteComment"
    .parameter "isUndo"

    .prologue
    .line 94
    const-string v8, "SPAM"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Z)V

    .line 96
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :cond_d
    :goto_d
    return-void

    :cond_e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mDeleteComment:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_d
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;

    .end local p1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    if-nez v1, :cond_29

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mActivityId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_11
    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->itemId:Ljava/util/List;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mIsUndo:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->isUndo:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/DataAbuseReport;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataAbuseReport;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;->abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mAbuseType:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataAbuseReport;->abuseType:Ljava/lang/String;

    return-void

    :cond_29
    iget-object v1, p0, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->mCommentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11
.end method
