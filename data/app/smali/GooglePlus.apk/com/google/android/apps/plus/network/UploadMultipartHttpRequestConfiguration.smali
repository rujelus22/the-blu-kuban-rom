.class public final Lcom/google/android/apps/plus/network/UploadMultipartHttpRequestConfiguration;
.super Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;
.source "UploadMultipartHttpRequestConfiguration.java"


# static fields
.field public static final DEFAULT_BOUNDARY_BYTES:[B


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 17
    const-string v0, "onetwothreefourfivesixseven"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/UploadMultipartHttpRequestConfiguration;->DEFAULT_BOUNDARY_BYTES:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "scope"
    .parameter "backendOverrideUrl"

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/network/ApiaryHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected final getContentType()Ljava/lang/String;
    .registers 2

    .prologue
    .line 27
    const-string v0, "multipart/related; boundary=onetwothreefourfivesixseven"

    return-object v0
.end method
