.class public Lcom/google/android/apps/plus/network/ApiaryVideoActivity;
.super Lcom/google/android/apps/plus/network/ApiaryActivity;
.source "ApiaryVideoActivity.java"


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mImage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getImage()Ljava/lang/String;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->mImage:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/google/android/apps/plus/network/ApiaryActivity$Type;
    .registers 2

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/plus/network/ApiaryActivity$Type;->VIDEO:Lcom/google/android/apps/plus/network/ApiaryActivity$Type;

    return-object v0
.end method

.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .registers 6
    .parameter "mediaLayout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    .line 36
    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->mDisplayName:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->mImage:Ljava/lang/String;

    .line 39
    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    .line 41
    .local v0, mediaItemList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/MediaItem;>;"
    if-eqz v0, :cond_12

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 42
    :cond_12
    new-instance v1, Ljava/io/IOException;

    const-string v2, "empty media item"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 45
    :cond_1a
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    if-nez v2, :cond_2d

    new-instance v1, Ljava/io/IOException;

    const-string v2, "missing image object"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2d
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->mImage:Ljava/lang/String;

    .line 47
    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;->mDisplayName:Ljava/lang/String;

    .line 49
    return-void
.end method
