.class public Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedEventFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;
.implements Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;
.implements Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;
.implements Lcom/google/android/apps/plus/views/EventActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$10;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$ActivityQuery;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$DetailsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventRefreshRunnable;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;",
        "Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog$CommentEditDialogListener;",
        "Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;",
        "Lcom/google/android/apps/plus/views/EventActionListener;"
    }
.end annotation


# static fields
.field private static mNextPagePreloadTriggerRows:I


# instance fields
.field private mActivityId:Ljava/lang/String;

.field private mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

.field private mCanComment:Z

.field private mCommentReqId:Ljava/lang/Integer;

.field private mDeleteReqId:Ljava/lang/Integer;

.field private mError:Z

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mEventId:Ljava/lang/String;

.field private mEventLoaded:Z

.field private mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

.field private mFetchReqId:Ljava/lang/Integer;

.field private mFirstActivityTimestamp:J

.field private mGhostEvent:Z

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private final mHandler:Landroid/os/Handler;

.field private mHasUserInteracted:Z

.field private mInvitationToken:Ljava/lang/String;

.field private mInviteReqId:Ljava/lang/Integer;

.field private final mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mNeedsRefresh:Z

.field private mNewPhotoReqId:Ljava/lang/Integer;

.field private mOwnerId:Ljava/lang/String;

.field private mPollingToken:Ljava/lang/String;

.field private mPreloadRequested:Z

.field private mRefreshRunnable:Ljava/lang/Runnable;

.field private mReportAbuseRequestId:Ljava/lang/Integer;

.field private mResumeToken:Ljava/lang/String;

.field private mSavedScrollPos:I

.field private mSendRsvpReqId:Ljava/lang/Integer;

.field private final mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

.field private mSettingsObserver:Landroid/database/ContentObserver;

.field private mTemporalRsvpState:Ljava/lang/String;

.field private mTypeId:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 173
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    .line 174
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHandler:Landroid/os/Handler;

    .line 181
    new-instance v0, Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/EventActiveState;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    .line 202
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    .line 273
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$1;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 317
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 1676
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_d

    :cond_c
    :goto_c
    return-void

    :cond_d
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz p2, :cond_2b

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c

    :cond_2b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_c
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .registers 9
    .parameter "x0"

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPollingToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    if-nez v0, :cond_f

    :cond_e
    :goto_e
    return-void

    :cond_f
    const v0, 0x7f08038e

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/apps/plus/service/EsService;->invitePeopleToEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    goto :goto_e
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/EventActiveState;
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 91
    const v0, 0x7f080392

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->turnOnInstantShare(ZZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/api/services/plusi/model/PlusEvent;
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;
    .registers 2
    .parameter "x0"

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 91
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedEventFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private fetchData()V
    .registers 9

    .prologue
    .line 615
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    .line 616
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPollingToken:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/service/EsService;->readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    .line 618
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    .line 619
    return-void
.end method

.method private handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_d

    .line 1066
    :cond_c
    :goto_c
    return-void

    .line 1056
    :cond_d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    .line 1058
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    .line 1060
    if-eqz p2, :cond_2b

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1061
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c

    .line 1064
    :cond_2b
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    goto :goto_c
.end method

.method private handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "serviceResult"

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1008
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_10

    .line 1033
    :cond_f
    :goto_f
    return-void

    .line 1012
    :cond_10
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    .line 1014
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    .line 1015
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    .line 1017
    if-eqz p2, :cond_f

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1018
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v0

    .line 1019
    .local v0, errorCode:I
    const/16 v1, 0x190

    if-lt v0, v1, :cond_3f

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_3f

    .line 1020
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    .line 1029
    :cond_2e
    :goto_2e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1031
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v2, "HEF"

    const-string v3, "HGEUC"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_f

    .line 1022
    :cond_3f
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    .line 1023
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v1, :cond_2e

    .line 1024
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f080097

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_2e
.end method

.method private handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_d

    .line 1217
    :cond_c
    :goto_c
    return-void

    .line 1207
    :cond_d
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    .line 1209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    .line 1211
    if-eqz p2, :cond_2b

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1212
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c

    .line 1215
    :cond_2b
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    goto :goto_c
.end method

.method private handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .registers 10
    .parameter "requestId"
    .parameter "serviceResult"
    .parameter "photoLocation"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1070
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_f

    .line 1109
    :cond_e
    :goto_e
    return-void

    .line 1074
    :cond_f
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    .line 1076
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->hideProgressDialog()V

    .line 1078
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1079
    .local v0, context:Landroid/content/Context;
    if-eqz p2, :cond_2c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 1080
    const v1, 0x7f0801a8

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_e

    .line 1082
    :cond_2c
    const v1, 0x7f080394

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1084
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$7;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    aput-object p3, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$7;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_e
.end method

.method private handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v1, 0x0

    .line 1036
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_e

    .line 1049
    :cond_d
    :goto_d
    return-void

    .line 1040
    :cond_e
    if-eqz p2, :cond_26

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1041
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1044
    :cond_26
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    .line 1045
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    .line 1046
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateActiveEventState()V

    .line 1047
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateRsvpSection()V

    goto :goto_d
.end method

.method private hideProgressDialog()V
    .registers 4

    .prologue
    .line 1300
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1302
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_11

    .line 1303
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1305
    :cond_11
    return-void
.end method

.method private inviteMore()V
    .registers 10

    .prologue
    const/4 v5, 0x0

    .line 922
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const v2, 0x7f080388

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0xb

    const/4 v7, 0x1

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 930
    return-void
.end method

.method private showChoosePhotoDialog()V
    .registers 4

    .prologue
    .line 283
    new-instance v0, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    const v1, 0x7f08010c

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(I)V

    .line 284
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    .line 285
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "change_photo"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method private showProgressDialog(I)V
    .registers 6
    .parameter "messageResId"

    .prologue
    .line 1293
    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 1296
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1297
    return-void
.end method

.method private toggleInstantShare(Z)V
    .registers 7
    .parameter "enableState"

    .prologue
    .line 1661
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v1, v1, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-ne p1, v1, :cond_7

    .line 1671
    :cond_6
    :goto_6
    return-void

    .line 1665
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1666
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_6

    .line 1670
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareToggle;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_6
.end method

.method private turnOnInstantShare(ZZ)V
    .registers 9
    .parameter "checkin"
    .parameter "takePhoto"

    .prologue
    .line 1644
    if-eqz p2, :cond_f

    .line 1645
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v1, "camera-event.jpg"

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntent$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1646
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1649
    .end local v0           #intent:Landroid/content/Intent;
    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsEventData;->isViewerCheckedIn(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v1

    if-nez v1, :cond_2a

    if-eqz p1, :cond_2a

    .line 1650
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->RSVP_CHECKIN:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->sendEventRsvp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 1654
    :cond_2a
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->toggleInstantShare(Z)V

    .line 1655
    return-void
.end method

.method private updateActiveEventState()V
    .registers 11

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1586
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1588
    .local v2, now:J
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHasUserInteracted:Z

    iput-boolean v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->hasUserInteracted:Z

    .line 1590
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    .line 1591
    .local v4, userGaiaId:Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v5, :cond_84

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v4}, Lcom/google/android/apps/plus/content/EsEventData;->canAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_84

    move v5, v6

    :goto_23
    iput-boolean v5, v8, Lcom/google/android/apps/plus/fragments/EventActiveState;->canUploadPhotos:Z

    .line 1593
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-static {v4, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    iput-boolean v8, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isOwner:Z

    .line 1596
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    .line 1597
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    .line 1599
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v4, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->isInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)Z

    move-result v5

    if-eqz v5, :cond_86

    .line 1600
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    .line 1616
    :cond_45
    :goto_45
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-nez v8, :cond_b9

    :goto_4b
    iput-boolean v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isRsvpEnabled:Z

    .line 1617
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    iput-object v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->temporalRsvpValue:Ljava/lang/String;

    .line 1620
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v5

    if-eqz v5, :cond_65

    .line 1621
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    .line 1623
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v7, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    .line 1626
    :cond_65
    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    const/16 v6, 0x3a

    if-ne v5, v6, :cond_81

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareAvailable:Z

    if-eqz v5, :cond_81

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-nez v5, :cond_81

    .line 1628
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHandler:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/apps/plus/fragments/HostedEventFragment$9;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$9;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1637
    :cond_81
    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    .line 1638
    return-void

    :cond_84
    move v5, v7

    .line 1591
    goto :goto_23

    .line 1601
    :cond_86
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v5

    if-eqz v5, :cond_93

    .line 1602
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v6, v5, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    goto :goto_45

    .line 1604
    :cond_93
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    if-nez v5, :cond_9e

    .line 1605
    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventRefreshRunnable;

    invoke-direct {v5, p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$EventRefreshRunnable;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;B)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    .line 1608
    :cond_9e
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1609
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v5, v4, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->timeUntilInstantShareAllowed(Lcom/google/api/services/plusi/model/PlusEvent;Ljava/lang/String;J)J

    move-result-wide v0

    .line 1610
    .local v0, delay:J
    const-wide/16 v8, 0x0

    cmp-long v5, v0, v8

    if-lez v5, :cond_45

    .line 1611
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHandler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mRefreshRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v8, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_45

    .end local v0           #delay:J
    :cond_b9
    move v6, v7

    .line 1616
    goto :goto_4b
.end method

.method private updateProgressIndicator()V
    .registers 3

    .prologue
    .line 1224
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    .line 1226
    .local v0, bar:Lcom/google/android/apps/plus/views/HostActionBar;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    if-nez v1, :cond_14

    .line 1227
    :cond_10
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 1231
    :goto_13
    return-void

    .line 1229
    :cond_14
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_13
.end method

.method private updateRsvpSection()V
    .registers 5

    .prologue
    .line 1571
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1572
    .local v1, view:Landroid/view/View;
    if-nez v1, :cond_7

    .line 1583
    :cond_6
    :goto_6
    return-void

    .line 1576
    :cond_7
    const v2, 0x7f090043

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventRsvpLayout;

    .line 1579
    .local v0, rsvpLayout:Lcom/google/android/apps/plus/views/EventRsvpLayout;
    if-eqz v0, :cond_6

    .line 1580
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V

    .line 1581
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->invalidate()V

    goto :goto_6
.end method

.method private updateView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 805
    if-nez p1, :cond_6

    .line 832
    :goto_5
    return-void

    .line 809
    :cond_6
    const v2, 0x7f090072

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 810
    .local v1, serverErrorView:Landroid/widget/TextView;
    const v2, 0x7f090109

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 811
    .local v0, contentView:Landroid/view/View;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    if-eqz v2, :cond_2d

    .line 812
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 813
    const v2, 0x7f080359

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 814
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 815
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showContent(Landroid/view/View;)V

    .line 831
    :cond_29
    :goto_29
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    goto :goto_5

    .line 816
    :cond_2d
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_3b

    .line 817
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 818
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 819
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_29

    .line 820
    :cond_3b
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    if-eqz v2, :cond_43

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_4d

    .line 821
    :cond_43
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 822
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 823
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_29

    .line 824
    :cond_4d
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    if-eqz v2, :cond_29

    .line 825
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 826
    const v2, 0x7f080358

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 827
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 828
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showContent(Landroid/view/View;)V

    goto :goto_29
.end method


# virtual methods
.method public final doPickPhotoFromAlbums()V
    .registers 6

    .prologue
    .line 308
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getCameraPhotosPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Z)Landroid/content/Intent;

    move-result-object v0

    .line 311
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 312
    return-void
.end method

.method public final doTakePhoto()V
    .registers 6

    .prologue
    .line 291
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_TAKE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 293
    :try_start_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v2, "camera-event.jpg"

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntent$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 294
    .local v1, intent:Landroid/content/Intent;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_12
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5 .. :try_end_12} :catch_13

    .line 299
    .end local v1           #intent:Landroid/content/Intent;
    :goto_12
    return-void

    .line 295
    :catch_13
    move-exception v0

    .line 296
    .local v0, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0802cd

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_12
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1246
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final handleReportEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v4, 0x0

    .line 374
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_e

    .line 395
    :cond_d
    :goto_d
    return-void

    .line 378
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "req_pending"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 380
    .local v1, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v1, :cond_1f

    .line 381
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 384
    :cond_1f
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateSpinner()V

    .line 387
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 389
    .local v0, context:Landroid/content/Context;
    if-eqz p2, :cond_3c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3c

    .line 390
    const v2, 0x7f0801a8

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 392
    :cond_3c
    const v2, 0x7f08027a

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_d
.end method

.method protected final handleSharePhotosToEventCallBack$b5e9bbb(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "result"

    .prologue
    .line 367
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f08027d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 371
    :cond_19
    return-void
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 1235
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected final isProgressIndicatorVisible()Z
    .registers 2

    .prologue
    .line 303
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public final onActionButtonClicked(I)V
    .registers 5
    .parameter "actionId"

    .prologue
    .line 904
    packed-switch p1, :pswitch_data_1e

    .line 916
    :goto_3
    return-void

    .line 906
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showChoosePhotoDialog()V

    goto :goto_3

    .line 910
    :pswitch_8
    const v1, 0x7f080119

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->newInstance(I)Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;

    move-result-object v0

    .line 912
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;
    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 913
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "comment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/CommentEditFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_3

    .line 904
    nop

    :pswitch_data_1e
    .packed-switch 0x0
        :pswitch_4
        :pswitch_8
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 12
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v7, 0x1

    const/4 v3, -0x1

    .line 937
    if-eq p2, v3, :cond_5

    .line 971
    :cond_4
    :goto_4
    return-void

    .line 941
    :cond_5
    packed-switch p1, :pswitch_data_98

    :pswitch_8
    goto :goto_4

    .line 943
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 944
    .local v0, activity:Landroid/app/Activity;
    instance-of v3, v0, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    if-eqz v3, :cond_17

    move-object v3, v0

    .line 945
    check-cast v3, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    invoke-interface {v3}, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    .line 948
    :cond_17
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v4, "camera-event.jpg"

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    goto :goto_4

    .line 953
    .end local v0           #activity:Landroid/app/Activity;
    :pswitch_22
    if-ne p2, v3, :cond_4

    if-eqz p3, :cond_4

    .line 954
    const-string v3, "mediarefs"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 956
    .local v2, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    if-eqz v2, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_37
    :goto_37
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_64

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_37

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_37

    :cond_64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v5, 0x7f080394

    invoke-static {v3, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;

    invoke-direct {v5, p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;)V

    new-array v3, v7, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$8;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_4

    .line 962
    .end local v2           #mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :pswitch_84
    const-string v3, "audience"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    .line 964
    .local v1, audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/apps/plus/fragments/HostedEventFragment$6;

    invoke-direct {v4, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_4

    .line 941
    :pswitch_data_98
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_84
        :pswitch_22
    .end packed-switch
.end method

.method public final onAddPhotosClicked()V
    .registers 1

    .prologue
    .line 1810
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showChoosePhotoDialog()V

    .line 1811
    return-void
.end method

.method public final onAvatarClicked(Ljava/lang/String;)V
    .registers 5
    .parameter "gaiaId"

    .prologue
    .line 1288
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    .line 1290
    return-void
.end method

.method public final onCommentEditComplete$42c1be52(Landroid/text/Spannable;)V
    .registers 7
    .parameter "comment"

    .prologue
    .line 994
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1005
    :cond_a
    :goto_a
    return-void

    .line 999
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p1}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v0

    .line 1002
    .local v0, commentText:Ljava/lang/String;
    const v1, 0x7f080371

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    .line 1003
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/EsService;->createEventComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    goto :goto_a
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x1

    .line 554
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean v2, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    .line 558
    if-eqz p1, :cond_ee

    .line 559
    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    .line 560
    const-string v0, "ownerid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    .line 561
    const-string v0, "typeid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    .line 562
    const-string v0, "invitation_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    .line 563
    const-string v0, "refresh"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    .line 564
    const-string v0, "scroll_pos"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    .line 565
    const-string v0, "first_timestamp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    .line 566
    const-string v0, "fetch_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 567
    const-string v0, "fetch_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    .line 569
    :cond_58
    const-string v0, "comment_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6c

    .line 570
    const-string v0, "comment_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    .line 572
    :cond_6c
    const-string v0, "new_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 573
    const-string v0, "new_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    .line 576
    :cond_80
    const-string v0, "invite_more_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 577
    const-string v0, "invite_more_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    .line 580
    :cond_94
    const-string v0, "rsvp_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a8

    .line 581
    const-string v0, "rsvp_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    .line 584
    :cond_a8
    const-string v0, "temp_rsvp_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b8

    .line 585
    const-string v0, "temp_rsvp_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    .line 588
    :cond_b8
    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_cc

    .line 589
    const-string v0, "delete_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    .line 591
    :cond_cc
    const-string v0, "abuse_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e0

    .line 592
    const-string v0, "abuse_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 596
    :cond_e0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    const-string v1, "expanded"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    .line 598
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->invalidateActionBar()V

    .line 609
    :goto_ed
    return-void

    .line 600
    :cond_ee
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_f9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    if-eqz v0, :cond_f9

    .line 601
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    goto :goto_ed

    .line 605
    :cond_f9
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    .line 606
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateView(Landroid/view/View;)V

    goto :goto_ed
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 6
    .parameter "loaderId"
    .parameter "bundle"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 668
    const/4 v1, 0x0

    .line 669
    .local v1, returnLoader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 671
    .local v0, context:Landroid/content/Context;
    packed-switch p1, :pswitch_data_1a

    .line 693
    :goto_8
    return-object v1

    .line 673
    :pswitch_9
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$3;

    .end local v1           #returnLoader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    .line 680
    .restart local v1       #returnLoader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    goto :goto_8

    .line 683
    :pswitch_11
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$4;

    .end local v1           #returnLoader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v0, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    .restart local v1       #returnLoader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    goto :goto_8

    .line 671
    nop

    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_9
        :pswitch_11
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 624
    const v2, 0x7f03004e

    invoke-virtual {p1, v2, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 626
    .local v1, view:Landroid/view/View;
    const v2, 0x7f090109

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 627
    new-instance v2, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {v2, v3, v4, p0, p0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/EventActionListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    .line 628
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 630
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsCallbacks:Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;

    invoke-virtual {v2, v3, v5, v4}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 631
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v6, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 632
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 634
    sget v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    if-nez v2, :cond_57

    .line 635
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    .line 636
    .local v0, screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    iget v2, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v2, :cond_5b

    .line 637
    const/16 v2, 0x8

    sput v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    .line 643
    .end local v0           #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_57
    :goto_57
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateView(Landroid/view/View;)V

    .line 644
    return-object v1

    .line 639
    .restart local v0       #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_5b
    const/16 v2, 0x10

    sput v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    goto :goto_57
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1778
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 1782
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1774
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 6
    .parameter "args"
    .parameter "tag"

    .prologue
    const/4 v1, 0x0

    .line 1763
    const-string v0, "dialog_photo_sync"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1764
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->turnOnInstantShare(ZZ)V

    .line 1770
    :cond_c
    :goto_c
    return-void

    .line 1765
    :cond_d
    const-string v0, "report_event"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1766
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->reportActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 1768
    const v0, 0x7f08027b

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->showProgressDialog(I)V

    goto :goto_c
.end method

.method public final onExpansionToggled(Z)V
    .registers 3
    .parameter "expanded"

    .prologue
    .line 1786
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iput-boolean p1, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    .line 1787
    return-void
.end method

.method public final onHangoutClicked()V
    .registers 12

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x0

    .line 1271
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

    if-eqz v0, :cond_20

    .line 1272
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const-string v4, "event"

    sget-object v7, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Event:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-object v6, v3

    move v9, v8

    move-object v10, v3

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    .line 1274
    :cond_20
    return-void
.end method

.method public final onInstantShareToggle(Z)V
    .registers 11
    .parameter "enable"

    .prologue
    const v8, 0x7f080362

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1320
    if-eqz p1, :cond_de

    .line 1321
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v5, "account"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/InstantUpload;->isSyncEnabled$1f9c1b43(Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    .line 1322
    .local v0, iuSyncEnabled:Z
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    .line 1325
    .local v1, masterSyncEnabled:Z
    if-eqz v1, :cond_72

    if-eqz v0, :cond_72

    .line 1326
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v2, "dialog_check_in"

    invoke-virtual {v6, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_5b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsEventData;->isViewerCheckedIn(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v2

    if-nez v2, :cond_5e

    move v2, v3

    :goto_38
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v5, :cond_60

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/EventOptions;->broadcast:Ljava/lang/Boolean;

    invoke-virtual {v5, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_60

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->ON_AIR:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    :goto_4e
    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;

    invoke-direct {v7, v2, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$InstantShareConfirmationDialog;-><init>(ZLcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;)V

    const-string v2, "dialog_check_in"

    invoke-virtual {v7, v6, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {v7, p0, v4}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1341
    .end local v0           #iuSyncEnabled:Z
    .end local v1           #masterSyncEnabled:Z
    :cond_5b
    :goto_5b
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHasUserInteracted:Z

    .line 1342
    return-void

    .restart local v0       #iuSyncEnabled:Z
    .restart local v1       #masterSyncEnabled:Z
    :cond_5e
    move v2, v4

    .line 1326
    goto :goto_38

    :cond_60
    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/PlusEvent;->isPublic:Ljava/lang/Boolean;

    invoke-virtual {v5, v7}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6f

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->PUBLIC:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    goto :goto_4e

    :cond_6f
    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;->PRIVATE:Lcom/google/android/apps/plus/fragments/HostedEventFragment$DialogType;

    goto :goto_4e

    .line 1329
    :cond_72
    if-nez v1, :cond_a0

    .line 1331
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v5, "dialog_master_sync"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_5b

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f08036b

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c4

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    invoke-virtual {v5, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v4, "dialog_master_sync"

    invoke-virtual {v5, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_5b

    .line 1334
    :cond_a0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v5, "dialog_photo_sync"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v5

    if-nez v5, :cond_5b

    const v5, 0x7f08002d

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f08036c

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v5, v7, v4

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c6

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0801c8

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v5, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v5

    invoke-virtual {v5, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v4, "dialog_photo_sync"

    invoke-virtual {v5, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_5b

    .line 1338
    .end local v0           #iuSyncEnabled:Z
    .end local v1           #masterSyncEnabled:Z
    :cond_de
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->toggleInstantShare(Z)V

    goto/16 :goto_5b
.end method

.method public final onInviteMoreClicked()V
    .registers 1

    .prologue
    .line 1805
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->inviteMore()V

    .line 1806
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 11
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v7, 0x4

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 91
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v3, "HEF"

    const-string v4, "OLF"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_d2

    :cond_17
    :goto_17
    return-void

    :pswitch_18
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPreloadRequested:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_96

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v0

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateActiveEventState()V

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_94

    move v0, v1

    :goto_3d
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCanComment:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    if-eqz v0, :cond_4e

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->onInstantShareToggle(Z)V

    :cond_4e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeInfoCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    if-eq v0, v6, :cond_6e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_6e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->isWrapContentEnabled()Z

    move-result v0

    if-eqz v0, :cond_6e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelection(I)V

    iput v6, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSavedScrollPos:I

    :cond_6e
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPollingToken:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->invalidateActionBar()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    if-eqz v0, :cond_8c

    :cond_89
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    :cond_8c
    :goto_8c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateView(Landroid/view/View;)V

    goto :goto_17

    :cond_94
    move v0, v2

    goto :goto_3d

    :cond_96
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeInfoCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGhostEvent:Z

    if-eqz v0, :cond_89

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventLoaded:Z

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    goto :goto_8c

    :pswitch_ac
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeActivityCursor(Landroid/database/Cursor;)V

    if-nez p2, :cond_b9

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    goto/16 :goto_17

    :cond_b9
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_17

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionToTop()V

    goto/16 :goto_17

    :pswitch_data_d2
    .packed-switch 0x0
        :pswitch_18
        :pswitch_ac
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 897
    .local p1, arg0:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onLocationClicked()V
    .registers 3

    .prologue
    .line 1264
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v0, :cond_11

    .line 1265
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/MapUtils;->showDrivingDirections(Landroid/content/Context;Lcom/google/api/services/plusi/model/Place;)V

    .line 1267
    :cond_11
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter "item"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 862
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_8c

    move v1, v2

    .line 883
    :goto_a
    return v1

    .line 864
    :sswitch_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/phone/EditEventActivity;

    invoke-direct {v0, v2, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "account"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "event_id"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "owner_id"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 866
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_a

    .line 870
    .end local v0           #intent:Landroid/content/Intent;
    :sswitch_38
    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;

    invoke-direct {v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "delete_event_conf"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {v3, p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$DeleteEventConfirmationDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    goto :goto_a

    .line 874
    :sswitch_4a
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->inviteMore()V

    goto :goto_a

    .line 878
    :sswitch_4e
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    const v3, 0x7f08012a

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f080140

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0801c5

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v3

    invoke-virtual {v3, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v4, "activity_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v4, "report_event"

    invoke-virtual {v3, v2, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_a

    .line 862
    nop

    :sswitch_data_8c
    .sparse-switch
        0x7f09029d -> :sswitch_4a
        0x7f0902b9 -> :sswitch_4e
        0x7f0902c0 -> :sswitch_b
        0x7f0902c1 -> :sswitch_38
    .end sparse-switch
.end method

.method public final onPause()V
    .registers 3

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 416
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 417
    return-void
.end method

.method public final onPhotoClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "photoId"
    .parameter "photoUrl"
    .parameter "gaiaId"

    .prologue
    const v2, 0x7f080354

    .line 1543
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v0

    .line 1544
    .local v0, builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1546
    if-eqz p1, :cond_41

    .line 1547
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    if-eqz v1, :cond_3c

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    :goto_1e
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1549
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1550
    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1551
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1559
    :goto_34
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    .line 1560
    return-void

    .line 1547
    :cond_3c
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1e

    .line 1555
    :cond_41
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    if-eqz v1, :cond_52

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    :goto_4b
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 1557
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    goto :goto_34

    .line 1555
    :cond_52
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_4b
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 7
    .parameter "actionBar"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 649
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 650
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 652
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_3d

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v3

    if-eqz v3, :cond_3d

    move v0, v1

    .line 653
    .local v0, isPlusPage:Z
    :goto_15
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsEventData;->isEventPostable(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    if-eqz v3, :cond_2c

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v3, :cond_2c

    if-nez v0, :cond_2c

    .line 654
    const v3, 0x7f020154

    const v4, 0x7f08010c

    invoke-virtual {p1, v2, v3, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 658
    :cond_2c
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCanComment:Z

    if-eqz v2, :cond_39

    .line 659
    const v2, 0x7f020153

    const v3, 0x7f08010d

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 663
    :cond_39
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateProgressIndicator()V

    .line 664
    return-void

    .end local v0           #isPlusPage:Z
    :cond_3d
    move v0, v2

    .line 652
    goto :goto_15
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 7
    .parameter "menu"

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 837
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 839
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    if-eqz v3, :cond_5a

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_5a

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5a

    move v0, v2

    .line 841
    .local v0, isOwner:Z
    :goto_1c
    if-eqz v0, :cond_32

    .line 842
    const v3, 0x7f0902c0

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 843
    const v3, 0x7f0902c1

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 846
    :cond_32
    if-nez v0, :cond_3d

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mActivityId:Ljava/lang/String;

    if-eqz v3, :cond_3d

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v3, :cond_3d

    move v1, v2

    .line 851
    .local v1, isReportAbuseVisible:Z
    :cond_3d
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/fragments/EventActiveState;->canUploadPhotos:Z

    if-eqz v3, :cond_4d

    .line 852
    const v3, 0x7f09029d

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 855
    :cond_4d
    if-eqz v1, :cond_59

    .line 856
    const v3, 0x7f0902b9

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 858
    :cond_59
    return-void

    .end local v0           #isOwner:Z
    .end local v1           #isReportAbuseVisible:Z
    :cond_5a
    move v0, v1

    .line 839
    goto :goto_1c
.end method

.method public final onResume()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 421
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 422
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 424
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v3, "HEF"

    const-string v4, "OR"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_37

    .line 427
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_37

    .line 428
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 429
    .local v1, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleGetEventUpdatesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 430
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    .line 434
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_37
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_5c

    .line 435
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_5c

    .line 436
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 437
    .restart local v1       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 438
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    .line 442
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_5c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_81

    .line 443
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_81

    .line 444
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 445
    .restart local v1       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleCreateCommentComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 446
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    .line 450
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_81
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_b3

    .line 451
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_b3

    .line 452
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    .line 453
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 454
    .restart local v1       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    .line 455
    .local v0, photoLocation:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleNewPhotoComplete(ILcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V

    .line 456
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    .line 460
    .end local v0           #photoLocation:Ljava/lang/String;
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_b3
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_d8

    .line 461
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_d8

    .line 462
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 463
    .restart local v1       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 464
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    .line 468
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_d8
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_fd

    .line 469
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_fd

    .line 470
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    .line 471
    .restart local v1       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->handleReportEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 472
    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 476
    .end local v1           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_fd
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->settingsUri:Landroid/net/Uri;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 478
    return-void
.end method

.method public final onRsvpChanged(Ljava/lang/String;)V
    .registers 6
    .parameter "rsvpType"

    .prologue
    .line 1251
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 1252
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/apps/plus/service/EsService;->sendEventRsvp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    .line 1254
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    .line 1255
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateActiveEventState()V

    .line 1256
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->updateRsvpSection()V

    .line 1259
    :cond_28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mHasUserInteracted:Z

    .line 1260
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 508
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 510
    const-string v0, "id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    const-string v0, "ownerid"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    const-string v0, "typeid"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 513
    const-string v0, "invitation_token"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    const-string v0, "refresh"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNeedsRefresh:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 515
    const-string v0, "expanded"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v1, v1, Lcom/google/android/apps/plus/fragments/EventActiveState;->expanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 516
    const-string v0, "first_timestamp"

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFirstActivityTimestamp:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_45

    .line 518
    const-string v0, "scroll_pos"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 520
    :cond_45
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_54

    .line 521
    const-string v0, "fetch_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mFetchReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 523
    :cond_54
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_63

    .line 524
    const-string v0, "rsvp_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mSendRsvpReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 526
    :cond_63
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    if-eqz v0, :cond_6e

    .line 527
    const-string v0, "temp_rsvp_state"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTemporalRsvpState:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    :cond_6e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_7d

    .line 530
    const-string v0, "comment_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mCommentReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 532
    :cond_7d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_8c

    .line 533
    const-string v0, "new_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNewPhotoReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    :cond_8c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_9b

    .line 537
    const-string v0, "invite_more_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInviteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 540
    :cond_9b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_aa

    .line 541
    const-string v0, "delete_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 544
    :cond_aa
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_b9

    .line 545
    const-string v0, "abuse_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 549
    :cond_b9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    const-string v1, "HEF"

    const-string v2, "ON"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->checkPartitions(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 3
    .parameter "args"

    .prologue
    .line 499
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 500
    const-string v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    .line 501
    const-string v0, "owner_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    .line 502
    const-string v0, "invitation_token"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mInvitationToken:Ljava/lang/String;

    .line 503
    const-string v0, "notif_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mTypeId:I

    .line 504
    return-void
.end method

.method public final onUpdateCardClicked(Lcom/google/android/apps/plus/views/EventUpdate;)V
    .registers 5
    .parameter "update"

    .prologue
    .line 1791
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 1793
    .local v1, fragmentManager:Landroid/support/v4/app/FragmentManager;
    const-string v2, "update_card"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 1801
    :goto_c
    return-void

    .line 1797
    :cond_d
    invoke-static {}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->newInstance()Lcom/google/android/apps/plus/fragments/EventUpdateDialog;

    move-result-object v0

    .line 1798
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/EventUpdateDialog;
    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->setUpdate(Lcom/google/android/apps/plus/views/EventUpdate;)V

    .line 1799
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1800
    const-string v2, "update_card"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EventUpdateDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_c
.end method

.method public final onViewAllInviteesClicked()V
    .registers 7

    .prologue
    .line 1279
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mOwnerId:Ljava/lang/String;

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "event_id"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "owner_id"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->startActivity(Landroid/content/Intent;)V

    .line 1281
    return-void
.end method

.method public final onViewUsed(I)V
    .registers 4
    .parameter "position"

    .prologue
    .line 771
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPreloadRequested:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mResumeToken:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mError:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-nez v0, :cond_11

    .line 778
    :cond_10
    :goto_10
    return-void

    .line 775
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->getCount()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mNextPagePreloadTriggerRows:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_10

    .line 776
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mPreloadRequested:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedEventFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->post(Ljava/lang/Runnable;)Z

    goto :goto_10
.end method

.method public final refresh()V
    .registers 1

    .prologue
    .line 1240
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    .line 1241
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->fetchData()V

    .line 1242
    return-void
.end method
