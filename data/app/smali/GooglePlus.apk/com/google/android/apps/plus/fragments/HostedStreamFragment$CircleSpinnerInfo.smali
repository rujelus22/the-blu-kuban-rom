.class final Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
.super Ljava/lang/Object;
.source "HostedStreamFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CircleSpinnerInfo"
.end annotation


# instance fields
.field private final mCircleId:Ljava/lang/String;

.field private final mCircleName:Ljava/lang/String;

.field private final mRealCircleId:Ljava/lang/String;

.field private final mView:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "circleName"
    .parameter "circleId"

    .prologue
    const/4 v1, 0x0

    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mRealCircleId:Ljava/lang/String;

    .line 148
    const-string v0, "v.all.circles"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 149
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    .line 150
    const v0, 0x7f0800e0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    .line 165
    :goto_1c
    return-void

    .line 152
    :cond_1d
    const-string v0, "v.whatshot"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 153
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    .line 154
    const v0, 0x7f0800e1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    .line 155
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    goto :goto_1c

    .line 156
    :cond_34
    const-string v0, "v.nearby"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 157
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    .line 158
    const v0, 0x7f0800df

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    .line 159
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    goto :goto_1c

    .line 161
    :cond_4b
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    .line 162
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    .line 163
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    goto :goto_1c
.end method


# virtual methods
.method public final getCircleId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getRealCircleId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mRealCircleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getView()I
    .registers 2

    .prologue
    .line 180
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mView:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->mCircleName:Ljava/lang/String;

    return-object v0
.end method
