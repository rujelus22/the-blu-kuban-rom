.class public Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
.super Landroid/app/Service;
.source "RealTimeChatService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;,
        Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ResultsLinkedHashMap;
    }
.end annotation


# static fields
.field private static sConversationsLoaded:Z

.field private static sCurrentConversationRowId:Ljava/lang/Long;

.field private static sLastRequestId:Ljava/lang/Integer;

.field private static final sListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final sResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;",
            ">;"
        }
    .end annotation
.end field

.field private static sWakeLock:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

.field private final mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

.field private mConnectionRequestCount:I

.field private mHandler:Landroid/os/Handler;

.field private mLastConnectAttemptTime:J

.field private mLastConnectRequestTimestamp:J

.field private mLastMessageTime:J

.field private mLastResponseTime:J

.field private mLongTermConnect:Landroid/app/PendingIntent;

.field private mNeedsSync:Z

.field private final mPingRunnable:Ljava/lang/Runnable;

.field private mReconnectCount:I

.field private mReconnectDelay:J

.field private mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

.field private final mStopRunnable:Ljava/lang/Runnable;

.field private final mTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    .line 192
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    invoke-direct {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    .line 194
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ResultsLinkedHashMap;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ResultsLinkedHashMap;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;

    .line 197
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    .line 198
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sCurrentConversationRowId:Ljava/lang/Long;

    .line 199
    sput-boolean v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 264
    iput v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    .line 265
    iput-boolean v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    .line 266
    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    .line 268
    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    .line 269
    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectAttemptTime:J

    .line 272
    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    .line 350
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    .line 395
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    .line 397
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$2;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    .line 409
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$3;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    .line 2487
    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Intent;)V
    .registers 12
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v3, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v4, 0x3

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "op"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v0, "RealTimeChatService"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_44

    const-string v0, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "ProcessIntent OpCode "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " requestId "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "rid"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_44
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_9e

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->active()Z

    move-result v0

    if-eqz v0, :cond_9e

    move v0, v3

    :goto_58
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v4, :cond_a0

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->active()Z

    move-result v4

    if-eqz v4, :cond_a0

    move v4, v3

    :goto_65
    if-eqz v4, :cond_a2

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    if-eqz v6, :cond_a2

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a2

    :goto_7b
    sparse-switch v5, :sswitch_data_aee

    if-nez v3, :cond_94

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_81e

    if-eqz v2, :cond_81e

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_81e

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_94
    :goto_94
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    sparse-switch v5, :sswitch_data_b34

    :cond_9d
    :goto_9d
    return-void

    :cond_9e
    move v0, v8

    goto :goto_58

    :cond_a0
    move v4, v8

    goto :goto_65

    :cond_a2
    move v3, v8

    goto :goto_7b

    :sswitch_a4
    :try_start_a4
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    :try_end_a7
    .catch Ljava/lang/Exception; {:try_start_a4 .. :try_end_a7} :catch_a8

    goto :goto_9d

    :catch_a8
    move-exception v0

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9d

    const-string v1, "RealTimeChatService"

    const-string v2, "Exception in processIntent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9d

    :sswitch_ba
    :try_start_ba
    const-string v0, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_ca

    const-string v0, "RealTimeChatService"

    const-string v2, "connectIfLoggedIn "

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_ca
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    if-eqz v2, :cond_e0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_104

    :cond_e0
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f0

    const-string v0, "RealTimeChatService"

    const-string v1, "Requested to connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_fc

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->active()Z

    move-result v0

    if-nez v0, :cond_9d

    :cond_fc
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_9d

    :cond_104
    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$4;

    invoke-direct {v5, p0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$4;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v3, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    if-nez v4, :cond_162

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v1, :cond_117

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_117
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_145

    :goto_11f
    if-eqz v9, :cond_150

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_150

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_137

    const-string v0, "RealTimeChatService"

    const-string v1, "marking needs sync"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_9d

    :cond_145
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v9

    goto :goto_11f

    :cond_150
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "RealTimeChatService"

    const-string v1, "requested connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9d

    :cond_162
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "RealTimeChatService"

    const-string v1, "already connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9d

    :sswitch_174
    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1ab

    const-string v0, "RealTimeChatService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "connectAndStayConnected "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1ab
    if-nez v3, :cond_9d

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_9d

    :sswitch_1b5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1e6

    const-string v0, "RealTimeChatService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "connectAndStayConnected "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e6
    if-nez v3, :cond_9d

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_9d

    :sswitch_1f0
    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-lez v1, :cond_1fa

    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    :cond_1fa
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_225

    const-string v1, "RealTimeChatService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "allowDisconnect "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_225
    if-nez v0, :cond_9d

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-lez v0, :cond_9d

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v0, :cond_9d

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-nez v0, :cond_9d

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_249

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v0

    if-nez v0, :cond_9d

    :cond_249
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_259

    const-string v0, "RealTimeChatService"

    const-string v1, "stopping service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_259
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_265

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_265
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_9d

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_9d

    :sswitch_286
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v0, :cond_291

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_291
    if-nez v4, :cond_2f3

    if-eqz v2, :cond_9d

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    if-gtz v0, :cond_29d

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-lez v0, :cond_2cb

    :cond_29d
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2ad

    const-string v0, "RealTimeChatService"

    const-string v1, "connecting..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2ad
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_2bc

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectAttemptTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->connect()V

    :cond_2bc
    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    if-lez v0, :cond_2c6

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    :cond_2c6
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_9d

    :cond_2cb
    iget-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleLongTermConnect(Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_2dc

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_2dc
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_9d

    :cond_2f3
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "RealTimeChatService"

    const-string v1, "already connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9d

    :sswitch_305
    if-nez v4, :cond_9d

    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v2, :cond_336

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_321

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_333

    :cond_321
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "RealTimeChatService"

    const-string v1, "Requested to connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9d

    :cond_333
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_336
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_362

    :goto_33e
    if-eqz v9, :cond_36d

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36d

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    const-string v0, "reconnect_delay"

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_9d

    :cond_362
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v9

    goto :goto_33e

    :cond_36d
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "RealTimeChatService"

    const-string v1, "requested connect to wrong account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9d

    :sswitch_37f
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    sub-long/2addr v0, v5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long/2addr v5, v7

    if-eqz v4, :cond_475

    const-string v3, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3d2

    const-string v3, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "OP_PING "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v7, :cond_40f

    :goto_3c7
    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3d2
    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v3, :cond_3de

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v3

    if-nez v3, :cond_3ed

    :cond_3de
    iget-boolean v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v3, :cond_3ed

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-gtz v3, :cond_3ed

    const-wide/32 v3, 0xea60

    cmp-long v3, v5, v3

    if-gez v3, :cond_42a

    :cond_3ed
    const-wide/32 v3, 0xafc8

    cmp-long v0, v0, v3

    if-ltz v0, :cond_41a

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_404

    const-string v0, "RealTimeChatService"

    const-string v1, "too long since last response, restarting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_404
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect(I)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_9d

    :cond_40f
    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    goto :goto_3c7

    :cond_41a
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendKeepAlive()V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_9d

    :cond_42a
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_43a

    const-string v0, "RealTimeChatService"

    const-string v1, "uneeded connection found in ping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_43a
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_446

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_446
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_9d

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_9d

    :cond_475
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9d

    const-string v0, "RealTimeChatService"

    const-string v1, "connection is down, can\'t ping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_9d

    :sswitch_487
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    goto/16 :goto_9d

    :sswitch_495
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastResponseTime:J

    goto/16 :goto_9d

    :sswitch_49d
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4ad

    const-string v0, "RealTimeChatService"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4ad
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastMessageTime:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_4c8

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getOrRequestC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendUserCreateRequest$74507863(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendC2DMIdToSessionServer(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->connectionStarted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_4c8
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_4f8

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_4f8
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$5;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_9d

    :sswitch_504
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_514

    const-string v0, "RealTimeChatService"

    const-string v1, "onConversationsLoaded"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_514
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    goto/16 :goto_9d

    :sswitch_519
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_529

    const-string v0, "RealTimeChatService"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_529
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long v3, v0, v3

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_575

    const-string v1, "RealTimeChatService"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "OP_ON_DISCONNECTED "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-nez v0, :cond_5be

    move-object v0, v9

    :goto_56a
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_575
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_581

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v0

    if-nez v0, :cond_590

    :cond_581
    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v0, :cond_590

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-gtz v0, :cond_590

    const-wide/32 v0, 0xea60

    cmp-long v0, v3, v0

    if-gez v0, :cond_5f0

    :cond_590
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5a0

    const-string v0, "RealTimeChatService"

    const-string v1, "scheduling reconnect"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5a0
    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    if-gtz v0, :cond_5a8

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-lez v0, :cond_5c9

    :cond_5a8
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_5ab
    :goto_5ab
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$6;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$6;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_9d

    :cond_5be
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_56a

    :cond_5c9
    iget-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleLongTermConnect(Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_5da

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_5da
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_5ab

    :cond_5f0
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_600

    const-string v0, "RealTimeChatService"

    const-string v1, "no need to stay connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_600
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_60c

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_60c
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_62a

    const-string v0, "RealTimeChatService"

    const-string v1, "scheduling stop runnable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_62a
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_5ab

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_5ab

    :sswitch_644
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_654

    const-string v0, "RealTimeChatService"

    const-string v1, "log out"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_654
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_6f1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->connected()Z

    move-result v0

    if-eqz v0, :cond_6f1

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v0

    const-string v3, "realtimechat"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "c2dm_registration_id"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6f1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->unregisterDevice$6995facd(J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const/4 v4, -0x1

    invoke-virtual {v1, v0, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const-string v0, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6a7

    const-string v0, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unregister C2DM to session server: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6a7
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v0, "phone"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<iq to=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\' type=\'set\'>  <dev:device-unregister xmlns:dev=\'google:devices\' "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "device-id=\'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' app-id=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' /></iq>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->write(Ljava/lang/String;)Z

    :cond_6f1
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_6fd

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    :cond_6fd
    const-string v0, "realtimechat"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "c2dm_registration_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0}, Lcom/google/android/apps/plus/c2dm/C2DMReceiver;->unregisterC2DM(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v1, :cond_9d

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto/16 :goto_9d

    :sswitch_743
    const-string v0, "registration"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "realtimechat"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "c2dm_registration_id"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v3, "sticky_c2dm_registration_id"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v3, "c2dm_registration_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    const-string v4, "c2dm_registration_build_version"

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v2, :cond_7a4

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->connected()Z

    move-result v2

    if-eqz v2, :cond_7a4

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendUserCreateRequest$74507863(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendC2DMIdToSessionServer(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_798
    :goto_798
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$7;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$7;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_9d

    :cond_7a4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v3, :cond_7b7

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v3

    if-nez v3, :cond_798

    :cond_7b7
    iget-boolean v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v3, :cond_798

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-nez v3, :cond_798

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-ltz v1, :cond_798

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_798

    :sswitch_7ce
    const-string v0, "realtimechat"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "c2dm_registration_id"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-eqz v4, :cond_7e9

    invoke-static {v1}, Lcom/google/android/apps/plus/c2dm/C2DMReceiver;->requestC2DMRegistrationId(Landroid/content/Context;)V

    goto/16 :goto_9d

    :cond_7e9
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectRequestTimestamp:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v2, :cond_7fc

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->hasPendingCommands()Z

    move-result v2

    if-nez v2, :cond_812

    :cond_7fc
    iget-boolean v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mNeedsSync:Z

    if-nez v2, :cond_812

    iget v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectionRequestCount:I

    if-nez v2, :cond_812

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-ltz v0, :cond_812

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_812
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$8;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_81c
    .catch Ljava/lang/Exception; {:try_start_ba .. :try_end_81c} :catch_a8

    goto/16 :goto_9d

    :cond_81e
    const-string v0, "RealTimeChatService"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_94

    const-string v0, "RealTimeChatService"

    const-string v3, "action requested on inactive account"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_94

    :sswitch_830
    :try_start_830
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    const-string v3, "message_text"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;

    invoke-direct {v4, v1, v2, v0, v3}, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;
    :try_end_848
    .catch Ljava/lang/Exception; {:try_start_830 .. :try_end_848} :catch_84a

    goto/16 :goto_9d

    :catch_84a
    move-exception v0

    const-string v1, "RealTimeChatService"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_85b

    const-string v1, "RealTimeChatService"

    const-string v2, "Exception in processIntent"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_85b
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    const-string v1, "rid"

    invoke-virtual {p1, v1, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    invoke-direct {p0, p1, v0, v9}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_9d

    :sswitch_86c
    :try_start_86c
    const-string v0, "conversation_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "message_text"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_88c
    const-string v0, "conversation_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_8a7
    const-string v0, "conversation_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "message_row_id"

    const-wide/16 v5, -0x1

    invoke-virtual {p1, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_8c9
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "message_row_id"

    const-wide/16 v5, -0x1

    invoke-virtual {p1, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SetMessageFailedOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_8e5
    const-string v0, "message_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_8f9
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_915
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/LeaveConversationOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/LeaveConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_929
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/MarkConversationReadOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/MarkConversationReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_93d
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/MarkConversationNotificationsSeenOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/MarkConversationNotificationsSeenOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_951
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "message_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_96a
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "conversation_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->setName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_987
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "conversation_muted"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    new-instance v5, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/realtimechat/UpdateConversationOperation;->setMuted(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_9a5
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "inviter_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "accept"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/ReplyToInvitationOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_9c6
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RequestMoreEventsOperation;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RequestMoreEventsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_9da
    const-string v0, "acl"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-instance v3, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/apps/plus/realtimechat/SetAclOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v0, p1, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_9ed
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "is_present"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v0, "reciprocate"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/SendPresenceRequestOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_a0f
    const-string v0, "conversation_row_id"

    const-wide/16 v3, -0x1

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "typing_status"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/SendTypingRequestOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_a2e
    const-string v0, "conversation_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "tile_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "tile_event_version"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "tile_event_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v0, "tile_event_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    goto/16 :goto_9d

    :sswitch_a5b
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantListFromAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a70
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a84

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a70

    :cond_a84
    const-string v0, "type"

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->getSuggestionsRequest(Ljava/util/Collection;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    const-string v3, "rid"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    const-string v2, "rid"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_9d

    :sswitch_abb
    const-string v0, "message_row_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p1, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string v0, "flags"

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0, v1, p1, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResultValue()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_9d

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9d

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V
    :try_end_aec
    .catch Ljava/lang/Exception; {:try_start_86c .. :try_end_aec} :catch_84a

    goto/16 :goto_9d

    :sswitch_data_aee
    .sparse-switch
        0x6e -> :sswitch_174
        0x6f -> :sswitch_1b5
        0x70 -> :sswitch_1f0
        0x71 -> :sswitch_743
        0x72 -> :sswitch_7ce
        0x73 -> :sswitch_ba
        0x74 -> :sswitch_644
        0x75 -> :sswitch_7ce
        0xdc -> :sswitch_286
        0xdd -> :sswitch_37f
        0xde -> :sswitch_495
        0xdf -> :sswitch_487
        0xe0 -> :sswitch_49d
        0xe1 -> :sswitch_519
        0xe3 -> :sswitch_504
        0xe4 -> :sswitch_305
        0x154 -> :sswitch_a4
    .end sparse-switch

    :sswitch_data_b34
    .sparse-switch
        0xe2 -> :sswitch_abb
        0x14a -> :sswitch_830
        0x14b -> :sswitch_86c
        0x14c -> :sswitch_8f9
        0x14d -> :sswitch_915
        0x14f -> :sswitch_929
        0x150 -> :sswitch_951
        0x151 -> :sswitch_96a
        0x152 -> :sswitch_987
        0x155 -> :sswitch_9a5
        0x156 -> :sswitch_9c6
        0x157 -> :sswitch_9da
        0x158 -> :sswitch_8e5
        0x159 -> :sswitch_88c
        0x15a -> :sswitch_8a7
        0x15b -> :sswitch_8c9
        0x15c -> :sswitch_9ed
        0x15d -> :sswitch_a0f
        0x15e -> :sswitch_93d
        0x15f -> :sswitch_a2e
        0x160 -> :sswitch_a5b
    .end sparse-switch
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Lcom/google/android/apps/plus/realtimechat/BunchClient;)Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 58
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$300()Landroid/os/PowerManager$WakeLock;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$400()Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/List;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/Map;
    .registers 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$902(Z)Z
    .registers 1
    .parameter "x0"

    .prologue
    .line 58
    sput-boolean p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z

    return p0
.end method

.method public static allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 812
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 813
    const-string v1, "RealTimeChatService"

    const-string v2, "allowDisconnect"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    :cond_10
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 816
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x70

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 817
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 818
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 819
    return-void
.end method

.method public static checkMessageSent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"
    .parameter "flags"

    .prologue
    .line 844
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 845
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0xe2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 846
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 847
    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 848
    const-string v1, "flags"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 849
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V
    .registers 6
    .parameter "intent"
    .parameter "serviceResult"
    .parameter "resultValue"

    .prologue
    .line 2397
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$9;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$9;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2406
    return-void
.end method

.method public static connectAndStayConnected(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 660
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 661
    const-string v1, "RealTimeChatService"

    const-string v2, "connectAndStayConnected"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    :cond_10
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    .line 664
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_30

    .line 665
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 666
    const-string v1, "RealTimeChatService"

    const-string v2, "acquiring wake lock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    :cond_2b
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 670
    :cond_30
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 671
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x6e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 672
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 673
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 674
    return-void
.end method

.method public static connectIfLoggedIn(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "accountId"
    .parameter "conversationId"
    .parameter "messageTimestamp"

    .prologue
    .line 706
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 707
    const-string v1, "RealTimeChatService"

    const-string v2, "connectIfLoggedIn"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    :cond_10
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    .line 711
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_30

    .line 712
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 713
    const-string v1, "RealTimeChatService"

    const-string v2, "acquiring wake lock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    :cond_2b
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 718
    :cond_30
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 719
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x73

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 720
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 721
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 722
    const-string v1, "message_timestamp"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 723
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 724
    return-void
.end method

.method public static createConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "audience"
    .parameter "messageText"

    .prologue
    .line 863
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 864
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 865
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 866
    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 867
    const-string v1, "message_text"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 868
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static debuggable()Z
    .registers 1

    .prologue
    .line 599
    sget-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    return v0
.end method

.method private executeOperation(Lcom/google/android/apps/plus/realtimechat/BunchClient;Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;)Ljava/lang/Object;
    .registers 11
    .parameter "client"
    .parameter "intent"
    .parameter "operation"

    .prologue
    const/4 v6, 0x0

    .line 2369
    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->execute()V

    .line 2370
    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResultCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v1, v6, v3, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    .line 2372
    .local v1, result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResultValue()Ljava/lang/Object;

    move-result-object v2

    .line 2374
    .local v2, resultValue:Ljava/lang/Object;
    invoke-direct {p0, p2, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V

    .line 2376
    invoke-virtual {p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->getResponses()Ljava/util/Collection;

    move-result-object v0

    .line 2377
    .local v0, responses:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;>;"
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4f

    .line 2378
    const-string v3, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_46

    .line 2379
    const-string v3, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sending "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " responses"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2381
    :cond_46
    const-string v3, "rid"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p1, v0, v3, v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommands(Ljava/util/Collection;II)Z

    .line 2383
    :cond_4f
    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v5, 0x3a98

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2384
    return-object v2
.end method

.method private static getBackendSetting(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .parameter "context"

    .prologue
    .line 609
    sget-boolean v3, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v3, :cond_1b

    .line 610
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 612
    .local v2, settings:Landroid/content/SharedPreferences;
    const v3, 0x7f080004

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 614
    .local v1, key:Ljava/lang/String;
    const v3, 0x7f080002

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 616
    .local v0, defaultBackend:Ljava/lang/String;
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 618
    .end local v0           #defaultBackend:Ljava/lang/String;
    .end local v1           #key:Ljava/lang/String;
    .end local v2           #settings:Landroid/content/SharedPreferences;
    :goto_1a
    return-object v3

    :cond_1b
    const v3, 0x7f080003

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1a
.end method

.method public static getConversationsLoaded()Z
    .registers 1

    .prologue
    .line 789
    sget-boolean v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sConversationsLoaded:Z

    return v0
.end method

.method public static getCurrentConversationRowId()Ljava/lang/Long;
    .registers 2

    .prologue
    .line 779
    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    monitor-enter v1

    .line 780
    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sCurrentConversationRowId:Ljava/lang/Long;

    monitor-exit v1
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_7

    return-object v0

    .line 781
    :catchall_7
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static getOrRequestC2dmId(Landroid/content/Context;)Ljava/lang/String;
    .registers 15
    .parameter "context"

    .prologue
    const/4 v12, 0x0

    const/4 v13, 0x3

    const/4 v10, 0x0

    .line 2108
    const-string v9, "realtimechat"

    invoke-virtual {p0, v9, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 2109
    .local v4, prefs:Landroid/content/SharedPreferences;
    const-string v9, "c2dm_registration_id"

    invoke-interface {v4, v9, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2110
    .local v6, registrationId:Ljava/lang/String;
    const-string v9, "c2dm_registration_time"

    const-wide/16 v10, 0x0

    invoke-interface {v4, v9, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    .line 2112
    .local v7, registrationTime:J
    const-string v9, "c2dm_registration_build_version"

    invoke-interface {v4, v9, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2117
    .local v5, registrationBuildVersion:Ljava/lang/String;
    :try_start_1d
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 2119
    .local v3, packageInfo:Landroid/content/pm/PackageInfo;
    iget-object v0, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_2c
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1d .. :try_end_2c} :catch_5f

    .line 2129
    .end local v3           #packageInfo:Landroid/content/pm/PackageInfo;
    .local v0, buildVersion:Ljava/lang/String;
    :goto_2c
    if-eqz v5, :cond_34

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_72

    .line 2140
    :cond_34
    const-string v9, "RealTimeChatService"

    invoke-static {v9, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_43

    .line 2141
    const-string v9, "RealTimeChatService"

    const-string v10, "refreshing registration for update"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2144
    :cond_43
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2148
    .local v2, editor:Landroid/content/SharedPreferences$Editor;
    if-eqz v6, :cond_55

    .line 2149
    const-string v9, "RealTimeChatService"

    const-string v10, "saving registration"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2150
    const-string v9, "sticky_c2dm_registration_id"

    invoke-interface {v2, v9, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2152
    :cond_55
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2154
    const/4 v6, 0x0

    .line 2174
    .end local v2           #editor:Landroid/content/SharedPreferences$Editor;
    :cond_59
    :goto_59
    if-nez v6, :cond_5e

    .line 2175
    invoke-static {p0}, Lcom/google/android/apps/plus/c2dm/C2DMReceiver;->requestC2DMRegistrationId(Landroid/content/Context;)V

    .line 2178
    :cond_5e
    return-object v6

    .line 2120
    .end local v0           #buildVersion:Ljava/lang/String;
    :catch_5f
    move-exception v1

    .line 2121
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "RealTimeChatService"

    const/4 v10, 0x6

    invoke-static {v9, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_70

    .line 2122
    const-string v9, "RealTimeChatService"

    const-string v10, "Can\'t find package information for current package, continuing anyway"

    invoke-static {v9, v10, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2126
    :cond_70
    move-object v0, v5

    .restart local v0       #buildVersion:Ljava/lang/String;
    goto :goto_2c

    .line 2155
    .end local v1           #e:Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    const-wide/32 v11, 0x2932e00

    cmp-long v9, v9, v11

    if-lez v9, :cond_59

    .line 2157
    const-string v9, "RealTimeChatService"

    invoke-static {v9, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_8d

    .line 2158
    const-string v9, "RealTimeChatService"

    const-string v10, "refreshing registration for expiration"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2161
    :cond_8d
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 2165
    .restart local v2       #editor:Landroid/content/SharedPreferences$Editor;
    if-eqz v6, :cond_9f

    .line 2166
    const-string v9, "RealTimeChatService"

    const-string v10, "saving registration"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2167
    const-string v9, "sticky_c2dm_registration_id"

    invoke-interface {v2, v9, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2169
    :cond_9f
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2171
    const/4 v6, 0x0

    goto :goto_59
.end method

.method public static getStickyC2dmId(Landroid/content/Context;)Ljava/lang/String;
    .registers 7
    .parameter "context"

    .prologue
    const/4 v5, 0x0

    .line 2090
    const-string v3, "realtimechat"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2091
    .local v2, prefs:Landroid/content/SharedPreferences;
    const-string v3, "c2dm_registration_id"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2092
    .local v0, c2dmId:Ljava/lang/String;
    if-eqz v0, :cond_1d

    .line 2093
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 2094
    .local v1, edit:Landroid/content/SharedPreferences$Editor;
    const-string v3, "sticky_c2dm_registration_id"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2095
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2099
    .end local v1           #edit:Landroid/content/SharedPreferences$Editor;
    :goto_1c
    return-object v0

    .line 2097
    :cond_1d
    const-string v3, "sticky_c2dm_registration_id"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1c
.end method

.method public static handleC2DMRegistration(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "registration"

    .prologue
    .line 733
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 734
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x71

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 735
    const-string v1, "registration"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 736
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 737
    return-void
.end method

.method public static handleC2DMRegistrationError(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "error"

    .prologue
    .line 752
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 753
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x75

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 754
    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 755
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 756
    return-void
.end method

.method public static handleC2DMUnregistration(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 746
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 747
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x72

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 748
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 749
    return-void
.end method

.method private static initWakeLock(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 203
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v1, :cond_15

    .line 204
    const-string v1, "power"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 206
    .local v0, powerManager:Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "realtimechat"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 209
    .end local v0           #powerManager:Landroid/os/PowerManager;
    :cond_15
    return-void
.end method

.method private initializeBunchClient(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 8
    .parameter "account"

    .prologue
    .line 2265
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_36

    .line 2267
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    if-eqz v0, :cond_24

    .line 2270
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->disconnect()V

    .line 2271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    .line 2273
    :cond_24
    if-nez p1, :cond_37

    .line 2274
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 2275
    const-string v0, "RealTimeChatService"

    const-string v1, "action requested on null account"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2284
    :cond_36
    :goto_36
    return-void

    .line 2279
    :cond_37
    new-instance v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;

    const/4 v0, 0x0

    invoke-direct {v5, p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;B)V

    .line 2280
    .local v5, bunchClientListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$BunchClientListener;
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getBackendSetting(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getBackendSetting(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "realtime-chat-dev@bot.talk.google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5e

    const-string v4, "realtime-chat-dev@bot.talk.google.com"

    :goto_51
    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient;-><init>(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    .line 2282
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mBunchClient:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->updateClientVersion()V

    goto :goto_36

    .line 2280
    :cond_5e
    const-string v4, "realtime-chat@bot.talk.google.com"

    goto :goto_51
.end method

.method public static initiateConnection(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 682
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 683
    const-string v1, "RealTimeChatService"

    const-string v2, "initiateConnection"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :cond_10
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    .line 686
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_30

    .line 687
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 688
    const-string v1, "RealTimeChatService"

    const-string v2, "acquiring wake lock"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    :cond_2b
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 692
    :cond_30
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 693
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x6f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 694
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 695
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 696
    return-void
.end method

.method public static inviteParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "audience"

    .prologue
    .line 1062
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1063
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x14c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1064
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1065
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1066
    const-string v1, "audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1067
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static isRequestPending(I)Z
    .registers 3
    .parameter "requestId"

    .prologue
    .line 580
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->requestPending(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 881
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 882
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x14d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 883
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 884
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 885
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static logOut(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 799
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 800
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x74

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 801
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 802
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 803
    return-void
.end method

.method public static markConversationNotificationsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 1113
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1114
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1115
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1116
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1117
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static markConversationRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 1096
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1097
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x14f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1098
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1099
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1100
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static notifyUserPresenceChanged(JLjava/lang/String;Z)V
    .registers 6
    .parameter "conversationRowId"
    .parameter "userId"
    .parameter "isPresent"

    .prologue
    .line 2417
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2418
    .local v0, handler:Landroid/os/Handler;
    new-instance v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;-><init>(JLjava/lang/String;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2426
    return-void
.end method

.method public static notifyUserTypingStatusChanged(JLjava/lang/String;Ljava/lang/String;Z)V
    .registers 12
    .parameter "conversationRowId"
    .parameter "conversationId"
    .parameter "userId"
    .parameter "isTyping"

    .prologue
    .line 2437
    new-instance v6, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2438
    .local v6, handler:Landroid/os/Handler;
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$11;

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$11;-><init>(JLjava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2447
    return-void
.end method

.method public static onIntentProcessed(Landroid/content/Intent;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;Ljava/lang/Object;)V
    .registers 11
    .parameter "intent"
    .parameter "serviceResult"
    .parameter "resultValue"

    .prologue
    const/4 v6, -0x1

    .line 2458
    const-string v5, "op"

    invoke-virtual {p0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2459
    .local v2, opCode:I
    const-string v5, "rid"

    invoke-virtual {p0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 2460
    .local v3, requestId:I
    const-string v5, "account"

    invoke-virtual {p0, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 2461
    sparse-switch v2, :sswitch_data_64

    .line 2482
    :cond_15
    return-void

    :sswitch_16
    move-object v4, p2

    .line 2463
    check-cast v4, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;

    .line 2465
    .local v4, result:Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;
    const-string v5, "RealTimeChatService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3a

    .line 2466
    const-string v6, "RealTimeChatService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v5, "conversation created "

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v4, :cond_50

    const-string v5, "-1"

    :goto_2f
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2469
    :cond_3a
    sget-object v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_40
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    .line 2470
    .local v1, listener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
    invoke-virtual {v1, v3, v4, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;->onConversationCreated$2ae26fbd(ILcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V

    goto :goto_40

    .line 2466
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #listener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
    :cond_50
    iget-object v5, v4, Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;->mConversationRowId:Ljava/lang/Long;

    goto :goto_2f

    .line 2477
    .end local v4           #result:Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;
    :sswitch_53
    sget-object v5, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0       #i$:Ljava/util/Iterator;
    :goto_59
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_15

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_59

    .line 2461
    nop

    :sswitch_data_64
    .sparse-switch
        0x14a -> :sswitch_16
        0x160 -> :sswitch_53
    .end sparse-switch
.end method

.method public static registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 560
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    return-void
.end method

.method public static removeMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "messageRowId"

    .prologue
    .line 1079
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1080
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x150

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1081
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1082
    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1083
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    .registers 3
    .parameter "requestId"

    .prologue
    .line 592
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sResults:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    return-object v0
.end method

.method public static replyToInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Z)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "inviterId"
    .parameter "accept"

    .prologue
    .line 1188
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1189
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x155

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1190
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1191
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1192
    const-string v1, "inviter_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1193
    const-string v1, "accept"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1194
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static requestMoreEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"

    .prologue
    .line 1207
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1208
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x156

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1209
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1210
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1211
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static requestSuggestedParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "audience"
    .parameter "type"

    .prologue
    .line 1130
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1131
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x160

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1132
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1133
    const-string v1, "audience"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1134
    const-string v1, "type"

    invoke-virtual {p3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->getNumber()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1135
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static resetNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 828
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 829
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x154

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 830
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 831
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 832
    return-void
.end method

.method private restartConnectCycle(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 4
    .parameter "account"

    .prologue
    .line 2318
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectCount:I

    .line 2319
    const-wide/32 v0, 0x1d4c0

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mReconnectDelay:J

    .line 2320
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 2321
    return-void
.end method

.method public static retrySendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "retryMessageRowId"

    .prologue
    .line 941
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 942
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x158

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 943
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 944
    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 945
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private scheduleConnectAttempt(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 11
    .parameter "account"

    .prologue
    const/4 v8, 0x3

    .line 2327
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 2328
    .local v0, currentTime:J
    iget-wide v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLastConnectAttemptTime:J

    const-wide/16 v6, 0x3a98

    add-long v2, v4, v6

    .line 2329
    .local v2, nextAttemptTime:J
    cmp-long v4, v2, v0

    if-gez v4, :cond_2a

    .line 2330
    const-string v4, "RealTimeChatService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 2331
    const-string v4, "RealTimeChatService"

    const-string v5, "scheduling next connect attempt immediately"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2333
    :cond_1e
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    iput-object p1, v4, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 2334
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2344
    :goto_29
    return-void

    .line 2336
    :cond_2a
    const-string v4, "RealTimeChatService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_48

    .line 2337
    const-string v4, "RealTimeChatService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "scheduling next connect attempt delayed "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2340
    :cond_48
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2341
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    iput-object p1, v4, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 2342
    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    sub-long v6, v2, v0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_29
.end method

.method private scheduleLongTermConnect(Lcom/google/android/apps/plus/content/EsAccount;J)V
    .registers 13
    .parameter "account"
    .parameter "delay"

    .prologue
    .line 2291
    const-string v6, "RealTimeChatService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 2292
    const-string v6, "RealTimeChatService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "scheduling long term connect "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2294
    :cond_1d
    new-instance v1, Landroid/content/Intent;

    const-class v6, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v1, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2295
    .local v1, intent:Landroid/content/Intent;
    const-string v6, "op"

    const/16 v7, 0xe4

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2296
    const-string v6, "account_id"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2297
    const-wide/16 v6, 0x2

    mul-long v2, p2, v6

    .line 2298
    .local v2, nextDelay:J
    const-wide/32 v6, 0x6ddd00

    cmp-long v6, v2, v6

    if-lez v6, :cond_42

    .line 2299
    const-wide/32 v2, 0x6ddd00

    .line 2301
    :cond_42
    const-string v6, "reconnect_delay"

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2303
    const-string v6, "alarm"

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 2304
    .local v0, am:Landroid/app/AlarmManager;
    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    if-eqz v6, :cond_58

    .line 2305
    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 2307
    :cond_58
    const/4 v6, 0x0

    const/high16 v7, 0x4000

    invoke-static {p0, v6, v1, v7}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    .line 2309
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    add-long v4, v6, p2

    .line 2310
    .local v4, triggerTime:J
    const/4 v6, 0x2

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mLongTermConnect:Landroid/app/PendingIntent;

    invoke-virtual {v0, v6, v4, v5, v7}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 2311
    return-void
.end method

.method private sendC2DMIdToSessionServer(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 17
    .parameter "bunchClient"
    .parameter "c2dmRegistration"
    .parameter "account"

    .prologue
    .line 2216
    const-string v5, "RealTimeChatService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1d

    .line 2217
    const-string v5, "RealTimeChatService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sendC2DMIdToSessionServer: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    :cond_1d
    if-nez p2, :cond_20

    .line 2239
    :goto_1f
    return-void

    .line 2224
    :cond_20
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    .line 2226
    .local v4, toEmail:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 2228
    .local v1, context:Landroid/content/Context;
    const-string v5, "phone"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 2230
    .local v3, telephonyManager:Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v2

    .line 2232
    .local v2, deviceId:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Lcom/google/android/apps/plus/network/ClientVersion;->from(Landroid/content/Context;)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2234
    .local v0, appVersion:Ljava/lang/String;
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0802d4

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    sget-object v9, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "<iq type=\'set\' to=\'"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\'>  <dev:device-register device-id=\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\' app-id=\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v11, "\' locale=\'"

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' name=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' xmlns:dev=\'google:devices\'>    <dev:version version=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' build-type=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' device-os-version=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' device-hardware=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'/>    <dev:android-resource registration-id=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\'>      <dev:capability>com.google.hangout.RING</dev:capability>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "    </dev:android-resource> </dev:device-register>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</iq>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->write(Ljava/lang/String;)Z

    goto/16 :goto_1f
.end method

.method public static sendLocalPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "uri"

    .prologue
    .line 1025
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1026
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x159

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1027
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1028
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1029
    const-string v1, "uri"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1030
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "text"
    .parameter "uri"

    .prologue
    .line 900
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 901
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x14b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 902
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 903
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 904
    const-string v1, "message_text"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 905
    const-string v1, "uri"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 906
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendPresenceRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZZ)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "isPresent"
    .parameter "reciprocate"

    .prologue
    .line 960
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 961
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 962
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 963
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 964
    const-string v1, "is_present"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 965
    const-string v1, "reciprocate"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 966
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendRemotePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJLjava/lang/String;)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "messageRowId"
    .parameter "uri"

    .prologue
    .line 922
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 923
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 924
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 925
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 926
    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 927
    const-string v1, "uri"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 928
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendTileEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)I
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "conversationId"
    .parameter "tileType"
    .parameter "tileVersion"
    .parameter "tileEventType"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1003
    .local p6, tileEventData:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1004
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1005
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1006
    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1007
    const-string v1, "tile_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1008
    const-string v1, "tile_event_version"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1009
    const-string v1, "tile_event_type"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1010
    const-string v1, "tile_event_data"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1011
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendTypingRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/wireless/realtimechat/proto/Client$Typing$Type;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "typingType"

    .prologue
    .line 980
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 981
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 982
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 983
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 984
    const-string v1, "typing_status"

    invoke-virtual {p4}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->getNumber()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 985
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private sendUserCreateRequest$74507863(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/lang/String;)V
    .registers 9
    .parameter "bunchClient"
    .parameter "c2dmRegistration"

    .prologue
    .line 2250
    invoke-static {p0}, Lcom/google/android/apps/plus/util/AndroidUtils;->getAndroidId(Landroid/content/Context;)J

    move-result-wide v0

    .line 2251
    .local v0, androidId:J
    invoke-static {p2, v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->createUser(Ljava/lang/String;J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    .line 2253
    .local v2, userCreationRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    const-string v3, "RealTimeChatService"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 2254
    const-string v3, "RealTimeChatService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UserCreationRequest registration "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " androidId "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2257
    :cond_2f
    const/4 v3, -0x1

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z

    .line 2258
    return-void
.end method

.method public static setAcl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)I
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "acl"

    .prologue
    .line 1223
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1224
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x157

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1225
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1226
    const-string v1, "acl"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1227
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setConversationMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "muted"

    .prologue
    .line 1168
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1169
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x152

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1170
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1171
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1172
    const-string v1, "conversation_muted"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1173
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setConversationName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "name"

    .prologue
    .line 1149
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1150
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x151

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1151
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1152
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1153
    const-string v1, "conversation_name"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1154
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setCurrentConversationRowId(Ljava/lang/Long;)V
    .registers 5
    .parameter "conversationRowId"

    .prologue
    .line 765
    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    monitor-enter v1

    .line 766
    :try_start_3
    const-string v0, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 767
    const-string v0, "RealTimeChatService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setCurrentConversationRowId "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    :cond_20
    sput-object p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sCurrentConversationRowId:Ljava/lang/Long;

    .line 770
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_24

    return-void

    :catchall_24
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static setMessageFailed(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJ)I
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "conversationRowId"
    .parameter "messageRowId"

    .prologue
    .line 1044
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1045
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "op"

    const/16 v2, 0x15b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1046
    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1047
    const-string v1, "conversation_row_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1048
    const-string v1, "message_row_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1049
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static startCommand(Landroid/content/Context;Landroid/content/Intent;)I
    .registers 7
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 1238
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    sget-object v2, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1239
    .local v0, requestId:I
    const-string v1, "rid"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1240
    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1241
    const-string v1, "RealTimeChatService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 1242
    const-string v1, "RealTimeChatService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "start command request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " opCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "op"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    :cond_4a
    sget-object v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->addRequest(Ljava/lang/Object;)V

    .line 1246
    return v0
.end method

.method public static unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 569
    sget-object v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 570
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    .prologue
    .line 642
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .registers 4

    .prologue
    .line 216
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 217
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 218
    const-string v0, "RealTimeChatService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_13
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    .line 221
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    const-string v2, "ServiceThread"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;-><init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->start()V

    .line 223
    return-void
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 240
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 241
    const-string v0, "RealTimeChatService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 242
    const-string v0, "RealTimeChatService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mPingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mConnectRunnable:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ConnectRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    if-eqz v0, :cond_3b

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->quit()V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    .line 253
    :cond_3b
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    .prologue
    .line 230
    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->mServiceThread:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;->access$100(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$ServiceThread;Landroid/content/Intent;)V

    .line 232
    const/4 v0, 0x2

    return v0
.end method
