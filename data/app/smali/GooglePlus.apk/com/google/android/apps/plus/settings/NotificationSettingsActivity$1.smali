.class final Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "NotificationSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChangeNotificationsRequestComplete$6a63df5(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "account"
    .parameter "result"

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    const v1, 0x7f0800a6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 46
    :cond_1f
    return-void
.end method

.method public final onGetNotificationSettings$434dcfc8(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V
    .registers 6
    .parameter "requestId"
    .parameter "account"
    .parameter "notificationSettings"

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    #getter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$000(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    const/4 v1, 0x0

    #setter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$002(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    #setter for: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$102(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Lcom/google/android/apps/plus/content/NotificationSettingsData;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    #calls: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$200(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    const v1, 0x7f09003f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->dismissDialog(I)V

    .line 59
    :cond_3c
    return-void
.end method
