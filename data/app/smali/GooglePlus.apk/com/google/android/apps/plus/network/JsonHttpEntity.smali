.class public final Lcom/google/android/apps/plus/network/JsonHttpEntity;
.super Lorg/apache/http/entity/AbstractHttpEntity;
.source "JsonHttpEntity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Request:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">",
        "Lorg/apache/http/entity/AbstractHttpEntity;"
    }
.end annotation


# static fields
.field private static final BOUNDARY_HYPHEN_BYTES:[B

.field private static final CONTENT_TYPE_BYTES:[B

.field private static final CRLF_BYTES:[B

.field private static final DEFAULT_BOUNDARY_BYTES:[B


# instance fields
.field private final mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;"
        }
    .end annotation
.end field

.field private final mPayloadBytes:[B

.field private final mRequest:Lcom/google/android/apps/plus/json/GenericJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRequest;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 29
    const-string v0, "--"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->BOUNDARY_HYPHEN_BYTES:[B

    .line 32
    sget-object v0, Lcom/google/android/apps/plus/network/UploadMultipartHttpRequestConfiguration;->DEFAULT_BOUNDARY_BYTES:[B

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->DEFAULT_BOUNDARY_BYTES:[B

    .line 36
    const-string v0, "Content-Type: "

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CONTENT_TYPE_BYTES:[B

    .line 40
    const-string v0, "\r\n"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;TRequest;)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    .local p1, jsonGenerator:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p2, request:Lcom/google/android/apps/plus/json/GenericJson;,"TRequest;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;-><init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;[B)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;[B)V
    .registers 5
    .parameter
    .parameter
    .parameter "fileData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;TRequest;[B)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    .local p1, jsonGenerator:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p2, request:Lcom/google/android/apps/plus/json/GenericJson;,"TRequest;"
    invoke-direct {p0}, Lorg/apache/http/entity/AbstractHttpEntity;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    .line 53
    iput-object p3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    if-nez v0, :cond_18

    .line 56
    const-string v0, "application/octet-stream"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->setContentType(Ljava/lang/String;)V

    .line 57
    const-string v0, "gzip"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 61
    :goto_17
    return-void

    .line 59
    :cond_18
    const-string v0, "multipart/related"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->setContentType(Ljava/lang/String;)V

    goto :goto_17
.end method

.method private static writeBoundary(Ljava/io/OutputStream;Z)V
    .registers 3
    .parameter "stream"
    .parameter "isFinalBoundary"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->BOUNDARY_HYPHEN_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 124
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->DEFAULT_BOUNDARY_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 125
    if-eqz p1, :cond_11

    .line 126
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->BOUNDARY_HYPHEN_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 128
    :cond_11
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 129
    return-void
.end method

.method private static writeMetaData(Ljava/io/OutputStream;[B)V
    .registers 3
    .parameter "stream"
    .parameter "metaDataBytes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CONTENT_TYPE_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 134
    const-string v0, "application/json; charset=UTF-8"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 137
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 138
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 139
    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 140
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 141
    return-void
.end method

.method private static writePayload(Ljava/io/OutputStream;[B)V
    .registers 3
    .parameter "stream"
    .parameter "payloadBytes"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 145
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CONTENT_TYPE_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 146
    const-string v0, "image/jpeg"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 147
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 148
    const-string v0, "Content-Transfer-Encoding: binary"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 151
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 152
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 153
    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 154
    sget-object v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->CRLF_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 155
    return-void
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    const/4 v3, 0x0

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    if-nez v1, :cond_d

    .line 66
    new-instance v1, Ljava/io/ByteArrayInputStream;

    new-array v2, v3, [B

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 79
    :goto_c
    return-object v1

    .line 70
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    if-nez v1, :cond_1f

    .line 71
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_c

    .line 73
    :cond_1f
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 74
    .local v0, outstream:Ljava/io/ByteArrayOutputStream;
    invoke-static {v0, v3}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    .line 75
    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeMetaData(Ljava/io/OutputStream;[B)V

    .line 76
    invoke-static {v0, v3}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writePayload(Ljava/io/OutputStream;[B)V

    .line 78
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    .line 79
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_c
.end method

.method public final getContentLength()J
    .registers 3

    .prologue
    .line 86
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final isRepeatable()Z
    .registers 2

    .prologue
    .line 91
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    const/4 v0, 0x1

    return v0
.end method

.method public final isStreaming()Z
    .registers 2

    .prologue
    .line 98
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public final writeTo(Ljava/io/OutputStream;)V
    .registers 7
    .parameter "outstream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/network/JsonHttpEntity;,"Lcom/google/android/apps/plus/network/JsonHttpEntity<TRequest;>;"
    const/4 v4, 0x0

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v2, :cond_2f

    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    if-eqz v2, :cond_2f

    .line 104
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 105
    .local v0, bufferedOutstream:Ljava/io/BufferedOutputStream;
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeMetaData(Ljava/io/OutputStream;[B)V

    .line 107
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mPayloadBytes:[B

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writePayload(Ljava/io/OutputStream;[B)V

    .line 109
    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;->writeBoundary(Ljava/io/OutputStream;Z)V

    .line 110
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->flush()V

    .line 111
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V

    .line 117
    .end local v0           #bufferedOutstream:Ljava/io/BufferedOutputStream;
    :goto_2e
    return-void

    .line 112
    :cond_2f
    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v2, :cond_48

    .line 113
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, p1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 115
    .local v1, gzipOutputStream:Ljava/util/zip/GZIPOutputStream;
    iget-object v2, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mJsonGenerator:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/JsonHttpEntity;->mRequest:Lcom/google/android/apps/plus/json/GenericJson;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/apps/plus/json/EsJson;->writeToStream(Ljava/io/OutputStream;Ljava/lang/Object;)V

    .line 116
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    goto :goto_2e

    .line 118
    .end local v1           #gzipOutputStream:Ljava/util/zip/GZIPOutputStream;
    :cond_48
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "A jsonGenerator was not found!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
