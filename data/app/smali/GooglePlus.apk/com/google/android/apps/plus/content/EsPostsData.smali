.class public final Lcom/google/android/apps/plus/content/EsPostsData;
.super Ljava/lang/Object;
.source "EsPostsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;,
        Lcom/google/android/apps/plus/content/EsPostsData$ActivityStreamKeyQuery;
    }
.end annotation


# static fields
.field private static final ACTIVITY_TIMESTAMP_AND_STATUS_COLUMNS:[Ljava/lang/String;

.field private static sInitialized:Z

.field private static sLargePlayerSize:I

.field private static sMaxContentLength:Ljava/lang/Integer;

.field private static sMaxPortraitAspectRatio:F

.field private static sMinLandscapeAspectRatio:F

.field private static sStreamNamespaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSyncEnabled:Z

.field private static final sSyncLock:Ljava/lang/Object;

.field private static sWidgetStreamNamespaces:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    .line 142
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncLock:Ljava/lang/Object;

    .line 144
    sput-boolean v3, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    .line 872
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "activity_id"

    aput-object v2, v0, v1

    const-string v1, "modified"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "data_state"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->ACTIVITY_TIMESTAMP_AND_STATUS_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public static buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;
    .registers 9
    .parameter "gaiaId"
    .parameter "circleId"
    .parameter "location"
    .parameter "fromWidget"
    .parameter "view"

    .prologue
    const/16 v3, 0x7c

    .line 1999
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2000
    .local v0, keyBuilder:Ljava/lang/StringBuilder;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1a

    const-string v1, "f."

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 2001
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2003
    :cond_1a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2004
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2005
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2006
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2007
    if-eqz p2, :cond_5e

    .line 2008
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->hasCoordinates()Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 2009
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLatitudeE7()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2010
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2011
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getLongitudeE7()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2012
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2013
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbLocation;->getPrecisionMeters()D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2019
    :cond_4a
    :goto_4a
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2020
    if-eqz p3, :cond_64

    const/4 v1, 0x1

    :goto_50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2024
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2025
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2027
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 2016
    :cond_5e
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4a

    .line 2020
    :cond_64
    const/4 v1, 0x0

    goto :goto_50
.end method

.method private static calculateMediaFlags(II)I
    .registers 6
    .parameter "playerWidth"
    .parameter "playerHeight"

    .prologue
    .line 2104
    sget v2, Lcom/google/android/apps/plus/content/EsPostsData;->sLargePlayerSize:I

    if-lt p0, v2, :cond_1f

    sget v2, Lcom/google/android/apps/plus/content/EsPostsData;->sLargePlayerSize:I

    if-lt p1, v2, :cond_1f

    .line 2107
    const/16 v1, 0x200

    .line 2112
    .local v1, contentFlags:I
    :goto_a
    sget v2, Lcom/google/android/apps/plus/content/EsPostsData;->sLargePlayerSize:I

    if-ge p0, v2, :cond_12

    sget v2, Lcom/google/android/apps/plus/content/EsPostsData;->sLargePlayerSize:I

    if-lt p1, v2, :cond_1e

    .line 2113
    :cond_12
    int-to-float v2, p0

    int-to-float v3, p1

    div-float v0, v2, v3

    .line 2114
    .local v0, aspectRatio:F
    sget v2, Lcom/google/android/apps/plus/content/EsPostsData;->sMinLandscapeAspectRatio:F

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_22

    .line 2115
    or-int/lit16 v1, v1, 0x400

    .line 2121
    .end local v0           #aspectRatio:F
    :cond_1e
    :goto_1e
    return v1

    .line 2109
    .end local v1           #contentFlags:I
    :cond_1f
    const/16 v1, 0x100

    .restart local v1       #contentFlags:I
    goto :goto_a

    .line 2116
    .restart local v0       #aspectRatio:F
    :cond_22
    sget v2, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxPortraitAspectRatio:F

    cmpg-float v2, v0, v2

    if-gtz v2, :cond_1e

    .line 2117
    or-int/lit16 v1, v1, 0x800

    goto :goto_1e
.end method

.method static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 9
    .parameter "db"

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 1888
    sget-boolean v2, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    if-nez v2, :cond_8

    .line 1916
    :goto_7
    return-void

    .line 1892
    :cond_8
    invoke-static {v6, v6, v6, v3, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream_key NOT IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "activity_streams"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const-string v3, "EsPostsData"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_48

    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "deleteNonEssentialStreams deleted streams: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1894
    :cond_48
    const-string v2, "activities"

    const-string v3, "activity_id NOT IN (SELECT DISTINCT activity_id FROM activity_streams)"

    invoke-virtual {p0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1898
    .local v0, deleted:I
    const-string v2, "EsPostsData"

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6c

    .line 1899
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cleanupData deleted unreferenced activities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    :cond_6c
    invoke-static {}, Lcom/google/android/apps/plus/content/EsPostsData;->getAvailableStorage()J

    move-result-wide v2

    const-wide/32 v4, 0xf42400

    cmp-long v2, v2, v4

    if-gez v2, :cond_9b

    .line 1905
    const-string v2, "activities"

    const-string v3, "activity_id IN (SELECT activity_id FROM activity_streams WHERE sort_index > 50)"

    invoke-virtual {p0, v2, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1910
    .local v1, deletedAllCircles:I
    const-string v2, "EsPostsData"

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9b

    .line 1911
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "cleanupData deleted \"all circles\" activities: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1915
    .end local v1           #deletedAllCircles:I
    :cond_9b
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsPostsData;->deleteUnusedLocations(Landroid/database/sqlite/SQLiteDatabase;)V

    goto/16 :goto_7
.end method

.method private static createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V
    .registers 8
    .parameter "comment"
    .parameter "activityId"
    .parameter "values"

    .prologue
    .line 1541
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    .line 1543
    iget-object v0, p0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    .line 1544
    .local v0, commentId:Ljava/lang/String;
    const-string v3, "activity_id"

    invoke-virtual {p2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1545
    const-string v3, "comment_id"

    invoke-virtual {p2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    const-string v3, "author_id"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    const-string v3, "content"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1548
    const-string v3, "created"

    iget-object v4, p0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1550
    :try_start_24
    iget-object v3, p0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v3, :cond_34

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B

    move-result-object v2

    .line 1552
    .local v2, plusOneBlob:[B
    :goto_2e
    const-string v3, "plus_one_data"

    invoke-virtual {p2, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_33} :catch_36

    .line 1556
    .end local v2           #plusOneBlob:[B
    :goto_33
    return-void

    .line 1550
    :cond_34
    const/4 v2, 0x0

    goto :goto_2e

    .line 1553
    :catch_36
    move-exception v1

    .line 1554
    .local v1, exception:Ljava/io/IOException;
    const-string v3, "plus_one_data"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_33
.end method

.method public static deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1241
    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 1242
    const-string v6, "EsPostsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, ">>>>> deleteActivity id: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1245
    :cond_1d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1248
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1250
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 1253
    .local v5, streamKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x1

    :try_start_2d
    new-array v3, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v3, v6

    .line 1254
    .local v3, selectionArgs:[Ljava/lang/String;
    const-string v6, "activity_streams"

    const-string v7, "activity_id=?"

    invoke-virtual {v0, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1257
    const-string v6, "activities"

    const-string v7, "activity_id=?"

    invoke-virtual {v0, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1259
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_43
    .catchall {:try_start_2d .. :try_end_43} :catchall_63

    .line 1261
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1265
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1266
    .local v2, resolver:Landroid/content/ContentResolver;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_4e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_68

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1267
    .local v4, streamKey:Ljava/lang/String;
    invoke-static {p1, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_4e

    .line 1261
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #resolver:Landroid/content/ContentResolver;
    .end local v3           #selectionArgs:[Ljava/lang/String;
    .end local v4           #streamKey:Ljava/lang/String;
    :catchall_63
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    .line 1269
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #resolver:Landroid/content/ContentResolver;
    .restart local v3       #selectionArgs:[Ljava/lang/String;
    :cond_68
    return-void
.end method

.method public static deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "commentId"

    .prologue
    .line 1474
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1477
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v4, 0x1

    new-array v1, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p2, v1, v4

    .line 1478
    .local v1, commentArgs:[Ljava/lang/String;
    :try_start_e
    const-string v4, "SELECT activity_id FROM activity_comments WHERE comment_id = ?"

    invoke-static {v2, v4, v1}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_e .. :try_end_13} :catch_70

    move-result-object v0

    .line 1490
    .local v0, activityId:Ljava/lang/String;
    const-string v4, "EsPostsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 1491
    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, ">>>> deleteComment: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for activity: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1494
    :cond_3b
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1497
    :try_start_3e
    new-instance v3, Ljava/lang/StringBuffer;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1498
    .local v3, sb:Ljava/lang/StringBuffer;
    const-string v4, "comment_id IN("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1499
    invoke-static {p2}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1500
    const/16 v4, 0x29

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1502
    const-string v4, "activity_comments"

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1504
    const/4 v4, -0x1

    invoke-static {v2, v0, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->updateTotalCommentCountInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 1506
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_67
    .catchall {:try_start_3e .. :try_end_67} :catchall_8f

    .line 1508
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1511
    if-eqz v0, :cond_6f

    .line 1512
    invoke-static {v2, p0, p1, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 1514
    .end local v0           #activityId:Ljava/lang/String;
    .end local v3           #sb:Ljava/lang/StringBuffer;
    :cond_6f
    :goto_6f
    return-void

    .line 1484
    :catch_70
    move-exception v4

    const-string v4, "EsPostsData"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6f

    .line 1485
    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WARNING: could not find photo for the comment: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6f

    .line 1508
    .restart local v0       #activityId:Ljava/lang/String;
    :catchall_8f
    move-exception v4

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method private static deleteUnusedLocations(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 13
    .parameter "db"

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1945
    const-string v1, "location_queries"

    new-array v2, v11, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    const-string v7, "_id DESC"

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1949
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_18

    .line 1983
    :goto_17
    return-void

    .line 1953
    :cond_18
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gt v0, v11, :cond_22

    .line 1954
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_17

    .line 1960
    :cond_22
    :try_start_22
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1963
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 1964
    .local v10, selection:Ljava/lang/StringBuilder;
    const-string v0, "_id IN("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1966
    const/4 v9, 0x1

    .line 1967
    .local v9, first:Z
    :goto_30
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 1968
    if-eqz v9, :cond_47

    .line 1969
    const/4 v9, 0x0

    .line 1974
    :goto_39
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_41
    .catchall {:try_start_22 .. :try_end_41} :catchall_42

    goto :goto_30

    .line 1982
    .end local v9           #first:Z
    .end local v10           #selection:Ljava/lang/StringBuilder;
    :catchall_42
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1971
    .restart local v9       #first:Z
    .restart local v10       #selection:Ljava/lang/StringBuilder;
    :cond_47
    const/16 v0, 0x2c

    :try_start_49
    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_39

    .line 1977
    :cond_4d
    const/16 v0, 0x29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1980
    const-string v0, "location_queries"

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_5c
    .catchall {:try_start_49 .. :try_end_5c} :catchall_42

    .line 1982
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_17
.end method

.method public static doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    .registers 26
    .parameter "context"
    .parameter "account"
    .parameter "view"
    .parameter "circleId"
    .parameter "gaiaId"
    .parameter "fromWidget"
    .parameter "continuationToken"
    .parameter "maxCount"
    .parameter "listener"
    .parameter "syncState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 293
    const-string v4, "EsPostsData"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 294
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    move/from16 v2, p2

    invoke-static {v0, v1, v4, v5, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v15

    .line 295
    .local v15, streamKey:Ljava/lang/String;
    const-string v4, "EsPostsData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "doActivityStreamSync starting sync stream: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p7

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    .end local v15           #streamKey:Ljava/lang/String;
    :cond_35
    new-instance v3, Lcom/google/android/apps/plus/api/GetActivitiesOperation;

    const/4 v13, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p5

    move-object/from16 v10, p6

    move/from16 v11, p7

    move-object/from16 v12, p9

    move-object/from16 v14, p8

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 302
    .local v3, op:Lcom/google/android/apps/plus/api/GetActivitiesOperation;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->start()V

    .line 303
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_5d

    .line 304
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    throw v4

    .line 307
    :cond_5d
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->hasError()Z

    move-result v4

    if-eqz v4, :cond_90

    .line 308
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getErrorCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetActivitiesOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 312
    :cond_90
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    return-object v4
.end method

.method public static doNearbyActivitiesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "location"
    .parameter "continuationToken"
    .parameter "maxCount"
    .parameter "listener"
    .parameter "syncState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 250
    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 251
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v1, v2, p2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v9

    .line 253
    .local v9, streamKey:Ljava/lang/String;
    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "doNearbyActivitiesSync starting sync stream: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    .end local v9           #streamKey:Ljava/lang/String;
    :cond_2f
    new-instance v0, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 259
    .local v0, op:Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->start()V

    .line 261
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_4b

    .line 262
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    throw v1

    .line 265
    :cond_4b
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_7e

    .line 266
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetNearbyActivitiesOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 270
    :cond_7e
    new-instance v1, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    return-object v1
.end method

.method public static getActivityAuthorId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1780
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1783
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "author_id"

    aput-object v3, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1786
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_22

    .line 1800
    :goto_21
    return-object v5

    .line 1791
    :cond_22
    :try_start_22
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_38

    .line 1792
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_38

    .line 1793
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_33
    .catchall {:try_start_22 .. :try_end_33} :catchall_3c

    move-result-object v5

    .line 1797
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_21

    :cond_38
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_21

    :catchall_3c
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getActivityCanReshare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 1745
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1748
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "activities"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "can_reshare"

    aput-object v3, v2, v10

    const-string v3, "activity_id=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p2, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1752
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_22

    .line 1766
    :goto_21
    return-object v5

    .line 1757
    :cond_22
    :try_start_22
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_41

    .line 1758
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_41

    .line 1759
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3f

    move v1, v9

    :goto_37
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3a
    .catchall {:try_start_22 .. :try_end_3a} :catchall_45

    move-result-object v5

    .line 1763
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_21

    :cond_3f
    move v1, v10

    .line 1759
    goto :goto_37

    .line 1763
    :cond_41
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_21

    :catchall_45
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getActivityImageData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1620
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1623
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v2, "activities"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v8, "media"

    aput-object v8, v3, v4

    const-string v4, "activity_id=?"

    const/4 v8, 0x1

    new-array v5, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p2, v5, v8

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1626
    .local v13, cursor:Landroid/database/Cursor;
    if-nez v13, :cond_25

    .line 1627
    const/4 v3, 0x0

    .line 1667
    :goto_24
    return-object v3

    .line 1631
    :cond_25
    :try_start_25
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_99

    .line 1632
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_99

    .line 1633
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    .line 1634
    .local v17, mediaData:[B
    if-eqz v17, :cond_99

    move-object/from16 v0, v17

    array-length v3, v0

    if-eqz v3, :cond_99

    .line 1635
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v16

    .line 1636
    .local v16, mediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    if-eqz v16, :cond_99

    .line 1637
    const/4 v14, 0x0

    .local v14, i:I
    :goto_45
    move-object/from16 v0, v16

    array-length v3, v0

    if-ge v14, v3, :cond_99

    .line 1638
    aget-object v15, v16, v14

    .line 1639
    .local v15, media:Lcom/google/android/apps/plus/content/DbMedia;
    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/DbMedia;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_96

    .line 1640
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 1641
    .local v18, res:Landroid/content/res/Resources;
    const v3, 0x7f0d0030

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v5, v3

    .line 1643
    .local v5, mediaFetchWidth:I
    const v3, 0x7f0d0030

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v6, v3

    .line 1645
    .local v6, mediaFetchHeight:I
    new-instance v2, Lcom/google/android/apps/plus/content/MediaImageRequest;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/content/DbMedia;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .line 1649
    .local v2, request:Lcom/google/android/apps/plus/content/MediaImageRequest;
    new-instance v7, Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getDownloadUrl()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    invoke-direct/range {v7 .. v12}, Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1653
    .local v7, op:Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;->start()V

    .line 1654
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_96

    .line 1655
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_91
    .catchall {:try_start_25 .. :try_end_91} :catchall_9e

    move-result-object v3

    .line 1664
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto :goto_24

    .line 1637
    .end local v2           #request:Lcom/google/android/apps/plus/content/MediaImageRequest;
    .end local v5           #mediaFetchWidth:I
    .end local v6           #mediaFetchHeight:I
    .end local v7           #op:Lcom/google/android/apps/plus/api/DownloadImageOperationNoCache;
    .end local v18           #res:Landroid/content/res/Resources;
    :cond_96
    add-int/lit8 v14, v14, 0x1

    goto :goto_45

    .line 1664
    .end local v14           #i:I
    .end local v15           #media:Lcom/google/android/apps/plus/content/DbMedia;
    .end local v16           #mediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v17           #mediaData:[B
    :cond_99
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1667
    const/4 v3, 0x0

    goto :goto_24

    .line 1664
    :catchall_9e
    move-exception v3

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v3
.end method

.method public static getActivityIsPublic(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 1681
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1684
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "activities"

    new-array v2, v9, [Ljava/lang/String;

    const-string v3, "public"

    aput-object v3, v2, v10

    const-string v3, "activity_id=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p2, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1690
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1f
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 1691
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3e

    .line 1692
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3c

    move v1, v9

    :goto_34
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_37
    .catchall {:try_start_1f .. :try_end_37} :catchall_42

    move-result-object v5

    .line 1696
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1699
    :goto_3b
    return-object v5

    :cond_3c
    move v1, v10

    .line 1692
    goto :goto_34

    .line 1696
    :cond_3e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_3b

    :catchall_42
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static getActivityLastModifiedTime(Lcom/google/api/services/plusi/model/Update;)J
    .registers 9
    .parameter "update"

    .prologue
    .line 859
    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->updatedTimestampUsec:Ljava/lang/Long;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iget-object v6, p0, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 862
    .local v2, timestamp:J
    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    if-eqz v4, :cond_3e

    .line 863
    iget-object v4, p0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Comment;

    .line 864
    .local v0, comment:Lcom/google/api/services/plusi/model/Comment;
    iget-object v4, v0, Lcom/google/api/services/plusi/model/Comment;->updatedTimestampUsec:Ljava/lang/Long;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/Long;)J

    move-result-wide v4

    iget-object v6, v0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    goto :goto_1d

    .line 869
    .end local v0           #comment:Lcom/google/api/services/plusi/model/Comment;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_3e
    return-wide v2
.end method

.method public static getActivityPostText(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 1569
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1572
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "activities"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "media"

    aput-object v4, v2, v3

    const-string v3, "activity_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1575
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_25

    .line 1576
    const/4 v1, 0x0

    .line 1606
    :goto_24
    return-object v1

    .line 1580
    :cond_25
    :try_start_25
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_74

    .line 1581
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_74

    .line 1582
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    .line 1583
    .local v13, mediaData:[B
    if-eqz v13, :cond_74

    array-length v1, v13

    if-eqz v1, :cond_74

    .line 1584
    invoke-static {v13}, Lcom/google/android/apps/plus/content/DbMedia;->deserialize([B)[Lcom/google/android/apps/plus/content/DbMedia;

    move-result-object v12

    .line 1585
    .local v12, mediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    if-eqz v12, :cond_74

    .line 1587
    const/4 v10, 0x0

    .local v10, i:I
    :goto_43
    array-length v1, v12

    if-ge v10, v1, :cond_74

    .line 1588
    aget-object v11, v12, v10

    .line 1589
    .local v11, media:Lcom/google/android/apps/plus/content/DbMedia;
    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/DbMedia;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_62

    .line 1590
    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/DbMedia;->getDescription()Ljava/lang/String;

    move-result-object v9

    .line 1596
    .local v9, html:Ljava/lang/String;
    :goto_56
    invoke-static {v9}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_5d
    .catchall {:try_start_25 .. :try_end_5d} :catchall_79

    move-result-object v1

    .line 1603
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_24

    .line 1591
    .end local v9           #html:Ljava/lang/String;
    :cond_62
    :try_start_62
    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/DbMedia;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_71

    .line 1592
    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/DbMedia;->getTitle()Ljava/lang/String;
    :try_end_6f
    .catchall {:try_start_62 .. :try_end_6f} :catchall_79

    move-result-object v9

    .restart local v9       #html:Ljava/lang/String;
    goto :goto_56

    .line 1587
    .end local v9           #html:Ljava/lang/String;
    :cond_71
    add-int/lit8 v10, v10, 0x1

    goto :goto_43

    .line 1603
    .end local v10           #i:I
    .end local v11           #media:Lcom/google/android/apps/plus/content/DbMedia;
    .end local v12           #mediaArray:[Lcom/google/android/apps/plus/content/DbMedia;
    .end local v13           #mediaData:[B
    :cond_74
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1606
    const/4 v1, 0x0

    goto :goto_24

    .line 1603
    :catchall_79
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static getActivityStatuses(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/HashMap;
    .registers 18
    .parameter "db"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 887
    .local p1, updates:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Update;>;"
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 888
    .local v12, sb:Ljava/lang/StringBuilder;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 889
    .local v9, args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "activity_id IN ("

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 890
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/api/services/plusi/model/Update;

    .line 891
    .local v15, update:Lcom/google/api/services/plusi/model/Update;
    const-string v0, "?,"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 892
    iget-object v0, v15, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 894
    .end local v15           #update:Lcom/google/api/services/plusi/model/Update;
    :cond_2a
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 895
    const-string v0, ")"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 897
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 898
    .local v14, statuses:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;>;"
    const-string v1, "activities"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPostsData;->ACTIVITY_TIMESTAMP_AND_STATUS_COLUMNS:[Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 902
    .local v10, cursor:Landroid/database/Cursor;
    :goto_57
    :try_start_57
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 903
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 904
    .local v8, activityId:Ljava/lang/String;
    new-instance v13, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;

    const/4 v0, 0x0

    invoke-direct {v13, v0}, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;-><init>(B)V

    .line 905
    .local v13, status:Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v13, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->timestamp:J

    .line 906
    const/4 v0, 0x2

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v13, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->dataStatus:I

    .line 907
    invoke-virtual {v14, v8, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_79
    .catchall {:try_start_57 .. :try_end_79} :catchall_7a

    goto :goto_57

    .line 910
    .end local v8           #activityId:Ljava/lang/String;
    .end local v13           #status:Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;
    :catchall_7a
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_7f
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 912
    return-object v14
.end method

.method private static getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;
    .registers 14
    .parameter "db"
    .parameter "activityId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 2064
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2066
    .local v11, streamKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "activity_streams"

    sget-object v3, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStreamKeyQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "activity_id=?"

    new-array v5, v1, [Ljava/lang/String;

    aput-object p1, v5, v0

    move-object v0, p0

    move-object v7, v6

    move-object v8, v6

    move-object v9, v6

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 2069
    .local v10, cursor:Landroid/database/Cursor;
    if-nez v10, :cond_1d

    .line 2081
    :goto_1c
    return-object v11

    .line 2074
    :cond_1d
    :goto_1d
    :try_start_1d
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 2075
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2b
    .catchall {:try_start_1d .. :try_end_2b} :catchall_2c

    goto :goto_1d

    .line 2078
    :catchall_2c
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_31
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1c
.end method

.method private static getAvailableStorage()J
    .registers 9

    .prologue
    .line 2089
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v3

    .line 2090
    .local v3, path:Ljava/io/File;
    new-instance v4, Landroid/os/StatFs;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 2091
    .local v4, stat:Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v7

    int-to-long v7, v7

    mul-long v0, v5, v7

    .line 2092
    .local v0, available:J
    const-string v5, "EsPostsData"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_36

    .line 2093
    const-string v5, "EsPostsData"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getAvailableStorage: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_36} :catch_37

    .line 2099
    .end local v0           #available:J
    .end local v4           #stat:Landroid/os/StatFs;
    :cond_36
    :goto_36
    return-wide v0

    .line 2096
    :catch_37
    move-exception v2

    .line 2098
    .local v2, ex:Ljava/lang/Exception;
    const-string v5, "EsPostsData"

    const-string v6, "getAvailableStorage"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2099
    const-wide/16 v0, -0x1

    goto :goto_36
.end method

.method private static getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .registers 11
    .parameter "db"
    .parameter "commentId"

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1042
    const-string v1, "activity_comments"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "comment_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1045
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_1b

    .line 1059
    :goto_1a
    return-object v5

    .line 1049
    :cond_1b
    :try_start_1b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 1050
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 1052
    new-instance v5, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V
    :try_end_2d
    .catchall {:try_start_1b .. :try_end_2d} :catchall_42

    .line 1059
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1a

    .line 1054
    :cond_31
    const/4 v0, 0x0

    :try_start_32
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    :try_end_39
    .catchall {:try_start_32 .. :try_end_39} :catchall_42

    move-result-object v5

    .line 1059
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1a

    .line 1057
    :cond_3e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1a

    :catchall_42
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getMostRecentSortIndex(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .registers 13
    .parameter "db"
    .parameter "streamKey"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v10, 0x0

    .line 924
    const-string v1, "activity_streams"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "sort_index"

    aput-object v0, v2, v10

    const-string v3, "stream_key=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v10

    const-string v7, "sort_index ASC"

    const-string v8, "1"

    move-object v0, p0

    move-object v6, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 928
    .local v9, cursor:Landroid/database/Cursor;
    if-nez v9, :cond_1f

    move v0, v10

    .line 940
    :goto_1e
    return v0

    .line 933
    :cond_1f
    :try_start_1f
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 934
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_29
    .catchall {:try_start_1f .. :try_end_29} :catchall_33

    move-result v0

    .line 937
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1e

    :cond_2e
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move v0, v10

    .line 940
    goto :goto_1e

    .line 937
    :catchall_33
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getPostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .registers 11
    .parameter "db"
    .parameter "activityId"

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 944
    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "plus_one_data"

    aput-object v0, v2, v6

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 947
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_1b

    .line 961
    :goto_1a
    return-object v5

    .line 951
    :cond_1b
    :try_start_1b
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 952
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 954
    new-instance v5, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v5}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V
    :try_end_2d
    .catchall {:try_start_1b .. :try_end_2d} :catchall_42

    .line 961
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1a

    .line 956
    :cond_31
    const/4 v0, 0x0

    :try_start_32
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    :try_end_39
    .catchall {:try_start_32 .. :try_end_39} :catchall_42

    move-result-object v5

    .line 961
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1a

    .line 959
    :cond_3e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1a

    :catchall_42
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getStreamNamespaces(Z)Ljava/util/ArrayList;
    .registers 3
    .parameter "fromWidget"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    if-eqz p0, :cond_1c

    .line 171
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    if-nez v0, :cond_19

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 173
    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "STREAM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PHOTO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_19
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sWidgetStreamNamespaces:Ljava/util/ArrayList;

    .line 187
    :goto_1b
    return-object v0

    .line 178
    :cond_1c
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    if-nez v0, :cond_4f

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 180
    sput-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "STREAM"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "A2A"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 182
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "EVENT"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "SEARCH"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PLUSONE"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    const-string v1, "PHOTO"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_4f
    sget-object v0, Lcom/google/android/apps/plus/content/EsPostsData;->sStreamNamespaces:Ljava/util/ArrayList;

    goto :goto_1b
.end method

.method public static insertActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "streamKey"
    .parameter
    .parameter "collapserType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 434
    .local p3, updates:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Update;>;"
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 437
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 440
    :try_start_b
    invoke-static {p0, p1, v0, p3, p4}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;)V

    .line 443
    if-eqz p2, :cond_85

    .line 444
    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3b

    .line 445
    const-string v6, "EsPostsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "insertActivities in stream: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_3b
    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 450
    .local v5, values:Landroid/content/ContentValues;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getMostRecentSortIndex(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v7

    sub-int v2, v6, v7

    .line 451
    .local v2, sortIndex:I
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_4f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_85

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Update;

    .line 452
    .local v3, update:Lcom/google/api/services/plusi/model/Update;
    const-string v6, "stream_key"

    invoke-virtual {v5, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v6, "activity_id"

    iget-object v7, v3, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const-string v6, "sort_index"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 455
    const-string v6, "last_activity"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 457
    const-string v6, "activity_streams"

    const-string v7, "activity_id"

    const/4 v8, 0x4

    invoke-virtual {v0, v6, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 460
    add-int/lit8 v2, v2, 0x1

    goto :goto_4f

    .line 464
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #sortIndex:I
    .end local v3           #update:Lcom/google/api/services/plusi/model/Update;
    .end local v5           #values:Landroid/content/ContentValues;
    :cond_85
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_88
    .catchall {:try_start_b .. :try_end_88} :catchall_9a

    .line 466
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 469
    if-eqz p2, :cond_9f

    .line 471
    invoke-static {p1, p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 472
    .local v4, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 479
    .end local v4           #uri:Landroid/net/Uri;
    :cond_99
    return-void

    .line 466
    :catchall_9a
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    .line 475
    :cond_9f
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_a3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_99

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/Update;

    .line 476
    .restart local v3       #update:Lcom/google/api/services/plusi/model/Update;
    iget-object v6, v3, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-static {v0, p0, p1, v6}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_a3
.end method

.method private static insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;)V
    .registers 73
    .parameter "context"
    .parameter "account"
    .parameter "db"
    .parameter
    .parameter "collapserType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 493
    .local p3, updates:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Update;>;"
    sget-boolean v3, Lcom/google/android/apps/plus/content/EsPostsData;->sInitialized:Z

    if-nez v3, :cond_56

    .line 494
    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/plus/content/EsPostsData;->sInitialized:Z

    .line 496
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v60

    .line 498
    .local v60, res:Landroid/content/res/Resources;
    const-string v3, "window"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v67

    check-cast v67, Landroid/view/WindowManager;

    .line 500
    .local v67, winMgr:Landroid/view/WindowManager;
    new-instance v46, Landroid/util/DisplayMetrics;

    invoke-direct/range {v46 .. v46}, Landroid/util/DisplayMetrics;-><init>()V

    .line 501
    .local v46, displayMetrics:Landroid/util/DisplayMetrics;
    invoke-interface/range {v67 .. v67}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    move-object/from16 v0, v46

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 502
    move-object/from16 v0, v46

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v46

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v50

    .line 504
    .local v50, imageWidth:I
    and-int/lit8 v3, v50, -0x2

    const v4, 0x7f0d000b

    move-object/from16 v0, v60

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/content/EsPostsData;->sLargePlayerSize:I

    .line 506
    const v3, 0x7f0d019f

    move-object/from16 v0, v60

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/google/android/apps/plus/content/EsPostsData;->sMinLandscapeAspectRatio:F

    .line 507
    const v3, 0x7f0d01a0

    move-object/from16 v0, v60

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxPortraitAspectRatio:F

    .line 510
    .end local v46           #displayMetrics:Landroid/util/DisplayMetrics;
    .end local v50           #imageWidth:I
    .end local v60           #res:Landroid/content/res/Resources;
    .end local v67           #winMgr:Landroid/view/WindowManager;
    :cond_56
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v64

    .line 511
    .local v64, updateCount:I
    if-nez v64, :cond_5d

    .line 826
    :cond_5c
    return-void

    .line 515
    :cond_5d
    new-instance v54, Ljava/util/HashSet;

    invoke-direct/range {v54 .. v54}, Ljava/util/HashSet;-><init>()V

    .line 516
    .local v54, mediaUrls:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v45, Ljava/util/ArrayList;

    invoke-direct/range {v45 .. v45}, Ljava/util/ArrayList;-><init>()V

    .line 518
    .local v45, dbMedia:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/DbMedia;>;"
    invoke-static/range {p2 .. p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStatuses(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v62

    .line 520
    .local v62, statuses:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;>;"
    const-string v3, "DEFAULT"

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_36a

    const/16 v42, 0x1

    .line 523
    .local v42, collapsed:Z
    :goto_77
    new-instance v66, Landroid/content/ContentValues;

    invoke-direct/range {v66 .. v66}, Landroid/content/ContentValues;-><init>()V

    .line 524
    .local v66, values:Landroid/content/ContentValues;
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v47

    :cond_80
    :goto_80
    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5c

    invoke-interface/range {v47 .. v47}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v63

    check-cast v63, Lcom/google/api/services/plusi/model/Update;

    .line 525
    .local v63, update:Lcom/google/api/services/plusi/model/Update;
    move-object/from16 v0, v63

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    move-object/from16 v65, v0

    .line 526
    .local v65, updateId:Ljava/lang/String;
    const-string v3, "EsPostsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_db

    .line 527
    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>>> Activity id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v65

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", author id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", updated: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->isRead:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :cond_db
    invoke-static/range {v63 .. v63}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityLastModifiedTime(Lcom/google/api/services/plusi/model/Update;)J

    move-result-wide v55

    .line 534
    .local v55, modifiedTime:J
    move-object/from16 v0, v62

    move-object/from16 v1, v65

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v61

    check-cast v61, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;

    .line 535
    .local v61, status:Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;
    if-eqz v61, :cond_f9

    move-object/from16 v0, v61

    iget-wide v3, v0, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->timestamp:J

    cmp-long v3, v55, v3

    if-nez v3, :cond_f9

    move-object/from16 v0, v61

    iget v3, v0, Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;->dataStatus:I

    if-eqz v3, :cond_36e

    :cond_f9
    const/16 v41, 0x1

    .line 537
    .local v41, activityChanged:Z
    :goto_fb
    if-eqz v41, :cond_80

    .line 538
    const-wide/16 v43, 0x0

    .line 543
    .local v43, contentFlags:J
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->authorName:Ljava/lang/String;

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->photoUrl:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 546
    invoke-virtual/range {v66 .. v66}, Landroid/content/ContentValues;->clear()V

    .line 547
    invoke-interface/range {v45 .. v45}, Ljava/util/List;->clear()V

    .line 548
    invoke-virtual/range {v54 .. v54}, Ljava/util/HashSet;->clear()V

    .line 550
    const-string v3, "activity_id"

    move-object/from16 v0, v66

    move-object/from16 v1, v65

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const-string v3, "author_id"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    const-string v3, "source_name"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->sourceStreamName:Ljava/lang/String;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    const-string v4, "data_state"

    if-eqz v42, :cond_372

    const/4 v3, 0x1

    :goto_13d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v66

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 558
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->location:Lcom/google/api/services/plusi/model/Location;

    if-eqz v3, :cond_375

    .line 559
    new-instance v51, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v3, 0x3

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->location:Lcom/google/api/services/plusi/model/Location;

    move-object/from16 v0, v51

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    .line 561
    .local v51, location:Lcom/google/android/apps/plus/content/DbLocation;
    const-string v3, "loc"

    invoke-static/range {v51 .. v51}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 562
    const-wide/16 v43, 0x8

    .line 567
    .end local v51           #location:Lcom/google/android/apps/plus/content/DbLocation;
    :goto_165
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isCheckin:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_173

    .line 568
    const-wide/16 v3, 0x10

    or-long v43, v43, v3

    .line 571
    :cond_173
    sget-object v3, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    if-nez v3, :cond_188

    .line 572
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    .line 576
    :cond_188
    const-string v3, "annotation"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->annotation:Ljava/lang/String;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->annotation:Ljava/lang/String;

    if-eqz v3, :cond_37e

    .line 578
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->annotation:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v59

    .line 579
    .local v59, plaintext:Ljava/lang/String;
    invoke-virtual/range {v59 .. v59}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v3, v4, :cond_1c0

    .line 580
    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v59

    .line 582
    :cond_1c0
    const-string v3, "annotation_plaintext"

    move-object/from16 v0, v66

    move-object/from16 v1, v59

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-wide/16 v3, 0x2

    or-long v43, v43, v3

    .line 588
    .end local v59           #plaintext:Ljava/lang/String;
    :goto_1cd
    const-string v3, "title"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->title:Ljava/lang/String;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->title:Ljava/lang/String;

    if-eqz v3, :cond_387

    .line 590
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v59

    .line 591
    .restart local v59       #plaintext:Ljava/lang/String;
    invoke-virtual/range {v59 .. v59}, Ljava/lang/String;->length()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v3, v4, :cond_205

    .line 592
    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/plus/content/EsPostsData;->sMaxContentLength:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v59

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v59

    .line 594
    :cond_205
    const-string v3, "title_plaintext"

    move-object/from16 v0, v66

    move-object/from16 v1, v59

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-wide/16 v3, 0x1

    or-long v43, v43, v3

    .line 600
    .end local v59           #plaintext:Ljava/lang/String;
    :goto_212
    const/16 v57, 0x0

    .line 601
    .local v57, originalAuthorId:Ljava/lang/String;
    const/16 v58, 0x0

    .line 602
    .local v58, originalAuthorName:Ljava/lang/String;
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->originalItemId:Ljava/lang/String;

    if-eqz v3, :cond_228

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->originalItemId:Ljava/lang/String;

    move-object/from16 v0, v65

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_23a

    :cond_228
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromItemId:Ljava/lang/String;

    if-eqz v3, :cond_250

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromItemId:Ljava/lang/String;

    move-object/from16 v0, v65

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_250

    .line 606
    :cond_23a
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    if-eqz v3, :cond_390

    .line 607
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Person;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v57, v0

    .line 608
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Person;->userName:Ljava/lang/String;

    move-object/from16 v58, v0

    .line 614
    :cond_250
    :goto_250
    const-string v3, "original_author_id"

    move-object/from16 v0, v66

    move-object/from16 v1, v57

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    const-string v3, "original_author_name"

    move-object/from16 v0, v66

    move-object/from16 v1, v58

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->mediaLayout:Ljava/util/List;

    if-eqz v3, :cond_4b3

    .line 625
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->mediaLayout:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v48

    :cond_270
    :goto_270
    invoke-interface/range {v48 .. v48}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4b3

    invoke-interface/range {v48 .. v48}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v53

    check-cast v53, Lcom/google/api/services/plusi/model/MediaLayout;

    .line 626
    .local v53, mediaLayout:Lcom/google/api/services/plusi/model/MediaLayout;
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    const-string v4, "IMAGE"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3a8

    .line 627
    const/16 v18, 0x3

    .line 640
    .local v18, type:I
    :goto_28a
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    if-nez v3, :cond_3dc

    const/4 v7, 0x0

    .line 643
    .local v7, titlePlaintext:Ljava/lang/String;
    :goto_291
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    if-eqz v3, :cond_475

    .line 644
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v49

    .local v49, i$:Ljava/util/Iterator;
    :cond_29f
    :goto_29f
    invoke-interface/range {v49 .. v49}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_270

    invoke-interface/range {v49 .. v49}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v52

    check-cast v52, Lcom/google/api/services/plusi/model/MediaItem;

    .line 645
    .local v52, media:Lcom/google/api/services/plusi/model/MediaItem;
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->provider:Ljava/lang/String;

    const-string v4, "picasa"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3ea

    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->photoId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3ea

    const/16 v21, 0x1

    .line 647
    .local v21, isPWA:Z
    :goto_2c3
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->playerWidth:Ljava/lang/Integer;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v19

    .line 648
    .local v19, playerWidth:I
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->playerHeight:Ljava/lang/Integer;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v20

    .line 652
    .local v20, playerHeight:I
    const/4 v3, 0x3

    move/from16 v0, v18

    if-eq v0, v3, :cond_2dd

    const/4 v3, 0x2

    move/from16 v0, v18

    if-ne v0, v3, :cond_3ee

    :cond_2dd
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3ee

    .line 655
    const/16 v18, 0x1

    .line 666
    :cond_2e9
    :goto_2e9
    sparse-switch v18, :sswitch_data_740

    .line 711
    :cond_2ec
    :goto_2ec
    if-eqz v21, :cond_472

    move-object/from16 v0, v52

    iget-object v13, v0, Lcom/google/api/services/plusi/model/MediaItem;->photoId:Ljava/lang/String;

    .line 713
    .local v13, photoId:Ljava/lang/String;
    :goto_2f2
    new-instance v3, Lcom/google/android/apps/plus/content/DbMedia;

    move-object/from16 v0, v52

    iget-object v4, v0, Lcom/google/api/services/plusi/model/MediaItem;->contentUrl:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v5, v0, Lcom/google/api/services/plusi/model/MediaLayout;->faviconUrl:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    move-object/from16 v0, v53

    iget-object v8, v0, Lcom/google/api/services/plusi/model/MediaLayout;->description:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v9, v0, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/google/api/services/plusi/model/MediaItem;->playerUrl:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v11, v0, Lcom/google/api/services/plusi/model/MediaItem;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v12, v0, Lcom/google/api/services/plusi/model/MediaItem;->albumId:Ljava/lang/String;

    move-object/from16 v0, v52

    iget-object v14, v0, Lcom/google/api/services/plusi/model/MediaItem;->albumHtml:Ljava/lang/String;

    invoke-static {v14}, Lcom/google/android/apps/plus/content/EsPostsData;->unescapeHtml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, v52

    iget-object v15, v0, Lcom/google/api/services/plusi/model/MediaItem;->albumArtistHtml:Ljava/lang/String;

    invoke-static {v15}, Lcom/google/android/apps/plus/content/EsPostsData;->unescapeHtml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, v52

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MediaItem;->caption:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsPostsData;->unescapeHtml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MediaLayout;->contentUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v3 .. v21}, Lcom/google/android/apps/plus/content/DbMedia;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    move-object/from16 v0, v45

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 721
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->albumId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_355

    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_355

    .line 722
    const-wide/16 v3, 0x40

    or-long v43, v43, v3

    .line 725
    :cond_355
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_29f

    .line 726
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    move-object/from16 v0, v54

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_29f

    .line 520
    .end local v7           #titlePlaintext:Ljava/lang/String;
    .end local v13           #photoId:Ljava/lang/String;
    .end local v18           #type:I
    .end local v19           #playerWidth:I
    .end local v20           #playerHeight:I
    .end local v21           #isPWA:Z
    .end local v41           #activityChanged:Z
    .end local v42           #collapsed:Z
    .end local v43           #contentFlags:J
    .end local v49           #i$:Ljava/util/Iterator;
    .end local v52           #media:Lcom/google/api/services/plusi/model/MediaItem;
    .end local v53           #mediaLayout:Lcom/google/api/services/plusi/model/MediaLayout;
    .end local v55           #modifiedTime:J
    .end local v57           #originalAuthorId:Ljava/lang/String;
    .end local v58           #originalAuthorName:Ljava/lang/String;
    .end local v61           #status:Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;
    .end local v63           #update:Lcom/google/api/services/plusi/model/Update;
    .end local v65           #updateId:Ljava/lang/String;
    .end local v66           #values:Landroid/content/ContentValues;
    :cond_36a
    const/16 v42, 0x0

    goto/16 :goto_77

    .line 535
    .restart local v42       #collapsed:Z
    .restart local v55       #modifiedTime:J
    .restart local v61       #status:Lcom/google/android/apps/plus/content/EsPostsData$ActivityStatus;
    .restart local v63       #update:Lcom/google/api/services/plusi/model/Update;
    .restart local v65       #updateId:Ljava/lang/String;
    .restart local v66       #values:Landroid/content/ContentValues;
    :cond_36e
    const/16 v41, 0x0

    goto/16 :goto_fb

    .line 555
    .restart local v41       #activityChanged:Z
    .restart local v43       #contentFlags:J
    :cond_372
    const/4 v3, 0x0

    goto/16 :goto_13d

    .line 564
    :cond_375
    const-string v3, "loc"

    move-object/from16 v0, v66

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_165

    .line 585
    :cond_37e
    const-string v3, "annotation_plaintext"

    move-object/from16 v0, v66

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_1cd

    .line 597
    :cond_387
    const-string v3, "title_plaintext"

    move-object/from16 v0, v66

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_212

    .line 609
    .restart local v57       #originalAuthorId:Ljava/lang/String;
    .restart local v58       #originalAuthorName:Ljava/lang/String;
    :cond_390
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromAuthor:Lcom/google/api/services/plusi/model/Person;

    if-eqz v3, :cond_250

    .line 610
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Person;->obfuscatedId:Ljava/lang/String;

    move-object/from16 v57, v0

    .line 611
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->sharedFromOriginalAuthor:Lcom/google/api/services/plusi/model/Person;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/Person;->userName:Ljava/lang/String;

    move-object/from16 v58, v0

    goto/16 :goto_250

    .line 628
    .restart local v53       #mediaLayout:Lcom/google/api/services/plusi/model/MediaLayout;
    :cond_3a8
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    const-string v4, "VIDEO"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3b8

    .line 629
    const/16 v18, 0x2

    .restart local v18       #type:I
    goto/16 :goto_28a

    .line 630
    .end local v18           #type:I
    :cond_3b8
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    const-string v4, "SKYJAM_PREVIEW"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3c8

    .line 632
    const/16 v18, 0xb

    .restart local v18       #type:I
    goto/16 :goto_28a

    .line 633
    .end local v18           #type:I
    :cond_3c8
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    const-string v4, "SKYJAM_PREVIEW_ALBUM"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3d8

    .line 635
    const/16 v18, 0xd

    .restart local v18       #type:I
    goto/16 :goto_28a

    .line 637
    .end local v18           #type:I
    :cond_3d8
    const/16 v18, 0x1

    .restart local v18       #type:I
    goto/16 :goto_28a

    .line 640
    :cond_3dc
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_291

    .line 645
    .restart local v7       #titlePlaintext:Ljava/lang/String;
    .restart local v49       #i$:Ljava/util/Iterator;
    .restart local v52       #media:Lcom/google/api/services/plusi/model/MediaItem;
    :cond_3ea
    const/16 v21, 0x0

    goto/16 :goto_2c3

    .line 658
    .restart local v19       #playerWidth:I
    .restart local v20       #playerHeight:I
    .restart local v21       #isPWA:Z
    :cond_3ee
    move-object/from16 v0, v53

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    const-string v4, "NAMETAG"

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2e9

    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2e9

    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->photoId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_2e9

    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->albumId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeLong(Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_2e9

    .line 663
    const/16 v18, 0x3

    goto/16 :goto_2e9

    .line 668
    :sswitch_424
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_439

    .line 669
    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/content/EsPostsData;->calculateMediaFlags(II)I

    move-result v3

    int-to-long v3, v3

    or-long v43, v43, v3

    .line 673
    const-wide/16 v3, 0x20

    or-long v43, v43, v3

    .line 676
    :cond_439
    move-object/from16 v0, v52

    iget-object v3, v0, Lcom/google/api/services/plusi/model/MediaItem;->contentUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2ec

    .line 677
    const-wide/16 v3, 0x4

    or-long v43, v43, v3

    goto/16 :goto_2ec

    .line 683
    :sswitch_449
    const-wide/16 v3, 0x20

    or-long v43, v43, v3

    .line 684
    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/content/EsPostsData;->calculateMediaFlags(II)I

    move-result v3

    int-to-long v3, v3

    or-long v43, v43, v3

    .line 685
    if-eqz v21, :cond_2ec

    .line 686
    move-object/from16 v0, p2

    move-object/from16 v1, v52

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertMediaPhotoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/api/services/plusi/model/MediaItem;)V

    goto/16 :goto_2ec

    .line 689
    :sswitch_45f
    const-wide/16 v3, 0x80

    or-long v43, v43, v3

    .line 697
    invoke-static/range {v19 .. v20}, Lcom/google/android/apps/plus/content/EsPostsData;->calculateMediaFlags(II)I

    move-result v3

    int-to-long v3, v3

    or-long v43, v43, v3

    .line 701
    goto/16 :goto_2ec

    .line 706
    :sswitch_46c
    const-wide/16 v3, 0x4000

    or-long v43, v43, v3

    goto/16 :goto_2ec

    .line 711
    :cond_472
    const/4 v13, 0x0

    goto/16 :goto_2f2

    .line 730
    .end local v19           #playerWidth:I
    .end local v20           #playerHeight:I
    .end local v21           #isPWA:Z
    .end local v49           #i$:Ljava/util/Iterator;
    .end local v52           #media:Lcom/google/api/services/plusi/model/MediaItem;
    :cond_475
    new-instance v22, Lcom/google/android/apps/plus/content/DbMedia;

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MediaLayout;->contentUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/api/services/plusi/model/MediaLayout;->description:Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    move-object/from16 v26, v7

    move/from16 v37, v18

    invoke-direct/range {v22 .. v40}, Lcom/google/android/apps/plus/content/DbMedia;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    move-object/from16 v0, v45

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_270

    .line 741
    .end local v7           #titlePlaintext:Ljava/lang/String;
    .end local v18           #type:I
    .end local v53           #mediaLayout:Lcom/google/api/services/plusi/model/MediaLayout;
    :cond_4b3
    const-string v3, "media"

    invoke-static/range {v45 .. v45}, Lcom/google/android/apps/plus/content/DbMedia;->serialize(Ljava/util/List;)[B

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 742
    const-string v3, "album_id"

    const/4 v4, 0x0

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    const-string v3, "total_comment_count"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->totalCommentCount:Ljava/lang/Integer;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 745
    const-string v3, "public"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 746
    const-string v4, "acl_display"

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_670

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isRestrictedToDomain:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_670

    const v3, 0x7f0803f4

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_4fb
    move-object/from16 v0, v66

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    const-string v3, "created"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->timestamp:Ljava/lang/Long;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 748
    const-string v3, "modified"

    invoke-static/range {v55 .. v56}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 750
    const-string v3, "can_comment"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->canViewerComment:Ljava/lang/Boolean;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 751
    const-string v3, "can_reshare"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->canViewerShare:Ljava/lang/Boolean;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 752
    const-string v3, "has_muted"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->isMute:Ljava/lang/Boolean;

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 753
    const-string v4, "has_read"

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isRead:Ljava/lang/Boolean;

    if-nez v3, :cond_6d5

    const/4 v3, -0x1

    :goto_540
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v66

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 756
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->explanation:Lcom/google/api/services/plusi/model/Explanation;

    if-eqz v3, :cond_6e8

    .line 757
    const-string v4, "popular_post"

    const-string v3, "ITEM_POPULAR"

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->explanation:Lcom/google/api/services/plusi/model/Explanation;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/Explanation;->type:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6e5

    const/4 v3, 0x1

    :goto_560
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v66

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 765
    :cond_569
    :goto_569
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v3, :cond_6f8

    .line 766
    const-string v3, "plus_one_data"

    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 772
    :goto_57e
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->metadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    if-eqz v3, :cond_701

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->metadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    if-eqz v3, :cond_701

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->metadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->a2aData:Lcom/google/api/services/plusi/model/A2aData;

    if-eqz v3, :cond_701

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->metadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->a2aData:Lcom/google/api/services/plusi/model/A2aData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/A2aData;->hangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    if-eqz v3, :cond_701

    .line 775
    const-string v3, "a2a_hangout_data"

    invoke-static {}, Lcom/google/api/services/plusi/model/HangoutDataJson;->getInstance()Lcom/google/api/services/plusi/model/HangoutDataJson;

    move-result-object v4

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->metadata:Lcom/google/api/services/plusi/model/UpdateMetadata;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/UpdateMetadata;->namespaceSpecificData:Lcom/google/api/services/plusi/model/NamespaceSpecificData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/NamespaceSpecificData;->a2aData:Lcom/google/api/services/plusi/model/A2aData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/A2aData;->hangoutData:Lcom/google/api/services/plusi/model/HangoutData;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/HangoutDataJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 778
    const-wide/16 v3, 0x2000

    or-long v43, v43, v3

    .line 783
    :goto_5bf
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v3, :cond_624

    .line 784
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v3, :cond_604

    .line 785
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v27, v0

    .line 786
    .local v27, event:Lcom/google/api/services/plusi/model/PlusEvent;
    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v25, v0

    .line 789
    .local v25, eventId:Ljava/lang/String;
    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->copyRsvpFromSummary(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 790
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v23

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v22, p0

    move-object/from16 v24, p2

    move-object/from16 v26, v65

    move-object/from16 v28, v63

    invoke-static/range {v22 .. v31}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventInTransaction$6b5f16b7(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/api/services/plusi/model/Update;Ljava/lang/Long;Ljava/util/List;I)Z

    .line 793
    const-string v3, "event_id"

    move-object/from16 v0, v66

    move-object/from16 v1, v25

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    const-wide/16 v3, 0x1000

    or-long v43, v43, v3

    .line 798
    .end local v25           #eventId:Ljava/lang/String;
    .end local v27           #event:Lcom/google/api/services/plusi/model/PlusEvent;
    :cond_604
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    if-eqz v3, :cond_70a

    .line 799
    const-string v3, "embed_json"

    invoke-static {}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->getInstance()Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    move-result-object v4

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 801
    const-wide/32 v3, 0x8000

    or-long v43, v43, v3

    .line 809
    :cond_624
    :goto_624
    const-string v3, "content_flags"

    invoke-static/range {v43 .. v44}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 811
    if-eqz v61, :cond_72c

    .line 812
    const-string v3, "activities"

    const-string v4, "activity_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v65, v5, v6

    move-object/from16 v0, p2

    move-object/from16 v1, v66

    invoke-virtual {v0, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 819
    :goto_642
    if-nez v61, :cond_73a

    const/4 v3, 0x1

    :goto_645
    move-object/from16 v0, p2

    move-object/from16 v1, v65

    move-object/from16 v2, v54

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->updateMediaInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/HashSet;Z)V

    .line 822
    if-nez v42, :cond_80

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    if-eqz v3, :cond_80

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_80

    .line 823
    move-object/from16 v0, v63

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Update;->comment:Ljava/util/List;

    if-nez v61, :cond_73d

    const/4 v3, 0x1

    :goto_667
    move-object/from16 v0, p2

    move-object/from16 v1, v65

    invoke-static {v0, v1, v4, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->updateCommentsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V

    goto/16 :goto_80

    .line 746
    :cond_670
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isPrivateToChatContacts:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_684

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isPrivateToLatitudeFriends:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_68f

    :cond_684
    const v3, 0x7f0803f5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4fb

    :cond_68f
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isSharedWithExtendedNetwork:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_6a4

    const v3, 0x7f0803f6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4fb

    :cond_6a4
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isRestrictedToDomain:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_6ca

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isPublic:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_6ca

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->restrictedDomainData:Lcom/google/api/services/plusi/model/DomainData;

    if-eqz v3, :cond_6c6

    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->restrictedDomainData:Lcom/google/api/services/plusi/model/DomainData;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DomainData;->name:Ljava/lang/String;

    goto/16 :goto_4fb

    :cond_6c6
    const-string v3, ""

    goto/16 :goto_4fb

    :cond_6ca
    const v3, 0x7f0803f7

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4fb

    .line 753
    :cond_6d5
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->isRead:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6e2

    const/4 v3, 0x1

    goto/16 :goto_540

    :cond_6e2
    const/4 v3, 0x0

    goto/16 :goto_540

    .line 757
    :cond_6e5
    const/4 v3, 0x0

    goto/16 :goto_560

    .line 759
    :cond_6e8
    if-eqz v42, :cond_569

    .line 761
    const-string v3, "popular_post"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_569

    .line 768
    :cond_6f8
    const-string v3, "plus_one_data"

    move-object/from16 v0, v66

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_57e

    .line 780
    :cond_701
    const-string v3, "a2a_hangout_data"

    move-object/from16 v0, v66

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_5bf

    .line 802
    :cond_70a
    move-object/from16 v0, v63

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/EmbedClientItem;->placeReview:Lcom/google/api/services/plusi/model/PlaceReview;

    if-eqz v3, :cond_624

    .line 803
    const-string v3, "embed_json"

    invoke-static {}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->getInstance()Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    move-result-object v4

    move-object/from16 v0, v63

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    invoke-virtual {v4, v5}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v4

    move-object/from16 v0, v66

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 805
    const-wide/32 v3, 0x10000

    or-long v43, v43, v3

    goto/16 :goto_624

    .line 815
    :cond_72c
    const-string v3, "activities"

    const-string v4, "activity_id"

    const/4 v5, 0x5

    move-object/from16 v0, p2

    move-object/from16 v1, v66

    invoke-virtual {v0, v3, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto/16 :goto_642

    .line 819
    :cond_73a
    const/4 v3, 0x0

    goto/16 :goto_645

    .line 823
    :cond_73d
    const/4 v3, 0x0

    goto/16 :goto_667

    .line 666
    :sswitch_data_740
    .sparse-switch
        0x1 -> :sswitch_424
        0x2 -> :sswitch_45f
        0x3 -> :sswitch_449
        0xb -> :sswitch_46c
        0xd -> :sswitch_46c
    .end sparse-switch
.end method

.method public static insertComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "comment"

    .prologue
    .line 1394
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 1395
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>> insertComment: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for activity: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1399
    :cond_29
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1402
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1404
    :try_start_34
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1405
    .local v1, values:Landroid/content/ContentValues;
    invoke-static {p3, p2, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1407
    const-string v2, "activity_comments"

    const-string v3, "activity_id"

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1410
    const/4 v2, 0x1

    invoke-static {v0, p2, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->updateTotalCommentCountInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 1412
    iget-object v2, p3, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iget-object v3, p3, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1415
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1417
    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    :try_end_57
    .catchall {:try_start_34 .. :try_end_57} :catchall_5b

    .line 1419
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1420
    return-void

    .line 1419
    .end local v1           #values:Landroid/content/ContentValues;
    :catchall_5b
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public static insertLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/DbLocation;Ljava/util/ArrayList;)V
    .registers 21
    .parameter "context"
    .parameter "account"
    .parameter "query"
    .parameter "preciseLocation"
    .parameter "coarseLocation"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/api/LocationQuery;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1817
    .local p5, places:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/DbLocation;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1821
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v6

    .line 1823
    .local v6, queryKey:Ljava/lang/String;
    const-string v11, "location_queries"

    const-string v12, "key=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object v6, v13, v14

    invoke-virtual {v1, v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1826
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_25

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_25
    if-eqz p4, :cond_2c

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2c
    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1829
    .local v4, locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/DbLocation;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_8d

    .line 1830
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 1833
    .local v10, values:Landroid/content/ContentValues;
    const-string v11, "key"

    invoke-virtual {v10, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1835
    const-string v11, "location_queries"

    const-string v12, "key"

    invoke-virtual {v1, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v7

    .line 1837
    .local v7, rowId:J
    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-ltz v11, :cond_8d

    .line 1838
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1840
    :try_start_52
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1841
    .local v3, locationCount:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_57
    if-ge v2, v3, :cond_87

    .line 1842
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/DbLocation;

    .line 1843
    .local v5, place:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    .line 1844
    const-string v11, "qrid"

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1845
    const-string v11, "name"

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1846
    const-string v11, "location"

    invoke-static {v5}, Lcom/google/android/apps/plus/content/DbLocation;->serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1848
    const-string v11, "locations"

    const-string v12, "qrid"

    invoke-virtual {v1, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1841
    add-int/lit8 v2, v2, 0x1

    goto :goto_57

    .line 1852
    .end local v5           #place:Lcom/google/android/apps/plus/content/DbLocation;
    :cond_87
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_8a
    .catchall {:try_start_52 .. :try_end_8a} :catchall_a0

    .line 1854
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1859
    .end local v2           #i:I
    .end local v3           #locationCount:I
    .end local v7           #rowId:J
    .end local v10           #values:Landroid/content/ContentValues;
    :cond_8d
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/api/LocationQuery;->getKey()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/apps/plus/content/EsProvider;->buildLocationQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 1860
    .local v9, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v9, v12}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1861
    return-void

    .line 1854
    .end local v9           #uri:Landroid/net/Uri;
    .restart local v7       #rowId:J
    .restart local v10       #values:Landroid/content/ContentValues;
    :catchall_a0
    move-exception v11

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v11
.end method

.method public static isActivityPlusOnedByViewer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    .line 1713
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1716
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "activities"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "plus_one_data"

    aput-object v3, v2, v10

    const-string v3, "activity_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v10

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1721
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_1f
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3f

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_3f

    .line 1722
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v9

    .line 1723
    .local v9, poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    if-eqz v9, :cond_3f

    .line 1724
    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z
    :try_end_3a
    .catchall {:try_start_1f .. :try_end_3a} :catchall_44

    move-result v1

    .line 1728
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1731
    .end local v9           #poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    :goto_3e
    return v1

    .line 1728
    :cond_3f
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v1, v10

    .line 1731
    goto :goto_3e

    .line 1728
    :catchall_44
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1191
    .local p2, activityIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const-string v6, "EsPostsData"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_35

    .line 1192
    const-string v6, "EsPostsData"

    const-string v7, ">>>>> markActivitiesAsRead activity ids:"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1193
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1194
    .local v0, activityId:Ljava/lang/String;
    const-string v6, "EsPostsData"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "\t"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_14

    .line 1198
    .end local v0           #activityId:Ljava/lang/String;
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_35
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1201
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1204
    :try_start_40
    new-instance v5, Landroid/content/ContentValues;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 1205
    .local v5, values:Landroid/content/ContentValues;
    const-string v6, "has_read"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1208
    new-instance v4, Ljava/lang/StringBuffer;

    const/16 v6, 0x100

    invoke-direct {v4, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1209
    .local v4, sb:Ljava/lang/StringBuffer;
    const-string v6, "activity_id IN("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1210
    const/4 v2, 0x1

    .line 1211
    .local v2, first:Z
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3       #i$:Ljava/util/Iterator;
    :goto_61
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_83

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1212
    .restart local v0       #activityId:Ljava/lang/String;
    if-eqz v2, :cond_7d

    .line 1213
    const/4 v2, 0x0

    .line 1217
    :goto_70
    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_77
    .catchall {:try_start_40 .. :try_end_77} :catchall_78

    goto :goto_61

    .line 1225
    .end local v0           #activityId:Ljava/lang/String;
    .end local v2           #first:Z
    .end local v3           #i$:Ljava/util/Iterator;
    .end local v4           #sb:Ljava/lang/StringBuffer;
    .end local v5           #values:Landroid/content/ContentValues;
    :catchall_78
    move-exception v6

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    .line 1215
    .restart local v0       #activityId:Ljava/lang/String;
    .restart local v2       #first:Z
    .restart local v3       #i$:Ljava/util/Iterator;
    .restart local v4       #sb:Ljava/lang/StringBuffer;
    .restart local v5       #values:Landroid/content/ContentValues;
    :cond_7d
    const/16 v6, 0x2c

    :try_start_7f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_70

    .line 1220
    .end local v0           #activityId:Ljava/lang/String;
    :cond_83
    const/16 v6, 0x29

    invoke-virtual {v4, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1222
    const-string v6, "activities"

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1223
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_95
    .catchall {:try_start_7f .. :try_end_95} :catchall_78

    .line 1225
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1226
    return-void
.end method

.method public static muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "isMuted"

    .prologue
    const/4 v11, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1152
    const-string v8, "EsPostsData"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_20

    .line 1153
    const-string v8, "EsPostsData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ">>>>> muteActivity id: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    :cond_20
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1159
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1162
    :try_start_2b
    new-instance v5, Landroid/content/ContentValues;

    const/4 v8, 0x1

    invoke-direct {v5, v8}, Landroid/content/ContentValues;-><init>(I)V

    .line 1163
    .local v5, values:Landroid/content/ContentValues;
    const-string v8, "has_muted"

    if-eqz p3, :cond_6f

    :goto_35
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1164
    const-string v6, "activities"

    const-string v7, "activity_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object p2, v8, v9

    invoke-virtual {v0, v6, v5, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1166
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4c
    .catchall {:try_start_2b .. :try_end_4c} :catchall_71

    .line 1168
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1172
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 1173
    .local v4, streamKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1174
    .local v2, resolver:Landroid/content/ContentResolver;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_5b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_76

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1175
    .local v3, streamKey:Ljava/lang/String;
    invoke-static {p1, v3}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_5b

    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #resolver:Landroid/content/ContentResolver;
    .end local v3           #streamKey:Ljava/lang/String;
    .end local v4           #streamKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_6f
    move v6, v7

    .line 1163
    goto :goto_35

    .line 1168
    .end local v5           #values:Landroid/content/ContentValues;
    :catchall_71
    move-exception v6

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6

    .line 1179
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v2       #resolver:Landroid/content/ContentResolver;
    .restart local v4       #streamKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v5       #values:Landroid/content/ContentValues;
    :cond_76
    invoke-static {p1, p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildActivityViewUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1180
    return-void
.end method

.method private static notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 12
    .parameter "db"
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    const/4 v7, 0x0

    .line 2041
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 2043
    .local v2, resolver:Landroid/content/ContentResolver;
    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 2045
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2046
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2048
    invoke-static {p0, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getActivityStreams(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 2049
    .local v4, streamKeys:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2050
    .local v3, streamKey:Ljava/lang/String;
    invoke-static {p2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 2051
    .local v5, uri:Landroid/net/Uri;
    invoke-virtual {v2, v5, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_1d

    .line 2053
    .end local v3           #streamKey:Ljava/lang/String;
    .end local v5           #uri:Landroid/net/Uri;
    :cond_31
    return-void
.end method

.method public static plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "plusOne"

    .prologue
    .line 1092
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 1093
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> plusOneComment activity id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", commentId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1097
    :cond_31
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1099
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v1

    .line 1100
    .local v1, poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    if-nez v1, :cond_41

    .line 1101
    const/4 v1, 0x0

    .line 1108
    .end local v1           #poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    :goto_40
    return-object v1

    .line 1104
    .restart local v1       #poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    :cond_41
    invoke-virtual {v1, p4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->updatePlusOnedByMe(Z)V

    .line 1105
    invoke-static {v0, p3, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->replaceCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    .line 1106
    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_40
.end method

.method public static plusOnePost(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "plusOne"

    .prologue
    .line 993
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 994
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> plusOnePost activity id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    :cond_27
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 999
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getPostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v1

    .line 1000
    .local v1, poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    if-nez v1, :cond_37

    .line 1001
    const/4 v1, 0x0

    .line 1008
    .end local v1           #poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    :goto_36
    return-object v1

    .line 1004
    .restart local v1       #poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    :cond_37
    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->updatePlusOnedByMe(Z)V

    .line 1005
    invoke-static {v0, p2, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->replacePostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    .line 1006
    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_36
.end method

.method private static replaceCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .registers 10
    .parameter "db"
    .parameter "commentId"
    .parameter "poData"

    .prologue
    const/4 v5, 0x1

    .line 1067
    :try_start_1
    invoke-static {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4} :catch_1c

    move-result-object v1

    .line 1073
    .local v1, poBytes:[B
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 1074
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "plus_one_data"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1075
    const-string v3, "activity_comments"

    const-string v4, "comment_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1077
    .end local v1           #poBytes:[B
    .end local v2           #values:Landroid/content/ContentValues;
    :goto_1b
    return-void

    .line 1068
    :catch_1c
    move-exception v0

    .line 1069
    .local v0, ioe:Ljava/io/IOException;
    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not serialize DbPlusOneData "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1b
.end method

.method private static replacePostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .registers 10
    .parameter "db"
    .parameter "activityId"
    .parameter "poData"

    .prologue
    const/4 v5, 0x1

    .line 969
    :try_start_1
    invoke-static {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4} :catch_1c

    move-result-object v1

    .line 975
    .local v1, poBytes:[B
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 976
    .local v2, values:Landroid/content/ContentValues;
    const-string v3, "plus_one_data"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 977
    const-string v3, "activities"

    const-string v4, "activity_id=?"

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 979
    .end local v1           #poBytes:[B
    .end local v2           #values:Landroid/content/ContentValues;
    :goto_1b
    return-void

    .line 970
    :catch_1c
    move-exception v0

    .line 971
    .local v0, ioe:Ljava/io/IOException;
    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not serialize DbPlusOneData "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1b
.end method

.method public static syncActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 19
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 202
    sget-object v14, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncLock:Ljava/lang/Object;

    monitor-enter v14

    .line 203
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_d

    sget-boolean v1, Lcom/google/android/apps/plus/content/EsPostsData;->sSyncEnabled:Z

    if-nez v1, :cond_f

    .line 204
    :cond_d
    monitor-exit v14

    .line 231
    :goto_e
    return-void

    .line 207
    :cond_f
    const-string v1, "Activities:Sync"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 209
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/google/android/apps/plus/widget/EsWidgetProvider;

    invoke-direct {v5, p0, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v5}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v5

    array-length v1, v5

    if-lez v1, :cond_4d

    array-length v6, v5

    const/4 v1, 0x0

    :goto_34
    if-ge v1, v6, :cond_4d

    aget v7, v5, v1

    invoke-static {p0, v7}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4a

    invoke-interface {v2, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4a

    invoke-interface {v2, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v13, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4a
    add-int/lit8 v1, v1, 0x1

    goto :goto_34

    .line 210
    .local v13, widgetCircleIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_4d
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_98

    .line 211
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :goto_57
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_98

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 214
    .local v12, widgetCircleId:Ljava/lang/String;
    const-string v1, "v.whatshot"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_80

    .line 215
    const/4 v3, 0x1

    .line 216
    .local v3, view:I
    const/4 v4, 0x0

    .line 225
    .local v4, circleId:Ljava/lang/String;
    :goto_6d
    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v9, p3

    move-object/from16 v10, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_7c
    .catchall {:try_start_3 .. :try_end_7c} :catchall_7d

    goto :goto_57

    .line 231
    .end local v3           #view:I
    .end local v4           #circleId:Ljava/lang/String;
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #widgetCircleId:Ljava/lang/String;
    .end local v13           #widgetCircleIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :catchall_7d
    move-exception v1

    monitor-exit v14

    throw v1

    .line 217
    .restart local v11       #i$:Ljava/util/Iterator;
    .restart local v12       #widgetCircleId:Ljava/lang/String;
    .restart local v13       #widgetCircleIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :cond_80
    :try_start_80
    const-string v1, "v.nearby"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8b

    .line 218
    const/4 v3, 0x2

    .line 219
    .restart local v3       #view:I
    const/4 v4, 0x0

    .restart local v4       #circleId:Ljava/lang/String;
    goto :goto_6d

    .line 221
    .end local v3           #view:I
    .end local v4           #circleId:Ljava/lang/String;
    :cond_8b
    const-string v1, "v.all.circles"

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_96

    const/4 v4, 0x0

    .line 223
    .restart local v4       #circleId:Ljava/lang/String;
    :goto_94
    const/4 v3, 0x0

    .restart local v3       #view:I
    goto :goto_6d

    .end local v3           #view:I
    .end local v4           #circleId:Ljava/lang/String;
    :cond_96
    move-object v4, v12

    .line 221
    goto :goto_94

    .line 230
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v12           #widgetCircleId:Ljava/lang/String;
    :cond_98
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 231
    monitor-exit v14
    :try_end_9c
    .catchall {:try_start_80 .. :try_end_9c} :catchall_7d

    goto/16 :goto_e
.end method

.method private static unescapeHtml(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "html"

    .prologue
    .line 2125
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2128
    .end local p0
    :goto_6
    return-object p0

    .restart local p0
    :cond_7
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_6
.end method

.method public static updateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/Comment;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "comment"

    .prologue
    .line 1433
    const-string v3, "EsPostsData"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_29

    .line 1434
    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>> editComment: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for activity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1437
    :cond_29
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1440
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1442
    :try_start_34
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1443
    .local v2, values:Landroid/content/ContentValues;
    invoke-static {p3, p2, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1446
    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v3, 0x100

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1447
    .local v1, sb:Ljava/lang/StringBuffer;
    const-string v3, "comment_id IN("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1448
    iget-object v3, p3, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-static {v3}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1449
    const/16 v3, 0x29

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1451
    const-string v3, "activity_comments"

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1453
    iget-object v3, p3, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iget-object v4, p3, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iget-object v5, p3, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1456
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1458
    invoke-static {v0, p0, p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->notifyActivityChange(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    :try_end_6f
    .catchall {:try_start_34 .. :try_end_6f} :catchall_73

    .line 1460
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1461
    return-void

    .line 1460
    .end local v1           #sb:Ljava/lang/StringBuffer;
    .end local v2           #values:Landroid/content/ContentValues;
    :catchall_73
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method public static updateCommentPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "plusOneId"

    .prologue
    .line 1122
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1123
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> updateCommentPlusOneId activity id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", comment id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    :cond_27
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1129
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/content/EsPostsData;->getCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v1

    .line 1130
    .local v1, poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    if-nez v1, :cond_36

    .line 1141
    :cond_35
    :goto_35
    return-void

    .line 1134
    :cond_36
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_35

    .line 1138
    invoke-virtual {v1, p4}, Lcom/google/android/apps/plus/content/DbPlusOneData;->setId(Ljava/lang/String;)V

    .line 1139
    invoke-static {v0, p3, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->replaceCommentPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    goto :goto_35
.end method

.method private static updateCommentsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/List;Z)V
    .registers 12
    .parameter "db"
    .parameter "activityId"
    .parameter
    .parameter "newActivity"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Comment;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p2, comments:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Comment;>;"
    const/4 v7, 0x3

    .line 1340
    if-nez p3, :cond_10

    .line 1341
    const-string v3, "activity_comments"

    const-string v4, "activity_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1345
    :cond_10
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1382
    :cond_16
    return-void

    .line 1350
    :cond_17
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1351
    .local v2, values:Landroid/content/ContentValues;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_20
    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Comment;

    .line 1352
    .local v0, comment:Lcom/google/api/services/plusi/model/Comment;
    const-string v3, "EsPostsData"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6e

    .line 1353
    const-string v3, "EsPostsData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    >>>>> insertComments comment id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", author id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", content: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    :cond_6e
    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->isSpam:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_8e

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_8e

    .line 1367
    const-string v3, "EsPostsData"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 1368
    const-string v3, "EsPostsData"

    const-string v4, "    >>>>> skipping! isSpam=true"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_20

    .line 1373
    :cond_8e
    invoke-static {v0, p1, v2}, Lcom/google/android/apps/plus/content/EsPostsData;->createCommentValues(Lcom/google/api/services/plusi/model/Comment;Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 1375
    const-string v3, "activity_comments"

    const-string v4, "activity_id"

    const/4 v5, 0x5

    invoke-virtual {p0, v3, v4, v2, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1379
    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/Comment;->authorPhotoUrl:Ljava/lang/String;

    invoke-static {p0, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_20
.end method

.method private static updateMediaInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/HashSet;Z)V
    .registers 20
    .parameter "db"
    .parameter "activityId"
    .parameter
    .parameter "newActivity"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1282
    .local p2, mediaUrls:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 1283
    .local v10, existingMedia:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    if-nez p3, :cond_39

    .line 1284
    const-string v2, "media"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "thumbnail_url"

    aput-object v4, v3, v1

    const-string v4, "activity_id=?"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1288
    .local v9, cursor:Landroid/database/Cursor;
    :goto_22
    :try_start_22
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 1289
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_30
    .catchall {:try_start_22 .. :try_end_30} :catchall_31

    goto :goto_22

    .line 1292
    :catchall_31
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_36
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1296
    .end local v9           #cursor:Landroid/database/Cursor;
    :cond_39
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_af

    .line 1298
    new-instance v15, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v15, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 1299
    .local v15, values:Landroid/content/ContentValues;
    invoke-virtual/range {p2 .. p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, i$:Ljava/util/Iterator;
    :cond_49
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_af

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 1300
    .local v13, mediaUrl:Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, i:I
    :goto_56
    const/4 v1, 0x2

    if-ge v11, v1, :cond_49

    .line 1301
    const/4 v1, 0x1

    if-ne v11, v1, :cond_6f

    .line 1302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&google_plus:card_type=nonsquare"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1304
    :cond_6f
    const-string v1, "EsPostsData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8c

    .line 1305
    const-string v1, "EsPostsData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    >>>>> insertMedia: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1308
    :cond_8c
    invoke-virtual {v10, v13}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_98

    .line 1309
    invoke-virtual {v10, v13}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1300
    :goto_95
    add-int/lit8 v11, v11, 0x1

    goto :goto_56

    .line 1311
    :cond_98
    const-string v1, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v15, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1312
    const-string v1, "thumbnail_url"

    invoke-virtual {v15, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1313
    const-string v1, "media"

    const-string v2, "activity_id"

    const/4 v3, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v15, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    goto :goto_95

    .line 1322
    .end local v11           #i:I
    .end local v12           #i$:Ljava/util/Iterator;
    .end local v13           #mediaUrl:Ljava/lang/String;
    .end local v15           #values:Landroid/content/ContentValues;
    :cond_af
    invoke-virtual {v10}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .restart local v12       #i$:Ljava/util/Iterator;
    :goto_b3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 1323
    .local v14, url:Ljava/lang/String;
    const-string v1, "media"

    const-string v2, "activity_id=? AND thumbnail_url=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object v14, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_b3

    .line 1327
    .end local v14           #url:Ljava/lang/String;
    :cond_d2
    return-void
.end method

.method public static updatePostPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "plusOneId"

    .prologue
    .line 1021
    const-string v2, "EsPostsData"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 1022
    const-string v2, "EsPostsData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ">>>>> update post plusone id: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    :cond_27
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1027
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getPostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v1

    .line 1028
    .local v1, poData:Lcom/google/android/apps/plus/content/DbPlusOneData;
    if-nez v1, :cond_36

    .line 1039
    :cond_35
    :goto_35
    return-void

    .line 1032
    :cond_36
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_35

    .line 1036
    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->setId(Ljava/lang/String;)V

    .line 1037
    invoke-static {v0, p2, v1}, Lcom/google/android/apps/plus/content/EsPostsData;->replacePostPlusOneData(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    goto :goto_35
.end method

.method public static updateStreamActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .registers 25
    .parameter "context"
    .parameter "account"
    .parameter "streamKey"
    .parameter
    .parameter "collapserType"
    .parameter "oldContinuationToken"
    .parameter "newContinuationToken"
    .parameter "syncState"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Update;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 331
    .local p3, updates:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Update;>;"
    if-nez p3, :cond_7

    .line 332
    new-instance p3, Ljava/util/ArrayList;

    .end local p3           #updates:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Update;>;"
    invoke-direct/range {p3 .. p3}, Ljava/util/ArrayList;-><init>()V

    .line 335
    .restart local p3       #updates:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/Update;>;"
    :cond_7
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    .line 339
    .local v4, activitiesCount:I
    invoke-static/range {p5 .. p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 340
    const/16 p6, 0x0

    .line 343
    :cond_13
    const-string v12, "EsPostsData"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_54

    .line 344
    const-string v12, "EsPostsData"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "updateStreamActivities: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " received activities: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ,new token: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p6

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " ,old token: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p5

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_54
    if-eqz p7, :cond_5b

    .line 351
    move-object/from16 v0, p7

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    .line 355
    :cond_5b
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 358
    .local v5, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 362
    :try_start_66
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_b6

    .line 363
    const/4 v8, 0x0

    .line 365
    .local v8, sortIndex:I
    const-string v12, "activity_streams"

    const-string v13, "stream_key=?"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    aput-object p2, v14, v15

    invoke-virtual {v5, v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 375
    :goto_7a
    new-instance v11, Landroid/content/ContentValues;

    const/4 v12, 0x5

    invoke-direct {v11, v12}, Landroid/content/ContentValues;-><init>(I)V

    .line 376
    .local v11, values:Landroid/content/ContentValues;
    add-int v12, v8, v4

    add-int/lit8 v7, v12, -0x1

    .line 377
    .local v7, lastSortIndex:I
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, i$:Ljava/util/Iterator;
    :goto_88
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_c4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/api/services/plusi/model/Update;

    .line 378
    .local v9, update:Lcom/google/api/services/plusi/model/Update;
    const-string v12, "stream_key"

    move-object/from16 v0, p2

    invoke-virtual {v11, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    const-string v12, "activity_id"

    iget-object v13, v9, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    const-string v12, "sort_index"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 381
    const-string v12, "activity_streams"

    const-string v13, "activity_id"

    const/4 v14, 0x5

    invoke-virtual {v5, v12, v13, v11, v14}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 384
    add-int/lit8 v8, v8, 0x1

    goto :goto_88

    .line 368
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #lastSortIndex:I
    .end local v8           #sortIndex:I
    .end local v9           #update:Lcom/google/api/services/plusi/model/Update;
    .end local v11           #values:Landroid/content/ContentValues;
    :cond_b6
    const-string v12, "SELECT count(*) FROM activity_streams WHERE stream_key=?"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {v5, v12, v13}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v12

    long-to-int v8, v12

    .restart local v8       #sortIndex:I
    goto :goto_7a

    .line 388
    .restart local v6       #i$:Ljava/util/Iterator;
    .restart local v7       #lastSortIndex:I
    .restart local v11       #values:Landroid/content/ContentValues;
    :cond_c4
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 389
    const-string v12, "token"

    move-object/from16 v0, p6

    invoke-virtual {v11, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    const-string v12, "activity_streams"

    const-string v13, "stream_key=? AND sort_index=0"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    aput-object p2, v14, v15

    invoke-virtual {v5, v12, v11, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 396
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_102

    .line 397
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    .line 398
    const-string v12, "last_activity"

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 399
    const-string v12, "activity_streams"

    const-string v13, "stream_key=? AND sort_index=?"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    aput-object p2, v14, v15

    const/4 v15, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v5, v12, v11, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 405
    :cond_102
    if-lez v4, :cond_13c

    .line 406
    const-string v12, "EsPostsData"

    const/4 v13, 0x3

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v12

    if-eqz v12, :cond_131

    .line 407
    const-string v12, "EsPostsData"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "updateStreamActivities: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " inserting activities:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :cond_131
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static {v0, v1, v5, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivitiesInTransaction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;Ljava/lang/String;)V

    .line 413
    :cond_13c
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_13f
    .catchall {:try_start_66 .. :try_end_13f} :catchall_14f

    .line 415
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 419
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 420
    .local v10, uri:Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v10, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 421
    return-void

    .line 415
    .end local v6           #i$:Ljava/util/Iterator;
    .end local v7           #lastSortIndex:I
    .end local v8           #sortIndex:I
    .end local v10           #uri:Landroid/net/Uri;
    .end local v11           #values:Landroid/content/ContentValues;
    :catchall_14f
    move-exception v12

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v12
.end method

.method private static updateTotalCommentCountInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .registers 16
    .parameter "db"
    .parameter "activityId"
    .parameter "delta"

    .prologue
    const/4 v5, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 1518
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 1519
    .local v10, values:Landroid/content/ContentValues;
    const/4 v9, 0x0

    .line 1520
    .local v9, totalCommentCount:I
    const-string v1, "activities"

    new-array v2, v12, [Ljava/lang/String;

    const-string v0, "total_comment_count"

    aput-object v0, v2, v11

    const-string v3, "activity_id=?"

    new-array v4, v12, [Ljava/lang/String;

    aput-object p1, v4, v11

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1523
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_2a

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 1524
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1527
    :cond_2a
    const-string v0, "total_comment_count"

    add-int v1, v9, p2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1528
    const-string v0, "activities"

    const-string v1, "activity_id=?"

    new-array v2, v12, [Ljava/lang/String;

    aput-object p1, v2, v11

    invoke-virtual {p0, v0, v10, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1530
    return-void
.end method
