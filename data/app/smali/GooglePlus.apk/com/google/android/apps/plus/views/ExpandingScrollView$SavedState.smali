.class public Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ExpandingScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mExpanded:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 325
    new-instance v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    .prologue
    const/4 v0, 0x1

    .line 339
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 340
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_d

    :goto_a
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    .line 341
    return-void

    .line 340
    :cond_d
    const/4 v0, 0x0

    goto :goto_a
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 310
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Z)V
    .registers 3
    .parameter "superState"
    .parameter "expanded"

    .prologue
    .line 314
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 315
    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    .line 316
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    return v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    .prologue
    .line 320
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 321
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 322
    return-void

    .line 321
    :cond_c
    const/4 v0, 0x0

    goto :goto_8
.end method
