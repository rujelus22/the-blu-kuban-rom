.class public abstract Lcom/google/android/apps/plus/fragments/EsTabActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EsTabActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    }
.end annotation


# instance fields
.field private mCurrentTab:I

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSwipeEnabled:Z

.field private mTabContainer:Lcom/google/android/apps/plus/views/TabContainer;

.field private mTabContainerId:I

.field private final mTabs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(II)V
    .registers 4
    .parameter "defaultTab"
    .parameter "tabContainerId"

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mSwipeEnabled:Z

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    .line 57
    const v0, 0x7f0900a6

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainerId:I

    .line 58
    return-void
.end method

.method private createTab(I)V
    .registers 8
    .parameter "index"

    .prologue
    .line 298
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    move-result-object v1

    .line 299
    .local v1, tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->onCreateTab(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 302
    .local v0, fragmentManager:Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 303
    .local v3, transaction:Landroid/support/v4/app/FragmentTransaction;
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 307
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "tab."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 308
    .local v2, tag:Ljava/lang/String;
    iget-object v4, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->containerView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v4

    iget-object v5, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3, v4, v5, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 313
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 314
    return-void
.end method

.method private getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    .registers 5
    .parameter "index"

    .prologue
    .line 137
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_14

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    return-object v0
.end method

.method protected static getTabIndexForFragment(Landroid/support/v4/app/Fragment;)I
    .registers 5
    .parameter "fragment"

    .prologue
    .line 120
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, tag:Ljava/lang/String;
    if-eqz v0, :cond_36

    const-string v1, "tab."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 123
    const/4 v1, 0x4

    :try_start_f
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_16
    .catch Ljava/lang/NumberFormatException; {:try_start_f .. :try_end_16} :catch_18

    move-result v1

    .line 130
    :goto_17
    return v1

    .line 125
    :catch_18
    move-exception v1

    const-string v1, "EsEvents"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_36

    .line 126
    const-string v1, "EsEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown format for fragment tag; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_36
    const/4 v1, -0x1

    goto :goto_17
.end method

.method private onPrepareSelectedTab()V
    .registers 3

    .prologue
    .line 218
    iget v1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    move-result-object v1

    iget-object v0, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    .line 219
    .local v0, fragment:Landroid/support/v4/app/Fragment;
    iget v1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    .line 220
    instance-of v1, v0, Lcom/google/android/apps/plus/fragments/Refreshable;

    if-eqz v1, :cond_15

    .line 221
    check-cast v0, Lcom/google/android/apps/plus/fragments/Refreshable;

    .end local v0           #fragment:Landroid/support/v4/app/Fragment;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/Refreshable;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 223
    :cond_15
    return-void
.end method

.method private selectTab(I)V
    .registers 11
    .parameter "index"

    .prologue
    const/4 v4, 0x0

    .line 183
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    if-ne p1, v0, :cond_6

    .line 203
    :goto_5
    return-void

    .line 187
    :cond_6
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_29

    .line 188
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    move-result-object v0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    .line 189
    .local v8, fragment:Landroid/support/v4/app/Fragment;
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    .line 190
    instance-of v0, v8, Lcom/google/android/apps/plus/fragments/Refreshable;

    if-eqz v0, :cond_29

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_24

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 196
    :cond_24
    check-cast v8, Lcom/google/android/apps/plus/fragments/Refreshable;

    .end local v8           #fragment:Landroid/support/v4/app/Fragment;
    invoke-interface {v8, v4}, Lcom/google/android/apps/plus/fragments/Refreshable;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 199
    :cond_29
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_42

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getViewForLogging$65a8335d()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getViewForLogging$65a8335d()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v3

    if-eq v3, v2, :cond_42

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 200
    :cond_42
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    .line 201
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->updateViewVisibility()V

    .line 202
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->onPrepareSelectedTab()V

    goto :goto_5
.end method

.method private updateViewVisibility()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 272
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainer:Lcom/google/android/apps/plus/views/TabContainer;

    if-nez v2, :cond_1b

    .line 273
    iget v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainerId:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/TabContainer;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainer:Lcom/google/android/apps/plus/views/TabContainer;

    .line 274
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainer:Lcom/google/android/apps/plus/views/TabContainer;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mSwipeEnabled:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TabContainer;->setScrollEnabled(Z)V

    .line 275
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainer:Lcom/google/android/apps/plus/views/TabContainer;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/TabContainer;->setOnTabChangeListener(Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;)V

    .line 278
    :cond_1b
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabContainer:Lcom/google/android/apps/plus/views/TabContainer;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/TabContainer;->setSelectedPanel(I)V

    .line 280
    const/4 v0, 0x0

    .local v0, i:I
    :goto_23
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_52

    .line 281
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    .line 282
    .local v1, tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    iget v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    if-ne v0, v2, :cond_4c

    .line 283
    iget-object v2, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->tabButton:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setSelected(Z)V

    .line 284
    iget-object v2, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->containerView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 285
    iget-object v2, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    if-nez v2, :cond_49

    .line 286
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->createTab(I)V

    .line 280
    :cond_49
    :goto_49
    add-int/lit8 v0, v0, 0x1

    goto :goto_23

    .line 289
    :cond_4c
    iget-object v2, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->tabButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_49

    .line 292
    .end local v1           #tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    :cond_52
    return-void
.end method


# virtual methods
.method protected final addTab(III)V
    .registers 6
    .parameter "index"
    .parameter "tabButtonId"
    .parameter "tabContainerViewId"

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    move-result-object v0

    .line 96
    .local v0, tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->tabButton:Landroid/view/View;

    .line 97
    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->tabButton:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->containerView:Landroid/view/View;

    .line 99
    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 152
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getViewForLogging$65a8335d()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    return-object v0
.end method

.method protected abstract getViewForLogging$65a8335d()Lcom/google/android/apps/plus/analytics/OzViews;
.end method

.method protected final onAttachFragment(ILandroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "index"
    .parameter "fragment"

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    .line 107
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    .line 108
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    .line 165
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_18

    .line 166
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    .line 167
    .local v1, tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    iget-object v2, v1, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->tabButton:Landroid/view/View;

    if-ne p1, v2, :cond_19

    .line 168
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->selectTab(I)V

    .line 172
    .end local v1           #tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    :cond_18
    return-void

    .line 165
    .restart local v1       #tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    :cond_19
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 321
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3e

    .line 322
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10001d

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 323
    const v0, 0x7f090291

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09004a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 325
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3e

    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->getTab(I)Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    instance-of v1, v0, Lcom/google/android/apps/plus/fragments/Refreshable;

    if-eqz v1, :cond_3e

    check-cast v0, Lcom/google/android/apps/plus/fragments/Refreshable;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/Refreshable;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 328
    :cond_3e
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected abstract onCreateTab(I)Landroid/support/v4/app/Fragment;
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 84
    const-string v0, "currentTab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    .line 85
    return-void
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 256
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 257
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->updateViewVisibility()V

    .line 258
    iget v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_23

    .line 259
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_20

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_20

    .line 261
    const v0, 0x7f09023d

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mProgressBar:Landroid/widget/ProgressBar;

    .line 264
    :cond_20
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->onPrepareSelectedTab()V

    .line 266
    :cond_23
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 75
    const-string v0, "currentTab"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mCurrentTab:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    return-void
.end method

.method public final onTabSelected(I)V
    .registers 2
    .parameter "index"

    .prologue
    .line 242
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->selectTab(I)V

    .line 243
    return-void
.end method

.method public final onTabVisibilityChange(IZ)V
    .registers 6
    .parameter "index"
    .parameter "visible"

    .prologue
    .line 230
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsTabActivity;->mTabs:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;

    .line 231
    .local v0, tab:Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;
    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->containerView:Landroid/view/View;

    if-eqz p2, :cond_1a

    const/4 v1, 0x0

    :goto_d
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    if-eqz p2, :cond_19

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/EsTabActivity$Tab;->fragment:Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_19

    .line 233
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->createTab(I)V

    .line 235
    :cond_19
    return-void

    .line 231
    :cond_1a
    const/4 v1, 0x4

    goto :goto_d
.end method
