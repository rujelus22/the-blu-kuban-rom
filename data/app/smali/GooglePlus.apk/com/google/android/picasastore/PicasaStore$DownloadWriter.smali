.class final Lcom/google/android/picasastore/PicasaStore$DownloadWriter;
.super Ljava/lang/Object;
.source "PicasaStore.java"

# interfaces
.implements Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/PicasaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DownloadWriter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private mDownloadListener:Lcom/google/android/picasastore/PicasaStore$DownloadListener;

.field private mDownloadUrl:Ljava/lang/String;

.field private mId:J

.field final synthetic this$0:Lcom/google/android/picasastore/PicasaStore;


# direct methods
.method public constructor <init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V
    .registers 6
    .parameter
    .parameter "id"
    .parameter "downloadUrl"
    .parameter "listener"

    .prologue
    .line 512
    iput-object p1, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->this$0:Lcom/google/android/picasastore/PicasaStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 513
    iput-wide p2, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mId:J

    .line 514
    iput-object p4, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mDownloadUrl:Ljava/lang/String;

    .line 515
    iput-object p5, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mDownloadListener:Lcom/google/android/picasastore/PicasaStore$DownloadListener;

    .line 516
    return-void
.end method


# virtual methods
.method public final writeDataToPipe(Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V
    .registers 16
    .parameter "output"
    .parameter "object"

    .prologue
    .line 521
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "PicasaStore.download "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v11, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mId:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-static {v11}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v8

    .line 522
    .local v8, statsId:I
    const/4 v2, 0x0

    .line 523
    .local v2, is:Ljava/io/InputStream;
    new-instance v4, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v4, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 524
    .local v4, os:Ljava/io/OutputStream;
    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mDownloadListener:Lcom/google/android/picasastore/PicasaStore$DownloadListener;

    .line 526
    .local v3, listener:Lcom/google/android/picasastore/PicasaStore$DownloadListener;
    const/16 v10, 0x800

    :try_start_27
    new-array v0, v10, [B

    .line 527
    .local v0, buffer:[B
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_2c
    .catchall {:try_start_27 .. :try_end_2c} :catchall_b9
    .catch Ljava/io/IOException; {:try_start_27 .. :try_end_2c} :catch_6b
    .catch Ljava/lang/Throwable; {:try_start_27 .. :try_end_2c} :catch_8f

    move-result-wide v6

    .line 529
    .local v6, startTime:J
    :try_start_2d
    iget-object v10, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mDownloadUrl:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/picasastore/HttpUtils;->openInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 530
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 531
    .local v5, rc:I
    :goto_37
    if-lez v5, :cond_48

    .line 532
    const/4 v10, 0x0

    invoke-virtual {v4, v0, v10, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 533
    if-eqz v3, :cond_43

    .line 534
    const/4 v10, 0x0

    invoke-interface {v3, v0, v10, v5}, Lcom/google/android/picasastore/PicasaStore$DownloadListener;->onDataAvailable([BII)V

    .line 536
    :cond_43
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_46
    .catchall {:try_start_2d .. :try_end_46} :catchall_61

    move-result v5

    goto :goto_37

    .line 539
    :cond_48
    :try_start_48
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    sub-long/2addr v10, v6

    invoke-static {v10, v11}, Lcom/google/android/picasastore/MetricsUtils;->incrementNetworkOpDurationAndCount(J)V

    .line 542
    if-eqz v3, :cond_55

    .line 543
    invoke-interface {v3}, Lcom/google/android/picasastore/PicasaStore$DownloadListener;->onDownloadComplete()V
    :try_end_55
    .catchall {:try_start_48 .. :try_end_55} :catchall_b9
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_55} :catch_6b
    .catch Ljava/lang/Throwable; {:try_start_48 .. :try_end_55} :catch_8f

    .line 559
    :cond_55
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 560
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 563
    const-string v10, "picasa.download.photo_video"

    invoke-static {v8, v10}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    .line 565
    .end local v0           #buffer:[B
    .end local v5           #rc:I
    .end local v6           #startTime:J
    :goto_60
    return-void

    .line 539
    .restart local v0       #buffer:[B
    .restart local v6       #startTime:J
    :catchall_61
    move-exception v10

    :try_start_62
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v11

    sub-long/2addr v11, v6

    invoke-static {v11, v12}, Lcom/google/android/picasastore/MetricsUtils;->incrementNetworkOpDurationAndCount(J)V

    throw v10
    :try_end_6b
    .catchall {:try_start_62 .. :try_end_6b} :catchall_b9
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_6b} :catch_6b
    .catch Ljava/lang/Throwable; {:try_start_62 .. :try_end_6b} :catch_8f

    .line 545
    .end local v0           #buffer:[B
    .end local v6           #startTime:J
    :catch_6b
    move-exception v1

    .line 546
    .local v1, e:Ljava/io/IOException;
    :try_start_6c
    invoke-static {v2}, Lcom/google/android/picasastore/HttpUtils;->abortConnectionSilently(Ljava/io/InputStream;)V

    .line 552
    const-string v10, "PicasaStore"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "pipe closed early by caller? "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_83
    .catchall {:try_start_6c .. :try_end_83} :catchall_b9

    .line 553
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 560
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 563
    const-string v10, "picasa.download.photo_video"

    invoke-static {v8, v10}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    goto :goto_60

    .line 554
    .end local v1           #e:Ljava/io/IOException;
    :catch_8f
    move-exception v9

    .line 555
    .local v9, t:Ljava/lang/Throwable;
    :try_start_90
    invoke-static {v2}, Lcom/google/android/picasastore/HttpUtils;->abortConnectionSilently(Ljava/io/InputStream;)V

    .line 556
    const-string v10, "PicasaStore"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "fail to write to pipe: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;->mDownloadUrl:Ljava/lang/String;

    invoke-static {v12}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ad
    .catchall {:try_start_90 .. :try_end_ad} :catchall_b9

    .line 557
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 560
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 563
    const-string v10, "picasa.download.photo_video"

    invoke-static {v8, v10}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    goto :goto_60

    .line 559
    .end local v9           #t:Ljava/lang/Throwable;
    :catchall_b9
    move-exception v10

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 560
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 563
    const-string v11, "picasa.download.photo_video"

    invoke-static {v8, v11}, Lcom/google/android/picasastore/MetricsUtils;->endWithReport(ILjava/lang/String;)V

    throw v10
.end method
