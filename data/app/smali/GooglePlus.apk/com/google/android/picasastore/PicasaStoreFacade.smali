.class public final Lcom/google/android/picasastore/PicasaStoreFacade;
.super Ljava/lang/Object;
.source "PicasaStoreFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/PicasaStoreFacade$DummyService;,
        Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    }
.end annotation


# static fields
.field private static sCacheDir:Ljava/io/File;

.field private static sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

.field private static sNetworkReceiver:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private mAlbumCoversUri:Landroid/net/Uri;

.field private mAuthority:Ljava/lang/String;

.field private mCachedFingerprintUri:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private mFingerprintUri:Landroid/net/Uri;

.field private mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

.field private mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

.field private mPhotosUri:Landroid/net/Uri;

.field private mRecalculateFingerprintUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    .line 126
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->updatePicasaSyncInfo(Z)V

    .line 127
    return-void
.end method

.method public static broadcastOperationReport(Ljava/lang/String;JJIJJ)V
    .registers 13
    .parameter "name"
    .parameter "totalTime"
    .parameter "netTime"
    .parameter "transactionCount"
    .parameter "bytesSent"
    .parameter "bytesReceived"

    .prologue
    .line 276
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    if-eqz v2, :cond_8

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sNetworkReceiver:Ljava/lang/Class;

    if-nez v2, :cond_9

    .line 288
    :cond_8
    :goto_8
    return-void

    .line 278
    :cond_9
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    iget-object v0, v2, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    .line 279
    .local v0, context:Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sNetworkReceiver:Ljava/lang/Class;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 280
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "com.google.android.picasastore.op_report"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 281
    const-string v2, "op_name"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    const-string v2, "total_time"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 283
    const-string v2, "net_duration"

    invoke-virtual {v1, v2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 284
    const-string v2, "transaction_count"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    const-string v2, "sent_bytes"

    invoke-virtual {v1, v2, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 286
    const-string v2, "received_bytes"

    invoke-virtual {v1, v2, p8, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 287
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_8
.end method

.method public static convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;
    .registers 7
    .parameter "imageUrl"
    .parameter "maxSide"
    .parameter "centerCropped"

    .prologue
    .line 391
    invoke-static {p0}, Lcom/google/android/picasastore/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_39

    .line 392
    invoke-static {p0}, Lcom/google/android/picasastore/FIFEUtil;->getImageUrlOptions(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "I"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 393
    .local v1, hasI:Z
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 394
    .local v0, builder:Ljava/lang/StringBuilder;
    const/16 v2, 0x73

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 395
    if-eqz p2, :cond_25

    const-string v2, "-c"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    :cond_25
    if-eqz v1, :cond_2c

    const-string v2, "-I"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    :cond_2c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p0}, Lcom/google/android/picasastore/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 402
    .end local v0           #builder:Ljava/lang/StringBuilder;
    .end local v1           #hasI:Z
    :goto_38
    return-object v2

    .line 399
    :cond_39
    if-eqz p2, :cond_42

    .line 400
    const-string v2, "PicasaStore"

    const-string v3, "not a FIFE url, ignore the crop option"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    :cond_42
    invoke-static {p1, p0}, Lcom/google/android/picasastore/ImageProxyUtil;->setImageUrlSize(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_38
.end method

.method public static createCacheFile(JLjava/lang/String;)Ljava/io/File;
    .registers 15
    .parameter "photoId"
    .parameter "ext"

    .prologue
    const/4 v8, 0x0

    .line 319
    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v7

    .line 320
    .local v7, root:Ljava/io/File;
    if-nez v7, :cond_9

    move-object v0, v8

    .line 348
    :cond_8
    :goto_8
    return-object v0

    .line 324
    :cond_9
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, cacheFileName:Ljava/lang/String;
    const-wide/16 v9, 0xa

    rem-long v9, p0, v9

    long-to-int v2, v9

    .line 326
    .local v2, dirIndex:I
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "picasa--"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 334
    .local v5, folderName:Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, i:I
    :goto_2f
    const/4 v9, 0x5

    if-ge v6, v9, :cond_85

    .line 335
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 336
    .local v4, folder:Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-nez v9, :cond_43

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-eqz v9, :cond_51

    .line 337
    :cond_43
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 339
    .local v0, cacheFile:Ljava/io/File;
    :try_start_48
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 340
    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_4e
    .catch Ljava/io/IOException; {:try_start_48 .. :try_end_4e} :catch_67

    move-result v9

    if-nez v9, :cond_8

    .line 345
    .end local v0           #cacheFile:Ljava/io/File;
    :cond_51
    :goto_51
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "e"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 334
    add-int/lit8 v6, v6, 0x1

    goto :goto_2f

    .line 341
    .restart local v0       #cacheFile:Ljava/io/File;
    :catch_67
    move-exception v3

    .line 342
    .local v3, e:Ljava/io/IOException;
    const-string v9, "PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is full: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_51

    .end local v0           #cacheFile:Ljava/io/File;
    .end local v3           #e:Ljava/io/IOException;
    .end local v4           #folder:Ljava/io/File;
    :cond_85
    move-object v0, v8

    .line 348
    goto :goto_8
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;
    .registers 3
    .parameter "context"

    .prologue
    .line 114
    const-class v1, Lcom/google/android/picasastore/PicasaStoreFacade;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    if-nez v0, :cond_e

    .line 115
    new-instance v0, Lcom/google/android/picasastore/PicasaStoreFacade;

    invoke-direct {v0, p0}, Lcom/google/android/picasastore/PicasaStoreFacade;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    .line 117
    :cond_e
    sget-object v0, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 114
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .registers 8
    .parameter "albumId"
    .parameter "thumbnailUrl"
    .parameter "ext"

    .prologue
    .line 376
    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    .line 377
    .local v0, root:Ljava/io/File;
    if-nez v0, :cond_8

    const/4 v1, 0x0

    .line 378
    :goto_7
    return-object v1

    :cond_8
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "picasa_covers/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAlbumCoverKey(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_7
.end method

.method public static getAlbumCoverKey(JLjava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "albumId"
    .parameter "thumbnailUrl"

    .prologue
    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->crc64Long(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getCacheDirectory()Ljava/io/File;
    .registers 6

    .prologue
    .line 298
    const-class v3, Lcom/google/android/picasastore/PicasaStoreFacade;

    monitor-enter v3

    :try_start_3
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    if-nez v2, :cond_36

    .line 299
    sget-object v2, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 301
    .local v1, root:Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v4, "cache/com.google.android.googlephotos"

    invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 302
    sput-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_24

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_55

    move-result v2

    if-eqz v2, :cond_58

    .line 304
    :cond_24
    :try_start_24
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    const-string v4, ".nomedia"

    invoke-direct {v0, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 305
    .local v0, nomedia:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_36

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_36
    .catchall {:try_start_24 .. :try_end_36} :catchall_55
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_36} :catch_3a

    .line 315
    .end local v0           #nomedia:Ljava/io/File;
    :cond_36
    :goto_36
    :try_start_36
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;
    :try_end_38
    .catchall {:try_start_36 .. :try_end_38} :catchall_55

    monitor-exit v3

    return-object v2

    .line 307
    :catch_3a
    move-exception v2

    :try_start_3b
    const-string v2, "PicasaStore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fail to create \'.nomedia\' in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    const/4 v2, 0x0

    sput-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;
    :try_end_54
    .catchall {:try_start_3b .. :try_end_54} :catchall_55

    goto :goto_36

    .line 298
    :catchall_55
    move-exception v2

    monitor-exit v3

    throw v2

    .line 311
    :cond_58
    :try_start_58
    const-string v2, "PicasaStore"

    const-string v4, "fail to create cache dir in external storage"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const/4 v2, 0x0

    sput-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;
    :try_end_62
    .catchall {:try_start_58 .. :try_end_62} :catchall_55

    goto :goto_36
.end method

.method public static getCacheFile(JLjava/lang/String;)Ljava/io/File;
    .registers 13
    .parameter "photoId"
    .parameter "ext"

    .prologue
    const/4 v7, 0x0

    .line 352
    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v6

    .line 353
    .local v6, root:Ljava/io/File;
    if-nez v6, :cond_9

    move-object v0, v7

    .line 372
    :cond_8
    :goto_8
    return-object v0

    .line 356
    :cond_9
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 357
    .local v1, cacheFileName:Ljava/lang/String;
    const-wide/16 v8, 0xa

    rem-long v8, p0, v8

    long-to-int v2, v8

    .line 358
    .local v2, dirIndex:I
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "picasa--"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 360
    .local v4, folderName:Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, i:I
    :goto_2f
    const/4 v8, 0x5

    if-ge v5, v8, :cond_66

    .line 361
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 362
    .local v3, folder:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_3f

    move-object v0, v7

    goto :goto_8

    .line 363
    :cond_3f
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_50

    .line 364
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 365
    .local v0, cacheFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_8

    .line 369
    .end local v0           #cacheFile:Ljava/io/File;
    :cond_50
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "e"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 360
    add-int/lit8 v5, v5, 0x1

    goto :goto_2f

    .end local v3           #folder:Ljava/io/File;
    :cond_66
    move-object v0, v7

    .line 372
    goto :goto_8
.end method

.method public static setNetworkReceiver(Ljava/lang/Class;)V
    .registers 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p0, networkReceiver:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    sput-object p0, Lcom/google/android/picasastore/PicasaStoreFacade;->sNetworkReceiver:Ljava/lang/Class;

    .line 122
    return-void
.end method

.method private declared-synchronized updatePicasaSyncInfo(Z)V
    .registers 15
    .parameter "initialize"

    .prologue
    const/4 v12, -0x1

    const/4 v7, 0x0

    .line 138
    monitor-enter p0

    :try_start_3
    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 140
    .local v2, pm:Landroid/content/pm/PackageManager;
    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.google.android.picasastore.PACKAGE_METADATA_LOOKUP"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v9, 0x84

    invoke-virtual {v2, v8, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 144
    .local v4, resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 145
    .local v1, localPackageName:Ljava/lang/String;
    const/4 v5, 0x0

    .line 146
    .local v5, result:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_21
    :goto_21
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 147
    .local v3, resolveInfo:Landroid/content/pm/ResolveInfo;
    iget-object v8, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-boolean v9, v8, Landroid/content/pm/ServiceInfo;->enabled:Z

    if-eqz v9, :cond_39

    iget-object v9, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v9, v9, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v9, :cond_67

    :cond_39
    const-string v9, "PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "ignore disabled picasa sync adapter: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    .line 148
    .local v6, storeInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    :goto_4e
    if-eqz v6, :cond_21

    .line 149
    if-eqz v5, :cond_58

    iget v8, v5, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->priority:I

    iget v9, v6, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->priority:I

    if-ge v8, v9, :cond_59

    .line 150
    :cond_58
    move-object v5, v6

    .line 152
    :cond_59
    iget-object v8, v6, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_21

    .line 153
    iput-object v6, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    :try_end_63
    .catchall {:try_start_3 .. :try_end_63} :catchall_64

    goto :goto_21

    .line 138
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #localPackageName:Ljava/lang/String;
    .end local v2           #pm:Landroid/content/pm/PackageManager;
    .end local v3           #resolveInfo:Landroid/content/pm/ResolveInfo;
    .end local v4           #resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v5           #result:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    .end local v6           #storeInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    :catchall_64
    move-exception v7

    monitor-exit p0

    throw v7

    .line 147
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #localPackageName:Ljava/lang/String;
    .restart local v2       #pm:Landroid/content/pm/PackageManager;
    .restart local v3       #resolveInfo:Landroid/content/pm/ResolveInfo;
    .restart local v4       #resolveInfos:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v5       #result:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    :cond_67
    :try_start_67
    iget-object v9, v8, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-nez v9, :cond_81

    const-string v9, "PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "missing metadata: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    goto :goto_4e

    :cond_81
    const-string v10, "com.google.android.picasastore.priority"

    const/4 v11, -0x1

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    const-string v11, "com.google.android.picasastore.authority"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eq v10, v12, :cond_96

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_ac

    :cond_96
    const-string v9, "PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "missing required metadata info: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    goto :goto_4e

    :cond_ac
    new-instance v6, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v8, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-direct {v6, v8, v9, v10}, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_4e

    .line 158
    .end local v3           #resolveInfo:Landroid/content/pm/ResolveInfo;
    :cond_b4
    iput-object v5, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    .line 160
    iget-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    iget-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v7, v7, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->authority:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_123

    iput-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "content://"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "photos"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPhotosUri:Landroid/net/Uri;

    const-string v8, "fingerprint"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "force_recalculate"

    const-string v10, "1"

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mRecalculateFingerprintUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "cache_only"

    const-string v10, "1"

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mCachedFingerprintUri:Landroid/net/Uri;

    const-string v8, "albumcovers"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAlbumCoversUri:Landroid/net/Uri;
    :try_end_123
    .catchall {:try_start_67 .. :try_end_123} :catchall_64

    .line 164
    :cond_123
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final getAuthority()Ljava/lang/String;
    .registers 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    return-object v0
.end method

.method public final getFingerprintUri()Landroid/net/Uri;
    .registers 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getFingerprintUri(ZZ)Landroid/net/Uri;
    .registers 4
    .parameter "recalculate"
    .parameter "cacheOnly"

    .prologue
    .line 223
    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mRecalculateFingerprintUri:Landroid/net/Uri;

    :goto_4
    return-object v0

    :cond_5
    if-eqz p2, :cond_a

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mCachedFingerprintUri:Landroid/net/Uri;

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    goto :goto_4
.end method

.method public final getPhotoUri(J)Landroid/net/Uri;
    .registers 5
    .parameter "id"

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPhotosUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final isMaster()Z
    .registers 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final onPackageAdded$552c4e01()V
    .registers 2

    .prologue
    .line 247
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->updatePicasaSyncInfo(Z)V

    .line 248
    return-void
.end method

.method public final onPackageRemoved$552c4e01()V
    .registers 2

    .prologue
    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->updatePicasaSyncInfo(Z)V

    .line 252
    return-void
.end method
