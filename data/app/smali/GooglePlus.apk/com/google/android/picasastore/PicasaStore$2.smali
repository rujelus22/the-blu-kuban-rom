.class final Lcom/google/android/picasastore/PicasaStore$2;
.super Ljava/lang/Object;
.source "PicasaStore.java"

# interfaces
.implements Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/PicasaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
        "<",
        "Lcom/google/android/picasastore/PicasaStore$ImagePack;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/picasastore/PicasaStore;


# direct methods
.method constructor <init>(Lcom/google/android/picasastore/PicasaStore;)V
    .registers 2
    .parameter

    .prologue
    .line 608
    iput-object p1, p0, Lcom/google/android/picasastore/PicasaStore$2;->this$0:Lcom/google/android/picasastore/PicasaStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic writeDataToPipe(Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V
    .registers 8
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 608
    check-cast p2, Lcom/google/android/picasastore/PicasaStore$ImagePack;

    .end local p2
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    invoke-direct {v1, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    :try_start_7
    iget-object v0, p2, Lcom/google/android/picasastore/PicasaStore$ImagePack;->data:[B

    iget v2, p2, Lcom/google/android/picasastore/PicasaStore$ImagePack;->offset:I

    iget-object v3, p2, Lcom/google/android/picasastore/PicasaStore$ImagePack;->data:[B

    array-length v3, v3

    iget v4, p2, Lcom/google/android/picasastore/PicasaStore$ImagePack;->offset:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_3d
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_14} :catch_18
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_14} :catch_31

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_17
    return-void

    :catch_18
    move-exception v0

    :try_start_19
    const-string v2, "PicasaStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "pipe closed early by caller? "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_19 .. :try_end_2d} :catchall_3d

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_17

    :catch_31
    move-exception v0

    :try_start_32
    const-string v2, "PicasaStore"

    const-string v3, "fail to write to pipe"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_39
    .catchall {:try_start_32 .. :try_end_39} :catchall_3d

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_17

    :catchall_3d
    move-exception v0

    invoke-static {v1}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v0
.end method
