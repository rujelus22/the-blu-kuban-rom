.class abstract Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;
.super Ljava/lang/Object;
.source "FIFEUtil.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/FIFEUtil$Splitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AbstractIterator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field next:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 480
    .local p0, this:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;,"Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 481
    sget-object v0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->NOT_READY:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    .line 483
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2
    .parameter

    .prologue
    .line 480
    .local p0, this:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;,"Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator<TT;>;"
    invoke-direct {p0}, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract computeNext()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final hasNext()Z
    .registers 5

    .prologue
    .local p0, this:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;,"Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator<TT;>;"
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 498
    iget-object v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    sget-object v3, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->FAILED:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    if-ne v2, v3, :cond_e

    .line 499
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 502
    :cond_e
    sget-object v2, Lcom/google/android/picasastore/FIFEUtil$1;->$SwitchMap$com$google$android$picasastore$FIFEUtil$Splitter$AbstractIterator$State:[I

    iget-object v3, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    invoke-virtual {v3}, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_34

    .line 509
    sget-object v2, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->FAILED:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    invoke-virtual {p0}, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->computeNext()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->next:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    sget-object v3, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->DONE:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    if-eq v2, v3, :cond_30

    sget-object v0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->READY:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    move v0, v1

    :cond_30
    :goto_30
    :pswitch_30
    return v0

    :pswitch_31
    move v0, v1

    .line 506
    goto :goto_30

    .line 502
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_30
        :pswitch_31
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 524
    .local p0, this:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;,"Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 525
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 527
    :cond_c
    sget-object v0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->NOT_READY:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    .line 528
    iget-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->next:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .registers 2

    .prologue
    .line 533
    .local p0, this:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;,"Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
