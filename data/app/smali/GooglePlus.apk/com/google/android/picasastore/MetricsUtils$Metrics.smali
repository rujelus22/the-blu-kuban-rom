.class final Lcom/google/android/picasastore/MetricsUtils$Metrics;
.super Ljava/lang/Object;
.source "MetricsUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/MetricsUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Metrics"
.end annotation


# instance fields
.field public endTimestamp:J

.field public inBytes:J

.field public name:Ljava/lang/String;

.field public networkOpCount:I

.field public networkOpDuration:J

.field public nextFree:Lcom/google/android/picasastore/MetricsUtils$Metrics;

.field public outBytes:J

.field public queryResultCount:I

.field public startTimestamp:J

.field public updateCount:I


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized obtain(Ljava/lang/String;)Lcom/google/android/picasastore/MetricsUtils$Metrics;
    .registers 6
    .parameter "name"

    .prologue
    .line 70
    const-class v2, Lcom/google/android/picasastore/MetricsUtils$Metrics;

    monitor-enter v2

    :try_start_3
    sget-object v0, Lcom/google/android/picasastore/MetricsUtils;->sFreeMetrics:Lcom/google/android/picasastore/MetricsUtils$Metrics;

    .line 71
    .local v0, result:Lcom/google/android/picasastore/MetricsUtils$Metrics;
    if-nez v0, :cond_16

    .line 72
    new-instance v0, Lcom/google/android/picasastore/MetricsUtils$Metrics;

    .end local v0           #result:Lcom/google/android/picasastore/MetricsUtils$Metrics;
    invoke-direct {v0}, Lcom/google/android/picasastore/MetricsUtils$Metrics;-><init>()V

    .line 76
    .restart local v0       #result:Lcom/google/android/picasastore/MetricsUtils$Metrics;
    :goto_c
    iput-object p0, v0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->name:Ljava/lang/String;

    .line 77
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iput-wide v3, v0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->startTimestamp:J
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_1b

    .line 78
    monitor-exit v2

    return-object v0

    .line 74
    :cond_16
    :try_start_16
    iget-object v1, v0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->nextFree:Lcom/google/android/picasastore/MetricsUtils$Metrics;

    sput-object v1, Lcom/google/android/picasastore/MetricsUtils;->sFreeMetrics:Lcom/google/android/picasastore/MetricsUtils$Metrics;
    :try_end_1a
    .catchall {:try_start_16 .. :try_end_1a} :catchall_1b

    goto :goto_c

    .line 70
    .end local v0           #result:Lcom/google/android/picasastore/MetricsUtils$Metrics;
    :catchall_1b
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static declared-synchronized recycle(Lcom/google/android/picasastore/MetricsUtils$Metrics;)V
    .registers 3
    .parameter "metrics"

    .prologue
    .line 82
    const-class v1, Lcom/google/android/picasastore/MetricsUtils$Metrics;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/picasastore/MetricsUtils;->sFreeMetrics:Lcom/google/android/picasastore/MetricsUtils$Metrics;

    iput-object v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->nextFree:Lcom/google/android/picasastore/MetricsUtils$Metrics;

    .line 83
    sput-object p0, Lcom/google/android/picasastore/MetricsUtils;->sFreeMetrics:Lcom/google/android/picasastore/MetricsUtils$Metrics;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_b

    .line 84
    monitor-exit v1

    return-void

    .line 82
    :catchall_b
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final merge(Lcom/google/android/picasastore/MetricsUtils$Metrics;)V
    .registers 6
    .parameter "that"

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->queryResultCount:I

    iget v1, p1, Lcom/google/android/picasastore/MetricsUtils$Metrics;->queryResultCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->queryResultCount:I

    .line 99
    iget v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->updateCount:I

    iget v1, p1, Lcom/google/android/picasastore/MetricsUtils$Metrics;->updateCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->updateCount:I

    .line 100
    iget-wide v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->inBytes:J

    iget-wide v2, p1, Lcom/google/android/picasastore/MetricsUtils$Metrics;->inBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->inBytes:J

    .line 101
    iget-wide v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->outBytes:J

    iget-wide v2, p1, Lcom/google/android/picasastore/MetricsUtils$Metrics;->outBytes:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->outBytes:J

    .line 102
    iget-wide v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpDuration:J

    iget-wide v2, p1, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpDuration:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpDuration:J

    .line 103
    iget v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpCount:I

    iget v1, p1, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpCount:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpCount:I

    .line 104
    return-void
.end method

.method public final recycle()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->name:Ljava/lang/String;

    .line 88
    iput v1, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->queryResultCount:I

    .line 89
    iput v1, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->updateCount:I

    .line 90
    iput-wide v2, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->inBytes:J

    .line 91
    iput-wide v2, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->outBytes:J

    .line 92
    iput-wide v2, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpDuration:J

    .line 93
    iput v1, p0, Lcom/google/android/picasastore/MetricsUtils$Metrics;->networkOpCount:I

    .line 94
    invoke-static {p0}, Lcom/google/android/picasastore/MetricsUtils$Metrics;->recycle(Lcom/google/android/picasastore/MetricsUtils$Metrics;)V

    .line 95
    return-void
.end method
