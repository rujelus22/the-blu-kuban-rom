.class public final Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadataOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadataOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private eventTimestamp_:J


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 57670
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 57780
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    .line 57671
    return-void
.end method

.method static synthetic access$78300()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 1

    .prologue
    .line 57665
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 3

    .prologue
    .line 57690
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57751
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 57752
    .local v0, tag:I
    sparse-switch v0, :sswitch_data_28

    .line 57757
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 57759
    :sswitch_d
    return-object p0

    .line 57764
    :sswitch_e
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57765
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    goto :goto_0

    .line 57769
    :sswitch_1b
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57770
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->eventTimestamp_:J

    goto :goto_0

    .line 57752
    :sswitch_data_28
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x10 -> :sswitch_1b
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 57665
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    .registers 3

    .prologue
    .line 57698
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    .line 57699
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 57700
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 57702
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 57665
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    .registers 6

    .prologue
    .line 57716
    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;B)V

    .line 57717
    .local v1, result:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57718
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 57719
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 57720
    const/4 v2, 0x1

    .line 57722
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->conversationId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->access$78502(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57723
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    .line 57724
    or-int/lit8 v2, v2, 0x2

    .line 57726
    :cond_1b
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->eventTimestamp_:J

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->eventTimestamp_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->access$78602(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;J)J

    .line 57727
    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->access$78702(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;I)I

    .line 57728
    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 57665
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 57665
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 3

    .prologue
    .line 57681
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 57682
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    .line 57683
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57684
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->eventTimestamp_:J

    .line 57685
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57686
    return-object p0
.end method

.method public final clearConversationId()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 2

    .prologue
    .line 57804
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57805
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    .line 57807
    return-object p0
.end method

.method public final clearEventTimestamp()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 3

    .prologue
    .line 57830
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57831
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->eventTimestamp_:J

    .line 57833
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 57665
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 57665
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 57665
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getConversationId()Ljava/lang/String;
    .registers 4

    .prologue
    .line 57785
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    .line 57786
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_f

    .line 57787
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 57788
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    .line 57791
    .end local v1           #s:Ljava/lang/String;
    :goto_e
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_e
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 57665
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 57665
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;
    .registers 2

    .prologue
    .line 57694
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final getEventTimestamp()J
    .registers 3

    .prologue
    .line 57821
    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->eventTimestamp_:J

    return-wide v0
.end method

.method public final hasConversationId()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 57782
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasEventTimestamp()Z
    .registers 3

    .prologue
    .line 57818
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 57743
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57665
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 57665
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57665
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 4
    .parameter "other"

    .prologue
    .line 57732
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 57739
    :cond_6
    :goto_6
    return-object p0

    .line 57733
    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->hasConversationId()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 57734
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->setConversationId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    .line 57736
    :cond_14
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->hasEventTimestamp()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 57737
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata;->getEventTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->setEventTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;

    goto :goto_6
.end method

.method public final setConversationId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 57795
    if-nez p1, :cond_8

    .line 57796
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 57798
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57799
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->conversationId_:Ljava/lang/Object;

    .line 57801
    return-object p0
.end method

.method public final setEventTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;
    .registers 4
    .parameter "value"

    .prologue
    .line 57824
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->bitField0_:I

    .line 57825
    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$EventMetadata$Builder;->eventTimestamp_:J

    .line 57827
    return-object p0
.end method
