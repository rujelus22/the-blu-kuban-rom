.class public final Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequestOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequestOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private count_:I

.field private participantId_:Lcom/google/protobuf/LazyStringList;

.field private stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

.field private suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 40076
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 40231
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    .line 40308
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->ALL:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    .line 40332
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 40077
    return-void
.end method

.method static synthetic access$55300()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 1

    .prologue
    .line 40071
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3

    .prologue
    .line 40100
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureParticipantIdIsMutable()V
    .registers 3

    .prologue
    .line 40233
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_16

    .line 40234
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    .line 40235
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40237
    :cond_16
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 9
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    .line 40185
    .local v2, tag:I
    sparse-switch v2, :sswitch_data_58

    .line 40190
    invoke-virtual {p0, p1, p2, v2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 40192
    :sswitch_d
    return-object p0

    .line 40197
    :sswitch_e
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->ensureParticipantIdIsMutable()V

    .line 40198
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 40202
    :sswitch_1b
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    move-result-object v1

    .line 40203
    .local v1, subBuilder:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->hasStubbyInfo()Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 40204
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->getStubbyInfo()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    .line 40206
    :cond_2c
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 40207
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->setStubbyInfo(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    goto :goto_0

    .line 40211
    .end local v1           #subBuilder:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;
    :sswitch_37
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40212
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->count_:I

    goto :goto_0

    .line 40216
    :sswitch_44
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 40217
    .local v0, rawValue:I
    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    move-result-object v3

    .line 40218
    .local v3, value:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;
    if-eqz v3, :cond_0

    .line 40219
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40220
    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    goto :goto_0

    .line 40185
    nop

    :sswitch_data_58
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_1b
        0x18 -> :sswitch_37
        0x20 -> :sswitch_44
    .end sparse-switch
.end method


# virtual methods
.method public final addAllParticipantId(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;"
        }
    .end annotation

    .prologue
    .line 40269
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->ensureParticipantIdIsMutable()V

    .line 40270
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 40272
    return-object p0
.end method

.method public final addParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 40259
    if-nez p1, :cond_8

    .line 40260
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40262
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->ensureParticipantIdIsMutable()V

    .line 40263
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 40265
    return-object p0
.end method

.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40071
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    .registers 3

    .prologue
    .line 40108
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    .line 40109
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 40110
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 40112
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40071
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    .registers 6

    .prologue
    .line 40126
    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;-><init>(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;B)V

    .line 40127
    .local v1, result:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40128
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 40129
    .local v2, to_bitField0_:I
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1f

    .line 40130
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    .line 40132
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40134
    :cond_1f
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55502(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 40135
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2a

    .line 40136
    const/4 v2, 0x1

    .line 40138
    :cond_2a
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->count_:I

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->count_:I
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55602(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;I)I

    .line 40139
    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_36

    .line 40140
    or-int/lit8 v2, v2, 0x2

    .line 40142
    :cond_36
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55702(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    .line 40143
    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_43

    .line 40144
    or-int/lit8 v2, v2, 0x4

    .line 40146
    :cond_43
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55802(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 40147
    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55902(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;I)I

    .line 40148
    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 40071
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 40071
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 2

    .prologue
    .line 40087
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 40088
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    .line 40089
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40090
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->count_:I

    .line 40091
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40092
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->ALL:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    .line 40093
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40094
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 40095
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40096
    return-object p0
.end method

.method public final clearCount()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 2

    .prologue
    .line 40301
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40302
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->count_:I

    .line 40304
    return-object p0
.end method

.method public final clearParticipantId()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 2

    .prologue
    .line 40275
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    .line 40276
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40278
    return-object p0
.end method

.method public final clearStubbyInfo()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 2

    .prologue
    .line 40368
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 40370
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40371
    return-object p0
.end method

.method public final clearSuggestionsType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 2

    .prologue
    .line 40325
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40326
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->ALL:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    .line 40328
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 40071
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 40071
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 40071
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 40292
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->count_:I

    return v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 40071
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40071
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    .registers 2

    .prologue
    .line 40104
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getParticipantId(I)Ljava/lang/String;
    .registers 3
    .parameter "index"

    .prologue
    .line 40246
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getParticipantIdCount()I
    .registers 2

    .prologue
    .line 40243
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantIdList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40240
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getStubbyInfo()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .registers 2

    .prologue
    .line 40337
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object v0
.end method

.method public final getSuggestionsType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;
    .registers 2

    .prologue
    .line 40313
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    return-object v0
.end method

.method public final hasCount()Z
    .registers 3

    .prologue
    .line 40289
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasStubbyInfo()Z
    .registers 3

    .prologue
    .line 40334
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasSuggestionsType()Z
    .registers 3

    .prologue
    .line 40310
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 40176
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40071
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 40071
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40071
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 5
    .parameter "other"

    .prologue
    .line 40152
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 40172
    :cond_6
    :goto_6
    return-object p0

    .line 40153
    :cond_7
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55500(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_25

    .line 40154
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 40155
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55500(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    .line 40156
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40163
    :cond_25
    :goto_25
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->hasCount()Z

    move-result v0

    if-eqz v0, :cond_32

    .line 40164
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->setCount(I)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    .line 40166
    :cond_32
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->hasSuggestionsType()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 40167
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getSuggestionsType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->setSuggestionsType(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;

    .line 40169
    :cond_3f
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->hasStubbyInfo()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 40170
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getStubbyInfo()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_7d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v2

    if-eq v1, v2, :cond_7d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    :goto_69
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    goto :goto_6

    .line 40158
    :cond_70
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->ensureParticipantIdIsMutable()V

    .line 40159
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->access$55500(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_25

    .line 40170
    :cond_7d
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    goto :goto_69
.end method

.method public final setCount(I)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 40295
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40296
    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->count_:I

    .line 40298
    return-object p0
.end method

.method public final setParticipantId(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 40250
    if-nez p2, :cond_8

    .line 40251
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40253
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->ensureParticipantIdIsMutable()V

    .line 40254
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 40256
    return-object p0
.end method

.method public final setStubbyInfo(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 40350
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 40352
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40353
    return-object p0
.end method

.method public final setStubbyInfo(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 40340
    if-nez p1, :cond_8

    .line 40341
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40343
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->stubbyInfo_:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    .line 40345
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40346
    return-object p0
.end method

.method public final setSuggestionsType(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 40316
    if-nez p1, :cond_8

    .line 40317
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40319
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->bitField0_:I

    .line 40320
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$Builder;->suggestionsType_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    .line 40322
    return-object p0
.end method
