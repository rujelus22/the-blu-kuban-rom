.class public final Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$SuggestionOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$Suggestion;",
        "Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private suggestedUser_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 40553
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 40656
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    .line 40554
    return-void
.end method

.method static synthetic access$56100()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 1

    .prologue
    .line 40548
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 3

    .prologue
    .line 40571
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureSuggestedUserIsMutable()V
    .registers 3

    .prologue
    .line 40659
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_16

    .line 40660
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    .line 40661
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    .line 40663
    :cond_16
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40631
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 40632
    .local v1, tag:I
    sparse-switch v1, :sswitch_data_1e

    .line 40637
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 40639
    :sswitch_d
    return-object p0

    .line 40644
    :sswitch_e
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    .line 40645
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 40646
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->addSuggestedUser(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    goto :goto_0

    .line 40632
    nop

    :sswitch_data_1e
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
    .end sparse-switch
.end method


# virtual methods
.method public final addAllSuggestedUser(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;"
        }
    .end annotation

    .prologue
    .line 40726
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40727
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 40729
    return-object p0
.end method

.method public final addSuggestedUser(ILcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 40719
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40720
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 40722
    return-object p0
.end method

.method public final addSuggestedUser(ILcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 40702
    if-nez p2, :cond_8

    .line 40703
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40705
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40706
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 40708
    return-object p0
.end method

.method public final addSuggestedUser(Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 40712
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40713
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40715
    return-object p0
.end method

.method public final addSuggestedUser(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 40692
    if-nez p1, :cond_8

    .line 40693
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40695
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40696
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40698
    return-object p0
.end method

.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40548
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    .registers 3

    .prologue
    .line 40579
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    .line 40580
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 40581
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 40583
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40548
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    .registers 4

    .prologue
    .line 40597
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;-><init>(Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;B)V

    .line 40598
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    .line 40599
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1d

    .line 40600
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    .line 40601
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    .line 40603
    :cond_1d
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->suggestedUser_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->access$56302(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;Ljava/util/List;)Ljava/util/List;

    .line 40604
    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 40548
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 40548
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 2

    .prologue
    .line 40564
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 40565
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    .line 40566
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    .line 40567
    return-object p0
.end method

.method public final clearSuggestedUser()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 2

    .prologue
    .line 40732
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    .line 40733
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    .line 40735
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 40548
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 40548
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 40548
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 40548
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40548
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    .registers 2

    .prologue
    .line 40575
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public final getSuggestedUser(I)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .registers 3
    .parameter "index"

    .prologue
    .line 40672
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getSuggestedUserCount()I
    .registers 2

    .prologue
    .line 40669
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getSuggestedUserList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40666
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 40623
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40548
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 40548
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40548
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 4
    .parameter "other"

    .prologue
    .line 40608
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 40619
    :cond_6
    :goto_6
    return-object p0

    .line 40609
    :cond_7
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->suggestedUser_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->access$56300(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 40610
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 40611
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->suggestedUser_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->access$56300(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    .line 40612
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->bitField0_:I

    goto :goto_6

    .line 40614
    :cond_26
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40615
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->suggestedUser_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->access$56300(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final setSuggestedUser(ILcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 40686
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40687
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 40689
    return-object p0
.end method

.method public final setSuggestedUser(ILcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 40676
    if-nez p2, :cond_8

    .line 40677
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40679
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->ensureSuggestedUserIsMutable()V

    .line 40680
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->suggestedUser_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 40682
    return-object p0
.end method
