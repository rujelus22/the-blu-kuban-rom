.class public final Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponseOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private requestError_:Ljava/lang/Object;

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

.field private suggestion_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$Suggestion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 40990
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 41127
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    .line 41151
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    .line 41187
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    .line 40991
    return-void
.end method

.method static synthetic access$56500()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 1

    .prologue
    .line 40985
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 3

    .prologue
    .line 41012
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureSuggestionIsMutable()V
    .registers 3

    .prologue
    .line 41190
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_16

    .line 41191
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    .line 41192
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41194
    :cond_16
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 8
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41088
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    .line 41089
    .local v2, tag:I
    sparse-switch v2, :sswitch_data_3e

    .line 41094
    invoke-virtual {p0, p1, p2, v2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 41096
    :sswitch_d
    return-object p0

    .line 41101
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 41102
    .local v0, rawValue:I
    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v3

    .line 41103
    .local v3, value:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    if-eqz v3, :cond_0

    .line 41104
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41105
    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    goto :goto_0

    .line 41110
    .end local v0           #rawValue:I
    .end local v3           #value:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    :sswitch_21
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41111
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    goto :goto_0

    .line 41115
    :sswitch_2e
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;

    move-result-object v1

    .line 41116
    .local v1, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 41117
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->addSuggestion(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    goto :goto_0

    .line 41089
    nop

    :sswitch_data_3e
    .sparse-switch
        0x0 -> :sswitch_d
        0x8 -> :sswitch_e
        0x12 -> :sswitch_21
        0x1a -> :sswitch_2e
    .end sparse-switch
.end method


# virtual methods
.method public final addAllSuggestion(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$Suggestion;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 41257
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/wireless/realtimechat/proto/Client$Suggestion;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41258
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 41260
    return-object p0
.end method

.method public final addSuggestion(ILcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 41250
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41251
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 41253
    return-object p0
.end method

.method public final addSuggestion(ILcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 41233
    if-nez p2, :cond_8

    .line 41234
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41236
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41237
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 41239
    return-object p0
.end method

.method public final addSuggestion(Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 41243
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41244
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41246
    return-object p0
.end method

.method public final addSuggestion(Lcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 41223
    if-nez p1, :cond_8

    .line 41224
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41226
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41227
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41229
    return-object p0
.end method

.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40985
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    .registers 3

    .prologue
    .line 41020
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    .line 41021
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 41022
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 41024
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40985
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    .registers 6

    .prologue
    .line 41038
    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;B)V

    .line 41039
    .local v1, result:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41040
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 41041
    .local v2, to_bitField0_:I
    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_f

    .line 41042
    const/4 v2, 0x1

    .line 41044
    :cond_f
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$56702(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    .line 41045
    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1b

    .line 41046
    or-int/lit8 v2, v2, 0x2

    .line 41048
    :cond_1b
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->requestError_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$56802(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41049
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_35

    .line 41050
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    .line 41051
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41053
    :cond_35
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->suggestion_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$56902(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;Ljava/util/List;)Ljava/util/List;

    .line 41054
    #setter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$57002(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;I)I

    .line 41055
    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 40985
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 40985
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 2

    .prologue
    .line 41001
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 41002
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    .line 41003
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41004
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    .line 41005
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41006
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    .line 41007
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41008
    return-object p0
.end method

.method public final clearRequestError()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 2

    .prologue
    .line 41175
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41176
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getRequestError()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    .line 41178
    return-object p0
.end method

.method public final clearStatus()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 2

    .prologue
    .line 41144
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41145
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    .line 41147
    return-object p0
.end method

.method public final clearSuggestion()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 2

    .prologue
    .line 41263
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    .line 41264
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41266
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 40985
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 40985
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 40985
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 40985
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 40985
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    .registers 2

    .prologue
    .line 41016
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestError()Ljava/lang/String;
    .registers 4

    .prologue
    .line 41156
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    .line 41157
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_f

    .line 41158
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 41159
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    .line 41162
    .end local v1           #s:Ljava/lang/String;
    :goto_e
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_e
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .registers 2

    .prologue
    .line 41132
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final getSuggestion(I)Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    .registers 3
    .parameter "index"

    .prologue
    .line 41203
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    return-object v0
.end method

.method public final getSuggestionCount()I
    .registers 2

    .prologue
    .line 41200
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getSuggestionList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$Suggestion;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41197
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final hasRequestError()Z
    .registers 3

    .prologue
    .line 41153
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasStatus()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 41129
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 41080
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40985
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 40985
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40985
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 4
    .parameter "other"

    .prologue
    .line 41059
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 41076
    :cond_6
    :goto_6
    return-object p0

    .line 41060
    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 41061
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    .line 41063
    :cond_14
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->hasRequestError()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 41064
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getRequestError()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->setRequestError(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;

    .line 41066
    :cond_21
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->suggestion_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$56900(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 41067
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 41068
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->suggestion_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$56900(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    .line 41069
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    goto :goto_6

    .line 41071
    :cond_40
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41072
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->suggestion_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->access$56900(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_6
.end method

.method public final setRequestError(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 41166
    if-nez p1, :cond_8

    .line 41167
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41169
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41170
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->requestError_:Ljava/lang/Object;

    .line 41172
    return-object p0
.end method

.method public final setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 41135
    if-nez p1, :cond_8

    .line 41136
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41138
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->bitField0_:I

    .line 41139
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    .line 41141
    return-object p0
.end method

.method public final setSuggestion(ILcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 41217
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41218
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 41220
    return-object p0
.end method

.method public final setSuggestion(ILcom/google/wireless/realtimechat/proto/Client$Suggestion;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 41207
    if-nez p2, :cond_8

    .line 41208
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41210
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->ensureSuggestionIsMutable()V

    .line 41211
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse$Builder;->suggestion_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 41213
    return-object p0
.end method
