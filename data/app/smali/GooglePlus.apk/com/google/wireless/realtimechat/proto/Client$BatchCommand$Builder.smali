.class public final Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BatchCommandOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;",
        "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$BatchCommandOrBuilder;"
    }
.end annotation


# instance fields
.field private androidNewUrl_:Ljava/lang/Object;

.field private bitField0_:I

.field private clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private clientVersion_:Ljava/lang/Object;

.field private command_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;"
        }
    .end annotation
.end field

.field private iphoneNewUrl_:Ljava/lang/Object;

.field private isAndroidDeprecated_:Z

.field private isIphoneDeprecated_:Z

.field private request_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation
.end field

.field private response_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;"
        }
    .end annotation
.end field

.field private stateUpdate_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 59393
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 59655
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    .line 59744
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    .line 59833
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    .line 59922
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    .line 60011
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    .line 60047
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    .line 60132
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    .line 60168
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    .line 59394
    return-void
.end method

.method static synthetic access$80300(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59388
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_14

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_14
    return-object v0
.end method

.method static synthetic access$80400()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 1

    .prologue
    .line 59388
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;-><init>()V

    return-object v0
.end method

.method private buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 6

    .prologue
    .line 59455
    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;B)V

    .line 59456
    .local v1, result:Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59457
    .local v0, from_bitField0_:I
    const/4 v2, 0x0

    .line 59458
    .local v2, to_bitField0_:I
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1e

    .line 59459
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    .line 59460
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59462
    :cond_1e
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80602(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    .line 59463
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_38

    .line 59464
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    .line 59465
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59467
    :cond_38
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80702(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    .line 59468
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_52

    .line 59469
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    .line 59470
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59472
    :cond_52
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80802(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    .line 59473
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_6d

    .line 59474
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    .line 59475
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59477
    :cond_6d
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80902(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    .line 59478
    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_79

    .line 59479
    const/4 v2, 0x1

    .line 59481
    :cond_79
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81002(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59482
    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_86

    .line 59483
    or-int/lit8 v2, v2, 0x2

    .line 59485
    :cond_86
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81102(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion;

    .line 59486
    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_93

    .line 59487
    or-int/lit8 v2, v2, 0x4

    .line 59489
    :cond_93
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81202(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Z)Z

    .line 59490
    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_a0

    .line 59491
    or-int/lit8 v2, v2, 0x8

    .line 59493
    :cond_a0
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81302(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Z)Z

    .line 59494
    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_ad

    .line 59495
    or-int/lit8 v2, v2, 0x10

    .line 59497
    :cond_ad
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81402(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59498
    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_ba

    .line 59499
    or-int/lit8 v2, v2, 0x20

    .line 59501
    :cond_ba
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81502(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59502
    #setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81602(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;I)I

    .line 59503
    return-object v1
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3

    .prologue
    .line 59429
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureCommandIsMutable()V
    .registers 3

    .prologue
    .line 59658
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_16

    .line 59659
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    .line 59660
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59662
    :cond_16
    return-void
.end method

.method private ensureRequestIsMutable()V
    .registers 3

    .prologue
    .line 59747
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_16

    .line 59748
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    .line 59749
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59751
    :cond_16
    return-void
.end method

.method private ensureResponseIsMutable()V
    .registers 3

    .prologue
    .line 59836
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_16

    .line 59837
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    .line 59838
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59840
    :cond_16
    return-void
.end method

.method private ensureStateUpdateIsMutable()V
    .registers 3

    .prologue
    .line 59925
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_17

    .line 59926
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    .line 59927
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59929
    :cond_17
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 6
    .parameter "input"
    .parameter "extensionRegistry"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59578
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    .line 59579
    .local v1, tag:I
    sparse-switch v1, :sswitch_data_aa

    .line 59584
    invoke-virtual {p0, p1, p2, v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 59586
    :sswitch_d
    return-object p0

    .line 59591
    :sswitch_e
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    move-result-object v0

    .line 59592
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59593
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    .line 59597
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;
    :sswitch_1d
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59598
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    goto :goto_0

    .line 59602
    :sswitch_2a
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    .line 59603
    .local v0, subBuilder:Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->hasClientVersionMessage()Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 59604
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    .line 59606
    :cond_3b
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59607
    invoke-virtual {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    .line 59611
    .end local v0           #subBuilder:Lcom/google/wireless/webapps/Version$ClientVersion$Builder;
    :sswitch_46
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;

    move-result-object v0

    .line 59612
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59613
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    .line 59617
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;
    :sswitch_55
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    .line 59618
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59619
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    .line 59623
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;
    :sswitch_64
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    .line 59624
    .local v0, subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 59625
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addStateUpdate(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    .line 59629
    .end local v0           #subBuilder:Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;
    :sswitch_73
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59630
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    goto :goto_0

    .line 59634
    :sswitch_80
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59635
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    goto/16 :goto_0

    .line 59639
    :sswitch_8e
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59640
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 59644
    :sswitch_9c
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59645
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 59579
    :sswitch_data_aa
    .sparse-switch
        0x0 -> :sswitch_d
        0xa -> :sswitch_e
        0x12 -> :sswitch_1d
        0x1a -> :sswitch_2a
        0x22 -> :sswitch_46
        0x2a -> :sswitch_55
        0x32 -> :sswitch_64
        0x38 -> :sswitch_73
        0x40 -> :sswitch_80
        0x4a -> :sswitch_8e
        0x52 -> :sswitch_9c
    .end sparse-switch
.end method


# virtual methods
.method public final addAllCommand(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    .prologue
    .line 59725
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59726
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 59728
    return-object p0
.end method

.method public final addAllRequest(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    .prologue
    .line 59814
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59815
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 59817
    return-object p0
.end method

.method public final addAllResponse(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    .prologue
    .line 59903
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59904
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 59906
    return-object p0
.end method

.method public final addAllStateUpdate(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    .prologue
    .line 59992
    .local p1, values:Ljava/lang/Iterable;,"Ljava/lang/Iterable<+Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;>;"
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59993
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 59995
    return-object p0
.end method

.method public final addCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59718
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59719
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59721
    return-object p0
.end method

.method public final addCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59701
    if-nez p2, :cond_8

    .line 59702
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59704
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59705
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59707
    return-object p0
.end method

.method public final addCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 59711
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59712
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59714
    return-object p0
.end method

.method public final addCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 59691
    if-nez p1, :cond_8

    .line 59692
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59694
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59695
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59697
    return-object p0
.end method

.method public final addRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59807
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59808
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59810
    return-object p0
.end method

.method public final addRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59790
    if-nez p2, :cond_8

    .line 59791
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59793
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59794
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59796
    return-object p0
.end method

.method public final addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 59800
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59801
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59803
    return-object p0
.end method

.method public final addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 59780
    if-nez p1, :cond_8

    .line 59781
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59783
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59784
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59786
    return-object p0
.end method

.method public final addResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59896
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59897
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59899
    return-object p0
.end method

.method public final addResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59879
    if-nez p2, :cond_8

    .line 59880
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59882
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59883
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59885
    return-object p0
.end method

.method public final addResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 59889
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59890
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59892
    return-object p0
.end method

.method public final addResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 59869
    if-nez p1, :cond_8

    .line 59870
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59872
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59873
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59875
    return-object p0
.end method

.method public final addStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59985
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59986
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59988
    return-object p0
.end method

.method public final addStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59968
    if-nez p2, :cond_8

    .line 59969
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59971
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59972
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 59974
    return-object p0
.end method

.method public final addStateUpdate(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "builderForValue"

    .prologue
    .line 59978
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59979
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59981
    return-object p0
.end method

.method public final addStateUpdate(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 59958
    if-nez p1, :cond_8

    .line 59959
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59961
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59962
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 59964
    return-object p0
.end method

.method public final bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 59388
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 3

    .prologue
    .line 59437
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    .line 59438
    .local v0, result:Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_10

    .line 59439
    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    .line 59441
    :cond_10
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 59388
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 59388
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 59388
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 59404
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 59405
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    .line 59406
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59407
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    .line 59408
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59409
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    .line 59410
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59411
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    .line 59412
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    .line 59414
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59415
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    .line 59416
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59417
    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    .line 59418
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59419
    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    .line 59420
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59421
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    .line 59422
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59423
    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    .line 59424
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59425
    return-object p0
.end method

.method public final clearAndroidNewUrl()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 60192
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60193
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getAndroidNewUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    .line 60195
    return-object p0
.end method

.method public final clearClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 60035
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60036
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    .line 60038
    return-object p0
.end method

.method public final clearClientVersionMessage()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 60083
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    .line 60085
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60086
    return-object p0
.end method

.method public final clearCommand()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 59731
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    .line 59732
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59734
    return-object p0
.end method

.method public final clearIphoneNewUrl()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 60156
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60157
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIphoneNewUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    .line 60159
    return-object p0
.end method

.method public final clearIsAndroidDeprecated()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 60125
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    .line 60128
    return-object p0
.end method

.method public final clearIsIphoneDeprecated()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 60104
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    .line 60107
    return-object p0
.end method

.method public final clearRequest()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 59820
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    .line 59821
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59823
    return-object p0
.end method

.method public final clearResponse()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 59909
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    .line 59910
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59912
    return-object p0
.end method

.method public final clearStateUpdate()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 2

    .prologue
    .line 59998
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    .line 59999
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60001
    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 2

    .prologue
    .line 59388
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 2

    .prologue
    .line 59388
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 59388
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAndroidNewUrl()Ljava/lang/String;
    .registers 4

    .prologue
    .line 60173
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    .line 60174
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_f

    .line 60175
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 60176
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    .line 60179
    .end local v1           #s:Ljava/lang/String;
    :goto_e
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_e
.end method

.method public final getClientVersion()Ljava/lang/String;
    .registers 4

    .prologue
    .line 60016
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    .line 60017
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_f

    .line 60018
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 60019
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    .line 60022
    .end local v1           #s:Ljava/lang/String;
    :goto_e
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_e
.end method

.method public final getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;
    .registers 2

    .prologue
    .line 60052
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method public final getCommand(I)Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .registers 3
    .parameter "index"

    .prologue
    .line 59671
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    return-object v0
.end method

.method public final getCommandCount()I
    .registers 2

    .prologue
    .line 59668
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getCommandList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59665
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .registers 2

    .prologue
    .line 59388
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 59388
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 2

    .prologue
    .line 59433
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final getIphoneNewUrl()Ljava/lang/String;
    .registers 4

    .prologue
    .line 60137
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    .line 60138
    .local v0, ref:Ljava/lang/Object;
    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_f

    .line 60139
    check-cast v0, Lcom/google/protobuf/ByteString;

    .end local v0           #ref:Ljava/lang/Object;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 60140
    .local v1, s:Ljava/lang/String;
    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    .line 60143
    .end local v1           #s:Ljava/lang/String;
    :goto_e
    return-object v1

    .restart local v0       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v0, Ljava/lang/String;

    .end local v0           #ref:Ljava/lang/Object;
    move-object v1, v0

    goto :goto_e
.end method

.method public final getIsAndroidDeprecated()Z
    .registers 2

    .prologue
    .line 60116
    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    return v0
.end method

.method public final getIsIphoneDeprecated()Z
    .registers 2

    .prologue
    .line 60095
    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    return v0
.end method

.method public final getRequest(I)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    .registers 3
    .parameter "index"

    .prologue
    .line 59760
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    return-object v0
.end method

.method public final getRequestCount()I
    .registers 2

    .prologue
    .line 59757
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getRequestList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59754
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getResponse(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .registers 3
    .parameter "index"

    .prologue
    .line 59849
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    return-object v0
.end method

.method public final getResponseCount()I
    .registers 2

    .prologue
    .line 59846
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getResponseList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59843
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getStateUpdate(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
    .registers 3
    .parameter "index"

    .prologue
    .line 59938
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    return-object v0
.end method

.method public final getStateUpdateCount()I
    .registers 2

    .prologue
    .line 59935
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getStateUpdateList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59932
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final hasAndroidNewUrl()Z
    .registers 3

    .prologue
    .line 60170
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasClientVersion()Z
    .registers 3

    .prologue
    .line 60013
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasClientVersionMessage()Z
    .registers 3

    .prologue
    .line 60049
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasIphoneNewUrl()Z
    .registers 3

    .prologue
    .line 60134
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasIsAndroidDeprecated()Z
    .registers 3

    .prologue
    .line 60113
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasIsIphoneDeprecated()Z
    .registers 3

    .prologue
    .line 60092
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final isInitialized()Z
    .registers 2

    .prologue
    .line 59570
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59388
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .registers 3
    .parameter "x0"

    .prologue
    .line 59388
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    .end local p1
    invoke-virtual {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59388
    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "other"

    .prologue
    .line 59507
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 59566
    :cond_6
    :goto_6
    return-object p0

    .line 59508
    :cond_7
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_25

    .line 59509
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f2

    .line 59510
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    .line 59511
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59518
    :cond_25
    :goto_25
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_43

    .line 59519
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_100

    .line 59520
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    .line 59521
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59528
    :cond_43
    :goto_43
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_61

    .line 59529
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10e

    .line 59530
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    .line 59531
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59538
    :cond_61
    :goto_61
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7f

    .line 59539
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11c

    .line 59540
    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    .line 59541
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59548
    :cond_7f
    :goto_7f
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasClientVersion()Z

    move-result v0

    if-eqz v0, :cond_8c

    .line 59549
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setClientVersion(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    .line 59551
    :cond_8c
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasClientVersionMessage()Z

    move-result v0

    if-eqz v0, :cond_bc

    .line 59552
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_12a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    if-eq v1, v2, :cond_12a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {v1}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    :goto_b6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 59554
    :cond_bc
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasIsIphoneDeprecated()Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 59555
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIsIphoneDeprecated()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setIsIphoneDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    .line 59557
    :cond_c9
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasIsAndroidDeprecated()Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 59558
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIsAndroidDeprecated()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setIsAndroidDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    .line 59560
    :cond_d6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasIphoneNewUrl()Z

    move-result v0

    if-eqz v0, :cond_e3

    .line 59561
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIphoneNewUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setIphoneNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    .line 59563
    :cond_e3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasAndroidNewUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 59564
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getAndroidNewUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setAndroidNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto/16 :goto_6

    .line 59513
    :cond_f2
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59514
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_25

    .line 59523
    :cond_100
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59524
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_43

    .line 59533
    :cond_10e
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59534
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_61

    .line 59543
    :cond_11c
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59544
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    #getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7f

    .line 59552
    :cond_12a
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    goto :goto_b6
.end method

.method public final setAndroidNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 60183
    if-nez p1, :cond_8

    .line 60184
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60186
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60187
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    .line 60189
    return-object p0
.end method

.method public final setClientVersion(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 60026
    if-nez p1, :cond_8

    .line 60027
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60029
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60030
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    .line 60032
    return-object p0
.end method

.method public final setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "builderForValue"

    .prologue
    .line 60065
    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->build()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    .line 60067
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60068
    return-object p0
.end method

.method public final setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 60055
    if-nez p1, :cond_8

    .line 60056
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60058
    :cond_8
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    .line 60060
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60061
    return-object p0
.end method

.method public final setCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59685
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59686
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59688
    return-object p0
.end method

.method public final setCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59675
    if-nez p2, :cond_8

    .line 59676
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59678
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    .line 59679
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59681
    return-object p0
.end method

.method public final setIphoneNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 60147
    if-nez p1, :cond_8

    .line 60148
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 60150
    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60151
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    .line 60153
    return-object p0
.end method

.method public final setIsAndroidDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 60119
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60120
    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    .line 60122
    return-object p0
.end method

.method public final setIsIphoneDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 3
    .parameter "value"

    .prologue
    .line 60098
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    .line 60099
    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    .line 60101
    return-object p0
.end method

.method public final setRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59774
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59775
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59777
    return-object p0
.end method

.method public final setRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59764
    if-nez p2, :cond_8

    .line 59765
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59767
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    .line 59768
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59770
    return-object p0
.end method

.method public final setResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59863
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59864
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59866
    return-object p0
.end method

.method public final setResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59853
    if-nez p2, :cond_8

    .line 59854
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59856
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    .line 59857
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59859
    return-object p0
.end method

.method public final setStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 5
    .parameter "index"
    .parameter "builderForValue"

    .prologue
    .line 59952
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59953
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59955
    return-object p0
.end method

.method public final setStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 4
    .parameter "index"
    .parameter "value"

    .prologue
    .line 59942
    if-nez p2, :cond_8

    .line 59943
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 59945
    :cond_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    .line 59946
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 59948
    return-object p0
.end method
