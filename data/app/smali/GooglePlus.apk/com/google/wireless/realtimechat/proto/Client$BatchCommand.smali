.class public final Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BatchCommandOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BatchCommand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

.field private static final serialVersionUID:J


# instance fields
.field private androidNewUrl_:Ljava/lang/Object;

.field private bitField0_:I

.field private clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private clientVersion_:Ljava/lang/Object;

.field private command_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;"
        }
    .end annotation
.end field

.field private iphoneNewUrl_:Ljava/lang/Object;

.field private isAndroidDeprecated_:Z

.field private isIphoneDeprecated_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private request_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation
.end field

.field private response_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;"
        }
    .end annotation
.end field

.field private stateUpdate_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 60207
    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;-><init>()V

    .line 60208
    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;

    .line 60209
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 58979
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 59213
    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedIsInitialized:B

    .line 59257
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedSerializedSize:I

    .line 58979
    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;)V
    .registers 4
    .parameter "builder"

    .prologue
    const/4 v1, -0x1

    .line 58977
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    .line 59213
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedIsInitialized:B

    .line 59257
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedSerializedSize:I

    .line 58978
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 58972
    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;)V

    return-void
.end method

.method static synthetic access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58972
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$80602(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58972
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$80702(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58972
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$80802(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 58972
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$80902(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$81002(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$81102(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object p1
.end method

.method static synthetic access$81202(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z

    return p1
.end method

.method static synthetic access$81302(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z

    return p1
.end method

.method static synthetic access$81402(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$81502(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$81602(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 58972
    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    return p1
.end method

.method private getAndroidNewUrlBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 59190
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;

    .line 59191
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 59192
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 59194
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;

    .line 59197
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method private getClientVersionBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 59096
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;

    .line 59097
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 59098
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 59100
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;

    .line 59103
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 1

    .prologue
    .line 58983
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    return-object v0
.end method

.method private getIphoneNewUrlBytes()Lcom/google/protobuf/ByteString;
    .registers 4

    .prologue
    .line 59158
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;

    .line 59159
    .local v1, ref:Ljava/lang/Object;
    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_f

    .line 59160
    check-cast v1, Ljava/lang/String;

    .end local v1           #ref:Ljava/lang/Object;
    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 59162
    .local v0, b:Lcom/google/protobuf/ByteString;
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;

    .line 59165
    .end local v0           #b:Lcom/google/protobuf/ByteString;
    :goto_e
    return-object v0

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_f
    check-cast v1, Lcom/google/protobuf/ByteString;

    .end local v1           #ref:Ljava/lang/Object;
    move-object v0, v1

    goto :goto_e
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .registers 1

    .prologue
    .line 59381
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->access$80400()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 2
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 59328
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->access$80400()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->access$80300(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAndroidNewUrl()Ljava/lang/String;
    .registers 5

    .prologue
    .line 59176
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;

    .line 59177
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 59178
    check-cast v1, Ljava/lang/String;

    .line 59186
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 59180
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 59182
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 59183
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 59184
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 59186
    goto :goto_8
.end method

.method public final getClientVersion()Ljava/lang/String;
    .registers 5

    .prologue
    .line 59082
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;

    .line 59083
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 59084
    check-cast v1, Ljava/lang/String;

    .line 59092
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 59086
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 59088
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 59089
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 59090
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 59092
    goto :goto_8
.end method

.method public final getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;
    .registers 2

    .prologue
    .line 59114
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method public final getCommand(I)Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .registers 3
    .parameter "index"

    .prologue
    .line 59005
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    return-object v0
.end method

.method public final getCommandCount()I
    .registers 2

    .prologue
    .line 59002
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getCommandList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58995
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    return-object v0
.end method

.method public final getCommandOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Client$BunchCommandOrBuilder;
    .registers 3
    .parameter "index"

    .prologue
    .line 59009
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommandOrBuilder;

    return-object v0
.end method

.method public final getCommandOrBuilderList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommandOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58999
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 58972
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .registers 2

    .prologue
    .line 58987
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    return-object v0
.end method

.method public final getIphoneNewUrl()Ljava/lang/String;
    .registers 5

    .prologue
    .line 59144
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;

    .line 59145
    .local v1, ref:Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 59146
    check-cast v1, Ljava/lang/String;

    .line 59154
    .end local v1           #ref:Ljava/lang/Object;
    :goto_8
    return-object v1

    .restart local v1       #ref:Ljava/lang/Object;
    :cond_9
    move-object v0, v1

    .line 59148
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 59150
    .local v0, bs:Lcom/google/protobuf/ByteString;
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    .line 59151
    .local v2, s:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 59152
    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;

    :cond_18
    move-object v1, v2

    .line 59154
    goto :goto_8
.end method

.method public final getIsAndroidDeprecated()Z
    .registers 2

    .prologue
    .line 59134
    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z

    return v0
.end method

.method public final getIsIphoneDeprecated()Z
    .registers 2

    .prologue
    .line 59124
    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z

    return v0
.end method

.method public final getRequest(I)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    .registers 3
    .parameter "index"

    .prologue
    .line 59026
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    return-object v0
.end method

.method public final getRequestCount()I
    .registers 2

    .prologue
    .line 59023
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getRequestList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59016
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    return-object v0
.end method

.method public final getRequestOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequestOrBuilder;
    .registers 3
    .parameter "index"

    .prologue
    .line 59030
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequestOrBuilder;

    return-object v0
.end method

.method public final getRequestOrBuilderList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequestOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59020
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    return-object v0
.end method

.method public final getResponse(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .registers 3
    .parameter "index"

    .prologue
    .line 59047
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    return-object v0
.end method

.method public final getResponseCount()I
    .registers 2

    .prologue
    .line 59044
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getResponseList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59037
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    return-object v0
.end method

.method public final getResponseOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponseOrBuilder;
    .registers 3
    .parameter "index"

    .prologue
    .line 59051
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponseOrBuilder;

    return-object v0
.end method

.method public final getResponseOrBuilderList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponseOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59041
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 9

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 59259
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedSerializedSize:I

    .line 59260
    .local v1, size:I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_c

    move v2, v1

    .line 59304
    .end local v1           #size:I
    .local v2, size:I
    :goto_b
    return v2

    .line 59262
    .end local v2           #size:I
    .restart local v1       #size:I
    :cond_c
    const/4 v1, 0x0

    .line 59263
    const/4 v0, 0x0

    .local v0, i:I
    :goto_e
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_26

    .line 59264
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59263
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 59267
    :cond_26
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v4, :cond_35

    .line 59268
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v5, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59271
    :cond_35
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    if-ne v3, v5, :cond_43

    .line 59272
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59275
    :cond_43
    const/4 v0, 0x0

    :goto_44
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_5c

    .line 59276
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v6, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59275
    add-int/lit8 v0, v0, 0x1

    goto :goto_44

    .line 59279
    :cond_5c
    const/4 v0, 0x0

    :goto_5d
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_76

    .line 59280
    const/4 v4, 0x5

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59279
    add-int/lit8 v0, v0, 0x1

    goto :goto_5d

    .line 59283
    :cond_76
    const/4 v0, 0x0

    :goto_77
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_90

    .line 59284
    const/4 v4, 0x6

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59283
    add-int/lit8 v0, v0, 0x1

    goto :goto_77

    .line 59287
    :cond_90
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    if-ne v3, v6, :cond_9e

    .line 59288
    const/4 v3, 0x7

    iget-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 59291
    :cond_9e
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    if-ne v3, v7, :cond_ab

    .line 59292
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z

    invoke-static {v7, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v1, v3

    .line 59295
    :cond_ab
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_be

    .line 59296
    const/16 v3, 0x9

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIphoneNewUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59299
    :cond_be
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_d1

    .line 59300
    const/16 v3, 0xa

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getAndroidNewUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v1, v3

    .line 59303
    :cond_d1
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedSerializedSize:I

    move v2, v1

    .line 59304
    .end local v1           #size:I
    .restart local v2       #size:I
    goto/16 :goto_b
.end method

.method public final getStateUpdate(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
    .registers 3
    .parameter "index"

    .prologue
    .line 59068
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    return-object v0
.end method

.method public final getStateUpdateCount()I
    .registers 2

    .prologue
    .line 59065
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getStateUpdateList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59058
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    return-object v0
.end method

.method public final getStateUpdateOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdateOrBuilder;
    .registers 3
    .parameter "index"

    .prologue
    .line 59072
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdateOrBuilder;

    return-object v0
.end method

.method public final getStateUpdateOrBuilderList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdateOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59062
    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    return-object v0
.end method

.method public final hasAndroidNewUrl()Z
    .registers 3

    .prologue
    .line 59173
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasClientVersion()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 59079
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_8

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final hasClientVersionMessage()Z
    .registers 3

    .prologue
    .line 59111
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final hasIphoneNewUrl()Z
    .registers 3

    .prologue
    .line 59141
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasIsAndroidDeprecated()Z
    .registers 3

    .prologue
    .line 59131
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final hasIsIphoneDeprecated()Z
    .registers 3

    .prologue
    .line 59121
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 59215
    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedIsInitialized:B

    .line 59216
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_b

    if-ne v0, v1, :cond_9

    .line 59219
    :goto_8
    return v1

    .line 59216
    :cond_9
    const/4 v1, 0x0

    goto :goto_8

    .line 59218
    :cond_b
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->memoizedIsInitialized:B

    goto :goto_8
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 58972
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->access$80400()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 58972
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->access$80400()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 59311
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 8
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 59224
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getSerializedSize()I

    .line 59225
    const/4 v0, 0x0

    .local v0, i:I
    :goto_9
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1f

    .line 59226
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59225
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 59228
    :cond_1f
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2c

    .line 59229
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 59231
    :cond_2c
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_38

    .line 59232
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59234
    :cond_38
    const/4 v0, 0x0

    :goto_39
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_4f

    .line 59235
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59234
    add-int/lit8 v0, v0, 0x1

    goto :goto_39

    .line 59237
    :cond_4f
    const/4 v0, 0x0

    :goto_50
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_67

    .line 59238
    const/4 v2, 0x5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59237
    add-int/lit8 v0, v0, 0x1

    goto :goto_50

    .line 59240
    :cond_67
    const/4 v0, 0x0

    :goto_68
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_7f

    .line 59241
    const/4 v2, 0x6

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 59240
    add-int/lit8 v0, v0, 0x1

    goto :goto_68

    .line 59243
    :cond_7f
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_8b

    .line 59244
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 59246
    :cond_8b
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_96

    .line 59247
    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z

    invoke-virtual {p1, v5, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 59249
    :cond_96
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_a7

    .line 59250
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIphoneNewUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 59252
    :cond_a7
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_b8

    .line 59253
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getAndroidNewUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 59255
    :cond_b8
    return-void
.end method
