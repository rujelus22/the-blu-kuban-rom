.class public final Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Network.java"

# interfaces
.implements Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/tacotruck/proto/Network;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AnalyticsLogResponse"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;

.field private static final serialVersionUID:J


# instance fields
.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37921
    new-instance v0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;

    invoke-direct {v0}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;-><init>()V

    .line 37922
    sput-object v0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->defaultInstance:Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;

    .line 37923
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, -0x1

    .line 37717
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 37730
    iput-byte v0, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedIsInitialized:B

    .line 37744
    iput v0, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedSerializedSize:I

    .line 37717
    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;)V
    .registers 4
    .parameter "builder"

    .prologue
    const/4 v1, -0x1

    .line 37715
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    .line 37730
    iput-byte v1, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedIsInitialized:B

    .line 37744
    iput v1, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedSerializedSize:I

    .line 37716
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 37710
    invoke-direct {p0, p1}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;-><init>(Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;)V

    return-void
.end method

.method public static getDefaultInstance()Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;
    .registers 1

    .prologue
    .line 37721
    sget-object v0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->defaultInstance:Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .registers 2

    .prologue
    .line 37710
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;
    .registers 2

    .prologue
    .line 37725
    sget-object v0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->defaultInstance:Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;

    return-object v0
.end method

.method public final getSerializedSize()I
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 37746
    iget v0, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedSerializedSize:I

    .line 37747
    .local v0, size:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_7

    .line 37751
    .end local v0           #size:I
    :goto_6
    return v0

    .line 37749
    .restart local v0       #size:I
    :cond_7
    iput v1, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedSerializedSize:I

    move v0, v1

    .line 37751
    goto :goto_6
.end method

.method public final isInitialized()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 37732
    iget-byte v0, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedIsInitialized:B

    .line 37733
    .local v0, isInitialized:B
    const/4 v2, -0x1

    if-eq v0, v2, :cond_b

    if-ne v0, v1, :cond_9

    .line 37736
    :goto_8
    return v1

    .line 37733
    :cond_9
    const/4 v1, 0x0

    goto :goto_8

    .line 37735
    :cond_b
    iput-byte v1, p0, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->memoizedIsInitialized:B

    goto :goto_8
.end method

.method public final bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 37710
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;->access$52500()Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .registers 2

    .prologue
    .line 37710
    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;->access$52500()Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;->mergeFrom(Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;)Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 37758
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .registers 2
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37741
    invoke-virtual {p0}, Lcom/google/wireless/tacotruck/proto/Network$AnalyticsLogResponse;->getSerializedSize()I

    .line 37742
    return-void
.end method
