.class public abstract Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
.super Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
.source "TouchExplorationHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;"
    }
.end annotation


# instance fields
.field private mCurrentItem:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

.field private mFocusedItemId:I

.field private final mManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mNodeCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnHoverListener:Landroid/view/View$OnHoverListener;

.field private mParentView:Landroid/view/View;

.field private final mTempGlobalRect:Landroid/graphics/Rect;

.field private final mTempParentRect:Landroid/graphics/Rect;

.field private final mTempScreenRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 67
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    invoke-direct {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;-><init>()V

    .line 48
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    .line 50
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    .line 55
    const/high16 v0, -0x8000

    iput v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    .line 59
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    .line 345
    new-instance v0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$1;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$1;-><init>(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    .line 352
    new-instance v0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;-><init>(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mOnHoverListener:Landroid/view/View$OnHoverListener;

    .line 68
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)Landroid/view/accessibility/AccessibilityManager;
    .registers 2
    .parameter "x0"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;Ljava/lang/Object;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 44
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    if-eq v0, p1, :cond_1c

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    const/16 v1, 0x100

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    :cond_f
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    const/16 v1, 0x80

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    :cond_1c
    return-void
.end method

.method private clearCache()V
    .registers 3

    .prologue
    .line 218
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_17

    .line 219
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 222
    :cond_17
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 223
    return-void
.end method

.method private sendEventForItem(Ljava/lang/Object;I)V
    .registers 8
    .parameter
    .parameter "eventType"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 242
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    .local p1, item:Ljava/lang/Object;,"TT;"
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    new-instance v2, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getIdForItem(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    invoke-virtual {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_30

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_30

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "You must add text or a content description in populateEventForItem()"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_30
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v4, v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;I)V

    .line 243
    .local v0, event:Landroid/view/accessibility/AccessibilityEvent;
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 245
    .local v1, group:Landroid/view/ViewGroup;
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 246
    return-void
.end method


# virtual methods
.method public final createAccessibilityNodeInfo(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .registers 9
    .parameter "virtualViewId"

    .prologue
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    const/4 v4, 0x1

    .line 157
    const/4 v3, -0x1

    if-ne p1, v3, :cond_2f

    .line 158
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-static {v3, v2}, Landroid/support/v4/view/ViewCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getVisibleItems(Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getIdForItem(Ljava/lang/Object;)I

    move-result v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v5, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addChild(Landroid/view/View;I)V

    goto :goto_1b

    .line 161
    :cond_2f
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 162
    .local v0, cachedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    if-eqz v0, :cond_3e

    .line 164
    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    .line 177
    .end local v0           #cachedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_3d
    :goto_3d
    return-object v2

    .line 167
    .restart local v0       #cachedNode:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_3e
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getItemForId(I)Ljava/lang/Object;

    move-result-object v1

    .line 168
    .local v1, item:Ljava/lang/Object;,"TT;"
    if-nez v1, :cond_46

    .line 169
    const/4 v2, 0x0

    goto :goto_3d

    .line 172
    :cond_46
    invoke-static {}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getIdForItem(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setEnabled(Z)V

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setVisibleToUser(Z)V

    invoke-virtual {p0, v1, v2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_73

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_73

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "You must add text or a content description in populateNodeForItem()"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_73
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setPackageName(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setParent(Landroid/view/View;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v4, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setSource(Landroid/view/View;I)V

    iget v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    if-ne v4, v3, :cond_c0

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    :goto_9e
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInParent(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c6

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_c6

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "You must set parent or screen bounds in populateNodeForItem()"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_c0
    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    goto :goto_9e

    :cond_c6
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_d6

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_fe

    :cond_d6
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_109

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 175
    .local v2, node:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_fe
    :goto_fe
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-static {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_3d

    .line 172
    .end local v2           #node:Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    :cond_109
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    neg-int v3, v3

    neg-int v4, v4

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    goto :goto_fe
.end method

.method protected abstract getIdForItem(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation
.end method

.method protected abstract getItemAt(FF)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)TT;"
        }
    .end annotation
.end method

.method protected abstract getItemForId(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method protected abstract getVisibleItems(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public final install(Landroid/view/View;)V
    .registers 4
    .parameter "parentView"

    .prologue
    .line 81
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;

    move-result-object v0

    instance-of v0, v0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    if-eqz v0, :cond_10

    .line 82
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot install TouchExplorationHelper on a View that already has a helper installed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_10
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    .line 87
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mOnHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 89
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 90
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 93
    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->invalidateParent()V

    .line 94
    return-void
.end method

.method public final invalidateParent()V
    .registers 3

    .prologue
    .line 128
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->clearCache()V

    .line 130
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 132
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 133
    return-void
.end method

.method public final performAction(IILandroid/os/Bundle;)Z
    .registers 8
    .parameter "virtualViewId"
    .parameter "action"
    .parameter "arguments"

    .prologue
    .line 182
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    const/4 v2, -0x1

    if-ne p1, v2, :cond_c

    .line 183
    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v3, p2, p3}, Landroid/support/v4/view/AccessibilityDelegateCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    .line 214
    :goto_b
    return v0

    .line 186
    :cond_c
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getItemForId(I)Ljava/lang/Object;

    move-result-object v1

    .line 187
    .local v1, item:Ljava/lang/Object;,"TT;"
    if-nez v1, :cond_14

    .line 188
    const/4 v0, 0x0

    goto :goto_b

    .line 191
    :cond_14
    const/4 v0, 0x0

    .line 193
    .local v0, handled:Z
    sparse-switch p2, :sswitch_data_3c

    .line 212
    :cond_18
    :goto_18
    invoke-virtual {p0, v1, p2, p3}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 214
    goto :goto_b

    .line 195
    :sswitch_1e
    iget v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    if-eq v2, p1, :cond_18

    .line 196
    iput p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    .line 197
    const v2, 0x8000

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    .line 199
    const/4 v0, 0x1

    goto :goto_18

    .line 203
    :sswitch_2c
    iget v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    if-ne v2, p1, :cond_18

    .line 204
    const/high16 v2, -0x8000

    iput v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    .line 205
    const/high16 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    .line 207
    const/4 v0, 0x1

    goto :goto_18

    .line 193
    nop

    :sswitch_data_3c
    .sparse-switch
        0x40 -> :sswitch_1e
        0x80 -> :sswitch_2c
    .end sparse-switch
.end method

.method protected abstract performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation
.end method

.method protected abstract populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ")V"
        }
    .end annotation
.end method

.method protected abstract populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")V"
        }
    .end annotation
.end method

.method public final uninstall()V
    .registers 4

    .prologue
    .local p0, this:Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;,"Lcom/googlecode/eyesfree/utils/TouchExplorationHelper<TT;>;"
    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    if-nez v0, :cond_d

    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot uninstall TouchExplorationHelper on a View that does not have a helper installed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_d
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    new-instance v1, Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-direct {v1}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    .line 111
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    .line 114
    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->clearCache()V

    .line 116
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 117
    iput-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    .line 118
    return-void
.end method
