.class public abstract Lcom/twitter/android/TweetListFragment;
.super Lcom/twitter/android/BaseListFragment;

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/twitter/android/cl;
.implements Lcom/twitter/android/widget/av;


# instance fields
.field protected B:Lcom/twitter/android/provider/m;

.field protected C:Lcom/twitter/android/gf;

.field private final m:Ljava/util/HashMap;

.field private n:I

.field private o:I

.field private p:Landroid/view/GestureDetector;

.field private q:Z

.field private r:Lcom/twitter/android/gd;

.field private s:J

.field private t:I


# direct methods
.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Lcom/twitter/android/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->m:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/TweetListFragment;->s:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/TweetListFragment;->t:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetListFragment;)Lcom/twitter/android/gd;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    return-object v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->j:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setPressed(Z)V

    if-eqz p1, :cond_13

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_13
    return-void
.end method

.method private c(J)V
    .registers 6

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    :cond_6
    :goto_6
    return-void

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->m:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/android/client/b;->a(J)Lcom/twitter/android/client/Session;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v2, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-static {v0}, Lcom/twitter/android/util/z;->b(Ljava/util/Collection;)[J

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;[J)V

    goto :goto_6
.end method


# virtual methods
.method public final a(JLjava/lang/String;Lcom/twitter/android/api/PromotedContent;)V
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p4}, Lcom/twitter/android/client/b;->a(Landroid/content/Context;JLcom/twitter/android/api/PromotedContent;)V

    return-void
.end method

.method final a(Landroid/view/MotionEvent;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    invoke-virtual {v0}, Lcom/twitter/android/gd;->c()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetListFragment;->b(Landroid/view/MotionEvent;)V

    :cond_d
    return-void
.end method

.method public final synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .registers 8

    check-cast p2, Lcom/twitter/android/provider/m;

    iget-boolean v0, p2, Lcom/twitter/android/provider/m;->K:Z

    if-nez v0, :cond_3a

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    iget-wide v2, p2, Lcom/twitter/android/provider/m;->J:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->m:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-nez v0, :cond_26

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/TweetListFragment;->m:Ljava/util/HashMap;

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_26
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    iget-object v0, p2, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    if-eqz v0, :cond_3a

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/api/PromotedContent;)V

    :cond_3a
    return-void
.end method

.method protected final a(Lcom/twitter/android/gd;)V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    if-eq p1, v0, :cond_17

    if-eqz p1, :cond_15

    iget-wide v0, p0, Lcom/twitter/android/TweetListFragment;->s:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_15

    iget-wide v0, p0, Lcom/twitter/android/TweetListFragment;->s:J

    iget v2, p0, Lcom/twitter/android/TweetListFragment;->t:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/twitter/android/gd;->a(JI)V

    :cond_15
    iput-object p1, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    :cond_17
    return-void
.end method

.method protected a(Z)V
    .registers 5

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_14

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->m:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_14
    return-void
.end method

.method final a(JLandroid/view/MotionEvent;I)Z
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    invoke-virtual {v0, p1, p2, p4}, Lcom/twitter/android/gd;->b(JI)V

    invoke-direct {p0, p3}, Lcom/twitter/android/TweetListFragment;->b(Landroid/view/MotionEvent;)V

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public b(J)V
    .registers 5

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseListFragment;->b(J)V

    iget-wide v0, p0, Lcom/twitter/android/TweetListFragment;->i:J

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetListFragment;->c(J)V

    return-void
.end method

.method protected e(I)Landroid/app/Dialog;
    .registers 8

    const v5, 0x7f0b00ec

    const v4, 0x7f0b001a

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_84

    :goto_a
    return-object v0

    :pswitch_b
    new-instance v1, Lcom/twitter/android/fy;

    invoke-direct {v1, p0}, Lcom/twitter/android/fy;-><init>(Lcom/twitter/android/TweetListFragment;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b005a

    invoke-virtual {p0, v3}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b0059

    invoke-virtual {p0, v3}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_a

    :pswitch_4b
    new-instance v1, Lcom/twitter/android/fz;

    invoke-direct {v1, p0}, Lcom/twitter/android/fz;-><init>(Lcom/twitter/android/TweetListFragment;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b01d3

    invoke-virtual {p0, v3}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b01d4

    invoke-virtual {p0, v3}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_a

    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_b
        :pswitch_4b
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_23

    const-string v0, "state_delete_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/provider/m;

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/provider/m;

    const-string v0, "state_revealer_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/TweetListFragment;->s:J

    const-string v0, "state_revealer_direction"

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetListFragment;->t:I

    :cond_23
    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_31

    const-string v2, "en_gest"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_35

    :cond_31
    move v0, v1

    :goto_32
    iput-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->q:Z

    return-void

    :cond_35
    const/4 v0, 0x0

    goto :goto_32
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 8

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/gc;

    if-eqz v0, :cond_12

    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p4, p5, v0, v1}, Lcom/twitter/android/TweetListFragment;->a(JLandroid/view/MotionEvent;I)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/provider/m;

    if-eqz v0, :cond_e

    const-string v0, "state_delete_key"

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/provider/m;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_e
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    if-eqz v0, :cond_28

    const-string v0, "state_revealer_id"

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    invoke-virtual {v1}, Lcom/twitter/android/gd;->a()J

    move-result-wide v1

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "state_revealer_direction"

    iget-object v1, p0, Lcom/twitter/android/TweetListFragment;->r:Lcom/twitter/android/gd;

    invoke-virtual {v1}, Lcom/twitter/android/gd;->b()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_28
    return-void
.end method

.method public onStop()V
    .registers 3

    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onStop()V

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TweetListFragment;->c(J)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    packed-switch v0, :pswitch_data_26

    :cond_c
    :goto_c
    :pswitch_c
    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->p:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_13
    iput v1, p0, Lcom/twitter/android/TweetListFragment;->n:I

    goto :goto_c

    :pswitch_16
    iget v0, p0, Lcom/twitter/android/TweetListFragment;->n:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/TweetListFragment;->o:I

    if-le v0, v1, :cond_c

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/MotionEvent;)V

    goto :goto_c

    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_13
        :pswitch_c
        :pswitch_16
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 11

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/twitter/android/TweetListFragment;->q:Z

    if-nez v0, :cond_8

    :cond_7
    :goto_7
    return-void

    :cond_8
    iget-object v4, p0, Lcom/twitter/android/TweetListFragment;->j:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/twitter/android/fv;

    invoke-direct {v0, p0}, Lcom/twitter/android/fv;-><init>(Lcom/twitter/android/TweetListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetListFragment;->C:Lcom/twitter/android/gf;

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v4, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    mul-int/lit8 v2, v5, 0x2

    iput v5, p0, Lcom/twitter/android/TweetListFragment;->o:I

    new-instance v7, Landroid/view/GestureDetector;

    new-instance v0, Lcom/twitter/android/fx;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/fx;-><init>(Lcom/twitter/android/TweetListFragment;IILandroid/widget/ListView;I)V

    invoke-direct {v7, v6, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v7, p0, Lcom/twitter/android/TweetListFragment;->p:Landroid/view/GestureDetector;

    goto :goto_7
.end method
