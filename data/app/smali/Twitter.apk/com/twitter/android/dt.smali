.class final Lcom/twitter/android/dt;
.super Landroid/support/v4/view/PagerAdapter;


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;

.field private final b:Landroid/view/LayoutInflater;

.field private c:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;Landroid/view/LayoutInflater;)V
    .registers 3

    iput-object p1, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/dt;->b:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 4

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public final getCount()I
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->q(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->r(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->h(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    :goto_25
    return v0

    :cond_26
    const/4 v0, 0x2

    goto :goto_25
.end method

.method public final getItemPosition(Ljava/lang/Object;)I
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/dt;->c:Landroid/view/View;

    if-ne p1, v0, :cond_2a

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->q(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->r(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->h(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const/4 v0, -0x2

    :goto_29
    return v0

    :cond_2a
    const/4 v0, -0x1

    goto :goto_29
.end method

.method public final instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 6

    const/4 v2, 0x0

    if-nez p2, :cond_70

    iget-object v0, p0, Lcom/twitter/android/dt;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03006f

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f07007a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->i(Lcom/twitter/android/ProfileFragment;)V

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f070028

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->j(Lcom/twitter/android/ProfileFragment;)V

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f070027

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0700e5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->k(Lcom/twitter/android/ProfileFragment;)V

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->l(Lcom/twitter/android/ProfileFragment;)V

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f07006c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/android/ProfileFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->m(Lcom/twitter/android/ProfileFragment;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v0, v1

    :goto_6f
    return-object v0

    :cond_70
    const/4 v0, 0x1

    if-ne p2, v0, :cond_bc

    iget-object v0, p0, Lcom/twitter/android/dt;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030070

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0700e6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->d(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->n(Lcom/twitter/android/ProfileFragment;)V

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0700e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->o(Lcom/twitter/android/ProfileFragment;)V

    iget-object v2, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    const v0, 0x7f0700e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v2, v0}, Lcom/twitter/android/ProfileFragment;->f(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/dt;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->p(Lcom/twitter/android/ProfileFragment;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iput-object v1, p0, Lcom/twitter/android/dt;->c:Landroid/view/View;

    move-object v0, v1

    goto :goto_6f

    :cond_bc
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Number of items is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4

    if-ne p1, p2, :cond_4

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method
