.class public final Lcom/twitter/android/fo;
.super Landroid/support/v4/widget/CursorAdapter;


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/support/v4/util/LruCache;

.field private final c:Lcom/twitter/android/client/b;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ref_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "cards"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/fo;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/b;)V
    .registers 5

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/fo;->b:Landroid/support/v4/util/LruCache;

    iput-object p2, p0, Lcom/twitter/android/fo;->c:Lcom/twitter/android/client/b;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/fo;->d:I

    return-void
.end method

.method private a(Landroid/database/Cursor;)Lcom/twitter/android/api/TweetMedia;
    .registers 6

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iget-object v0, p0, Lcom/twitter/android/fo;->b:Landroid/support/v4/util/LruCache;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    if-eqz v0, :cond_38

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetMedia;

    :goto_19
    if-nez v0, :cond_37

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/z;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/api/TweetMedia;

    const/4 v3, 0x0

    aget-object v0, v0, v3

    iget-object v3, p0, Lcom/twitter/android/fo;->b:Landroid/support/v4/util/LruCache;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v1, v2}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_37
    return-object v0

    :cond_38
    const/4 v0, 0x0

    goto :goto_19
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 9

    invoke-direct {p0, p3}, Lcom/twitter/android/fo;->a(Landroid/database/Cursor;)Lcom/twitter/android/api/TweetMedia;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fo;->c:Lcom/twitter/android/client/b;

    iget-object v0, v0, Lcom/twitter/android/api/TweetMedia;->imageUrl:Ljava/lang/String;

    check-cast p1, Landroid/widget/ImageView;

    new-instance v2, Lcom/twitter/android/util/f;

    iget v3, p0, Lcom/twitter/android/fo;->d:I

    iget v4, p0, Lcom/twitter/android/fo;->d:I

    invoke-direct {v2, v0, v3, v4}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 4

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_17

    invoke-direct {p0, v0}, Lcom/twitter/android/fo;->a(Landroid/database/Cursor;)Lcom/twitter/android/api/TweetMedia;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fo;->c:Lcom/twitter/android/client/b;

    iget v1, v1, Lcom/twitter/android/client/b;->g:F

    invoke-virtual {v0, v1}, Lcom/twitter/android/api/TweetMedia;->a(F)Lcom/twitter/android/api/m;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/api/m;->a:Ljava/lang/String;

    :goto_16
    return-object v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public final getItemId(I)J
    .registers 4

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_e

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_d
    return-wide v0

    :cond_e
    const-wide/16 v0, 0x0

    goto :goto_d
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030033

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
