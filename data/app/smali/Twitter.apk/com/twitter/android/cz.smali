.class final Lcom/twitter/android/cz;
.super Landroid/os/AsyncTask;


# instance fields
.field private final a:Lcom/twitter/android/PostActivity;

.field private final b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;Landroid/content/Context;)V
    .registers 3

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/cz;->a:Lcom/twitter/android/PostActivity;

    iput-object p2, p0, Lcom/twitter/android/cz;->b:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/twitter/android/dh;)F
    .registers 6

    const/4 v0, 0x0

    :try_start_1
    iget-object v1, p0, Lcom/twitter/android/cz;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/android/dh;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_1f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_c} :catch_19

    move-result-object v1

    :try_start_d
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I
    :try_end_10
    .catchall {:try_start_d .. :try_end_10} :catchall_27
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_29

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x4480

    div-float/2addr v0, v2

    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :goto_18
    return v0

    :catch_19
    move-exception v1

    :goto_1a
    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    goto :goto_18

    :catchall_1f
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_23
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_27
    move-exception v0

    goto :goto_23

    :catch_29
    move-exception v0

    move-object v0, v1

    goto :goto_1a
.end method

.method private varargs a([Lcom/twitter/android/dh;)Lcom/twitter/android/dh;
    .registers 10

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    aget-object v6, p1, v7

    :try_start_5
    invoke-direct {p0, v6}, Lcom/twitter/android/cz;->a(Lcom/twitter/android/dh;)F
    :try_end_8
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_8} :catch_60

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/cz;->b:Landroid/content/Context;

    const v2, 0x7f0b014a

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {v1, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/twitter/android/dh;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/cz;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/twitter/android/dh;->b:Landroid/net/Uri;

    new-array v2, v5, [Ljava/lang/String;

    const-string v4, "title"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_41

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3e

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/twitter/android/dh;->e:Ljava/lang/String;

    :cond_3e
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_41
    iget-object v0, v6, Lcom/twitter/android/dh;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_5e

    const/high16 v0, 0x4290

    iget-object v1, p0, Lcom/twitter/android/cz;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/cz;->b:Landroid/content/Context;

    iget-object v2, v6, Lcom/twitter/android/dh;->b:Landroid/net/Uri;

    invoke-static {v1, v2, v0, v0, v7}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;FFI)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v6, Lcom/twitter/android/dh;->d:Landroid/graphics/Bitmap;

    :cond_5e
    move-object v0, v6

    :goto_5f
    return-object v0

    :catch_60
    move-exception v0

    iput v5, v6, Lcom/twitter/android/dh;->c:I

    move-object v0, v6

    goto :goto_5f
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    check-cast p1, [Lcom/twitter/android/dh;

    invoke-direct {p0, p1}, Lcom/twitter/android/cz;->a([Lcom/twitter/android/dh;)Lcom/twitter/android/dh;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 3

    check-cast p1, Lcom/twitter/android/dh;

    iget-object v0, p0, Lcom/twitter/android/cz;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity;Lcom/twitter/android/dh;)V

    return-void
.end method

.method protected final onPreExecute()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/cz;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->h(Lcom/twitter/android/PostActivity;)V

    return-void
.end method
