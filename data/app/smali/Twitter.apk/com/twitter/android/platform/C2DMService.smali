.class public Lcom/twitter/android/platform/C2DMService;
.super Landroid/app/IntentService;


# static fields
.field public static a:I

.field public static b:I

.field static final c:Ljava/util/HashMap;

.field private static final d:I

.field private static final e:Z

.field private static final f:Z

.field private static final g:Ljava/util/HashMap;

.field private static h:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private i:Lcom/twitter/android/network/d;

.field private j:Ljava/lang/String;

.field private k:Lcom/twitter/android/network/p;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x20

    sput v2, Lcom/twitter/android/platform/C2DMService;->a:I

    const/16 v2, 0x40

    sput v2, Lcom/twitter/android/platform/C2DMService;->b:I

    sget v2, Lcom/twitter/android/platform/C2DMService;->a:I

    or-int/lit8 v2, v2, 0x5

    or-int/lit16 v2, v2, 0x80

    or-int/lit8 v2, v2, 0x10

    sput v2, Lcom/twitter/android/platform/C2DMService;->d:I

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/twitter/android/platform/C2DMService;->c:Ljava/util/HashMap;

    sput-boolean v0, Lcom/twitter/android/platform/C2DMService;->e:Z

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x8

    if-lt v2, v3, :cond_27

    move v0, v1

    :cond_27
    sput-boolean v0, Lcom/twitter/android/platform/C2DMService;->f:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->c:Ljava/util/HashMap;

    const-string v2, "com.google.android.c2dm.intent.REGISTRATION"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->c:Ljava/util/HashMap;

    const-string v2, "com.google.android.c2dm.intent.RECEIVE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->c:Ljava/util/HashMap;

    const-string v2, "com.google.android.c2dm.intent.RETRY"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->c:Ljava/util/HashMap;

    const-string v2, "com.twitter.android.action.c2dm.ENABLE"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    const-string v2, "tweet"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    const-string v1, "mention"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    const-string v1, "direct_message"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    const-string v1, "favorited"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    const-string v1, "retweeted"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    const-string v1, "followed"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    const-string v0, "C2DMService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/android/network/k;Z)Lcom/twitter/android/network/c;
    .registers 9

    new-instance v0, Lcom/twitter/android/network/j;

    iget-object v1, p0, Lcom/twitter/android/platform/C2DMService;->i:Lcom/twitter/android/network/d;

    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v3, Lcom/twitter/android/network/a;

    invoke-direct {v3, p2}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/network/j;-><init>(Lcom/twitter/android/network/d;Lorg/apache/http/client/methods/HttpPost;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)V

    if-eqz p3, :cond_1e

    iget-object v1, p0, Lcom/twitter/android/platform/C2DMService;->k:Lcom/twitter/android/network/p;

    invoke-virtual {v1, v0}, Lcom/twitter/android/network/p;->b(Lcom/twitter/android/network/c;)V

    :goto_19
    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    return-object v0

    :cond_1e
    iget-object v1, p0, Lcom/twitter/android/platform/C2DMService;->k:Lcom/twitter/android/network/p;

    invoke-virtual {v1, v0}, Lcom/twitter/android/network/p;->a(Lcom/twitter/android/network/c;)V

    goto :goto_19
.end method

.method private a()V
    .registers 4

    const-wide/16 v0, 0x4e20

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/platform/C2DMService;->a(JI)V

    return-void
.end method

.method private a(JI)V
    .registers 6

    const-string v0, "c2dm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/platform/C2DMService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "backoff"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "backoff_ceil"

    invoke-interface {v0, v1, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private a(Landroid/accounts/Account;Lcom/twitter/android/network/k;Ljava/lang/String;IZ)V
    .registers 13

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p2, :cond_c7

    iget-object v2, p0, Lcom/twitter/android/platform/C2DMService;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/platform/C2DMService;->k:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "account"

    aput-object v5, v4, v0

    const-string v5, "push_destinations"

    aput-object v5, v4, v1

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "udid"

    invoke-static {v3, v4, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "enabled_for"

    invoke-static {v3, v2, p4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/twitter/android/platform/C2DMService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v2, :cond_3c

    const-string v4, "lang"

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3c
    if-eqz p3, :cond_43

    const-string v2, "token"

    invoke-static {v3, v2, p3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, p2, p5}, Lcom/twitter/android/platform/C2DMService;->a(Ljava/lang/String;Lcom/twitter/android/network/k;Z)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->b()Z

    move-result v3

    if-nez v3, :cond_57

    iget v2, v2, Lcom/twitter/android/network/c;->b:I

    const/16 v3, 0x130

    if-ne v2, v3, :cond_58

    :cond_57
    move v0, v1

    :cond_58
    if-eqz v0, :cond_ac

    sget-object v2, Lcom/twitter/android/provider/NotificationSetting;->a:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v2, p4}, Lcom/twitter/android/provider/NotificationSetting;->e(I)I

    move-result v2

    or-int/lit8 v2, v2, 0x0

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->c:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, p4}, Lcom/twitter/android/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->b:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, p4}, Lcom/twitter/android/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->d:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, p4}, Lcom/twitter/android/provider/NotificationSetting;->e(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->e:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, p4}, Lcom/twitter/android/provider/NotificationSetting;->e(I)I

    move-result v3

    sget-object v4, Lcom/twitter/android/provider/NotificationSetting;->f:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v4, p4}, Lcom/twitter/android/provider/NotificationSetting;->e(I)I

    move-result v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "notif_mention"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "notif_message"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "notif_tweet"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v2

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v5, v1}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;Landroid/content/ContentValues;Z)I

    :cond_ac
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {p0, p1, v1, v2}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;Landroid/accounts/Account;J)V

    if-nez p5, :cond_c7

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.twitter.android.c2dm.push_result"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "error_code"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.twitter.android.permission.RESTRICTED"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/platform/C2DMService;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_c7
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;)V
    .registers 8

    const-wide/16 v4, 0x0

    new-instance v0, Lcom/twitter/android/client/a;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v2, "c2dm"

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/client/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v1, "last_refresh."

    invoke-virtual {v0, v1, v4, v5}, Lcom/twitter/android/client/a;->a(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_22

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_2a

    :cond_22
    invoke-static {p0, p1}, Lcom/twitter/android/platform/C2DMService;->c(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;Landroid/accounts/Account;IZ)V

    :cond_2a
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;IZ)V
    .registers 11

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    :goto_8
    return-void

    :cond_9
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    if-eqz v0, :cond_a3

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v3, "com.twitter.android.auth.login"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    move v0, v2

    :goto_1b
    if-ge v0, v4, :cond_a3

    aget-object v5, v3, v0

    const-string v6, "com.twitter.android.provider.TwitterProvider"

    invoke-static {v5, v6}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_58

    const-string v6, "com.twitter.android.provider.TwitterProvider"

    invoke-static {v5, v6}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_58

    move v0, v1

    :goto_30
    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5b

    :goto_3a
    if-eq v0, v1, :cond_7d

    if-eqz v0, :cond_5d

    new-instance v0, Lcom/twitter/android/client/a;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v3, "c2dm"

    invoke-direct {v0, p0, v1, v3, v2}, Lcom/twitter/android/client/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/twitter/android/client/a;->a()Lcom/twitter/android/client/a;

    move-result-object v0

    const-string v1, "reg_enabled_for."

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/client/a;->b(Ljava/lang/String;I)Lcom/twitter/android/client/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/a;->c()Z

    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->c(Landroid/content/Context;)V

    goto :goto_8

    :cond_58
    add-int/lit8 v0, v0, 0x1

    goto :goto_1b

    :cond_5b
    move v1, v2

    goto :goto_3a

    :cond_5d
    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.UNREGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v2, v3, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_8

    :cond_7d
    if-eqz v0, :cond_8

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/platform/C2DMService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.twitter.android.action.c2dm.ENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "enabled_for"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "polling"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_8

    :cond_a3
    move v0, v2

    goto :goto_30
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;J)V
    .registers 8

    new-instance v0, Lcom/twitter/android/client/a;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v2, "c2dm"

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/client/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/twitter/android/client/a;->a()Lcom/twitter/android/client/a;

    move-result-object v0

    const-string v1, "last_refresh."

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/android/client/a;->b(Ljava/lang/String;J)Lcom/twitter/android/client/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/a;->c()Z

    return-void
.end method

.method static declared-synchronized a(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6

    const-class v1, Lcom/twitter/android/platform/C2DMService;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/twitter/android/platform/C2DMService;->h:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_18

    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "C2DMService"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/platform/C2DMService;->h:Landroid/os/PowerManager$WakeLock;

    :cond_18
    sget-object v0, Lcom/twitter/android/platform/C2DMService;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-class v0, Lcom/twitter/android/platform/C2DMService;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_25
    .catchall {:try_start_3 .. :try_end_25} :catchall_27

    monitor-exit v1

    return-void

    :catchall_27
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 4

    const-string v0, "c2dm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/platform/C2DMService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    if-eqz p1, :cond_16

    const-string v1, "reg_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_12
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void

    :cond_16
    const-string v1, "reg_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_12
.end method

.method public static a(Landroid/content/Context;)Z
    .registers 4

    const/4 v0, 0x0

    sget-boolean v1, Lcom/twitter/android/platform/C2DMService;->f:Z

    if-nez v1, :cond_6

    :cond_5
    :goto_5
    return v0

    :cond_6
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_5

    array-length v1, v1

    if-lez v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method private static b(Landroid/content/Context;)Ljava/lang/String;
    .registers 4

    const-string v0, "c2dm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "reg_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Landroid/accounts/Account;)V
    .registers 4

    invoke-static {p0, p1}, Lcom/twitter/android/platform/C2DMService;->c(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;Landroid/accounts/Account;IZ)V

    return-void
.end method

.method private static c(Landroid/content/Context;Landroid/accounts/Account;)I
    .registers 12

    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/16 v6, 0x55

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/provider/d;->a:Landroid/net/Uri;

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "notif_mention"

    aput-object v4, v2, v8

    const-string v4, "notif_message"

    aput-object v4, v2, v7

    const-string v4, "notif_tweet"

    aput-object v4, v2, v9

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_88

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_85

    invoke-interface {v3, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_83

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_39
    invoke-interface {v3, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_81

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_43
    invoke-interface {v3, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_7e

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move v6, v0

    move v0, v2

    :goto_4f
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :goto_52
    sget-object v2, Lcom/twitter/android/provider/NotificationSetting;->a:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v2, v6}, Lcom/twitter/android/provider/NotificationSetting;->d(I)I

    move-result v2

    or-int/lit8 v2, v2, 0x0

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->c:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, v6}, Lcom/twitter/android/provider/NotificationSetting;->d(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->b:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, v6}, Lcom/twitter/android/provider/NotificationSetting;->d(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->d:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, v6}, Lcom/twitter/android/provider/NotificationSetting;->d(I)I

    move-result v3

    or-int/2addr v2, v3

    sget-object v3, Lcom/twitter/android/provider/NotificationSetting;->e:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v3, v1}, Lcom/twitter/android/provider/NotificationSetting;->d(I)I

    move-result v1

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/android/provider/NotificationSetting;->f:Lcom/twitter/android/provider/NotificationSetting;

    invoke-virtual {v2, v0}, Lcom/twitter/android/provider/NotificationSetting;->d(I)I

    move-result v0

    or-int/2addr v0, v1

    return v0

    :cond_7e
    move v6, v0

    move v0, v8

    goto :goto_4f

    :cond_81
    move v1, v7

    goto :goto_43

    :cond_83
    move v0, v6

    goto :goto_39

    :cond_85
    move v0, v8

    move v1, v7

    goto :goto_4f

    :cond_88
    move v0, v8

    move v1, v7

    goto :goto_52
.end method

.method private static c(Landroid/content/Context;)V
    .registers 5

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_8

    :goto_7
    return-void

    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.c2dm.intent.REGISTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "app"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-static {p0, v3, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sender"

    const-string v2, "twittermobileclients@gmail.com"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_7
.end method


# virtual methods
.method public onCreate()V
    .registers 12

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-virtual {p0}, Lcom/twitter/android/platform/C2DMService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "android_id"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/platform/C2DMService;->j:Ljava/lang/String;

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/platform/C2DMService;->i:Lcom/twitter/android/network/d;

    invoke-static {p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/platform/C2DMService;->k:Lcom/twitter/android/network/p;

    const-string v0, "c2dm"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/platform/C2DMService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v0, "ver"

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-gtz v0, :cond_aa

    invoke-interface {v2}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9c

    if-nez v0, :cond_9c

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v3, "com.twitter.android.auth.login"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    if-eqz v3, :cond_9c

    array-length v4, v3

    move v0, v1

    :goto_44
    if-ge v0, v4, :cond_9c

    aget-object v5, v3, v0

    new-instance v6, Lcom/twitter/android/client/a;

    iget-object v7, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v8, "c2dm"

    invoke-direct {v6, p0, v7, v8, v1}, Lcom/twitter/android/client/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v6}, Lcom/twitter/android/client/a;->a()Lcom/twitter/android/client/a;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "last_refresh."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_76

    const-string v8, "last_refresh."

    const-wide/16 v9, 0x0

    invoke-interface {v2, v7, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    invoke-virtual {v6, v8, v9, v10}, Lcom/twitter/android/client/a;->b(Ljava/lang/String;J)Lcom/twitter/android/client/a;

    :cond_76
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "reg_enabled_for."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_96

    const-string v7, "reg_enabled_for."

    invoke-interface {v2, v5, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v6, v7, v5}, Lcom/twitter/android/client/a;->b(Ljava/lang/String;I)Lcom/twitter/android/client/a;

    :cond_96
    invoke-virtual {v6}, Lcom/twitter/android/client/a;->c()Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_44

    :cond_9c
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ver"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_aa
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .registers 14

    const/4 v1, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/twitter/android/platform/C2DMService;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_18

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_14
    packed-switch v0, :pswitch_data_384

    :cond_17
    :goto_17
    return-void

    :cond_18
    move v0, v5

    goto :goto_14

    :pswitch_1a
    packed-switch v0, :pswitch_data_390

    :cond_1d
    :goto_1d
    sget-object v0, Lcom/twitter/android/platform/C2DMService;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_17

    :pswitch_23
    :try_start_23
    const-string v0, "error"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "unregistered"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "registration_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_9b

    sget-boolean v0, Lcom/twitter/android/platform/C2DMService;->e:Z

    if-eqz v0, :cond_4f

    const-string v0, "C2DMService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unregistered -> "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4f
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/platform/C2DMService;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/platform/C2DMService;->a()V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v1, "com.twitter.android.auth.login"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    :goto_61
    if-ge v5, v2, :cond_1d

    aget-object v3, v1, v5

    invoke-static {v0, v3}, Lcom/twitter/android/platform/j;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/android/network/k;

    move-result-object v3

    if-eqz v3, :cond_98

    iget-object v4, p0, Lcom/twitter/android/platform/C2DMService;->j:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/platform/C2DMService;->k:Lcom/twitter/android/network/p;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "account"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "push_destinations"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string v9, "destroy"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "udid"

    invoke-static {v6, v7, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-direct {p0, v4, v3, v6}, Lcom/twitter/android/platform/C2DMService;->a(Ljava/lang/String;Lcom/twitter/android/network/k;Z)Lcom/twitter/android/network/c;

    :cond_98
    add-int/lit8 v5, v5, 0x1

    goto :goto_61

    :cond_9b
    if-eqz v0, :cond_109

    sget-boolean v1, Lcom/twitter/android/platform/C2DMService;->e:Z

    if-eqz v1, :cond_b5

    const-string v1, "C2DMService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Registration error -> "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b5
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/twitter/android/platform/C2DMService;->a(Ljava/lang/String;)V

    const-string v1, "SERVICE_NOT_AVAILABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const-string v0, "c2dm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/platform/C2DMService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "backoff"

    const-wide/16 v2, 0x4e20

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v3, "backoff_ceil"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    new-instance v4, Landroid/content/Intent;

    const-string v0, "com.google.android.c2dm.intent.RETRY"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/twitter/android/platform/C2DMService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v5, 0x3

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {p0, v6, v4, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v5, v1, v2, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    int-to-long v4, v3

    const-wide/16 v6, 0x5

    cmp-long v0, v4, v6

    if-gez v0, :cond_1d

    const-wide/16 v4, 0x2

    mul-long v0, v1, v4

    add-int/lit8 v2, v3, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/platform/C2DMService;->a(JI)V
    :try_end_100
    .catchall {:try_start_23 .. :try_end_100} :catchall_102

    goto/16 :goto_1d

    :catchall_102
    move-exception v0

    sget-object v1, Lcom/twitter/android/platform/C2DMService;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    :cond_109
    if-eqz v3, :cond_1d

    :try_start_10b
    sget-boolean v0, Lcom/twitter/android/platform/C2DMService;->e:Z

    if-eqz v0, :cond_123

    const-string v0, "C2DMService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Registered -> "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_123
    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v3}, Lcom/twitter/android/platform/C2DMService;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/platform/C2DMService;->a()V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v8

    const-string v0, "com.twitter.android.auth.login"

    invoke-virtual {v8, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v9

    array-length v10, v9

    move v6, v5

    :goto_139
    if-ge v6, v10, :cond_1d

    aget-object v1, v9, v6

    const-wide/16 v4, 0x0

    invoke-static {p0, v1, v4, v5}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;Landroid/accounts/Account;J)V

    invoke-static {v8, v1}, Lcom/twitter/android/platform/j;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/android/network/k;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/client/a;

    iget-object v4, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v5, "c2dm"

    const/4 v11, 0x0

    invoke-direct {v0, p0, v4, v5, v11}, Lcom/twitter/android/client/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    const-string v4, "reg_enabled_for."

    const/4 v5, -0x1

    invoke-virtual {v0, v4, v5}, Lcom/twitter/android/client/a;->a(Ljava/lang/String;I)I

    move-result v4

    const/4 v0, -0x1

    if-ne v4, v0, :cond_162

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16b

    sget v4, Lcom/twitter/android/platform/C2DMService;->d:I

    :cond_162
    :goto_162
    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/accounts/Account;Lcom/twitter/android/network/k;Ljava/lang/String;IZ)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_139

    :cond_16b
    invoke-static {p0, v1}, Lcom/twitter/android/platform/C2DMService;->c(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v4

    goto :goto_162

    :pswitch_170
    sget-boolean v0, Lcom/twitter/android/platform/C2DMService;->e:Z

    if-eqz v0, :cond_17b

    const-string v0, "C2DMService"

    const-string v2, "Push received."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17b
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "recipient_name"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/twitter/android/platform/j;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1a1

    move-object v0, v1

    :goto_18c
    if-eqz v0, :cond_1d

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.twitter.android.poll.data"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "data"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "com.twitter.android.permission.RESTRICTED"

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/platform/C2DMService;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto/16 :goto_1d

    :cond_1a1
    const-string v0, "collapse_key"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/twitter/android/platform/C2DMService;->g:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1b3

    move-object v0, v1

    goto :goto_18c

    :cond_1b3
    const-string v5, "user_id"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "status_id"

    invoke-virtual {v2, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "sender_name"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "text"

    invoke-virtual {v2, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    sget-boolean v2, Lcom/twitter/android/platform/C2DMService;->e:Z

    if-eqz v2, :cond_247

    const-string v2, "C2DMService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "collapse_key: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "C2DMService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "sender_name: "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "C2DMService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "recipient_name: "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "C2DMService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "user_id: "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "C2DMService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "status_id: "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "C2DMService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v9, "text: "

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_247
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v9

    invoke-static {p0, v4, v5}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v10

    new-instance v2, Lcom/twitter/android/platform/d;

    const/4 v11, 0x1

    invoke-direct {v2, v3, v4, v5, v11}, Lcom/twitter/android/platform/d;-><init>(Ljava/lang/String;JZ)V

    new-instance v4, Lcom/twitter/android/platform/f;

    invoke-direct {v4}, Lcom/twitter/android/platform/f;-><init>()V

    const/4 v5, 0x1

    iput v5, v4, Lcom/twitter/android/platform/f;->b:I

    iput-object v8, v4, Lcom/twitter/android/platform/f;->c:Ljava/lang/String;

    iput-object v7, v4, Lcom/twitter/android/platform/f;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_39a

    move-object v0, v1

    goto/16 :goto_18c

    :pswitch_273
    const-string v0, "tweet"

    const/4 v1, 0x1

    invoke-virtual {v9, v3, v0, v1}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    const/4 v0, 0x1

    iput v0, v4, Lcom/twitter/android/platform/f;->a:I

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/twitter/android/platform/f;->d:J

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {v10, v3, v0, v7, v1}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v0

    iput v0, v4, Lcom/twitter/android/platform/f;->g:I

    iput-object v4, v2, Lcom/twitter/android/platform/d;->f:Lcom/twitter/android/platform/f;

    :goto_290
    invoke-virtual {v9, v3}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/twitter/android/platform/d;->d:I

    move-object v0, v2

    goto/16 :goto_18c

    :pswitch_299
    const/4 v0, 0x1

    invoke-virtual {v9, v3, v0}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I

    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/twitter/android/provider/ae;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/twitter/android/platform/f;->a:I

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/twitter/android/platform/f;->d:J

    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-virtual {v10, v3, v0, v7, v1}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v0

    iput v0, v4, Lcom/twitter/android/platform/f;->g:I

    const/4 v0, 0x1

    iput v0, v2, Lcom/twitter/android/platform/d;->e:I

    iput-object v4, v2, Lcom/twitter/android/platform/d;->i:Lcom/twitter/android/platform/f;

    goto :goto_290

    :pswitch_2be
    const-string v0, "message"

    const/4 v1, 0x1

    invoke-virtual {v9, v3, v0, v1}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    invoke-virtual {v10}, Lcom/twitter/android/provider/ae;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/twitter/android/platform/f;->a:I

    const/4 v0, 0x7

    const/4 v1, 0x1

    invoke-virtual {v10, v3, v0, v7, v1}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v0

    iput v0, v4, Lcom/twitter/android/platform/f;->g:I

    iput-object v4, v2, Lcom/twitter/android/platform/d;->g:Lcom/twitter/android/platform/f;

    goto :goto_290

    :pswitch_2d7
    const/4 v0, 0x2

    invoke-virtual {v9, v3, v0}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I

    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/twitter/android/provider/ae;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/twitter/android/platform/f;->a:I

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/twitter/android/platform/f;->d:J

    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {v10, v3, v0, v7, v1}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v0

    iput v0, v4, Lcom/twitter/android/platform/f;->g:I

    const/4 v0, 0x2

    iput v0, v2, Lcom/twitter/android/platform/d;->e:I

    iput-object v4, v2, Lcom/twitter/android/platform/d;->i:Lcom/twitter/android/platform/f;

    goto :goto_290

    :pswitch_2fc
    const/16 v0, 0x8

    invoke-virtual {v9, v3, v0}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I

    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/twitter/android/provider/ae;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/twitter/android/platform/f;->a:I

    invoke-static {v6}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, v4, Lcom/twitter/android/platform/f;->d:J

    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {v10, v3, v0, v7, v1}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v0

    iput v0, v4, Lcom/twitter/android/platform/f;->g:I

    const/16 v0, 0x8

    iput v0, v2, Lcom/twitter/android/platform/d;->e:I

    iput-object v4, v2, Lcom/twitter/android/platform/d;->i:Lcom/twitter/android/platform/f;

    goto/16 :goto_290

    :pswitch_324
    const/4 v0, 0x4

    invoke-virtual {v9, v3, v0}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I

    const/4 v0, 0x0

    invoke-virtual {v10, v0}, Lcom/twitter/android/provider/ae;->g(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/twitter/android/platform/f;->a:I

    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {v10, v3, v0, v7, v1}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v0

    iput v0, v4, Lcom/twitter/android/platform/f;->g:I

    const/4 v0, 0x4

    iput v0, v2, Lcom/twitter/android/platform/d;->e:I

    iput-object v4, v2, Lcom/twitter/android/platform/d;->i:Lcom/twitter/android/platform/f;

    goto/16 :goto_290

    :pswitch_340
    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->c(Landroid/content/Context;)V
    :try_end_34d
    .catchall {:try_start_10b .. :try_end_34d} :catchall_102

    goto/16 :goto_1d

    :pswitch_34f
    sget-boolean v0, Lcom/twitter/android/platform/C2DMService;->e:Z

    if-eqz v0, :cond_35a

    const-string v0, "C2DMService"

    const-string v1, "Refresh."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_35a
    invoke-static {p0}, Lcom/twitter/android/platform/C2DMService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_17

    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/twitter/android/platform/j;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/android/network/k;

    move-result-object v2

    const-string v0, "enabled_for"

    sget v4, Lcom/twitter/android/platform/C2DMService;->d:I

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "polling"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/accounts/Account;Lcom/twitter/android/network/k;Ljava/lang/String;IZ)V

    goto/16 :goto_17

    :pswitch_data_384
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_1a
        :pswitch_1a
        :pswitch_34f
    .end packed-switch

    :pswitch_data_390
    .packed-switch 0x1
        :pswitch_23
        :pswitch_170
        :pswitch_340
    .end packed-switch

    :pswitch_data_39a
    .packed-switch 0x1
        :pswitch_273
        :pswitch_299
        :pswitch_2be
        :pswitch_2d7
        :pswitch_2fc
        :pswitch_324
    .end packed-switch
.end method
