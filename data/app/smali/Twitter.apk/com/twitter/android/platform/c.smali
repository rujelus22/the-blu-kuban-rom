.class public final Lcom/twitter/android/platform/c;
.super Landroid/content/AbstractThreadedSyncAdapter;


# static fields
.field private static final a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/twitter/android/platform/c;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;I)V
    .registers 10

    const/4 v6, 0x0

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const v1, 0xea60

    mul-int/2addr v1, p2

    int-to-long v1, v1

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/client/AppService;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "com.twitter.android.poll.alarm"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "account_name"

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-static {p0, v6, v3, v6}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_3c

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v1, v4

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :goto_3b
    return-void

    :cond_3c
    invoke-virtual {v0, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_3b
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/android/network/d;Landroid/accounts/Account;Lcom/twitter/android/network/k;Lcom/twitter/android/api/ad;ILandroid/content/SyncResult;Lcom/twitter/android/platform/d;)V
    .registers 22

    sget-boolean v1, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v1, :cond_b

    const-string v1, "TwitterDataSync"

    const-string v2, "=====> Sync direct messages"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    move-object/from16 v0, p4

    iget-wide v4, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-static {p0, v4, v5}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v7

    const/4 v2, 0x4

    const/4 v6, 0x1

    move/from16 v3, p5

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_128

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Lcom/twitter/android/provider/ae;->c(I)J

    move-result-wide v2

    const/16 v6, 0x64

    move-wide v12, v2

    move v2, v6

    :goto_2b
    if-nez p5, :cond_12f

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v8, "direct_messages"

    aput-object v8, v3, v6

    const/4 v6, 0x1

    const-string v8, "sent"

    aput-object v8, v3, v6

    invoke-virtual {v7, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".json"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    :goto_44
    const-wide/16 v9, 0x0

    cmp-long v3, v12, v9

    if-lez v3, :cond_4f

    const-string v3, "since_id"

    invoke-static {v8, v3, v12, v13}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_4f
    if-lez v2, :cond_56

    const-string v3, "count"

    invoke-static {v8, v3, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_56
    const-string v2, "include_entities"

    const/4 v3, 0x1

    invoke-static {v8, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_user_entities"

    const/4 v3, 0x1

    invoke-static {v8, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :try_start_62
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v9, Lcom/twitter/android/network/a;

    move-object/from16 v0, p3

    invoke-direct {v9, v0}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    const/4 v10, 0x1

    move-object v6, p1

    invoke-static/range {v6 .. v11}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v2

    iget v3, v2, Lcom/twitter/android/network/c;->b:I

    const/16 v6, 0xc8

    if-ne v3, v6, :cond_148

    sget-object v2, Lcom/twitter/android/api/s;->b:Lorg/codehaus/jackson/a;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/codehaus/jackson/a;->a([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    invoke-static {v2}, Lcom/twitter/android/api/s;->f(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v3

    const-wide/16 v6, 0x0

    cmp-long v2, v12, v6

    if-nez v2, :cond_143

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_143

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/p;

    iget-wide v6, v2, Lcom/twitter/android/api/p;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    :goto_ad
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, v1

    move/from16 v6, p5

    invoke-virtual/range {v2 .. v9}, Lcom/twitter/android/provider/ae;->a(Ljava/util/List;JIZZLjava/lang/String;)I

    move-result v4

    sget-boolean v2, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v2, :cond_ce

    const-string v2, "TwitterDataSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "====> Sync direct messages, got "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_ce
    const/4 v2, 0x1

    move/from16 v0, p5

    if-ne v0, v2, :cond_127

    invoke-virtual {v1}, Lcom/twitter/android/provider/ae;->d()I

    move-result v5

    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v6

    move-object/from16 v0, p2

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v8, "message"

    if-lez v5, :cond_146

    const/4 v2, 0x1

    :goto_e4
    invoke-virtual {v6, v7, v8, v2}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    if-lez v4, :cond_127

    new-instance v6, Lcom/twitter/android/platform/f;

    invoke-direct {v6}, Lcom/twitter/android/platform/f;-><init>()V

    iput v4, v6, Lcom/twitter/android/platform/f;->b:I

    move-object/from16 v0, p7

    iget-boolean v2, v0, Lcom/twitter/android/platform/d;->c:Z

    if-nez v2, :cond_123

    iput v5, v6, Lcom/twitter/android/platform/f;->a:I

    const/4 v2, 0x1

    if-ne v5, v2, :cond_116

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/p;

    iget-object v3, v2, Lcom/twitter/android/api/p;->b:Ljava/lang/String;

    iput-object v3, v6, Lcom/twitter/android/platform/f;->c:Ljava/lang/String;

    iget-object v3, v2, Lcom/twitter/android/api/p;->d:Lcom/twitter/android/api/ad;

    if-eqz v3, :cond_116

    iget-object v3, v2, Lcom/twitter/android/api/p;->d:Lcom/twitter/android/api/ad;

    iget-wide v3, v3, Lcom/twitter/android/api/ad;->a:J

    iput-wide v3, v6, Lcom/twitter/android/platform/f;->e:J

    iget-object v2, v2, Lcom/twitter/android/api/p;->d:Lcom/twitter/android/api/ad;

    iget-object v2, v2, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    iput-object v2, v6, Lcom/twitter/android/platform/f;->f:Ljava/lang/String;

    :cond_116
    move-object/from16 v0, p2

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x7

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v1

    iput v1, v6, Lcom/twitter/android/platform/f;->g:I

    :cond_123
    move-object/from16 v0, p7

    iput-object v6, v0, Lcom/twitter/android/platform/d;->g:Lcom/twitter/android/platform/f;
    :try_end_127
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_62 .. :try_end_127} :catch_15a
    .catch Ljava/io/IOException; {:try_start_62 .. :try_end_127} :catch_167

    :cond_127
    :goto_127
    return-void

    :cond_128
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    move-wide v12, v2

    move v2, v6

    goto/16 :goto_2b

    :cond_12f
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v8, "direct_messages"

    aput-object v8, v3, v6

    invoke-virtual {v7, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".json"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto/16 :goto_44

    :cond_143
    const/4 v9, 0x0

    goto/16 :goto_ad

    :cond_146
    const/4 v2, 0x0

    goto :goto_e4

    :cond_148
    :try_start_148
    iget v1, v2, Lcom/twitter/android/network/c;->b:I

    const/16 v2, 0x191

    if-ne v1, v2, :cond_127

    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_159
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_148 .. :try_end_159} :catch_15a
    .catch Ljava/io/IOException; {:try_start_148 .. :try_end_159} :catch_167

    goto :goto_127

    :catch_15a
    move-exception v1

    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_127

    :catch_167
    move-exception v1

    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_127
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/android/network/d;Landroid/accounts/Account;Lcom/twitter/android/network/k;Lcom/twitter/android/api/ad;Landroid/content/SyncResult;Lcom/twitter/android/platform/d;)V
    .registers 21

    sget-boolean v1, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v1, :cond_b

    const-string v1, "TwitterDataSync"

    const-string v2, "=====> Sync about me activity"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    move-object/from16 v0, p4

    iget-wide v8, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-static {p0, v8, v9}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v10

    invoke-static {p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v2

    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/twitter/android/provider/ae;->d(I)J

    move-result-wide v4

    iget-object v1, v2, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "i"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "activity"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string v7, "about_me"

    aput-object v7, v3, v6

    invoke-static {v1, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v1, "include_entities"

    const/4 v6, 0x1

    invoke-static {v3, v1, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v1, "include_user_entities"

    const/4 v6, 0x1

    invoke-static {v3, v1, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v1, "include_cards"

    const/4 v6, 0x1

    invoke-static {v3, v1, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_57

    const-string v1, "since_id"

    invoke-static {v3, v1, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_57
    const-string v1, "count"

    const/16 v4, 0x14

    invoke-static {v3, v1, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :try_start_5e
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v4, Lcom/twitter/android/network/a;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    const/4 v5, 0x1

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v1

    iget v2, v1, Lcom/twitter/android/network/c;->b:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_220

    sget-object v1, Lcom/twitter/android/api/s;->b:Lorg/codehaus/jackson/a;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/codehaus/jackson/a;->a([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-virtual {v1}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    invoke-static {v1}, Lcom/twitter/android/api/s;->s(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v10

    move-wide v3, v8

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;JIZZ)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v11

    sget-boolean v1, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v1, :cond_b0

    const-string v1, "TwitterDataSync"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "=====> Sync about me activity, got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b0
    if-lez v11, :cond_21f

    new-instance v12, Lcom/twitter/android/platform/f;

    invoke-direct {v12}, Lcom/twitter/android/platform/f;-><init>()V

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v9, v1

    :goto_c2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1a7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/n;

    move-object/from16 v0, p6

    iget-boolean v2, v0, Lcom/twitter/android/platform/d;->c:Z

    if-nez v2, :cond_e8

    if-nez v9, :cond_e8

    const/4 v2, 0x1

    move v8, v2

    :goto_d8
    iget v2, v1, Lcom/twitter/android/api/n;->a:I

    packed-switch v2, :pswitch_data_25a

    :cond_dd
    move v1, v3

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    :goto_e2
    move v6, v4

    move v7, v5

    move v5, v3

    move v4, v2

    move v3, v1

    goto :goto_c2

    :cond_e8
    const/4 v2, 0x0

    move v8, v2

    goto :goto_d8

    :pswitch_eb
    add-int/lit8 v2, v7, 0x1

    if-eqz v8, :cond_253

    iget-object v7, v1, Lcom/twitter/android/api/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_253

    iget-object v1, v1, Lcom/twitter/android/api/n;->k:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ab;

    iget-object v7, v1, Lcom/twitter/android/api/ab;->m:Lcom/twitter/android/api/ad;

    invoke-virtual {v12, v1, v7}, Lcom/twitter/android/platform/f;->a(Lcom/twitter/android/api/ab;Lcom/twitter/android/api/ad;)V

    const/4 v1, 0x1

    move v9, v1

    move v7, v2

    goto :goto_c2

    :pswitch_109
    add-int/lit8 v2, v7, 0x1

    if-eqz v8, :cond_253

    iget-object v7, v1, Lcom/twitter/android/api/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_253

    iget-object v1, v1, Lcom/twitter/android/api/n;->o:Ljava/util/ArrayList;

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ab;

    iget-object v7, v1, Lcom/twitter/android/api/ab;->m:Lcom/twitter/android/api/ad;

    invoke-virtual {v12, v1, v7}, Lcom/twitter/android/platform/f;->a(Lcom/twitter/android/api/ab;Lcom/twitter/android/api/ad;)V

    const/4 v1, 0x1

    move v9, v1

    move v7, v2

    goto :goto_c2

    :pswitch_127
    add-int/lit8 v6, v6, 0x1

    if-eqz v8, :cond_dd

    iget-object v2, v1, Lcom/twitter/android/api/n;->k:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_dd

    iget-object v2, v1, Lcom/twitter/android/api/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_dd

    iget-object v2, v1, Lcom/twitter/android/api/n;->k:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/ab;

    iget-object v1, v1, Lcom/twitter/android/api/n;->g:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ad;

    invoke-virtual {v12, v2, v1}, Lcom/twitter/android/platform/f;->a(Lcom/twitter/android/api/ab;Lcom/twitter/android/api/ad;)V

    const/4 v1, 0x1

    move v9, v1

    goto/16 :goto_c2

    :pswitch_154
    add-int/lit8 v2, v5, 0x1

    if-eqz v8, :cond_24c

    iget-object v5, v1, Lcom/twitter/android/api/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_24c

    const/4 v5, 0x0

    iget-object v1, v1, Lcom/twitter/android/api/n;->g:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ad;

    invoke-virtual {v12, v5, v1}, Lcom/twitter/android/platform/f;->a(Lcom/twitter/android/api/ab;Lcom/twitter/android/api/ad;)V

    const/4 v1, 0x1

    move v9, v1

    move v5, v2

    goto/16 :goto_c2

    :pswitch_172
    add-int/lit8 v4, v4, 0x1

    if-eqz v8, :cond_dd

    iget-object v2, v1, Lcom/twitter/android/api/n;->o:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_dd

    iget-object v2, v1, Lcom/twitter/android/api/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_dd

    iget-object v2, v1, Lcom/twitter/android/api/n;->o:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/ab;

    iget-object v1, v1, Lcom/twitter/android/api/n;->g:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ad;

    invoke-virtual {v12, v2, v1}, Lcom/twitter/android/platform/f;->a(Lcom/twitter/android/api/ab;Lcom/twitter/android/api/ad;)V

    const/4 v1, 0x1

    move v9, v1

    goto/16 :goto_c2

    :pswitch_19f
    add-int/lit8 v1, v3, 0x1

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    goto/16 :goto_e2

    :cond_1a7
    sget-boolean v1, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v1, :cond_1dc

    const-string v1, "TwitterDataSync"

    const-string v2, "=====> Sync about me activity, %d %d %d %d %d"

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v8, v9

    const/4 v9, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v8, v9

    const/4 v9, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v8, v9

    const/4 v9, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v8, v9

    const/4 v9, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1dc
    const/4 v1, 0x0

    if-lez v7, :cond_1e0

    const/4 v1, 0x1

    :cond_1e0
    if-lez v6, :cond_1e4

    or-int/lit8 v1, v1, 0x2

    :cond_1e4
    if-lez v5, :cond_1e8

    or-int/lit8 v1, v1, 0x4

    :cond_1e8
    if-lez v4, :cond_1ec

    or-int/lit8 v1, v1, 0x8

    :cond_1ec
    if-lez v3, :cond_1f0

    or-int/lit8 v1, v1, 0x10

    :cond_1f0
    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I

    iput v11, v12, Lcom/twitter/android/platform/f;->b:I

    move-object/from16 v0, p6

    iget-boolean v2, v0, Lcom/twitter/android/platform/d;->c:Z

    if-nez v2, :cond_217

    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Lcom/twitter/android/provider/ae;->g(I)I

    move-result v2

    iput v2, v12, Lcom/twitter/android/platform/f;->a:I

    move-object/from16 v0, p2

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x6

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v10, v2, v3, v4, v5}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v2

    iput v2, v12, Lcom/twitter/android/platform/f;->g:I

    :cond_217
    move-object/from16 v0, p6

    iput v1, v0, Lcom/twitter/android/platform/d;->e:I

    move-object/from16 v0, p6

    iput-object v12, v0, Lcom/twitter/android/platform/d;->i:Lcom/twitter/android/platform/f;

    :cond_21f
    :goto_21f
    return-void

    :cond_220
    iget v1, v1, Lcom/twitter/android/network/c;->b:I

    const/16 v2, 0x191

    if-ne v1, v2, :cond_21f

    move-object/from16 v0, p5

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_231
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_5e .. :try_end_231} :catch_232
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_231} :catch_23f

    goto :goto_21f

    :catch_232
    move-exception v1

    move-object/from16 v0, p5

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_21f

    :catch_23f
    move-exception v1

    move-object/from16 v0, p5

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_21f

    :cond_24c
    move v1, v3

    move v5, v7

    move v3, v2

    move v2, v4

    move v4, v6

    goto/16 :goto_e2

    :cond_253
    move v1, v3

    move v3, v5

    move v5, v2

    move v2, v4

    move v4, v6

    goto/16 :goto_e2

    :pswitch_data_25a
    .packed-switch 0x1
        :pswitch_127
        :pswitch_109
        :pswitch_eb
        :pswitch_172
        :pswitch_154
        :pswitch_19f
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Ljava/lang/StringBuilder;)V
    .registers 6

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "location"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    invoke-static {v0}, Lcom/twitter/android/platform/h;->b(Landroid/location/LocationManager;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2d

    const-string v1, "lat"

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {p1, v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    const-string v1, "long"

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {p1, v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_2d
    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/android/network/d;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V
    .registers 30

    if-nez p2, :cond_3

    :cond_2
    :goto_2
    return-void

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/platform/c;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-static {v15}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v16

    const-string v2, "account_user_info"

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2a

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-string v2, "TwitterDataSync"

    const-string v3, "ACCOUNT_USER_INFO_KEY content not found."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2a
    :try_start_2a
    const-string v3, "com.twitter.android.oauth.token"

    const/4 v4, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.twitter.android.oauth.token.secret"

    const/4 v5, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v4, v5}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_44

    if-nez v4, :cond_64

    :cond_44
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-string v2, "TwitterDataSync"

    const-string v3, "Token not found."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_56
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2a .. :try_end_56} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2a .. :try_end_56} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_56} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_2a .. :try_end_56} :catch_441

    goto :goto_2

    :catch_57
    move-exception v2

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_2

    :cond_64
    :try_start_64
    invoke-static {v2}, Lcom/twitter/android/api/s;->c(Ljava/lang/String;)Lcom/twitter/android/api/ad;

    move-result-object v17

    new-instance v18, Lcom/twitter/android/network/k;

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4}, Lcom/twitter/android/network/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    move-object/from16 v0, p2

    invoke-static {v15, v0}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;Landroid/accounts/Account;)V

    new-instance v21, Landroid/content/Intent;

    const-string v2, "com.twitter.android.poll.data"

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v15}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;)Z

    move-result v22

    new-instance v23, Lcom/twitter/android/platform/d;

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-wide v3, v0, Lcom/twitter/android/api/ad;->a:J

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-direct {v0, v2, v3, v4, v1}, Lcom/twitter/android/platform/d;-><init>(Ljava/lang/String;JZ)V

    if-eqz p3, :cond_a3

    const-string v2, "home"

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_20c

    :cond_a3
    move-object/from16 v0, v17

    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-static {v15, v5, v6}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v2

    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v7, 0x1

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3c5

    const/4 v3, 0x0

    invoke-virtual {v2, v5, v6, v3}, Lcom/twitter/android/provider/ae;->d(JI)J

    move-result-wide v3

    const/16 v7, 0x64

    move-wide v13, v3

    move v3, v7

    :goto_bd
    sget-boolean v4, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v4, :cond_d5

    const-string v4, "TwitterDataSync"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Sync home statuses newer than sinceId: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d5
    invoke-static {v15}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v8

    iget-object v4, v8, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "1.1"

    aput-object v10, v7, v9

    const/4 v9, 0x1

    const-string v10, "statuses"

    aput-object v10, v7, v9

    const/4 v9, 0x2

    const-string v10, "home_timeline"

    aput-object v10, v7, v9

    invoke-static {v4, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ".json"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v4, "include_entities"

    const/4 v7, 0x1

    invoke-static {v9, v4, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v4, "include_user_entities"

    const/4 v7, 0x1

    invoke-static {v9, v4, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v4, "include_cards"

    const/4 v7, 0x1

    invoke-static {v9, v4, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-lez v3, :cond_112

    const-string v3, "count"

    const/16 v4, 0x64

    invoke-static {v9, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_112
    const-wide/16 v3, 0x0

    cmp-long v3, v13, v3

    if-lez v3, :cond_11d

    const-string v3, "since_id"

    invoke-static {v9, v3, v13, v14}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_11d
    const-string v3, "earned"

    const/4 v4, 0x1

    invoke-static {v9, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v3, "pc"

    const/4 v4, 0x1

    invoke-static {v9, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-static {v15, v9}, Lcom/twitter/android/platform/c;->a(Landroid/content/Context;Ljava/lang/StringBuilder;)V
    :try_end_12c
    .catch Landroid/accounts/AuthenticatorException; {:try_start_64 .. :try_end_12c} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_64 .. :try_end_12c} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_64 .. :try_end_12c} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_64 .. :try_end_12c} :catch_441

    :try_start_12c
    new-instance v12, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v12}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v10, Lcom/twitter/android/network/a;

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    const/4 v11, 0x1

    move-object/from16 v7, p1

    invoke-static/range {v7 .. v12}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    iget v4, v3, Lcom/twitter/android/network/c;->b:I

    const/16 v7, 0xc8

    if-ne v4, v7, :cond_3d2

    sget-object v3, Lcom/twitter/android/api/s;->b:Lorg/codehaus/jackson/a;

    invoke-virtual {v12}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/codehaus/jackson/a;->a([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v3

    invoke-virtual {v3}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    const/4 v4, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-static {v3, v0, v4, v7}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;Lcom/twitter/android/api/ad;ZZ)Ljava/util/ArrayList;

    move-result-object v4

    const-wide/16 v7, 0x0

    cmp-long v3, v13, v7

    if-nez v3, :cond_3cc

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3cc

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/ab;

    iget-wide v7, v3, Lcom/twitter/android/api/ab;->a:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    :goto_17c
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/provider/ae;->e(I)I

    const/4 v7, 0x0

    const-wide/16 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x1

    move-object v3, v2

    invoke-virtual/range {v3 .. v14}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I

    move-result v24

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v7, 0x64

    if-lt v3, v7, :cond_1aa

    const/4 v10, 0x0

    const-wide/16 v11, -0x1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/ab;

    iget-wide v13, v3, Lcom/twitter/android/api/ab;->a:J

    move-object v7, v2

    move-wide v8, v5

    invoke-virtual/range {v7 .. v14}, Lcom/twitter/android/provider/ae;->a(JIJJ)V

    :cond_1aa
    const/4 v3, 0x0

    invoke-virtual {v2, v5, v6, v3}, Lcom/twitter/android/provider/ae;->f(JI)I

    move-result v5

    invoke-static {v15}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v6

    move-object/from16 v0, p2

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v8, "tweet"

    if-lez v5, :cond_3cf

    const/4 v3, 0x1

    :goto_1bc
    invoke-virtual {v6, v7, v8, v3}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    sget-boolean v3, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v3, :cond_1d9

    const-string v3, "TwitterDataSync"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "====> Sync home statuses, got "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v24

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1d9
    if-lez v24, :cond_20c

    new-instance v6, Lcom/twitter/android/platform/f;

    invoke-direct {v6}, Lcom/twitter/android/platform/f;-><init>()V

    move/from16 v0, v24

    iput v0, v6, Lcom/twitter/android/platform/f;->b:I

    move-object/from16 v0, v23

    iget-boolean v3, v0, Lcom/twitter/android/platform/d;->c:Z

    if-nez v3, :cond_208

    iput v5, v6, Lcom/twitter/android/platform/f;->a:I

    const/4 v3, 0x1

    if-ne v5, v3, :cond_1fb

    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/ab;

    iget-object v4, v3, Lcom/twitter/android/api/ab;->m:Lcom/twitter/android/api/ad;

    invoke-virtual {v6, v3, v4}, Lcom/twitter/android/platform/f;->a(Lcom/twitter/android/api/ab;Lcom/twitter/android/api/ad;)V

    :cond_1fb
    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x1

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;ILjava/lang/String;Z)I

    move-result v2

    iput v2, v6, Lcom/twitter/android/platform/f;->g:I

    :cond_208
    move-object/from16 v0, v23

    iput-object v6, v0, Lcom/twitter/android/platform/d;->f:Lcom/twitter/android/platform/f;
    :try_end_20c
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_12c .. :try_end_20c} :catch_3e5
    .catch Ljava/io/IOException; {:try_start_12c .. :try_end_20c} :catch_401
    .catch Landroid/accounts/AuthenticatorException; {:try_start_12c .. :try_end_20c} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_12c .. :try_end_20c} :catch_3f3
    .catch Lorg/json/JSONException; {:try_start_12c .. :try_end_20c} :catch_441

    :cond_20c
    :goto_20c
    if-eqz p3, :cond_219

    :try_start_20e
    const-string v2, "messages"

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_23b

    :cond_219
    const/4 v7, 0x1

    move-object v2, v15

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, v18

    move-object/from16 v6, v17

    move-object/from16 v8, p4

    move-object/from16 v9, v23

    invoke-static/range {v2 .. v9}, Lcom/twitter/android/platform/c;->a(Landroid/content/Context;Lcom/twitter/android/network/d;Landroid/accounts/Account;Lcom/twitter/android/network/k;Lcom/twitter/android/api/ad;ILandroid/content/SyncResult;Lcom/twitter/android/platform/d;)V

    const/4 v7, 0x0

    move-object v2, v15

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, v18

    move-object/from16 v6, v17

    move-object/from16 v8, p4

    move-object/from16 v9, v23

    invoke-static/range {v2 .. v9}, Lcom/twitter/android/platform/c;->a(Landroid/content/Context;Lcom/twitter/android/network/d;Landroid/accounts/Account;Lcom/twitter/android/network/k;Lcom/twitter/android/api/ad;ILandroid/content/SyncResult;Lcom/twitter/android/platform/d;)V

    :cond_23b
    if-eqz p3, :cond_248

    const-string v2, "activity"

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_258

    :cond_248
    move-object v2, v15

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, v18

    move-object/from16 v6, v17

    move-object/from16 v7, p4

    move-object/from16 v8, v23

    invoke-static/range {v2 .. v8}, Lcom/twitter/android/platform/c;->a(Landroid/content/Context;Lcom/twitter/android/network/d;Landroid/accounts/Account;Lcom/twitter/android/network/k;Lcom/twitter/android/api/ad;Landroid/content/SyncResult;Lcom/twitter/android/platform/d;)V

    :cond_258
    if-eqz p3, :cond_265

    const-string v2, "discover"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_30f

    :cond_265
    invoke-static {v15}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v3

    iget-object v2, v3, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "i"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "discovery"

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".json"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-static {v15, v5, v6}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v8

    invoke-static {v15, v4}, Lcom/twitter/android/platform/c;->a(Landroid/content/Context;Ljava/lang/StringBuilder;)V

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v2, :cond_2a8

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2a8

    const-string v5, "lang"

    invoke-static {v4, v5, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2a8
    const-string v2, "include_user_entities"

    const/4 v5, 0x1

    invoke-static {v4, v2, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "schema"

    const/4 v5, 0x2

    invoke-static {v4, v2, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V
    :try_end_2b4
    .catch Landroid/accounts/AuthenticatorException; {:try_start_20e .. :try_end_2b4} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_20e .. :try_end_2b4} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_20e .. :try_end_2b4} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_20e .. :try_end_2b4} :catch_441

    :try_start_2b4
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v5, Lcom/twitter/android/network/a;

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    const/4 v6, 0x1

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v2

    iget v3, v2, Lcom/twitter/android/network/c;->b:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_420

    sget-object v2, Lcom/twitter/android/api/s;->b:Lorg/codehaus/jackson/a;

    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/codehaus/jackson/a;->a([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    invoke-static {v2}, Lcom/twitter/android/api/s;->u(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, v17

    iget-wide v3, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-virtual {v8, v2, v3, v4}, Lcom/twitter/android/provider/ae;->a(Ljava/util/List;J)Ljava/util/BitSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/BitSet;->cardinality()I

    move-result v3

    if-lez v3, :cond_30f

    invoke-static {v15}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v4

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    const-string v6, "discover"

    const/16 v2, 0xa

    if-lt v3, v2, :cond_41d

    const/4 v2, 0x1

    :goto_2ff
    invoke-virtual {v4, v5, v6, v2}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    new-instance v2, Lcom/twitter/android/platform/f;

    invoke-direct {v2}, Lcom/twitter/android/platform/f;-><init>()V

    iput v3, v2, Lcom/twitter/android/platform/f;->b:I

    iput v3, v2, Lcom/twitter/android/platform/f;->a:I

    move-object/from16 v0, v23

    iput-object v2, v0, Lcom/twitter/android/platform/d;->h:Lcom/twitter/android/platform/f;
    :try_end_30f
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_2b4 .. :try_end_30f} :catch_433
    .catch Ljava/io/IOException; {:try_start_2b4 .. :try_end_30f} :catch_44f
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2b4 .. :try_end_30f} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2b4 .. :try_end_30f} :catch_3f3
    .catch Lorg/json/JSONException; {:try_start_2b4 .. :try_end_30f} :catch_441

    :cond_30f
    :goto_30f
    :try_start_30f
    invoke-static {v15}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v4

    invoke-virtual/range {v23 .. v23}, Lcom/twitter/android/platform/d;->a()Z

    move-result v2

    if-eqz v2, :cond_34b

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v23

    iput v2, v0, Lcom/twitter/android/platform/d;->d:I

    const-string v3, "show_notif"

    if-eqz p3, :cond_336

    const-string v5, "show_notif"

    if-nez v22, :cond_45d

    const/4 v2, 0x1

    :goto_32e
    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_460

    :cond_336
    const/4 v2, 0x1

    :goto_337
    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "data"

    move-object/from16 v0, v23

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "com.twitter.android.permission.RESTRICTED"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_34b
    invoke-static {v15}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "auto_clean"

    const-wide/16 v5, 0x0

    invoke-interface {v2, v3, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-wide/32 v7, 0x36ee80

    add-long/2addr v5, v7

    cmp-long v3, v5, v19

    if-gez v3, :cond_379

    move-object/from16 v0, v17

    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-static {v15, v5, v6}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v3

    move-object/from16 v0, v17

    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-virtual {v3, v5, v6}, Lcom/twitter/android/provider/ae;->f(J)V

    const-string v3, "auto_clean"

    move-wide/from16 v0, v19

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    :cond_379
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v2, "com.twitter.android.auth.login"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    :goto_386
    if-ge v2, v6, :cond_2

    aget-object v7, v5, v2

    new-instance v3, Landroid/content/Intent;

    const-class v8, Lcom/twitter/android/client/AppService;

    invoke-direct {v3, v15, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "com.twitter.android.poll.alarm"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    iget-object v8, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v8, "account_name"

    iget-object v9, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const/4 v8, 0x0

    const/high16 v9, 0x2000

    invoke-static {v15, v8, v3, v9}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    if-eqz v3, :cond_463

    const/4 v3, 0x1

    :goto_3af
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3b9

    if-nez v3, :cond_3c2

    :cond_3b9
    iget-object v3, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/twitter/android/provider/a;->e(Ljava/lang/String;)I

    move-result v3

    invoke-static {v15, v7, v3}, Lcom/twitter/android/platform/c;->a(Landroid/content/Context;Landroid/accounts/Account;I)V
    :try_end_3c2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_30f .. :try_end_3c2} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_30f .. :try_end_3c2} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_30f .. :try_end_3c2} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_30f .. :try_end_3c2} :catch_441

    :cond_3c2
    add-int/lit8 v2, v2, 0x1

    goto :goto_386

    :cond_3c5
    const-wide/16 v3, 0x0

    const/4 v7, 0x0

    move-wide v13, v3

    move v3, v7

    goto/16 :goto_bd

    :cond_3cc
    const/4 v12, 0x0

    goto/16 :goto_17c

    :cond_3cf
    const/4 v3, 0x0

    goto/16 :goto_1bc

    :cond_3d2
    :try_start_3d2
    iget v2, v3, Lcom/twitter/android/network/c;->b:I

    const/16 v3, 0x191

    if-ne v2, v3, :cond_20c

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_3e3
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_3d2 .. :try_end_3e3} :catch_3e5
    .catch Ljava/io/IOException; {:try_start_3d2 .. :try_end_3e3} :catch_401
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3d2 .. :try_end_3e3} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3d2 .. :try_end_3e3} :catch_3f3
    .catch Lorg/json/JSONException; {:try_start_3d2 .. :try_end_3e3} :catch_441

    goto/16 :goto_20c

    :catch_3e5
    move-exception v2

    :try_start_3e6
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_3f1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3e6 .. :try_end_3f1} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3e6 .. :try_end_3f1} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_3e6 .. :try_end_3f1} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_3e6 .. :try_end_3f1} :catch_441

    goto/16 :goto_20c

    :catch_3f3
    move-exception v2

    sget-boolean v2, Lcom/twitter/android/platform/c;->a:Z

    if-eqz v2, :cond_2

    const-string v2, "TwitterDataSync"

    const-string v3, "Sync canceled."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catch_401
    move-exception v2

    :try_start_402
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_40d
    .catch Landroid/accounts/AuthenticatorException; {:try_start_402 .. :try_end_40d} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_402 .. :try_end_40d} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_402 .. :try_end_40d} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_402 .. :try_end_40d} :catch_441

    goto/16 :goto_20c

    :catch_40f
    move-exception v2

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_2

    :cond_41d
    const/4 v2, 0x0

    goto/16 :goto_2ff

    :cond_420
    :try_start_420
    iget v2, v2, Lcom/twitter/android/network/c;->b:I

    const/16 v3, 0x191

    if-ne v2, v3, :cond_30f

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_431
    .catch Lorg/codehaus/jackson/JsonParseException; {:try_start_420 .. :try_end_431} :catch_433
    .catch Ljava/io/IOException; {:try_start_420 .. :try_end_431} :catch_44f
    .catch Landroid/accounts/AuthenticatorException; {:try_start_420 .. :try_end_431} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_420 .. :try_end_431} :catch_3f3
    .catch Lorg/json/JSONException; {:try_start_420 .. :try_end_431} :catch_441

    goto/16 :goto_30f

    :catch_433
    move-exception v2

    :try_start_434
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_43f
    .catch Landroid/accounts/AuthenticatorException; {:try_start_434 .. :try_end_43f} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_434 .. :try_end_43f} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_434 .. :try_end_43f} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_434 .. :try_end_43f} :catch_441

    goto/16 :goto_30f

    :catch_441
    move-exception v2

    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    goto/16 :goto_2

    :catch_44f
    move-exception v2

    :try_start_450
    move-object/from16 v0, p4

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_45b
    .catch Landroid/accounts/AuthenticatorException; {:try_start_450 .. :try_end_45b} :catch_57
    .catch Landroid/accounts/OperationCanceledException; {:try_start_450 .. :try_end_45b} :catch_3f3
    .catch Ljava/io/IOException; {:try_start_450 .. :try_end_45b} :catch_40f
    .catch Lorg/json/JSONException; {:try_start_450 .. :try_end_45b} :catch_441

    goto/16 :goto_30f

    :cond_45d
    const/4 v2, 0x0

    goto/16 :goto_32e

    :cond_460
    const/4 v2, 0x0

    goto/16 :goto_337

    :cond_463
    const/4 v3, 0x0

    goto/16 :goto_3af
.end method

.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .registers 7

    invoke-virtual {p0}, Lcom/twitter/android/platform/c;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p5}, Lcom/twitter/android/platform/c;->a(Lcom/twitter/android/network/d;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    return-void
.end method
