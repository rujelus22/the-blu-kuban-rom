.class final Lcom/twitter/android/fu;
.super Landroid/widget/BaseAdapter;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;

.field private final b:Lcom/twitter/android/widget/az;

.field private final c:Lcom/twitter/android/provider/m;

.field private d:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/widget/az;Lcom/twitter/android/provider/m;)V
    .registers 6

    iput-object p1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/fu;->b:Lcom/twitter/android/widget/az;

    iput-object p3, p0, Lcom/twitter/android/fu;->c:Lcom/twitter/android/provider/m;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/fu;->d:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/fu;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/fu;->c:Lcom/twitter/android/provider/m;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(I)Lcom/twitter/android/provider/m;
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/fu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/provider/m;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/twitter/android/provider/m;)I
    .registers 4

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/fu;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_15

    invoke-direct {p0, v0}, Lcom/twitter/android/fu;->a(I)Lcom/twitter/android/provider/m;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/android/provider/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    :goto_11
    return v0

    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_15
    const/4 v0, -0x1

    goto :goto_11
.end method

.method public final a(Ljava/util/ArrayList;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/fu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/fu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/twitter/android/fu;->notifyDataSetChanged()V

    return-void
.end method

.method public final getCount()I
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/fu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    invoke-direct {p0, p1}, Lcom/twitter/android/fu;->a(I)Lcom/twitter/android/provider/m;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    invoke-direct {p0, p1}, Lcom/twitter/android/fu;->a(I)Lcom/twitter/android/provider/m;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/android/provider/m;->o:J

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/fu;->c:Lcom/twitter/android/provider/m;

    invoke-direct {p0, p1}, Lcom/twitter/android/fu;->a(I)Lcom/twitter/android/provider/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/m;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x1

    goto :goto_d
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9

    invoke-direct {p0, p1}, Lcom/twitter/android/fu;->a(I)Lcom/twitter/android/provider/m;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/fu;->c:Lcom/twitter/android/provider/m;

    invoke-virtual {v1, v0}, Lcom/twitter/android/provider/m;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->o:Lcom/twitter/android/api/TweetMedia;

    if-nez p2, :cond_79

    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object p2, v0, Lcom/twitter/android/TweetFragment;->r:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/provider/m;)Lcom/twitter/android/api/TweetEntities;

    move-result-object v4

    invoke-virtual {p2, v1, v2, v0, v4}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/provider/m;Lcom/twitter/android/client/b;Lcom/twitter/android/widget/an;Lcom/twitter/android/api/TweetEntities;)V

    if-eqz v3, :cond_82

    iget-object v0, p2, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/view/View;

    if-eqz v0, :cond_30

    iget-object v0, p2, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_30
    iget v0, v3, Lcom/twitter/android/api/TweetMedia;->type:I

    packed-switch v0, :pswitch_data_c2

    :goto_35
    move-object v1, p2

    :goto_36
    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    invoke-virtual {p0}, Lcom/twitter/android/fu;->getCount()I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    return-object v1

    :pswitch_41
    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->T:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    move-object v1, p2

    goto :goto_36

    :pswitch_54
    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->U:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    move-object v1, p2

    goto :goto_36

    :pswitch_67
    iget-object v0, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/client/Session;

    invoke-virtual {v1}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->S:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_35

    :cond_79
    move-object v0, p2

    check-cast v0, Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/client/b;)V

    invoke-virtual {v0, v3, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/api/TweetMedia;Lcom/twitter/android/client/b;)V

    :cond_82
    move-object v1, p2

    goto :goto_36

    :cond_84
    if-nez p2, :cond_bb

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030013

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f070037

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/fu;->b:Lcom/twitter/android/widget/az;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/widget/az;)V

    iget-object v2, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/widget/av;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_ac
    iget-object v2, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/android/client/b;

    iget v2, v2, Lcom/twitter/android/client/b;->f:F

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(F)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/provider/m;)V

    move-object v1, p2

    goto/16 :goto_36

    :cond_bb
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetView;

    goto :goto_ac

    :pswitch_data_c2
    .packed-switch 0x1
        :pswitch_41
        :pswitch_54
        :pswitch_67
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .registers 2

    const/4 v0, 0x2

    return v0
.end method
