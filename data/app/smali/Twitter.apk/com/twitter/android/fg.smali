.class Lcom/twitter/android/fg;
.super Ljava/lang/Object;


# instance fields
.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/ImageView;

.field public final e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f070024

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/fg;->b:Landroid/widget/TextView;

    const v0, 0x7f070043

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/fg;->c:Landroid/widget/TextView;

    const v0, 0x7f07006c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/fg;->d:Landroid/widget/ImageView;

    const v0, 0x7f07007c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/fg;->e:Landroid/widget/TextView;

    return-void
.end method

.method public static a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/fe;)Landroid/view/View;
    .registers 8

    const/16 v4, 0x8

    const/4 v3, 0x0

    if-nez p0, :cond_46

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03004a

    invoke-virtual {v0, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    new-instance v0, Lcom/twitter/android/fg;

    invoke-direct {v0, p0}, Lcom/twitter/android/fg;-><init>(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1c
    iget-object v1, v0, Lcom/twitter/android/fg;->b:Landroid/widget/TextView;

    iget-object v2, p2, Lcom/twitter/android/fe;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p2, Lcom/twitter/android/fe;->b:I

    if-eqz v1, :cond_4d

    iget-object v1, v0, Lcom/twitter/android/fg;->d:Landroid/widget/ImageView;

    iget v2, p2, Lcom/twitter/android/fe;->b:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, v0, Lcom/twitter/android/fg;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_33
    iget v1, p2, Lcom/twitter/android/fe;->d:I

    if-lez v1, :cond_53

    iget-object v2, v0, Lcom/twitter/android/fg;->e:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/twitter/android/fg;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_45
    return-object p0

    :cond_46
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/fg;

    goto :goto_1c

    :cond_4d
    iget-object v1, v0, Lcom/twitter/android/fg;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_33

    :cond_53
    iget-object v0, v0, Lcom/twitter/android/fg;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_45
.end method
