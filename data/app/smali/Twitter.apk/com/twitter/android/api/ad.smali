.class public final Lcom/twitter/android/api/ad;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/twitter/android/api/g;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Z

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:I

.field public final m:J

.field public final n:I

.field public final o:Lcom/twitter/android/api/ab;

.field public final p:Z

.field public final q:J

.field public final r:I

.field public final s:Lcom/twitter/android/api/PromotedContent;

.field public final t:J

.field public final u:Lcom/twitter/android/api/TweetEntities;

.field public final v:Lcom/twitter/android/api/TweetEntities;

.field public w:Ljava/lang/String;

.field public x:Z

.field public y:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/twitter/android/api/ae;

    invoke-direct {v0}, Lcom/twitter/android/api/ae;-><init>()V

    sput-object v0, Lcom/twitter/android/api/ad;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IZZLjava/lang/String;IJIZIJLcom/twitter/android/api/ab;JILcom/twitter/android/api/PromotedContent;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/api/TweetEntities;)V
    .registers 32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/android/api/ad;->a:J

    iput-object p3, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    if-eqz p7, :cond_72

    const-string v2, "null"

    invoke-virtual {p7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_72

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    :goto_1a
    iput p8, p0, Lcom/twitter/android/api/ad;->k:I

    if-eqz p9, :cond_75

    const-string v2, "null"

    invoke-virtual {p9, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_75

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    :goto_29
    iput p10, p0, Lcom/twitter/android/api/ad;->f:I

    iput-boolean p11, p0, Lcom/twitter/android/api/ad;->h:Z

    iput-boolean p12, p0, Lcom/twitter/android/api/ad;->i:Z

    if-eqz p13, :cond_78

    const-string v2, "null"

    move-object/from16 v0, p13

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_78

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    :goto_3e
    move/from16 v0, p14

    iput v0, p0, Lcom/twitter/android/api/ad;->l:I

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/twitter/android/api/ad;->m:J

    move/from16 v0, p17

    iput v0, p0, Lcom/twitter/android/api/ad;->n:I

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/twitter/android/api/ad;->p:Z

    move/from16 v0, p19

    iput v0, p0, Lcom/twitter/android/api/ad;->y:I

    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/twitter/android/api/ad;->q:J

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/android/api/ad;->x:Z

    move-wide/from16 v0, p23

    iput-wide v0, p0, Lcom/twitter/android/api/ad;->t:J

    move/from16 v0, p25

    iput v0, p0, Lcom/twitter/android/api/ad;->r:I

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    return-void

    :cond_72
    iput-object p7, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    goto :goto_1a

    :cond_75
    iput-object p9, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    goto :goto_29

    :cond_78
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    goto :goto_3e
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/api/ad;->a:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/api/ad;->k:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/api/ad;->f:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a3

    move v0, v1

    :goto_42
    iput-boolean v0, p0, Lcom/twitter/android/api/ad;->h:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a5

    move v0, v1

    :goto_4b
    iput-boolean v0, p0, Lcom/twitter/android/api/ad;->i:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/api/ad;->l:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/api/ad;->m:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/api/ad;->n:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a7

    :goto_6b
    iput-boolean v1, p0, Lcom/twitter/android/api/ad;->p:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/api/ad;->y:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/api/ad;->q:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/api/ad;->t:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/api/ad;->r:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    iput-boolean v2, p0, Lcom/twitter/android/api/ad;->x:Z

    return-void

    :cond_a3
    move v0, v2

    goto :goto_42

    :cond_a5
    move v0, v2

    goto :goto_4b

    :cond_a7
    move v1, v2

    goto :goto_6b
.end method


# virtual methods
.method public final a()J
    .registers 3

    iget-wide v0, p0, Lcom/twitter/android/api/ad;->a:J

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    goto :goto_6
.end method

.method public final c()Z
    .registers 5

    iget-wide v0, p0, Lcom/twitter/android/api/ad;->m:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public final describeContents()I
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_5

    :cond_4
    :goto_4
    return v0

    :cond_5
    if-eqz p1, :cond_11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_13

    :cond_11
    move v0, v1

    goto :goto_4

    :cond_13
    check-cast p1, Lcom/twitter/android/api/ad;

    iget-wide v2, p0, Lcom/twitter/android/api/ad;->m:J

    iget-wide v4, p1, Lcom/twitter/android/api/ad;->m:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1f

    move v0, v1

    goto :goto_4

    :cond_1f
    iget v2, p0, Lcom/twitter/android/api/ad;->r:I

    iget v3, p1, Lcom/twitter/android/api/ad;->r:I

    if-eq v2, v3, :cond_27

    move v0, v1

    goto :goto_4

    :cond_27
    iget v2, p0, Lcom/twitter/android/api/ad;->k:I

    iget v3, p1, Lcom/twitter/android/api/ad;->k:I

    if-eq v2, v3, :cond_2f

    move v0, v1

    goto :goto_4

    :cond_2f
    iget v2, p0, Lcom/twitter/android/api/ad;->l:I

    iget v3, p1, Lcom/twitter/android/api/ad;->l:I

    if-eq v2, v3, :cond_37

    move v0, v1

    goto :goto_4

    :cond_37
    iget v2, p0, Lcom/twitter/android/api/ad;->y:I

    iget v3, p1, Lcom/twitter/android/api/ad;->y:I

    if-eq v2, v3, :cond_3f

    move v0, v1

    goto :goto_4

    :cond_3f
    iget-wide v2, p0, Lcom/twitter/android/api/ad;->q:J

    iget-wide v4, p1, Lcom/twitter/android/api/ad;->q:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_49

    move v0, v1

    goto :goto_4

    :cond_49
    iget-boolean v2, p0, Lcom/twitter/android/api/ad;->p:Z

    iget-boolean v3, p1, Lcom/twitter/android/api/ad;->p:Z

    if-eq v2, v3, :cond_51

    move v0, v1

    goto :goto_4

    :cond_51
    iget-boolean v2, p0, Lcom/twitter/android/api/ad;->h:Z

    iget-boolean v3, p1, Lcom/twitter/android/api/ad;->h:Z

    if-eq v2, v3, :cond_59

    move v0, v1

    goto :goto_4

    :cond_59
    iget-boolean v2, p0, Lcom/twitter/android/api/ad;->x:Z

    iget-boolean v3, p1, Lcom/twitter/android/api/ad;->x:Z

    if-eq v2, v3, :cond_61

    move v0, v1

    goto :goto_4

    :cond_61
    iget v2, p0, Lcom/twitter/android/api/ad;->n:I

    iget v3, p1, Lcom/twitter/android/api/ad;->n:I

    if-eq v2, v3, :cond_69

    move v0, v1

    goto :goto_4

    :cond_69
    iget-wide v2, p0, Lcom/twitter/android/api/ad;->a:J

    iget-wide v4, p1, Lcom/twitter/android/api/ad;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_73

    move v0, v1

    goto :goto_4

    :cond_73
    iget-boolean v2, p0, Lcom/twitter/android/api/ad;->i:Z

    iget-boolean v3, p1, Lcom/twitter/android/api/ad;->i:Z

    if-eq v2, v3, :cond_7b

    move v0, v1

    goto :goto_4

    :cond_7b
    iget-object v2, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    if-eqz v2, :cond_8c

    iget-object v2, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_90

    :cond_89
    move v0, v1

    goto/16 :goto_4

    :cond_8c
    iget-object v2, p1, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    if-nez v2, :cond_89

    :cond_90
    iget-object v2, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    if-eqz v2, :cond_a1

    iget-object v2, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a5

    :cond_9e
    move v0, v1

    goto/16 :goto_4

    :cond_a1
    iget-object v2, p1, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    if-nez v2, :cond_9e

    :cond_a5
    iget-object v2, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    if-eqz v2, :cond_b6

    iget-object v2, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_ba

    :cond_b3
    move v0, v1

    goto/16 :goto_4

    :cond_b6
    iget-object v2, p1, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    if-nez v2, :cond_b3

    :cond_ba
    iget-object v2, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    if-eqz v2, :cond_cb

    iget-object v2, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_cf

    :cond_c8
    move v0, v1

    goto/16 :goto_4

    :cond_cb
    iget-object v2, p1, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    if-nez v2, :cond_c8

    :cond_cf
    iget-object v2, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    if-eqz v2, :cond_e0

    iget-object v2, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e4

    :cond_dd
    move v0, v1

    goto/16 :goto_4

    :cond_e0
    iget-object v2, p1, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    if-nez v2, :cond_dd

    :cond_e4
    iget-object v2, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    if-eqz v2, :cond_f5

    iget-object v2, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f9

    :cond_f2
    move v0, v1

    goto/16 :goto_4

    :cond_f5
    iget-object v2, p1, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    if-nez v2, :cond_f2

    :cond_f9
    iget v2, p0, Lcom/twitter/android/api/ad;->f:I

    iget v3, p1, Lcom/twitter/android/api/ad;->f:I

    if-eq v2, v3, :cond_102

    move v0, v1

    goto/16 :goto_4

    :cond_102
    iget-object v2, p0, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    if-eqz v2, :cond_113

    iget-object v2, p0, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_117

    :cond_110
    move v0, v1

    goto/16 :goto_4

    :cond_113
    iget-object v2, p1, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    if-nez v2, :cond_110

    :cond_117
    iget-object v2, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    if-eqz v2, :cond_128

    iget-object v2, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12c

    :cond_125
    move v0, v1

    goto/16 :goto_4

    :cond_128
    iget-object v2, p1, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    if-nez v2, :cond_125

    :cond_12c
    iget-object v2, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    if-eqz v2, :cond_13d

    iget-object v2, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v2, v3}, Lcom/twitter/android/api/PromotedContent;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_141

    :cond_13a
    move v0, v1

    goto/16 :goto_4

    :cond_13d
    iget-object v2, p1, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    if-nez v2, :cond_13a

    :cond_141
    iget-object v2, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    if-eqz v2, :cond_152

    iget-object v2, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {v2, v3}, Lcom/twitter/android/api/TweetEntities;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_156

    :cond_14f
    move v0, v1

    goto/16 :goto_4

    :cond_152
    iget-object v2, p1, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    if-nez v2, :cond_14f

    :cond_156
    iget-object v2, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    if-eqz v2, :cond_167

    iget-object v2, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {v2, v3}, Lcom/twitter/android/api/TweetEntities;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :goto_164
    move v0, v1

    goto/16 :goto_4

    :cond_167
    iget-object v2, p1, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    if-eqz v2, :cond_4

    goto :goto_164
.end method

.method public final hashCode()I
    .registers 9

    const/16 v7, 0x20

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-wide v3, p0, Lcom/twitter/android/api/ad;->a:J

    iget-wide v5, p0, Lcom/twitter/android/api/ad;->a:J

    ushr-long/2addr v5, v7

    xor-long/2addr v3, v5

    long-to-int v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    if-eqz v0, :cond_e2

    iget-object v0, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_17
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    if-eqz v0, :cond_e5

    iget-object v0, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_24
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    if-eqz v0, :cond_e8

    iget-object v0, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_31
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    if-eqz v0, :cond_eb

    iget-object v0, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3e
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    if-eqz v0, :cond_ee

    iget-object v0, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4b
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/android/api/ad;->f:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    if-eqz v0, :cond_f1

    iget-object v0, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5d
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_f4

    move v0, v2

    :goto_65
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/android/api/ad;->i:Z

    if-eqz v0, :cond_f7

    move v0, v2

    :goto_6d
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    if-eqz v0, :cond_fa

    iget-object v0, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7a
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/android/api/ad;->k:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/android/api/ad;->l:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v3, p0, Lcom/twitter/android/api/ad;->m:J

    iget-wide v5, p0, Lcom/twitter/android/api/ad;->m:J

    ushr-long/2addr v5, v7

    xor-long/2addr v3, v5

    long-to-int v3, v3

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/android/api/ad;->n:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    if-eqz v0, :cond_fd

    iget-object v0, p0, Lcom/twitter/android/api/ad;->o:Lcom/twitter/android/api/ab;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_a0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/android/api/ad;->p:Z

    if-eqz v0, :cond_ff

    move v0, v2

    :goto_a8
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/android/api/ad;->y:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/twitter/android/api/ad;->r:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/android/api/ad;->x:Z

    if-eqz v3, :cond_101

    :goto_b9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    if-eqz v0, :cond_103

    iget-object v0, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0}, Lcom/twitter/android/api/PromotedContent;->hashCode()I

    move-result v0

    :goto_c6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    if-eqz v0, :cond_105

    iget-object v0, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {v0}, Lcom/twitter/android/api/TweetEntities;->hashCode()I

    move-result v0

    :goto_d3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    if-eqz v2, :cond_e0

    iget-object v1, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {v1}, Lcom/twitter/android/api/TweetEntities;->hashCode()I

    move-result v1

    :cond_e0
    add-int/2addr v0, v1

    return v0

    :cond_e2
    move v0, v1

    goto/16 :goto_17

    :cond_e5
    move v0, v1

    goto/16 :goto_24

    :cond_e8
    move v0, v1

    goto/16 :goto_31

    :cond_eb
    move v0, v1

    goto/16 :goto_3e

    :cond_ee
    move v0, v1

    goto/16 :goto_4b

    :cond_f1
    move v0, v1

    goto/16 :goto_5d

    :cond_f4
    move v0, v1

    goto/16 :goto_65

    :cond_f7
    move v0, v1

    goto/16 :goto_6d

    :cond_fa
    move v0, v1

    goto/16 :goto_7a

    :cond_fd
    move v0, v1

    goto :goto_a0

    :cond_ff
    move v0, v1

    goto :goto_a8

    :cond_101
    move v2, v1

    goto :goto_b9

    :cond_103
    move v0, v1

    goto :goto_c6

    :cond_105
    move v0, v1

    goto :goto_d3
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .registers 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p0, Lcom/twitter/android/api/ad;->a:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/android/api/ad;->k:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/android/api/ad;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_7e

    move v0, v1

    :goto_34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/android/api/ad;->i:Z

    if-eqz v0, :cond_80

    move v0, v1

    :goto_3c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/android/api/ad;->l:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/android/api/ad;->m:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/android/api/ad;->n:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/android/api/ad;->p:Z

    if-eqz v0, :cond_82

    :goto_57
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/android/api/ad;->y:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v0, p0, Lcom/twitter/android/api/ad;->q:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/android/api/ad;->t:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/android/api/ad;->r:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->s:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void

    :cond_7e
    move v0, v2

    goto :goto_34

    :cond_80
    move v0, v2

    goto :goto_3c

    :cond_82
    move v1, v2

    goto :goto_57
.end method
