.class final Lcom/twitter/android/v;
.super Landroid/os/AsyncTask;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/graphics/Rect;

.field final c:Landroid/content/Intent;

.field final synthetic d:Lcom/twitter/android/CropActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/CropActivity;)V
    .registers 7

    iput-object p1, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/v;->a:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/v;->c:Landroid/content/Intent;

    invoke-static {p1}, Lcom/twitter/android/CropActivity;->b(Lcom/twitter/android/CropActivity;)Lcom/twitter/android/widget/CroppableImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/CroppableImageView;->a()Landroid/graphics/RectF;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/twitter/android/v;->b:Landroid/graphics/Rect;

    return-void
.end method

.method private varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 12

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v1, p0, Lcom/twitter/android/v;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/client/b;->a(Landroid/content/Context;)Lcom/twitter/android/client/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/v;->b:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    invoke-static {v0}, Lcom/twitter/android/CropActivity;->b(Lcom/twitter/android/CropActivity;)Lcom/twitter/android/widget/CroppableImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/CroppableImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    if-le v7, v9, :cond_37

    if-gt v6, v9, :cond_3a

    :cond_37
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_39
    return-object v0

    :cond_3a
    invoke-virtual {v5, v4}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_85

    :try_start_40
    iget v5, v4, Landroid/graphics/Rect;->left:I

    iget v6, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v7

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v8

    invoke-static {v0, v5, v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/graphics/Bitmap;J)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_85

    iget-object v1, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    iget v1, v1, Lcom/twitter/android/CropActivity;->a:I

    iget v2, v4, Landroid/graphics/Rect;->left:I

    mul-int/2addr v2, v1

    iget v3, v4, Landroid/graphics/Rect;->top:I

    mul-int/2addr v3, v1

    iget v5, v4, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    iget v6, v6, Lcom/twitter/android/CropActivity;->b:I

    sub-int/2addr v5, v6

    mul-int/2addr v5, v1

    iget v6, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    iget v7, v7, Lcom/twitter/android/CropActivity;->c:I

    sub-int/2addr v6, v7

    mul-int/2addr v1, v6

    invoke-virtual {v4, v2, v3, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/twitter/android/v;->c:Landroid/content/Intent;

    const-string v2, "cropped_rect"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/v;->c:Landroid/content/Intent;

    const-string v2, "uri"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_83
    .catch Ljava/lang/OutOfMemoryError; {:try_start_40 .. :try_end_83} :catch_84

    goto :goto_39

    :catch_84
    move-exception v0

    :cond_85
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_39
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    check-cast p1, [Ljava/lang/Void;

    invoke-direct {p0, p1}, Lcom/twitter/android/v;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 6

    const/4 v3, 0x0

    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/CropActivity;->removeDialog(I)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/twitter/android/v;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/CropActivity;->setResult(ILandroid/content/Intent;)V

    :goto_16
    iget-object v0, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    invoke-virtual {v0}, Lcom/twitter/android/CropActivity;->finish()V

    return-void

    :cond_1c
    iget-object v0, p0, Lcom/twitter/android/v;->a:Landroid/content/Context;

    const v1, 0x7f0b004d

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/CropActivity;->setResult(I)V

    goto :goto_16
.end method

.method protected final onPreExecute()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/v;->d:Lcom/twitter/android/CropActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/CropActivity;->showDialog(I)V

    return-void
.end method
