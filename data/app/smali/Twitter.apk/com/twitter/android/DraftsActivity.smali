.class public Lcom/twitter/android/DraftsActivity;
.super Lcom/twitter/android/BaseFragmentActivity;

# interfaces
.implements Lcom/twitter/android/widget/x;


# instance fields
.field e:Lcom/twitter/android/client/Session;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .registers 5

    packed-switch p1, :pswitch_data_18

    :cond_3
    :goto_3
    return-void

    :pswitch_4
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/DraftsActivity;->a:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/DraftsActivity;->e:Lcom/twitter/android/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/DraftsActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/DraftsActivity;->finish()V

    goto :goto_3

    nop

    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method public final a_(I)V
    .registers 4

    const v0, 0x7f070054

    if-ne p1, v0, :cond_2e

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b015e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b015f

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ed

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b00ee

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DraftsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :goto_2d
    return-void

    :cond_2e
    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->a_(I)V

    goto :goto_2d
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6

    const/4 v3, 0x0

    const v0, 0x7f030019

    const/4 v1, 0x1

    invoke-super {p0, p1, v0, v1}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    const v0, 0x7f0b0111

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/DraftsActivity;->a(I[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/twitter/android/DraftsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez p1, :cond_3a

    new-instance v2, Lcom/twitter/android/DraftsFragment;

    invoke-direct {v2}, Lcom/twitter/android/DraftsFragment;-><init>()V

    invoke-static {v0, v3}, Lcom/twitter/android/DraftsFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/DraftsFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DraftsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v3, 0x7f07004a

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_3a
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_49

    iget-object v0, p0, Lcom/twitter/android/DraftsActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DraftsActivity;->e:Lcom/twitter/android/client/Session;

    :goto_48
    return-void

    :cond_49
    iget-object v0, p0, Lcom/twitter/android/DraftsActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->b(Ljava/lang/String;)Lcom/twitter/android/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DraftsActivity;->e:Lcom/twitter/android/client/Session;

    goto :goto_48
.end method
