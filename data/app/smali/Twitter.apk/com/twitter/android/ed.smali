.class public final Lcom/twitter/android/ed;
.super Ljava/lang/Object;


# static fields
.field public static final action_bar:I = 0x7f0a000c

.field public static final action_bar_background:I = 0x7f0a000e

.field public static final action_bar_pressed:I = 0x7f0a000d

.field public static final at_sign:I = 0x7f0a0011

.field public static final bg_focused:I = 0x7f0a001a

.field public static final bg_image:I = 0x7f0a001c

.field public static final bg_list:I = 0x7f0a0014

.field public static final bg_player:I = 0x7f0a001d

.field public static final bg_pressed:I = 0x7f0a0019

.field public static final btn_titlebar_text:I = 0x7f0a0023

.field public static final button_text:I = 0x7f0a0024

.field public static final button_text_disabled:I = 0x7f0a000f

.field public static final card_stroke:I = 0x7f0a001b

.field public static final clear:I = 0x7f0a0004

.field public static final confirm_btn_text:I = 0x7f0a0025

.field public static final confirm_btn_text_shadow:I = 0x7f0a0026

.field public static final divider_dark:I = 0x7f0a0010

.field public static final dot:I = 0x7f0a0018

.field public static final event_divider:I = 0x7f0a0020

.field public static final event_divider_reflection:I = 0x7f0a0021

.field public static final event_header_background:I = 0x7f0a0022

.field public static final follow_btn_text:I = 0x7f0a0027

.field public static final follow_btn_text_shadow:I = 0x7f0a0028

.field public static final image_overlay:I = 0x7f0a0013

.field public static final link_color:I = 0x7f0a0009

.field public static final list_divider_color:I = 0x7f0a0017

.field public static final list_item_secondary_text:I = 0x7f0a0029

.field public static final list_item_text:I = 0x7f0a002a

.field public static final list_item_timestamp_text:I = 0x7f0a002b

.field public static final mentioned:I = 0x7f0a000b

.field public static final negative_color:I = 0x7f0a001f

.field public static final notification:I = 0x7f0a0012

.field public static final positive_color:I = 0x7f0a001e

.field public static final primary_text:I = 0x7f0a0005

.field public static final secondary_text:I = 0x7f0a0006

.field public static final section_shadow_color:I = 0x7f0a000a

.field public static final signup_error:I = 0x7f0a0016

.field public static final soft_black:I = 0x7f0a0003

.field public static final solid_black:I = 0x7f0a0001

.field public static final solid_gray:I = 0x7f0a0002

.field public static final solid_white:I = 0x7f0a0000

.field public static final tab_item_title:I = 0x7f0a002c

.field public static final timestamp_text:I = 0x7f0a0007

.field public static final title_shadow_color:I = 0x7f0a0008

.field public static final translucent_black:I = 0x7f0a0015
