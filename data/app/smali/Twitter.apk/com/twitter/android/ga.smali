.class public Lcom/twitter/android/ga;
.super Lcom/twitter/android/client/j;


# instance fields
.field protected final b:Landroid/content/Context;

.field final synthetic c:Lcom/twitter/android/TweetListFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TweetListFragment;Landroid/content/Context;)V
    .registers 3

    iput-object p1, p0, Lcom/twitter/android/ga;->c:Lcom/twitter/android/TweetListFragment;

    invoke-direct {p0}, Lcom/twitter/android/client/j;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/ga;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .registers 8

    iget-object v0, p0, Lcom/twitter/android/ga;->c:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/TweetListFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_19

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/ga;->b:Landroid/content/Context;

    const v1, 0x7f0b005d

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_19
    return-void
.end method

.method public final a(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;IJ)V
    .registers 11

    iget-object v0, p0, Lcom/twitter/android/ga;->c:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/TweetListFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_1d

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_1d

    const/16 v0, 0x8b

    if-eq p5, v0, :cond_1d

    iget-object v0, p0, Lcom/twitter/android/ga;->b:Landroid/content/Context;

    const v1, 0x7f0b0057

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1d
    return-void
.end method

.method public final b(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/ga;->c:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/TweetListFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_19

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/ga;->b:Landroid/content/Context;

    const v1, 0x7f0b0058

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_19
    return-void
.end method

.method public final c(Lcom/twitter/android/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/ga;->c:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/TweetListFragment;->c(Ljava/lang/String;)Lcom/twitter/android/co;

    move-result-object v0

    if-eqz v0, :cond_19

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_19

    iget-object v0, p0, Lcom/twitter/android/ga;->b:Landroid/content/Context;

    const v1, 0x7f0b005b

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_19
    return-void
.end method
