.class public Lcom/twitter/android/widget/TweetView;
.super Landroid/view/View;


# static fields
.field private static final a:[I

.field private static final b:[I

.field private static final k:Landroid/text/TextPaint;


# instance fields
.field private final A:I

.field private final B:I

.field private final C:I

.field private final D:I

.field private final E:I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:I

.field private final J:I

.field private final K:I

.field private final L:I

.field private final M:I

.field private final N:I

.field private final O:I

.field private final P:I

.field private Q:Landroid/graphics/drawable/Drawable;

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:Landroid/graphics/drawable/Drawable;

.field private T:Landroid/graphics/drawable/Drawable;

.field private U:Landroid/graphics/drawable/Drawable;

.field private V:[Landroid/graphics/drawable/Drawable;

.field private W:Ljava/lang/String;

.field private Z:Z

.field private aa:J

.field private ab:Landroid/graphics/Rect;

.field private ac:Lcom/twitter/android/widget/av;

.field private ad:Lcom/twitter/android/widget/ay;

.field private ae:Lcom/twitter/android/widget/ax;

.field private af:Lcom/twitter/android/widget/aw;

.field private ag:I

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Lcom/twitter/android/provider/m;

.field private e:Lcom/twitter/android/widget/az;

.field private f:Landroid/text/StaticLayout;

.field private g:Landroid/text/StaticLayout;

.field private h:Landroid/text/StaticLayout;

.field private i:Landroid/text/StaticLayout;

.field private j:Landroid/text/StaticLayout;

.field private final l:Landroid/content/res/ColorStateList;

.field private m:F

.field private n:I

.field private o:Ljava/lang/String;

.field private p:F

.field private final q:Landroid/content/res/ColorStateList;

.field private r:I

.field private final s:Landroid/content/res/ColorStateList;

.field private t:I

.field private u:Ljava/lang/String;

.field private final v:F

.field private final w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [I

    sget v1, Lcom/twitter/android/eb;->state_mentioned:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/android/widget/TweetView;->a:[I

    new-array v0, v2, [I

    sget v1, Lcom/twitter/android/eb;->state_avatar_pressed:I

    aput v1, v0, v3

    sput-object v0, Lcom/twitter/android/widget/TweetView;->b:[I

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0, v2}, Landroid/text/TextPaint;-><init>(I)V

    sput-object v0, Lcom/twitter/android/widget/TweetView;->k:Landroid/text/TextPaint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    sget v0, Lcom/twitter/android/eb;->tweetViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/TweetView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10

    const/16 v5, 0x30

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/twitter/android/ek;->TweetView:[I

    invoke-virtual {p1, p2, v0, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/TweetView;->l:Landroid/content/res/ColorStateList;

    const/high16 v1, 0x4160

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->m:F

    const/4 v1, 0x2

    const/high16 v2, 0x3f80

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->v:F

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->w:I

    const/4 v1, 0x6

    const/high16 v2, 0x4140

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->p:F

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/TweetView;->q:Landroid/content/res/ColorStateList;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/TweetView;->s:Landroid/content/res/ColorStateList;

    const/16 v1, 0x15

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->K:I

    const/16 v1, 0x16

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->L:I

    const/16 v1, 0x17

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->M:I

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->x:I

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_79

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-object v1, p0, Lcom/twitter/android/widget/TweetView;->c:Landroid/graphics/drawable/Drawable;

    :cond_79
    const/16 v1, 0x19

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->N:I

    const/16 v1, 0x1a

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->O:I

    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_9d

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->N:I

    iget v3, p0, Lcom/twitter/android/widget/TweetView;->O:I

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iput-object v1, p0, Lcom/twitter/android/widget/TweetView;->U:Landroid/graphics/drawable/Drawable;

    :cond_9d
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->y:I

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->z:I

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->A:I

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->B:I

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->C:I

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->D:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->E:I

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->F:I

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->G:I

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->H:I

    const/16 v1, 0x13

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->I:I

    const/16 v1, 0x14

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->J:I

    const/16 v1, 0x1b

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->P:I

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->d()V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private static a(Ljava/lang/String;Landroid/text/TextPaint;)I
    .registers 3

    invoke-static {p0, p1}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F

    move-result v0

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private static a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    .registers 6

    const/4 v3, 0x0

    if-lez p1, :cond_13

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :goto_12
    return-object v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method private a(Lcom/twitter/android/provider/m;Landroid/content/res/Resources;)V
    .registers 7

    sget v0, Lcom/twitter/android/ej;->tweets_retweeted_by:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->y:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private d()V
    .registers 4

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getDrawableState()[I

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->l:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->l:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->n:I

    :cond_11
    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->q:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->q:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->r:I

    :cond_1d
    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->s:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_29

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->s:Landroid/content/res/ColorStateList;

    invoke-virtual {v1, v0, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->t:I

    :cond_29
    return-void
.end method

.method private e()Z
    .registers 2

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method private f()V
    .registers 2

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ad:Lcom/twitter/android/widget/ay;

    if-eqz v0, :cond_13

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ad:Lcom/twitter/android/widget/ay;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_13
    return-void
.end method

.method private g()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ae:Lcom/twitter/android/widget/ax;

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ae:Lcom/twitter/android/widget/ax;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_f
    return-void
.end method


# virtual methods
.method public final a()Lcom/twitter/android/provider/m;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    return-object v0
.end method

.method public final a(F)V
    .registers 4

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v1, p1

    iput v1, p0, Lcom/twitter/android/widget/TweetView;->m:F

    const/high16 v1, 0x4000

    sub-float v1, p1, v1

    iget v0, v0, Landroid/util/DisplayMetrics;->scaledDensity:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->p:F

    return-void
.end method

.method public final a(Lcom/twitter/android/provider/m;)V
    .registers 14

    const-wide/16 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->e:Lcom/twitter/android/widget/az;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call setProvider before calling setTweet"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    invoke-virtual {p1, v1}, Lcom/twitter/android/provider/m;->a(Lcom/twitter/android/provider/m;)Z

    move-result v1

    if-nez v1, :cond_ae

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->f:Landroid/text/StaticLayout;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->i:Landroid/text/StaticLayout;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->j:Landroid/text/StaticLayout;

    iput-object p1, p0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->Q:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v2, v2, -0x4

    iput v2, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/widget/TweetView;->aa:J

    invoke-virtual {p1, v2, v3}, Lcom/twitter/android/provider/m;->a(J)Z

    move-result v4

    iget-wide v5, p1, Lcom/twitter/android/provider/m;->h:J

    cmp-long v5, v5, v10

    if-gtz v5, :cond_ba

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->u:Ljava/lang/String;

    :goto_47
    if-eqz v4, :cond_c3

    iget-boolean v5, p1, Lcom/twitter/android/provider/m;->l:Z

    if-eqz v5, :cond_c3

    iget v5, p0, Lcom/twitter/android/widget/TweetView;->z:I

    invoke-static {v0, v5}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    :goto_55
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->c()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/widget/TweetView;->o:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->i()Z

    move-result v5

    if-eqz v5, :cond_e0

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    iget-boolean v4, p1, Lcom/twitter/android/provider/m;->r:Z

    if-eqz v4, :cond_72

    iget-wide v4, p1, Lcom/twitter/android/provider/m;->q:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_72

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/provider/m;Landroid/content/res/Resources;)V

    :cond_72
    :goto_72
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->d()Z

    move-result v2

    if-eqz v2, :cond_14b

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->G:I

    invoke-static {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    :cond_80
    :goto_80
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    aput-object v2, v0, v8

    iget-object v2, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    aput-object v2, v0, v9

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->U:Landroid/graphics/drawable/Drawable;

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->V:[Landroid/graphics/drawable/Drawable;

    iget-boolean v0, p0, Lcom/twitter/android/widget/TweetView;->Z:Z

    if-eqz v0, :cond_a2

    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->j()Z

    move-result v0

    if-eqz v0, :cond_189

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    :cond_a2
    :goto_a2
    if-nez v1, :cond_a8

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    if-eqz v0, :cond_ab

    :cond_a8
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->refreshDrawableState()V

    :cond_ab
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->requestLayout()V

    :cond_ae
    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->e:Lcom/twitter/android/widget/az;

    invoke-interface {v0, p1}, Lcom/twitter/android/widget/az;->c(Lcom/twitter/android/provider/m;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->b()V

    return-void

    :cond_ba
    iget-wide v5, p1, Lcom/twitter/android/provider/m;->h:J

    invoke-static {v0, v5, v6}, Lcom/twitter/android/util/z;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/widget/TweetView;->u:Ljava/lang/String;

    goto :goto_47

    :cond_c3
    if-eqz v4, :cond_ce

    iget v5, p0, Lcom/twitter/android/widget/TweetView;->A:I

    invoke-static {v0, v5}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    goto :goto_55

    :cond_ce
    iget-boolean v5, p1, Lcom/twitter/android/provider/m;->l:Z

    if-eqz v5, :cond_dc

    iget v5, p0, Lcom/twitter/android/widget/TweetView;->B:I

    invoke-static {v0, v5}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_55

    :cond_dc
    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_55

    :cond_e0
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->h()Z

    move-result v5

    if-eqz v5, :cond_100

    if-nez v4, :cond_100

    sget v2, Lcom/twitter/android/ej;->promoted_by:I

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/android/provider/m;->f:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->E:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_72

    :cond_100
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->g()Z

    move-result v5

    if-eqz v5, :cond_120

    if-nez v4, :cond_120

    sget v2, Lcom/twitter/android/ej;->promoted_by:I

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/twitter/android/provider/m;->f:Ljava/lang/String;

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->D:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_72

    :cond_120
    iget-boolean v4, p1, Lcom/twitter/android/provider/m;->z:Z

    if-eqz v4, :cond_136

    sget v2, Lcom/twitter/android/ej;->top_tweet:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->C:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_72

    :cond_136
    iget-wide v4, p1, Lcom/twitter/android/provider/m;->q:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_145

    iget-boolean v2, p1, Lcom/twitter/android/provider/m;->r:Z

    if-eqz v2, :cond_145

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TweetView;->a(Lcom/twitter/android/provider/m;Landroid/content/res/Resources;)V

    goto/16 :goto_72

    :cond_145
    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    iput-object v7, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_72

    :cond_14b
    iget-wide v2, p1, Lcom/twitter/android/provider/m;->j:J

    cmp-long v2, v2, v10

    if-lez v2, :cond_15b

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->H:I

    invoke-static {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_80

    :cond_15b
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->e()Z

    move-result v2

    if-eqz v2, :cond_16b

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->I:I

    invoke-static {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_80

    :cond_16b
    invoke-virtual {p1}, Lcom/twitter/android/provider/m;->f()Z

    move-result v2

    if-eqz v2, :cond_17b

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->J:I

    invoke-static {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_80

    :cond_17b
    iget-boolean v2, p1, Lcom/twitter/android/provider/m;->x:Z

    if-eqz v2, :cond_80

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->F:I

    invoke-static {v0, v2}, Lcom/twitter/android/widget/TweetView;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_80

    :cond_189
    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    goto/16 :goto_a2
.end method

.method public final a(Lcom/twitter/android/widget/av;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/widget/TweetView;->ac:Lcom/twitter/android/widget/av;

    return-void
.end method

.method public final a(Lcom/twitter/android/widget/az;)V
    .registers 4

    iput-object p1, p0, Lcom/twitter/android/widget/TweetView;->e:Lcom/twitter/android/widget/az;

    invoke-interface {p1}, Lcom/twitter/android/widget/az;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/widget/TweetView;->aa:J

    return-void
.end method

.method public final a(Z)V
    .registers 2

    iput-boolean p1, p0, Lcom/twitter/android/widget/TweetView;->Z:Z

    return-void
.end method

.method public final b()V
    .registers 6

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    if-eqz v1, :cond_a

    iget-object v2, p0, Lcom/twitter/android/widget/TweetView;->Q:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_b

    :cond_a
    :goto_a
    return-void

    :cond_b
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v1, Lcom/twitter/android/provider/m;->k:Ljava/lang/String;

    if-eqz v3, :cond_31

    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->e:Lcom/twitter/android/widget/az;

    invoke-interface {v3, v1}, Lcom/twitter/android/widget/az;->a(Lcom/twitter/android/provider/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_19
    if-eqz v1, :cond_2b

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->N:I

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->O:I

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_2b
    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->Q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->invalidate()V

    goto :goto_a

    :cond_31
    move-object v1, v0

    goto :goto_19
.end method

.method final b(Z)V
    .registers 3

    if-eqz p1, :cond_12

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    :goto_e
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->refreshDrawableState()V

    return-void

    :cond_12
    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    goto :goto_e
.end method

.method final c()V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->ac:Lcom/twitter/android/widget/av;

    iget-wide v2, v0, Lcom/twitter/android/provider/m;->n:J

    iget-object v4, v0, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/twitter/android/widget/av;->a(JLjava/lang/String;Lcom/twitter/android/api/PromotedContent;)V

    return-void
.end method

.method protected drawableStateChanged()V
    .registers 7

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->d()V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->V:[Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_22

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getDrawableState()[I

    move-result-object v2

    array-length v3, v1

    const/4 v0, 0x0

    :goto_10
    if-ge v0, v3, :cond_22

    aget-object v4, v1, v0

    if-eqz v4, :cond_1f

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v5

    if-eqz v5, :cond_1f

    invoke-virtual {v4, v2}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_22
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->U:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_8

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->invalidate()V

    :goto_7
    return-void

    :cond_8
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7
.end method

.method protected onCreateDrawableState(I)[I
    .registers 4

    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Landroid/view/View;->onCreateDrawableState(I)[I

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_11

    sget-object v1, Lcom/twitter/android/widget/TweetView;->a:[I

    invoke-static {v0, v1}, Lcom/twitter/android/widget/TweetView;->mergeDrawableStates([I[I)[I

    :cond_11
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v1

    if-eqz v1, :cond_1c

    sget-object v1, Lcom/twitter/android/widget/TweetView;->b:[I

    invoke-static {v0, v1}, Lcom/twitter/android/widget/TweetView;->mergeDrawableStates([I[I)[I

    :cond_1c
    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->f()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 12

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    if-nez v0, :cond_9

    :cond_8
    :goto_8
    return-void

    :cond_9
    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getScrollY()I

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getPaddingTop()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v3, v0

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getPaddingRight()I

    move-result v5

    sub-int v5, v0, v5

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->K:I

    add-int/2addr v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v3

    iget v6, p0, Lcom/twitter/android/widget/TweetView;->L:I

    add-int/2addr v6, v4

    int-to-float v6, v6

    invoke-virtual {p1, v0, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->Q:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_178

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->c:Landroid/graphics/drawable/Drawable;

    :goto_3a
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->U:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_44

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_44
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_17c

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    sub-int/2addr v2, v0

    int-to-float v2, v2

    const/4 v6, 0x0

    invoke-virtual {p1, v2, v6}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/twitter/android/widget/TweetView;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    div-int/lit8 v0, v0, 0x2

    :goto_64
    sget-object v6, Lcom/twitter/android/widget/TweetView;->k:Landroid/text/TextPaint;

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->m:F

    iget-object v7, p0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    iget v8, p0, Lcom/twitter/android/widget/TweetView;->N:I

    iget v9, p0, Lcom/twitter/android/widget/TweetView;->M:I

    add-int/2addr v8, v9

    add-int/2addr v8, v3

    if-eqz v7, :cond_17f

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v1, v8

    int-to-float v3, v4

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v6, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->n:I

    invoke-virtual {v6, v1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v7, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    :goto_96
    sget-object v7, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v6, v7}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v7, p0, Lcom/twitter/android/widget/TweetView;->f:Landroid/text/StaticLayout;

    if-eqz v7, :cond_c0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    add-int/2addr v3, v8

    iget v7, p0, Lcom/twitter/android/widget/TweetView;->x:I

    add-int/2addr v3, v7

    int-to-float v3, v3

    iget v7, p0, Lcom/twitter/android/widget/TweetView;->L:I

    add-int/2addr v7, v4

    int-to-float v7, v7

    invoke-virtual {p1, v3, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget v3, p0, Lcom/twitter/android/widget/TweetView;->p:F

    invoke-virtual {v6, v3}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v3, p0, Lcom/twitter/android/widget/TweetView;->r:I

    invoke-virtual {v6, v3}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->f:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_c0
    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->i:Landroid/text/StaticLayout;

    if-eqz v3, :cond_105

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    sub-int v0, v5, v0

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v5

    sub-int/2addr v0, v5

    int-to-float v0, v0

    iget v5, p0, Lcom/twitter/android/widget/TweetView;->L:I

    add-int/2addr v5, v4

    int-to-float v5, v5

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->p:F

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->t:I

    invoke-virtual {v6, v0}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_102

    iget v5, p0, Lcom/twitter/android/widget/TweetView;->x:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    add-int/2addr v5, v7

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    sub-int/2addr v3, v7

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v5, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_105
    int-to-float v0, v4

    add-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    if-eqz v1, :cond_12b

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v1, v8

    int-to-float v3, v0

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v6, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->n:I

    invoke-virtual {v6, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    :cond_12b
    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_8

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->P:I

    add-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v1, v8

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget v2, p0, Lcom/twitter/android/widget/TweetView;->x:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->p:F

    invoke-virtual {v6, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    iget v1, p0, Lcom/twitter/android/widget/TweetView;->r:I

    invoke-virtual {v6, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/TweetView;->j:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v6}, Landroid/text/TextPaint;->clearShadowLayer()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    goto/16 :goto_8

    :cond_178
    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->Q:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_3a

    :cond_17c
    move v0, v1

    goto/16 :goto_64

    :cond_17f
    move v3, v1

    move v1, v2

    goto/16 :goto_96
.end method

.method protected onMeasure(II)V
    .registers 26

    const/4 v4, 0x0

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v21

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v19

    const/high16 v3, 0x4000

    if-ne v5, v3, :cond_1d3

    move/from16 v17, v2

    :goto_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/widget/TweetView;->d:Lcom/twitter/android/provider/m;

    move-object/from16 v22, v0

    if-eqz v22, :cond_259

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingLeft()I

    move-result v2

    sub-int v2, v17, v2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    sget-object v4, Lcom/twitter/android/widget/TweetView;->k:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TweetView;->N:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/widget/TweetView;->O:I

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getTop()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/widget/TweetView;->L:I

    add-int/2addr v6, v7

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getLeft()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/android/widget/TweetView;->K:I

    add-int/2addr v7, v8

    new-instance v8, Landroid/graphics/Rect;

    add-int/2addr v3, v7

    add-int v9, v6, v5

    invoke-direct {v8, v7, v6, v3, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/twitter/android/widget/TweetView;->ab:Landroid/graphics/Rect;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, v5

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingBottom()I

    move-result v5

    add-int v20, v3, v5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TweetView;->K:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetView;->ab:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    add-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/widget/TweetView;->M:I

    add-int/2addr v3, v5

    sub-int v18, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->i:Landroid/text/StaticLayout;

    if-nez v2, :cond_255

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->u:Ljava/lang/String;

    if-eqz v2, :cond_255

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TweetView;->p:F

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v2, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TweetView;->u:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetView;->u:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/twitter/android/widget/TweetView;->a(Ljava/lang/String;Landroid/text/TextPaint;)I

    move-result v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f80

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TweetView;->i:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->i:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TweetView;->x:I

    add-int/2addr v2, v3

    sub-int v2, v18, v2

    :goto_bd
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_251

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TweetView;->T:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/widget/TweetView;->x:I

    add-int/2addr v3, v5

    sub-int v16, v2, v3

    :goto_d2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    if-nez v2, :cond_151

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TweetView;->m:F

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual/range {v22 .. v22}, Lcom/twitter/android/provider/m;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_11a

    new-instance v5, Landroid/text/StaticLayout;

    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v6, v4}, Lcom/twitter/android/widget/TweetView;->a(Ljava/lang/String;Landroid/text/TextPaint;)I

    move-result v10

    sget-object v11, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/android/widget/TweetView;->v:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TweetView;->w:I

    int-to-float v13, v2

    const/4 v14, 0x0

    sget-object v15, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v9, v4

    invoke-direct/range {v5 .. v16}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getWidth()I

    move-result v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TweetView;->x:I

    add-int/2addr v2, v3

    sub-int v16, v16, v2

    :cond_11a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->f:Landroid/text/StaticLayout;

    if-nez v2, :cond_151

    if-lez v16, :cond_151

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TweetView;->p:F

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    if-nez v2, :cond_1e3

    const-string v6, ""

    :goto_136
    new-instance v5, Landroid/text/StaticLayout;

    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    invoke-static {v6, v4}, Lcom/twitter/android/widget/TweetView;->a(Ljava/lang/String;Landroid/text/TextPaint;)I

    move-result v10

    sget-object v11, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v12, 0x3f80

    const/4 v13, 0x0

    const/4 v14, 0x0

    sget-object v15, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v9, v4

    invoke-direct/range {v5 .. v16}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/twitter/android/widget/TweetView;->f:Landroid/text/StaticLayout;

    :cond_151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    if-nez v2, :cond_184

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->o:Ljava/lang/String;

    if-eqz v2, :cond_184

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TweetView;->m:F

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    new-instance v2, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TweetView;->o:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/widget/TweetView;->v:F

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/widget/TweetView;->w:I

    int-to-float v8, v5

    const/4 v9, 0x0

    move/from16 v5, v18

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    :cond_184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1c1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TweetView;->p:F

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TweetView;->W:Ljava/lang/String;

    new-instance v2, Landroid/text/StaticLayout;

    invoke-static {v3, v4}, Lcom/twitter/android/widget/TweetView;->a(Ljava/lang/String;Landroid/text/TextPaint;)I

    move-result v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f80

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TweetView;->j:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1c1
    move/from16 v3, v20

    :goto_1c3
    const/high16 v2, 0x4000

    move/from16 v0, v21

    if-ne v0, v2, :cond_1f8

    move/from16 v2, v19

    :cond_1cb
    :goto_1cb
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/TweetView;->setMeasuredDimension(II)V

    return-void

    :cond_1d3
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getSuggestedMinimumWidth()I

    move-result v3

    const/high16 v6, -0x8000

    if-ne v5, v6, :cond_25c

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_17

    :cond_1e3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_136

    :cond_1f8
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingTop()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getPaddingBottom()I

    move-result v4

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    if-eqz v4, :cond_210

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetView;->g:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    :cond_210
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    if-eqz v4, :cond_21f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetView;->h:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    :cond_21f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_237

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TweetView;->S:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/widget/TweetView;->P:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    :cond_237
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TweetView;->getSuggestedMinimumHeight()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/high16 v3, -0x8000

    move/from16 v0, v21

    if-ne v0, v3, :cond_1cb

    move/from16 v0, v19

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto/16 :goto_1cb

    :cond_251
    move/from16 v16, v2

    goto/16 :goto_d2

    :cond_255
    move/from16 v2, v18

    goto/16 :goto_bd

    :cond_259
    move v3, v4

    goto/16 :goto_1c3

    :cond_25c
    move/from16 v17, v3

    goto/16 :goto_17
.end method

.method public onStartTemporaryDetach()V
    .registers 1

    invoke-super {p0}, Landroid/view/View;->onStartTemporaryDetach()V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->f()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ac:Lcom/twitter/android/widget/av;

    if-eqz v0, :cond_2c

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1f

    if-ne v0, v1, :cond_1d

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    :cond_1d
    move v1, v2

    :cond_1e
    :goto_1e
    return v1

    :cond_1f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    packed-switch v0, :pswitch_data_e8

    :cond_2c
    :goto_2c
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1e

    :pswitch_31
    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ab:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_3d
    if-eqz v0, :cond_44

    instance-of v3, v0, Landroid/widget/ListView;

    if-eqz v3, :cond_62

    move v2, v1

    :cond_44
    if-eqz v2, :cond_67

    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ae:Lcom/twitter/android/widget/ax;

    if-nez v0, :cond_57

    new-instance v0, Lcom/twitter/android/widget/ax;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/ax;-><init>(Lcom/twitter/android/widget/TweetView;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TweetView;->ae:Lcom/twitter/android/widget/ax;

    :cond_57
    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ae:Lcom/twitter/android/widget/ax;

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/android/widget/TweetView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1e

    :cond_62
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_3d

    :cond_67
    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    goto :goto_1e

    :cond_6b
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    goto :goto_2c

    :pswitch_75
    iget v0, p0, Lcom/twitter/android/widget/TweetView;->ag:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_bb

    move v0, v1

    :goto_7c
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v3

    if-nez v3, :cond_84

    if-eqz v0, :cond_2c

    :cond_84
    if-eqz v0, :cond_89

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    :cond_89
    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->af:Lcom/twitter/android/widget/aw;

    if-nez v3, :cond_94

    new-instance v3, Lcom/twitter/android/widget/aw;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/aw;-><init>(Lcom/twitter/android/widget/TweetView;)V

    iput-object v3, p0, Lcom/twitter/android/widget/TweetView;->af:Lcom/twitter/android/widget/aw;

    :cond_94
    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->af:Lcom/twitter/android/widget/aw;

    invoke-virtual {p0, v3}, Lcom/twitter/android/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v3

    if-nez v3, :cond_9f

    invoke-virtual {p0}, Lcom/twitter/android/widget/TweetView;->c()V

    :cond_9f
    iget-object v3, p0, Lcom/twitter/android/widget/TweetView;->ad:Lcom/twitter/android/widget/ay;

    if-nez v3, :cond_aa

    new-instance v3, Lcom/twitter/android/widget/ay;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/ay;-><init>(Lcom/twitter/android/widget/TweetView;)V

    iput-object v3, p0, Lcom/twitter/android/widget/TweetView;->ad:Lcom/twitter/android/widget/ay;

    :cond_aa
    if-eqz v0, :cond_bd

    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ad:Lcom/twitter/android/widget/ay;

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/twitter/android/widget/TweetView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_b6
    :goto_b6
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->g()V

    goto/16 :goto_1e

    :cond_bb
    move v0, v2

    goto :goto_7c

    :cond_bd
    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ad:Lcom/twitter/android/widget/ay;

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_b6

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    goto :goto_b6

    :pswitch_c9
    iget-object v0, p0, Lcom/twitter/android/widget/TweetView;->ab:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_2c

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->g()V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    goto/16 :goto_1e

    :pswitch_df
    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->g()V

    goto/16 :goto_2c

    nop

    :pswitch_data_e8
    .packed-switch 0x0
        :pswitch_31
        :pswitch_75
        :pswitch_c9
        :pswitch_df
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .registers 3

    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    if-nez p1, :cond_12

    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->e()Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TweetView;->b(Z)V

    :cond_f
    invoke-direct {p0}, Lcom/twitter/android/widget/TweetView;->g()V

    :cond_12
    return-void
.end method
