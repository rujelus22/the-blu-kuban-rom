.class public Lcom/twitter/android/widget/BaseUserView;
.super Lcom/twitter/android/widget/CardRowView;


# instance fields
.field protected a:J

.field protected b:Landroid/widget/ImageButton;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:I

.field private e:I

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/CardRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lcom/twitter/android/ek;->UserView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/BaseUserView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/BaseUserView;->d:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/BaseUserView;->e:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/twitter/android/api/PromotedContent;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->l:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/PromotedContent;

    return-object v0
.end method

.method public final a(J)V
    .registers 3

    iput-wide p1, p0, Lcom/twitter/android/widget/BaseUserView;->a:J

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->j:Landroid/widget/ImageView;

    if-eqz p1, :cond_8

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_7
    return-void

    :cond_8
    iget-object v1, p0, Lcom/twitter/android/widget/BaseUserView;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7
.end method

.method public final a(Lcom/twitter/android/api/PromotedContent;)V
    .registers 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->l:Landroid/widget/TextView;

    if-eqz p1, :cond_23

    invoke-virtual {p1}, Lcom/twitter/android/api/PromotedContent;->b()Z

    move-result v1

    if-nez v1, :cond_23

    invoke-virtual {p1}, Lcom/twitter/android/api/PromotedContent;->a()Z

    move-result v1

    if-eqz v1, :cond_1d

    iget v1, p0, Lcom/twitter/android/widget/BaseUserView;->e:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_16
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    :goto_1c
    return-void

    :cond_1d
    iget v1, p0, Lcom/twitter/android/widget/BaseUserView;->d:I

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_16

    :cond_23
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    goto :goto_1c
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->k:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_d
    return-void

    :cond_e
    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_d
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_b
    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_20
    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b
.end method

.method public final a(Z)V
    .registers 4

    if-eqz p1, :cond_9

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_8
    return-void

    :cond_9
    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_8
.end method

.method public final b(Z)V
    .registers 4

    if-eqz p1, :cond_9

    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->h:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_8
    return-void

    :cond_9
    iget-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->h:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_8
.end method

.method protected onFinishInflate()V
    .registers 2

    sget v0, Lcom/twitter/android/eg;->action_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->b:Landroid/widget/ImageButton;

    sget v0, Lcom/twitter/android/eg;->screenname_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->f:Landroid/widget/TextView;

    sget v0, Lcom/twitter/android/eg;->protected_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->g:Landroid/widget/ImageView;

    sget v0, Lcom/twitter/android/eg;->verified_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->h:Landroid/widget/ImageView;

    sget v0, Lcom/twitter/android/eg;->name_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->i:Landroid/widget/TextView;

    sget v0, Lcom/twitter/android/eg;->user_image:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->j:Landroid/widget/ImageView;

    sget v0, Lcom/twitter/android/eg;->extra_info:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->k:Landroid/widget/TextView;

    sget v0, Lcom/twitter/android/eg;->promoted:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BaseUserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/BaseUserView;->l:Landroid/widget/TextView;

    return-void
.end method
