.class final Lcom/twitter/android/widget/g;
.super Landroid/database/DataSetObserver;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/HorizontalListView;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/HorizontalListView;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v1, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v1, v1, Lcom/twitter/android/widget/HorizontalListView;->a:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, v0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v0, v0, Lcom/twitter/android/widget/HorizontalListView;->d:Lcom/twitter/android/util/j;

    invoke-virtual {v0}, Lcom/twitter/android/util/j;->a()V

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->requestLayout()V

    return-void
.end method

.method public final onInvalidated()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/android/widget/HorizontalListView;->b:I

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v0, v0, Lcom/twitter/android/widget/HorizontalListView;->d:Lcom/twitter/android/util/j;

    invoke-virtual {v0}, Lcom/twitter/android/util/j;->a()V

    iget-object v0, p0, Lcom/twitter/android/widget/g;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->requestLayout()V

    return-void
.end method
