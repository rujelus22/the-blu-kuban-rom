.class final Lcom/twitter/android/widget/i;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/HorizontalListView;

.field private b:Landroid/widget/Scroller;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/HorizontalListView;)V
    .registers 4

    iput-object p1, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p1}, Lcom/twitter/android/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/widget/i;->b:Landroid/widget/Scroller;

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget v0, v0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_23

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    const/4 v1, -0x1

    iput v1, v0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/HorizontalListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/twitter/android/widget/i;->b:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_23

    iget-object v0, p0, Lcom/twitter/android/widget/i;->b:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->invalidate()V

    :cond_23
    return-void
.end method

.method public final a(I)V
    .registers 11

    const v6, 0x7fffffff

    const/4 v2, 0x0

    if-nez p1, :cond_7

    :goto_6
    return-void

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    const/4 v1, 0x4

    iput v1, v0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/HorizontalListView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/android/widget/i;->d:I

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/HorizontalListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    if-gez p1, :cond_3e

    move v1, v6

    :goto_2c
    iput v1, p0, Lcom/twitter/android/widget/i;->c:I

    iget-object v0, p0, Lcom/twitter/android/widget/i;->b:Landroid/widget/Scroller;

    move v3, p1

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/HorizontalListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_6

    :cond_3e
    move v1, v2

    goto :goto_2c
.end method

.method public final run()V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget v1, v1, Lcom/twitter/android/widget/HorizontalListView;->b:I

    if-eqz v1, :cond_e

    if-nez v0, :cond_12

    :cond_e
    invoke-virtual {p0}, Lcom/twitter/android/widget/i;->a()V

    :goto_11
    return-void

    :cond_12
    iget-object v0, p0, Lcom/twitter/android/widget/i;->b:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    iget v0, p0, Lcom/twitter/android/widget/i;->c:I

    sub-int/2addr v0, v2

    if-lez v0, :cond_39

    iget v3, p0, Lcom/twitter/android/widget/i;->d:I

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_27
    iget-object v3, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v3, v0, v0}, Lcom/twitter/android/widget/HorizontalListView;->a(Lcom/twitter/android/widget/HorizontalListView;II)Z

    move-result v3

    if-eqz v1, :cond_4e

    if-eqz v3, :cond_41

    iput v2, p0, Lcom/twitter/android/widget/i;->c:I

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/HorizontalListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_11

    :cond_39
    iget v3, p0, Lcom/twitter/android/widget/i;->d:I

    neg-int v3, v3

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_27

    :cond_41
    if-lez v0, :cond_52

    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->g(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v0

    :goto_49
    iget-object v1, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v1, v0, v0}, Lcom/twitter/android/widget/HorizontalListView;->a(Lcom/twitter/android/widget/HorizontalListView;II)Z

    :cond_4e
    invoke-virtual {p0}, Lcom/twitter/android/widget/i;->a()V

    goto :goto_11

    :cond_52
    iget-object v0, p0, Lcom/twitter/android/widget/i;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->h(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v0

    neg-int v0, v0

    goto :goto_49
.end method
