.class final Lcom/twitter/android/widget/ae;
.super Landroid/database/DataSetObserver;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/ad;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/ad;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/ad;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/ad;

    iget-boolean v0, v0, Lcom/twitter/android/widget/ad;->a:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/ad;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ad;->notifyDataSetChanged()V

    :cond_b
    return-void
.end method

.method public final onInvalidated()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/ad;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ad;->getCount()I

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/ad;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ad;->notifyDataSetInvalidated()V

    :goto_d
    return-void

    :cond_e
    invoke-virtual {p0}, Lcom/twitter/android/widget/ae;->onChanged()V

    goto :goto_d
.end method
