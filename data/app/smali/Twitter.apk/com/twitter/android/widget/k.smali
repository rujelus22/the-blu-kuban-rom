.class final Lcom/twitter/android/widget/k;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/HorizontalListView;

.field private b:[Landroid/view/View;

.field private c:Ljava/util/ArrayList;

.field private d:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/HorizontalListView;)V
    .registers 3

    iput-object p1, p0, Lcom/twitter/android/widget/k;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/k;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .registers 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/android/widget/k;->d:I

    sub-int v2, p1, v0

    iget-object v3, p0, Lcom/twitter/android/widget/k;->b:[Landroid/view/View;

    if-ltz v2, :cond_11

    array-length v0, v3

    if-ge v2, v0, :cond_11

    aget-object v0, v3, v2

    aput-object v1, v3, v2

    :goto_10
    return-object v0

    :cond_11
    move-object v0, v1

    goto :goto_10
.end method

.method public final a()V
    .registers 8

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/widget/k;->b:[Landroid/view/View;

    iget-object v4, p0, Lcom/twitter/android/widget/k;->c:Ljava/util/ArrayList;

    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_24

    aget-object v5, v3, v1

    if-eqz v5, :cond_20

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    const/4 v6, 0x0

    aput-object v6, v3, v1

    iget v6, p0, Lcom/twitter/android/widget/k;->d:I

    add-int/2addr v6, v1

    iput v6, v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->b:I

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_20
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    :cond_24
    iget-object v0, p0, Lcom/twitter/android/widget/k;->b:[Landroid/view/View;

    array-length v0, v0

    iget-object v4, p0, Lcom/twitter/android/widget/k;->c:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int v5, v1, v0

    add-int/lit8 v0, v1, -0x1

    move v1, v2

    move v3, v0

    :goto_33
    if-ge v1, v5, :cond_46

    iget-object v6, p0, Lcom/twitter/android/widget/k;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v6, v0, v2}, Lcom/twitter/android/widget/HorizontalListView;->a(Lcom/twitter/android/widget/HorizontalListView;Landroid/view/View;Z)V

    add-int/lit8 v3, v3, -0x1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_33

    :cond_46
    return-void
.end method

.method public final a(II)V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/widget/k;->b:[Landroid/view/View;

    if-eqz v0, :cond_7

    array-length v1, v0

    if-ge v1, p2, :cond_d

    :cond_7
    new-array v0, p2, [Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/widget/k;->b:[Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/widget/k;->b:[Landroid/view/View;

    :cond_d
    iput p1, p0, Lcom/twitter/android/widget/k;->d:I

    const/4 v1, 0x0

    :goto_10
    if-ge v1, p2, :cond_1d

    iget-object v2, p0, Lcom/twitter/android/widget/k;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    :cond_1d
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .registers 5

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    if-nez v0, :cond_9

    :goto_8
    return-void

    :cond_9
    iput p2, v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->b:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->a:Z

    iget-object v0, p0, Lcom/twitter/android/widget/k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public final b(I)Landroid/view/View;
    .registers 7

    iget-object v3, p0, Lcom/twitter/android/widget/k;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_a
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_10
    if-ge v2, v4, :cond_2a

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;

    iget v1, v1, Lcom/twitter/android/widget/HorizontalListView$LayoutParams;->b:I

    if-ne v1, p1, :cond_26

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_9

    :cond_26
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_10

    :cond_2a
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_9
.end method

.method public final b()V
    .registers 6

    iget-object v2, p0, Lcom/twitter/android/widget/k;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_9
    if-ltz v1, :cond_1b

    iget-object v3, p0, Lcom/twitter/android/widget/k;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Lcom/twitter/android/widget/HorizontalListView;->b(Lcom/twitter/android/widget/HorizontalListView;Landroid/view/View;Z)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_9

    :cond_1b
    return-void
.end method
