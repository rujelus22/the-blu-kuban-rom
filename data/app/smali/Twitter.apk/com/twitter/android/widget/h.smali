.class final Lcom/twitter/android/widget/h;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/HorizontalListView;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget v0, v0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    if-nez v0, :cond_60

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    iput v3, v0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget-object v1, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v1}, Lcom/twitter/android/widget/HorizontalListView;->a(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v2}, Lcom/twitter/android/widget/HorizontalListView;->b(Lcom/twitter/android/widget/HorizontalListView;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_60

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v1

    if-nez v1, :cond_60

    iget-object v1, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    iget-boolean v1, v1, Lcom/twitter/android/widget/HorizontalListView;->c:Z

    if-nez v1, :cond_5b

    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/HorizontalListView;->setPressed(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->c(Lcom/twitter/android/widget/HorizontalListView;)V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/HorizontalListView;->refreshDrawableState()V

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->d(Lcom/twitter/android/widget/HorizontalListView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/android/widget/HorizontalListView;->d(Lcom/twitter/android/widget/HorizontalListView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_5b

    instance-of v1, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v1, :cond_5b

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    :cond_5b
    iget-object v0, p0, Lcom/twitter/android/widget/h;->a:Lcom/twitter/android/widget/HorizontalListView;

    const/4 v1, 0x2

    iput v1, v0, Lcom/twitter/android/widget/HorizontalListView;->e:I

    :cond_60
    return-void
.end method
