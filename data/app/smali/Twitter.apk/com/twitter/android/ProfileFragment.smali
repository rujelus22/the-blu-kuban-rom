.class public Lcom/twitter/android/ProfileFragment;
.super Lcom/twitter/android/BaseListFragment;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/cl;
.implements Lcom/twitter/android/f;
.implements Lcom/twitter/android/fn;
.implements Lcom/twitter/android/widget/a;
.implements Lcom/twitter/android/widget/an;
.implements Lcom/twitter/android/widget/av;
.implements Lcom/twitter/android/widget/x;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field private C:Z

.field private D:Landroid/widget/ImageView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field private G:Landroid/widget/TextView;

.field private H:Landroid/widget/ImageView;

.field private I:Landroid/widget/TextView;

.field private J:Landroid/widget/TextView;

.field private K:Landroid/widget/TextView;

.field private L:Landroid/widget/ImageView;

.field private M:Landroid/widget/ImageView;

.field private N:Landroid/widget/TextView;

.field private O:Lcom/twitter/android/dt;

.field private P:I

.field private Q:Ljava/lang/String;

.field private R:Ljava/util/HashSet;

.field private S:Lcom/twitter/android/util/FriendshipCache;

.field private T:I

.field private U:Z

.field private V:Z

.field private W:Landroid/widget/LinearLayout;

.field private X:Lcom/twitter/android/util/f;

.field private Y:Lcom/twitter/android/util/f;

.field private Z:Lcom/twitter/android/widget/ShadowTextView;

.field private aa:Landroid/widget/ImageButton;

.field private ab:Landroid/widget/ImageButton;

.field private ac:Landroid/widget/ImageButton;

.field private ad:Landroid/widget/ImageButton;

.field private ae:Landroid/view/View;

.field private af:Lcom/twitter/android/widget/ad;

.field private ag:Lcom/twitter/android/ff;

.field private ah:Lcom/twitter/android/dz;

.field private ai:Landroid/widget/RelativeLayout;

.field private aj:Lcom/twitter/android/widget/ProfileHeader;

.field private ak:Landroid/support/v4/view/ViewPager;

.field private al:Z

.field private am:I

.field private an:Z

.field private ao:Landroid/view/animation/TranslateAnimation;

.field private ap:Lcom/twitter/android/fo;

.field private aq:Lcom/twitter/android/cq;

.field private ar:Lcom/twitter/android/e;

.field private as:Z

.field m:Lcom/twitter/android/dy;

.field n:Lcom/twitter/android/api/PromotedContent;

.field o:Lcom/twitter/android/api/ad;

.field p:Landroid/content/SharedPreferences;

.field q:J

.field private final r:Ljava/util/HashSet;

.field private s:Landroid/graphics/Bitmap;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Lcom/twitter/android/api/TweetEntities;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->r:Ljava/util/HashSet;

    iput v1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    iput-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->an:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;I)I
    .registers 2

    iput p1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;)Landroid/widget/ImageView;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->L:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->D:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->E:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Lcom/twitter/android/util/f;)Lcom/twitter/android/util/f;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->X:Lcom/twitter/android/util/f;

    return-object p1
.end method

.method private a(Landroid/content/res/Resources;)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->X:Lcom/twitter/android/util/f;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_15

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_14
    return-void

    :cond_15
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    const v1, 0x7f02001e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundResource(I)V

    goto :goto_14
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .registers 4

    if-nez p1, :cond_d

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020117

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    :cond_d
    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->s:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->u()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v0}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_1b
    return-void
.end method

.method private a(Landroid/view/ViewGroup;ILjava/lang/String;I)V
    .registers 8

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f070028

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0700b9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p4}, Lcom/twitter/android/util/z;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;)V
    .registers 11

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-virtual {p1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_c
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->L:Landroid/widget/ImageView;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->M:Landroid/widget/ImageView;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_76

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_76

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_76

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->M:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_36
    :goto_36
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->N:Landroid/widget/TextView;

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_81

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_81

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->N:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4f
    :goto_4f
    return-void

    :cond_50
    if-eqz p3, :cond_72

    iget-object v0, p3, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_72

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/at;->a(Landroid/content/res/Resources;Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;Lcom/twitter/android/widget/an;Z)V

    :goto_6e
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_c

    :cond_72
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6e

    :cond_76
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->M:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_36

    :cond_81
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->N:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4f
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Landroid/content/res/Resources;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/res/Resources;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Landroid/graphics/Bitmap;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private a(Lcom/twitter/android/service/ScribeEvent;)V
    .registers 8

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    new-instance v1, Lcom/twitter/android/service/ScribeLog;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, p1, v4}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1c

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/android/service/ScribeLog;->profileId:Ljava/lang/String;

    :cond_1c
    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/ProfileFragment;I)I
    .registers 3

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/ProfileFragment;)Landroid/widget/ImageView;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->M:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/ProfileFragment;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->H:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->F:Landroid/widget/TextView;

    return-object p1
.end method

.method private b(Z)V
    .registers 15

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-object v8, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-object v9, v8, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    iget-wide v0, v8, Lcom/twitter/android/api/ad;->a:J

    iput-wide v0, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v7}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_2ff

    const/4 v0, 0x1

    :goto_19
    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->W:Landroid/widget/LinearLayout;

    const v1, 0x7f0700db

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v1, 0x7f0700ba

    const v2, 0x7f0b002b

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v8, Lcom/twitter/android/api/ad;->n:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/ViewGroup;ILjava/lang/String;I)V

    const v1, 0x7f0700bb

    const v2, 0x7f0b0033

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v8, Lcom/twitter/android/api/ad;->l:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/ViewGroup;ILjava/lang/String;I)V

    const v1, 0x7f0700bc

    const v2, 0x7f0b0034

    invoke-virtual {v10, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, v8, Lcom/twitter/android/api/ad;->k:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/ViewGroup;ILjava/lang/String;I)V

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->e()Lcom/twitter/android/client/k;

    move-result-object v5

    iget-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v2, :cond_302

    if-eqz v5, :cond_302

    iget-object v4, v5, Lcom/twitter/android/client/k;->b:Ljava/lang/String;

    iget-object v3, v5, Lcom/twitter/android/client/k;->d:Ljava/lang/String;

    iget-object v2, v5, Lcom/twitter/android/client/k;->c:Ljava/lang/String;

    iget-object v5, v5, Lcom/twitter/android/client/k;->a:Ljava/lang/String;

    :cond_6d
    :goto_6d
    iput-object v5, p0, Lcom/twitter/android/ProfileFragment;->t:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->t()V

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v5, :cond_7b

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v5}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_7b
    iput-object v9, p0, Lcom/twitter/android/ProfileFragment;->u:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->s()V

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v5, :cond_89

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v5}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_89
    iget-boolean v5, v8, Lcom/twitter/android/api/ad;->i:Z

    iget-boolean v9, v8, Lcom/twitter/android/api/ad;->h:Z

    iput-boolean v5, p0, Lcom/twitter/android/ProfileFragment;->B:Z

    iput-boolean v9, p0, Lcom/twitter/android/ProfileFragment;->C:Z

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->v()V

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v5, :cond_9d

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v5}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_9d
    iput-object v4, p0, Lcom/twitter/android/ProfileFragment;->x:Ljava/lang/String;

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->y:Lcom/twitter/android/api/TweetEntities;

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->w()V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v1, :cond_ad

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v1}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_ad
    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->w:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->q()V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v1, :cond_bb

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v1}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_bb
    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    if-eqz v0, :cond_32d

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    :goto_c1
    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->r()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v0, :cond_cd

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v0}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    :cond_cd
    iget-object v0, v8, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    iget v1, v7, Lcom/twitter/android/client/b;->g:F

    invoke-static {v0, v1}, Lcom/twitter/android/util/z;->a(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/util/f;

    invoke-direct {v1, v0}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->Y:Lcom/twitter/android/util/f;

    iget-object v0, v8, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    iget v1, v7, Lcom/twitter/android/client/b;->g:F

    invoke-static {v0, v1}, Lcom/twitter/android/util/z;->b(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/util/f;

    invoke-direct {v1, v0}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->X:Lcom/twitter/android/util/f;

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_331

    new-instance v0, Lcom/twitter/android/du;

    invoke-direct {v0, p0}, Lcom/twitter/android/du;-><init>(Lcom/twitter/android/ProfileFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/du;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_101
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v2, :cond_336

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileFragment;->h(I)V

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-boolean v2, v2, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v2, :cond_129

    iget v2, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v2, v2, 0x20

    if-nez v2, :cond_129

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/b;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    iget v2, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/twitter/android/ProfileFragment;->am:I

    :cond_129
    iget-wide v2, v8, Lcom/twitter/android/api/ad;->t:J

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_14c

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_14c

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->u:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    :cond_14c
    :goto_14c
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->c(Z)V

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->o()V

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->ag:Lcom/twitter/android/ff;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/ff;->setNotifyOnChange(Z)V

    invoke-virtual {v2}, Lcom/twitter/android/ff;->clear()V

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/UsersActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "type"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/fe;

    const v5, 0x7f0b0033

    invoke-virtual {v6, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct {v4, v5, v9, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v2, v4}, Lcom/twitter/android/ff;->add(Ljava/lang/Object;)V

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/UsersActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "type"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/fe;

    const v5, 0x7f0b0034

    invoke-virtual {v6, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct {v4, v5, v9, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v2, v4}, Lcom/twitter/android/ff;->add(Ljava/lang/Object;)V

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v3, :cond_1d8

    iget-boolean v3, v8, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v3, :cond_1d8

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/UsersActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "type"

    const/16 v5, 0x12

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/fe;

    const v5, 0x7f0b01ec

    invoke-virtual {v6, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct {v4, v5, v9, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v2, v4}, Lcom/twitter/android/ff;->add(Ljava/lang/Object;)V

    :cond_1d8
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/TimelineActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "type"

    const/4 v5, 0x2

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "title"

    const v5, 0x7f0b0030

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/fe;

    const v5, 0x7f0b0030

    invoke-virtual {v6, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct {v4, v5, v9, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v2, v4}, Lcom/twitter/android/ff;->add(Ljava/lang/Object;)V

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/ListsActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "profile"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/fe;

    const v5, 0x7f0b002c

    invoke-virtual {v6, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v9, 0x0

    invoke-direct {v4, v5, v9, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v2, v4}, Lcom/twitter/android/ff;->add(Ljava/lang/Object;)V

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v3, :cond_257

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/UsersActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/fe;

    const v3, 0x7f0b002f

    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v3, v4, v0}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    invoke-virtual {v2, v1}, Lcom/twitter/android/ff;->add(Ljava/lang/Object;)V

    :cond_257
    invoke-virtual {v2}, Lcom/twitter/android/ff;->notifyDataSetChanged()V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_37c

    new-instance v1, Lcom/twitter/android/fe;

    const v0, 0x7f0b0111

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/DraftsActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "return_to_drafts"

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    new-instance v2, Lcom/twitter/android/fe;

    const v0, 0x7f0b01b2

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/SearchQueriesActivity;

    invoke-direct {v4, v6, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "q_type"

    const/4 v6, 0x6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    new-instance v0, Lcom/twitter/android/fd;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/twitter/android/fe;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v2, v3, v1

    invoke-direct {v0, v3}, Lcom/twitter/android/fd;-><init>([Lcom/twitter/android/fe;)V

    :goto_2a2
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v1, v8}, Lcom/twitter/android/dy;->a(Lcom/twitter/android/api/ad;)V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v1, v8}, Lcom/twitter/android/dz;->a(Lcom/twitter/android/api/ad;)V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->af:Lcom/twitter/android/widget/ad;

    if-nez v1, :cond_2d4

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-nez v1, :cond_386

    iget-boolean v1, v8, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v1, :cond_386

    iget v1, v8, Lcom/twitter/android/api/ad;->y:I

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_386

    new-instance v0, Lcom/twitter/android/widget/ad;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->af:Lcom/twitter/android/widget/ad;

    :goto_2cd
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->af:Lcom/twitter/android/widget/ad;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2d4
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(I)V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->U:Z

    if-eqz v0, :cond_2f1

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->ai:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    iget-object v0, v8, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    if-eqz v0, :cond_2ee

    invoke-virtual {v7}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/android/service/ScribeEvent;->aq:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v7, v0, v1, v2}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    :cond_2ee
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->U:Z

    :cond_2f1
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_2fe

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-boolean v0, v0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_2fe

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->m()V

    :cond_2fe
    return-void

    :cond_2ff
    const/4 v0, 0x0

    goto/16 :goto_19

    :cond_302
    iget-object v4, v8, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    iget-object v3, v8, Lcom/twitter/android/api/ad;->j:Ljava/lang/String;

    iget-object v2, v8, Lcom/twitter/android/api/ad;->e:Ljava/lang/String;

    iget-object v5, v8, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    iget-object v11, v8, Lcom/twitter/android/api/ad;->v:Lcom/twitter/android/api/TweetEntities;

    if-eqz v11, :cond_325

    iget-object v12, v11, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v12, :cond_325

    iget-object v12, v11, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_325

    iget-object v0, v11, Lcom/twitter/android/api/TweetEntities;->urls:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/TweetEntities$Url;

    iget-object v0, v0, Lcom/twitter/android/api/TweetEntities$Url;->displayUrl:Ljava/lang/String;

    :cond_325
    iget-object v11, v8, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    if-eqz v11, :cond_6d

    iget-object v1, v8, Lcom/twitter/android/api/ad;->u:Lcom/twitter/android/api/TweetEntities;

    goto/16 :goto_6d

    :cond_32d
    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    goto/16 :goto_c1

    :cond_331
    invoke-direct {p0, v10}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/res/Resources;)V

    goto/16 :goto_101

    :cond_336
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "friendship"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_356

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "friendship"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileFragment;->h(I)V

    const-string v1, "friendship"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto/16 :goto_14c

    :cond_356
    iget-wide v2, v8, Lcom/twitter/android/api/ad;->q:J

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_375

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_14c

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/b;->b(J)Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    goto/16 :goto_14c

    :cond_375
    iget v0, v8, Lcom/twitter/android/api/ad;->y:I

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->h(I)V

    goto/16 :goto_14c

    :cond_37c
    new-instance v0, Lcom/twitter/android/fd;

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/twitter/android/fe;

    invoke-direct {v0, v1}, Lcom/twitter/android/fd;-><init>([Lcom/twitter/android/fe;)V

    goto/16 :goto_2a2

    :cond_386
    new-instance v1, Lcom/twitter/android/widget/ad;

    const/4 v2, 0x5

    new-array v2, v2, [Landroid/widget/BaseAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->aq:Lcom/twitter/android/cq;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->ag:Lcom/twitter/android/ff;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    aput-object v3, v2, v0

    invoke-direct {v1, v2}, Lcom/twitter/android/widget/ad;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->af:Lcom/twitter/android/widget/ad;

    goto/16 :goto_2cd
.end method

.method static synthetic c(Lcom/twitter/android/ProfileFragment;I)I
    .registers 3

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->G:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic c(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/dt;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    return-object v0
.end method

.method private c(Z)V
    .registers 8

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_28

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_27

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_27
    :goto_27
    return-void

    :cond_28
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_3b

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    :cond_3b
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_4e

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    :cond_4e
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_61

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    :cond_61
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_27

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_27

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->am:I

    goto :goto_27
.end method

.method static synthetic d(Lcom/twitter/android/ProfileFragment;)Landroid/support/v4/view/ViewPager;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->I:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic e(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->J:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/widget/ProfileHeader;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->K:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic f(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/ProfileFragment;)I
    .registers 2

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    return-object v0
.end method

.method private h(I)V
    .registers 6

    const v3, 0x7f0b010e

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-eqz v0, :cond_7f

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_23

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    :cond_14
    :goto_14
    iput p1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ae:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ai:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void

    :cond_23
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_3f

    move v0, v1

    :goto_28
    if-eqz v0, :cond_41

    const v0, 0x7f0b010f

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    :cond_30
    :goto_30
    iput p1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->x()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    invoke-virtual {v0}, Lcom/twitter/android/dt;->notifyDataSetChanged()V

    goto :goto_14

    :cond_3f
    move v0, v2

    goto :goto_28

    :cond_41
    invoke-virtual {p0, v1, v3}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-boolean v0, v0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->W:Landroid/widget/LinearLayout;

    const v3, 0x7f0700db

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v3, 0x7f0700ba

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setEnabled(Z)V

    const v3, 0x7f0700bb

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setEnabled(Z)V

    const v3, 0x7f0700bc

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gd;->a(I)V

    goto :goto_30

    :cond_7f
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    goto :goto_14
.end method

.method static synthetic i(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->u()V

    return-void
.end method

.method static synthetic j(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->t()V

    return-void
.end method

.method static synthetic k(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->s()V

    return-void
.end method

.method static synthetic l(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->x()V

    return-void
.end method

.method static synthetic m(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->v()V

    return-void
.end method

.method static synthetic n(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->w()V

    return-void
.end method

.method private o()V
    .registers 11

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    if-nez v0, :cond_12

    new-instance v0, Lcom/twitter/android/dy;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/twitter/android/dy;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;Lcom/twitter/android/widget/av;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    :cond_12
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ag:Lcom/twitter/android/ff;

    if-nez v0, :cond_1d

    new-instance v0, Lcom/twitter/android/ff;

    invoke-direct {v0, v1}, Lcom/twitter/android/ff;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ag:Lcom/twitter/android/ff;

    :cond_1d
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    if-nez v0, :cond_28

    new-instance v0, Lcom/twitter/android/fo;

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/fo;-><init>(Landroid/content/Context;Lcom/twitter/android/client/b;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    :cond_28
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aq:Lcom/twitter/android/cq;

    if-nez v0, :cond_3a

    new-instance v0, Lcom/twitter/android/cq;

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    invoke-direct {v0, v4}, Lcom/twitter/android/cq;-><init>(Lcom/twitter/android/fo;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aq:Lcom/twitter/android/cq;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aq:Lcom/twitter/android/cq;

    invoke-virtual {v0, p0}, Lcom/twitter/android/cq;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_3a
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    if-nez v0, :cond_5c

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-eqz v0, :cond_5d

    new-instance v0, Lcom/twitter/android/el;

    const v5, 0x7f020049

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    move v4, v2

    move-object v6, p0

    move v8, v2

    move v9, v2

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/el;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    :goto_52
    invoke-virtual {v0, p0}, Lcom/twitter/android/el;->a(Lcom/twitter/android/cl;)V

    new-instance v2, Lcom/twitter/android/dz;

    invoke-direct {v2, v1, v0}, Lcom/twitter/android/dz;-><init>(Landroid/content/Context;Lcom/twitter/android/el;)V

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    :cond_5c
    return-void

    :cond_5d
    new-instance v0, Lcom/twitter/android/el;

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    move v4, v2

    move v5, v2

    move-object v6, p0

    move v8, v2

    move v9, v2

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/el;-><init>(Landroid/content/Context;ILcom/twitter/android/client/b;ZILcom/twitter/android/widget/a;Lcom/twitter/android/util/FriendshipCache;II)V

    goto :goto_52
.end method

.method static synthetic o(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->q()V

    return-void
.end method

.method static synthetic p(Lcom/twitter/android/ProfileFragment;)V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->r()V

    return-void
.end method

.method private p()Z
    .registers 7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->p:Landroid/content/SharedPreferences;

    const-string v3, "last_refresh"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method static synthetic q(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Ljava/lang/String;

    return-object v0
.end method

.method private q()V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->J:Landroid/widget/TextView;

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->w:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;)V

    :cond_a
    return-void
.end method

.method static synthetic r(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Ljava/lang/String;

    return-object v0
.end method

.method private r()V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->K:Landroid/widget/TextView;

    if-eqz v0, :cond_12

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;)V

    new-instance v1, Lcom/twitter/android/dq;

    invoke-direct {v1, p0}, Lcom/twitter/android/dq;-><init>(Lcom/twitter/android/ProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_12
    return-void
.end method

.method static synthetic s(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/util/f;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->X:Lcom/twitter/android/util/f;

    return-object v0
.end method

.method private s()V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->F:Landroid/widget/TextView;

    if-eqz v0, :cond_18

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "@"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_18
    return-void
.end method

.method static synthetic t(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/util/f;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->Y:Lcom/twitter/android/util/f;

    return-object v0
.end method

.method private t()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->E:Landroid/widget/TextView;

    if-eqz v0, :cond_11

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->t:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_11
    :goto_11
    return-void

    :cond_12
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_11
.end method

.method static synthetic u(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/fo;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    return-object v0
.end method

.method private u()V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->D:Landroid/widget/ImageView;

    if-eqz v0, :cond_24

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->s:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v1, 0x7f0b0008

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->u:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/ProfileFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/dr;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/dr;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/net/Uri;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_24
    return-void
.end method

.method private v()V
    .registers 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->H:Landroid/widget/ImageView;

    if-eqz v0, :cond_12

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->B:Z

    if-eqz v1, :cond_13

    const v1, 0x7f020129

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_12
    :goto_12
    return-void

    :cond_13
    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->C:Z

    if-eqz v1, :cond_21

    const v1, 0x7f020123

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_12

    :cond_21
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_12
.end method

.method static synthetic v(Lcom/twitter/android/ProfileFragment;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    return v0
.end method

.method private w()V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->I:Landroid/widget/TextView;

    if-eqz v0, :cond_b

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->y:Lcom/twitter/android/api/TweetEntities;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/android/api/TweetEntities;)V

    :cond_b
    return-void
.end method

.method static synthetic w(Lcom/twitter/android/ProfileFragment;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->as:Z

    return v0
.end method

.method private x()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->G:Landroid/widget/TextView;

    if-eqz v0, :cond_10

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->G:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_10
    :goto_10
    return-void

    :cond_11
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->G:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_10
.end method


# virtual methods
.method public final a(II)V
    .registers 10

    const v6, 0x7f0b010e

    const/4 v5, 0x4

    const/4 v0, -0x1

    const/4 v4, 0x1

    packed-switch p1, :pswitch_data_b8

    :cond_9
    :goto_9
    :pswitch_9
    return-void

    :pswitch_a
    if-ne p2, v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->b(JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->aj:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-static {v0, v5}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-virtual {p0, v4, v6}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    goto :goto_9

    :pswitch_26
    if-ne p2, v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->c(JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->ak:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-static {v0, v5}, Lcom/twitter/android/provider/ad;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-virtual {p0, v4, v6}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    goto :goto_9

    :pswitch_42
    if-ne p2, v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->d(JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->al:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_9

    :pswitch_53
    if-ne p2, v0, :cond_9

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-static {v0, v4}, Lcom/twitter/android/provider/ad;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-virtual {p0, v4, v6}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->an:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_9

    :pswitch_73
    if-nez p2, :cond_84

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_9

    :cond_84
    if-ne p2, v4, :cond_96

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_9

    :cond_96
    const/4 v0, 0x2

    if-ne p2, v0, :cond_9

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x7f0b0009

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_9

    :pswitch_data_b8
    .packed-switch 0x1
        :pswitch_53
        :pswitch_a
        :pswitch_26
        :pswitch_42
        :pswitch_9
        :pswitch_73
    .end packed-switch
.end method

.method public final a(JLjava/lang/String;Lcom/twitter/android/api/PromotedContent;)V
    .registers 12

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    cmp-long v0, p1, v2

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p4}, Lcom/twitter/android/client/b;->a(Landroid/content/Context;JLcom/twitter/android/api/PromotedContent;)V

    :goto_14
    return-void

    :cond_15
    const v0, 0x7f070036

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ao:Landroid/view/animation/TranslateAnimation;

    if-nez v0, :cond_60

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    const v3, 0x7f0c0024

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v4, v3

    invoke-direct {v0, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    new-instance v4, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v4}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    new-instance v4, Lcom/twitter/android/dp;

    invoke-direct {v4, p0, v2, v0}, Lcom/twitter/android/dp;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/view/View;Landroid/view/animation/TranslateAnimation;)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v3, v3

    invoke-direct {v0, v6, v3, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    int-to-long v5, v1

    invoke-virtual {v0, v5, v6}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ao:Landroid/view/animation/TranslateAnimation;

    :cond_60
    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_14
.end method

.method public final a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 12

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_114

    :cond_7
    :goto_7
    return-void

    :pswitch_8
    if-eqz p2, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->h(I)V

    goto :goto_7

    :pswitch_19
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0, p2}, Lcom/twitter/android/gd;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0}, Lcom/twitter/android/gd;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3e

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_57

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_57

    :cond_3e
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x3

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/b;->a(IJJJI)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;I)V

    goto :goto_7

    :cond_57
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/dy;->a(Z)V

    goto :goto_7

    :pswitch_5e
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v0}, Lcom/twitter/android/dz;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/el;

    invoke-virtual {v0, p2}, Lcom/twitter/android/el;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v0}, Lcom/twitter/android/dz;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/el;

    invoke-virtual {v0}, Lcom/twitter/android/el;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_83

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_83
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x6

    iget-wide v4, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/b;->a(ZIIJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_7

    :pswitch_99
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    invoke-virtual {v0, p2}, Lcom/twitter/android/fo;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    invoke-virtual {v0}, Lcom/twitter/android/fo;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b2

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_b2
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/16 v1, 0x11

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/16 v8, 0x32

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/b;->a(IJJJI)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;I)V

    goto/16 :goto_7

    :pswitch_ce
    if-eqz p2, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_ed

    const/4 v0, 0x1

    :goto_df
    if-eqz v0, :cond_ef

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/widget/ImageButton;

    const v2, 0x7f020108

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_e9
    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->an:Z

    goto/16 :goto_7

    :cond_ed
    const/4 v0, 0x0

    goto :goto_df

    :cond_ef
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/widget/ImageButton;

    const v2, 0x7f020125

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_e9

    :pswitch_f8
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-boolean v0, v0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_7

    if-eqz p2, :cond_7

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ag:Lcom/twitter/android/ff;

    const/4 v1, 0x2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ff;->a(II)V

    goto/16 :goto_7

    :pswitch_data_114
    .packed-switch 0x1
        :pswitch_8
        :pswitch_19
        :pswitch_5e
        :pswitch_99
        :pswitch_ce
        :pswitch_f8
    .end packed-switch
.end method

.method public final synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .registers 5

    check-cast p2, Lcom/twitter/android/api/PromotedContent;

    if-eqz p2, :cond_14

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->r:Ljava/util/HashSet;

    iget-object v1, p2, Lcom/twitter/android/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/api/PromotedContent;)V

    :cond_14
    return-void
.end method

.method public final a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 11

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v1

    if-nez v1, :cond_14

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_13
    :goto_13
    return-void

    :cond_14
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->af:Lcom/twitter/android/widget/ad;

    if-eqz v1, :cond_13

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v2, p3, v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ad;->a(I)I

    move-result v3

    packed-switch v3, :pswitch_data_94

    :pswitch_25
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_13

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_13

    :pswitch_31
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/fe;

    iget-object v0, v0, Lcom/twitter/android/fe;->c:Landroid/content/Intent;

    if-eqz v0, :cond_13

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_13

    :pswitch_3f
    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v1, v3, v2}, Lcom/twitter/android/widget/ad;->a(II)I

    move-result v1

    invoke-virtual {v4}, Lcom/twitter/android/dz;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_89

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "user_id"

    invoke-virtual {v1, v2, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "type"

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v2, p4, p5}, Lcom/twitter/android/util/FriendshipCache;->d(J)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_73

    const-string v3, "friendship"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_73
    check-cast p2, Lcom/twitter/android/widget/UserView;

    invoke-virtual {p2}, Lcom/twitter/android/widget/UserView;->a()Lcom/twitter/android/api/PromotedContent;

    move-result-object v2

    if-eqz v2, :cond_84

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v2}, Lcom/twitter/android/client/b;->a(ILcom/twitter/android/api/PromotedContent;)V

    const-string v0, "pc"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_84
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_13

    :cond_89
    invoke-virtual {v4, v1}, Lcom/twitter/android/dz;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_13

    nop

    :pswitch_data_94
    .packed-switch 0x2
        :pswitch_31
        :pswitch_25
        :pswitch_3f
    .end packed-switch
.end method

.method public final a(Lcom/twitter/android/api/TweetEntities$Url;)V
    .registers 4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/api/TweetEntities$Url;->url:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    return-void
.end method

.method public final a(Lcom/twitter/android/api/TweetMedia;)V
    .registers 2

    return-void
.end method

.method public final a(Lcom/twitter/android/api/ad;)V
    .registers 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-object v0, v0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    if-eqz v0, :cond_1c

    iget-object v0, p1, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-object v0, v0, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    :cond_1c
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-object v0, v0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    if-eqz v0, :cond_4b

    iget-object v0, p1, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-object v0, v0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4b

    :cond_32
    move v0, v1

    :goto_33
    iget-wide v3, p1, Lcom/twitter/android/api/ad;->a:J

    iput-wide v3, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-wide v3, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v5}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_4d

    :goto_45
    iput-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Z)V

    return-void

    :cond_4b
    move v0, v2

    goto :goto_33

    :cond_4d
    move v1, v2

    goto :goto_45
.end method

.method public final a(Lcom/twitter/android/util/x;Ljava/util/HashMap;)V
    .registers 5

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/android/util/x;->h:I

    if-ne v0, v1, :cond_f

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->notifyDataSetChanged()V

    :cond_e
    :goto_e
    return-void

    :cond_f
    const/4 v0, 0x2

    iget v1, p1, Lcom/twitter/android/util/x;->h:I

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v0}, Lcom/twitter/android/dz;->notifyDataSetChanged()V

    goto :goto_e
.end method

.method public final synthetic a(Lcom/twitter/android/widget/BaseUserView;J)V
    .registers 7

    check-cast p1, Lcom/twitter/android/widget/UserView;

    invoke-virtual {p1}, Lcom/twitter/android/widget/UserView;->a()Lcom/twitter/android/api/PromotedContent;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/widget/UserView;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_28

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->R:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, p2, p3, v0}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    :cond_1d
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/util/FriendshipCache;->c(J)V

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->ap:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    :goto_27
    return-void

    :cond_28
    if-eqz v0, :cond_3b

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p3, v2, v0}, Lcom/twitter/android/client/b;->a(JZLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    :goto_30
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/util/FriendshipCache;->b(J)V

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->ao:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_27

    :cond_3b
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->R:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_30
.end method

.method public final a(Ljava/lang/String;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;)V

    return-void
.end method

.method final a(ZI)V
    .registers 15

    const v8, 0x7f02004a

    const/4 v11, 0x1

    const/16 v10, 0x8

    const/4 v7, 0x0

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->Z:Lcom/twitter/android/widget/ShadowTextView;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->ac:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->ad:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/ShadowTextView;->setEnabled(Z)V

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    if-lez p2, :cond_2e

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/ShadowTextView;->setText(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    packed-switch p2, :pswitch_data_80

    :goto_24
    const v6, 0x7f0c0004

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v5, v9, v5, v9}, Lcom/twitter/android/widget/ShadowTextView;->setPadding(IIII)V

    :cond_2e
    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v7}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-eqz v5, :cond_61

    invoke-virtual {v1, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_3d
    return-void

    :pswitch_3e
    invoke-virtual {v0, v9}, Lcom/twitter/android/widget/ShadowTextView;->setChecked(Z)V

    const v6, 0x7f0200e3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6, v7, v7, v7}, Lcom/twitter/android/widget/ShadowTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/ShadowTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_24

    :pswitch_53
    invoke-virtual {v0, v11}, Lcom/twitter/android/widget/ShadowTextView;->setChecked(Z)V

    invoke-virtual {v0, v7, v7, v7, v7}, Lcom/twitter/android/widget/ShadowTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/ShadowTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_24

    :cond_61
    invoke-virtual {v0, v10}, Lcom/twitter/android/widget/ShadowTextView;->setVisibility(I)V

    invoke-virtual {v1, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {v2, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/platform/j;->a(Landroid/content/Context;)I

    move-result v0

    if-le v0, v11, :cond_7b

    invoke-virtual {v3, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3d

    :cond_7b
    invoke-virtual {v3, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3d

    nop

    :pswitch_data_80
    .packed-switch 0x7f0b010e
        :pswitch_3e
        :pswitch_53
    .end packed-switch
.end method

.method public final d(Ljava/lang/String;)V
    .registers 2

    return-void
.end method

.method public final d_()V
    .registers 1

    return-void
.end method

.method protected final e(I)Landroid/app/Dialog;
    .registers 10

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_aa

    const/4 v0, 0x0

    :goto_6
    return-object v0

    :pswitch_7
    new-instance v7, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget v3, p0, Lcom/twitter/android/ProfileFragment;->v:I

    and-int/lit8 v2, v3, 0x8

    if-nez v2, :cond_17

    and-int/lit8 v2, v3, 0x2

    if-eqz v2, :cond_8b

    :cond_17
    move v2, v1

    :goto_18
    iget-boolean v4, p0, Lcom/twitter/android/ProfileFragment;->V:Z

    if-eqz v4, :cond_8d

    and-int/lit8 v4, v3, 0x1

    if-eqz v4, :cond_8d

    move v4, v1

    :goto_21
    and-int/lit8 v5, v3, 0x10

    if-eqz v5, :cond_8f

    move v5, v1

    :goto_26
    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_91

    move v6, v1

    :goto_2b
    if-eqz v2, :cond_37

    const v0, 0x7f0b0012

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_37
    if-eqz v4, :cond_45

    if-eqz v5, :cond_93

    const v0, 0x7f0b007e

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_45
    :goto_45
    const v0, 0x7f0b011e

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v6, :cond_9e

    const v0, 0x7f0b006d

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_5b
    const v0, 0x7f0b011d

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v0, Lcom/twitter/android/do;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/do;-><init>(Lcom/twitter/android/ProfileFragment;ZLandroid/content/Context;ZZZ)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_6

    :cond_8b
    move v2, v0

    goto :goto_18

    :cond_8d
    move v4, v0

    goto :goto_21

    :cond_8f
    move v5, v0

    goto :goto_26

    :cond_91
    move v6, v0

    goto :goto_2b

    :cond_93
    const v0, 0x7f0b007b

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_45

    :cond_9e
    const v0, 0x7f0b006e

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5b

    nop

    :pswitch_data_aa
    .packed-switch 0x5
        :pswitch_7
    .end packed-switch
.end method

.method public final g(I)V
    .registers 5

    packed-switch p1, :pswitch_data_70

    :goto_3
    return-void

    :pswitch_4
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0078

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0079

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    :goto_17
    const v1, 0x7f0b00ed

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0b00ee

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_3

    :pswitch_34
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b006d

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b012e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    goto :goto_17

    :pswitch_48
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b006e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b006f

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    goto :goto_17

    :pswitch_5c
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b011d

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->c(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0b0072

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    goto :goto_17

    :pswitch_data_70
    .packed-switch 0x1
        :pswitch_4
        :pswitch_48
        :pswitch_34
        :pswitch_5c
    .end packed-switch
.end method

.method protected final j()Z
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v1

    sub-int/2addr v0, v1

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method final l()V
    .registers 4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method final m()V
    .registers 4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final n()I
    .registers 2

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->v:I

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 8

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->o()V

    new-instance v0, Lcom/twitter/android/dv;

    invoke-direct {v0, p0}, Lcom/twitter/android/dv;-><init>(Lcom/twitter/android/ProfileFragment;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->d:Lcom/twitter/android/client/j;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_21

    invoke-direct {p0, v5}, Lcom/twitter/android/ProfileFragment;->b(Z)V

    :cond_15
    :goto_15
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->P:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_20
    return-void

    :cond_21
    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_15

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_3c

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->d()Lcom/twitter/android/api/ad;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/api/ad;)V

    :cond_3c
    invoke-direct {p0, v5}, Lcom/twitter/android/ProfileFragment;->c(Z)V

    goto :goto_15
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 12

    const-wide/16 v6, 0x0

    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/BaseListFragment;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p2, :cond_d

    :cond_c
    :goto_c
    return-void

    :cond_d
    packed-switch p1, :pswitch_data_b2

    goto :goto_c

    :pswitch_11
    const-string v0, "list_id"

    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const/4 v1, 0x4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/b;->a(IJJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    goto :goto_c

    :pswitch_28
    if-ne v0, p2, :cond_c

    if-eqz p3, :cond_c

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-lez v2, :cond_c

    const-string v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "friendship"

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_c

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v0}, Lcom/twitter/android/dz;->notifyDataSetChanged()V

    goto :goto_c

    :pswitch_55
    if-ne v0, p2, :cond_c

    if-eqz p3, :cond_c

    invoke-direct {p0, v3}, Lcom/twitter/android/ProfileFragment;->b(Z)V

    const-string v0, "update_header"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6f

    new-instance v0, Lcom/twitter/android/du;

    invoke-direct {v0, p0}, Lcom/twitter/android/du;-><init>(Lcom/twitter/android/ProfileFragment;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/du;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_c

    :cond_6f
    const-string v0, "remove_header"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iput-object v1, v0, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    new-instance v0, Lcom/twitter/android/util/f;

    invoke-direct {v0, v1}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->X:Lcom/twitter/android/util/f;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    const v1, 0x7f02001e

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundResource(I)V

    goto :goto_c

    :pswitch_8b
    const/4 v0, 0x1

    if-ne p2, v0, :cond_c

    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gl;

    iget-object v0, v0, Lcom/twitter/android/gl;->a:Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/e;->a(Landroid/accounts/Account;)V

    goto/16 :goto_c

    nop

    :pswitch_data_b2
    .packed-switch 0x1
        :pswitch_11
        :pswitch_28
        :pswitch_8b
        :pswitch_55
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 3

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onAttach(Landroid/app/Activity;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->as:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-nez v0, :cond_7

    :goto_6
    return-void

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v1

    if-nez v1, :cond_1b

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_6

    :cond_1b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_112

    goto :goto_6

    :sswitch_23
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TimelineActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    const v2, 0x7f0b002b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_6

    :sswitch_49
    iget v1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_53

    invoke-virtual {p0, v4}, Lcom/twitter/android/ProfileFragment;->g(I)V

    goto :goto_6

    :cond_53
    iget v1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-static {v1, v4}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    const v1, 0x7f0b010f

    invoke-virtual {p0, v4, v1}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v5, v3}, Lcom/twitter/android/client/b;->a(JZLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->am:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    goto :goto_6

    :sswitch_72
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f(I)V

    goto :goto_6

    :sswitch_77
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/MessagesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "force_refresh"

    iget-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->an:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_6

    :sswitch_8f
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account_name"

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_6

    :sswitch_ae
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f080005

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v5}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_6

    :sswitch_ca
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_6

    :sswitch_ee
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "type"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_6

    :sswitch_data_112
    .sparse-switch
        0x7f0700ba -> :sswitch_23
        0x7f0700bb -> :sswitch_ca
        0x7f0700bc -> :sswitch_ee
        0x7f0700dd -> :sswitch_72
        0x7f0700de -> :sswitch_49
        0x7f0700df -> :sswitch_77
        0x7f0700e0 -> :sswitch_ae
        0x7f0700e1 -> :sswitch_8f
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 10

    const/4 v6, 0x6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    const-string v0, "profile_prefs"

    invoke-virtual {v3, v0, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->p:Landroid/content/SharedPreferences;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->R:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v0, "pc"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/api/PromotedContent;

    if-eqz p1, :cond_8c

    const-string v0, "state_dialog_text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->Q:Ljava/lang/String;

    const-string v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_84

    const-string v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    :goto_45
    const-string v0, "state_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/ad;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    const-string v0, "header_page"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->P:I

    const-string v0, "state_re"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->T:I

    :goto_5f
    invoke-static {v3}, Lcom/twitter/android/platform/C2DMService;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->V:Z

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_a4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-wide v5, v0, Lcom/twitter/android/api/ad;->a:J

    iput-wide v5, p0, Lcom/twitter/android/ProfileFragment;->q:J

    :goto_6f
    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v3

    cmp-long v0, v5, v3

    if-nez v0, :cond_af

    move v0, v1

    :goto_7a
    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    invoke-virtual {p0, v1, p0}, Lcom/twitter/android/ProfileFragment;->a(ILcom/twitter/android/util/y;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ProfileFragment;->a(ILcom/twitter/android/util/y;)V

    return-void

    :cond_84
    new-instance v0, Lcom/twitter/android/util/FriendshipCache;

    invoke-direct {v0, v6}, Lcom/twitter/android/util/FriendshipCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    goto :goto_45

    :cond_8c
    const-string v0, "user"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/ad;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    new-instance v0, Lcom/twitter/android/util/FriendshipCache;

    invoke-direct {v0, v6}, Lcom/twitter/android/util/FriendshipCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    iput v2, p0, Lcom/twitter/android/ProfileFragment;->P:I

    iput v2, p0, Lcom/twitter/android/ProfileFragment;->T:I

    iput-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->U:Z

    goto :goto_5f

    :cond_a4
    const-string v0, "user_id"

    const-wide/16 v6, -0x1

    invoke-virtual {v5, v0, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/twitter/android/ProfileFragment;->q:J

    goto :goto_6f

    :cond_af
    move v0, v2

    goto :goto_7a
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 15

    const/4 v7, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_138

    move-object v0, v4

    :goto_6
    return-object v0

    :pswitch_7
    sget-object v0, Lcom/twitter/android/provider/ad;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ownerId"

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/u;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/ds;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/u;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :pswitch_39
    sget-object v0, Lcom/twitter/android/provider/y;->b:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v0, "limit"

    const-string v1, "3"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ownerId"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0, v7}, Lcom/twitter/android/dy;->a(Z)V

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/provider/m;->c:[Ljava/lang/String;

    const-string v6, "updated_at DESC, _id ASC"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :pswitch_74
    sget-object v0, Lcom/twitter/android/provider/ac;->b:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const-string v2, "3"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ownerId"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v1}, Lcom/twitter/android/util/FriendshipCache;->a()Z

    move-result v1

    if-eqz v1, :cond_bf

    const-string v9, "friendship IS NULL OR friendship NOT IN (1,3,9,10,11) AND user_id!=?"

    new-array v10, v7, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v1

    :goto_ac
    new-instance v5, Lcom/twitter/android/u;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/twitter/android/provider/bd;->b:[Ljava/lang/String;

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/twitter/android/u;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto/16 :goto_6

    :cond_bf
    move-object v10, v4

    move-object v9, v4

    goto :goto_ac

    :pswitch_c2
    sget-object v0, Lcom/twitter/android/provider/z;->t:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ownerId"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    new-instance v5, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/twitter/android/fo;->a:[Ljava/lang/String;

    const-string v9, "cards NOT NULL AND flags&1 != 0"

    const-string v11, "updated_at DESC, _id ASC"

    move-object v10, v4

    invoke-direct/range {v5 .. v11}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto/16 :goto_6

    :pswitch_100
    sget-object v0, Lcom/twitter/android/provider/ab;->r:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v1, v2}, Lcom/twitter/android/provider/o;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/provider/bd;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :pswitch_11d
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/e;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v3}, Lcom/twitter/android/client/b;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/provider/b;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :pswitch_data_138
    .packed-switch 0x1
        :pswitch_7
        :pswitch_39
        :pswitch_74
        :pswitch_c2
        :pswitch_11d
        :pswitch_100
    .end packed-switch
.end method

.method public onDestroy()V
    .registers 2

    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ProfileFragment;->b(ILcom/twitter/android/util/y;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ProfileFragment;->b(ILcom/twitter/android/util/y;)V

    return-void
.end method

.method public onDetach()V
    .registers 2

    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onDetach()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->as:Z

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 11

    sget-object v0, Lcom/twitter/android/provider/y;->c:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ownerId"

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "prj"

    sget-object v3, Lcom/twitter/android/provider/m;->c:[Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "id"

    invoke-virtual {v0, v2, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "sel"

    const-string v3, "cards NOT NULL AND flags&1 != 0"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_28

    :goto_8
    return-void

    :pswitch_9
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/dy;

    invoke-virtual {v0}, Lcom/twitter/android/dy;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gd;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_8

    :pswitch_15
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/dz;

    invoke-virtual {v0}, Lcom/twitter/android/dz;->c()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/el;

    invoke-virtual {v0, v1}, Lcom/twitter/android/el;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_8

    :pswitch_21
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/fo;

    invoke-virtual {v0, v1}, Lcom/twitter/android/fo;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_8

    nop

    :pswitch_data_28
    .packed-switch 0x2
        :pswitch_9
        :pswitch_15
        :pswitch_21
    .end packed-switch
.end method

.method public onResume()V
    .registers 9

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onResume()V

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-eqz v0, :cond_25

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v3, :cond_25

    invoke-virtual {v2}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v3

    if-eqz v3, :cond_25

    iget-wide v4, p0, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-virtual {v3}, Lcom/twitter/android/client/Session;->g()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5c

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->d:Lcom/twitter/android/client/j;

    invoke-virtual {v4, v3}, Lcom/twitter/android/client/j;->a(Lcom/twitter/android/client/Session;)V

    :cond_25
    :goto_25
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->i()Z

    move-result v3

    if-eqz v3, :cond_37

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->i()Z

    move-result v3

    if-nez v3, :cond_37

    iput v1, p0, Lcom/twitter/android/ProfileFragment;->v:I

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->l()V

    move v0, v1

    :cond_37
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(ZI)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-nez v0, :cond_42

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(I)V

    :cond_42
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->al:Z

    if-eqz v0, :cond_5b

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->q:J

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->aw:Lcom/twitter/android/service/ScribeEvent;

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_5b

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    iget-boolean v0, v0, Lcom/twitter/android/api/ad;->h:Z

    if-eqz v0, :cond_5b

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->m()V

    :cond_5b
    return-void

    :cond_5c
    invoke-virtual {v2}, Lcom/twitter/android/client/b;->d()Lcom/twitter/android/api/ad;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/api/ad;)V

    goto :goto_25
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "state_dialog_text"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "state_re"

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->T:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/android/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, "state_friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/android/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_20
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_2b

    const-string v0, "state_user"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/android/api/ad;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2b
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_3a

    const-string v0, "header_page"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3a
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    if-eqz v0, :cond_49

    const-string v0, "state_pfs"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    invoke-virtual {v1}, Lcom/twitter/android/widget/ProfileHeader;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_49
    return-void
.end method

.method public onStop()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->R:Ljava/util/HashSet;

    invoke-static {v0}, Lcom/twitter/android/util/z;->b(Ljava/util/Collection;)[J

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/b;->b([J)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->R:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    :cond_12
    invoke-super {p0}, Lcom/twitter/android/BaseListFragment;->onStop()V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 9

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v0, 0x7f03006d

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const v1, 0x7f0700dc

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->W:Landroid/widget/LinearLayout;

    const v2, 0x7f0700de

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/ShadowTextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->Z:Lcom/twitter/android/widget/ShadowTextView;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->Z:Lcom/twitter/android/widget/ShadowTextView;

    invoke-virtual {v2, p0}, Lcom/twitter/android/widget/ShadowTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0700dd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0700df

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0700e1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->ac:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->ac:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0700e0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->ad:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->ad:Landroid/widget/ImageButton;

    invoke-virtual {v2, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->ai:Landroid/widget/RelativeLayout;

    const v1, 0x7f07006d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->ae:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const v2, 0x7f0700d9

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/ProfileHeader;

    if-eqz p2, :cond_a0

    const-string v3, "state_pfs"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a0

    const-string v3, "state_pfs"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_a0
    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/android/widget/ProfileHeader;

    const v3, 0x7f070085

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_107

    new-instance v5, Lcom/twitter/android/dt;

    invoke-direct {v5, p0, v4}, Lcom/twitter/android/dt;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/view/LayoutInflater;)V

    const v4, 0x7f0700e2

    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/twitter/android/ProfileFragment;->L:Landroid/widget/ImageView;

    const v4, 0x7f0700e3

    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->M:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    invoke-virtual {v3, v5}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    new-instance v4, Lcom/twitter/android/dm;

    invoke-direct {v4, p0, v2}, Lcom/twitter/android/dm;-><init>(Lcom/twitter/android/ProfileFragment;I)V

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    new-instance v2, Lcom/twitter/android/dn;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/dn;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/support/v4/view/ViewPager;)V

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->M:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v5, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/android/dt;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/support/v4/view/ViewPager;

    :goto_f2
    new-instance v3, Lcom/twitter/android/e;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-direct {v3, v4, v2, p0}, Lcom/twitter/android/e;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/twitter/android/f;)V

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/e;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    return-void

    :cond_107
    const v3, 0x7f07007a

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->D:Landroid/widget/ImageView;

    const v3, 0x7f070028

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->E:Landroid/widget/TextView;

    const v3, 0x7f07006c

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->H:Landroid/widget/ImageView;

    const v3, 0x7f070027

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->F:Landroid/widget/TextView;

    const v3, 0x7f0700e5

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->G:Landroid/widget/TextView;

    const v3, 0x7f0700e6

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->I:Landroid/widget/TextView;

    const v3, 0x7f0700e7

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->J:Landroid/widget/TextView;

    const v3, 0x7f0700ea

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->N:Landroid/widget/TextView;

    const v3, 0x7f0700e8

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->K:Landroid/widget/TextView;

    goto :goto_f2
.end method
