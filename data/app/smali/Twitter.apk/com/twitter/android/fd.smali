.class final Lcom/twitter/android/fd;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private final a:[Lcom/twitter/android/fe;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/twitter/android/fe;)V
    .registers 4

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/twitter/android/fe;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-direct {p0, v0}, Lcom/twitter/android/fd;-><init>([Lcom/twitter/android/fe;)V

    return-void
.end method

.method constructor <init>([Lcom/twitter/android/fe;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/fd;-><init>([Lcom/twitter/android/fe;I)V

    return-void
.end method

.method constructor <init>([Lcom/twitter/android/fe;I)V
    .registers 3

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/fd;->a:[Lcom/twitter/android/fe;

    iput p2, p0, Lcom/twitter/android/fd;->b:I

    return-void
.end method


# virtual methods
.method public final a(II)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/fd;->a:[Lcom/twitter/android/fe;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/twitter/android/fe;->d:I

    invoke-virtual {p0}, Lcom/twitter/android/fd;->notifyDataSetChanged()V

    return-void
.end method

.method public final getCount()I
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/fd;->a:[Lcom/twitter/android/fe;

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/fd;->a:[Lcom/twitter/android/fe;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/twitter/android/fe;->c:Landroid/content/Intent;

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/fd;->a:[Lcom/twitter/android/fe;

    aget-object v0, v0, p1

    invoke-static {p2, p3, v0}, Lcom/twitter/android/fg;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/fe;)Landroid/view/View;

    move-result-object v1

    iget v0, p0, Lcom/twitter/android/fd;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_18

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    iget-object v2, p0, Lcom/twitter/android/fd;->a:[Lcom/twitter/android/fe;

    array-length v2, v2

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/widget/CardRowView;->b(II)V

    :goto_17
    return-object v1

    :cond_18
    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/CardRowView;

    invoke-virtual {p0}, Lcom/twitter/android/fd;->getCount()I

    move-result v2

    invoke-virtual {v0, p1, v2}, Lcom/twitter/android/widget/CardRowView;->a(II)V

    goto :goto_17
.end method
