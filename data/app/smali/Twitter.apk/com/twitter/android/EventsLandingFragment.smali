.class public Lcom/twitter/android/EventsLandingFragment;
.super Lcom/twitter/android/TweetListFragment;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/fn;
.implements Lcom/twitter/android/widget/a;
.implements Lcom/twitter/android/widget/av;


# instance fields
.field private m:Lcom/twitter/android/widget/ad;

.field private n:[Landroid/widget/BaseAdapter;

.field private o:Lcom/twitter/android/ao;

.field private p:Lcom/twitter/android/api/ad;

.field private q:Ljava/lang/String;

.field private r:I

.field private s:Lcom/twitter/android/fo;

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    iput-boolean v1, p0, Lcom/twitter/android/EventsLandingFragment;->t:Z

    iput-boolean v1, p0, Lcom/twitter/android/EventsLandingFragment;->u:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventsLandingFragment;I)I
    .registers 2

    iput p1, p0, Lcom/twitter/android/EventsLandingFragment;->r:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/ao;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->o:Lcom/twitter/android/ao;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/EventsLandingFragment;Lcom/twitter/android/api/ad;)Lcom/twitter/android/api/ad;
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/EventsLandingFragment;Lcom/twitter/android/service/ScribeEvent;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    return-void
.end method

.method private a(Lcom/twitter/android/service/ScribeEvent;)V
    .registers 6

    new-instance v0, Lcom/twitter/android/service/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->q:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/android/util/z;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/service/ScribeLog;->query:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->c:Lcom/twitter/android/client/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventsLandingFragment;Z)Z
    .registers 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->u:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/EventsLandingFragment;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->t:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/EventsLandingFragment;Z)Z
    .registers 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->t:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/EventsLandingFragment;)Lcom/twitter/android/fo;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->s:Lcom/twitter/android/fo;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/EventsLandingFragment;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->u:Z

    return v0
.end method

.method private g(I)J
    .registers 7

    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_4a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid fetch type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1a
    iget-object v2, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_42

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_42

    :cond_28
    const/16 v3, 0x14

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_43

    const/4 v4, 0x6

    if-eq v3, v4, :cond_43

    const/16 v3, 0x1f

    invoke-interface {v2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_43

    const/16 v0, 0x17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_42
    :goto_42
    :pswitch_42
    return-wide v0

    :cond_43
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_28

    goto :goto_42

    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_42
        :pswitch_1a
        :pswitch_42
    .end packed-switch
.end method

.method private h(I)J
    .registers 6

    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_30

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid fetch type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1a
    iget-object v2, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2e

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_2e

    const/16 v0, 0x17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_2e
    :pswitch_2e
    return-wide v0

    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_2e
        :pswitch_2e
    .end packed-switch
.end method

.method private i(I)V
    .registers 12

    const/16 v4, 0x64

    const/4 v8, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->a(I)Z

    move-result v1

    if-eqz v1, :cond_b

    :goto_a
    return-void

    :cond_b
    packed-switch p1, :pswitch_data_62

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid fetch type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_23
    move v7, v8

    :goto_24
    iput-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->t:Z

    iput-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->u:Z

    invoke-virtual {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->q:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->g(I)J

    move-result-wide v2

    invoke-direct {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->h(I)J

    move-result-wide v5

    move v9, v8

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;JIJIZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/EventsLandingFragment;->a(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->c:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->q:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->g(I)J

    move-result-wide v2

    invoke-direct {p0, p1}, Lcom/twitter/android/EventsLandingFragment;->h(I)J

    move-result-wide v5

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;JIJI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/EventsLandingFragment;->a(Ljava/lang/String;I)V

    goto :goto_a

    :pswitch_53
    sget-object v1, Lcom/twitter/android/service/ScribeEvent;->A:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v1}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    move v7, v0

    goto :goto_24

    :pswitch_5a
    sget-object v1, Lcom/twitter/android/service/ScribeEvent;->B:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v1}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    iget v7, p0, Lcom/twitter/android/EventsLandingFragment;->r:I

    goto :goto_24

    :pswitch_data_62
    .packed-switch 0x1
        :pswitch_5a
        :pswitch_53
        :pswitch_23
    .end packed-switch
.end method


# virtual methods
.method public final a(J)I
    .registers 6

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(J)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    iget-object v2, p0, Lcom/twitter/android/EventsLandingFragment;->n:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ad;->b(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected final a()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0}, Lcom/twitter/android/gd;->d()V

    :cond_b
    return-void
.end method

.method protected final a(Landroid/database/Cursor;)V
    .registers 4

    iget-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->b:Z

    if-eqz v0, :cond_10

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_10

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->i(I)V

    :cond_10
    return-void
.end method

.method public final a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 4

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_16

    :goto_7
    return-void

    :pswitch_8
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->c(I)V

    goto :goto_7

    :pswitch_10
    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->s:Lcom/twitter/android/fo;

    invoke-virtual {v0, p2}, Lcom/twitter/android/fo;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_7

    :pswitch_data_16
    .packed-switch 0x0
        :pswitch_8
        :pswitch_10
    .end packed-switch
.end method

.method public final a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 10

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    if-nez v0, :cond_5

    :cond_4
    :goto_4
    :pswitch_4
    return-void

    :cond_5
    if-eqz p3, :cond_4

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/ad;->a(I)I

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/EventsLandingFragment;->a:Z

    if-eqz v1, :cond_19

    add-int/lit8 v0, v0, -0x1

    :cond_19
    packed-switch v0, :pswitch_data_42

    goto :goto_4

    :pswitch_1d
    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->I:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "tw"

    new-instance v3, Lcom/twitter/android/provider/m;

    invoke-direct {v3, v0}, Lcom/twitter/android/provider/m;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_1d
    .end packed-switch
.end method

.method public final bridge synthetic a(Lcom/twitter/android/widget/BaseUserView;J)V
    .registers 4

    return-void
.end method

.method protected final a_()V
    .registers 2

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->i(I)V

    return-void
.end method

.method protected final d()I
    .registers 2

    const/16 v0, 0x17

    return v0
.end method

.method public final d_()V
    .registers 1

    return-void
.end method

.method public final f()Lcom/twitter/android/widget/m;
    .registers 8

    iget-object v3, p0, Lcom/twitter/android/EventsLandingFragment;->j:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->n:[Landroid/widget/BaseAdapter;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ad;->b(I)I

    move-result v4

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    add-int v1, v0, v4

    iget-boolean v0, p0, Lcom/twitter/android/EventsLandingFragment;->a:Z

    if-eqz v0, :cond_1d

    add-int/lit8 v1, v1, 0x1

    :cond_1d
    if-ge v2, v1, :cond_5c

    move v0, v1

    :goto_20
    iget-object v5, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v5}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v5

    sub-int v4, v0, v4

    if-eqz v5, :cond_46

    invoke-interface {v5, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_46

    :cond_30
    const/16 v4, 0x1f

    invoke-interface {v5, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_5e

    const/16 v4, 0x14

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v6, 0x6

    if-eq v4, v6, :cond_5e

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    add-int/2addr v0, v1

    :cond_46
    :goto_46
    sub-int v1, v0, v2

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/m;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v3

    if-eqz v1, :cond_65

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_58
    invoke-direct {v2, v0, v3, v4, v1}, Lcom/twitter/android/widget/m;-><init>(IJI)V

    return-object v2

    :cond_5c
    move v0, v2

    goto :goto_20

    :cond_5e
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_30

    goto :goto_46

    :cond_65
    const/4 v1, 0x0

    goto :goto_58
.end method

.method protected final g()V
    .registers 4

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->g()V

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 10

    const/4 v7, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    if-nez v0, :cond_67

    iget-object v5, p0, Lcom/twitter/android/EventsLandingFragment;->c:Lcom/twitter/android/client/b;

    new-instance v4, Lcom/twitter/android/bk;

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->g:Lcom/twitter/android/bg;

    invoke-direct {v4, v5, v0}, Lcom/twitter/android/bk;-><init>(Lcom/twitter/android/client/b;Lcom/twitter/android/bg;)V

    new-instance v0, Lcom/twitter/android/gd;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/gd;-><init>(Landroid/content/Context;IZLcom/twitter/android/widget/az;Lcom/twitter/android/client/b;Lcom/twitter/android/widget/av;)V

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/gd;

    invoke-virtual {v0, p0}, Lcom/twitter/android/gd;->a(Lcom/twitter/android/cl;)V

    new-instance v0, Lcom/twitter/android/ao;

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/ao;-><init>(Landroid/content/Context;Lcom/twitter/android/client/b;)V

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->o:Lcom/twitter/android/ao;

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_38

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->o:Lcom/twitter/android/ao;

    iget-object v4, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    invoke-virtual {v0, v4}, Lcom/twitter/android/ao;->a(Lcom/twitter/android/api/ad;)V

    :cond_38
    new-instance v0, Lcom/twitter/android/fo;

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/fo;-><init>(Landroid/content/Context;Lcom/twitter/android/client/b;)V

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->s:Lcom/twitter/android/fo;

    new-instance v0, Lcom/twitter/android/cq;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->s:Lcom/twitter/android/fo;

    invoke-direct {v0, v1}, Lcom/twitter/android/cq;-><init>(Lcom/twitter/android/fo;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/cq;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-array v1, v7, [Landroid/widget/BaseAdapter;

    iget-object v4, p0, Lcom/twitter/android/EventsLandingFragment;->o:Lcom/twitter/android/ao;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->e:Landroid/support/v4/widget/CursorAdapter;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->n:[Landroid/widget/BaseAdapter;

    new-instance v0, Lcom/twitter/android/widget/ad;

    new-array v1, v7, [I

    fill-array-data v1, :array_70

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/EventsLandingFragment;->n:[Landroid/widget/BaseAdapter;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/widget/ad;-><init>([I[I[Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    :cond_67
    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->j:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->m:Lcom/twitter/android/widget/ad;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    nop

    :array_70
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/ap;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ap;-><init>(Lcom/twitter/android/EventsLandingFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->d:Lcom/twitter/android/client/j;

    if-eqz p1, :cond_1a

    const-string v0, "mediator"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/ad;

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    :cond_1a
    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "event_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->q:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/EventsLandingFragment;->a(ILcom/twitter/android/util/y;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/EventsLandingFragment;->a(ILcom/twitter/android/util/y;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 15

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_5c

    move-object v0, v4

    :goto_5
    return-object v0

    :pswitch_6
    new-instance v0, Lcom/twitter/android/u;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/z;->p:Landroid/net/Uri;

    iget-wide v5, p0, Lcom/twitter/android/EventsLandingFragment;->h:J

    invoke-static {v2, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    iget-wide v5, p0, Lcom/twitter/android/EventsLandingFragment;->h:J

    invoke-static {v2, v5, v6}, Lcom/twitter/android/provider/o;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/provider/m;->b:[Ljava/lang/String;

    const-string v6, "tag DESC, updated_at DESC, created DESC"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/u;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :pswitch_23
    sget-object v0, Lcom/twitter/android/provider/z;->u:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/EventsLandingFragment;->h:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "limit"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ownerId"

    iget-wide v5, p0, Lcom/twitter/android/EventsLandingFragment;->h:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    new-instance v5, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/twitter/android/fo;->a:[Ljava/lang/String;

    const-string v9, "cards NOT NULL AND flags&1 != 0"

    const-string v11, "updated_at DESC, _id ASC"

    move-object v10, v4

    invoke-direct/range {v5 .. v11}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto :goto_5

    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_6
        :pswitch_23
    .end packed-switch
.end method

.method public onDestroy()V
    .registers 2

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/EventsLandingFragment;->b(ILcom/twitter/android/util/y;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/EventsLandingFragment;->b(ILcom/twitter/android/util/y;)V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 11

    sget-object v0, Lcom/twitter/android/provider/y;->d:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/EventsLandingFragment;->h:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ownerId"

    iget-wide v2, p0, Lcom/twitter/android/EventsLandingFragment;->h:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "prj"

    sget-object v3, Lcom/twitter/android/provider/m;->c:[Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "id"

    invoke-virtual {v0, v2, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "sel"

    const-string v3, "cards NOT NULL AND flags&1 != 0"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->D:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->a(Lcom/twitter/android/service/ScribeEvent;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/EventsLandingFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .registers 2

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lcom/twitter/android/EventsLandingFragment;->g()V

    :goto_a
    return-void

    :cond_b
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/android/EventsLandingFragment;->i(I)V

    goto :goto_a
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "mediator"

    iget-object v1, p0, Lcom/twitter/android/EventsLandingFragment;->p:Lcom/twitter/android/api/ad;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
