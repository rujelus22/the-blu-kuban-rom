.class final Lcom/twitter/android/du;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;

.field private final b:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .registers 4

    iput-object p1, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/du;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 10

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/du;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_d

    move-object v0, v1

    :cond_c
    :goto_c
    return-object v0

    :cond_d
    iget-object v2, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, v2, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/client/b;

    iget-object v3, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v3}, Lcom/twitter/android/ProfileFragment;->f(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/b;->i(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/32 v6, 0x927c0

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_35

    iget-object v1, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v4, v1, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v4, v5}, Lcom/twitter/android/util/z;->a(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_c

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/b;->k(Ljava/lang/String;)V

    goto :goto_c

    :cond_35
    invoke-virtual {v2, v3}, Lcom/twitter/android/client/b;->k(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->q:J

    invoke-static {v0, v2, v3}, Lcom/twitter/android/util/z;->d(Landroid/content/Context;J)V

    move-object v0, v1

    goto :goto_c
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 6

    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/twitter/android/du;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->w(Lcom/twitter/android/ProfileFragment;)Z

    move-result v1

    if-eqz v1, :cond_14

    if-nez v0, :cond_15

    :cond_14
    :goto_14
    return-void

    :cond_15
    if-eqz p1, :cond_35

    iget-object v1, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    new-instance v2, Lcom/twitter/android/util/f;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Lcom/twitter/android/util/f;)Lcom/twitter/android/util/f;

    iget-object v1, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/widget/ProfileHeader;

    move-result-object v1

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v2, v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_14

    :cond_35
    iget-object v1, p0, Lcom/twitter/android/du;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;Landroid/content/res/Resources;)V

    goto :goto_14
.end method
