.class public Lcom/twitter/android/service/TwitterService;
.super Landroid/app/Service;


# static fields
.field private static final b:Z

.field private static final c:[Ljava/lang/String;

.field private static final d:Ljava/util/HashMap;


# instance fields
.field final a:Landroid/os/Handler;

.field private e:Ljava/util/concurrent/ExecutorService;

.field private f:Ljava/util/LinkedHashMap;

.field private g:Lcom/twitter/android/network/p;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sput-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "impression"

    aput-object v1, v0, v2

    const-string v1, "url_click"

    aput-object v1, v0, v3

    const-string v1, "profile_image_click"

    aput-object v1, v0, v4

    const-string v1, "screen_name_click"

    aput-object v1, v0, v5

    const-string v1, "hashtag_click"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "user_mention_click"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "view_details"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "contributed_by_name_click"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "click"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "dismiss"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "footer_profile"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "card_url_click"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/service/TwitterService;->c:[Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x54

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ACTION_RESTART_CONNECTION_MANAGER"

    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "REFRESH"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "MARK_TWEETS"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "MARK_MESSAGES"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "CLEAN"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "LOGOUT"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "USER_TIMELINE"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "CREATE_FAV"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DESTROY_FAV"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_FAVS"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "RATE_LIMIT"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_LISTS"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_USERS"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_BLOCKING"

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "FOLLOW"

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "UNFOLLOW"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_MESSAGES"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_SENT_MESSAGES"

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_UNREAD"

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "NEW_DM"

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DESTROY_DM"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SHORTEN_URL"

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_FRIENDSHIP"

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_USER"

    const/16 v2, 0x17

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "TWEET"

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "RETWEET"

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DESTROY_STATUS"

    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "CREATE_BLOCK"

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "REPORT_SPAM"

    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DESTROY_BLOCK"

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "CREATE_LIST"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "EDIT_LIST"

    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DELETE_LIST"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_LIST_TWEETS"

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_LIST_USERS"

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ADD_LIST_USER"

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "REMOVE_LIST_USER"

    const/16 v2, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "IS_LIST_USER"

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_TRENDS"

    const/16 v2, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SEARCH_TWEETS"

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SEARCH_USERS"

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "UPDATE_PROFILE"

    const/16 v2, 0x29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "REVERSE_GEO"

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "VERIFY"

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "LOGIN"

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "LOG"

    const/16 v2, 0x2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "HOME"

    const/16 v2, 0x2e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SAVE_DRAFT"

    const/16 v2, 0x2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DEL_DRAFT"

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SEND_ALL_DRAFTS"

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_SAVED_SEARCHES"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_SLUG"

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_SLUG_USERS"

    const/16 v2, 0x34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_LIST"

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "LOOKUP"

    const/16 v2, 0x36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SIGNUP"

    const/16 v2, 0x37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "FOLLOW_ALL"

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "CONFIG"

    const/16 v2, 0x39

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "UPDATE_SETTINGS"

    const/16 v2, 0x3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_SETTINGS"

    const/16 v2, 0x3b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DELETE_DMS"

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DECIDER"

    const/16 v2, 0x3d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SAVE_SEARCH"

    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DELETE_SEARCH"

    const/16 v2, 0x3f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ACTIVITY"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "FETCH_STORIES"

    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "RELATED_RESULTS"

    const/16 v2, 0x42

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_USER_RECOMMENDATIONS"

    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "EMAIL_AVAILABLE"

    const/16 v2, 0x44

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "USERNAME_AVAILABLE"

    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "SUGGEST_SCREEN_NAMES"

    const/16 v2, 0x53

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ACTIVITY_SUMMARY"

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ACTION_MARK_STORIES"

    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DISMISS"

    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ADD_USER_SUGGESTION"

    const/16 v2, 0x4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "TYPEAHEAD"

    const/16 v2, 0x4b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "UPDATE_FRIENDSHIP"

    const/16 v2, 0x4c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "CLEAR_NOTIFICATIONS"

    const/16 v2, 0x4d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ACTION_CHECK_FOR_UPDATE"

    const/16 v2, 0x3e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "FETCH_EVENT"

    const/16 v2, 0x4e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "GET_INCOMING_FRIENDSHIPS"

    const/16 v2, 0x4f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "ACCEPT_INCOMING_FRIENDSHIP"

    const/16 v2, 0x50

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "DENY_INCOMING_FRIENDSHIP"

    const/16 v2, 0x51

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    const-string v1, "FETCH_EVENT_MEDIA"

    const/16 v2, 0x52

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/twitter/android/service/i;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/service/i;-><init>(Lcom/twitter/android/service/TwitterService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/android/service/TwitterService;->a:Landroid/os/Handler;

    return-void
.end method

.method private a(Lcom/twitter/android/network/a;Lcom/twitter/android/provider/ae;J[Ljava/lang/String;[Ljava/lang/String;[JIJLjava/lang/String;Z)Landroid/util/Pair;
    .registers 31

    if-eqz p5, :cond_ea

    move-object/from16 v0, p5

    array-length v3, v0

    :goto_5
    if-eqz p6, :cond_ed

    move-object/from16 v0, p6

    array-length v11, v0

    :goto_a
    if-eqz p7, :cond_f0

    move-object/from16 v0, p7

    array-length v1, v0

    :goto_f
    add-int v2, v3, v11

    add-int/2addr v2, v1

    const/16 v4, 0x64

    invoke-static {v2, v4}, Lcom/twitter/android/network/p;->a(II)I

    move-result v17

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v2, 0x0

    move v14, v2

    move v4, v1

    move v5, v3

    move v1, v11

    move-object v2, v13

    :goto_20
    move/from16 v0, v17

    if-ge v14, v0, :cond_134

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "users"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "lookup"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0x64

    if-eqz p7, :cond_131

    if-lez v4, :cond_131

    const-string v2, "user_id"

    move-object/from16 v0, p7

    array-length v6, v0

    sub-int/2addr v6, v4

    const/16 v7, 0x64

    move-object/from16 v0, p7

    invoke-static {v3, v2, v0, v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    move-result v2

    sub-int/2addr v4, v2

    rsub-int/lit8 v2, v2, 0x64

    move v15, v4

    :goto_57
    if-eqz p5, :cond_13b

    if-lez v2, :cond_13b

    if-lez v5, :cond_13b

    const-string v4, "email"

    move-object/from16 v0, p5

    array-length v6, v0

    sub-int/2addr v6, v5

    move-object/from16 v0, p5

    invoke-static {v3, v4, v0, v6, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/String;II)I

    move-result v4

    sub-int/2addr v5, v4

    sub-int/2addr v2, v4

    move/from16 v16, v5

    :goto_6d
    if-eqz p6, :cond_138

    if-lez v2, :cond_138

    if-lez v1, :cond_138

    const-string v4, "phone"

    move-object/from16 v0, p6

    array-length v5, v0

    sub-int/2addr v5, v1

    move-object/from16 v0, p6

    invoke-static {v3, v4, v0, v5, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/String;II)I

    move-result v2

    sub-int/2addr v1, v2

    move v11, v1

    :goto_81
    const-string v1, "include_user_entities"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p1

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_136

    :try_start_a5
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz p12, :cond_10f

    if-eqz p7, :cond_10f

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4, v3}, Ljava/util/HashMap;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_c3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ad;

    iget-wide v5, v1, Lcom/twitter/android/api/ad;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d8
    .catch Ljava/io/IOException; {:try_start_a5 .. :try_end_d8} :catch_d9

    goto :goto_c3

    :catch_d9
    move-exception v1

    const/4 v2, 0x0

    iput v2, v13, Lcom/twitter/android/network/c;->b:I

    iput-object v1, v13, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v1, v13

    :goto_e0
    new-instance v2, Landroid/util/Pair;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    :cond_ea
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_ed
    const/4 v11, 0x0

    goto/16 :goto_a

    :cond_f0
    const/4 v1, 0x0

    goto/16 :goto_f

    :cond_f3
    :try_start_f3
    move-object/from16 v0, p7

    array-length v5, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_f8
    if-ge v3, v5, :cond_110

    aget-wide v6, p7, v3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ad;

    if-eqz v1, :cond_10b

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_10b
    .catch Ljava/io/IOException; {:try_start_f3 .. :try_end_10b} :catch_d9

    :cond_10b
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_f8

    :cond_10f
    move-object v2, v1

    :cond_110
    if-nez v14, :cond_12f

    const-string v8, "-1"

    :goto_114
    const/4 v10, 0x1

    move-object/from16 v1, p2

    move-wide/from16 v3, p3

    move/from16 v5, p8

    move-wide/from16 v6, p9

    move-object/from16 v9, p11

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    add-int/2addr v1, v12

    :goto_124
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v12, v1

    move v4, v15

    move/from16 v5, v16

    move v1, v11

    move-object v2, v13

    goto/16 :goto_20

    :cond_12f
    const/4 v8, 0x0

    goto :goto_114

    :cond_131
    move v15, v4

    goto/16 :goto_57

    :cond_134
    move-object v1, v2

    goto :goto_e0

    :cond_136
    move v1, v12

    goto :goto_124

    :cond_138
    move v11, v1

    goto/16 :goto_81

    :cond_13b
    move/from16 v16, v5

    goto/16 :goto_6d
.end method

.method private a(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 20

    const-string v1, "status_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "impression_id"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v1, "earned"

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    const/4 v6, 0x1

    move-object/from16 v1, p3

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(JJZ)V

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v6, "favorites"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    const-string v6, "create"

    aput-object v6, v2, v3

    const/4 v3, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "send_error_codes"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz v7, :cond_67

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "impression_id"

    invoke-direct {v2, v3, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v8, :cond_67

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "earned"

    const-string v6, "true"

    invoke-direct {v2, v3, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_67
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "include_entities"

    const-string v6, "true"

    invoke-direct {v2, v3, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "include_cards"

    const-string v6, "true"

    invoke-direct {v2, v3, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v9, p4

    invoke-static/range {v6 .. v11}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_cd

    :try_start_9e
    invoke-static {v11}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->e(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ab;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/twitter/android/api/ab;->l:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x2

    const-wide/16 v6, -0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v1, p3

    move-wide v3, p1

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I
    :try_end_bf
    .catch Ljava/io/IOException; {:try_start_9e .. :try_end_bf} :catch_c0

    :cond_bf
    :goto_bf
    return-object v13

    :catch_c0
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_bf

    const-string v2, "TwitterService"

    const-string v3, "Problem processing response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_bf

    :cond_cd
    const/4 v2, 0x1

    :try_start_ce
    invoke-static {v11}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->A(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_102

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_dc
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_102

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v6, "count"

    move-object/from16 v0, p5

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_f3
    .catch Ljava/io/IOException; {:try_start_ce .. :try_end_f3} :catch_104

    const/16 v6, 0x8b

    if-ne v1, v6, :cond_dc

    const/4 v1, 0x0

    :goto_f8
    if-eqz v1, :cond_bf

    const/4 v6, 0x0

    move-object/from16 v1, p3

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(JJZ)V

    goto :goto_bf

    :cond_102
    move v1, v2

    goto :goto_f8

    :catch_104
    move-exception v1

    sget-boolean v3, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v3, :cond_110

    const-string v3, "TwitterService"

    const-string v6, "Problem processing response."

    invoke-static {v3, v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_110
    move v1, v2

    goto :goto_f8
.end method

.method private a(Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 11

    const/4 v4, 0x0

    const-string v0, "email"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v2, v2, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "i"

    aput-object v5, v3, v4

    const/4 v5, 0x1

    const-string v6, "users"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "suggest_screen_names"

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_37

    const-string v3, "email"

    invoke-static {v2, v3, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_37
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_42

    const-string v0, "full_name"

    invoke-static {v2, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_42
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x400

    invoke-direct {v5, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-instance v3, Lcom/twitter/android/network/a;

    sget-object v6, Lcom/twitter/android/network/a;->o:Lcom/twitter/android/network/k;

    sget-object v7, Lcom/twitter/android/network/a;->m:Ljava/lang/String;

    sget-object v8, Lcom/twitter/android/network/a;->n:Ljava/lang/String;

    invoke-direct {v3, v6, v7, v8}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_75

    :try_start_68
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->y(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, "screen_name_suggestions"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_75} :catch_76

    :cond_75
    :goto_75
    return-object v1

    :catch_76
    move-exception v0

    iput v4, v1, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v1, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_75
.end method

.method private a(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 9

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "account"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "rate_limit_status"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_40

    :try_start_33
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    const-string v2, "rate_limit"

    invoke-static {v1}, Lcom/twitter/android/api/s;->i(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/i;

    move-result-object v1

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_40} :catch_41

    :cond_40
    :goto_40
    return-object v0

    :catch_41
    move-exception v1

    iput v4, v0, Lcom/twitter/android/network/c;->b:I

    goto :goto_40
.end method

.method private a(Lcom/twitter/android/network/a;Landroid/net/Uri;Landroid/graphics/Rect;)Lcom/twitter/android/network/c;
    .registers 11

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "account"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v4, "profile_banner"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p3, :cond_52

    iget v2, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    rem-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, p3, Landroid/graphics/Rect;->right:I

    iget v2, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, p3, Landroid/graphics/Rect;->bottom:I

    const-string v2, "offset_left"

    iget v3, p3, Landroid/graphics/Rect;->left:I

    invoke-static {v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v2, "offset_top"

    iget v3, p3, Landroid/graphics/Rect;->top:I

    invoke-static {v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v2, "width"

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v2, "height"

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_52
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/android/network/j;

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v4

    invoke-direct {v1, v4, v3, p1, v2}, Lcom/twitter/android/network/j;-><init>(Lcom/twitter/android/network/d;Lorg/apache/http/client/methods/HttpPost;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)V

    invoke-static {p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/network/p;->a(Lcom/twitter/android/network/c;)V

    :try_start_72
    invoke-virtual {p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_79
    .catchall {:try_start_72 .. :try_end_79} :catchall_a1
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_79} :catch_99

    move-result-object v0

    :try_start_7a
    new-instance v2, Lcom/twitter/android/network/g;

    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/twitter/android/network/g;-><init>(Lcom/twitter/android/network/m;)V

    const-string v4, "banner"

    const/16 v5, 0x8

    invoke-static {v5}, Lcom/twitter/android/util/z;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5, v0}, Lcom/twitter/android/network/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)I

    invoke-virtual {v2}, Lcom/twitter/android/network/g;->a()V

    invoke-virtual {v3, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;
    :try_end_95
    .catchall {:try_start_7a .. :try_end_95} :catchall_a9
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_95} :catch_99

    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    :goto_98
    return-object v1

    :catch_99
    move-exception v2

    const/4 v2, 0x0

    :try_start_9b
    iput v2, v1, Lcom/twitter/android/network/c;->b:I
    :try_end_9d
    .catchall {:try_start_9b .. :try_end_9d} :catchall_a9

    invoke-static {v0}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    goto :goto_98

    :catchall_a1
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    :goto_a5
    invoke-static {v1}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_a9
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_a5
.end method

.method private a(Lcom/twitter/android/network/a;Ljava/lang/String;JLjava/io/ByteArrayOutputStream;)Lcom/twitter/android/network/c;
    .registers 12

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v0, v0, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "1.1"

    aput-object v2, v1, v4

    const-string v2, "users"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string v3, "show"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-nez v0, :cond_41

    const-string v0, "screen_name"

    invoke-static {v2, v0, p2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_2b
    const-string v0, "include_user_entities"

    invoke-static {v2, v0, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p1

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    return-object v0

    :cond_41
    const-string v0, "user_id"

    invoke-static {v2, v0, p3, p4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto :goto_2b
.end method

.method private a(Lcom/twitter/android/provider/ae;ILcom/twitter/android/network/a;JJJILandroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 23

    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-eqz v2, :cond_102

    const/4 v3, 0x4

    const/4 v7, 0x1

    move-object v2, p1

    move v4, p2

    move-wide v5, p4

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_13

    const-wide/16 p6, 0x0

    :cond_13
    :goto_13
    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-lez v2, :cond_35

    const/16 p10, 0x64

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_35

    const-string v2, "TwitterService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getMessages: newer than sinceId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p6

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_35
    const/4 v2, 0x1

    if-ne p2, v2, :cond_108

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "direct_messages"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    :goto_46
    const-string v2, ".json"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v2, 0x0

    cmp-long v2, p6, v2

    if-lez v2, :cond_58

    const-string v2, "since_id"

    move-wide/from16 v0, p6

    invoke-static {v4, v2, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_58
    const-wide/16 v2, 0x0

    cmp-long v2, p8, v2

    if-lez v2, :cond_65

    const-string v2, "max_id"

    move-wide/from16 v0, p8

    invoke-static {v4, v2, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_65
    if-lez p10, :cond_6e

    const-string v2, "count"

    move/from16 v0, p10

    invoke-static {v4, v2, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_6e
    const-string v2, "include_entities"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_user_entities"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x0

    move-object v5, p3

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_101

    :try_start_95
    invoke-static {v7}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/api/s;->f(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v3

    const-wide/16 v4, 0x0

    cmp-long v2, p8, v4

    if-lez v2, :cond_11d

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_11d

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/p;

    iget-wide v4, v2, Lcom/twitter/android/api/p;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    :goto_bb
    const-wide/16 v4, 0x0

    cmp-long v2, p8, v4

    if-eqz v2, :cond_11f

    const/4 v7, 0x1

    :goto_c2
    const-wide/16 v4, 0x0

    cmp-long v2, p8, v4

    if-lez v2, :cond_121

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, p10

    if-ge v2, v0, :cond_121

    const/4 v8, 0x1

    :goto_d1
    move-object v2, p1

    move-wide v4, p4

    move v6, p2

    invoke-virtual/range {v2 .. v9}, Lcom/twitter/android/provider/ae;->a(Ljava/util/List;JIZZLjava/lang/String;)I

    move-result v3

    const/4 v2, 0x1

    if-ne p2, v2, :cond_101

    invoke-virtual {p1}, Lcom/twitter/android/provider/ae;->d()I

    move-result v4

    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v5

    const-string v2, "account_name"

    move-object/from16 v0, p11

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "message"

    if-lez v4, :cond_123

    const/4 v2, 0x1

    :goto_f0
    invoke-virtual {v5, v6, v7, v2}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    const-string v2, "new_dm"

    move-object/from16 v0, p11

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "unread_dm"

    move-object/from16 v0, p11

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_101
    .catch Ljava/io/IOException; {:try_start_95 .. :try_end_101} :catch_125

    :cond_101
    :goto_101
    return-object v10

    :cond_102
    invoke-virtual {p1, p2}, Lcom/twitter/android/provider/ae;->c(I)J

    move-result-wide p6

    goto/16 :goto_13

    :cond_108
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "direct_messages"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "sent"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    goto/16 :goto_46

    :cond_11d
    const/4 v9, 0x0

    goto :goto_bb

    :cond_11f
    const/4 v7, 0x0

    goto :goto_c2

    :cond_121
    const/4 v8, 0x0

    goto :goto_d1

    :cond_123
    const/4 v2, 0x0

    goto :goto_f0

    :catch_125
    move-exception v2

    const/4 v3, 0x0

    iput v3, v10, Lcom/twitter/android/network/c;->b:I

    iput-object v2, v10, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_101
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/api/ad;Lcom/twitter/android/network/a;JJILandroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 43

    if-nez p2, :cond_4

    const/4 v5, 0x0

    :goto_3
    return-object v5

    :cond_4
    const-string v5, "account_name"

    move-object/from16 v0, p9

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const/4 v5, -0x2

    move/from16 v0, p8

    if-ne v0, v5, :cond_67

    const/4 v5, 0x1

    move v11, v5

    :goto_13
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/twitter/android/api/ad;->a:J

    move-wide/from16 v25, v0

    if-eqz v11, :cond_6a

    const/16 p8, 0x64

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v25

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/provider/ae;->e(JI)J

    move-result-wide p4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v25

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/provider/ae;->d(JI)J

    move-result-wide p6

    sget-boolean v5, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v5, :cond_55

    const-string v5, "TwitterService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[Refresh] sinceId: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p4

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", maxId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, p6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_55
    const-wide/16 v5, 0x0

    cmp-long v5, p4, v5

    if-eqz v5, :cond_65

    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-eqz v5, :cond_65

    cmp-long v5, p6, p4

    if-nez v5, :cond_6a

    :cond_65
    const/4 v5, 0x0

    goto :goto_3

    :cond_67
    const/4 v5, 0x0

    move v11, v5

    goto :goto_13

    :cond_6a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v5, v5, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "1.1"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "statuses"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "home_timeline"

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".json"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-lez p8, :cond_95

    const-string v5, "count"

    move/from16 v0, p8

    invoke-static {v7, v5, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_95
    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-lez v5, :cond_a2

    const-string v5, "max_id"

    move-wide/from16 v0, p6

    invoke-static {v7, v5, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_a2
    const-wide/16 v5, 0x0

    cmp-long v5, p4, v5

    if-lez v5, :cond_af

    const-string v5, "since_id"

    move-wide/from16 v0, p4

    invoke-static {v7, v5, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_af
    const-string v5, "lat"

    const-wide/high16 v8, 0x7ff8

    move-object/from16 v0, p9

    invoke-virtual {v0, v5, v8, v9}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v5

    const-string v8, "long"

    const-wide/high16 v9, 0x7ff8

    move-object/from16 v0, p9

    invoke-virtual {v0, v8, v9, v10}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v8

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v10

    if-nez v10, :cond_d9

    invoke-static {v8, v9}, Ljava/lang/Double;->isNaN(D)Z

    move-result v10

    if-nez v10, :cond_d9

    const-string v10, "lat"

    invoke-static {v7, v10, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    const-string v5, "long"

    invoke-static {v7, v5, v8, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_d9
    const-string v5, "include_entities"

    const/4 v6, 0x1

    invoke-static {v7, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v5, "include_user_entities"

    const/4 v6, 0x1

    invoke-static {v7, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v5, "include_my_retweet"

    const/4 v6, 0x1

    invoke-static {v7, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v5, "include_cards"

    const/4 v6, 0x1

    invoke-static {v7, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v5, "earned"

    const/4 v6, 0x1

    invoke-static {v7, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-gtz v5, :cond_203

    const/4 v5, 0x1

    move/from16 v23, v5

    :goto_100
    if-eqz v23, :cond_108

    const-string v5, "pc"

    const/4 v6, 0x1

    invoke-static {v7, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_108
    new-instance v10, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x400

    invoke-direct {v10, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v9, 0x0

    move-object/from16 v8, p3

    invoke-static/range {v5 .. v10}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/twitter/android/network/c;->b()Z

    move-result v5

    if-eqz v5, :cond_1ff

    :try_start_129
    invoke-static {v10}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v5

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-static {v5, v0, v6, v7}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;Lcom/twitter/android/api/ad;ZZ)Ljava/util/ArrayList;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v28

    const/16 v16, 0x0

    const-wide v14, 0x7fffffffffffffffL

    const-wide/16 v12, 0x0

    if-lez v28, :cond_281

    if-nez v11, :cond_281

    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-lez v5, :cond_208

    move-object/from16 v0, p1

    move-wide/from16 v1, v25

    move-wide/from16 v3, p6

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/provider/ae;->d(JJ)I

    move-result v5

    if-lez v5, :cond_208

    add-int/lit8 v5, v28, -0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/twitter/android/api/ab;

    move-object v11, v0

    if-eqz v11, :cond_289

    const/4 v8, 0x0

    invoke-virtual {v11}, Lcom/twitter/android/api/ab;->b()Lcom/twitter/android/api/ab;

    move-result-object v5

    iget-wide v9, v5, Lcom/twitter/android/api/ab;->a:J

    move-object/from16 v5, p1

    move-wide/from16 v6, v25

    invoke-virtual/range {v5 .. v10}, Lcom/twitter/android/provider/ae;->a(JIJ)Z

    move-result v5

    if-nez v5, :cond_289

    const/4 v9, 0x1

    iget-wide v5, v11, Lcom/twitter/android/api/ab;->a:J

    iget-wide v7, v11, Lcom/twitter/android/api/ab;->d:J

    :goto_17c
    move-wide/from16 v18, v5

    move-wide/from16 v20, v7

    move/from16 v22, v9

    :goto_182
    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-lez v5, :cond_258

    if-lez v28, :cond_258

    add-int/lit8 v5, v28, -0x1

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/api/ab;

    iget-wide v5, v5, Lcom/twitter/android/api/ab;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    :goto_19a
    if-eqz v23, :cond_1a2

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/twitter/android/provider/ae;->e(I)I

    :cond_1a2
    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-lez v5, :cond_25b

    const/4 v12, 0x1

    :goto_1ac
    const-wide/16 v5, 0x0

    cmp-long v5, p6, v5

    if-lez v5, :cond_25e

    if-nez v22, :cond_25e

    const/4 v13, 0x1

    :goto_1b5
    const/4 v15, 0x0

    const/16 v16, 0x1

    move-object/from16 v5, p1

    move-object/from16 v6, v27

    move-wide/from16 v7, v25

    invoke-virtual/range {v5 .. v16}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I

    move-result v13

    if-eqz v22, :cond_1d0

    const/4 v12, 0x0

    move-object/from16 v5, p1

    move-wide/from16 v6, v25

    move-wide/from16 v8, v18

    move-wide/from16 v10, v20

    invoke-virtual/range {v5 .. v12}, Lcom/twitter/android/provider/ae;->a(JJJI)V

    :cond_1d0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/twitter/android/provider/z;->a:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v25

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/provider/ae;->f(JI)I

    move-result v6

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v7

    const-string v8, "tweet"

    if-lez v6, :cond_261

    const/4 v5, 0x1

    :goto_1ec
    move-object/from16 v0, v24

    invoke-virtual {v7, v0, v8, v5}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    const-string v5, "new_tweet"

    move-object/from16 v0, p9

    invoke-virtual {v0, v5, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "unread_tweet"

    move-object/from16 v0, p9

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1ff
    :goto_1ff
    move-object/from16 v5, v17

    goto/16 :goto_3

    :cond_203
    const/4 v5, 0x0

    move/from16 v23, v5

    goto/16 :goto_100

    :cond_208
    const-wide/16 v5, 0x0

    cmp-long v5, p4, v5

    if-lez v5, :cond_281

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v25

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/provider/ae;->d(JI)J

    move-result-wide v18

    const-wide/16 v5, 0x0

    cmp-long v5, v18, v5

    if-lez v5, :cond_281

    const/16 v16, 0x1

    add-int/lit8 v5, v28, -0x1

    move-wide v6, v12

    move-wide v8, v14

    move v10, v5

    :goto_224
    if-ltz v10, :cond_279

    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/api/ab;

    invoke-virtual {v5}, Lcom/twitter/android/api/ab;->b()Lcom/twitter/android/api/ab;

    move-result-object v11

    iget-wide v11, v11, Lcom/twitter/android/api/ab;->a:J

    cmp-long v11, v18, v11

    if-nez v11, :cond_242

    const/16 v16, 0x0

    move-wide/from16 v18, v6

    move-wide/from16 v20, v8

    move/from16 v22, v16

    goto/16 :goto_182

    :cond_242
    iget-wide v11, v5, Lcom/twitter/android/api/ab;->d:J

    cmp-long v11, v8, v11

    if-lez v11, :cond_270

    iget-wide v7, v5, Lcom/twitter/android/api/ab;->d:J

    iget-wide v5, v5, Lcom/twitter/android/api/ab;->a:J
    :try_end_24c
    .catch Ljava/io/IOException; {:try_start_129 .. :try_end_24c} :catch_263

    :goto_24c
    add-int/lit8 v9, v10, -0x1

    move v10, v9

    move-wide/from16 v29, v7

    move-wide/from16 v8, v29

    move-wide/from16 v31, v5

    move-wide/from16 v6, v31

    goto :goto_224

    :cond_258
    const/4 v14, 0x0

    goto/16 :goto_19a

    :cond_25b
    const/4 v12, 0x0

    goto/16 :goto_1ac

    :cond_25e
    const/4 v13, 0x0

    goto/16 :goto_1b5

    :cond_261
    const/4 v5, 0x0

    goto :goto_1ec

    :catch_263
    move-exception v5

    sget-boolean v6, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v6, :cond_1ff

    const-string v6, "TwitterService"

    const-string v7, "Problem processing response."

    invoke-static {v6, v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1ff

    :cond_270
    move-wide/from16 v29, v6

    move-wide/from16 v5, v29

    move-wide/from16 v31, v8

    move-wide/from16 v7, v31

    goto :goto_24c

    :cond_279
    move-wide/from16 v18, v6

    move-wide/from16 v20, v8

    move/from16 v22, v16

    goto/16 :goto_182

    :cond_281
    move-wide/from16 v18, v12

    move-wide/from16 v20, v14

    move/from16 v22, v16

    goto/16 :goto_182

    :cond_289
    move-wide v5, v12

    move-wide v7, v14

    move/from16 v9, v16

    goto/16 :goto_17c
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;J)Lcom/twitter/android/network/c;
    .registers 11

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "trends"

    aput-object v2, v1, v4

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p2, Lcom/twitter/android/network/a;->p:Lcom/twitter/android/network/k;

    if-eqz v0, :cond_24

    const-string v0, "pc"

    invoke-static {v2, v0, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_24
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x400

    invoke-direct {v5, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_4c

    :try_start_40
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->h(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;I)I
    :try_end_4c
    .catch Ljava/io/IOException; {:try_start_40 .. :try_end_4c} :catch_4d

    :cond_4c
    :goto_4c
    return-object v1

    :catch_4d
    move-exception v0

    iput v4, v1, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v1, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_4c
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JIJJJIZLandroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 32

    packed-switch p5, :pswitch_data_242

    :pswitch_3
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v3, v3, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "1.1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "user_timeline"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v3, "user_id"

    move-wide/from16 v0, p3

    invoke-static {v5, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string v3, "include_rts"

    const/4 v4, 0x1

    invoke-static {v5, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v3, "earned"

    const/4 v4, 0x1

    invoke-static {v5, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :goto_4f
    const-string v3, "include_entities"

    const/4 v4, 0x1

    invoke-static {v5, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v3, "include_cards"

    const/4 v4, 0x1

    invoke-static {v5, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v3, "include_user_entities"

    const/4 v4, 0x1

    invoke-static {v5, v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-lez p12, :cond_6a

    const-string v3, "count"

    move/from16 v0, p12

    invoke-static {v5, v3, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_6a
    const-wide/16 v3, 0x0

    cmp-long v3, p8, v3

    if-nez v3, :cond_83

    const-wide/16 v3, 0x0

    cmp-long v3, p10, v3

    if-nez v3, :cond_83

    const-wide/16 v3, 0x0

    cmp-long v3, p6, v3

    if-lez v3, :cond_83

    const-string v3, "since_id"

    move-wide/from16 v0, p6

    invoke-static {v5, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_83
    const-wide/16 v3, 0x0

    cmp-long v3, p10, v3

    if-lez v3, :cond_90

    const-string v3, "max_id"

    move-wide/from16 v0, p10

    invoke-static {v5, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_90
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v8, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x0

    move-object/from16 v6, p2

    invoke-static/range {v3 .. v8}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v15

    invoke-virtual {v15}, Lcom/twitter/android/network/c;->b()Z

    move-result v3

    if-eqz v3, :cond_151

    :try_start_b0
    invoke-static {v8}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v3

    const-wide/16 v4, 0x0

    cmp-long v4, p8, v4

    if-nez v4, :cond_214

    invoke-static {v3}, Lcom/twitter/android/api/s;->d(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v4

    const-wide/16 v5, 0x0

    cmp-long v3, p10, v5

    if-lez v3, :cond_20e

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_20e

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/ab;

    iget-wide v5, v3, Lcom/twitter/android/api/ab;->a:J

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    :goto_dc
    const-wide/16 v5, 0x0

    cmp-long v3, p10, v5

    if-lez v3, :cond_211

    const/4 v3, 0x1

    :goto_e3
    move v11, v3

    :goto_e4
    const-string v16, "new_tweet"

    const-wide/16 v8, -0x1

    const-wide/16 v5, 0x0

    cmp-long v3, p10, v5

    if-lez v3, :cond_22d

    const/4 v10, 0x1

    :goto_ef
    const/16 v3, 0x11

    move/from16 v0, p5

    if-eq v0, v3, :cond_230

    const/4 v14, 0x1

    :goto_f6
    move-object/from16 v3, p1

    move-wide/from16 v5, p3

    move/from16 v7, p5

    move/from16 v13, p13

    invoke-virtual/range {v3 .. v14}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I

    move-result v3

    move-object/from16 v0, p14

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v5, 0x64

    if-lt v3, v5, :cond_12a

    const-wide/16 v7, -0x1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/ab;

    iget-wide v9, v3, Lcom/twitter/android/api/ab;->a:J

    move-object/from16 v3, p1

    move-wide/from16 v4, p3

    move/from16 v6, p5

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/android/provider/ae;->a(JIJJ)V

    :cond_12a
    const/4 v3, 0x5

    move/from16 v0, p5

    if-ne v0, v3, :cond_151

    const/4 v3, 0x5

    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/provider/ae;->f(JI)I

    move-result v3

    if-lez v3, :cond_14a

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v4

    const-string v5, "account_name"

    move-object/from16 v0, p14

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I

    :cond_14a
    const-string v4, "unread_mention"

    move-object/from16 v0, p14

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_151
    .catch Ljava/io/IOException; {:try_start_b0 .. :try_end_151} :catch_233

    :cond_151
    :goto_151
    return-object v15

    :pswitch_152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "mentions"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/16 :goto_4f

    :pswitch_16f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "retweeted_by_me"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/16 :goto_4f

    :pswitch_18c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "retweeted_to_me"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/16 :goto_4f

    :pswitch_1a9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "retweets_of_me"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/16 :goto_4f

    :pswitch_1c6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "show"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    goto/16 :goto_4f

    :pswitch_1ea
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "statuses"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "media_timeline"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v3, "user_id"

    move-wide/from16 v0, p3

    invoke-static {v5, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto/16 :goto_4f

    :cond_20e
    const/4 v12, 0x0

    goto/16 :goto_dc

    :cond_211
    const/4 v3, 0x0

    goto/16 :goto_e3

    :cond_214
    :try_start_214
    invoke-static {v3}, Lcom/twitter/android/api/s;->e(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ab;

    move-result-object v3

    const-string v4, "status_id"

    iget-wide v5, v3, Lcom/twitter/android/api/ab;->a:J

    move-object/from16 v0, p14

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_229
    .catch Ljava/io/IOException; {:try_start_214 .. :try_end_229} :catch_233

    const/4 v11, 0x0

    const/4 v12, 0x0

    goto/16 :goto_e4

    :cond_22d
    const/4 v10, 0x0

    goto/16 :goto_ef

    :cond_230
    const/4 v14, 0x0

    goto/16 :goto_f6

    :catch_233
    move-exception v3

    sget-boolean v4, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v4, :cond_151

    const-string v4, "TwitterService"

    const-string v5, "Problem processing response."

    invoke-static {v4, v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_151

    nop

    :pswitch_data_242
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_3
        :pswitch_152
        :pswitch_3
        :pswitch_152
        :pswitch_16f
        :pswitch_18c
        :pswitch_1a9
        :pswitch_3
        :pswitch_1c6
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1ea
    .end packed-switch
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JJ)Lcom/twitter/android/network/c;
    .registers 13

    const-wide/16 v1, 0x0

    const/4 v4, 0x0

    cmp-long v0, p3, v1

    if-eqz v0, :cond_b

    cmp-long v0, p5, v1

    if-nez v0, :cond_d

    :cond_b
    const/4 v0, 0x0

    :cond_c
    :goto_c
    return-object v0

    :cond_d
    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v0, v0, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "1.1"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string v3, "friendships"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "show"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, "source_id"

    invoke-static {v2, v0, p3, p4}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string v0, "target_id"

    invoke-static {v2, v0, p5, p6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v0, p2, Lcom/twitter/android/network/a;->p:Lcom/twitter/android/network/k;

    if-eqz v0, :cond_6d

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    :goto_4e
    invoke-virtual {v0}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_c

    :try_start_54
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->k(Lorg/codehaus/jackson/JsonParser;)I

    move-result v1

    invoke-virtual {p1, p5, p6, v1}, Lcom/twitter/android/provider/ae;->a(JI)V
    :try_end_5f
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_5f} :catch_60

    goto :goto_c

    :catch_60
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_c

    const-string v2, "TwitterService"

    const-string v3, "Problem processing response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_c

    :cond_6d
    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    goto :goto_4e
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JLjava/lang/String;Lcom/twitter/android/api/TweetEntities;JLandroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 27

    const-string v3, "impression_id"

    move-object/from16 v0, p9

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v3, "earned"

    const/4 v4, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v3, "lat"

    const-wide/high16 v7, 0x7ff8

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v7, v8}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v7

    const-string v3, "long"

    const-wide/high16 v9, 0x7ff8

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v9, v10}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v9

    if-eqz p6, :cond_fb

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/twitter/android/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v3, :cond_fb

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/twitter/android/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_fb

    const/4 v3, 0x1

    :goto_38
    new-instance v16, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x400

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    if-eqz v3, :cond_fe

    sget v4, Lcom/twitter/android/ej;->base_url_upload:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/android/service/TwitterService;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "statuses"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string v13, "update_with_media"

    aput-object v13, v11, v12

    invoke-static {v4, v11}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, ".json"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    :goto_62
    const-string v11, "status"

    move-object/from16 v0, p5

    invoke-static {v4, v11, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "send_error_codes"

    const/4 v12, 0x1

    invoke-static {v4, v11, v12}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-wide/16 v11, 0x0

    cmp-long v11, p7, v11

    if-lez v11, :cond_8b

    const-string v11, "in_reply_to_status_id"

    move-wide/from16 v0, p7

    invoke-static {v4, v11, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    if-eqz v5, :cond_8b

    const-string v11, "impression_id"

    invoke-static {v4, v11, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v6, :cond_8b

    const-string v5, "earned"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_8b
    invoke-static {v7, v8}, Ljava/lang/Double;->isNaN(D)Z

    move-result v5

    if-nez v5, :cond_a1

    invoke-static {v9, v10}, Ljava/lang/Double;->isNaN(D)Z

    move-result v5

    if-nez v5, :cond_a1

    const-string v5, "lat"

    invoke-static {v4, v5, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    const-string v5, "long"

    invoke-static {v4, v5, v9, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_a1
    new-instance v5, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v14, Lcom/twitter/android/network/j;

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-direct {v14, v4, v5, v0, v1}, Lcom/twitter/android/network/j;-><init>(Lcom/twitter/android/network/d;Lorg/apache/http/client/methods/HttpPost;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v4

    invoke-virtual {v4, v14}, Lcom/twitter/android/network/p;->a(Lcom/twitter/android/network/c;)V

    const/4 v12, 0x0

    const/4 v4, 0x0

    if-eqz v3, :cond_217

    const/4 v15, 0x0

    :try_start_c3
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/twitter/android/api/TweetEntities;->media:Ljava/util/ArrayList;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/twitter/android/api/TweetEntities$Media;

    move-object v12, v0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v6, v12, Lcom/twitter/android/api/TweetEntities$Media;->url:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-wide/from16 v0, p3

    invoke-static {v3, v6, v0, v1}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_df
    .catchall {:try_start_c3 .. :try_end_df} :catchall_1a1
    .catch Ljava/io/IOException; {:try_start_c3 .. :try_end_df} :catch_19a

    move-result-object v13

    if-nez v13, :cond_127

    const-wide/16 v4, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p5

    move-wide/from16 v7, p3

    move-wide/from16 v9, p7

    move-object/from16 v11, p6

    :try_start_ee
    invoke-virtual/range {v3 .. v11}, Lcom/twitter/android/provider/ae;->a(JLjava/lang/String;JJLcom/twitter/android/api/TweetEntities;)J

    const/16 v3, 0x19d

    iput v3, v14, Lcom/twitter/android/network/c;->b:I
    :try_end_f5
    .catchall {:try_start_ee .. :try_end_f5} :catchall_1a1
    .catch Ljava/io/IOException; {:try_start_ee .. :try_end_f5} :catch_20f

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object v3, v14

    :goto_fa
    return-object v3

    :cond_fb
    const/4 v3, 0x0

    goto/16 :goto_38

    :cond_fe
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string v13, "statuses"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string v13, "update"

    aput-object v13, v11, v12

    invoke-virtual {v4, v11}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, ".json"

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "include_entities"

    const/4 v12, 0x1

    invoke-static {v4, v11, v12}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v11, "include_cards"

    const/4 v12, 0x1

    invoke-static {v4, v11, v12}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    goto/16 :goto_62

    :cond_127
    :try_start_127
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_12e
    .catchall {:try_start_127 .. :try_end_12e} :catchall_1a1
    .catch Ljava/io/IOException; {:try_start_127 .. :try_end_12e} :catch_20f

    move-result-object v3

    :try_start_12f
    new-instance v4, Lcom/twitter/android/network/g;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Lcom/twitter/android/network/g;-><init>(Lcom/twitter/android/network/m;)V

    const-string v6, "media[]"

    const/16 v7, 0x8

    invoke-static {v7}, Lcom/twitter/android/util/z;->a(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7, v3}, Lcom/twitter/android/network/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)I

    invoke-virtual {v4}, Lcom/twitter/android/network/g;->a()V

    invoke-virtual {v5, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_146
    .catchall {:try_start_12f .. :try_end_146} :catchall_20b
    .catch Ljava/io/IOException; {:try_start_12f .. :try_end_146} :catch_213

    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object v3, v13

    :goto_14a
    invoke-virtual {v14}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    invoke-virtual {v14}, Lcom/twitter/android/network/c;->b()Z

    move-result v4

    if-eqz v4, :cond_1cd

    if-eqz v12, :cond_15a

    :try_start_155
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    :cond_15a
    invoke-static/range {v16 .. v16}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/android/api/s;->e(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ab;

    move-result-object v3

    move-object/from16 v0, p1

    move-wide/from16 v1, p3

    invoke-virtual {v0, v3, v1, v2}, Lcom/twitter/android/provider/ae;->a(Lcom/twitter/android/api/ab;J)V

    iget-object v4, v3, Lcom/twitter/android/api/ab;->n:Lcom/twitter/android/api/TweetEntities;

    if-eqz v4, :cond_197

    iget-object v4, v3, Lcom/twitter/android/api/ab;->n:Lcom/twitter/android/api/TweetEntities;

    iget-object v4, v4, Lcom/twitter/android/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v4, :cond_197

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iget-object v3, v3, Lcom/twitter/android/api/ab;->n:Lcom/twitter/android/api/TweetEntities;

    iget-object v3, v3, Lcom/twitter/android/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_180
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1a6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/TweetEntities$Mention;

    iget-wide v6, v3, Lcom/twitter/android/api/TweetEntities$Mention;->userId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_195
    .catch Ljava/io/IOException; {:try_start_155 .. :try_end_195} :catch_196

    goto :goto_180

    :catch_196
    move-exception v3

    :cond_197
    :goto_197
    move-object v3, v14

    goto/16 :goto_fa

    :catch_19a
    move-exception v3

    move-object v3, v4

    move-object v4, v15

    :goto_19d
    invoke-static {v4}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    goto :goto_14a

    :catchall_1a1
    move-exception v3

    :goto_1a2
    invoke-static {v15}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v3

    :cond_1a6
    :try_start_1a6
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/service/TwitterService;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v5, "ADD_USER_SUGGESTION"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string v5, "soid"

    move-wide/from16 v0, p3

    invoke-virtual {v3, v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string v5, "user_id"

    invoke-static {v4}, Lcom/twitter/android/util/z;->b(Ljava/util/Collection;)[J

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/service/TwitterService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1cc
    .catch Ljava/io/IOException; {:try_start_1a6 .. :try_end_1cc} :catch_196

    goto :goto_197

    :cond_1cd
    const-wide/16 v4, 0x0

    move-object/from16 v3, p1

    move-object/from16 v6, p5

    move-wide/from16 v7, p3

    move-wide/from16 v9, p7

    move-object/from16 v11, p6

    invoke-virtual/range {v3 .. v11}, Lcom/twitter/android/provider/ae;->a(JLjava/lang/String;JJLcom/twitter/android/api/TweetEntities;)J

    :try_start_1dc
    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "errors"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_197

    const-string v4, "errors"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "code"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v14, Lcom/twitter/android/network/c;->b:I

    const-string v4, "message"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v14, Lcom/twitter/android/network/c;->c:Ljava/lang/String;
    :try_end_208
    .catch Lorg/json/JSONException; {:try_start_1dc .. :try_end_208} :catch_209

    goto :goto_197

    :catch_209
    move-exception v3

    goto :goto_197

    :catchall_20b
    move-exception v4

    move-object v15, v3

    move-object v3, v4

    goto :goto_1a2

    :catch_20f
    move-exception v3

    move-object v3, v13

    move-object v4, v15

    goto :goto_19d

    :catch_213
    move-exception v4

    move-object v4, v3

    move-object v3, v13

    goto :goto_19d

    :cond_217
    move-object v3, v4

    goto/16 :goto_14a
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JZ)Lcom/twitter/android/network/c;
    .registers 11

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    cmp-long v1, p3, v1

    if-gtz v1, :cond_8

    :cond_7
    :goto_7
    return-object v0

    :cond_8
    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "direct_messages"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "destroy"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1, p2, v0}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    if-eqz p5, :cond_7

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_46

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/provider/ae;->c(J)V

    goto :goto_7

    :cond_46
    iget v1, v0, Lcom/twitter/android/network/c;->b:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_7

    invoke-virtual {p1, p3, p4}, Lcom/twitter/android/provider/ae;->c(J)V

    goto :goto_7
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 20

    const-string v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "favorites"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v1, ".json"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "id"

    invoke-static {v3, v1, v13, v14}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    if-lez v9, :cond_34

    const-string v1, "page"

    invoke-static {v3, v1, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_34
    const-string v1, "include_entities"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v1, "include_cards"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v15

    invoke-virtual {v15}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_8f

    :try_start_5e
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->d(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v2

    if-lez v9, :cond_90

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_90

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ab;

    iget-wide v3, v1, Lcom/twitter/android/api/ab;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    :goto_80
    const/4 v5, 0x2

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    if-lez v9, :cond_92

    const/4 v9, 0x1

    :goto_87
    const/4 v11, 0x1

    const/4 v12, 0x1

    move-object/from16 v1, p1

    move-wide v3, v13

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I
    :try_end_8f
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_8f} :catch_94

    :cond_8f
    :goto_8f
    return-object v15

    :cond_90
    const/4 v10, 0x0

    goto :goto_80

    :cond_92
    const/4 v9, 0x0

    goto :goto_87

    :catch_94
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_8f

    const-string v2, "TwitterService"

    const-string v3, "Problem processing response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8f
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;Ljava/lang/String;)Lcom/twitter/android/network/c;
    .registers 17

    const/4 v1, 0x0

    const-string v0, "remove_header"

    const/4 v2, 0x0

    invoke-virtual {p3, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_77

    const-string v0, "user"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/android/api/ad;

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "profile_banner"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-static {v2, v3, v0, p2, v1}, Lcom/twitter/android/network/c;->b(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;

    move-result-object v11

    invoke-virtual {v11}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_61

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/twitter/android/api/ad;->w:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v0, p1

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_61
    const-string v0, "user"

    invoke-virtual {p3, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "header_error"

    iget v1, v11, Lcom/twitter/android/network/c;->b:I

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v11

    :goto_6e
    if-eqz v0, :cond_95

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->c()Z

    move-result v1

    if-nez v1, :cond_95

    :cond_76
    :goto_76
    return-object v0

    :cond_77
    const-string v0, "header_uri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_e2

    const-string v1, "crop"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    invoke-direct {p0, p2, v0, v1}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;Landroid/net/Uri;Landroid/graphics/Rect;)Lcom/twitter/android/network/c;

    move-result-object v0

    const-string v1, "header_error"

    iget v2, v0, Lcom/twitter/android/network/c;->b:I

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_6e

    :cond_95
    const-string v1, "avatar_uri"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    if-eqz v5, :cond_b6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p4

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Ljava/lang/String;Lcom/twitter/android/network/a;Landroid/content/Intent;Landroid/net/Uri;)Lcom/twitter/android/network/c;

    move-result-object v0

    const-string v1, "avatar_error"

    iget v2, v0, Lcom/twitter/android/network/c;->b:I

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->c()Z

    move-result v1

    if-eqz v1, :cond_76

    :cond_b6
    const-string v1, "name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d6

    const-string v1, "url"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d6

    const-string v1, "desc"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d6

    const-string v1, "place"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_76

    :cond_d6
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/service/TwitterService;->u(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v0

    const-string v1, "profile_error"

    iget v2, v0, Lcom/twitter/android/network/c;->b:I

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_76

    :cond_e2
    move-object v0, v1

    goto :goto_6e
.end method

.method private a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;Z)Lcom/twitter/android/network/c;
    .registers 19

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v1, "impression_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "earned"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    if-eqz p4, :cond_a0

    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v9, "report_spam"

    aput-object v9, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v6, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4e
    if-eqz v1, :cond_5d

    const-string v4, "impression_id"

    invoke-static {v3, v4, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_5d

    const-string v1, "earned"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_5d
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_9f

    :try_start_78
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v1

    iget v2, v1, Lcom/twitter/android/api/ad;->y:I

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v2

    iput v2, v1, Lcom/twitter/android/api/ad;->y:I

    invoke-virtual {p1, v11, v12, v7, v8}, Lcom/twitter/android/provider/ae;->b(JJ)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x2

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object v1, p1

    move-wide v3, v11

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I
    :try_end_9f
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_9f} :catch_c8

    :cond_9f
    :goto_9f
    return-object v13

    :cond_a0
    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v9, "blocks"

    aput-object v9, v4, v6

    const/4 v6, 0x1

    const-string v9, "create"

    aput-object v9, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "user_id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v6, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4e

    :catch_c8
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_9f

    const-string v2, "TwitterService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createBlock, spam: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9f
.end method

.method private a(Lcom/twitter/android/provider/ae;Ljava/lang/String;Lcom/twitter/android/network/a;Landroid/content/Intent;Landroid/net/Uri;)Lcom/twitter/android/network/c;
    .registers 24

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    const-string v2, "owner_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "account"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "update_profile_image"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v6, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v7, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    new-instance v12, Lcom/twitter/android/network/j;

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-direct {v12, v2, v7, v0, v6}, Lcom/twitter/android/network/j;-><init>(Lcom/twitter/android/network/d;Lorg/apache/http/client/methods/HttpPost;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/twitter/android/network/p;->a(Lcom/twitter/android/network/c;)V

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v8, 0x2bc

    :try_start_4f
    move-object/from16 v0, p5

    invoke-static {v15, v0, v8, v4, v5}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;IJ)Landroid/net/Uri;
    :try_end_54
    .catchall {:try_start_4f .. :try_end_54} :catchall_11d
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_54} :catch_110

    move-result-object v13

    if-nez v13, :cond_66

    const/16 v2, 0x19d

    :try_start_59
    iput v2, v12, Lcom/twitter/android/network/c;->b:I
    :try_end_5b
    .catchall {:try_start_59 .. :try_end_5b} :catchall_12a
    .catch Ljava/io/IOException; {:try_start_59 .. :try_end_5b} :catch_134

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-object v2, v12

    :goto_65
    return-object v2

    :cond_66
    :try_start_66
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_6d
    .catchall {:try_start_66 .. :try_end_6d} :catchall_12a
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_6d} :catch_134

    move-result-object v14

    :try_start_6e
    invoke-virtual {v14}, Ljava/io/InputStream;->available()I

    move-result v2

    new-array v0, v2, [B

    move-object/from16 v16, v0

    const/4 v2, 0x0

    move-object/from16 v0, v16

    array-length v3, v0

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    if-gtz v2, :cond_90

    const/16 v2, 0x1a6

    iput v2, v12, Lcom/twitter/android/network/c;->b:I
    :try_end_86
    .catchall {:try_start_6e .. :try_end_86} :catchall_12d
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_86} :catch_137

    invoke-static {v14}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    move-object v2, v12

    goto :goto_65

    :cond_90
    :try_start_90
    new-instance v2, Lcom/twitter/android/network/g;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/twitter/android/network/g;-><init>(Lcom/twitter/android/network/m;)V

    const-string v3, "image"

    const/16 v4, 0x8

    invoke-static {v4}, Lcom/twitter/android/util/z;->a(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v4, v0}, Lcom/twitter/android/network/g;->a(Ljava/lang/String;Ljava/lang/String;[B)I

    invoke-virtual {v2}, Lcom/twitter/android/network/g;->a()V

    invoke-virtual {v7, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    const/4 v2, 0x0

    invoke-virtual {v12, v2}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;

    invoke-virtual {v12}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_105

    sget-object v2, Lcom/twitter/android/api/s;->b:Lorg/codehaus/jackson/a;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/codehaus/jackson/a;->a([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-virtual {v2}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    invoke-static {v2}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v17

    const/4 v2, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-static {v15, v0, v1, v2}, Lcom/twitter/android/platform/j;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/android/api/ad;Lcom/twitter/android/api/ah;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v4, -0x1

    const/4 v6, -0x1

    const-wide/16 v7, -0x1

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-object/from16 v0, v17

    iget-wide v3, v0, Lcom/twitter/android/api/ad;->a:J

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/twitter/android/ee;->user_image_size:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    move-object/from16 v2, p1

    move-object/from16 v6, v16

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/provider/ae;->a(JLjava/lang/String;[BI)[B

    const-string v2, "user"

    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_105
    .catchall {:try_start_90 .. :try_end_105} :catchall_12d
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_105} :catch_137

    :cond_105
    invoke-static {v14}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    :goto_10d
    move-object v2, v12

    goto/16 :goto_65

    :catch_110
    move-exception v4

    :goto_111
    const/4 v4, 0x0

    :try_start_112
    iput v4, v12, Lcom/twitter/android/network/c;->b:I
    :try_end_114
    .catchall {:try_start_112 .. :try_end_114} :catchall_12f

    invoke-static {v3}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    goto :goto_10d

    :catchall_11d
    move-exception v4

    move-object v13, v2

    move-object v14, v3

    move-object v2, v4

    :goto_121
    invoke-static {v14}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;Landroid/net/Uri;)Z

    throw v2

    :catchall_12a
    move-exception v2

    move-object v14, v3

    goto :goto_121

    :catchall_12d
    move-exception v2

    goto :goto_121

    :catchall_12f
    move-exception v4

    move-object v13, v2

    move-object v14, v3

    move-object v2, v4

    goto :goto_121

    :catch_134
    move-exception v2

    move-object v2, v13

    goto :goto_111

    :catch_137
    move-exception v2

    move-object v2, v13

    move-object v3, v14

    goto :goto_111
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 10

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v1, v1, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/oauth/access_token"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x_auth_mode"

    const-string v2, "client_auth"

    invoke-static {v0, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x_auth_password"

    invoke-static {v0, v1, p2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "x_auth_username"

    invoke-static {v0, v1, p1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-instance v4, Lcom/twitter/android/network/a;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    invoke-static {v2, v3, v0, v4, v1}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_51

    invoke-static {v1}, Lcom/twitter/android/service/TwitterService;->a(Ljava/io/ByteArrayOutputStream;)Lcom/twitter/android/network/k;

    move-result-object v0

    const-string v1, "auth"

    invoke-virtual {p3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    new-instance v1, Lcom/twitter/android/network/a;

    invoke-direct {v1, v0}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    invoke-direct {p0, v1, p3}, Lcom/twitter/android/service/TwitterService;->e(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v0

    :cond_51
    return-object v0
.end method

.method private static a(Ljava/io/ByteArrayOutputStream;)Lcom/twitter/android/network/k;
    .registers 5

    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/network/a;->a(Ljava/lang/String;Z)Ljava/util/TreeMap;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/network/k;

    const-string v0, "oauth_token"

    invoke-virtual {v1, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "oauth_token_secret"

    invoke-virtual {v1, v3}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/network/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private a(JLjava/lang/String;)V
    .registers 12

    const/4 v3, 0x0

    const/4 v7, 0x0

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/twitter/android/service/TwitterService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/NotificationManager;

    invoke-virtual {p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/provider/v;->a:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Lcom/twitter/android/provider/o;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "notif_id"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3d

    :cond_24
    :goto_24
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3a

    invoke-interface {v0, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_24

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_24

    invoke-virtual {v6, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_24

    :cond_3a
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3d
    invoke-virtual {v6, v7}, Landroid/app/NotificationManager;->cancel(I)V

    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4e

    invoke-virtual {v6, v0}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_4e
    return-void
.end method

.method private a(Lcom/twitter/android/network/a;ILjava/lang/String;JZ)V
    .registers 13

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "promoted_content"

    aput-object v2, v1, v4

    const-string v2, "log"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_20

    const-string v0, "impression_id"

    invoke-static {v2, v0, p3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_20
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_2b

    const-string v0, "promoted_trend_id"

    invoke-static {v2, v0, p4, p5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_2b
    if-eqz p6, :cond_32

    const-string v0, "earned"

    invoke-static {v2, v0, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_32
    const-string v0, "event"

    sget-object v1, Lcom/twitter/android/service/TwitterService;->c:[Ljava/lang/String;

    aget-object v1, v1, p2

    invoke-static {v2, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    return-void
.end method

.method private a(Lcom/twitter/android/provider/ae;IIJLjava/util/ArrayList;Landroid/net/Uri;)V
    .registers 24

    if-eqz p6, :cond_8

    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    :goto_8
    return-void

    :cond_9
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    move/from16 v0, p3

    if-eq v0, v1, :cond_3c

    const/4 v8, 0x1

    :goto_1e
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ab;

    iget-object v3, v1, Lcom/twitter/android/api/ab;->r:Lcom/twitter/android/api/PromotedContent;

    if-eqz v3, :cond_3e

    iget-object v3, v1, Lcom/twitter/android/api/ab;->r:Lcom/twitter/android/api/PromotedContent;

    iget-object v3, v3, Lcom/twitter/android/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v3, :cond_3e

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_22

    :cond_3c
    const/4 v8, 0x0

    goto :goto_1e

    :cond_3e
    iget-boolean v3, v1, Lcom/twitter/android/api/ab;->t:Z

    if-eqz v3, :cond_48

    if-nez v8, :cond_48

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_22

    :cond_48
    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_22

    :cond_4c
    const/4 v5, -0x1

    const-wide/16 v6, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v1, p1

    move-object/from16 v2, p6

    move-wide/from16 v3, p4

    move v9, v8

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I

    const-wide/16 v6, 0x3

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v1, p1

    move-object v2, v13

    move-wide/from16 v3, p4

    move/from16 v5, p2

    move v9, v8

    invoke-virtual/range {v1 .. v11}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;Z)I

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_80

    const-wide/16 v6, 0x6

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v1, p1

    move-object v2, v15

    move-wide/from16 v3, p4

    move/from16 v5, p2

    move v9, v8

    invoke-virtual/range {v1 .. v11}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;Z)I

    :cond_80
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_95

    const-wide/16 v6, 0x5

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v1, p1

    move-object v2, v14

    move-wide/from16 v3, p4

    move/from16 v5, p2

    move v9, v8

    invoke-virtual/range {v1 .. v11}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;Z)I

    :cond_95
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p7

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_8
.end method

.method private b(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "status_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "impression_id"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v1, "earned"

    const/4 v2, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    const/4 v6, 0x0

    move-object v1, p3

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(JJZ)V

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v6, "favorites"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    const-string v6, "destroy"

    aput-object v6, v2, v3

    const/4 v3, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v1, ".json"

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz v7, :cond_5f

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "impression_id"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v9, :cond_5f

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "earned"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5f
    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v11, 0x0

    move-object/from16 v9, p4

    invoke-static/range {v6 .. v11}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v7

    iget v1, v7, Lcom/twitter/android/network/c;->b:I

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_7c

    const/4 v6, 0x1

    move-object v1, p3

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(JJZ)V

    :cond_7c
    return-object v7
.end method

.method private b(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 10

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "help"

    aput-object v2, v1, v6

    const-string v2, "configuration"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_40

    :try_start_33
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    const-string v2, "settings"

    invoke-static {v1}, Lcom/twitter/android/api/s;->j(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/c;

    move-result-object v1

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_40} :catch_41

    :cond_40
    :goto_40
    return-object v0

    :catch_41
    move-exception v1

    iput v6, v0, Lcom/twitter/android/network/c;->b:I

    goto :goto_40
.end method

.method private b(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "list_type"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v1, "user_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "owner_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    const-string v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const/4 v2, 0x2

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v1

    packed-switch v3, :pswitch_data_f8

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Invalid list type: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_44
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "lists"

    aput-object v8, v6, v7

    invoke-virtual {v2, v6}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ".json"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    :goto_58
    const-string v2, "user_id"

    invoke-static {v8, v2, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    if-eqz v1, :cond_64

    const-string v2, "cursor"

    invoke-static {v8, v2, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_64
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v10, 0x0

    move-object/from16 v9, p2

    invoke-static/range {v6 .. v11}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_91

    packed-switch v3, :pswitch_data_102

    const/4 v1, 0x0

    :goto_84
    :try_start_84
    invoke-static {v11}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;I)Lcom/twitter/android/api/o;

    move-result-object v1

    if-nez v1, :cond_d8

    const/4 v1, 0x0

    iput v1, v2, Lcom/twitter/android/network/c;->b:I
    :try_end_91
    .catch Ljava/io/IOException; {:try_start_84 .. :try_end_91} :catch_e7

    :cond_91
    :goto_91
    return-object v2

    :pswitch_92
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "lists"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "memberships"

    aput-object v8, v6, v7

    invoke-virtual {v2, v6}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ".json"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto :goto_58

    :pswitch_ac
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "lists"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "subscriptions"

    aput-object v8, v6, v7

    invoke-virtual {v2, v6}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ".json"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    goto :goto_58

    :pswitch_c6
    const/4 v1, 0x0

    goto :goto_84

    :pswitch_c8
    cmp-long v1, v4, v12

    if-nez v1, :cond_ce

    const/4 v1, 0x1

    goto :goto_84

    :cond_ce
    const/4 v1, 0x0

    goto :goto_84

    :pswitch_d0
    cmp-long v1, v4, v12

    if-nez v1, :cond_d6

    const/4 v1, 0x1

    goto :goto_84

    :cond_d6
    const/4 v1, 0x0

    goto :goto_84

    :cond_d8
    :try_start_d8
    invoke-virtual {v1}, Lcom/twitter/android/api/o;->b()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v1}, Lcom/twitter/android/api/o;->a()Ljava/lang/String;

    move-result-object v11

    move-object v6, p1

    move-wide v8, v4

    move v10, v3

    invoke-virtual/range {v6 .. v11}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JILjava/lang/String;)V
    :try_end_e6
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_e6} :catch_e7

    goto :goto_91

    :catch_e7
    move-exception v1

    const/4 v3, 0x0

    iput v3, v2, Lcom/twitter/android/network/c;->b:I

    sget-boolean v3, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v3, :cond_91

    const-string v3, "TwitterService"

    const-string v4, "Problem processing response."

    invoke-static {v3, v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_91

    nop

    :pswitch_data_f8
    .packed-switch 0x0
        :pswitch_44
        :pswitch_92
        :pswitch_ac
    .end packed-switch

    :pswitch_data_102
    .packed-switch 0x0
        :pswitch_d0
        :pswitch_c6
        :pswitch_c8
    .end packed-switch
.end method

.method private static b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;
    .registers 3

    sget-object v0, Lcom/twitter/android/api/s;->b:Lorg/codehaus/jackson/a;

    invoke-virtual {p0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/jackson/a;->a([B)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    return-object v0
.end method

.method private c(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 33

    const-string v2, "page"

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v2, "user_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v2, "user_type"

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v2, "count"

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    if-lez v3, :cond_5d

    const/16 v2, 0x64

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v3, v2}, Lcom/twitter/android/network/p;->a(II)I

    move-result v3

    move v8, v2

    move/from16 v18, v3

    :goto_34
    const/4 v3, 0x1

    move-object/from16 v2, p3

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object/from16 v24, v0

    sparse-switch v4, :sswitch_data_1ac

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Unknown user type: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5d
    const/4 v2, -0x1

    const/4 v3, 0x1

    move v8, v2

    move/from16 v18, v3

    goto :goto_34

    :sswitch_63
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v9, "followers"

    aput-object v9, v2, v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x2

    move-object/from16 v19, v3

    move v3, v2

    :goto_75
    const-string v2, ".json"

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "include_user_entities"

    const/4 v9, 0x1

    move-object/from16 v0, v19

    invoke-static {v0, v2, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "user_id"

    move-object/from16 v0, v19

    invoke-static {v0, v2, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    if-lez v8, :cond_94

    const-string v2, "count"

    move-object/from16 v0, v19

    invoke-static {v0, v2, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_94
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v25

    const/16 v21, 0x0

    const/16 v20, 0x0

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move/from16 v22, v2

    move-object/from16 v2, v21

    :goto_a6
    move/from16 v0, v22

    move/from16 v1, v18

    if-ge v0, v1, :cond_1a0

    if-eqz v16, :cond_b7

    const-string v2, "cursor"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-static {v0, v2, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b7
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x400

    invoke-direct {v13, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    const/4 v12, 0x0

    move-object/from16 v8, v23

    move-object/from16 v9, v24

    move-object/from16 v10, v19

    move-object/from16 v11, p4

    invoke-static/range {v8 .. v13}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_1a5

    :try_start_d5
    invoke-static {v13}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/o;

    move-result-object v8

    if-nez v8, :cond_12b

    const/4 v2, 0x0

    move-object/from16 v0, v21

    iput v2, v0, Lcom/twitter/android/network/c;->b:I
    :try_end_e4
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_e4} :catch_14c

    move/from16 v2, v20

    move-object/from16 v3, v21

    :goto_e8
    const-string v4, "count"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "users"

    move-object/from16 v0, p5

    move-object/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    return-object v3

    :sswitch_f9
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v9, "friends"

    aput-object v9, v2, v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x1

    move-object/from16 v19, v3

    move v3, v2

    goto/16 :goto_75

    :sswitch_10d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v2, v2, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "i"

    aput-object v10, v3, v9

    const/4 v9, 0x1

    const-string v10, "device_following"

    aput-object v10, v3, v9

    invoke-static {v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v2, 0x11

    move-object/from16 v19, v3

    move v3, v2

    goto/16 :goto_75

    :cond_12b
    :try_start_12b
    invoke-virtual {v8}, Lcom/twitter/android/api/o;->b()Ljava/util/ArrayList;

    move-result-object v9

    cmp-long v2, p1, v5

    if-nez v2, :cond_15c

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_137
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_15c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/ad;

    iget v11, v2, Lcom/twitter/android/api/ad;->y:I

    invoke-static {v11, v3}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v11

    iput v11, v2, Lcom/twitter/android/api/ad;->y:I
    :try_end_14b
    .catch Ljava/io/IOException; {:try_start_12b .. :try_end_14b} :catch_14c

    goto :goto_137

    :catch_14c
    move-exception v2

    move-object v3, v2

    move/from16 v2, v20

    :goto_150
    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput v4, v0, Lcom/twitter/android/network/c;->b:I

    move-object/from16 v0, v21

    iput-object v3, v0, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object/from16 v3, v21

    goto :goto_e8

    :cond_15c
    :try_start_15c
    move-object/from16 v0, v26

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v8}, Lcom/twitter/android/api/o;->a()Ljava/lang/String;

    move-result-object v16

    const-wide/16 v13, -0x1

    if-nez v22, :cond_187

    if-nez v7, :cond_187

    const-string v15, "-1"

    :goto_16d
    const/16 v17, 0x1

    move-object/from16 v8, p3

    move-wide v10, v5

    move v12, v4

    invoke-virtual/range {v8 .. v17}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I
    :try_end_176
    .catch Ljava/io/IOException; {:try_start_15c .. :try_end_176} :catch_14c

    move-result v2

    add-int v2, v2, v20

    :try_start_179
    const-string v8, "0"

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_180
    .catch Ljava/io/IOException; {:try_start_179 .. :try_end_180} :catch_19e

    move-result v8

    if-eqz v8, :cond_189

    move-object/from16 v3, v21

    goto/16 :goto_e8

    :cond_187
    const/4 v15, 0x0

    goto :goto_16d

    :cond_189
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1, v8}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v22, 0x1

    move/from16 v22, v8

    move/from16 v20, v2

    move-object/from16 v2, v21

    goto/16 :goto_a6

    :catch_19e
    move-exception v3

    goto :goto_150

    :cond_1a0
    move-object v3, v2

    move/from16 v2, v20

    goto/16 :goto_e8

    :cond_1a5
    move/from16 v2, v20

    move-object/from16 v3, v21

    goto/16 :goto_e8

    nop

    :sswitch_data_1ac
    .sparse-switch
        0x0 -> :sswitch_f9
        0x1 -> :sswitch_63
        0x10 -> :sswitch_10d
    .end sparse-switch
.end method

.method private c(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 9

    const/4 v4, 0x0

    const-string v0, "url"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "urls"

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const-string v5, "shorten"

    aput-object v5, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v1, "url"

    invoke-static {v2, v1, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v1

    :try_start_38
    new-instance v0, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_58

    const-string v0, "short_url"

    const-string v3, "url"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_57
    :goto_57
    return-object v1

    :cond_58
    const-string v0, "errors"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "message"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/android/network/c;->c:Ljava/lang/String;
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_6b} :catch_6c

    goto :goto_57

    :catch_6c
    move-exception v0

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_57

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_57
.end method

.method private c(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "blocks"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "blocking"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v1, ".json"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-lez v8, :cond_32

    const-string v1, "page"

    invoke-static {v3, v1, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_32
    const-string v1, "include_user_entities"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_82

    :try_start_54
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_60
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_83

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/ad;

    iget v4, v1, Lcom/twitter/android/api/ad;->y:I

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/twitter/android/provider/ad;->a(II)I

    move-result v4

    iput v4, v1, Lcom/twitter/android/api/ad;->y:I
    :try_end_75
    .catch Ljava/io/IOException; {:try_start_54 .. :try_end_75} :catch_76

    goto :goto_60

    :catch_76
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_82

    const-string v2, "TwitterService"

    const-string v3, "Problem processing response."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_82
    :goto_82
    return-object v13

    :cond_83
    const/4 v5, 0x2

    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    if-ne v8, v1, :cond_a2

    :try_start_89
    const-string v8, "-1"

    :goto_8b
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_a4

    const-string v9, "0"

    :goto_93
    const/4 v10, 0x1

    move-object v1, p1

    move-wide v3, v11

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    const-string v2, "count"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_a1
    .catch Ljava/io/IOException; {:try_start_89 .. :try_end_a1} :catch_76

    goto :goto_82

    :cond_a2
    const/4 v8, 0x0

    goto :goto_8b

    :cond_a4
    const/4 v9, 0x0

    goto :goto_93
.end method

.method private d(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 15

    const/4 v8, 0x0

    const-string v0, "status_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p5, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "impression_id"

    invoke-virtual {p5, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "earned"

    invoke-virtual {p5, v2, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string v6, "statuses"

    aput-object v6, v4, v8

    const/4 v6, 0x1

    const-string v7, "retweet"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz v3, :cond_53

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "impression_id"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_53

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "earned"

    const-string v3, "true"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_53
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "include_entities"

    const-string v3, "true"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "include_cards"

    const-string v3, "true"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_90

    :try_start_85
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->e(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ab;

    move-result-object v0

    invoke-virtual {p3, v0, p1, p2}, Lcom/twitter/android/provider/ae;->a(Lcom/twitter/android/api/ab;J)V
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_85 .. :try_end_90} :catch_91

    :cond_90
    :goto_90
    return-object v1

    :catch_91
    move-exception v0

    iput v8, v1, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v1, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_90
.end method

.method private d(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 13

    const/4 v4, 0x0

    const-wide/high16 v5, 0x7ff8

    const-string v0, "lat"

    invoke-virtual {p2, v0, v5, v6}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    const-string v2, "long"

    invoke-virtual {p2, v2, v5, v6}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v5

    const-string v2, "accuracy"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "geo"

    aput-object v8, v7, v4

    const/4 v8, 0x1

    const-string v9, "reverse_geocode"

    aput-object v9, v7, v8

    invoke-virtual {v2, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ".json"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v7

    if-nez v7, :cond_4b

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v7

    if-nez v7, :cond_4b

    const-string v7, "lat"

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v7, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "long"

    invoke-static {v5, v6}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4b
    if-eqz v3, :cond_52

    const-string v0, "accuracy"

    invoke-static {v2, v0, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_52
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_e0

    :try_start_6c
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->l(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_e0

    const/4 v1, 0x0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/v;

    const/4 v5, 0x3

    iget v6, v0, Lcom/twitter/android/api/v;->a:I

    if-ne v5, v6, :cond_7b

    iget-object v1, v0, Lcom/twitter/android/api/v;->b:Ljava/lang/String;

    sget-boolean v0, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v0, :cond_a6

    const-string v0, "TwitterService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found city type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a6
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_ee

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ee

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/api/v;

    const/4 v4, 0x4

    iget v5, v0, Lcom/twitter/android/api/v;->a:I

    if-ne v4, v5, :cond_b0

    iget-object v0, v0, Lcom/twitter/android/api/v;->b:Ljava/lang/String;

    sget-boolean v1, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v1, :cond_db

    const-string v1, "TwitterService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found admin type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_db
    :goto_db
    const-string v1, "place"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_e0
    .catch Ljava/io/IOException; {:try_start_6c .. :try_end_e0} :catch_e1

    :cond_e0
    :goto_e0
    return-object v2

    :catch_e1
    move-exception v0

    sget-boolean v1, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v1, :cond_e0

    const-string v1, "TwitterService"

    const-string v3, "reverseGeo"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_e0

    :cond_ee
    move-object v0, v1

    goto :goto_db
.end method

.method private d(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 21

    const-string v1, "device_follow"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "user_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v2, "owner_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    const-string v2, "impression_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "earned"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "friendships"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "create"

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "user_id"

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_6a

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "follow"

    const-string v7, "true"

    invoke-direct {v1, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6a
    if-eqz v2, :cond_79

    const-string v1, "impression_id"

    invoke-static {v4, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_79

    const-string v1, "earned"

    const/4 v2, 0x1

    invoke-static {v4, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_79
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v15

    invoke-virtual {v15}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_fa

    :try_start_9a
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v16

    move-object/from16 v0, v16

    iget-wide v1, v0, Lcom/twitter/android/api/ad;->a:J

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/provider/ae;->b(JI)V

    move-object/from16 v0, v16

    iget-wide v1, v0, Lcom/twitter/android/api/ad;->a:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/provider/ae;->d(J)I

    move-result v1

    move-object/from16 v0, v16

    iput v1, v0, Lcom/twitter/android/api/ad;->y:I

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p1

    move-wide v3, v13

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    const/4 v2, 0x2

    move-object/from16 v1, p1

    move-wide v3, v13

    move-wide v5, v11

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(IJJ)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/service/TwitterService;

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "ADD_USER_SUGGESTION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "soid"

    invoke-virtual {v1, v2, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "user"

    move-object/from16 v0, v16

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/twitter/android/service/TwitterService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_fa
    .catch Ljava/io/IOException; {:try_start_9a .. :try_end_fa} :catch_fb

    :cond_fa
    :goto_fa
    return-object v15

    :catch_fb
    move-exception v1

    const/4 v1, 0x0

    iput v1, v15, Lcom/twitter/android/network/c;->b:I

    goto :goto_fa
.end method

.method private e(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 14

    const/16 v7, 0xc8

    const/4 v6, 0x0

    const-string v0, "status_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p5, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "statuses"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, "destroy"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x400

    invoke-direct {v3, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-static {v4, v5, v2, p4, v3}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->b()Z

    move-result v4

    if-eqz v4, :cond_55

    :try_start_45
    invoke-static {v3}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->e(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ab;

    move-result-object v0

    invoke-virtual {p3, p1, p2, v0}, Lcom/twitter/android/provider/ae;->a(JLcom/twitter/android/api/ab;)V
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_50} :catch_51

    :cond_50
    :goto_50
    return-object v2

    :catch_51
    move-exception v0

    iput v6, v2, Lcom/twitter/android/network/c;->b:I

    goto :goto_50

    :cond_55
    iget v3, v2, Lcom/twitter/android/network/c;->b:I

    const/16 v4, 0x194

    if-ne v3, v4, :cond_50

    invoke-virtual {p3, v0, v1}, Lcom/twitter/android/provider/ae;->a(J)Lcom/twitter/android/api/ab;

    move-result-object v3

    if-eqz v3, :cond_67

    invoke-virtual {p3, p1, p2, v3}, Lcom/twitter/android/provider/ae;->a(JLcom/twitter/android/api/ab;)V

    iput v7, v2, Lcom/twitter/android/network/c;->b:I

    goto :goto_50

    :cond_67
    invoke-virtual {p3, v0, v1, p1, p2}, Lcom/twitter/android/provider/ae;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_50

    iput v7, v2, Lcom/twitter/android/network/c;->b:I

    goto :goto_50
.end method

.method private e(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 14

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "verify_credentials"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, "include_user_entities"

    const/4 v1, 0x1

    invoke-static {v2, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x0

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_60

    :try_start_3a
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v0

    const-string v1, "user"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-wide v2, v0, Lcom/twitter/android/api/ad;->a:J

    invoke-static {p0, v2, v3}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v0

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I
    :try_end_60
    .catch Ljava/io/IOException; {:try_start_3a .. :try_end_60} :catch_61

    :cond_60
    :goto_60
    return-object v10

    :catch_61
    move-exception v0

    const/4 v1, 0x0

    iput v1, v10, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v10, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_60
.end method

.method private e(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 16

    const-wide/16 v1, 0x0

    const/4 v11, 0x1

    const/4 v5, 0x0

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "owner_id"

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v0, "impression_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "earned"

    invoke-virtual {p3, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "friendships"

    aput-object v4, v3, v5

    const-string v4, "destroy"

    aput-object v4, v3, v11

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "user_id"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v5, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_53

    const-string v3, "impression_id"

    invoke-static {v2, v3, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_53

    const-string v0, "earned"

    invoke-static {v2, v0, v11}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_53
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_81

    :try_start_6d
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p1, v6, v7, v1}, Lcom/twitter/android/provider/ae;->c(JI)V

    const/4 v1, 0x0

    iget-wide v4, v0, Lcom/twitter/android/api/ad;->a:J

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/provider/ae;->a(IJJ)V
    :try_end_81
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_81} :catch_82

    :cond_81
    :goto_81
    return-object v10

    :catch_82
    move-exception v0

    sget-boolean v1, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v1, :cond_81

    const-string v1, "TwitterService"

    const-string v2, "Problem processing response."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_81
.end method

.method private f(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v1, "device_follow"

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "friendships"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "update"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v5, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v11, :cond_d7

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device"

    const-string v4, "true"

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_58
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x400

    invoke-direct {v6, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v12

    invoke-virtual {v12}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_d6

    :try_start_75
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v13

    if-eqz v11, :cond_e5

    new-instance v2, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x10

    invoke-virtual {p1, v7, v8, v1}, Lcom/twitter/android/provider/ae;->b(JI)V

    const/16 v5, 0x10

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object v1, p1

    move-wide v3, v9

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;)V

    const-string v1, "account_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/provider/d;->a:Landroid/net/Uri;

    invoke-static {v3, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "notif_tweet"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    if-eqz v2, :cond_d0

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_cd

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_cd

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :cond_cd
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_d0
    if-nez v1, :cond_d6

    const/16 v1, 0x3e9

    iput v1, v12, Lcom/twitter/android/network/c;->b:I
    :try_end_d6
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_d6} :catch_f9

    :cond_d6
    :goto_d6
    return-object v12

    :cond_d7
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device"

    const-string v4, "false"

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_58

    :cond_e5
    const/16 v1, 0x10

    :try_start_e7
    invoke-virtual {p1, v7, v8, v1}, Lcom/twitter/android/provider/ae;->c(JI)V

    const/16 v2, 0x10

    move-object v1, p1

    move-wide v3, v9

    move-wide v5, v7

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(IJJ)V

    const/4 v1, 0x1

    iget-object v2, v13, Lcom/twitter/android/api/ad;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/twitter/android/provider/ae;->a(ILjava/lang/String;)I
    :try_end_f8
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_f8} :catch_f9

    goto :goto_d6

    :catch_f9
    move-exception v1

    const/4 v2, 0x0

    iput v2, v12, Lcom/twitter/android/network/c;->b:I

    iput-object v1, v12, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_d6
.end method

.method private g(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 16

    const-wide/16 v3, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "owner_id"

    invoke-virtual {p3, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v2, "screen_name"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-string v6, "direct_messages"

    aput-object v6, v4, v7

    const-string v6, "new"

    aput-object v6, v4, v10

    invoke-virtual {v2, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".json"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-nez v3, :cond_b6

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "user_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_47
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "text"

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "include_entities"

    const-string v3, "true"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_b5

    :try_start_77
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->g(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/p;

    move-result-object v11

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    move-wide v2, v8

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/provider/ae;->a(Ljava/util/List;JIZZLjava/lang/String;)I

    const-string v0, "user"

    iget-object v1, v11, Lcom/twitter/android/api/p;->e:Lcom/twitter/android/api/ad;

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/service/TwitterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ADD_USER_SUGGESTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "soid"

    invoke-virtual {v0, v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "user"

    iget-object v2, v11, Lcom/twitter/android/api/p;->e:Lcom/twitter/android/api/ad;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/service/TwitterService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_b5
    .catch Ljava/io/IOException; {:try_start_77 .. :try_end_b5} :catch_c1

    :cond_b5
    :goto_b5
    return-object v10

    :cond_b6
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "screen_name"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_47

    :catch_c1
    move-exception v0

    sget-boolean v1, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v1, :cond_b5

    const-string v1, "TwitterService"

    const-string v2, "Problem processing response."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_b5
.end method

.method private h(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 19

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v1, "screen_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;Ljava/lang/String;JLjava/io/ByteArrayOutputStream;)Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_66

    :try_start_2e
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v14

    if-eqz v14, :cond_66

    new-instance v2, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-wide/16 v3, -0x1

    const/4 v5, -0x1

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    iget-wide v1, v14, Lcom/twitter/android/api/ad;->a:J

    cmp-long v1, v11, v1

    if-eqz v1, :cond_5f

    iget-wide v6, v14, Lcom/twitter/android/api/ad;->a:J

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-wide v4, v11

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JJ)Lcom/twitter/android/network/c;

    :cond_5f
    const-string v1, "user"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_66} :catch_67

    :cond_66
    :goto_66
    return-object v13

    :catch_67
    move-exception v1

    const/4 v2, 0x0

    iput v2, v13, Lcom/twitter/android/network/c;->b:I

    iput-object v1, v13, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_66
.end method

.method private i(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 16

    const-wide/16 v1, 0x0

    const/4 v3, 0x2

    const/4 v11, 0x1

    const/4 v5, 0x0

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "owner_id"

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v0, "impression_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "earned"

    invoke-virtual {p3, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "blocks"

    aput-object v4, v3, v5

    const-string v4, "destroy"

    aput-object v4, v3, v11

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "user_id"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v5, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_54

    const-string v3, "impression_id"

    invoke-static {v2, v3, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_54

    const-string v0, "earned"

    invoke-static {v2, v0, v11}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_54
    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_75

    const/4 v1, 0x2

    move-object v0, p1

    move-wide v2, v8

    move-wide v4, v6

    :try_start_6e
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/provider/ae;->a(IJJ)V

    const/4 v0, 0x4

    invoke-virtual {p1, v6, v7, v0}, Lcom/twitter/android/provider/ae;->c(JI)V
    :try_end_75
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_75} :catch_76

    :cond_75
    :goto_75
    return-object v10

    :catch_76
    move-exception v0

    sget-boolean v1, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v1, :cond_75

    const-string v1, "TwitterService"

    const-string v2, "destroyBlock"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_75
.end method

.method private j(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 16

    const-string v0, "owner_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v0, "name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "desc"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "list_mode"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v2, "list_id"

    const-wide/16 v4, 0x0

    invoke-virtual {p3, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_9c

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "lists"

    aput-object v9, v5, v8

    const/4 v8, 0x1

    const-string v9, "create"

    aput-object v9, v5, v8

    invoke-virtual {v2, v5}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ".json"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    :goto_47
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "name"

    invoke-direct {v5, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    packed-switch v3, :pswitch_data_dc

    :goto_54
    if-eqz v1, :cond_60

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "description"

    invoke-direct {v0, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_60
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_9b

    :try_start_7a
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;I)Lcom/twitter/android/api/r;

    move-result-object v9

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JILjava/lang/String;)V

    const-string v0, "list_id"

    invoke-virtual {v9}, Lcom/twitter/android/api/r;->a()J

    move-result-wide v1

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
    :try_end_9b
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_9b} :catch_d5

    :cond_9b
    :goto_9b
    return-object v8

    :cond_9c
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "lists"

    aput-object v11, v5, v10

    const/4 v10, 0x1

    const-string v11, "update"

    aput-object v11, v5, v10

    invoke-virtual {v2, v5}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ".json"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "list_id"

    invoke-static {v2, v5, v8, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto :goto_47

    :pswitch_bb
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "mode"

    const-string v5, "public"

    invoke-direct {v0, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_54

    :pswitch_c8
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "mode"

    const-string v5, "private"

    invoke-direct {v0, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_54

    :catch_d5
    move-exception v0

    const/4 v1, 0x0

    iput v1, v8, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v8, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_9b

    :pswitch_data_dc
    .packed-switch 0x0
        :pswitch_bb
        :pswitch_c8
    .end packed-switch
.end method

.method private k(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 13

    const-wide/16 v3, 0x0

    const/4 v5, 0x2

    const/4 v8, 0x0

    const-string v0, "owner_id"

    invoke-virtual {p3, v0, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "list_id"

    invoke-virtual {p3, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-array v5, v5, [Ljava/lang/Object;

    const-string v6, "lists"

    aput-object v6, v5, v8

    const/4 v6, 0x1

    const-string v7, "destroy"

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "list_id"

    invoke-static {v4, v5, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-static {v6, v7, v4, p2, v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_50

    :try_start_45
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;I)Lcom/twitter/android/api/r;

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/twitter/android/provider/ae;->c(JJ)V
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_50} :catch_51

    :cond_50
    :goto_50
    return-object v4

    :catch_51
    move-exception v0

    iput v8, v4, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v4, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_50
.end method

.method private l(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 23

    const-string v2, "since_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "user_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v15

    const-string v4, "list_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v17

    const-string v4, "max_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v4, "count"

    const/16 v5, 0x64

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v10, "lists"

    aput-object v10, v6, v7

    const/4 v7, 0x1

    const-string v10, "statuses"

    aput-object v10, v6, v7

    invoke-virtual {v4, v6}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ".json"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "list_id"

    move-wide/from16 v0, v17

    invoke-static {v4, v6, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_5f

    const-string v6, "since_id"

    invoke-static {v4, v6, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_5f
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-lez v2, :cond_6a

    const-string v2, "max_id"

    invoke-static {v4, v2, v8, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_6a
    const-string v2, "per_page"

    invoke-static {v4, v2, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v2, "include_entities"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_cards"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_user_entities"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/twitter/android/network/a;->p:Lcom/twitter/android/network/k;

    if-eqz v2, :cond_e7

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x0

    move-object/from16 v5, p2

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    move-object v14, v2

    :goto_9c
    invoke-virtual {v14}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_e6

    :try_start_a6
    invoke-static {v7}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/api/s;->d(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v3

    const-wide/16 v4, 0x0

    cmp-long v2, v8, v4

    if-lez v2, :cond_f6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_f6

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/ab;

    iget-wide v4, v2, Lcom/twitter/android/api/ab;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    :goto_cc
    const-wide/16 v4, 0x0

    cmp-long v2, v8, v4

    if-lez v2, :cond_f8

    const/4 v10, 0x1

    :goto_d3
    const/16 v6, 0x9

    const-wide/16 v4, 0x0

    cmp-long v2, v8, v4

    if-lez v2, :cond_fa

    const/4 v9, 0x1

    :goto_dc
    const/4 v12, 0x1

    const/4 v13, 0x1

    move-object/from16 v2, p1

    move-wide v4, v15

    move-wide/from16 v7, v17

    invoke-virtual/range {v2 .. v13}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I
    :try_end_e6
    .catch Ljava/io/IOException; {:try_start_a6 .. :try_end_e6} :catch_fc

    :cond_e6
    :goto_e6
    return-object v14

    :cond_e7
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5, v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    move-object v14, v2

    goto :goto_9c

    :cond_f6
    const/4 v11, 0x0

    goto :goto_cc

    :cond_f8
    const/4 v10, 0x0

    goto :goto_d3

    :cond_fa
    const/4 v9, 0x0

    goto :goto_dc

    :catch_fc
    move-exception v2

    const/4 v3, 0x0

    iput v3, v14, Lcom/twitter/android/network/c;->b:I

    iput-object v2, v14, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_e6
.end method

.method private m(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 23

    const-string v2, "user_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v2, "list_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v17

    const-string v2, "page"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v2, "user_type"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const/4 v3, 0x1

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v2

    packed-switch v4, :pswitch_data_ee

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Unknown user type: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_45
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "lists"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "members"

    aput-object v10, v8, v9

    invoke-virtual {v3, v8}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ".json"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    :goto_60
    const-string v3, "list_id"

    move-wide/from16 v0, v17

    invoke-static {v10, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string v3, "include_user_entities"

    const/4 v8, 0x1

    invoke-static {v10, v3, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-eqz v2, :cond_74

    const-string v3, "cursor"

    invoke-static {v10, v3, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_74
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v12, 0x0

    move-object/from16 v11, p2

    invoke-static/range {v8 .. v13}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_9f

    :try_start_92
    invoke-static {v13}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/o;

    move-result-object v2

    if-nez v2, :cond_bc

    const/4 v2, 0x0

    iput v2, v3, Lcom/twitter/android/network/c;->b:I
    :try_end_9f
    .catch Ljava/io/IOException; {:try_start_92 .. :try_end_9f} :catch_dc

    :cond_9f
    :goto_9f
    return-object v3

    :pswitch_a0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "lists"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "subscribers"

    aput-object v10, v8, v9

    invoke-virtual {v3, v8}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, ".json"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    goto :goto_60

    :cond_bc
    :try_start_bc
    invoke-virtual {v2}, Lcom/twitter/android/api/o;->b()Ljava/util/ArrayList;

    move-result-object v8

    if-nez v7, :cond_ec

    const-string v14, "-1"

    :goto_c4
    invoke-virtual {v2}, Lcom/twitter/android/api/o;->a()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x1

    move-object/from16 v7, p1

    move-wide v9, v5

    move v11, v4

    move-wide/from16 v12, v17

    invoke-virtual/range {v7 .. v16}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    const-string v4, "count"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_db
    .catch Ljava/io/IOException; {:try_start_bc .. :try_end_db} :catch_dc

    goto :goto_9f

    :catch_dc
    move-exception v2

    const/4 v4, 0x0

    iput v4, v3, Lcom/twitter/android/network/c;->b:I

    sget-boolean v4, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v4, :cond_9f

    const-string v4, "TwitterService"

    const-string v5, "getListUsers"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9f

    :cond_ec
    const/4 v14, 0x0

    goto :goto_c4

    :pswitch_data_ee
    .packed-switch 0x4
        :pswitch_45
        :pswitch_a0
        :pswitch_45
    .end packed-switch
.end method

.method private n(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "list_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v1, "user_type"

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v3, 0x0

    move-object v1, p0

    move-object/from16 v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;Ljava/lang/String;JLjava/io/ByteArrayOutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_6d

    :try_start_3f
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v13

    if-eqz v13, :cond_6d

    packed-switch v12, :pswitch_data_104

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown user type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_3f .. :try_end_61} :catch_61

    :catch_61
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_6d

    const-string v2, "TwitterService"

    const-string v3, "addListUser"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6d
    const/4 v1, 0x0

    :goto_6e
    return-object v1

    :pswitch_6f
    :try_start_6f
    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v6, "lists"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    const-string v6, "members"

    aput-object v6, v2, v3

    const/4 v3, 0x2

    const-string v6, "create"

    aput-object v6, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "list_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v2, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_a9
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object/from16 v4, p2

    move-object v5, v7

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_d5

    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;I)Lcom/twitter/android/api/r;

    move-result-object v6

    move-object v1, p1

    move-wide v2, v8

    move-object v4, v13

    move v5, v12

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/provider/ae;->a(JLcom/twitter/android/api/ad;ILcom/twitter/android/api/r;)V

    :cond_d5
    move-object v1, v7

    goto :goto_6e

    :pswitch_d7
    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "lists"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "subscribers"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "create"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "list_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_103
    .catch Ljava/io/IOException; {:try_start_6f .. :try_end_103} :catch_61

    goto :goto_a9

    :pswitch_data_104
    .packed-switch 0x4
        :pswitch_6f
        :pswitch_d7
    .end packed-switch
.end method

.method private o(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v1, "list_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v3, "user_type"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    packed-switch v12, :pswitch_data_ec

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown user type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_44
    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "lists"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, "members"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    const-string v7, "destroy"

    aput-object v7, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "list_id"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "user_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_7e
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_ac

    packed-switch v12, :pswitch_data_f4

    const/4 v1, 0x0

    :goto_9d
    :try_start_9d
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;I)Lcom/twitter/android/api/r;

    move-result-object v7

    move-object v1, p1

    move v2, v12

    move-wide v3, v8

    move-wide v5, v10

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/android/provider/ae;->a(IJJLcom/twitter/android/api/r;)V
    :try_end_ac
    .catch Ljava/io/IOException; {:try_start_9d .. :try_end_ac} :catch_de

    :cond_ac
    :goto_ac
    return-object v13

    :pswitch_ad
    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "lists"

    aput-object v7, v4, v6

    const/4 v6, 0x1

    const-string v7, "subscribers"

    aput-object v7, v4, v6

    const/4 v6, 0x2

    const-string v7, "destroy"

    aput-object v7, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "list_id"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7e

    :pswitch_da
    const/4 v1, 0x1

    goto :goto_9d

    :pswitch_dc
    const/4 v1, 0x2

    goto :goto_9d

    :catch_de
    move-exception v1

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_ac

    const-string v2, "TwitterService"

    const-string v3, "removeListUser"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_ac

    nop

    :pswitch_data_ec
    .packed-switch 0x4
        :pswitch_44
        :pswitch_ad
    .end packed-switch

    :pswitch_data_f4
    .packed-switch 0x4
        :pswitch_da
        :pswitch_dc
    .end packed-switch
.end method

.method private p(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 14

    const-wide/16 v5, 0x0

    const/4 v3, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    const-string v0, "user_id"

    invoke-virtual {p3, v0, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "list_id"

    invoke-virtual {p3, v2, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v2, "user_type"

    invoke-virtual {p3, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    packed-switch v2, :pswitch_data_94

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unknown user type: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_30
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "lists"

    aput-object v5, v3, v4

    const-string v5, "members"

    aput-object v5, v3, v8

    const-string v5, "show"

    aput-object v5, v3, v9

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    :goto_4a
    const-string v3, "user_id"

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string v0, "list_id"

    invoke-static {v2, v0, v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_89

    invoke-virtual {p1, v6, v7, v8}, Lcom/twitter/android/provider/ae;->a(JZ)V

    :cond_6d
    :goto_6d
    return-object v0

    :pswitch_6e
    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "lists"

    aput-object v5, v3, v4

    const-string v5, "subscribers"

    aput-object v5, v3, v8

    const-string v5, "show"

    aput-object v5, v3, v9

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto :goto_4a

    :cond_89
    iget v1, v0, Lcom/twitter/android/network/c;->b:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_6d

    invoke-virtual {p1, v6, v7, v4}, Lcom/twitter/android/provider/ae;->a(JZ)V

    goto :goto_6d

    nop

    :pswitch_data_94
    .packed-switch 0x4
        :pswitch_30
        :pswitch_6e
    .end packed-switch
.end method

.method private q(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 26

    const-string v2, "owner_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v2, "max_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v2, "since_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v2, "page"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v2, "q"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v2, "q_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v2, "q_type"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v2, "place"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v2, "lang"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v2, "show_user"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    const-string v2, "geo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v2, "locale"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v2, "count"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-gtz v2, :cond_76

    const/16 v2, 0x14

    :cond_76
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v4, v4, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const-string v21, "i"

    aput-object v21, v19, v20

    const/16 v20, 0x1

    const-string v21, "search"

    aput-object v21, v19, v20

    move-object/from16 v0, v19

    invoke-static {v4, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, ".json"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v19, "q"

    move-object/from16 v0, v19

    invoke-static {v4, v0, v15}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v3, v0, :cond_b6

    const-string v19, "result_type"

    const-string v20, "recent"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v4, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b6
    if-eqz v9, :cond_bf

    const-string v19, "lang"

    move-object/from16 v0, v19

    invoke-static {v4, v0, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_bf
    if-eqz v18, :cond_c8

    const-string v9, "locale"

    move-object/from16 v0, v18

    invoke-static {v4, v9, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c8
    const-wide/16 v18, 0x0

    cmp-long v9, v7, v18

    if-eqz v9, :cond_d3

    const-string v9, "since_id"

    invoke-static {v4, v9, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_d3
    const-string v7, "rpp"

    invoke-static {v4, v7, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-wide/16 v7, 0x0

    cmp-long v2, v5, v7

    if-eqz v2, :cond_e3

    const-string v2, "max_id"

    invoke-static {v4, v2, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_e3
    if-eqz v14, :cond_ea

    const-string v2, "show_user"

    invoke-static {v4, v2, v14}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_ea
    if-eqz v17, :cond_f3

    const-string v2, "geocode"

    move-object/from16 v0, v17

    invoke-static {v4, v2, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_f3
    const/4 v2, 0x1

    if-ne v13, v2, :cond_119

    const-string v2, "stories"

    const-string v5, "fetch_stories"

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static {v4, v2, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "related_queries"

    const/4 v5, 0x1

    invoke-static {v4, v2, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "spelling_corrections"

    const/4 v5, 0x1

    invoke-static {v4, v2, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/4 v2, 0x5

    if-eq v3, v2, :cond_119

    const-string v2, "pc"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_119
    const-string v2, "earned"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_entities"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_cards"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "events"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v2, "include_user_entities"

    const/4 v3, 0x1

    invoke-static {v4, v2, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v6, 0x0

    move-object/from16 v5, p2

    invoke-static/range {v2 .. v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/twitter/android/network/c;->b()Z

    move-result v2

    if-eqz v2, :cond_1bf

    :try_start_155
    invoke-static {v7}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/api/s;->t(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/y;

    move-result-object v14

    const/4 v2, 0x1

    if-ne v13, v2, :cond_167

    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/twitter/android/provider/ae;->a(I)I

    :cond_167
    iget-object v2, v14, Lcom/twitter/android/api/y;->b:Ljava/util/ArrayList;

    if-eqz v2, :cond_183

    iget-object v2, v14, Lcom/twitter/android/api/y;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_183

    iget-object v2, v14, Lcom/twitter/android/api/y;->b:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/api/TwitterStory;

    const-string v3, "stories"

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_183
    const/16 v4, 0xd

    iget-object v8, v14, Lcom/twitter/android/api/y;->a:Ljava/util/ArrayList;

    sget-object v9, Lcom/twitter/android/provider/z;->o:Landroid/net/Uri;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move v5, v13

    move-wide v6, v10

    invoke-direct/range {v2 .. v9}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;IIJLjava/util/ArrayList;Landroid/net/Uri;)V

    iget-object v2, v14, Lcom/twitter/android/api/y;->c:Ljava/util/ArrayList;

    iget-object v3, v14, Lcom/twitter/android/api/y;->d:Ljava/util/ArrayList;

    if-eqz v2, :cond_19f

    const-string v4, "related_queries"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_19f
    if-eqz v3, :cond_1a8

    const-string v2, "spelling_corrections"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_1a8
    new-instance v2, Lcom/twitter/android/api/x;

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v3, v16

    move-object v4, v15

    invoke-direct/range {v2 .. v14}, Lcom/twitter/android/api/x;-><init>(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/android/api/PromotedContent;)V

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/provider/ae;->a(Lcom/twitter/android/api/x;I)J
    :try_end_1bf
    .catch Ljava/io/IOException; {:try_start_155 .. :try_end_1bf} :catch_1c0

    :cond_1bf
    :goto_1bf
    return-object v17

    :catch_1c0
    move-exception v2

    const/4 v3, 0x0

    move-object/from16 v0, v17

    iput v3, v0, Lcom/twitter/android/network/c;->b:I

    move-object/from16 v0, v17

    iput-object v2, v0, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    sget-boolean v3, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v3, :cond_1bf

    const-string v3, "TwitterService"

    const-string v4, "searchTweets"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1bf
.end method

.method private r(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 18

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v1, "max_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "since_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v1, "event_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "page"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v1, "user"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/twitter/android/api/ad;

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v1, v1, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v8, "i"

    aput-object v8, v3, v6

    const/4 v6, 0x1

    const-string v8, "search"

    aput-object v8, v3, v6

    const/4 v6, 0x2

    const-string v8, "image_facets"

    aput-object v8, v3, v6

    invoke-static {v1, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v1, "q"

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    cmp-long v1, v11, v1

    if-eqz v1, :cond_6a

    const-string v1, "since_id"

    invoke-static {v3, v1, v11, v12}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_6a
    const-wide/16 v1, 0x0

    cmp-long v1, v4, v1

    if-eqz v1, :cond_75

    const-string v1, "max_id"

    invoke-static {v3, v1, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_75
    const-string v1, "is_event"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v1, "include_entities"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x400

    invoke-direct {v6, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v11

    invoke-virtual {v11}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_b9

    :try_start_9f
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v7, v2, v3}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;Lcom/twitter/android/api/ad;ZZ)Ljava/util/ArrayList;

    move-result-object v7

    const/16 v1, 0x12

    invoke-virtual {p1, v1}, Lcom/twitter/android/provider/ae;->a(I)I

    const/16 v3, 0x12

    sget-object v8, Lcom/twitter/android/provider/z;->u:Landroid/net/Uri;

    move-object v1, p0

    move-object v2, p1

    move v4, v13

    move-wide v5, v9

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;IIJLjava/util/ArrayList;Landroid/net/Uri;)V
    :try_end_b9
    .catch Ljava/io/IOException; {:try_start_9f .. :try_end_b9} :catch_ba

    :cond_b9
    :goto_b9
    return-object v11

    :catch_ba
    move-exception v1

    const/4 v2, 0x0

    iput v2, v11, Lcom/twitter/android/network/c;->b:I

    iput-object v1, v11, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_b9
.end method

.method private s(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 21

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v1, "max_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v1, "since_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const-string v1, "event_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "page"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    const-string v1, "parse_statuses"

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    const-string v1, "parse_users"

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    const-string v1, "user"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/twitter/android/api/ad;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v1, v1, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v15, "i"

    aput-object v15, v3, v6

    const/4 v6, 0x1

    const-string v15, "events"

    aput-object v15, v3, v6

    invoke-static {v1, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ".json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v1, "q"

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v1, 0x0

    cmp-long v1, v8, v1

    if-eqz v1, :cond_79

    const-string v1, "since_id"

    invoke-static {v3, v1, v8, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_79
    const-wide/16 v1, 0x0

    cmp-long v1, v4, v1

    if-eqz v1, :cond_84

    const-string v1, "max_id"

    invoke-static {v3, v1, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_84
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v15

    invoke-virtual {v15}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_121

    :try_start_a2
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1, v7}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;Lcom/twitter/android/api/ad;)Lcom/twitter/android/api/q;

    move-result-object v16

    const/4 v1, 0x1

    if-ne v10, v1, :cond_122

    const/16 v1, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/ae;->a(I)I

    const/16 v1, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/twitter/android/provider/ae;->b(I)I

    :cond_bb
    :goto_bb
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/twitter/android/api/q;->c:Lcom/twitter/android/api/ad;

    if-eqz v1, :cond_cc

    const-string v1, "event_mediator"

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/android/api/q;->c:Lcom/twitter/android/api/ad;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_cc
    if-eqz v13, :cond_df

    const/16 v3, 0x10

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/twitter/android/api/q;->a:Ljava/util/ArrayList;

    sget-object v8, Lcom/twitter/android/provider/z;->p:Landroid/net/Uri;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move v4, v10

    move-wide v5, v11

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;IIJLjava/util/ArrayList;Landroid/net/Uri;)V

    :cond_df
    move-object/from16 v0, v16

    iget-object v1, v0, Lcom/twitter/android/api/q;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_121

    if-eqz v14, :cond_121

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/android/api/q;->b:Ljava/util/ArrayList;

    const/16 v5, 0x11

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p1

    move-wide v3, v11

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/android/api/q;->b:Ljava/util/ArrayList;

    const/16 v5, 0x11

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object/from16 v1, p1

    move-wide v3, v11

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;)V

    const-string v1, "count"

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/android/api/q;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "users"

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/twitter/android/api/q;->b:Ljava/util/ArrayList;

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_121
    :goto_121
    return-object v15

    :cond_122
    if-nez v10, :cond_bb

    const/16 v1, 0x10

    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/provider/ae;->a(II)I
    :try_end_12c
    .catch Ljava/io/IOException; {:try_start_a2 .. :try_end_12c} :catch_12d

    goto :goto_bb

    :catch_12d
    move-exception v1

    const/4 v2, 0x0

    iput v2, v15, Lcom/twitter/android/network/c;->b:I

    iput-object v1, v15, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    sget-boolean v2, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v2, :cond_121

    const-string v2, "TwitterService"

    const-string v3, "fetchEvent"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_121
.end method

.method private t(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 20

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v1, "q"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v1, "count"

    const/16 v2, 0x14

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v1, "page"

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "users"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "search"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v2, "q"

    invoke-static {v3, v2, v12}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "per_page"

    invoke-static {v3, v2, v13}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    if-lez v1, :cond_51

    const-string v2, "page"

    invoke-static {v3, v2, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_51
    const-string v1, "include_user_entities"

    const/4 v2, 0x1

    invoke-static {v3, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/twitter/android/network/a;->p:Lcom/twitter/android/network/k;

    if-eqz v1, :cond_b3

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v1

    move-object v9, v1

    :goto_76
    invoke-virtual {v9}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_b2

    :try_start_7c
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v14

    const-string v15, "search_id"

    new-instance v1, Lcom/twitter/android/api/x;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-object v2, v12

    move-object v3, v12

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/api/x;-><init>(Ljava/lang/String;Ljava/lang/String;JJLcom/twitter/android/api/PromotedContent;)V

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v13, :cond_c7

    const/4 v7, 0x1

    :goto_99
    move-object/from16 v2, p1

    move-object v3, v1

    move-object v4, v14

    move-wide v5, v10

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/android/provider/ae;->a(Lcom/twitter/android/api/x;Ljava/util/List;JZ)J

    move-result-wide v1

    move-object/from16 v0, p3

    invoke-virtual {v0, v15, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "count"

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_b2
    .catch Ljava/io/IOException; {:try_start_7c .. :try_end_b2} :catch_c9

    :cond_b2
    :goto_b2
    return-object v9

    :cond_b3
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;

    move-result-object v1

    move-object v9, v1

    goto :goto_76

    :cond_c7
    const/4 v7, 0x0

    goto :goto_99

    :catch_c9
    move-exception v1

    const/4 v1, 0x0

    iput v1, v9, Lcom/twitter/android/network/c;->b:I

    goto :goto_b2
.end method

.method private u(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 16

    const/4 v9, 0x1

    const-string v0, "name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "desc"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "url"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "place"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "account_name"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "account"

    aput-object v8, v4, v7

    const-string v7, "update_profile"

    aput-object v7, v4, v9

    invoke-virtual {v2, v4}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ".json"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_48

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "name"

    invoke-direct {v7, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_48
    if-eqz v3, :cond_54

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "url"

    invoke-direct {v0, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_54
    if-eqz v5, :cond_60

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "location"

    invoke-direct {v0, v3, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_60
    if-eqz v1, :cond_6c

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "description"

    invoke-direct {v0, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6c
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/android/network/c;->b()Z

    move-result v0

    if-eqz v0, :cond_ab

    :try_start_86
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v11

    const/4 v0, 0x0

    invoke-static {p0, v6, v11, v0}, Lcom/twitter/android/platform/j;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/android/api/ad;Lcom/twitter/android/api/ah;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object v0, p1

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    const-string v0, "user"

    invoke-virtual {p3, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_ab
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_ab} :catch_ac

    :cond_ab
    :goto_ab
    return-object v10

    :catch_ac
    move-exception v0

    sget-boolean v1, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v1, :cond_ab

    const-string v1, "TwitterService"

    const-string v2, "updateProfile"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_ab
.end method

.method private v(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 19

    const-string v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v1, "since_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    const-string v3, "max_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v3, "count"

    const/16 v4, 0x64

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v3, "i_type"

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const/4 v3, 0x1

    if-ne v3, v13, :cond_f8

    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v3, v3, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "i"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "activity"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "by_friends"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    :goto_54
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-nez v4, :cond_66

    const-wide/16 v4, 0x0

    cmp-long v4, v11, v4

    if-nez v4, :cond_66

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/twitter/android/provider/ae;->d(I)J

    move-result-wide v1

    :cond_66
    const-string v4, "include_entities"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v4, "include_user_entities"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v4, "include_cards"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-lez v7, :cond_7f

    const-string v4, "count"

    invoke-static {v3, v4, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_7f
    const-wide/16 v4, 0x0

    cmp-long v4, v11, v4

    if-lez v4, :cond_8a

    const-string v4, "max_id"

    invoke-static {v3, v4, v11, v12}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_8a
    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-lez v4, :cond_95

    const-string v4, "since_id"

    invoke-static {v3, v4, v1, v2}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_95
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x400

    invoke-direct {v6, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v5, 0x0

    move-object/from16 v4, p2

    invoke-static/range {v1 .. v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;

    move-result-object v14

    const/4 v8, 0x0

    invoke-virtual {v14}, Lcom/twitter/android/network/c;->b()Z

    move-result v1

    if-eqz v1, :cond_152

    :try_start_b5
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/api/s;->s(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v2

    const-wide/16 v3, 0x0

    cmp-long v1, v11, v3

    if-lez v1, :cond_11a

    const/4 v6, 0x1

    :goto_c4
    if-eqz v6, :cond_11c

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v1, v7, :cond_11c

    const/4 v7, 0x1

    :goto_cd
    move-object/from16 v1, p1

    move-wide v3, v9

    move v5, v13

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;JIZZ)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_d8
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_d8} :catch_145

    move-result v3

    if-nez v13, :cond_13c

    if-nez v6, :cond_13c

    if-lez v3, :cond_13c

    const/4 v2, 0x0

    :try_start_e0
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_e4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_12d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/n;

    iget v1, v1, Lcom/twitter/android/api/n;->a:I
    :try_end_f2
    .catch Ljava/io/IOException; {:try_start_e0 .. :try_end_f2} :catch_14e

    packed-switch v1, :pswitch_data_154

    move v1, v2

    :goto_f6
    move v2, v1

    goto :goto_e4

    :cond_f8
    iget-object v3, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v3, v3, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "i"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "activity"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "about_me"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    goto/16 :goto_54

    :cond_11a
    const/4 v6, 0x0

    goto :goto_c4

    :cond_11c
    const/4 v7, 0x0

    goto :goto_cd

    :pswitch_11e
    or-int/lit8 v2, v2, 0x1

    goto :goto_e4

    :pswitch_121
    or-int/lit8 v2, v2, 0x2

    goto :goto_e4

    :pswitch_124
    or-int/lit8 v2, v2, 0x4

    goto :goto_e4

    :pswitch_127
    or-int/lit8 v2, v2, 0x8

    goto :goto_e4

    :pswitch_12a
    or-int/lit8 v1, v2, 0x10

    goto :goto_f6

    :cond_12d
    :try_start_12d
    invoke-static {p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v1

    const-string v4, "account_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Lcom/twitter/android/provider/a;->a(Ljava/lang/String;I)I
    :try_end_13c
    .catch Ljava/io/IOException; {:try_start_12d .. :try_end_13c} :catch_14e

    :cond_13c
    move v1, v3

    :goto_13d
    const-string v2, "count"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    return-object v14

    :catch_145
    move-exception v1

    move-object v2, v1

    move v1, v8

    :goto_148
    const/4 v3, 0x0

    iput v3, v14, Lcom/twitter/android/network/c;->b:I

    iput-object v2, v14, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_13d

    :catch_14e
    move-exception v1

    move-object v2, v1

    move v1, v3

    goto :goto_148

    :cond_152
    move v1, v8

    goto :goto_13d

    :pswitch_data_154
    .packed-switch 0x1
        :pswitch_121
        :pswitch_11e
        :pswitch_11e
        :pswitch_127
        :pswitch_124
        :pswitch_12a
    .end packed-switch
.end method

.method private w(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;
    .registers 15

    const/4 v10, 0x2

    const-wide/high16 v5, 0x7ff8

    const/4 v9, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v0, v0, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    new-array v1, v10, [Ljava/lang/Object;

    const-string v2, "i"

    aput-object v2, v1, v4

    const-string v2, "discovery"

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v0, "lat"

    invoke-virtual {p3, v0, v5, v6}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v0

    const-string v3, "long"

    invoke-virtual {p3, v3, v5, v6}, Landroid/content/Intent;->getDoubleExtra(Ljava/lang/String;D)D

    move-result-wide v5

    const-string v3, "soid"

    const-wide/16 v7, 0x0

    invoke-virtual {p3, v3, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_47

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_47

    const-string v3, "lat"

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    const-string v0, "long"

    invoke-static {v2, v0, v5, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_47
    const-string v0, "lang"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_58

    const-string v1, "lang"

    invoke-static {v2, v1, v0}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_58
    const-string v0, "schema"

    invoke-static {v2, v0, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v0, "include_user_entities"

    invoke-static {v2, v0, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    const/16 v0, 0x400

    invoke-direct {v5, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static {p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/twitter/android/network/c;->a(I)Lcom/twitter/android/network/c;

    move-result-object v1

    :try_start_78
    invoke-static {v5}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/jackson/JsonParser;->a()Lorg/codehaus/jackson/JsonToken;

    invoke-static {v0}, Lcom/twitter/android/api/s;->u(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1, v0, v7, v8}, Lcom/twitter/android/provider/ae;->a(Ljava/util/List;J)Ljava/util/BitSet;

    move-result-object v0

    const-string v2, "unread_story_states"

    invoke-virtual {p3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "unread_stories"

    invoke-virtual {v0}, Ljava/util/BitSet;->cardinality()I

    move-result v0

    invoke-virtual {p3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_95
    .catch Ljava/io/IOException; {:try_start_78 .. :try_end_95} :catch_96

    :goto_95
    return-object v1

    :catch_96
    move-exception v0

    iput v4, v1, Lcom/twitter/android/network/c;->b:I

    iput-object v0, v1, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_95
.end method


# virtual methods
.method final a(I)V
    .registers 5

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->f:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_18

    :goto_30
    return-void

    :cond_31
    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/service/TwitterService;->stopSelf(I)V

    goto :goto_3b

    :cond_4f
    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    goto :goto_30
.end method

.method final a(Landroid/content/Intent;I)V
    .registers 31

    if-nez p1, :cond_3

    :goto_2
    return-void

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    sget-object v3, Lcom/twitter/android/service/TwitterService;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Ljava/lang/Integer;

    new-instance v8, Lcom/twitter/android/network/a;

    const-string v3, "auth"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/network/k;

    invoke-direct {v8, v3}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    const-string v3, "account_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v3, "soid"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/twitter/android/provider/ae;->a(Landroid/content/Context;J)Lcom/twitter/android/provider/ae;

    move-result-object v3

    if-nez v6, :cond_cf

    const/4 v6, 0x0

    move/from16 v25, v6

    :goto_3c
    const/16 v24, 0x0

    const/16 v23, 0x0

    const/16 v22, 0x0

    const/16 v16, 0x0

    sparse-switch v25, :sswitch_data_1350

    sget-boolean v3, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v3, :cond_5f

    const-string v3, "TwitterService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Unknown action: "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5f
    move-object/from16 v8, v16

    :goto_61
    if-eqz v8, :cond_1331

    iget v7, v8, Lcom/twitter/android/network/c;->b:I

    iget-object v6, v8, Lcom/twitter/android/network/c;->c:Ljava/lang/String;

    iget-object v3, v8, Lcom/twitter/android/network/c;->e:Lcom/twitter/android/api/i;

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v8}, Lcom/twitter/android/service/ScribeService;->a(Landroid/content/Context;JLcom/twitter/android/network/c;)V

    move-object v4, v6

    move v5, v7

    :goto_70
    sget-boolean v6, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v6, :cond_92

    const-string v6, "TwitterService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Action complete: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_92
    const-string v6, "ibinder"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_ba

    const-string v6, "action_code"

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "resp_code"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "reason_phrase"

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "rate_limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_ba
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->a:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/service/TwitterService;->a:Landroid/os/Handler;

    move/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v4, v0, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_2

    :cond_cf
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move/from16 v25, v6

    goto/16 :goto_3c

    :sswitch_d7
    new-instance v6, Lcom/twitter/android/platform/c;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/twitter/android/platform/c;-><init>(Landroid/content/Context;)V

    new-instance v7, Landroid/content/SyncResult;

    invoke-direct {v7}, Landroid/content/SyncResult;-><init>()V

    const-string v3, "account"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    const-string v8, "_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    invoke-virtual {v6, v9, v3, v8, v7}, Lcom/twitter/android/platform/c;->a(Lcom/twitter/android/network/d;Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_100
    const-string v6, "i_type"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v7, "status_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v9

    const-wide/16 v7, -0x1

    invoke-virtual/range {v3 .. v9}, Lcom/twitter/android/provider/ae;->a(JIJ[J)V

    if-nez v9, :cond_5f

    sparse-switch v6, :sswitch_data_14a2

    :goto_11b
    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_11f
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v3

    const-string v6, "tweet"

    const/4 v7, 0x0

    invoke-virtual {v3, v10, v6, v7}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_12d
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/twitter/android/provider/ae;->f(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v3

    const-string v6, "mention"

    const/4 v7, 0x0

    invoke-virtual {v3, v10, v6, v7}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    goto :goto_11b

    :sswitch_13c
    invoke-virtual {v3}, Lcom/twitter/android/provider/ae;->c()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v3

    const-string v6, "message"

    const/4 v7, 0x0

    invoke-virtual {v3, v10, v6, v7}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_14d
    const-string v6, "owner_id"

    const-wide/16 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/twitter/android/provider/ae;->f(J)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_15e
    move-object/from16 v0, p0

    invoke-static {v0, v10}, Lcom/twitter/android/platform/j;->b(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v10}, Lcom/twitter/android/service/TwitterService;->a(JLjava/lang/String;)V

    invoke-virtual {v3}, Lcom/twitter/android/provider/ae;->b()V

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/twitter/android/util/g;->a(Landroid/content/Context;J)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_174
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v10}, Lcom/twitter/android/service/TwitterService;->a(JLjava/lang/String;)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_17d
    const-string v6, "user_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v6, "status_type"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    const-string v6, "max_id"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v16

    const-string v6, "status_id"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v14

    const-string v6, "since_id"

    const-wide/16 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    const-string v6, "count"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    const/16 v19, 0x1

    move-object/from16 v6, p0

    move-object v7, v3

    move-object/from16 v20, p1

    invoke-direct/range {v6 .. v20}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JIJJJIZLandroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1c5
    move-object/from16 v9, p0

    move-wide v10, v4

    move-object v12, v3

    move-object v13, v8

    move-object/from16 v14, p1

    invoke-direct/range {v9 .. v14}, Lcom/twitter/android/service/TwitterService;->a(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1d3
    move-object/from16 v9, p0

    move-wide v10, v4

    move-object v12, v3

    move-object v13, v8

    move-object/from16 v14, p1

    invoke-direct/range {v9 .. v14}, Lcom/twitter/android/service/TwitterService;->b(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1e1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1ec
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1f7
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->b(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_202
    move-object/from16 v9, p0

    move-wide v10, v4

    move-object v12, v3

    move-object v13, v8

    move-object/from16 v14, p1

    invoke-direct/range {v9 .. v14}, Lcom/twitter/android/service/TwitterService;->c(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_210
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->c(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_21b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->d(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_226
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->e(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_231
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->f(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_23c
    const-string v6, "user_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    const-string v6, "max_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v17

    const-string v6, "since_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v15

    const-string v6, "count"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    const/16 v6, 0x10

    move/from16 v0, v25

    if-ne v0, v6, :cond_277

    const/4 v11, 0x1

    move-object/from16 v9, p0

    move-object v10, v3

    move-object v12, v8

    move-object/from16 v20, p1

    invoke-direct/range {v9 .. v20}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;ILcom/twitter/android/network/a;JJJILandroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :cond_277
    const/4 v11, 0x0

    const/16 v20, 0x0

    move-object/from16 v9, p0

    move-object v10, v3

    move-object v12, v8

    invoke-direct/range {v9 .. v20}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;ILcom/twitter/android/network/a;JJJILandroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_285
    const-string v6, "user_id"

    const-wide/16 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v8, "unread_tweet"

    const/4 v9, 0x0

    invoke-virtual {v3, v6, v7, v9}, Lcom/twitter/android/provider/ae;->f(JI)I

    move-result v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v8, "unread_mention"

    const/4 v9, 0x5

    invoke-virtual {v3, v6, v7, v9}, Lcom/twitter/android/provider/ae;->f(JI)I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v6, "unread_dm"

    invoke-virtual {v3}, Lcom/twitter/android/provider/ae;->d()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_2b6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->g(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_2c1
    const-string v6, "message_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const/4 v11, 0x1

    move-object/from16 v6, p0

    move-object v7, v3

    invoke-direct/range {v6 .. v11}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JZ)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_2d6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/twitter/android/service/TwitterService;->c(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_2e1
    const-string v6, "owner_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v6, "user_id"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    move-object/from16 v6, p0

    move-object v7, v3

    invoke-direct/range {v6 .. v12}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JJ)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_2ff
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->h(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_30a
    const-string v6, "android.intent.extra.TEXT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v12

    check-cast v12, Lcom/twitter/android/api/TweetEntities;

    const-string v6, "status_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    move-object/from16 v6, p0

    move-object v7, v3

    move-wide v9, v4

    move-object/from16 v15, p1

    invoke-direct/range {v6 .. v15}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JLjava/lang/String;Lcom/twitter/android/api/TweetEntities;JLandroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_333
    move-object/from16 v9, p0

    move-wide v10, v4

    move-object v12, v3

    move-object v13, v8

    move-object/from16 v14, p1

    invoke-direct/range {v9 .. v14}, Lcom/twitter/android/service/TwitterService;->d(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_341
    move-object/from16 v9, p0

    move-wide v10, v4

    move-object v12, v3

    move-object v13, v8

    move-object/from16 v14, p1

    invoke-direct/range {v9 .. v14}, Lcom/twitter/android/service/TwitterService;->e(JLcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_34f
    const/16 v6, 0x1c

    move/from16 v0, v25

    if-ne v0, v6, :cond_361

    const/4 v6, 0x1

    :goto_356
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1, v6}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;Z)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :cond_361
    const/4 v6, 0x0

    goto :goto_356

    :sswitch_363
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->i(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_36e
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->j(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_379
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->k(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_384
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->l(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_38f
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->m(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_39a
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->n(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3a5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->o(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3b0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->p(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3bb
    const-string v6, "locale"

    const-wide/16 v9, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8, v6, v7}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;J)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3ce
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->q(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3d9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->t(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3e4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1, v10}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;Ljava/lang/String;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3ef
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/twitter/android/service/TwitterService;->d(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_3fa
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/twitter/android/service/TwitterService;->e(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_405
    const-string v3, "screen_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "pass"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v6, v1}, Lcom/twitter/android/service/TwitterService;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_420
    const-string v3, "log_ev"

    const/4 v6, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    const/4 v3, -0x1

    if-eq v9, v3, :cond_5f

    const-string v3, "impression_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v3, "trend_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v3, "earned"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v13}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;ILjava/lang/String;JZ)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_450
    const-string v6, "max_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v15

    const-string v6, "since_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    const-string v6, "count"

    const/16 v7, 0x64

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    const-string v6, "user"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v11

    check-cast v11, Lcom/twitter/android/api/ad;

    move-object/from16 v9, p0

    move-object v10, v3

    move-object v12, v8

    move-object/from16 v18, p1

    invoke-direct/range {v9 .. v18}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/api/ad;Lcom/twitter/android/network/a;JJILandroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_485
    const-string v6, "status_id"

    const-wide/16 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v6, "android.intent.extra.TEXT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v6, "android.intent.extra.UID"

    const-wide/16 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    const-string v6, "_data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v14

    check-cast v14, Lcom/twitter/android/api/TweetEntities;

    move-object v6, v3

    move-wide v10, v4

    invoke-virtual/range {v6 .. v14}, Lcom/twitter/android/provider/ae;->a(JLjava/lang/String;JJLcom/twitter/android/api/TweetEntities;)J

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_4b4
    const-string v6, "status_id"

    const-wide/16 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v8, 0x1

    new-array v8, v8, [J

    const/4 v9, 0x0

    aput-wide v6, v8, v9

    invoke-virtual {v3, v8}, Lcom/twitter/android/provider/ae;->a([J)I

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_4cb
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v7, Lcom/twitter/android/provider/z;->l:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v10, "ownerId"

    invoke-virtual {v7, v10, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v10

    const/4 v6, 0x4

    new-array v11, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "g_status_id"

    aput-object v7, v11, v6

    const/4 v6, 0x1

    const-string v7, "content"

    aput-object v7, v11, v6

    const/4 v6, 0x2

    const-string v7, "entities"

    aput-object v7, v11, v6

    const/4 v6, 0x3

    const-string v7, "in_r_status_id"

    aput-object v7, v11, v6

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-string v14, "updated_at ASC"

    invoke-virtual/range {v9 .. v14}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    if-eqz v17, :cond_5f

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getCount()I

    move-result v6

    new-array v0, v6, [J

    move-object/from16 v18, v0

    :goto_510
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_546

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    aput-wide v9, v18, v6

    const/4 v6, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v6, 0x2

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/android/api/TweetEntities;->a([B)Lcom/twitter/android/api/TweetEntities;

    move-result-object v12

    const/4 v6, 0x3

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    move-object/from16 v6, p0

    move-object v7, v3

    move-wide v9, v4

    move-object/from16 v15, p1

    invoke-direct/range {v6 .. v15}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JLjava/lang/String;Lcom/twitter/android/api/TweetEntities;JLandroid/content/Intent;)Lcom/twitter/android/network/c;

    goto :goto_510

    :cond_546
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/twitter/android/provider/ae;->b([J)I

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_552
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "saved_searches"

    aput-object v10, v7, v9

    invoke-virtual {v6, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v14}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_1340

    :try_start_585
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v7

    invoke-static {v7}, Lcom/twitter/android/api/s;->m(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v7

    const/4 v8, 0x6

    invoke-virtual {v3, v7, v8}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;I)I
    :try_end_591
    .catch Ljava/io/IOException; {:try_start_585 .. :try_end_591} :catch_594

    move-object v8, v6

    goto/16 :goto_61

    :catch_594
    move-exception v3

    sget-boolean v7, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v7, :cond_5a0

    const-string v7, "TwitterService"

    const-string v8, "getSavedSearches"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_5a0
    move-object v8, v6

    goto/16 :goto_61

    :sswitch_5a3
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "users"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "suggestions"

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "lang"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "locale"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v7, :cond_5e2

    if-eqz v8, :cond_5e2

    const-string v9, "lang"

    invoke-static {v6, v9, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "country"

    invoke-static {v6, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "localize"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_5e2
    const-string v7, "samples"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v7, "sample_size"

    const-string v8, "large"

    invoke-static {v6, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "include_user_entities"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v10, 0x0

    invoke-static {v8, v9, v6, v10, v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_133c

    :try_start_611
    invoke-static {v7}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/android/api/s;->o(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v17

    const-string v6, "count"

    const/4 v7, 0x7

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v7}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;I)I

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_630
    :goto_630
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_656

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/api/x;

    iget-object v9, v6, Lcom/twitter/android/api/x;->i:Ljava/util/ArrayList;

    if-eqz v9, :cond_630

    iget-object v6, v6, Lcom/twitter/android/api/x;->i:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_645
    .catch Ljava/io/IOException; {:try_start_611 .. :try_end_645} :catch_646

    goto :goto_630

    :catch_646
    move-exception v3

    sget-boolean v6, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v6, :cond_652

    const-string v6, "TwitterService"

    const-string v7, "Parsing suggestions"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_652
    move-object/from16 v8, v16

    goto/16 :goto_61

    :cond_656
    :try_start_656
    const-string v6, "owner_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const/4 v10, -0x1

    const-wide/16 v11, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x1

    move-object v6, v3

    invoke-virtual/range {v6 .. v15}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_66e
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_685

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/api/x;

    iget-object v7, v6, Lcom/twitter/android/api/x;->i:Ljava/util/ArrayList;

    const/4 v10, 0x6

    iget-wide v11, v6, Lcom/twitter/android/api/x;->g:J

    const/4 v13, 0x0

    move-object v6, v3

    invoke-virtual/range {v6 .. v13}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;)V

    goto :goto_66e

    :cond_685
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v6, Lcom/twitter/android/provider/x;->a:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_68f
    .catch Ljava/io/IOException; {:try_start_656 .. :try_end_68f} :catch_646

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_693
    const-string v6, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "users"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "suggestions"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    aput-object v6, v9, v10

    invoke-virtual {v7, v9}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v6, "include_user_entities"

    const/4 v7, 0x1

    invoke-static {v11, v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v14}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_133c

    :try_start_6dc
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/android/api/s;->p(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v7

    if-nez v7, :cond_6ef

    const/4 v3, 0x0

    move-object/from16 v0, v16

    iput v3, v0, Lcom/twitter/android/network/c;->b:I

    move-object/from16 v8, v16

    goto/16 :goto_61

    :cond_6ef
    const-string v6, "owner_id"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    const/4 v10, 0x6

    const-string v6, "list_id"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    const-string v13, "-1"

    const-string v14, "0"

    const/4 v15, 0x1

    move-object v6, v3

    invoke-virtual/range {v6 .. v15}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    const-string v6, "count"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_715
    .catch Ljava/io/IOException; {:try_start_6dc .. :try_end_715} :catch_719

    move-object/from16 v8, v16

    goto/16 :goto_61

    :catch_719
    move-exception v3

    sget-boolean v6, Lcom/twitter/android/service/TwitterService;->b:Z

    if-eqz v6, :cond_725

    const-string v6, "TwitterService"

    const-string v7, "Parse slug users"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_725
    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_729
    const-string v6, "page"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v7, "user_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v15

    const-string v7, "limit"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v9, "user_type"

    const/16 v10, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "users"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "recommendations"

    aput-object v12, v10, v11

    invoke-virtual {v9, v10}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v9, "connections"

    const-string v10, "connections"

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    invoke-static {v11, v9, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-wide/16 v9, 0x0

    cmp-long v9, v15, v9

    if-lez v9, :cond_7e0

    const-string v9, "user_id"

    move-wide v0, v15

    invoke-static {v11, v9, v0, v1}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string v9, "display_location"

    const-string v10, "st-component"

    invoke-static {v11, v9, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_78b
    if-lez v7, :cond_792

    const-string v9, "limit"

    invoke-static {v11, v9, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_792
    const-string v7, "pc"

    const/4 v9, 0x1

    invoke-static {v11, v7, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v7, "include_user_entities"

    const/4 v9, 0x1

    invoke-static {v11, v7, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v14}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_134d

    :try_start_7bb
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v7

    invoke-static {v7}, Lcom/twitter/android/api/s;->w(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v7

    const-wide/16 v8, 0x0

    cmp-long v8, v15, v8

    if-lez v8, :cond_7e8

    move-wide v8, v15

    :goto_7ca
    if-nez v6, :cond_7ea

    const-string v11, "-1"

    :goto_7ce
    const/4 v12, 0x0

    move-object v6, v3

    move/from16 v10, v17

    invoke-virtual/range {v6 .. v12}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;JILjava/lang/String;Ljava/lang/String;)I

    move-result v3

    const-string v6, "count"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_7dd
    .catch Ljava/io/IOException; {:try_start_7bb .. :try_end_7dd} :catch_7ec

    move-object v8, v13

    goto/16 :goto_61

    :cond_7e0
    const-string v9, "display_location"

    const-string v10, "wtf-component"

    invoke-static {v11, v9, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_78b

    :cond_7e8
    move-wide v8, v4

    goto :goto_7ca

    :cond_7ea
    const/4 v11, 0x0

    goto :goto_7ce

    :catch_7ec
    move-exception v3

    const/4 v3, 0x0

    iput v3, v13, Lcom/twitter/android/network/c;->b:I

    move-object v8, v13

    goto/16 :goto_61

    :sswitch_7f3
    const-string v3, "screen_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "lists"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "show"

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "slug"

    invoke-static {v7, v8, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "owner_screen_name"

    invoke-static {v7, v6, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v6, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v9, 0x0

    invoke-static {v3, v8, v7, v9, v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_1343

    :try_start_844
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/twitter/android/api/s;->b(Lorg/codehaus/jackson/JsonParser;I)Lcom/twitter/android/api/r;

    move-result-object v6

    const-string v7, "list_id"

    invoke-virtual {v6}, Lcom/twitter/android/api/r;->a()J

    move-result-wide v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v7, "owner_id"

    invoke-virtual {v6}, Lcom/twitter/android/api/r;->h()Lcom/twitter/android/api/ad;

    move-result-object v6

    iget-wide v8, v6, Lcom/twitter/android/api/ad;->a:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;
    :try_end_865
    .catch Ljava/io/IOException; {:try_start_844 .. :try_end_865} :catch_868

    move-object v8, v3

    goto/16 :goto_61

    :catch_868
    move-exception v6

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_86c
    const-string v6, "owner_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    const-string v6, "email"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    const-string v6, "phone"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    const-string v6, "user_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v14

    const-string v6, "i_type"

    const/4 v7, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    const-string v6, "list_id"

    const-wide/16 v16, -0x1

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-virtual {v0, v6, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v16

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v7, p0

    move-object v9, v3

    invoke-direct/range {v7 .. v19}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;Lcom/twitter/android/provider/ae;J[Ljava/lang/String;[Ljava/lang/String;[JIJLjava/lang/String;Z)Landroid/util/Pair;

    move-result-object v6

    const-string v7, "count"

    iget-object v3, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/io/Serializable;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v3, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/twitter/android/network/c;

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_8c0
    const-string v3, "name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "screen_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "email"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "pass"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v8, "q_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v8, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v8, "lang"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v8, "name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "email"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "screen_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "pass"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "q_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const-string v8, "lang"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v8, v8, Lcom/twitter/android/network/p;->c:Ljava/lang/String;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string v15, "signup"

    aput-object v15, v10, v14

    invoke-static {v8, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz v3, :cond_94c

    new-instance v14, Lorg/apache/http/message/BasicNameValuePair;

    const-string v15, "fullname"

    invoke-direct {v14, v15, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_94c
    if-eqz v6, :cond_958

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "screen_name"

    invoke-direct {v3, v14, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_958
    if-eqz v7, :cond_964

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "email"

    invoke-direct {v3, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_964
    if-eqz v9, :cond_970

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "password"

    invoke-direct {v3, v6, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_970
    if-eqz v13, :cond_97c

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "lang"

    invoke-direct {v3, v6, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_97c
    if-eqz v11, :cond_994

    if-eqz v12, :cond_994

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "captcha_token"

    invoke-direct {v3, v6, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "captcha_solution"

    invoke-direct {v3, v6, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_994
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    new-instance v9, Lcom/twitter/android/network/a;

    const/4 v3, 0x0

    sget-object v12, Lcom/twitter/android/network/a;->g:Ljava/lang/String;

    sget-object v13, Lcom/twitter/android/network/a;->h:Ljava/lang/String;

    invoke-direct {v9, v3, v12, v13}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v6 .. v11}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v3

    if-eqz v3, :cond_9dd

    invoke-static {v11}, Lcom/twitter/android/service/TwitterService;->a(Ljava/io/ByteArrayOutputStream;)Lcom/twitter/android/network/k;

    move-result-object v3

    const-string v7, "auth"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    new-instance v7, Lcom/twitter/android/network/a;

    invoke-direct {v7, v3}, Lcom/twitter/android/network/a;-><init>(Lcom/twitter/android/network/k;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v7, v1}, Lcom/twitter/android/service/TwitterService;->e(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v3

    if-nez v3, :cond_9da

    const/16 v3, 0x190

    iput v3, v6, Lcom/twitter/android/network/c;->b:I

    :cond_9da
    move-object v8, v6

    goto/16 :goto_61

    :cond_9dd
    iget v3, v6, Lcom/twitter/android/network/c;->b:I

    const/16 v7, 0x193

    if-ne v3, v7, :cond_a9d

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/String;-><init>([B)V

    :try_start_9ec
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "fullname"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a19

    const-string v3, "fullname"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-lez v8, :cond_a19

    const-string v8, "name"

    sget-object v9, Lcom/twitter/android/util/a;->c:Lcom/twitter/android/util/a;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lcom/twitter/android/util/a;->a(Ljava/lang/String;)Lcom/twitter/android/util/e;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/android/util/e;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_a19
    const-string v3, "email"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a41

    const-string v3, "email"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-lez v8, :cond_a41

    const-string v8, "email"

    sget-object v9, Lcom/twitter/android/util/a;->c:Lcom/twitter/android/util/a;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lcom/twitter/android/util/a;->a(Ljava/lang/String;)Lcom/twitter/android/util/e;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/android/util/e;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_a41
    const-string v3, "screen_name"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a69

    const-string v3, "screen_name"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-lez v8, :cond_a69

    const-string v8, "screen_name"

    sget-object v9, Lcom/twitter/android/util/a;->c:Lcom/twitter/android/util/a;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v3}, Lcom/twitter/android/util/a;->a(Ljava/lang/String;)Lcom/twitter/android/util/e;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/android/util/e;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_a69
    const-string v3, "password"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a91

    const-string v3, "password"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-lez v7, :cond_a91

    const-string v7, "pass"

    sget-object v8, Lcom/twitter/android/util/a;->c:Lcom/twitter/android/util/a;

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v3}, Lcom/twitter/android/util/a;->a(Ljava/lang/String;)Lcom/twitter/android/util/e;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/android/util/e;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_a91
    .catch Lorg/json/JSONException; {:try_start_9ec .. :try_end_a91} :catch_a94

    :cond_a91
    move-object v8, v6

    goto/16 :goto_61

    :catch_a94
    move-exception v3

    const/4 v7, 0x0

    iput v7, v6, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v6, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v6

    goto/16 :goto_61

    :cond_a9d
    iget v3, v6, Lcom/twitter/android/network/c;->b:I

    const/16 v7, 0x19c

    if-ne v3, v7, :cond_1340

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/String;-><init>([B)V

    :try_start_aac
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v3, "captcha_token"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_ade

    const-string v3, "captcha_image_url"

    invoke-virtual {v7, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_ade

    const-string v3, "q_name"

    const-string v8, "captcha_token"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "q"

    const-string v8, "captcha_image_url"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v8, v6

    goto/16 :goto_61

    :cond_ade
    const/4 v3, 0x0

    iput v3, v6, Lcom/twitter/android/network/c;->b:I
    :try_end_ae1
    .catch Lorg/json/JSONException; {:try_start_aac .. :try_end_ae1} :catch_ae4

    move-object v8, v6

    goto/16 :goto_61

    :catch_ae4
    move-exception v3

    const/4 v7, 0x0

    iput v7, v6, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v6, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v6

    goto/16 :goto_61

    :sswitch_aed
    const-string v6, "user_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v11

    array-length v6, v11

    const/16 v7, 0x64

    invoke-static {v6, v7}, Lcom/twitter/android/network/p;->a(II)I

    move-result v12

    const/4 v9, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    move v10, v6

    move v6, v7

    move v7, v9

    move-object/from16 v9, v16

    :goto_b04
    if-ge v10, v12, :cond_b7a

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const-string v15, "friendships"

    aput-object v15, v13, v14

    const/4 v14, 0x1

    const-string v15, "create_all"

    aput-object v15, v13, v14

    invoke-virtual {v9, v13}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, ".json"

    invoke-virtual {v9, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v13, "user_id"

    mul-int/lit8 v14, v10, 0x64

    const/16 v15, 0x64

    invoke-static {v9, v13, v11, v14, v15}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-static {v14, v15, v9, v8, v13}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v9

    invoke-virtual {v9}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/android/network/c;->b()Z

    move-result v9

    if-eqz v9, :cond_b78

    :try_start_b45
    invoke-static {v13}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v9

    invoke-static {v9}, Lcom/twitter/android/api/s;->q(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v9

    const/4 v13, 0x1

    invoke-virtual {v3, v9, v13}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;I)V

    const/4 v6, 0x1

    new-instance v9, Landroid/content/Intent;

    const-class v13, Lcom/twitter/android/service/TwitterService;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v13, "ADD_USER_SUGGESTION"

    invoke-virtual {v9, v13}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v13, "soid"

    invoke-virtual {v9, v13, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v9

    const-string v13, "user_id"

    invoke-virtual {v9, v13, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    move-result-object v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/twitter/android/service/TwitterService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_b72
    .catch Ljava/io/IOException; {:try_start_b45 .. :try_end_b72} :catch_132e

    :goto_b72
    add-int/lit8 v9, v10, 0x1

    move v10, v9

    move-object/from16 v9, v16

    goto :goto_b04

    :cond_b78
    const/4 v7, 0x1

    goto :goto_b72

    :cond_b7a
    if-eqz v7, :cond_134a

    if-eqz v6, :cond_134a

    const/16 v3, 0x1a2

    iput v3, v9, Lcom/twitter/android/network/c;->b:I

    move-object v8, v9

    goto/16 :goto_61

    :sswitch_b85
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v8, v1}, Lcom/twitter/android/service/TwitterService;->b(Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_b90
    const/16 v3, 0x3a

    move/from16 v0, v25

    if-ne v0, v3, :cond_c45

    const/4 v3, 0x1

    :goto_b97
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "account"

    aput-object v10, v7, v9

    const/4 v9, 0x1

    const-string v10, "settings"

    aput-object v10, v7, v9

    invoke-virtual {v6, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x200

    invoke-direct {v14, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    if-eqz v3, :cond_c48

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "settings"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/api/ah;

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "geo_enabled"

    iget-boolean v9, v3, Lcom/twitter/android/api/ah;->b:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "sleep_time_enabled"

    iget-boolean v9, v3, Lcom/twitter/android/api/ah;->c:Z

    invoke-static {v9}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v6, v3, Lcom/twitter/android/api/ah;->c:Z

    if-eqz v6, :cond_c1c

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "start_sleep_time"

    invoke-virtual {v3}, Lcom/twitter/android/api/ah;->a()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v6, v7, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string v7, "end_sleep_time"

    invoke-virtual {v3}, Lcom/twitter/android/api/ah;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v7, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "time_zone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c1c
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    :goto_c2d
    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_1343

    :try_start_c33
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    const-string v7, "settings"

    invoke-static {v6}, Lcom/twitter/android/api/ah;->a(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ah;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_c42
    .catch Ljava/io/IOException; {:try_start_c33 .. :try_end_c42} :catch_c5b

    move-object v8, v3

    goto/16 :goto_61

    :cond_c45
    const/4 v3, 0x0

    goto/16 :goto_b97

    :cond_c48
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x1

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    goto :goto_c2d

    :catch_c5b
    move-exception v6

    const/4 v7, 0x0

    iput v7, v3, Lcom/twitter/android/network/c;->b:I

    iput-object v6, v3, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_c64
    const-string v6, "message_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v13

    if-eqz v13, :cond_5f

    array-length v14, v13

    const/4 v6, 0x0

    move v12, v6

    :goto_c71
    if-ge v12, v14, :cond_c86

    aget-wide v9, v13, v12

    const/4 v11, 0x0

    move-object/from16 v6, p0

    move-object v7, v3

    invoke-direct/range {v6 .. v11}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JZ)Lcom/twitter/android/network/c;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5, v6}, Lcom/twitter/android/service/ScribeService;->a(Landroid/content/Context;JLcom/twitter/android/network/c;)V

    add-int/lit8 v6, v12, 0x1

    move v12, v6

    goto :goto_c71

    :cond_c86
    invoke-virtual {v3, v13}, Lcom/twitter/android/provider/ae;->c([J)I

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_c8d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v3, v3, Lcom/twitter/android/network/p;->c:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "decider"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "android"

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".json"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v9, 0x1

    invoke-static {v6, v8, v3, v9, v7}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v3

    if-eqz v3, :cond_1340

    :try_start_cc6
    invoke-static {v7}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v3

    const-string v7, "settings"

    invoke-static {v3}, Lcom/twitter/android/api/s;->r(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/e;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_cd5
    .catch Ljava/io/IOException; {:try_start_cc6 .. :try_end_cd5} :catch_cd8

    move-object v8, v6

    goto/16 :goto_61

    :catch_cd8
    move-exception v3

    const/4 v7, 0x0

    iput v7, v6, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v6, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v6

    goto/16 :goto_61

    :sswitch_ce1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "saved_searches"

    aput-object v10, v7, v9

    const/4 v9, 0x1

    const-string v10, "create"

    aput-object v10, v7, v9

    invoke-virtual {v6, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v14}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    const/4 v9, 0x1

    invoke-direct {v13, v9}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "query"

    invoke-direct {v9, v10, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_1340

    :try_start_d34
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v7

    invoke-static {v7}, Lcom/twitter/android/api/s;->n(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/x;

    move-result-object v7

    const/4 v8, 0x6

    invoke-virtual {v3, v7, v8}, Lcom/twitter/android/provider/ae;->a(Lcom/twitter/android/api/x;I)J
    :try_end_d40
    .catch Ljava/io/IOException; {:try_start_d34 .. :try_end_d40} :catch_d43

    move-object v8, v6

    goto/16 :goto_61

    :catch_d43
    move-exception v3

    const/4 v7, 0x0

    iput v7, v6, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v6, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v6

    goto/16 :goto_61

    :sswitch_d4c
    const-string v6, "q"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    invoke-virtual {v3, v6, v7}, Lcom/twitter/android/provider/ae;->a(Ljava/lang/String;I)J

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "saved_searches"

    aput-object v12, v7, v11

    const/4 v11, 0x1

    const-string v12, "destroy"

    aput-object v12, v7, v11

    const/4 v11, 0x2

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v7, v11

    invoke-virtual {v6, v7}, Lcom/twitter/android/network/p;->a([Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v12, 0x0

    invoke-static {v7, v11, v6, v8, v12}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v6

    iget v7, v6, Lcom/twitter/android/network/c;->b:I

    const/16 v8, 0xc8

    if-eq v7, v8, :cond_d98

    iget v7, v6, Lcom/twitter/android/network/c;->b:I

    const/16 v8, 0x194

    if-ne v7, v8, :cond_1340

    :cond_d98
    invoke-virtual {v3, v9, v10}, Lcom/twitter/android/provider/ae;->b(J)I

    move-object v8, v6

    goto/16 :goto_61

    :sswitch_d9e
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->v(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_da9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->w(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_db4
    const-string v6, "status_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v26

    const-string v6, "user_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v9

    const-string v6, "q"

    const-wide/16 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v14

    const/16 v21, 0x0

    const-wide/16 v6, 0x0

    cmp-long v6, v14, v6

    if-lez v6, :cond_1346

    const/16 v11, 0xa

    const-wide/16 v12, 0x0

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    move-object/from16 v6, p0

    move-object v7, v3

    invoke-direct/range {v6 .. v20}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;JIJJJIZLandroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_1346

    const/4 v6, 0x1

    :goto_df7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v7, v7, Lcom/twitter/android/network/p;->b:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "related_results"

    aput-object v12, v8, v11

    const/4 v11, 0x1

    const-string v12, "show"

    aput-object v12, v8, v11

    const/4 v11, 0x2

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v8, v11

    invoke-static {v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "include_entities"

    const/4 v11, 0x1

    invoke-static {v7, v8, v11}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v8, "include_cards"

    const/4 v11, 0x1

    invoke-static {v7, v8, v11}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    const/16 v11, 0x400

    invoke-direct {v8, v11}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    invoke-static {v11, v12, v7, v13, v8}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_e5f

    :try_start_e45
    invoke-static {v8}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v7

    invoke-static {v7}, Lcom/twitter/android/api/s;->v(Lorg/codehaus/jackson/JsonParser;)Ljava/util/ArrayList;

    move-result-object v8

    const/16 v11, 0xa

    const-wide/16 v12, -0x1

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object v7, v3

    invoke-virtual/range {v7 .. v18}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJZZLjava/lang/String;ZZ)I
    :try_end_e5d
    .catch Ljava/io/IOException; {:try_start_e45 .. :try_end_e5d} :catch_e6a

    move-result v3

    add-int/2addr v6, v3

    :cond_e5f
    :goto_e5f
    const-string v3, "new_tweet"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v8, v19

    goto/16 :goto_61

    :catch_e6a
    move-exception v3

    const/4 v7, 0x0

    move-object/from16 v0, v19

    iput v7, v0, Lcom/twitter/android/network/c;->b:I

    move-object/from16 v0, v19

    iput-object v3, v0, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    goto :goto_e5f

    :sswitch_e75
    const-string v3, "email"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "lang"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v7, v7, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "i"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "users"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string v10, "email_available"

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "email"

    invoke-static {v7, v8, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_eb7

    const-string v3, "lang"

    invoke-static {v7, v3, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_eb7
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v6, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v9, 0x0

    invoke-static {v3, v8, v7, v9, v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_1343

    new-instance v7, Ljava/lang/String;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    invoke-direct {v7, v6}, Ljava/lang/String;-><init>([B)V

    :try_start_ede
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v7, "valid"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_ef3

    const-string v7, "valid"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_f06

    :cond_ef3
    const/4 v7, 0x0

    iput v7, v3, Lcom/twitter/android/network/c;->b:I

    const-string v7, "msg"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f06

    const-string v7, "msg"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/twitter/android/network/c;->c:Ljava/lang/String;
    :try_end_f06
    .catch Lorg/json/JSONException; {:try_start_ede .. :try_end_f06} :catch_f09

    :cond_f06
    move-object v8, v3

    goto/16 :goto_61

    :catch_f09
    move-exception v6

    const/4 v6, 0x0

    iput v6, v3, Lcom/twitter/android/network/c;->b:I

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_f10
    const-string v3, "screen_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "lang"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v7, v7, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "i"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string v10, "users"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string v10, "username_available"

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "custom"

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string v8, "context"

    const-string v9, "signup"

    invoke-static {v7, v8, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "username"

    invoke-static {v7, v8, v3}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_f5f

    const-string v3, "lang"

    invoke-static {v7, v3, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_f5f
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v6, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v9, 0x0

    invoke-static {v3, v8, v7, v9, v6}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_1343

    :try_start_f7d
    invoke-static {v6}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/android/api/s;->x(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ak;

    move-result-object v6

    iget-boolean v7, v6, Lcom/twitter/android/api/ak;->a:Z

    if-nez v7, :cond_f91

    const/16 v7, 0x190

    iput v7, v3, Lcom/twitter/android/network/c;->b:I

    iget-object v6, v6, Lcom/twitter/android/api/ak;->b:Ljava/lang/String;

    iput-object v6, v3, Lcom/twitter/android/network/c;->c:Ljava/lang/String;
    :try_end_f91
    .catch Ljava/io/IOException; {:try_start_f7d .. :try_end_f91} :catch_f94

    :cond_f91
    move-object v8, v3

    goto/16 :goto_61

    :catch_f94
    move-exception v6

    const/4 v6, 0x0

    iput v6, v3, Lcom/twitter/android/network/c;->b:I

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_f9b
    invoke-direct/range {p0 .. p1}, Lcom/twitter/android/service/TwitterService;->a(Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_fa2
    const-string v3, "status_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v3, v3, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "i"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "statuses"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v10

    const/4 v6, 0x3

    const-string v7, "activity"

    aput-object v7, v9, v6

    const/4 v6, 0x4

    const-string v7, "summary"

    aput-object v7, v9, v6

    invoke-static {v3, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ".json"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v3, "include_user_entities"

    const/4 v6, 0x1

    invoke-static {v11, v3, v6}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x400

    invoke-direct {v14, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_1343

    :try_start_fff
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    const-string v7, "q"

    invoke-static {v6}, Lcom/twitter/android/api/s;->z(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/a;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;
    :try_end_100e
    .catch Ljava/io/IOException; {:try_start_fff .. :try_end_100e} :catch_1011

    move-object v8, v3

    goto/16 :goto_61

    :catch_1011
    move-exception v6

    const/4 v6, 0x0

    iput v6, v3, Lcom/twitter/android/network/c;->b:I

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1018
    invoke-virtual {v3}, Lcom/twitter/android/provider/ae;->a()I

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/provider/a;->a(Landroid/content/Context;)Lcom/twitter/android/provider/a;

    move-result-object v3

    const-string v6, "discover"

    const/4 v7, 0x0

    invoke-virtual {v3, v10, v6, v7}, Lcom/twitter/android/provider/a;->b(Ljava/lang/String;Ljava/lang/String;I)I

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_1029
    const-string v6, "status_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    const-string v9, "impression_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v9, "earned"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    const/4 v9, 0x1

    new-array v9, v9, [J

    const/4 v11, 0x0

    aput-wide v6, v9, v11

    invoke-virtual {v3, v9}, Lcom/twitter/android/provider/ae;->d([J)I

    move-result v3

    if-lez v3, :cond_105a

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/service/TwitterService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v6, Lcom/twitter/android/provider/z;->a:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_105a
    if-eqz v10, :cond_5f

    const/16 v9, 0x9

    const-wide/16 v11, 0x0

    move-object/from16 v7, p0

    invoke-direct/range {v7 .. v13}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;ILjava/lang/String;JZ)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_1069
    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/network/o;->b()V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_1074
    const-string v6, "user"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/twitter/android/api/ad;

    if-eqz v6, :cond_1087

    invoke-virtual {v3, v4, v5, v6}, Lcom/twitter/android/provider/ae;->a(JLcom/twitter/android/api/ad;)V

    move-object/from16 v8, v16

    goto/16 :goto_61

    :cond_1087
    const-string v6, "user_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v6

    if-eqz v6, :cond_1094

    invoke-virtual {v3, v4, v5, v6}, Lcom/twitter/android/provider/ae;->a(J[J)V

    :cond_1094
    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_1098
    const-string v6, "i_type"

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const-string v7, "count"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v7, v7, Lcom/twitter/android/network/p;->a:Ljava/lang/String;

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "i"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "search"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string v12, "typeahead"

    aput-object v12, v10, v11

    invoke-static {v7, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, ".json"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v7, "prefetch"

    const/4 v10, 0x1

    invoke-static {v11, v7, v10}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string v7, "cache_age"

    const-wide/16 v12, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v12, v13}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    packed-switch v6, :pswitch_data_14ac

    :pswitch_10df
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_10f4
    const-string v6, "result_type"

    const-string v7, "users"

    invoke-static {v11, v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "users_cache_age"

    invoke-static {v11, v6, v12, v13}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const/4 v6, 0x1

    move v7, v6

    :goto_1102
    if-lez v9, :cond_1109

    const-string v6, "count"

    invoke-static {v11, v6, v9}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_1109
    new-instance v14, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x400

    invoke-direct {v14, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v8

    if-eqz v8, :cond_1340

    :try_start_1128
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v8, v9, v10}, Lcom/twitter/android/api/s;->a(Lorg/codehaus/jackson/JsonParser;J)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v3, v8, v4, v5, v7}, Lcom/twitter/android/provider/ae;->a(Ljava/util/ArrayList;JI)I
    :try_end_1137
    .catch Ljava/io/IOException; {:try_start_1128 .. :try_end_1137} :catch_1149

    move-object v8, v6

    goto/16 :goto_61

    :pswitch_113a
    const-string v6, "result_type"

    const-string v7, "topics"

    invoke-static {v11, v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "topics_cache_age"

    invoke-static {v11, v6, v12, v13}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const/4 v6, 0x3

    move v7, v6

    goto :goto_1102

    :catch_1149
    move-exception v3

    const/4 v7, 0x0

    iput v7, v6, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v6, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v6

    goto/16 :goto_61

    :sswitch_1152
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v6, v6, Lcom/twitter/android/network/p;->b:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "friendships"

    aput-object v10, v7, v9

    const/4 v9, 0x1

    const-string v10, "incoming"

    aput-object v10, v7, v9

    invoke-static {v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "page"

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    const-string v7, "owner_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    const/4 v10, 0x1

    const/16 v11, 0x12

    move-object v9, v3

    invoke-virtual/range {v9 .. v14}, Lcom/twitter/android/provider/ae;->a(IIJI)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1191

    const-string v9, "cursor"

    invoke-static {v6, v9, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1191
    new-instance v14, Ljava/io/ByteArrayOutputStream;

    const/16 v7, 0x400

    invoke-direct {v14, v7}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    const/4 v13, 0x0

    move-object v11, v6

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;ZLjava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->b()Z

    move-result v7

    if-eqz v7, :cond_1340

    :try_start_11b1
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v7

    invoke-static {v7}, Lcom/twitter/android/api/s;->B(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/o;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/android/api/o;->b()Ljava/util/ArrayList;

    move-result-object v9

    if-eqz v9, :cond_11e8

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_11e8

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v9}, Lcom/twitter/android/util/z;->b(Ljava/util/Collection;)[J

    move-result-object v14

    const/16 v15, 0x12

    const-wide/16 v16, -0x1

    invoke-virtual {v7}, Lcom/twitter/android/api/o;->a()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move-object/from16 v7, p0

    move-object v9, v3

    move-wide v10, v4

    invoke-direct/range {v7 .. v19}, Lcom/twitter/android/service/TwitterService;->a(Lcom/twitter/android/network/a;Lcom/twitter/android/provider/ae;J[Ljava/lang/String;[Ljava/lang/String;[JIJLjava/lang/String;Z)Landroid/util/Pair;

    move-result-object v3

    const-string v7, "count"

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/io/Serializable;

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;
    :try_end_11e8
    .catch Ljava/io/IOException; {:try_start_11b1 .. :try_end_11e8} :catch_11eb

    :cond_11e8
    move-object v8, v6

    goto/16 :goto_61

    :catch_11eb
    move-exception v3

    const/4 v7, 0x0

    iput v7, v6, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v6, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v6

    goto/16 :goto_61

    :sswitch_11f4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v6, v6, Lcom/twitter/android/network/p;->b:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "friendships"

    aput-object v10, v7, v9

    const/4 v9, 0x1

    const-string v10, "accept"

    aput-object v10, v7, v9

    invoke-static {v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v6, "soid"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v17

    const-string v6, "user_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "user_id"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, v10, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x400

    invoke-direct {v14, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_133c

    :try_start_1256
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v6

    new-instance v19, Ljava/util/ArrayList;

    const/4 v7, 0x1

    move-object/from16 v0, v19

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v7, 0x12

    iget-wide v10, v6, Lcom/twitter/android/api/ad;->a:J

    move-object v6, v3

    move-wide/from16 v8, v17

    invoke-virtual/range {v6 .. v11}, Lcom/twitter/android/provider/ae;->a(IJJ)V

    const/4 v10, 0x1

    const-wide/16 v11, -0x1

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x1

    move-object v6, v3

    move-object/from16 v7, v19

    move-wide/from16 v8, v17

    invoke-virtual/range {v6 .. v15}, Lcom/twitter/android/provider/ae;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I
    :try_end_1283
    .catch Ljava/io/IOException; {:try_start_1256 .. :try_end_1283} :catch_1287

    move-object/from16 v8, v16

    goto/16 :goto_61

    :catch_1287
    move-exception v3

    const/4 v6, 0x0

    move-object/from16 v0, v16

    iput v6, v0, Lcom/twitter/android/network/c;->b:I

    move-object/from16 v0, v16

    iput-object v3, v0, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_1295
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    iget-object v6, v6, Lcom/twitter/android/network/p;->b:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "friendships"

    aput-object v10, v7, v9

    const/4 v9, 0x1

    const-string v10, "deny"

    aput-object v10, v7, v9

    invoke-static {v6, v7}, Lcom/twitter/android/network/p;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v6, "soid"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v15

    const-string v6, "user_id"

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "user_id"

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, v10, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v14, Ljava/io/ByteArrayOutputStream;

    const/16 v6, 0x400

    invoke-direct {v14, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    invoke-static/range {p0 .. p0}, Lcom/twitter/android/network/o;->b(Landroid/content/Context;)Lcom/twitter/android/network/o;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    move-object v12, v8

    invoke-static/range {v9 .. v14}, Lcom/twitter/android/network/c;->a(Lcom/twitter/android/network/d;Lcom/twitter/android/network/p;Ljava/lang/CharSequence;Lcom/twitter/android/network/a;Ljava/util/ArrayList;Ljava/io/OutputStream;)Lcom/twitter/android/network/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/network/c;->a()Lcom/twitter/android/network/c;

    move-result-object v12

    invoke-virtual {v12}, Lcom/twitter/android/network/c;->b()Z

    move-result v6

    if-eqz v6, :cond_1339

    :try_start_12f7
    invoke-static {v14}, Lcom/twitter/android/service/TwitterService;->b(Ljava/io/ByteArrayOutputStream;)Lorg/codehaus/jackson/JsonParser;

    move-result-object v6

    invoke-static {v6}, Lcom/twitter/android/api/s;->c(Lorg/codehaus/jackson/JsonParser;)Lcom/twitter/android/api/ad;

    move-result-object v6

    const/16 v7, 0x12

    iget-wide v10, v6, Lcom/twitter/android/api/ad;->a:J

    move-object v6, v3

    move-wide v8, v15

    invoke-virtual/range {v6 .. v11}, Lcom/twitter/android/provider/ae;->a(IJJ)V
    :try_end_1308
    .catch Ljava/io/IOException; {:try_start_12f7 .. :try_end_1308} :catch_130b

    move-object v8, v12

    goto/16 :goto_61

    :catch_130b
    move-exception v3

    const/4 v6, 0x0

    iput v6, v12, Lcom/twitter/android/network/c;->b:I

    iput-object v3, v12, Lcom/twitter/android/network/c;->d:Ljava/lang/Exception;

    move-object v8, v12

    goto/16 :goto_61

    :sswitch_1314
    move-object/from16 v8, v16

    goto/16 :goto_61

    :sswitch_1318
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->s(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :sswitch_1323
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v8, v1}, Lcom/twitter/android/service/TwitterService;->r(Lcom/twitter/android/provider/ae;Lcom/twitter/android/network/a;Landroid/content/Intent;)Lcom/twitter/android/network/c;

    move-result-object v3

    move-object v8, v3

    goto/16 :goto_61

    :catch_132e
    move-exception v9

    goto/16 :goto_b72

    :cond_1331
    move-object/from16 v3, v22

    move-object/from16 v4, v23

    move/from16 v5, v24

    goto/16 :goto_70

    :cond_1339
    move-object v8, v12

    goto/16 :goto_61

    :cond_133c
    move-object/from16 v8, v16

    goto/16 :goto_61

    :cond_1340
    move-object v8, v6

    goto/16 :goto_61

    :cond_1343
    move-object v8, v3

    goto/16 :goto_61

    :cond_1346
    move/from16 v6, v21

    goto/16 :goto_df7

    :cond_134a
    move-object v8, v9

    goto/16 :goto_61

    :cond_134d
    move-object v8, v13

    goto/16 :goto_61

    :sswitch_data_1350
    .sparse-switch
        0x1 -> :sswitch_d7
        0x2 -> :sswitch_100
        0x3 -> :sswitch_13c
        0x4 -> :sswitch_14d
        0x5 -> :sswitch_15e
        0x6 -> :sswitch_17d
        0x7 -> :sswitch_1c5
        0x8 -> :sswitch_1d3
        0x9 -> :sswitch_1e1
        0xa -> :sswitch_1ec
        0xb -> :sswitch_1f7
        0xc -> :sswitch_202
        0xd -> :sswitch_210
        0xe -> :sswitch_21b
        0xf -> :sswitch_226
        0x10 -> :sswitch_23c
        0x11 -> :sswitch_23c
        0x12 -> :sswitch_285
        0x13 -> :sswitch_2b6
        0x14 -> :sswitch_2c1
        0x15 -> :sswitch_2d6
        0x16 -> :sswitch_2e1
        0x17 -> :sswitch_2ff
        0x18 -> :sswitch_30a
        0x19 -> :sswitch_333
        0x1a -> :sswitch_341
        0x1b -> :sswitch_34f
        0x1c -> :sswitch_34f
        0x1d -> :sswitch_363
        0x1e -> :sswitch_36e
        0x1f -> :sswitch_36e
        0x20 -> :sswitch_379
        0x21 -> :sswitch_384
        0x22 -> :sswitch_38f
        0x23 -> :sswitch_39a
        0x24 -> :sswitch_3a5
        0x25 -> :sswitch_3b0
        0x26 -> :sswitch_3bb
        0x27 -> :sswitch_3ce
        0x28 -> :sswitch_3d9
        0x29 -> :sswitch_3e4
        0x2a -> :sswitch_3ef
        0x2b -> :sswitch_3fa
        0x2c -> :sswitch_405
        0x2d -> :sswitch_420
        0x2e -> :sswitch_450
        0x2f -> :sswitch_485
        0x30 -> :sswitch_4b4
        0x31 -> :sswitch_4cb
        0x32 -> :sswitch_552
        0x33 -> :sswitch_5a3
        0x34 -> :sswitch_693
        0x35 -> :sswitch_7f3
        0x36 -> :sswitch_86c
        0x37 -> :sswitch_8c0
        0x38 -> :sswitch_aed
        0x39 -> :sswitch_b85
        0x3a -> :sswitch_b90
        0x3b -> :sswitch_b90
        0x3c -> :sswitch_c64
        0x3d -> :sswitch_c8d
        0x3e -> :sswitch_ce1
        0x3f -> :sswitch_d4c
        0x40 -> :sswitch_d9e
        0x41 -> :sswitch_da9
        0x42 -> :sswitch_db4
        0x43 -> :sswitch_729
        0x44 -> :sswitch_e75
        0x45 -> :sswitch_f10
        0x46 -> :sswitch_fa2
        0x47 -> :sswitch_1018
        0x48 -> :sswitch_1029
        0x49 -> :sswitch_1069
        0x4a -> :sswitch_1074
        0x4b -> :sswitch_1098
        0x4c -> :sswitch_231
        0x4d -> :sswitch_174
        0x4e -> :sswitch_1318
        0x4f -> :sswitch_1152
        0x50 -> :sswitch_11f4
        0x51 -> :sswitch_1295
        0x52 -> :sswitch_1323
        0x53 -> :sswitch_f9b
        0x3e7 -> :sswitch_1314
    .end sparse-switch

    :sswitch_data_14a2
    .sparse-switch
        0x0 -> :sswitch_11f
        0x5 -> :sswitch_12d
    .end sparse-switch

    :pswitch_data_14ac
    .packed-switch 0x1
        :pswitch_10f4
        :pswitch_10df
        :pswitch_113a
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .registers 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/service/TwitterService;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x32

    const/high16 v2, 0x4248

    invoke-direct {v0, v1, v2}, Ljava/util/LinkedHashMap;-><init>(IF)V

    iput-object v0, p0, Lcom/twitter/android/service/TwitterService;->f:Ljava/util/LinkedHashMap;

    invoke-static {p0}, Lcom/twitter/android/network/p;->a(Landroid/content/Context;)Lcom/twitter/android/network/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/service/TwitterService;->g:Lcom/twitter/android/network/p;

    return-void
.end method

.method public onDestroy()V
    .registers 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->e:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 7

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/twitter/android/service/j;

    invoke-direct {v1, p0, p1, p3}, Lcom/twitter/android/service/j;-><init>(Lcom/twitter/android/service/TwitterService;Landroid/content/Intent;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/twitter/android/service/TwitterService;->f:Ljava/util/LinkedHashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x2

    return v0
.end method
