.class public final Lcom/twitter/android/util/j;
.super Ljava/lang/Object;


# instance fields
.field private transient a:[Lcom/twitter/android/util/k;

.field private transient b:I

.field private c:I

.field private d:F


# direct methods
.method public constructor <init>()V
    .registers 3

    const/16 v0, 0x14

    const/high16 v1, 0x3f40

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/util/j;-><init>(IF)V

    return-void
.end method

.method private constructor <init>(IF)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f40

    iput v0, p0, Lcom/twitter/android/util/j;->d:F

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/twitter/android/util/k;

    iput-object v0, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    const/16 v0, 0xf

    iput v0, p0, Lcom/twitter/android/util/j;->c:I

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 12

    const v8, 0x7fffffff

    iget-object v1, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    and-int v0, p1, v8

    array-length v2, v1

    rem-int/2addr v0, v2

    aget-object v2, v1, v0

    :goto_b
    if-eqz v2, :cond_19

    iget v3, v2, Lcom/twitter/android/util/k;->a:I

    if-ne v3, p1, :cond_16

    iget-object v0, v2, Lcom/twitter/android/util/k;->c:Ljava/lang/Object;

    iput-object p2, v2, Lcom/twitter/android/util/k;->c:Ljava/lang/Object;

    :goto_15
    return-object v0

    :cond_16
    iget-object v2, v2, Lcom/twitter/android/util/k;->d:Lcom/twitter/android/util/k;

    goto :goto_b

    :cond_19
    iget v2, p0, Lcom/twitter/android/util/j;->b:I

    iget v3, p0, Lcom/twitter/android/util/j;->c:I

    if-lt v2, v3, :cond_51

    iget-object v0, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    array-length v0, v0

    iget-object v3, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v4, v1, 0x1

    new-array v5, v4, [Lcom/twitter/android/util/k;

    int-to-float v1, v4

    iget v2, p0, Lcom/twitter/android/util/j;->d:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/twitter/android/util/j;->c:I

    iput-object v5, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    :goto_33
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_4b

    aget-object v0, v3, v1

    :goto_39
    if-eqz v0, :cond_49

    iget-object v2, v0, Lcom/twitter/android/util/k;->d:Lcom/twitter/android/util/k;

    iget v6, v0, Lcom/twitter/android/util/k;->a:I

    and-int/2addr v6, v8

    rem-int/2addr v6, v4

    aget-object v7, v5, v6

    iput-object v7, v0, Lcom/twitter/android/util/k;->d:Lcom/twitter/android/util/k;

    aput-object v0, v5, v6

    move-object v0, v2

    goto :goto_39

    :cond_49
    move v0, v1

    goto :goto_33

    :cond_4b
    iget-object v1, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    and-int v0, p1, v8

    array-length v2, v1

    rem-int/2addr v0, v2

    :cond_51
    new-instance v2, Lcom/twitter/android/util/k;

    aget-object v3, v1, v0

    invoke-direct {v2, p1, p1, p2, v3}, Lcom/twitter/android/util/k;-><init>(IILjava/lang/Object;Lcom/twitter/android/util/k;)V

    aput-object v2, v1, v0

    iget v0, p0, Lcom/twitter/android/util/j;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/util/j;->b:I

    const/4 v0, 0x0

    goto :goto_15
.end method

.method public final declared-synchronized a()V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/twitter/android/util/j;->a:[Lcom/twitter/android/util/k;

    array-length v0, v1

    :goto_4
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_f

    const/4 v2, 0x0

    aput-object v2, v1, v0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_c

    goto :goto_4

    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_f
    const/4 v0, 0x0

    :try_start_10
    iput v0, p0, Lcom/twitter/android/util/j;->b:I
    :try_end_12
    .catchall {:try_start_10 .. :try_end_12} :catchall_c

    monitor-exit p0

    return-void
.end method
