.class public final Lcom/twitter/android/util/f;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v0}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    iput p2, p0, Lcom/twitter/android/util/f;->b:I

    iput p3, p0, Lcom/twitter/android/util/f;->c:I

    if-lez p2, :cond_11

    if-lez p3, :cond_11

    const/4 v0, 0x1

    :goto_e
    iput-boolean v0, p0, Lcom/twitter/android/util/f;->d:Z

    return-void

    :cond_11
    const/4 v0, 0x0

    goto :goto_e
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_5

    :cond_4
    :goto_4
    return v0

    :cond_5
    if-eqz p1, :cond_11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_13

    :cond_11
    move v0, v1

    goto :goto_4

    :cond_13
    check-cast p1, Lcom/twitter/android/util/f;

    iget v2, p0, Lcom/twitter/android/util/f;->c:I

    iget v3, p1, Lcom/twitter/android/util/f;->c:I

    if-eq v2, v3, :cond_1d

    move v0, v1

    goto :goto_4

    :cond_1d
    iget v2, p0, Lcom/twitter/android/util/f;->b:I

    iget v3, p1, Lcom/twitter/android/util/f;->b:I

    if-eq v2, v3, :cond_25

    move v0, v1

    goto :goto_4

    :cond_25
    iget-object v2, p0, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_32

    iget-object v0, p0, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4

    :cond_32
    iget-object v2, p1, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_4
.end method

.method public final hashCode()I
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/android/util/f;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/android/util/f;->c:I

    add-int/2addr v0, v1

    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_a
.end method
