.class public final Lcom/twitter/android/util/o;
.super Lcom/twitter/android/util/s;


# instance fields
.field private final h:Lcom/twitter/android/util/p;

.field private final i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/util/p;I)V
    .registers 6

    const/16 v0, 0x19

    const/4 v1, 0x5

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/util/s;-><init>(Landroid/content/Context;II)V

    iput-object p2, p0, Lcom/twitter/android/util/o;->h:Lcom/twitter/android/util/p;

    iput p3, p0, Lcom/twitter/android/util/o;->i:I

    return-void
.end method


# virtual methods
.method public final a(JLcom/twitter/android/util/f;)Landroid/graphics/Bitmap;
    .registers 10

    iget-object v4, p3, Lcom/twitter/android/util/f;->a:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-super/range {v0 .. v5}, Lcom/twitter/android/util/s;->a(JLjava/lang/Object;Ljava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/android/util/r;
    .registers 6

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;[B)Lcom/twitter/android/util/r;
    .registers 7

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/String;[B)Lcom/twitter/android/util/r;
    .registers 6

    check-cast p1, Lcom/twitter/android/util/f;

    iget-boolean v0, p1, Lcom/twitter/android/util/f;->d:Z

    if-eqz v0, :cond_16

    iget v0, p1, Lcom/twitter/android/util/f;->b:I

    iget v1, p1, Lcom/twitter/android/util/f;->c:I

    :goto_a
    invoke-static {p3, v0, v1}, Lcom/twitter/android/util/g;->b([BII)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1b

    new-instance v0, Lcom/twitter/android/util/r;

    invoke-direct {v0, p2, v1}, Lcom/twitter/android/util/r;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :goto_15
    return-object v0

    :cond_16
    iget v0, p0, Lcom/twitter/android/util/o;->i:I

    iget v1, p0, Lcom/twitter/android/util/o;->i:I

    goto :goto_a

    :cond_1b
    const/4 v0, 0x0

    goto :goto_15
.end method

.method protected final a(Ljava/util/HashMap;)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/util/o;->h:Lcom/twitter/android/util/p;

    invoke-interface {v0, p0, p1}, Lcom/twitter/android/util/p;->a(Lcom/twitter/android/util/s;Ljava/util/HashMap;)V

    return-void
.end method
