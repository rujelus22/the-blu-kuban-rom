.class public final Lcom/twitter/android/ao;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Landroid/view/View;

.field private c:Lcom/twitter/android/api/ad;

.field private final d:Lcom/twitter/android/client/b;

.field private e:Lcom/twitter/android/util/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/b;)V
    .registers 3

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/ao;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/ao;->d:Lcom/twitter/android/client/b;

    return-void
.end method


# virtual methods
.method public final a(Lcom/twitter/android/api/ad;)V
    .registers 6

    iput-object p1, p0, Lcom/twitter/android/ao;->c:Lcom/twitter/android/api/ad;

    new-instance v0, Lcom/twitter/android/util/f;

    iget-object v1, p0, Lcom/twitter/android/ao;->c:Lcom/twitter/android/api/ad;

    iget-object v1, v1, Lcom/twitter/android/api/ad;->c:Ljava/lang/String;

    const-string v2, "_normal."

    const-string v3, "."

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/util/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/ao;->e:Lcom/twitter/android/util/f;

    invoke-virtual {p0}, Lcom/twitter/android/ao;->notifyDataSetChanged()V

    return-void
.end method

.method public final a(Z)V
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_b
    return-void
.end method

.method public final getCount()I
    .registers 2

    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/ao;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03001e

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    const v1, 0x7f070067

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/ao;->c:Lcom/twitter/android/api/ad;

    if-eqz v1, :cond_50

    iget-object v1, p0, Lcom/twitter/android/ao;->c:Lcom/twitter/android/api/ad;

    iget-object v1, v1, Lcom/twitter/android/api/ad;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/ao;->e:Lcom/twitter/android/util/f;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    const v1, 0x7f070066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/android/ao;->d:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/ao;->e:Lcom/twitter/android/util/f;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/util/f;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_53

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_46
    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_4b
    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_50
    iget-object v0, p0, Lcom/twitter/android/ao;->b:Landroid/view/View;

    return-object v0

    :cond_53
    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_46
.end method
