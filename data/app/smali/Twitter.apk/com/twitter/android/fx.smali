.class final Lcom/twitter/android/fx;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Landroid/widget/ListView;

.field final synthetic d:I

.field final synthetic e:Lcom/twitter/android/TweetListFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetListFragment;IILandroid/widget/ListView;I)V
    .registers 6

    iput-object p1, p0, Lcom/twitter/android/fx;->e:Lcom/twitter/android/TweetListFragment;

    iput p2, p0, Lcom/twitter/android/fx;->a:I

    iput p3, p0, Lcom/twitter/android/fx;->b:I

    iput-object p4, p0, Lcom/twitter/android/fx;->c:Landroid/widget/ListView;

    iput p5, p0, Lcom/twitter/android/fx;->d:I

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 12

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/fx;->e:Lcom/twitter/android/TweetListFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetListFragment;->a(Lcom/twitter/android/TweetListFragment;)Lcom/twitter/android/gd;

    move-result-object v1

    if-eqz v1, :cond_d

    if-eqz p1, :cond_d

    if-nez p2, :cond_e

    :cond_d
    :goto_d
    return v0

    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    sub-int v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lcom/twitter/android/fx;->a:I

    if-ge v2, v3, :cond_d

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/twitter/android/fx;->b:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_d

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/twitter/android/fx;->c:Landroid/widget/ListView;

    invoke-virtual {v4, v2, v1}, Landroid/widget/ListView;->pointToPosition(II)I

    move-result v1

    iget-object v4, p0, Lcom/twitter/android/fx;->c:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    invoke-interface {v4, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    sub-int v1, v2, v3

    iget v6, p0, Lcom/twitter/android/fx;->d:I

    if-le v1, v6, :cond_54

    iget-object v1, p0, Lcom/twitter/android/fx;->e:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v1, v4, v5, p1, v0}, Lcom/twitter/android/TweetListFragment;->a(JLandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_d

    :cond_54
    sub-int v1, v3, v2

    iget v2, p0, Lcom/twitter/android/fx;->d:I

    if-le v1, v2, :cond_d

    iget-object v0, p0, Lcom/twitter/android/fx;->e:Lcom/twitter/android/TweetListFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v5, p1, v1}, Lcom/twitter/android/TweetListFragment;->a(JLandroid/view/MotionEvent;I)Z

    move-result v0

    goto :goto_d
.end method
