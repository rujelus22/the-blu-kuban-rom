.class final Lcom/twitter/android/al;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lcom/twitter/android/EditProfileActivity;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/content/Intent;

.field private final d:Landroid/app/ProgressDialog;

.field private final e:Ljava/lang/String;

.field private final f:J


# direct methods
.method constructor <init>(Lcom/twitter/android/EditProfileActivity;)V
    .registers 4

    iput-object p1, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/al;->c:Landroid/content/Intent;

    iput-object p1, p0, Lcom/twitter/android/al;->b:Landroid/content/Context;

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/twitter/android/al;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0b004f

    invoke-virtual {p1, v1}, Lcom/twitter/android/EditProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/twitter/android/al;->d:Landroid/app/ProgressDialog;

    iget-object v0, p1, Lcom/twitter/android/EditProfileActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/al;->f:J

    iget-object v0, p1, Lcom/twitter/android/EditProfileActivity;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/al;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    if-eqz v0, :cond_38

    iget-object v0, p0, Lcom/twitter/android/al;->b:Landroid/content/Context;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    iget-wide v3, p0, Lcom/twitter/android/al;->f:J

    invoke-static {v0, v3, v4}, Lcom/twitter/android/util/z;->c(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_2a
    invoke-virtual {v1, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_39

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    :cond_38
    :goto_38
    return-object v5

    :cond_39
    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iput-object v5, v0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    goto :goto_38
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 15

    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/EditProfileActivity;->a:Lcom/twitter/android/client/b;

    iget-object v1, p0, Lcom/twitter/android/al;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/b;->b(Ljava/lang/String;)Lcom/twitter/android/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v3, v3, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v4, v4, Lcom/twitter/android/EditProfileActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v5, v5, Lcom/twitter/android/EditProfileActivity;->o:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    invoke-virtual {v6}, Lcom/twitter/android/EditProfileActivity;->a()Z

    move-result v9

    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v6, v6, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    if-eqz v6, :cond_109

    iget-object v6, p0, Lcom/twitter/android/al;->e:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/twitter/android/client/b;->j(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/twitter/android/al;->c:Landroid/content/Intent;

    const-string v7, "update_header"

    invoke-virtual {v6, v7, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-wide v6, p0, Lcom/twitter/android/al;->f:J

    sget-object v8, Lcom/twitter/android/service/ScribeEvent;->as:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v6, v7, v8}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    const v7, 0x7fffffff

    :try_start_5f
    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    invoke-virtual {v6}, Lcom/twitter/android/EditProfileActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v8, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v8, v8, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    invoke-virtual {v6, v8}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_6c
    .catchall {:try_start_5f .. :try_end_6c} :catchall_e2
    .catch Ljava/io/FileNotFoundException; {:try_start_5f .. :try_end_6c} :catch_d4
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_6c} :catch_db

    move-result-object v6

    :try_start_6d
    invoke-virtual {v6}, Ljava/io/InputStream;->available()I
    :try_end_70
    .catchall {:try_start_6d .. :try_end_70} :catchall_102
    .catch Ljava/io/FileNotFoundException; {:try_start_6d .. :try_end_70} :catch_107
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_70} :catch_105

    move-result v7

    invoke-static {v6}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move v6, v7

    :goto_75
    const/high16 v7, 0x50

    if-le v6, v7, :cond_e7

    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v7, v6, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    move-object v8, v10

    :goto_7e
    if-eqz v9, :cond_f2

    new-instance v6, Lcom/twitter/android/client/k;

    invoke-direct {v6, v2, v5, v3, v4}, Lcom/twitter/android/client/k;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Lcom/twitter/android/client/Session;->a(Lcom/twitter/android/client/k;)V

    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v6, v6, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    iget-object v9, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-boolean v9, v9, Lcom/twitter/android/EditProfileActivity;->j:Z

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/graphics/Rect;Z)Ljava/lang/String;

    :goto_93
    iget-object v1, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-boolean v1, v1, Lcom/twitter/android/EditProfileActivity;->j:Z

    if-eqz v1, :cond_b5

    iget-object v1, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-boolean v1, v1, Lcom/twitter/android/EditProfileActivity;->k:Z

    if-eqz v1, :cond_b5

    iget-object v1, p0, Lcom/twitter/android/al;->c:Landroid/content/Intent;

    const-string v2, "remove_header"

    invoke-virtual {v1, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-wide v1, p0, Lcom/twitter/android/al;->f:J

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->at:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iput-boolean v11, v0, Lcom/twitter/android/EditProfileActivity;->j:Z

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iput-boolean v11, v0, Lcom/twitter/android/EditProfileActivity;->k:Z

    :cond_b5
    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iput-object v10, v0, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iput-object v10, v0, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iput-object v10, v0, Lcom/twitter/android/EditProfileActivity;->h:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/al;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/twitter/android/al;->c:Landroid/content/Intent;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/EditProfileActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    return-void

    :catch_d4
    move-exception v6

    move-object v6, v10

    :goto_d6
    invoke-static {v6}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move v6, v7

    goto :goto_75

    :catch_db
    move-exception v6

    move-object v6, v10

    :goto_dd
    invoke-static {v6}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    move v6, v7

    goto :goto_75

    :catchall_e2
    move-exception v0

    :goto_e3
    invoke-static {v10}, Lcom/twitter/android/util/z;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_e7
    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v7, v6, Lcom/twitter/android/EditProfileActivity;->f:Landroid/net/Uri;

    iget-object v6, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    invoke-static {v6}, Lcom/twitter/android/EditProfileActivity;->a(Lcom/twitter/android/EditProfileActivity;)Landroid/graphics/Rect;

    move-result-object v8

    goto :goto_7e

    :cond_f2
    iget-object v2, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-object v6, v2, Lcom/twitter/android/EditProfileActivity;->g:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/al;->a:Lcom/twitter/android/EditProfileActivity;

    iget-boolean v9, v2, Lcom/twitter/android/EditProfileActivity;->j:Z

    move-object v2, v10

    move-object v3, v10

    move-object v4, v10

    move-object v5, v10

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Landroid/graphics/Rect;Z)Ljava/lang/String;

    goto :goto_93

    :catchall_102
    move-exception v0

    move-object v10, v6

    goto :goto_e3

    :catch_105
    move-exception v8

    goto :goto_dd

    :catch_107
    move-exception v8

    goto :goto_d6

    :cond_109
    move-object v8, v10

    move-object v7, v10

    goto/16 :goto_7e
.end method

.method protected final onPreExecute()V
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/al;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method
