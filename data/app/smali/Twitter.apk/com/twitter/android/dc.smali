.class final Lcom/twitter/android/dc;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lcom/twitter/android/PostActivity;

.field private final b:Ljava/io/File;

.field private final c:Lcom/twitter/android/dh;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;Ljava/io/File;Lcom/twitter/android/dh;)V
    .registers 4

    iput-object p1, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/dc;->b:Ljava/io/File;

    iput-object p3, p0, Lcom/twitter/android/dc;->c:Lcom/twitter/android/dh;

    return-void
.end method

.method private varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .registers 6

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0}, Lcom/twitter/android/PostActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/dc;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    const v3, 0x7f0b014b

    invoke-virtual {v2, v3}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;
    :try_end_1b
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_1b} :catch_1c
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_1b} :catch_20

    :goto_1b
    return-object v0

    :catch_1c
    move-exception v0

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_1b

    :catch_20
    move-exception v0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1b
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    check-cast p1, [Ljava/lang/Void;

    invoke-direct {p0, p1}, Lcom/twitter/android/dc;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 5

    check-cast p1, Ljava/lang/Boolean;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne p1, v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v1, p0, Lcom/twitter/android/dc;->c:Lcom/twitter/android/dh;

    invoke-virtual {v0, v1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/dh;)V

    :goto_d
    return-void

    :cond_e
    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->k:Landroid/widget/ImageView;

    const v1, 0x7f020119

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->k:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->l:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v1, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    const v2, 0x7f0b00e2

    invoke-virtual {v1, v2}, Lcom/twitter/android/PostActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_d
.end method

.method protected final onPreExecute()V
    .registers 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->m:Lcom/twitter/android/widget/ShadowTextView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ShadowTextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/dc;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->k:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method
