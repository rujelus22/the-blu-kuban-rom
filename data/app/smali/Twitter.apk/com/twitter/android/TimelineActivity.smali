.class public Lcom/twitter/android/TimelineActivity;
.super Lcom/twitter/android/UserQueryActivity;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/UserQueryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/twitter/android/api/ad;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/android/api/ad;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f070036

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TimelineFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/android/api/ad;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 10

    const v6, 0x7f030021

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_3a

    const-string v3, "twitter"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3a

    move v4, v2

    :goto_1c
    if-eqz v4, :cond_4f

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string v3, "user_timeline"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3c

    move v3, v2

    :goto_2b
    sparse-switch v3, :sswitch_data_be

    invoke-super {p0, p1, v6, v2}, Lcom/twitter/android/UserQueryActivity;->a(Landroid/os/Bundle;IZ)V

    iget-object v0, p0, Lcom/twitter/android/TimelineActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-nez v0, :cond_5a

    :goto_39
    return-void

    :cond_3a
    move v4, v1

    goto :goto_1c

    :cond_3c
    const-string v3, "favorites"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    const/4 v0, 0x2

    move v3, v0

    goto :goto_2b

    :cond_47
    const-string v0, "type"

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    move v3, v0

    goto :goto_2b

    :cond_4f
    const-string v0, "type"

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    move v3, v0

    goto :goto_2b

    :sswitch_57
    invoke-super {p0, p1, v6, v1}, Lcom/twitter/android/UserQueryActivity;->a(Landroid/os/Bundle;IZ)V

    :cond_5a
    move v0, v2

    :goto_5b
    if-eqz v4, :cond_88

    const-string v4, "owner_id"

    iget-wide v6, p0, Lcom/twitter/android/TimelineActivity;->e:J

    invoke-virtual {v5, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v4, "type"

    invoke-virtual {v5, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/TimelineActivity;->f:Ljava/lang/String;

    if-nez v3, :cond_88

    iget-wide v3, p0, Lcom/twitter/android/TimelineActivity;->e:J

    const-wide/16 v6, 0x0

    cmp-long v3, v3, v6

    if-nez v3, :cond_88

    const v0, 0x7f0b0054

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->finish()V

    goto :goto_39

    :sswitch_83
    invoke-super {p0, p1, v6, v2}, Lcom/twitter/android/UserQueryActivity;->a(Landroid/os/Bundle;IZ)V

    move v0, v1

    goto :goto_5b

    :cond_88
    if-nez p1, :cond_ab

    new-instance v2, Lcom/twitter/android/TimelineFragment;

    invoke-direct {v2}, Lcom/twitter/android/TimelineFragment;-><init>()V

    invoke-virtual {v2, p0}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/android/t;)V

    invoke-static {v5, v0}, Lcom/twitter/android/TimelineFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/TimelineFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v3, 0x7f070036

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_ab
    const-string v0, "title"

    const v2, 0x7f0b0025

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TimelineActivity;->a(I[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->a()V

    goto/16 :goto_39

    :sswitch_data_be
    .sparse-switch
        0x1 -> :sswitch_57
        0x2 -> :sswitch_57
        0x9 -> :sswitch_57
        0xc -> :sswitch_83
        0xe -> :sswitch_83
    .end sparse-switch
.end method
