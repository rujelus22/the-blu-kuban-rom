.class public Lcom/twitter/android/SearchActivity;
.super Lcom/twitter/android/BaseFragmentActivity;

# interfaces
.implements Lcom/twitter/android/widget/af;


# static fields
.field private static final f:Ljava/util/HashMap;


# instance fields
.field e:I

.field private g:Ljava/lang/String;

.field private h:Lcom/twitter/android/ar;

.field private i:Lcom/twitter/android/aq;

.field private j:Lcom/twitter/android/aq;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    const/4 v3, 0x6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    const-string v1, "com.twitter.android.action.USER_SHOW"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    const-string v1, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    const-string v1, "com.twitter.android.action.USER_SHOW_SEARCH_SUGGESTION"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    const-string v1, "com.twitter.android.action.SEARCH"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    const-string v1, "com.twitter.android.action.SEARCH_RECENT"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    const-string v1, "com.twitter.android.action.SEARCH_TYPEAHEAD_TOPIC"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/twitter/android/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/android/service/ScribeEvent;Ljava/lang/CharSequence;)V
    .registers 7

    new-instance v0, Lcom/twitter/android/service/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/twitter/android/service/ScribeLog;-><init>(JLcom/twitter/android/service/ScribeEvent;Lcom/twitter/android/service/ScribeEvent;)V

    if-eqz p2, :cond_14

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/service/ScribeLog;->query:Ljava/lang/String;

    :cond_14
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/b;->a(Lcom/twitter/android/service/ScribeLog;)V

    return-void
.end method

.method private a(Landroid/content/Intent;)[Lcom/twitter/android/aq;
    .registers 10

    const/4 v2, 0x0

    const/4 v3, 0x1

    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SegmentedControl;

    const-string v1, "story"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/api/TwitterStory;

    if-eqz v1, :cond_1f

    iget-object v4, v1, Lcom/twitter/android/api/TwitterStory;->socialProof:Lcom/twitter/android/api/TwitterStory$SocialProof;

    if-eqz v4, :cond_1f

    invoke-virtual {v1}, Lcom/twitter/android/api/TwitterStory;->a()Z

    move-result v1

    if-nez v1, :cond_63

    :cond_1f
    move v1, v3

    :goto_20
    new-instance v4, Lcom/twitter/android/aq;

    invoke-static {p1, v1}, Lcom/twitter/android/SearchTweetsFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v5

    const-class v6, Lcom/twitter/android/SearchTweetsFragment;

    const-string v7, "search_tweets"

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/android/aq;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/twitter/android/SearchActivity;->i:Lcom/twitter/android/aq;

    const-string v4, "view_people"

    invoke-virtual {p1, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_65

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SegmentedControl;->a(Lcom/twitter/android/widget/af;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/SegmentedControl;->setVisibility(I)V

    invoke-static {p1, v1}, Lcom/twitter/android/UsersFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "follow"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "type"

    const/4 v4, 0x3

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lcom/twitter/android/aq;

    const-class v4, Lcom/twitter/android/UsersFragment;

    const-string v5, "search_users"

    invoke-direct {v1, v0, v4, v5}, Lcom/twitter/android/aq;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/aq;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/aq;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->i:Lcom/twitter/android/aq;

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/aq;

    aput-object v1, v0, v3

    :goto_62
    return-object v0

    :cond_63
    move v1, v2

    goto :goto_20

    :cond_65
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/aq;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SegmentedControl;->setVisibility(I)V

    new-array v0, v3, [Lcom/twitter/android/aq;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->i:Lcom/twitter/android/aq;

    aput-object v1, v0, v2

    goto :goto_62
.end method

.method private b(Landroid/content/Intent;)V
    .registers 8

    const/4 v2, 0x0

    const-string v0, "twitter"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "context"

    const-string v4, "protocol_search"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move v1, v2

    move-object v3, v0

    :goto_26
    const-string v4, "type"

    invoke-virtual {p1, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iput v1, p0, Lcom/twitter/android/SearchActivity;->e:I

    iput-object v3, p0, Lcom/twitter/android/SearchActivity;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_ee

    const v0, 0x7f0b00a5

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchActivity;->a(I[Ljava/lang/Object;)V

    :goto_43
    return-void

    :cond_44
    sget-object v0, Lcom/twitter/android/SearchActivity;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string v1, "name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "query"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_6b

    const-string v4, "user_query"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_f8

    :cond_6b
    :goto_6b
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_db

    const-string v0, "type"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_26

    :pswitch_7b
    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->z:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/service/ScribeEvent;Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "screen_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto :goto_43

    :pswitch_94
    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->x:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/service/ScribeEvent;Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "screen_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto :goto_43

    :pswitch_ad
    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->y:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/service/ScribeEvent;Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "screen_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_43

    :pswitch_c7
    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->u:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/service/ScribeEvent;Ljava/lang/CharSequence;)V

    goto :goto_6b

    :pswitch_cd
    const-string v0, "context"

    const-string v4, "recent"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_6b

    :pswitch_d5
    sget-object v0, Lcom/twitter/android/service/ScribeEvent;->w:Lcom/twitter/android/service/ScribeEvent;

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/service/ScribeEvent;Ljava/lang/CharSequence;)V

    goto :goto_6b

    :cond_db
    const-string v4, "type"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto/16 :goto_26

    :cond_ee
    invoke-virtual {p0, v3}, Lcom/twitter/android/SearchActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_43

    :cond_f3
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->b(Ljava/lang/String;)V

    goto/16 :goto_43

    :pswitch_data_f8
    .packed-switch 0x1
        :pswitch_7b
        :pswitch_94
        :pswitch_ad
        :pswitch_c7
        :pswitch_cd
        :pswitch_d5
    .end packed-switch
.end method


# virtual methods
.method public final a_(I)V
    .registers 7

    const/4 v4, 0x0

    const v0, 0x7f070054

    if-ne p1, v0, :cond_56

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_56

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_56

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/PostActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "selection"

    const/4 v3, 0x2

    new-array v3, v3, [I

    aput v4, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    aput v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.twitter.android.post.status"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    :goto_55
    return-void

    :cond_56
    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->a_(I)V

    goto :goto_55
.end method

.method public final b(I)V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ar;->a(I)Z

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6

    const v0, 0x7f030047

    const/4 v1, 0x0

    invoke-super {p0, p1, v0, v1}, Lcom/twitter/android/BaseFragmentActivity;->a(Landroid/os/Bundle;IZ)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->b(Landroid/content/Intent;)V

    new-instance v1, Lcom/twitter/android/eq;

    invoke-direct {v1, p0}, Lcom/twitter/android/eq;-><init>(Lcom/twitter/android/SearchActivity;)V

    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->d:Lcom/twitter/android/client/j;

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Landroid/content/Intent;)[Lcom/twitter/android/aq;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ar;

    const v3, 0x7f070036

    invoke-direct {v2, p0, v3, v1}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/aq;)V

    iput-object v2, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    if-eqz p1, :cond_4b

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    const-string v1, "state_tag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->b(Ljava/lang/String;)V

    const-string v0, "search_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchActivity;->e:I

    :goto_38
    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SegmentedControl;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    invoke-virtual {v1}, Lcom/twitter/android/ar;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SegmentedControl;->a(I)V

    return-void

    :cond_4b
    const-string v1, "current_segment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_59

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    goto :goto_38

    :cond_59
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    const-string v1, "search_tweets"

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    goto :goto_38
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v1}, Lcom/twitter/android/client/b;->i()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    const v3, 0x7f100007

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    :cond_16
    if-eqz v0, :cond_1c

    if-eqz v1, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 6

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/SearchActivity;->b(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->i:Lcom/twitter/android/aq;

    if-eqz v1, :cond_22

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->i:Lcom/twitter/android/aq;

    invoke-virtual {v2, p0}, Lcom/twitter/android/aq;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_22
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/aq;

    if-eqz v1, :cond_37

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->j:Lcom/twitter/android/aq;

    invoke-virtual {v2, p0}, Lcom/twitter/android/aq;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_37
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    invoke-virtual {v0}, Lcom/twitter/android/ar;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/SearchActivity;->a(Landroid/content/Intent;)[Lcom/twitter/android/aq;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/ar;

    const v3, 0x7f070036

    invoke-direct {v2, p0, v3, v1}, Lcom/twitter/android/ar;-><init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/aq;)V

    iput-object v2, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    const-string v1, "search_tweets"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_70

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    const-string v1, "search_tweets"

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    :goto_5d
    const v0, 0x7f070049

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SegmentedControl;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    invoke-virtual {v1}, Lcom/twitter/android/ar;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SegmentedControl;->a(I)V

    return-void

    :cond_70
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    const-string v1, "search_users"

    invoke-virtual {v0, v1}, Lcom/twitter/android/ar;->a(Ljava/lang/String;)Z

    goto :goto_5d
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6

    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_40

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_d
    return v0

    :pswitch_e
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/b;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->c(Ljava/lang/String;)V

    const v1, 0x7f0b0164

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const/4 v1, 0x6

    iput v1, p0, Lcom/twitter/android/SearchActivity;->e:I

    goto :goto_d

    :pswitch_27
    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/client/b;

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->c(Ljava/lang/String;)V

    const v1, 0x7f0b0165

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iput v3, p0, Lcom/twitter/android/SearchActivity;->e:I

    goto :goto_d

    nop

    :pswitch_data_40
    .packed-switch 0x7f070104
        :pswitch_e
        :pswitch_27
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/client/b;

    invoke-virtual {v0}, Lcom/twitter/android/client/b;->i()Z

    move-result v0

    if-nez v0, :cond_b

    :goto_a
    return v2

    :cond_b
    iget v0, p0, Lcom/twitter/android/SearchActivity;->e:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_2c

    move v0, v1

    :goto_11
    const v3, 0x7f070105

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const v3, 0x7f070104

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-nez v0, :cond_2e

    :goto_24
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    goto :goto_a

    :cond_2c
    move v0, v2

    goto :goto_11

    :cond_2e
    move v1, v2

    goto :goto_24
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4

    invoke-super {p0, p1}, Lcom/twitter/android/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "state_tag"

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->h:Lcom/twitter/android/ar;

    invoke-virtual {v1}, Lcom/twitter/android/ar;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "search_type"

    iget v1, p0, Lcom/twitter/android/SearchActivity;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
