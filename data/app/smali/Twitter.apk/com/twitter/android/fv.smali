.class final Lcom/twitter/android/fv;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/twitter/android/gf;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetListFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetListFragment;)V
    .registers 2

    iput-object p1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .registers 3

    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetListFragment;->d(I)I

    return-void
.end method

.method public final a(Landroid/view/View;Lcom/twitter/android/provider/m;)V
    .registers 15

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    iget-object v4, v0, Lcom/twitter/android/TweetListFragment;->c:Lcom/twitter/android/client/b;

    iget-object v5, p2, Lcom/twitter/android/provider/m;->I:Lcom/twitter/android/api/PromotedContent;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_cc

    :goto_e
    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/MotionEvent;)V

    return-void

    :sswitch_15
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/PostActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "reply_to_tweet"

    new-array v2, v3, [Lcom/twitter/android/provider/m;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.twitter.android.post.reply"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetListFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/android/service/ScribeEvent;->ac:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v0, v1, v2}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_e

    :sswitch_40
    iget-boolean v0, p2, Lcom/twitter/android/provider/m;->l:Z

    if-eqz v0, :cond_5d

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v0

    iget-wide v1, p2, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual {v4, v0, v1, v2, v5}, Lcom/twitter/android/client/b;->c(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->ag:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    :goto_57
    iget-object v1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetListFragment;->b(Ljava/lang/String;)V

    goto :goto_e

    :cond_5d
    invoke-virtual {v4}, Lcom/twitter/android/client/b;->g()Lcom/twitter/android/client/Session;

    move-result-object v0

    iget-wide v1, p2, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual {v4, v0, v1, v2, v5}, Lcom/twitter/android/client/b;->b(Lcom/twitter/android/client/Session;JLcom/twitter/android/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/service/ScribeEvent;->af:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v1, v2, v3}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto :goto_57

    :sswitch_71
    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->k()J

    move-result-wide v6

    invoke-virtual {p2, v6, v7}, Lcom/twitter/android/provider/m;->a(J)Z

    move-result v2

    new-instance v0, Lcom/twitter/android/fw;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/fw;-><init>(Lcom/twitter/android/fv;ZLcom/twitter/android/provider/m;Lcom/twitter/android/client/b;Lcom/twitter/android/api/PromotedContent;JLandroid/content/Context;)V

    iget-object v1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v2, v0}, Lcom/twitter/android/client/b;->a(Landroid/app/Activity;ZLandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_e

    :sswitch_95
    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    iput-object p2, v0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/provider/m;

    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v0, v3}, Lcom/twitter/android/TweetListFragment;->f(I)V

    goto/16 :goto_e

    :sswitch_a0
    invoke-virtual {p2}, Lcom/twitter/android/provider/m;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p2, Lcom/twitter/android/provider/m;->p:Ljava/lang/String;

    iget-object v7, p2, Lcom/twitter/android/provider/m;->d:Ljava/lang/String;

    iget-wide v8, p2, Lcom/twitter/android/provider/m;->h:J

    iget-wide v10, p2, Lcom/twitter/android/provider/m;->o:J

    invoke-virtual/range {v4 .. v11}, Lcom/twitter/android/client/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetListFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {v4}, Lcom/twitter/android/client/b;->a()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/android/service/ScribeEvent;->ah:Lcom/twitter/android/service/ScribeEvent;

    invoke-virtual {v4, v0, v1, v2}, Lcom/twitter/android/client/b;->a(JLcom/twitter/android/service/ScribeEvent;)V

    goto/16 :goto_e

    :sswitch_c0
    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    iput-object p2, v0, Lcom/twitter/android/TweetListFragment;->B:Lcom/twitter/android/provider/m;

    iget-object v0, p0, Lcom/twitter/android/fv;->a:Lcom/twitter/android/TweetListFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetListFragment;->f(I)V

    goto/16 :goto_e

    :sswitch_data_cc
    .sparse-switch
        0x7f07001e -> :sswitch_15
        0x7f07001f -> :sswitch_71
        0x7f070020 -> :sswitch_40
        0x7f070021 -> :sswitch_a0
        0x7f070022 -> :sswitch_95
        0x7f0700cc -> :sswitch_c0
    .end sparse-switch
.end method

.method public final b()V
    .registers 1

    return-void
.end method
