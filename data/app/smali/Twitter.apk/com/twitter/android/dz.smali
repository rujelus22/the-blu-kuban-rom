.class final Lcom/twitter/android/dz;
.super Lcom/twitter/android/widget/d;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Lcom/twitter/android/api/ad;

.field private final d:Lcom/twitter/android/fe;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/el;)V
    .registers 9

    const/16 v0, 0xb

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/widget/d;-><init>(Landroid/widget/ListAdapter;I)V

    iput-object p1, p0, Lcom/twitter/android/dz;->b:Landroid/content/Context;

    new-instance v0, Lcom/twitter/android/fe;

    const v1, 0x7f0b0039

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/UsersActivity;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "type"

    const/16 v5, 0xa

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v3

    const-string v4, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/fe;-><init>(Ljava/lang/String;ILandroid/content/Intent;)V

    iput-object v0, p0, Lcom/twitter/android/dz;->d:Lcom/twitter/android/fe;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8

    iget-object v1, p0, Lcom/twitter/android/dz;->b:Landroid/content/Context;

    const v2, 0x7f0b0038

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/dz;->c:Lcom/twitter/android/api/ad;

    if-nez v0, :cond_1a

    const-string v0, ""

    :goto_f
    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/twitter/android/dz;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1a
    iget-object v0, p0, Lcom/twitter/android/dz;->c:Lcom/twitter/android/api/ad;

    invoke-virtual {v0}, Lcom/twitter/android/api/ad;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_f
.end method

.method protected final a()Ljava/lang/Object;
    .registers 2

    iget-object v0, p0, Lcom/twitter/android/dz;->d:Lcom/twitter/android/fe;

    iget-object v0, v0, Lcom/twitter/android/fe;->c:Landroid/content/Intent;

    return-object v0
.end method

.method public final a(Lcom/twitter/android/api/ad;)V
    .registers 6

    iget-object v0, p0, Lcom/twitter/android/dz;->c:Lcom/twitter/android/api/ad;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/dz;->c:Lcom/twitter/android/api/ad;

    invoke-virtual {v0, p1}, Lcom/twitter/android/api/ad;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_c
    return-void

    :cond_d
    iget-object v0, p0, Lcom/twitter/android/dz;->d:Lcom/twitter/android/fe;

    iget-object v0, v0, Lcom/twitter/android/fe;->c:Landroid/content/Intent;

    const-string v1, "username"

    iget-object v2, p1, Lcom/twitter/android/api/ad;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    iget-wide v2, p1, Lcom/twitter/android/api/ad;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iput-object p1, p0, Lcom/twitter/android/dz;->c:Lcom/twitter/android/api/ad;

    invoke-virtual {p0}, Lcom/twitter/android/dz;->notifyDataSetChanged()V

    goto :goto_c
.end method

.method protected final b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 4

    iget-object v0, p0, Lcom/twitter/android/dz;->d:Lcom/twitter/android/fe;

    invoke-static {p1, p2, v0}, Lcom/twitter/android/fg;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/fe;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
