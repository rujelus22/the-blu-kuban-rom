.class public Lcom/samsungframeworks/internal/PhoneInfoInterface;
.super Lcom/samsungframeworks/internal/IPhoneInfoInterface$Stub;
.source "PhoneInfoInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;
    }
.end annotation


# static fields
.field static final DISCONNECT_WAIT_TIME:I = 0xfa0

.field private static final EVENT_ERROR:I = -0x1

.field private static final EVENT_GET:I = 0x1

.field private static final EVENT_GET_DATA_PROF:I = 0x3

.field private static final EVENT_GET_PRL:I = 0x6

.field private static final EVENT_NOT_RESPOND:I = 0x5

.field private static final EVENT_SET:I = 0x2

.field private static final EVENT_SET_DATA_PROF:I = 0x4

.field static final ID_SETUPCOMPLETED:I = 0x2710

.field private static final LOG_TAG:Ljava/lang/String; = "PhoneInfoInterface"

.field static final OEM_MAIN_CMD_HIDDEN:I = 0x9

.field static final OEM_SUB_CMD_AKEY_VERIFY:I = 0x1

.field static final OEM_SUB_CMD_GET_MSL:I = 0x2

.field static final VM_COUNT_CDMA:Ljava/lang/String; = "vm_count_key_cdma"

.field private static kl:Landroid/app/KeyguardManager$KeyguardLock;

.field private static km:Landroid/app/KeyguardManager;

.field private static pm:Landroid/os/PowerManager;

.field private static wl:Landroid/os/PowerManager$WakeLock;


# instance fields
.field CMD_WAIT_TIME:I

.field private DB_TABLE:Ljava/lang/String;

.field private changingSlot:Z

.field private curCmd:I

.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field private mConnMgr:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mDPM:Landroid/app/admin/DevicePolicyManager;

.field private mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

.field mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

.field mPIMThread:Landroid/os/HandlerThread;

.field mPhone:Lcom/android/internal/telephony/Phone;

.field private final mPhoneLock:Ljava/lang/Object;

.field mPref:Landroid/content/SharedPreferences;

.field mdn:Ljava/lang/String;

.field proxyBackup:Ljava/lang/String;

.field private useOamdm:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 95
    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    .line 97
    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    .line 99
    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->km:Landroid/app/KeyguardManager;

    .line 101
    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->kl:Landroid/app/KeyguardManager$KeyguardLock;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "contxt"

    .prologue
    const/4 v2, 0x0

    .line 132
    invoke-direct {p0}, Lcom/samsungframeworks/internal/IPhoneInfoInterface$Stub;-><init>()V

    .line 65
    const/16 v1, 0x1f40

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->CMD_WAIT_TIME:I

    .line 67
    iput v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 81
    const-string v1, "t_systemproperty"

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    .line 89
    iput-boolean v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->changingSlot:Z

    .line 91
    iput-boolean v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->useOamdm:Z

    .line 93
    new-instance v1, Ljava/lang/Object;

    invoke-direct/range {v1 .. v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    .line 133
    iput-object p1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    .line 134
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "mSSThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIMThread:Landroid/os/HandlerThread;

    .line 135
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIMThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 137
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIMThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 138
    .local v0, looper:Landroid/os/Looper;
    new-instance v1, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    invoke-direct {v1, p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;-><init>(Lcom/samsungframeworks/internal/PhoneInfoInterface;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    .line 139
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v1

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    .line 140
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->PrepareInternalDB()V

    .line 146
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->setSystemProp()V

    .line 148
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v2, "device_policy"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 149
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager;->getMiscPolicy()Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    move-result-object v1

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    .line 150
    return-void
.end method

.method private GetValueInternalDB(I)Ljava/lang/String;
    .registers 12
    .parameter "id"

    .prologue
    .line 215
    const/4 v6, 0x0

    .line 216
    .local v6, ret:Ljava/lang/String;
    const/4 v3, 0x0

    .line 219
    .local v3, cursor:Landroid/database/Cursor;
    const/16 v7, 0x12c

    if-ne v7, p1, :cond_49

    .line 220
    :try_start_6
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'MobileSyncEnabled\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_27
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_27} :catch_1ff

    move-result-object v3

    .line 263
    :cond_28
    :goto_28
    if-eqz v3, :cond_48

    .line 265
    const-string v7, "FieldName"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 266
    .local v5, firstFieldNameColumn:I
    const-string v7, "FieldValue"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 269
    .local v2, FieldValueColumn:I
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_45

    .line 270
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, FieldName:Ljava/lang/String;
    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 273
    .local v1, FieldValue:Ljava/lang/String;
    move-object v6, v1

    .line 275
    .end local v0           #FieldName:Ljava/lang/String;
    .end local v1           #FieldValue:Ljava/lang/String;
    :cond_45
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 277
    .end local v2           #FieldValueColumn:I
    .end local v5           #firstFieldNameColumn:I
    :cond_48
    return-object v6

    .line 222
    :cond_49
    const/16 v7, 0x192

    if-ne v7, p1, :cond_70

    .line 223
    :try_start_4d
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'DSAServerURL\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_28

    .line 225
    :cond_70
    const/16 v7, 0xa1

    if-ne v7, p1, :cond_97

    .line 226
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'PDEPrimaryIP\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_28

    .line 228
    :cond_97
    const/16 v7, 0xa3

    if-ne v7, p1, :cond_bf

    .line 229
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'PDEPrimaryPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 231
    :cond_bf
    const/16 v7, 0x2710

    if-ne v7, p1, :cond_e7

    .line 232
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'SetUpCompleted\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 234
    :cond_e7
    const/16 v7, 0x195

    if-ne v7, p1, :cond_10f

    .line 235
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'DSAServerIP\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 237
    :cond_10f
    const/16 v7, 0x196

    if-ne v7, p1, :cond_137

    .line 238
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'DSAServerPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 240
    :cond_137
    const/16 v7, 0x19c

    if-ne v7, p1, :cond_15f

    .line 241
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'RTSPProxyAddr\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 243
    :cond_15f
    const/16 v7, 0x19d

    if-ne v7, p1, :cond_187

    .line 244
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'RTSPProxyPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 246
    :cond_187
    const/16 v7, 0x19e

    if-ne v7, p1, :cond_1af

    .line 247
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'HTTPPDProxyAddr\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 249
    :cond_1af
    const/16 v7, 0x19f

    if-ne v7, p1, :cond_1d7

    .line 250
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'HTTPPDProxyPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_28

    .line 253
    :cond_1d7
    const/16 v7, 0x1f4

    if-ne v7, p1, :cond_28

    .line 254
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "select FieldName,FieldValue from "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " where FieldName==\'WorkMode\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1fc
    .catch Landroid/database/SQLException; {:try_start_4d .. :try_end_1fc} :catch_1ff

    move-result-object v3

    goto/16 :goto_28

    .line 259
    :catch_1ff
    move-exception v4

    .line 260
    .local v4, e:Landroid/database/SQLException;
    const-string v7, "PhoneInfoInterface"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SQLException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_28
.end method

.method private PrepareInternalDB()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 154
    const/4 v1, 0x1

    .line 155
    .local v1, DB_VERSION:I
    const/4 v0, 0x0

    .line 158
    .local v0, DB_MODE:I
    :try_start_3
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v4, "db_systemproperty"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/Context;->openOrCreateDatabase(Ljava/lang/String;ILandroid/database/sqlite/SQLiteDatabase$CursorFactory;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    iput-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 159
    const-string v3, "PhoneInfoInterface"

    const-string v4, "Success Create DB"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_15} :catch_1fe

    .line 167
    :goto_15
    :try_start_15
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create table if not exists "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName VARCHAR PRIMARY KEY, FieldValue VARCHAR)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 169
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'MobileSyncEnabled\', \'true\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 172
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'DSAServerURL\',\'https://dsa.spcsdns.net:443/dsa/\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 175
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'PDEPrimaryIP\',\'0.0.0.0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 178
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'PDEPrimaryPort\',\'0.0.0.0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 181
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'SetUpCompleted\',\'false\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 184
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'DSAServerIP\',\'144.226.247.31\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 187
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'DSAServerPort\',\'80\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 190
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'RTSPProxyAddr\',\'0.0.0.0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 193
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'RTSPProxyPort\',\'0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 196
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'HTTPPDProxyAddr\',\'0.0.0.0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 199
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'HTTPPDProxyPort\',\'0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 204
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert into "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (FieldName, FieldValue)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " values (\'WorkMode\',\'0\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1fd
    .catch Landroid/database/SQLException; {:try_start_15 .. :try_end_1fd} :catch_20a

    .line 212
    :goto_1fd
    return-void

    .line 160
    :catch_1fe
    move-exception v2

    .line 161
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    iput-object v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 162
    const-string v3, "PhoneInfoInterface"

    const-string v4, "Fail to Create DB"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_15

    .line 208
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    :catch_20a
    move-exception v2

    .line 209
    .local v2, e:Landroid/database/SQLException;
    const-string v3, "PhoneInfoInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SQLException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1fd
.end method

.method private SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "id"
    .parameter "value"

    .prologue
    .line 281
    const/4 v6, 0x0

    .line 282
    .local v6, ret:Ljava/lang/String;
    const/4 v3, 0x0

    .line 285
    .local v3, cursor:Landroid/database/Cursor;
    const/16 v7, 0x12c

    if-ne v7, p1, :cond_59

    .line 286
    :try_start_6
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'MobileSyncEnabled\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_37
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_37} :catch_2c0

    move-result-object v3

    .line 329
    :cond_38
    :goto_38
    if-eqz v3, :cond_58

    .line 331
    const-string v7, "FieldName"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 332
    .local v5, firstFieldNameColumn:I
    const-string v7, "FieldValue"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 335
    .local v2, FieldValueColumn:I
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_55

    .line 336
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, FieldName:Ljava/lang/String;
    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, FieldValue:Ljava/lang/String;
    move-object v6, v1

    .line 341
    .end local v0           #FieldName:Ljava/lang/String;
    .end local v1           #FieldValue:Ljava/lang/String;
    :cond_55
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 343
    .end local v2           #FieldValueColumn:I
    .end local v5           #firstFieldNameColumn:I
    :cond_58
    return-object v6

    .line 288
    :cond_59
    const/16 v7, 0x192

    if-ne v7, p1, :cond_90

    .line 289
    :try_start_5d
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'DSAServerURL\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_38

    .line 291
    :cond_90
    const/16 v7, 0xa1

    if-ne v7, p1, :cond_c8

    .line 292
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'PDEPrimaryIP\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 294
    :cond_c8
    const/16 v7, 0xa3

    if-ne v7, p1, :cond_100

    .line 295
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'PDEPrimaryPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 297
    :cond_100
    const/16 v7, 0x2710

    if-ne v7, p1, :cond_138

    .line 298
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'SetUpCompleted\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 300
    :cond_138
    const/16 v7, 0x195

    if-ne v7, p1, :cond_170

    .line 301
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'DSAServerIP\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 303
    :cond_170
    const/16 v7, 0x196

    if-ne v7, p1, :cond_1a8

    .line 304
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'DSAServerPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 306
    :cond_1a8
    const/16 v7, 0x19c

    if-ne v7, p1, :cond_1e0

    .line 307
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'RTSPProxyAddr\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 309
    :cond_1e0
    const/16 v7, 0x19d

    if-ne v7, p1, :cond_218

    .line 310
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'RTSPProxyPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 312
    :cond_218
    const/16 v7, 0x19e

    if-ne v7, p1, :cond_250

    .line 313
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'HTTPPDProxyAddr\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 315
    :cond_250
    const/16 v7, 0x19f

    if-ne v7, p1, :cond_288

    .line 316
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'HTTPPDProxyPort\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto/16 :goto_38

    .line 319
    :cond_288
    const/16 v7, 0x1f4

    if-ne v7, p1, :cond_38

    .line 320
    iget-object v7, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->db:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "update "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->DB_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " set FieldValue = \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "where FieldName==\'WorkMode\'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2bd
    .catch Landroid/database/SQLException; {:try_start_5d .. :try_end_2bd} :catch_2c0

    move-result-object v3

    goto/16 :goto_38

    .line 325
    :catch_2c0
    move-exception v4

    .line 326
    .local v4, e:Landroid/database/SQLException;
    const-string v7, "PhoneInfoInterface"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SQLException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_38
.end method

.method static synthetic access$000(Lcom/samsungframeworks/internal/PhoneInfoInterface;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 36
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    return v0
.end method

.method private enforceUID()V
    .registers 5

    .prologue
    .line 347
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 349
    .local v0, uid:I
    const/16 v1, 0xfa1

    if-eq v0, v1, :cond_36

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_36

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_36

    .line 351
    const-string v1, "PhoneInfoInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enforceUID() uid"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not sprint extension"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "user is not sprint extension"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 354
    :cond_36
    return-void
.end method

.method private fetchLatestNotNullValue(ILjava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "propId"
    .parameter "propValue"

    .prologue
    .line 1003
    const/4 v2, 0x0

    .line 1004
    .local v2, prefId:Ljava/lang/String;
    const/4 v3, 0x0

    .line 1007
    .local v3, prefValue:Ljava/lang/String;
    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v5, "SPR_Prefs"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1011
    .local v1, mPref:Landroid/content/SharedPreferences;
    const-string v4, "PhoneInfoInterface"

    const-string v5, "Inside fetchLatestNotNullValue"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    if-eqz v1, :cond_5c

    .line 1015
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1024
    .local v0, edit:Landroid/content/SharedPreferences$Editor;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pref"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1029
    if-eqz p2, :cond_64

    .line 1031
    const-string v4, "PhoneInfoInterface"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " propId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  has fresh value as : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1034
    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1037
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1049
    :goto_55
    const/4 v4, 0x0

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object p2, v3

    .line 1050
    .end local v0           #edit:Landroid/content/SharedPreferences$Editor;
    .end local p2
    :goto_5b
    return-object p2

    .line 1019
    .restart local p2
    :cond_5c
    const-string v4, "PhoneInfoInterface"

    const-string v5, "fetchLatestNotNullValue  : mPref null Returning"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5b

    .line 1045
    .restart local v0       #edit:Landroid/content/SharedPreferences$Editor;
    :cond_64
    const-string v4, "PhoneInfoInterface"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " propId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " value came as null, stored value is  : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_55
.end method

.method private isNetworkAvailable()Z
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 1124
    const/4 v1, 0x0

    .local v1, i:I
    :goto_2
    const/16 v3, 0x28

    if-ge v1, v3, :cond_16

    .line 1125
    const/4 v3, 0x0

    :try_start_7
    invoke-direct {p0, v3}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->isNetworkAvailable(I)Z

    move-result v3

    if-nez v3, :cond_15

    .line 1126
    const-wide/16 v3, 0x1f4

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_12} :catch_17

    .line 1124
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1133
    :cond_15
    const/4 v2, 0x1

    .line 1138
    :cond_16
    :goto_16
    return v2

    .line 1135
    :catch_17
    move-exception v0

    .line 1136
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "PhoneInfoInterface"

    const-string v4, "isNetworkAvailable:wait"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_16
.end method

.method private isNetworkAvailable(I)Z
    .registers 8
    .parameter "networkType"

    .prologue
    const/4 v3, 0x0

    .line 1143
    const/4 v1, 0x0

    .line 1145
    .local v1, info:Landroid/net/NetworkInfo;
    :try_start_2
    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 1146
    .local v2, mCM:Landroid/net/ConnectivityManager;
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_f} :catch_13

    move-result-object v1

    .line 1150
    .end local v2           #mCM:Landroid/net/ConnectivityManager;
    :goto_10
    if-nez v1, :cond_1c

    .line 1154
    :cond_12
    :goto_12
    return v3

    .line 1147
    :catch_13
    move-exception v0

    .line 1148
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "PhoneInfoInterface"

    const-string v5, "isNetworkAvailable"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_10

    .line 1151
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1c
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, p1, :cond_12

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v4

    sget-object v5, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v4, v5, :cond_12

    .line 1154
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    goto :goto_12
.end method

.method private setSystemProp()V
    .registers 3

    .prologue
    .line 1191
    const/16 v1, 0x19c

    invoke-direct {p0, v1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    .line 1192
    .local v0, value:Ljava/lang/String;
    const-string v1, "net.cdma.rtsp.proxy.addr"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1194
    const/16 v1, 0x19d

    invoke-direct {p0, v1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    .line 1195
    const-string v1, "net.cdma.rtsp.proxy.port"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1197
    const/16 v1, 0x19e

    invoke-direct {p0, v1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    .line 1198
    const-string v1, "net.cdma.httppd.proxy.addr"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1200
    const/16 v1, 0x19f

    invoke-direct {p0, v1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    .line 1201
    const-string v1, "net.cdma.httppd.proxy.port"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1202
    return-void
.end method

.method private stopAndWaitReturn()Ljava/lang/String;
    .registers 5

    .prologue
    .line 1208
    :try_start_0
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    iget v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->CMD_WAIT_TIME:I

    invoke-virtual {v1, v2}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->waitResponse(I)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_7} :catch_e

    .line 1212
    :goto_7
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    invoke-virtual {v1}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->getPropertyValue()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 1209
    :catch_e
    move-exception v0

    .line 1210
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "PhoneInfoInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "InterruptedException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7
.end method


# virtual methods
.method public IsBluetoothEnabled()V
    .registers 4

    .prologue
    .line 1352
    const-string v1, "PhoneInfoInterface"

    const-string v2, "IsBluetoothEnabled()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->isBluetoothEnabled(Z)Z

    move-result v0

    .line 1356
    .local v0, mRet:Z
    return-void
.end method

.method public IsCameraEnabled()V
    .registers 4

    .prologue
    .line 1359
    const-string v1, "PhoneInfoInterface"

    const-string v2, "IsCameraEnabled()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->isCameraEnabled(Z)Z

    move-result v0

    .line 1363
    .local v0, mRet:Z
    return-void
.end method

.method public IsEncryptDeviceEnabled()V
    .registers 4

    .prologue
    .line 1366
    const-string v1, "PhoneInfoInterface"

    const-string v2, "IsEncryptDeviceEnabled()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1369
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->isInternalStorageEncrypted()Z

    move-result v0

    .line 1370
    .local v0, mRet:Z
    return-void
.end method

.method public IsEncryptSDCardEnable()V
    .registers 4

    .prologue
    .line 1373
    const-string v1, "PhoneInfoInterface"

    const-string v2, "IsEncryptSDCardEnable()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1376
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    invoke-virtual {v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->isExternalStorageEncrypted()Z

    move-result v0

    .line 1377
    .local v0, mRet:Z
    return-void
.end method

.method public IsWiFiEnabled()V
    .registers 4

    .prologue
    .line 1380
    const-string v1, "PhoneInfoInterface"

    const-string v2, "IsWiFiEnabled()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->isWiFiEnabled(Z)Z

    move-result v0

    .line 1384
    .local v0, mRet:Z
    return-void
.end method

.method public SetDisableBluetooth()V
    .registers 3

    .prologue
    .line 1388
    const-string v0, "PhoneInfoInterface"

    const-string v1, "SetDisableBluetooth()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1390
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->setBluetoothState(Z)Z

    .line 1391
    return-void
.end method

.method public SetDisableCamera()V
    .registers 3

    .prologue
    .line 1394
    const-string v0, "PhoneInfoInterface"

    const-string v1, "SetDisableCamera()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->setCameraState(Z)Z

    .line 1397
    return-void
.end method

.method public SetDisableWifi()V
    .registers 3

    .prologue
    .line 1400
    const-string v0, "PhoneInfoInterface"

    const-string v1, "SetDisableWifi()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1402
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->setWiFiState(Z)Z

    .line 1403
    return-void
.end method

.method public SetEncryptDevice()V
    .registers 3

    .prologue
    .line 1406
    const-string v0, "PhoneInfoInterface"

    const-string v1, "SetEncryptDevice()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1408
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->setInternalStorageEncryption(Z)V

    .line 1409
    return-void
.end method

.method public SetEncryptSDCard()V
    .registers 3

    .prologue
    .line 1412
    const-string v0, "PhoneInfoInterface"

    const-string v1, "SetEncryptSDCard()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1414
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mMP:Landroid/app/admin/DevicePolicyManager$MiscPolicy;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager$MiscPolicy;->setExternalStorageEncryption(Z)V

    .line 1415
    return-void
.end method

.method public disableDataconnection()V
    .registers 3

    .prologue
    .line 974
    const-string v0, "PhoneInfoInterface"

    const-string v1, "disableDataconnection"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->disableDataConnectivity()Z

    .line 976
    return-void
.end method

.method public disableScreenTimeout()V
    .registers 4

    .prologue
    .line 877
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 879
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    if-nez v0, :cond_13

    .line 880
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    .line 882
    :cond_13
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_24

    .line 883
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    const v1, 0x3000000a

    const-string v2, "SprintPowerLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    .line 888
    :cond_24
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_31

    .line 889
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 892
    :cond_31
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->km:Landroid/app/KeyguardManager;

    if-nez v0, :cond_41

    .line 893
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->km:Landroid/app/KeyguardManager;

    .line 895
    :cond_41
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->kl:Landroid/app/KeyguardManager$KeyguardLock;

    if-nez v0, :cond_4f

    .line 896
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->km:Landroid/app/KeyguardManager;

    const-string v1, "SprintKeyLock"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v0

    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->kl:Landroid/app/KeyguardManager$KeyguardLock;

    .line 899
    :cond_4f
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->kl:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V

    .line 900
    return-void
.end method

.method public enableScreenTimeout()V
    .registers 3

    .prologue
    .line 866
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 868
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_15

    .line 869
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 871
    :cond_15
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->kl:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v0, :cond_1e

    .line 872
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->kl:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->reenableKeyguard()V

    .line 874
    :cond_1e
    return-void
.end method

.method public getA_Key()Ljava/lang/String;
    .registers 2

    .prologue
    .line 414
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCdmaPrlVersion()Ljava/lang/String;
    .registers 13

    .prologue
    const/4 v11, 0x2

    .line 915
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 917
    const/4 v4, 0x0

    .line 919
    .local v4, ret:Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 920
    .local v0, bos:Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 922
    .local v1, dos:Ljava/io/DataOutputStream;
    iget-object v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v6

    .line 923
    :try_start_12
    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I
    :try_end_18
    .catchall {:try_start_12 .. :try_end_18} :catchall_68

    .line 927
    const/4 v3, 0x3

    .line 928
    .local v3, fileSize:I
    const/16 v5, 0x9

    :try_start_1b
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 929
    const/16 v5, 0x14

    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 930
    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeByte(I)V
    :try_end_26
    .catchall {:try_start_1b .. :try_end_26} :catchall_68
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_26} :catch_5d

    .line 938
    :try_start_26
    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    iget-object v8, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v9, 0x6

    iget v10, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    invoke-interface {v5, v7, v8}, Lcom/android/internal/telephony/Phone;->invokeOemRilRequestRaw([BLandroid/os/Message;)V

    .line 940
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 942
    const-string v5, "ril.prl_ver_1"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 944
    if-eqz v4, :cond_6b

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v11, :cond_6b

    .line 945
    const/4 v5, 0x2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 949
    :goto_5a
    monitor-exit v6

    move-object v5, v4

    .line 951
    :goto_5c
    return-object v5

    .line 932
    :catch_5d
    move-exception v2

    .line 934
    .local v2, e:Ljava/io/IOException;
    const-string v5, "PhoneInfoInterface"

    const-string v7, "getCdmaPrlVersionexception occured during operation read PRL"

    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    const/4 v5, 0x0

    monitor-exit v6

    goto :goto_5c

    .line 949
    .end local v2           #e:Ljava/io/IOException;
    .end local v3           #fileSize:I
    :catchall_68
    move-exception v5

    monitor-exit v6
    :try_end_6a
    .catchall {:try_start_26 .. :try_end_6a} :catchall_68

    throw v5

    .line 947
    .restart local v3       #fileSize:I
    :cond_6b
    :try_start_6b
    const-string v4, "Unknown"
    :try_end_6d
    .catchall {:try_start_6b .. :try_end_6d} :catchall_68

    goto :goto_5a
.end method

.method public getCurrentSlot()I
    .registers 6

    .prologue
    .line 956
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 957
    :try_start_3
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v1, v3}, Lcom/android/internal/telephony/Phone;->getDataProfile(Landroid/os/Message;)V

    .line 958
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 959
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    invoke-virtual {v1}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->getDataProfileReturnValue()I

    move-result v0

    .line 960
    .local v0, ret:I
    monitor-exit v2

    .line 961
    return v0

    .line 960
    .end local v0           #ret:I
    :catchall_1a
    move-exception v1

    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    throw v1
.end method

.method public getDSA_SERVER_IP()Ljava/lang/String;
    .registers 2

    .prologue
    .line 596
    const/16 v0, 0x195

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSA_SERVER_PORT()Ljava/lang/String;
    .registers 2

    .prologue
    .line 600
    const/16 v0, 0x196

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDSA_SERVER_URL()Ljava/lang/String;
    .registers 2

    .prologue
    .line 561
    const/16 v0, 0x192

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getDataProfile()I
    .registers 5

    .prologue
    const/4 v2, 0x1

    .line 982
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->getCurrentSlot()I

    move-result v0

    .line 983
    .local v0, slot:I
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v3

    invoke-virtual {v3}, Landroid/telephony/ServiceState;->getState()I
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_23

    move-result v1

    .line 985
    .local v1, ss:I
    if-nez v1, :cond_18

    .line 986
    if-nez v0, :cond_16

    .line 987
    const/16 v2, 0x64

    .line 995
    :cond_16
    :goto_16
    monitor-exit p0

    return v2

    .line 992
    :cond_18
    if-eq v1, v2, :cond_1d

    const/4 v2, 0x3

    if-ne v1, v2, :cond_20

    .line 993
    :cond_1d
    const/16 v2, 0x3e8

    goto :goto_16

    .line 995
    :cond_20
    const/16 v2, 0x3e9

    goto :goto_16

    .line 982
    .end local v0           #slot:I
    .end local v1           #ss:I
    :catchall_23
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getHTTPPD_PROXY_ADDR()Ljava/lang/String;
    .registers 2

    .prologue
    .line 613
    const/16 v0, 0x19e

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHTTPPD_PROXY_PORT()Ljava/lang/String;
    .registers 2

    .prologue
    .line 617
    const/16 v0, 0x19f

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMDN()Ljava/lang/String;
    .registers 8

    .prologue
    .line 368
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 369
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 370
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 372
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 374
    return-object v0

    .line 373
    .end local v0           #ret:Ljava/lang/String;
    :catchall_26
    move-exception v1

    monitor-exit v2
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    throw v1
.end method

.method public getMEID()Ljava/lang/String;
    .registers 8

    .prologue
    .line 390
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 391
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 392
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x1e

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 394
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 395
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 396
    return-object v0

    .line 395
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getMOBILESYNK_ENABLED()Ljava/lang/String;
    .registers 2

    .prologue
    .line 557
    const/16 v0, 0x12c

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMSID()Ljava/lang/String;
    .registers 8

    .prologue
    .line 379
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 380
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 381
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x23

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 383
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 384
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 385
    return-object v0

    .line 384
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getPDE_PRIMARY_IP()Ljava/lang/String;
    .registers 2

    .prologue
    .line 567
    const/4 v0, 0x0

    .line 578
    .local v0, ret:Ljava/lang/String;
    return-object v0
.end method

.method public getPDE_PRIMARY_PORT()Ljava/lang/String;
    .registers 3

    .prologue
    .line 582
    const/4 v0, 0x0

    .line 592
    .local v0, ret:I
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getPESUDO_ESN()Ljava/lang/String;
    .registers 8

    .prologue
    .line 401
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 402
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 403
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 405
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 407
    return-object v0

    .line 406
    .end local v0           #ret:Ljava/lang/String;
    :catchall_26
    move-exception v1

    monitor-exit v2
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    throw v1
.end method

.method public getRTSP_PROXY_ADDR()Ljava/lang/String;
    .registers 2

    .prologue
    .line 604
    const/16 v0, 0x19c

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRTSP_PROXY_PORT()Ljava/lang/String;
    .registers 2

    .prologue
    .line 609
    const/16 v0, 0x19d

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRoamingIndicator()I
    .registers 2

    .prologue
    .line 1419
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getCdmaRoamingIndicator()I

    move-result v0

    return v0
.end method

.method public getSLOT_1_AAA_AUTH_ALGORITHM()Ljava/lang/String;
    .registers 8

    .prologue
    .line 499
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 500
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 501
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x5d

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 504
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 505
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 506
    return-object v0

    .line 505
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_AAA_SPI()Ljava/lang/String;
    .registers 8

    .prologue
    .line 511
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 512
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 513
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x5e

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 515
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 516
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 517
    return-object v0

    .line 516
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_HA_AUTH_ALGORITHM()Ljava/lang/String;
    .registers 8

    .prologue
    .line 469
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 470
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 471
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x5a

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 474
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 475
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 476
    return-object v0

    .line 475
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_HA_PASSWORD()Ljava/lang/String;
    .registers 2

    .prologue
    .line 494
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSLOT_1_HA_SPI()Ljava/lang/String;
    .registers 8

    .prologue
    .line 481
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 482
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 483
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x5b

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 485
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 486
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 487
    return-object v0

    .line 486
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_MOBILE_IP_ADDRESS()Ljava/lang/String;
    .registers 8

    .prologue
    .line 534
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 535
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 536
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x57

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 539
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 540
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 541
    return-object v0

    .line 540
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_NAI()Ljava/lang/String;
    .registers 8

    .prologue
    .line 419
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 420
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 421
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 423
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 428
    .local v0, ret:Ljava/lang/String;
    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->fetchLatestNotNullValue(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    monitor-exit v2

    .line 431
    return-object v0

    .line 430
    .end local v0           #ret:Ljava/lang/String;
    :catchall_2b
    move-exception v1

    monitor-exit v2
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2b

    throw v1
.end method

.method public getSLOT_1_NAI_PASSWORD()Ljava/lang/String;
    .registers 8

    .prologue
    .line 436
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 437
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 438
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x2b

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 440
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 442
    return-object v0

    .line 441
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_PRIMARY_HA()Ljava/lang/String;
    .registers 8

    .prologue
    .line 447
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 448
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 449
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 451
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 452
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 453
    return-object v0

    .line 452
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_REVERSE_TUNNELING()Ljava/lang/String;
    .registers 8

    .prologue
    .line 522
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 523
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 524
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x56

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 527
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 528
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 529
    return-object v0

    .line 528
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSLOT_1_SECONDARY_HA()Ljava/lang/String;
    .registers 8

    .prologue
    .line 458
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 459
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 460
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0x33

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 462
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 463
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 464
    return-object v0

    .line 463
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getSetUpCompleted()Ljava/lang/String;
    .registers 2

    .prologue
    .line 363
    const/16 v0, 0x2710

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUAPROF()Ljava/lang/String;
    .registers 8

    .prologue
    .line 547
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v2

    .line 548
    :try_start_3
    iget v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 549
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v3, 0xe

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v5, 0x1

    iget v6, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Lcom/android/internal/telephony/Phone;->getSystemProperties(Ljava/lang/String;Landroid/os/Message;)V

    .line 551
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    move-result-object v0

    .line 552
    .local v0, ret:Ljava/lang/String;
    monitor-exit v2

    .line 553
    return-object v0

    .line 552
    .end local v0           #ret:Ljava/lang/String;
    :catchall_27
    move-exception v1

    monitor-exit v2
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_27

    throw v1
.end method

.method public getVoiceMailCount()I
    .registers 6

    .prologue
    .line 1328
    const-string v3, "PhoneInfoInterface"

    const-string v4, "getVoiceMailCount()"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    const/4 v1, 0x0

    .line 1331
    .local v1, ret:I
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1333
    .local v2, sp:Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1334
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v3, "vm_count_key_cdma"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1336
    return v1
.end method

.method public getWORK_MODE()Ljava/lang/String;
    .registers 2

    .prologue
    .line 622
    const/16 v0, 0x1f4

    invoke-direct {p0, v0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->GetValueInternalDB(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public reboot()V
    .registers 3

    .prologue
    .line 903
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 905
    const-string v0, "PhoneInfoInterface"

    const-string v1, "Rebooting !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    if-nez v0, :cond_1a

    .line 908
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    .line 911
    :cond_1a
    sget-object v0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->pm:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->rebootWithIntent()V

    .line 912
    return-void
.end method

.method public release()V
    .registers 3

    .prologue
    .line 1158
    const-string v0, "PhoneInfoInterface"

    const-string v1, "InterruptedException: release()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1159
    return-void
.end method

.method public releaseDSAProxy()V
    .registers 3

    .prologue
    .line 1182
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 1183
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->proxyBackup:Ljava/lang/String;

    if-eqz v0, :cond_e

    .line 1184
    const-string v0, "net.gprs.http-proxy"

    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->proxyBackup:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    :cond_e
    return-void
.end method

.method public declared-synchronized requestDataProfile(I)I
    .registers 11
    .parameter "dataconnection"

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1056
    monitor-enter p0

    :try_start_3
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 1059
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    iput-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mConnMgr:Landroid/net/ConnectivityManager;

    .line 1060
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mConnMgr:Landroid/net/ConnectivityManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1061
    .local v2, netInfo:Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    .line 1062
    .local v0, connectedOrConnecting:Z
    const/4 v3, 0x2

    if-eq p1, v3, :cond_86

    .line 1063
    if-ne v0, v7, :cond_2e

    .line 1064
    const-string v3, "PhoneInfoInterface"

    const-string v4, "requestDataProfile connectedOrConnecting: true"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->disableDataConnectivity()Z

    .line 1067
    :cond_2e
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->useOamdm:Z

    .line 1072
    :goto_31
    const-string v3, "PhoneInfoInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestDataProfile setDataProfile: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1074
    if-ne v7, p1, :cond_92

    .line 1075
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/android/internal/telephony/Phone;->setDataProfile(ILandroid/os/Message;)V

    .line 1079
    :goto_5c
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 1097
    iget-boolean v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->useOamdm:Z

    if-nez v3, :cond_6f

    .line 1098
    const-string v3, "PhoneInfoInterface"

    const-string v4, "requestDataProfile mPhone.enableDataConnectivity() "

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->enableDataConnectivity()Z

    .line 1104
    :cond_6f
    if-ne v0, v7, :cond_73

    if-eqz p1, :cond_77

    :cond_73
    iget-boolean v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->useOamdm:Z
    :try_end_75
    .catchall {:try_start_3 .. :try_end_75} :catchall_8f

    if-ne v3, v7, :cond_7c

    .line 1106
    :cond_77
    const-wide/16 v3, 0xfa0

    :try_start_79
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_7c
    .catchall {:try_start_79 .. :try_end_7c} :catchall_8f
    .catch Ljava/lang/InterruptedException; {:try_start_79 .. :try_end_7c} :catch_a0

    .line 1112
    :cond_7c
    :goto_7c
    if-nez p1, :cond_81

    .line 1113
    :try_start_7e
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->isNetworkAvailable()Z

    .line 1116
    :cond_81
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->useOamdm:Z
    :try_end_84
    .catchall {:try_start_7e .. :try_end_84} :catchall_8f

    .line 1118
    monitor-exit p0

    return v8

    .line 1069
    :cond_86
    :try_start_86
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    invoke-interface {v3}, Lcom/android/internal/telephony/Phone;->disableDataConnectivity()Z

    .line 1070
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->useOamdm:Z
    :try_end_8e
    .catchall {:try_start_86 .. :try_end_8e} :catchall_8f

    goto :goto_31

    .line 1056
    .end local v0           #connectedOrConnecting:Z
    .end local v2           #netInfo:Landroid/net/NetworkInfo;
    :catchall_8f
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1077
    .restart local v0       #connectedOrConnecting:Z
    .restart local v2       #netInfo:Landroid/net/NetworkInfo;
    :cond_92
    :try_start_92
    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/android/internal/telephony/Phone;->setDataProfile(ILandroid/os/Message;)V

    goto :goto_5c

    .line 1107
    :catch_a0
    move-exception v1

    .line 1108
    .local v1, e:Ljava/lang/InterruptedException;
    const-string v3, "PhoneInfoInterface"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DISCONNECT_WAIT_TIME error"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b9
    .catchall {:try_start_92 .. :try_end_b9} :catchall_8f

    goto :goto_7c
.end method

.method public resetVoiceMailCount()V
    .registers 5

    .prologue
    .line 1341
    const-string v2, "PhoneInfoInterface"

    const-string v3, "resetVoiceMailCount()"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1343
    iget-object v2, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1345
    .local v1, sp:Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1346
    .local v0, editor:Landroid/content/SharedPreferences$Editor;
    const-string v2, "vm_count_key_cdma"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1347
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1348
    return-void
.end method

.method public setA_Key(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 659
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 660
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 661
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 662
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x190

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 664
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 665
    monitor-exit v1

    .line 666
    return-void

    .line 665
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setDSAProxy(Ljava/lang/String;I)V
    .registers 9
    .parameter "proxy"
    .parameter "port"

    .prologue
    .line 1162
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 1163
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_12

    .line 1164
    :cond_b
    const-string v1, "net.gprs.http-proxy"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1179
    :goto_11
    return-void

    .line 1169
    :cond_12
    if-ltz p2, :cond_18

    const/16 v1, 0x2710

    if-lt p2, v1, :cond_1a

    .line 1170
    :cond_18
    const/16 p2, 0x1f90

    .line 1172
    :cond_1a
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%04d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1173
    .local v0, htttpProxy:Ljava/lang/String;
    const-string v1, "net.gprs.http-proxy"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->proxyBackup:Ljava/lang/String;

    .line 1174
    const-string v1, "net.gprs.http-proxy"

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1176
    const-string v1, "PhoneInfoInterface"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDSAProxy :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "net.gprs.http-proxy"

    invoke-static {v3, v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11
.end method

.method public setDSA_SERVER_IP(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 825
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 826
    const/16 v0, 0x195

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 827
    return-void
.end method

.method public setDSA_SERVER_PORT(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 830
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 831
    const/16 v0, 0x196

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 832
    return-void
.end method

.method public setDSA_SERVER_URL(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 791
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 792
    const/16 v0, 0x192

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 793
    return-void
.end method

.method public setHTTPPD_PROXY_ADDR(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 847
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 848
    const-string v0, "net.cdma.httppd.proxy.addr"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const/16 v0, 0x19e

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 850
    return-void
.end method

.method public setHTTPPD_PROXY_PORT(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 853
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 854
    const-string v0, "net.cdma.httppd.proxy.port"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const/16 v0, 0x19f

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 856
    return-void
.end method

.method public setMDN(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 627
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 628
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 629
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 630
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 632
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 633
    monitor-exit v1

    .line 634
    return-void

    .line 633
    :catchall_28
    move-exception v0

    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_6 .. :try_end_2a} :catchall_28

    throw v0
.end method

.method public setMEID(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    .prologue
    .line 650
    return-void
.end method

.method public setMOBILESYNK_ENABLED(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 786
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 787
    const/16 v0, 0x12c

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 788
    return-void
.end method

.method public setMSID(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 637
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 638
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 639
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 640
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 642
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 643
    monitor-exit v1

    .line 644
    return-void

    .line 643
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setNetworkPreference(I)V
    .registers 5
    .parameter "type"

    .prologue
    .line 967
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mContext:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 969
    .local v0, mConnMgr:Landroid/net/ConnectivityManager;
    invoke-virtual {v0, p1}, Landroid/net/ConnectivityManager;->setNetworkPreference(I)V

    .line 970
    return-void
.end method

.method public setPDE_PRIMARY_IP(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    .prologue
    .line 808
    return-void
.end method

.method public setPDE_PRIMARY_PORT(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    .prologue
    .line 822
    return-void
.end method

.method public setPESUDO_ESN(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    .prologue
    .line 656
    return-void
.end method

.method public setRTSP_PROXY_ADDR(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 835
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 836
    const-string v0, "net.cdma.rtsp.proxy.addr"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    const/16 v0, 0x19c

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 838
    return-void
.end method

.method public setRTSP_PROXY_PORT(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 841
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 842
    const-string v0, "net.cdma.rtsp.proxy.port"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    const/16 v0, 0x19d

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 844
    return-void
.end method

.method public setSLOT_1_AAA_AUTH_ALGORITHM(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 740
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 741
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 742
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 743
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x5d

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 746
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 747
    monitor-exit v1

    .line 748
    return-void

    .line 747
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_AAA_SPI(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 751
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 752
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 753
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 754
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x5e

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 756
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 757
    monitor-exit v1

    .line 758
    return-void

    .line 757
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_HA_AUTH_ALGORITHM(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 709
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 710
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 711
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 712
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x5a

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 715
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 716
    monitor-exit v1

    .line 717
    return-void

    .line 716
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_HA_PASSWORD(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 731
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 732
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 733
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x5c

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 735
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 736
    monitor-exit v1

    .line 737
    return-void

    .line 736
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_HA_SPI(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 720
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 721
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 722
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 723
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x5b

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 725
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 726
    monitor-exit v1

    .line 727
    return-void

    .line 726
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_MOBILE_IP_ADDRESS(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 772
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 773
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 774
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 775
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x57

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 778
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 779
    monitor-exit v1

    .line 780
    return-void

    .line 779
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_NAI(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 669
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 670
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 671
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 672
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 674
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 675
    monitor-exit v1

    .line 676
    return-void

    .line 675
    :catchall_28
    move-exception v0

    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_6 .. :try_end_2a} :catchall_28

    throw v0
.end method

.method public setSLOT_1_NAI_PASSWORD(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 679
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 680
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 681
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 682
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 684
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 685
    monitor-exit v1

    .line 686
    return-void

    .line 685
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_PRIMARY_HA(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 689
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 690
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 691
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 692
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 694
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 695
    monitor-exit v1

    .line 696
    return-void

    .line 695
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_REVERSE_TUNNELING(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 761
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 762
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 763
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 764
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x56

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 767
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 768
    monitor-exit v1

    .line 769
    return-void

    .line 768
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSLOT_1_SECONDARY_HA(Ljava/lang/String;)V
    .registers 8
    .parameter "value"

    .prologue
    .line 699
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 700
    iget-object v1, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhoneLock:Ljava/lang/Object;

    monitor-enter v1

    .line 701
    :try_start_6
    iget v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    .line 702
    iget-object v0, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPhone:Lcom/android/internal/telephony/Phone;

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->mPIM:Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;

    const/4 v4, 0x2

    iget v5, p0, Lcom/samsungframeworks/internal/PhoneInfoInterface;->curCmd:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsungframeworks/internal/PhoneInfoInterface$PhoneInfoManager;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lcom/android/internal/telephony/Phone;->setSystemProperties(Ljava/lang/String;Ljava/lang/String;Landroid/os/Message;)V

    .line 704
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->stopAndWaitReturn()Ljava/lang/String;

    .line 705
    monitor-exit v1

    .line 706
    return-void

    .line 705
    :catchall_29
    move-exception v0

    monitor-exit v1
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_29

    throw v0
.end method

.method public setSetUpCompleted(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 358
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 359
    const/16 v0, 0x2710

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 360
    return-void
.end method

.method public setUAPROF(Ljava/lang/String;)V
    .registers 2
    .parameter "value"

    .prologue
    .line 783
    return-void
.end method

.method public setWORK_MODE(Ljava/lang/String;)V
    .registers 3
    .parameter "value"

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->enforceUID()V

    .line 861
    const/16 v0, 0x1f4

    invoke-direct {p0, v0, p1}, Lcom/samsungframeworks/internal/PhoneInfoInterface;->SetValueInternalDB(ILjava/lang/String;)Ljava/lang/String;

    .line 862
    return-void
.end method
