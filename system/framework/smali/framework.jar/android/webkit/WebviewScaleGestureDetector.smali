.class public Landroid/webkit/WebviewScaleGestureDetector;
.super Ljava/lang/Object;
.source "WebviewScaleGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/webkit/WebviewScaleGestureDetector$SimpleOnScaleGestureListener;,
        Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "WebviewScaleGestureDetector"


# instance fields
.field private mActive0MostRecent:Z

.field private mActiveId0:I

.field private mActiveId1:I

.field private mCalledInBrowserTab:Z

.field private final mContext:Landroid/content/Context;

.field private mCurrDockZoom:F

.field private mCurrSpan:F

.field private mCurrSpanX:F

.field private mCurrSpanY:F

.field mCurrTilt:I

.field private mCurrTime:J

.field private mDockZoomUse:Z

.field private mFocusX:F

.field private mFocusY:F

.field private mInProgress:Z

.field private mInitialSpan:F

.field private final mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

.field private final mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

.field private mMotionUse:Z

.field mOnScaleBeginTime:J

.field private mPrevSpan:F

.field private mPrevSpanX:F

.field private mPrevSpanY:F

.field private mPrevTime:J

.field private mPreventMoveEvent:Z

.field private mSpanSlop:I

.field mTiltBeginTime:J

.field private mTiltIntended:Z

.field private mTiltUse:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;)V
    .registers 7
    .parameter "context"
    .parameter "listener"

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 222
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-boolean v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPreventMoveEvent:Z

    .line 87
    iput v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrTilt:I

    .line 88
    iput-wide v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mOnScaleBeginTime:J

    .line 89
    iput-wide v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltBeginTime:J

    .line 91
    iput-boolean v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltUse:Z

    .line 92
    iput-boolean v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltIntended:Z

    .line 93
    iput-boolean v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCalledInBrowserTab:Z

    .line 96
    iput-boolean v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mDockZoomUse:Z

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrDockZoom:F

    .line 218
    invoke-static {}, Landroid/view/InputEventConsistencyVerifier;->isInstrumentationEnabled()Z

    move-result v0

    if-eqz v0, :cond_37

    new-instance v0, Landroid/view/InputEventConsistencyVerifier;

    invoke-direct {v0, p0, v1}, Landroid/view/InputEventConsistencyVerifier;-><init>(Ljava/lang/Object;I)V

    :goto_24
    iput-object v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    .line 223
    iput-object p1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mContext:Landroid/content/Context;

    .line 224
    iput-object p2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    .line 225
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mSpanSlop:I

    .line 226
    return-void

    .line 218
    :cond_37
    const/4 v0, 0x0

    goto :goto_24
.end method


# virtual methods
.method public SetReadyToFinishZoom(Z)V
    .registers 5
    .parameter "bIsFinishZoom"

    .prologue
    .line 204
    sget-boolean v0, Landroid/webkit/DebugFlags;->WEB_TOUCH_LOG:Z

    if-eqz v0, :cond_1c

    .line 205
    const-string v0, "WebviewScaleGestureDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SetReadyToFinishZoom = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_1c
    iput-boolean p1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPreventMoveEvent:Z

    .line 208
    return-void
.end method

.method public getCurrDockZoom()F
    .registers 2

    .prologue
    .line 100
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrDockZoom:F

    return v0
.end method

.method public getCurrTilt()I
    .registers 2

    .prologue
    .line 107
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrTilt:I

    return v0
.end method

.method public getCurrentSpan()F
    .registers 2

    .prologue
    .line 374
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpan:F

    return v0
.end method

.method public getCurrentSpanX()F
    .registers 2

    .prologue
    .line 384
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanX:F

    return v0
.end method

.method public getCurrentSpanY()F
    .registers 2

    .prologue
    .line 394
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanY:F

    return v0
.end method

.method public getEventTime()J
    .registers 3

    .prologue
    .line 454
    iget-wide v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrTime:J

    return-wide v0
.end method

.method public getFocusX()F
    .registers 2

    .prologue
    .line 350
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mFocusX:F

    return v0
.end method

.method public getFocusY()F
    .registers 2

    .prologue
    .line 364
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mFocusY:F

    return v0
.end method

.method public getMotionUse()Z
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149
    iget-object v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "motion_engine"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_25

    iget-object v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "motion_zooming"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_25

    :goto_20
    iput-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mMotionUse:Z

    .line 151
    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mMotionUse:Z

    return v0

    :cond_25
    move v0, v1

    .line 149
    goto :goto_20
.end method

.method public getPreviousSpan()F
    .registers 2

    .prologue
    .line 404
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpan:F

    return v0
.end method

.method public getPreviousSpanX()F
    .registers 2

    .prologue
    .line 414
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanX:F

    return v0
.end method

.method public getPreviousSpanY()F
    .registers 2

    .prologue
    .line 424
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanY:F

    return v0
.end method

.method public getScaleFactor()F
    .registers 3

    .prologue
    .line 435
    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpan:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    iget v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpan:F

    iget v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpan:F

    div-float/2addr v0, v1

    :goto_c
    return v0

    :cond_d
    const/high16 v0, 0x3f80

    goto :goto_c
.end method

.method public getTiltUse()Z
    .registers 2

    .prologue
    .line 188
    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltUse:Z

    return v0
.end method

.method public getTimeDelta()J
    .registers 5

    .prologue
    .line 445
    iget-wide v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrTime:J

    iget-wide v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPrevTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public initCurrDockZoom()V
    .registers 2

    .prologue
    .line 104
    const/4 v0, 0x0

    iput v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrDockZoom:F

    .line 105
    return-void
.end method

.method public isInProgress()Z
    .registers 2

    .prologue
    .line 336
    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltUse:Z

    if-eqz v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public isReadyToFinishZoom()Z
    .registers 4

    .prologue
    .line 197
    sget-boolean v0, Landroid/webkit/DebugFlags;->WEB_TOUCH_LOG:Z

    if-eqz v0, :cond_1f

    .line 198
    const-string v0, "WebviewScaleGestureDetector"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "isReadyToFinishZoom = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPreventMoveEvent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_1f
    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mPreventMoveEvent:Z

    return v0
.end method

.method public onDockZoomEvent(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter "motionEvent"

    .prologue
    .line 137
    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 139
    .local v0, vscroll:F
    const/high16 v1, 0x3f80

    const/high16 v2, 0x4040

    div-float v2, v0, v2

    add-float/2addr v1, v2

    iput v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrDockZoom:F

    .line 140
    iget v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrDockZoom:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1b

    .line 141
    iget-object v1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    invoke-interface {v1, p0}, Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;->onScale(Landroid/webkit/WebviewScaleGestureDetector;)Z

    .line 142
    :cond_1b
    return-void
.end method

.method public onMREvent(Landroid/hardware/motion/MREvent;)V
    .registers 6
    .parameter "motionEvent"

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v0, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/String;

    const-string v3, "com.android.browser"

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 124
    .local v1, pkgName:Ljava/lang/String;
    invoke-virtual {p0}, Landroid/webkit/WebviewScaleGestureDetector;->getMotionUse()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    .line 134
    :cond_21
    :goto_21
    return-void

    .line 127
    :cond_22
    invoke-virtual {p1}, Landroid/hardware/motion/MREvent;->getMotion()I

    move-result v2

    const/16 v3, 0x43

    if-ne v2, v3, :cond_21

    .line 128
    invoke-virtual {p1}, Landroid/hardware/motion/MREvent;->getTilt()I

    move-result v2

    iput v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrTilt:I

    .line 129
    invoke-virtual {p0}, Landroid/webkit/WebviewScaleGestureDetector;->tiltUseDecision()V

    .line 130
    iget-boolean v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltUse:Z

    if-eqz v2, :cond_21

    .line 131
    iget-object v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    invoke-interface {v2, p0}, Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;->onScale(Landroid/webkit/WebviewScaleGestureDetector;)Z

    goto :goto_21
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 28
    .parameter "event"

    .prologue
    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    move-object/from16 v24, v0

    if-eqz v24, :cond_19

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInputEventConsistencyVerifier:Landroid/view/InputEventConsistencyVerifier;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Landroid/view/InputEventConsistencyVerifier;->onTouchEvent(Landroid/view/MotionEvent;I)V

    .line 233
    :cond_19
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    .line 235
    .local v3, action:I
    const/16 v24, 0x1

    move/from16 v0, v24

    if-eq v3, v0, :cond_29

    const/16 v24, 0x3

    move/from16 v0, v24

    if-ne v3, v0, :cond_59

    :cond_29
    const/16 v19, 0x1

    .line 237
    .local v19, streamComplete:Z
    :goto_2b
    if-eqz v3, :cond_2f

    if-eqz v19, :cond_5c

    .line 241
    :cond_2f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    move/from16 v24, v0

    if-eqz v24, :cond_54

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Landroid/webkit/WebviewScaleGestureDetector;)V

    .line 243
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    .line 244
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mInitialSpan:F

    .line 247
    :cond_54
    if-eqz v19, :cond_5c

    .line 248
    const/16 v24, 0x1

    .line 329
    :goto_58
    return v24

    .line 235
    .end local v19           #streamComplete:Z
    :cond_59
    const/16 v19, 0x0

    goto :goto_2b

    .line 252
    .restart local v19       #streamComplete:Z
    :cond_5c
    const/16 v24, 0x6

    move/from16 v0, v24

    if-eq v3, v0, :cond_68

    const/16 v24, 0x5

    move/from16 v0, v24

    if-ne v3, v0, :cond_86

    :cond_68
    const/4 v4, 0x1

    .line 255
    .local v4, configChanged:Z
    :goto_69
    const/16 v24, 0x6

    move/from16 v0, v24

    if-ne v3, v0, :cond_88

    const/4 v14, 0x1

    .line 256
    .local v14, pointerUp:Z
    :goto_70
    if-eqz v14, :cond_8a

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v15

    .line 259
    .local v15, skipIndex:I
    :goto_76
    const/16 v20, 0x0

    .local v20, sumX:F
    const/16 v21, 0x0

    .line 260
    .local v21, sumY:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    .line 261
    .local v5, count:I
    const/4 v13, 0x0

    .local v13, i:I
    :goto_7f
    if-ge v13, v5, :cond_9d

    .line 262
    if-ne v15, v13, :cond_8c

    .line 261
    :goto_83
    add-int/lit8 v13, v13, 0x1

    goto :goto_7f

    .line 252
    .end local v4           #configChanged:Z
    .end local v5           #count:I
    .end local v13           #i:I
    .end local v14           #pointerUp:Z
    .end local v15           #skipIndex:I
    .end local v20           #sumX:F
    .end local v21           #sumY:F
    :cond_86
    const/4 v4, 0x0

    goto :goto_69

    .line 255
    .restart local v4       #configChanged:Z
    :cond_88
    const/4 v14, 0x0

    goto :goto_70

    .line 256
    .restart local v14       #pointerUp:Z
    :cond_8a
    const/4 v15, -0x1

    goto :goto_76

    .line 263
    .restart local v5       #count:I
    .restart local v13       #i:I
    .restart local v15       #skipIndex:I
    .restart local v20       #sumX:F
    .restart local v21       #sumY:F
    :cond_8c
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getX(I)F

    move-result v24

    add-float v20, v20, v24

    .line 264
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getY(I)F

    move-result v24

    add-float v21, v21, v24

    goto :goto_83

    .line 266
    :cond_9d
    if-eqz v14, :cond_b5

    add-int/lit8 v10, v5, -0x1

    .line 267
    .local v10, div:I
    :goto_a1
    int-to-float v0, v10

    move/from16 v24, v0

    div-float v11, v20, v24

    .line 268
    .local v11, focusX:F
    int-to-float v0, v10

    move/from16 v24, v0

    div-float v12, v21, v24

    .line 271
    .local v12, focusY:F
    const/4 v6, 0x0

    .local v6, devSumX:F
    const/4 v7, 0x0

    .line 272
    .local v7, devSumY:F
    const/4 v13, 0x0

    :goto_ae
    if-ge v13, v5, :cond_d4

    .line 273
    if-ne v15, v13, :cond_b7

    .line 272
    :goto_b2
    add-int/lit8 v13, v13, 0x1

    goto :goto_ae

    .end local v6           #devSumX:F
    .end local v7           #devSumY:F
    .end local v10           #div:I
    .end local v11           #focusX:F
    .end local v12           #focusY:F
    :cond_b5
    move v10, v5

    .line 266
    goto :goto_a1

    .line 274
    .restart local v6       #devSumX:F
    .restart local v7       #devSumY:F
    .restart local v10       #div:I
    .restart local v11       #focusX:F
    .restart local v12       #focusY:F
    :cond_b7
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getX(I)F

    move-result v24

    sub-float v24, v24, v11

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    move-result v24

    add-float v6, v6, v24

    .line 275
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getY(I)F

    move-result v24

    sub-float v24, v24, v12

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    move-result v24

    add-float v7, v7, v24

    goto :goto_b2

    .line 277
    :cond_d4
    int-to-float v0, v10

    move/from16 v24, v0

    div-float v8, v6, v24

    .line 278
    .local v8, devX:F
    int-to-float v0, v10

    move/from16 v24, v0

    div-float v9, v7, v24

    .line 283
    .local v9, devY:F
    const/high16 v24, 0x4000

    mul-float v17, v8, v24

    .line 284
    .local v17, spanX:F
    const/high16 v24, 0x4000

    mul-float v18, v9, v24

    .line 285
    .local v18, spanY:F
    mul-float v24, v17, v17

    mul-float v25, v18, v18

    add-float v24, v24, v25

    invoke-static/range {v24 .. v24}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v16

    .line 290
    .local v16, span:F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    move/from16 v23, v0

    .line 291
    .local v23, wasInProgress:Z
    move-object/from16 v0, p0

    iput v11, v0, Landroid/webkit/WebviewScaleGestureDetector;->mFocusX:F

    .line 292
    move-object/from16 v0, p0

    iput v12, v0, Landroid/webkit/WebviewScaleGestureDetector;->mFocusY:F

    .line 293
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    move/from16 v24, v0

    if-eqz v24, :cond_129

    const/16 v24, 0x0

    cmpl-float v24, v16, v24

    if-eqz v24, :cond_10e

    if-eqz v4, :cond_129

    .line 294
    :cond_10e
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;->onScaleEnd(Landroid/webkit/WebviewScaleGestureDetector;)V

    .line 295
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    .line 296
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mInitialSpan:F

    .line 298
    :cond_129
    if-eqz v4, :cond_155

    .line 299
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanX:F

    .line 300
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanY:F

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanY:F

    .line 301
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpan:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpan:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mInitialSpan:F

    .line 303
    :cond_155
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    move/from16 v24, v0

    if-nez v24, :cond_1b8

    const/16 v24, 0x0

    cmpl-float v24, v16, v24

    if-eqz v24, :cond_1b8

    if-nez v23, :cond_180

    move-object/from16 v0, p0

    iget v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInitialSpan:F

    move/from16 v24, v0

    sub-float v24, v16, v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->abs(F)F

    move-result v24

    move-object/from16 v0, p0

    iget v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mSpanSlop:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    cmpl-float v24, v24, v25

    if-lez v24, :cond_1b8

    .line 305
    :cond_180
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanX:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanX:F

    .line 306
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanY:F

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanY:F

    .line 307
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpan:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpan:F

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;->onScaleBegin(Landroid/webkit/WebviewScaleGestureDetector;)Z

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    .line 312
    :cond_1b8
    const/16 v24, 0x2

    move/from16 v0, v24

    if-ne v3, v0, :cond_20e

    .line 313
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanX:F

    .line 314
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanY:F

    .line 315
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpan:F

    .line 317
    const/16 v22, 0x1

    .line 318
    .local v22, updatePrev:Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mInProgress:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1e8

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mListener:Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/webkit/WebviewScaleGestureDetector$OnScaleGestureListener;->onScale(Landroid/webkit/WebviewScaleGestureDetector;)Z

    move-result v22

    .line 322
    :cond_1e8
    if-eqz v22, :cond_20e

    .line 323
    move-object/from16 v0, p0

    iget v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanX:F

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanX:F

    .line 324
    move-object/from16 v0, p0

    iget v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpanY:F

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpanY:F

    .line 325
    move-object/from16 v0, p0

    iget v0, v0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrSpan:F

    move/from16 v24, v0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Landroid/webkit/WebviewScaleGestureDetector;->mPrevSpan:F

    .line 329
    .end local v22           #updatePrev:Z
    :cond_20e
    const/16 v24, 0x1

    goto/16 :goto_58
.end method

.method public setCurrTilt(I)V
    .registers 2
    .parameter "newTilt"

    .prologue
    .line 111
    iput p1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCurrTilt:I

    .line 112
    return-void
.end method

.method public setNativeBrowser(Z)V
    .registers 2
    .parameter "mCalledInBrowserTab"

    .prologue
    .line 145
    iput-boolean p1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mCalledInBrowserTab:Z

    .line 146
    return-void
.end method

.method public setTiltStartTime(J)V
    .registers 3
    .parameter "start"

    .prologue
    .line 115
    iput-wide p1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mOnScaleBeginTime:J

    .line 116
    return-void
.end method

.method public setTiltUse(Z)V
    .registers 3
    .parameter "TiltUse"

    .prologue
    const/4 v0, 0x0

    .line 182
    invoke-virtual {p0, v0}, Landroid/webkit/WebviewScaleGestureDetector;->setCurrTilt(I)V

    .line 183
    iput-boolean p1, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltUse:Z

    .line 184
    iput-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltIntended:Z

    .line 185
    return-void
.end method

.method public tiltUseDecision()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    .line 155
    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltUse:Z

    if-nez v0, :cond_36

    .line 156
    invoke-virtual {p0}, Landroid/webkit/WebviewScaleGestureDetector;->getScaleFactor()F

    move-result v0

    const/high16 v1, 0x3f80

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f7cac083126e979L

    cmpg-double v0, v0, v2

    if-gez v0, :cond_37

    .line 157
    iget-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltIntended:Z

    if-nez v0, :cond_26

    .line 158
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltBeginTime:J

    .line 159
    iput-boolean v6, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltIntended:Z

    .line 167
    :cond_26
    :goto_26
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltBeginTime:J

    const-wide/16 v4, 0x1e

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_36

    .line 168
    invoke-virtual {p0, v6}, Landroid/webkit/WebviewScaleGestureDetector;->setTiltUse(Z)V

    .line 179
    :cond_36
    return-void

    .line 163
    :cond_37
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltBeginTime:J

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/webkit/WebviewScaleGestureDetector;->mTiltIntended:Z

    goto :goto_26
.end method
