.class public Lcom/google/android/street/GoogleLocationSettingHelper;
.super Ljava/lang/Object;
.source "GoogleLocationSettingHelper.java"


# static fields
.field private static final GOOGLE_SETTINGS_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 46
    const-string v0, "content://com.google.settings/partner"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/street/GoogleLocationSettingHelper;->GOOGLE_SETTINGS_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getUseLocationForServices(Landroid/content/Context;)I
    .registers 13
    .parameter "context"

    .prologue
    .line 67
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 68
    .local v0, resolver:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 69
    .local v6, c:Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 71
    .local v9, stringValue:Ljava/lang/String;
    :try_start_6
    sget-object v1, Lcom/google/android/street/GoogleLocationSettingHelper;->GOOGLE_SETTINGS_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v11, "use_location_for_services"

    aput-object v11, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 73
    if-eqz v6, :cond_2c

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 74
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2b
    .catchall {:try_start_6 .. :try_end_2b} :catchall_44
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_2b} :catch_35

    move-result-object v9

    .line 79
    :cond_2c
    if-eqz v6, :cond_31

    .line 80
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 83
    :cond_31
    :goto_31
    if-nez v9, :cond_4b

    .line 84
    const/4 v1, 0x2

    .line 92
    :goto_34
    return v1

    .line 76
    :catch_35
    move-exception v1

    move-object v7, v1

    .line 77
    .local v7, e:Ljava/lang/RuntimeException;
    :try_start_37
    const-string v1, "GoogleLocationSettingHelper"

    const-string v2, "Failed to get \'Use My Location\' setting"

    invoke-static {v1, v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3e
    .catchall {:try_start_37 .. :try_end_3e} :catchall_44

    .line 79
    if-eqz v6, :cond_31

    .line 80
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_31

    .line 79
    .end local v7           #e:Ljava/lang/RuntimeException;
    :catchall_44
    move-exception v1

    if-eqz v6, :cond_4a

    .line 80
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4a
    throw v1

    .line 88
    :cond_4b
    :try_start_4b
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4e
    .catch Ljava/lang/NumberFormatException; {:try_start_4b .. :try_end_4e} :catch_51

    move-result v10

    .local v10, value:I
    :goto_4f
    move v1, v10

    .line 92
    goto :goto_34

    .line 89
    .end local v10           #value:I
    :catch_51
    move-exception v8

    .line 90
    .local v8, nfe:Ljava/lang/NumberFormatException;
    const/4 v10, 0x2

    .restart local v10       #value:I
    goto :goto_4f
.end method

.method public static isEnforceable(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    .prologue
    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.gsf.GOOGLE_LOCATION_SETTINGS"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 57
    .local v0, ri:Landroid/content/pm/ResolveInfo;
    if-eqz v0, :cond_15

    const/4 v1, 0x1

    :goto_14
    return v1

    :cond_15
    const/4 v1, 0x0

    goto :goto_14
.end method
