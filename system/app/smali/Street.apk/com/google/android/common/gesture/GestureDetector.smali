.class public Lcom/google/android/common/gesture/GestureDetector;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/common/gesture/GestureDetector$GestureHandler;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final LONGPRESS_TIMEOUT:I

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

.field private mDoubleTapSlopSquare:I

.field private mDoubleTapTouchSlopSquare:I

.field private mDownFocusX:F

.field private mDownFocusY:F

.field private final mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private mIsDoubleTapping:Z

.field private mIsLongpressEnabled:Z

.field private mLastFocusX:F

.field private mLastFocusY:F

.field private final mListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mMaximumFlingVelocity:I

.field private mMinimumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 60
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/google/android/common/gesture/GestureDetector;->LONGPRESS_TIMEOUT:I

    .line 61
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/common/gesture/GestureDetector;->TAP_TIMEOUT:I

    .line 62
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/common/gesture/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V
    .registers 4
    .parameter "context"
    .parameter "listener"

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/common/gesture/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    .line 194
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .registers 5
    .parameter "context"
    .parameter "listener"
    .parameter "handler"

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    if-eqz p3, :cond_1b

    .line 211
    new-instance v0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;

    invoke-direct {v0, p0, p3}, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;-><init>(Lcom/google/android/common/gesture/GestureDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    .line 215
    :goto_c
    iput-object p2, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 216
    instance-of v0, p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_17

    .line 217
    check-cast p2, Landroid/view/GestureDetector$OnDoubleTapListener;

    .end local p2
    invoke-virtual {p0, p2}, Lcom/google/android/common/gesture/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    .line 219
    :cond_17
    invoke-direct {p0, p1}, Lcom/google/android/common/gesture/GestureDetector;->init(Landroid/content/Context;)V

    .line 220
    return-void

    .line 213
    .restart local p2
    :cond_1b
    new-instance v0, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;

    invoke-direct {v0, p0}, Lcom/google/android/common/gesture/GestureDetector$GestureHandler;-><init>(Lcom/google/android/common/gesture/GestureDetector;)V

    iput-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    goto :goto_c
.end method

.method static synthetic access$000(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/MotionEvent;
    .registers 2
    .parameter "x0"

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnGestureListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/common/gesture/GestureDetector;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/common/gesture/GestureDetector;->dispatchLongPress()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/common/gesture/GestureDetector;)Landroid/view/GestureDetector$OnDoubleTapListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/common/gesture/GestureDetector;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    return v0
.end method

.method private cancel()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 516
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 517
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 518
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 519
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 520
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 521
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    .line 522
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    .line 523
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    .line 524
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 525
    iget-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_29

    .line 526
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    .line 528
    :cond_29
    return-void
.end method

.method private cancelTaps()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 531
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 532
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 533
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 534
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    .line 535
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    .line 536
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 537
    iget-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_1f

    .line 538
    iput-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    .line 540
    :cond_1f
    return-void
.end method

.method private dispatchLongPress()V
    .registers 3

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 559
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    .line 560
    iget-object v0, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 561
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    .prologue
    .line 240
    iget-object v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    if-nez v4, :cond_c

    .line 241
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "OnGestureListener must not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 243
    :cond_c
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsLongpressEnabled:Z

    .line 247
    if-nez p1, :cond_31

    .line 249
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v3

    .line 250
    .local v3, touchSlop:I
    move v2, v3

    .line 257
    .local v2, doubleTapTouchSlop:I
    mul-int/lit8 v1, v3, 0x2

    .line 259
    .local v1, doubleTapSlop:I
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    .line 260
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    .line 275
    :goto_24
    mul-int v4, v3, v3

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mTouchSlopSquare:I

    .line 276
    mul-int v4, v2, v2

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapTouchSlopSquare:I

    .line 277
    mul-int v4, v1, v1

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapSlopSquare:I

    .line 278
    return-void

    .line 262
    .end local v1           #doubleTapSlop:I
    .end local v2           #doubleTapTouchSlop:I
    .end local v3           #touchSlop:I
    :cond_31
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 263
    .local v0, configuration:Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    .line 270
    .restart local v3       #touchSlop:I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    .line 271
    .restart local v2       #doubleTapTouchSlop:I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 272
    .restart local v1       #doubleTapSlop:I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    .line 273
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    goto :goto_24
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .registers 11
    .parameter "firstDown"
    .parameter "firstUp"
    .parameter "secondDown"

    .prologue
    const/4 v6, 0x0

    .line 544
    iget-boolean v2, p0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-nez v2, :cond_7

    move v2, v6

    .line 554
    :goto_6
    return v2

    .line 548
    :cond_7
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    sget v4, Lcom/google/android/common/gesture/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_19

    move v2, v6

    .line 549
    goto :goto_6

    .line 552
    :cond_19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    sub-int v0, v2, v3

    .line 553
    .local v0, deltaX:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    sub-int v1, v2, v3

    .line 554
    .local v1, deltaY:I
    mul-int v2, v0, v0

    mul-int v3, v1, v1

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapSlopSquare:I

    if-ge v2, v3, :cond_3c

    const/4 v2, 0x1

    goto :goto_6

    :cond_3c
    move v2, v6

    goto :goto_6
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 43
    .parameter "ev"

    .prologue
    .line 329
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    .line 331
    .local v5, action:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    if-nez v35, :cond_16

    .line 332
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 334
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 336
    move v0, v5

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    const/16 v36, 0x6

    move/from16 v0, v35

    move/from16 v1, v36

    if-ne v0, v1, :cond_54

    const/16 v35, 0x1

    move/from16 v21, v35

    .line 338
    .local v21, pointerUp:Z
    :goto_34
    if-eqz v21, :cond_59

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v35

    move/from16 v24, v35

    .line 341
    .local v24, skipIndex:I
    :goto_3c
    const/16 v25, 0x0

    .local v25, sumX:F
    const/16 v26, 0x0

    .line 342
    .local v26, sumY:F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    .line 343
    .local v6, count:I
    const/16 v17, 0x0

    .local v17, i:I
    :goto_46
    move/from16 v0, v17

    move v1, v6

    if-ge v0, v1, :cond_73

    .line 344
    move/from16 v0, v24

    move/from16 v1, v17

    if-ne v0, v1, :cond_5e

    .line 343
    :goto_51
    add-int/lit8 v17, v17, 0x1

    goto :goto_46

    .line 336
    .end local v6           #count:I
    .end local v17           #i:I
    .end local v21           #pointerUp:Z
    .end local v24           #skipIndex:I
    .end local v25           #sumX:F
    .end local v26           #sumY:F
    :cond_54
    const/16 v35, 0x0

    move/from16 v21, v35

    goto :goto_34

    .line 338
    .restart local v21       #pointerUp:Z
    :cond_59
    const/16 v35, -0x1

    move/from16 v24, v35

    goto :goto_3c

    .line 345
    .restart local v6       #count:I
    .restart local v17       #i:I
    .restart local v24       #skipIndex:I
    .restart local v25       #sumX:F
    .restart local v26       #sumY:F
    :cond_5e
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v35

    add-float v25, v25, v35

    .line 346
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v35

    add-float v26, v26, v35

    goto :goto_51

    .line 348
    :cond_73
    if-eqz v21, :cond_92

    const/16 v35, 0x1

    sub-int v35, v6, v35

    move/from16 v11, v35

    .line 349
    .local v11, div:I
    :goto_7b
    move v0, v11

    int-to-float v0, v0

    move/from16 v35, v0

    div-float v13, v25, v35

    .line 350
    .local v13, focusX:F
    move v0, v11

    int-to-float v0, v0

    move/from16 v35, v0

    div-float v14, v26, v35

    .line 352
    .local v14, focusY:F
    const/16 v16, 0x0

    .line 354
    .local v16, handled:Z
    move v0, v5

    and-int/lit16 v0, v0, 0xff

    move/from16 v35, v0

    packed-switch v35, :pswitch_data_4b0

    .line 512
    :cond_91
    :goto_91
    :pswitch_91
    return v16

    .end local v11           #div:I
    .end local v13           #focusX:F
    .end local v14           #focusY:F
    .end local v16           #handled:Z
    :cond_92
    move v11, v6

    .line 348
    goto :goto_7b

    .line 356
    .restart local v11       #div:I
    .restart local v13       #focusX:F
    .restart local v14       #focusY:F
    .restart local v16       #handled:Z
    :pswitch_94
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    .line 357
    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    .line 359
    invoke-direct/range {p0 .. p0}, Lcom/google/android/common/gesture/GestureDetector;->cancelTaps()V

    goto :goto_91

    .line 363
    :pswitch_ac
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    .line 364
    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    .line 368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    const/16 v36, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    move/from16 v37, v0

    move/from16 v0, v37

    int-to-float v0, v0

    move/from16 v37, v0

    invoke-virtual/range {v35 .. v37}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 369
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v27

    .line 370
    .local v27, upIndex:I
    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v18

    .line 371
    .local v18, id1:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v32

    .line 372
    .local v32, x1:F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v34

    .line 373
    .local v34, y1:F
    const/16 v17, 0x0

    :goto_100
    move/from16 v0, v17

    move v1, v6

    if-ge v0, v1, :cond_91

    .line 374
    move/from16 v0, v17

    move/from16 v1, v27

    if-ne v0, v1, :cond_10e

    .line 373
    :cond_10b
    add-int/lit8 v17, v17, 0x1

    goto :goto_100

    .line 376
    :cond_10e
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v19

    .line 377
    .local v19, id2:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v35

    mul-float v31, v32, v35

    .line 378
    .local v31, x:F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v35

    mul-float v33, v34, v35

    .line 380
    .local v33, y:F
    add-float v12, v31, v33

    .line 381
    .local v12, dot:F
    const/16 v35, 0x0

    cmpg-float v35, v12, v35

    if-gez v35, :cond_10b

    .line 382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_91

    .line 389
    .end local v12           #dot:F
    .end local v18           #id1:I
    .end local v19           #id2:I
    .end local v27           #upIndex:I
    .end local v31           #x:F
    .end local v32           #x1:F
    .end local v33           #y:F
    .end local v34           #y1:F
    :pswitch_149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    if-eqz v35, :cond_1c0

    .line 390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v15

    .line 391
    .local v15, hadTapMessage:Z
    if-eqz v15, :cond_16a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 392
    :cond_16a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_279

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_279

    if-eqz v15, :cond_279

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/common/gesture/GestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v35

    if-eqz v35, :cond_279

    .line 395
    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    .line 397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    invoke-interface/range {v35 .. v36}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    .line 399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    .line 406
    .end local v15           #hadTapMessage:Z
    :cond_1c0
    :goto_1c0
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    .line 407
    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_1e5

    .line 409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/MotionEvent;->recycle()V

    .line 411
    :cond_1e5
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    .line 412
    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    .line 413
    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 414
    const/16 v35, 0x1

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    .line 415
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    .line 417
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mIsLongpressEnabled:Z

    move/from16 v35, v0

    if-eqz v35, :cond_249

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v37

    sget v39, Lcom/google/android/common/gesture/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v39, v0

    add-long v37, v37, v39

    sget v39, Lcom/google/android/common/gesture/GestureDetector;->LONGPRESS_TIMEOUT:I

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v39, v0

    add-long v37, v37, v39

    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 422
    :cond_249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v37

    sget v39, Lcom/google/android/common/gesture/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v39

    int-to-long v0, v0

    move-wide/from16 v39, v0

    add-long v37, v37, v39

    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    .line 424
    goto/16 :goto_91

    .line 402
    .restart local v15       #hadTapMessage:Z
    :cond_279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    sget v37, Lcom/google/android/common/gesture/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    move/from16 v0, v37

    int-to-long v0, v0

    move-wide/from16 v37, v0

    invoke-virtual/range {v35 .. v38}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1c0

    .line 427
    .end local v15           #hadTapMessage:Z
    :pswitch_28d
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    move/from16 v35, v0

    if-nez v35, :cond_91

    .line 430
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    move/from16 v35, v0

    sub-float v22, v35, v13

    .line 431
    .local v22, scrollX:F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    move/from16 v35, v0

    sub-float v23, v35, v14

    .line 432
    .local v23, scrollY:F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v35, v0

    if-eqz v35, :cond_2bf

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    goto/16 :goto_91

    .line 435
    :cond_2bf
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v35, v0

    if-eqz v35, :cond_352

    .line 436
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusX:F

    move/from16 v35, v0

    sub-float v35, v13, v35

    move/from16 v0, v35

    float-to-int v0, v0

    move v8, v0

    .line 437
    .local v8, deltaX:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDownFocusY:F

    move/from16 v35, v0

    sub-float v35, v14, v35

    move/from16 v0, v35

    float-to-int v0, v0

    move v9, v0

    .line 438
    .local v9, deltaY:I
    mul-int v35, v8, v8

    mul-int v36, v9, v9

    add-int v10, v35, v36

    .line 439
    .local v10, distance:I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mTouchSlopSquare:I

    move/from16 v35, v0

    move v0, v10

    move/from16 v1, v35

    if-le v0, v1, :cond_33d

    .line 440
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    .line 441
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    .line 442
    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    .line 443
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    .line 444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 445
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 448
    :cond_33d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapTouchSlopSquare:I

    move/from16 v35, v0

    move v0, v10

    move/from16 v1, v35

    if-le v0, v1, :cond_91

    .line 449
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto/16 :goto_91

    .line 451
    .end local v8           #deltaX:I
    .end local v9           #deltaY:I
    .end local v10           #distance:I
    :cond_352
    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->abs(F)F

    move-result v35

    const/high16 v36, 0x3f80

    cmpl-float v35, v35, v36

    if-gez v35, :cond_366

    invoke-static/range {v23 .. v23}, Ljava/lang/Math;->abs(F)F

    move-result v35

    const/high16 v36, 0x3f80

    cmpl-float v35, v35, v36

    if-ltz v35, :cond_91

    .line 452
    :cond_366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    .line 453
    move v0, v13

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusX:F

    .line 454
    move v0, v14

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mLastFocusY:F

    goto/16 :goto_91

    .line 459
    .end local v22           #scrollX:F
    .end local v23           #scrollY:F
    :pswitch_38c
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mStillDown:Z

    .line 460
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v7

    .line 461
    .local v7, currentUpEvent:Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v35, v0

    if-eqz v35, :cond_3ff

    .line 463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v35

    or-int v16, v16, v35

    .line 483
    :cond_3b0
    :goto_3b0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    if-eqz v35, :cond_3c1

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/MotionEvent;->recycle()V

    .line 487
    :cond_3c1
    move-object v0, v7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    if-eqz v35, :cond_3df

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/view/VelocityTracker;->recycle()V

    .line 492
    const/16 v35, 0x0

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 494
    :cond_3df
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mIsDoubleTapping:Z

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_91

    .line 464
    :cond_3ff
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    move/from16 v35, v0

    if-eqz v35, :cond_41b

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    invoke-virtual/range {v35 .. v36}, Landroid/os/Handler;->removeMessages(I)V

    .line 466
    const/16 v35, 0x0

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/common/gesture/GestureDetector;->mInLongPress:Z

    goto :goto_3b0

    .line 467
    :cond_41b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v35, v0

    if-eqz v35, :cond_433

    .line 468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v16

    goto/16 :goto_3b0

    .line 472
    :cond_433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v28, v0

    .line 473
    .local v28, velocityTracker:Landroid/view/VelocityTracker;
    const/16 v35, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v20

    .line 474
    .local v20, pointerId:I
    const/16 v35, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMaximumFlingVelocity:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    move-object/from16 v0, v28

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 475
    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v30

    .line 476
    .local v30, velocityY:F
    move-object/from16 v0, v28

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v29

    .line 478
    .local v29, velocityX:F
    invoke-static/range {v30 .. v30}, Ljava/lang/Math;->abs(F)F

    move-result v35

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    cmpl-float v35, v35, v36

    if-gtz v35, :cond_48f

    invoke-static/range {v29 .. v29}, Ljava/lang/Math;->abs(F)F

    move-result v35

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v36, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    cmpl-float v35, v35, v36

    if-lez v35, :cond_3b0

    .line 480
    :cond_48f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mListener:Landroid/view/GestureDetector$OnGestureListener;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/common/gesture/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v36, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    move-object/from16 v2, p1

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/view/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    goto/16 :goto_3b0

    .line 500
    .end local v7           #currentUpEvent:Landroid/view/MotionEvent;
    .end local v20           #pointerId:I
    .end local v28           #velocityTracker:Landroid/view/VelocityTracker;
    .end local v29           #velocityX:F
    .end local v30           #velocityY:F
    :pswitch_4ab
    invoke-direct/range {p0 .. p0}, Lcom/google/android/common/gesture/GestureDetector;->cancel()V

    goto/16 :goto_91

    .line 354
    :pswitch_data_4b0
    .packed-switch 0x0
        :pswitch_149
        :pswitch_38c
        :pswitch_28d
        :pswitch_4ab
        :pswitch_91
        :pswitch_94
        :pswitch_ac
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .registers 2
    .parameter "isLongpressEnabled"

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/google/android/common/gesture/GestureDetector;->mIsLongpressEnabled:Z

    .line 302
    return-void
.end method

.method public setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V
    .registers 2
    .parameter "onDoubleTapListener"

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/android/common/gesture/GestureDetector;->mDoubleTapListener:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 289
    return-void
.end method
