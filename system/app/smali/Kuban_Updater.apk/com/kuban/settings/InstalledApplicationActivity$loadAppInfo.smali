.class Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;
.super Landroid/os/AsyncTask;
.source "InstalledApplicationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/InstalledApplicationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "loadAppInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/kuban/settings/utils/AppInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/InstalledApplicationActivity;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/InstalledApplicationActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/InstalledApplicationActivity;Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .registers 11
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/kuban/settings/utils/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 104
    iget-object v7, p0, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v7}, Lcom/kuban/settings/InstalledApplicationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 105
    .local v5, manager:Landroid/content/pm/PackageManager;
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v4, v7, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 106
    .local v4, mainIntent:Landroid/content/Intent;
    const-string v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 107
    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 108
    .local v1, apps:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v7, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v7, v5}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v1, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 109
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v7}, Lcom/kuban/settings/InstalledApplicationActivity;->access$0(Ljava/util/List;)V

    .line 110
    if-nez v1, :cond_2b

    .line 123
    :goto_2a
    return-object v6

    .line 111
    :cond_2b
    const/4 v2, 0x0

    .local v2, i:I
    :goto_2c
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-lt v2, v6, :cond_37

    .line 123
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$1()Ljava/util/List;

    move-result-object v6

    goto :goto_2a

    .line 112
    :cond_37
    new-instance v0, Lcom/kuban/settings/utils/AppInfo;

    invoke-direct {v0}, Lcom/kuban/settings/utils/AppInfo;-><init>()V

    .line 113
    .local v0, appInfo:Lcom/kuban/settings/utils/AppInfo;
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 114
    .local v3, info:Landroid/content/pm/ResolveInfo;
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/kuban/settings/utils/AppInfo;->setPackageName(Ljava/lang/String;)V

    .line 115
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/kuban/settings/utils/AppInfo;->setName(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v3, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, v0, Lcom/kuban/settings/utils/AppInfo;->title:Ljava/lang/CharSequence;

    .line 117
    new-instance v6, Landroid/content/ComponentName;

    .line 118
    iget-object v7, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v8, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const/high16 v7, 0x1020

    .line 117
    invoke-virtual {v0, v6, v7}, Lcom/kuban/settings/utils/AppInfo;->setActivity(Landroid/content/ComponentName;I)V

    .line 120
    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v6, v5}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    iput-object v6, v0, Lcom/kuban/settings/utils/AppInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 121
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$1()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v2, v2, 0x1

    goto :goto_2c
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/kuban/settings/utils/AppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, result:Ljava/util/List;,"Ljava/util/List<Lcom/kuban/settings/utils/AppInfo;>;"
    iget-object v0, p0, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/kuban/settings/InstalledApplicationActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 128
    new-instance v0, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    iget-object v1, p0, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v0, v1}, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/kuban/settings/InstalledApplicationActivity;->access$2(Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;)V

    .line 129
    iget-object v0, p0, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$3()Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/InstalledApplicationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    return-void
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 98
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 99
    iget-object v0, p0, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/InstalledApplicationActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 100
    return-void
.end method
