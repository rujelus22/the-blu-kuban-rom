.class Lcom/kuban/settings/DownloadedItemsActivity$1;
.super Ljava/lang/Object;
.source "DownloadedItemsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/DownloadedItemsActivity;->ListDir(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/DownloadedItemsActivity;

.field private final synthetic val$dl_listview:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/kuban/settings/DownloadedItemsActivity;Landroid/widget/ListView;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/DownloadedItemsActivity$1;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    iput-object p2, p0, Lcom/kuban/settings/DownloadedItemsActivity$1;->val$dl_listview:Landroid/widget/ListView;

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 15
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 85
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    move-object v1, p2

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 86
    .local v1, check:Landroid/widget/CheckedTextView;
    const/4 v2, 0x0

    .line 87
    .local v2, checked:I
    iget-object v7, p0, Lcom/kuban/settings/DownloadedItemsActivity$1;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    const v8, 0x7f0c0016

    invoke-virtual {v7, v8}, Lcom/kuban/settings/DownloadedItemsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 88
    .local v0, btn:Landroid/widget/Button;
    iget-object v7, p0, Lcom/kuban/settings/DownloadedItemsActivity$1;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    const v8, 0x7f0c0002

    invoke-virtual {v7, v8}, Lcom/kuban/settings/DownloadedItemsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 89
    .local v5, installbtn:Landroid/widget/Button;
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getCount()I

    move-result v3

    .line 90
    .local v3, cntChoice:I
    iget-object v7, p0, Lcom/kuban/settings/DownloadedItemsActivity$1;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v6

    .line 91
    .local v6, sparseBooleanArray:Landroid/util/SparseBooleanArray;
    const/4 v4, 0x0

    .local v4, i:I
    :goto_25
    if-lt v4, v3, :cond_28

    .line 110
    return-void

    .line 92
    :cond_28
    invoke-virtual {v6, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v7

    if-eqz v7, :cond_46

    .line 93
    const/4 v2, 0x1

    .line 94
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_44

    const/4 v7, 0x0

    :goto_36
    invoke-virtual {v1, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 95
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 96
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 91
    :goto_41
    add-int/lit8 v4, v4, 0x1

    goto :goto_25

    .line 94
    :cond_44
    const/4 v7, 0x1

    goto :goto_36

    .line 99
    :cond_46
    const/4 v7, 0x1

    if-ne v2, v7, :cond_5e

    .line 100
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_5c

    const/4 v7, 0x0

    :goto_50
    invoke-virtual {v1, v7}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 101
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 102
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_41

    .line 100
    :cond_5c
    const/4 v7, 0x1

    goto :goto_50

    .line 105
    :cond_5e
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 106
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_41
.end method
