.class public Lcom/kuban/settings/Home;
.super Landroid/app/Activity;
.source "Home.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/Home$HomeAsync;
    }
.end annotation


# static fields
.field static final KEY_ICON:Ljava/lang/String; = "Icon"

.field static final KEY_ITEM:Ljava/lang/String; = "Tab"

.field static final KEY_NAME:Ljava/lang/String; = "TabName"

.field static final KEY_TITLE:Ljava/lang/String; = "Rom"

.field static final KEY_URL:Ljava/lang/String; = "Url"

.field public static mobile:Z

.field public static mobileType:I

.field public static time:J

.field public static wifi:Z

.field public static wimax:Z


# instance fields
.field adapter:Landroid/widget/SimpleAdapter;

.field eventsData:Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;

.field icon:I

.field result:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput v0, Lcom/kuban/settings/Home;->mobileType:I

    .line 53
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 59
    const v0, 0x7f020021

    iput v0, p0, Lcom/kuban/settings/Home;->icon:I

    .line 53
    return-void
.end method

.method public static setHasPermanentMenuKey(Landroid/content/Context;Z)V
    .registers 6
    .parameter "context"
    .parameter "value"

    .prologue
    .line 335
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 337
    .local v0, config:Landroid/view/ViewConfiguration;
    :try_start_4
    const-class v2, Landroid/view/ViewConfiguration;

    const-string v3, "sHasPermanentMenuKey"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 338
    .local v1, f:Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 339
    if-eqz v1, :cond_19

    .line 340
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_19} :catch_1a

    .line 343
    .end local v1           #f:Ljava/lang/reflect/Field;
    :cond_19
    :goto_19
    return-void

    .line 342
    :catch_1a
    move-exception v2

    goto :goto_19
.end method


# virtual methods
.method public isOnline()Z
    .registers 15

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 132
    const-string v9, "activity"

    invoke-virtual {p0, v9}, Lcom/kuban/settings/Home;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 133
    .local v0, activityManager:Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v6

    .line 134
    .local v6, procInfos:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/4 v3, 0x0

    .local v3, i:I
    :goto_f
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-lt v3, v9, :cond_59

    .line 142
    new-instance v8, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Lcom/kuban/settings/utils/RomCheck;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    .local v8, startRomServiceIntent:Landroid/content/Intent;
    new-instance v7, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-class v10, Lcom/kuban/settings/utils/ItemCheck;

    invoke-direct {v7, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 144
    .local v7, startItemServiceIntent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 145
    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 146
    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getBaseContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 147
    .local v5, prefs:Landroid/content/SharedPreferences;
    const-string v9, "warnmobile"

    const/4 v10, 0x1

    invoke-interface {v5, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    .line 148
    .local v4, mobilewarn:Z
    sget-boolean v9, Lcom/kuban/settings/Home;->mobile:Z

    if-eqz v9, :cond_ae

    .line 149
    if-nez v4, :cond_99

    .line 150
    new-instance v9, Lcom/kuban/settings/Home$HomeAsync;

    invoke-direct {v9, p0, v13}, Lcom/kuban/settings/Home$HomeAsync;-><init>(Lcom/kuban/settings/Home;Lcom/kuban/settings/Home$HomeAsync;)V

    new-array v10, v12, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/kuban/settings/Home$HomeAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 195
    :goto_58
    return v12

    .line 135
    .end local v4           #mobilewarn:Z
    .end local v5           #prefs:Landroid/content/SharedPreferences;
    .end local v7           #startItemServiceIntent:Landroid/content/Intent;
    .end local v8           #startRomServiceIntent:Landroid/content/Intent;
    :cond_59
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v9, v9, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string v10, "com.kuban.settings.RomCheck"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_77

    .line 136
    new-instance v9, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-class v11, Lcom/kuban/settings/utils/RomCheck;

    invoke-direct {v9, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v9}, Lcom/kuban/settings/Home;->stopService(Landroid/content/Intent;)Z

    .line 138
    :cond_77
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget-object v9, v9, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string v10, "com.kuban.settings.ItemCheck"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_95

    .line 139
    new-instance v9, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const-class v11, Lcom/kuban/settings/utils/ItemCheck;

    invoke-direct {v9, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v9}, Lcom/kuban/settings/Home;->stopService(Landroid/content/Intent;)Z

    .line 134
    :cond_95
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_f

    .line 152
    .restart local v4       #mobilewarn:Z
    .restart local v5       #prefs:Landroid/content/SharedPreferences;
    .restart local v7       #startItemServiceIntent:Landroid/content/Intent;
    .restart local v8       #startRomServiceIntent:Landroid/content/Intent;
    :cond_99
    sget v9, Lcom/kuban/settings/Home;->mobileType:I

    const/16 v10, 0xd

    if-ne v9, v10, :cond_aa

    .line 153
    new-instance v9, Lcom/kuban/settings/Home$HomeAsync;

    invoke-direct {v9, p0, v13}, Lcom/kuban/settings/Home$HomeAsync;-><init>(Lcom/kuban/settings/Home;Lcom/kuban/settings/Home$HomeAsync;)V

    new-array v10, v12, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/kuban/settings/Home$HomeAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_58

    .line 156
    :cond_aa
    invoke-virtual {p0}, Lcom/kuban/settings/Home;->mobile()Z

    goto :goto_58

    .line 159
    :cond_ae
    sget-boolean v9, Lcom/kuban/settings/Home;->wifi:Z

    if-eqz v9, :cond_bd

    .line 160
    new-instance v9, Lcom/kuban/settings/Home$HomeAsync;

    invoke-direct {v9, p0, v13}, Lcom/kuban/settings/Home$HomeAsync;-><init>(Lcom/kuban/settings/Home;Lcom/kuban/settings/Home$HomeAsync;)V

    new-array v10, v12, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/kuban/settings/Home$HomeAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_58

    .line 162
    :cond_bd
    sget-boolean v9, Lcom/kuban/settings/Home;->wimax:Z

    if-eqz v9, :cond_cc

    .line 163
    new-instance v9, Lcom/kuban/settings/Home$HomeAsync;

    invoke-direct {v9, p0, v13}, Lcom/kuban/settings/Home$HomeAsync;-><init>(Lcom/kuban/settings/Home;Lcom/kuban/settings/Home$HomeAsync;)V

    new-array v10, v12, [Ljava/lang/Void;

    invoke-virtual {v9, v10}, Lcom/kuban/settings/Home$HomeAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_58

    .line 166
    :cond_cc
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 167
    .local v2, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v9, "No Internet Detected!"

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 169
    const-string v9, "\nNo internet was found.\n\nYou must have internet it use this application\n"

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    .line 170
    invoke-virtual {v9, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    .line 171
    const-string v10, "Retry"

    new-instance v11, Lcom/kuban/settings/Home$2;

    invoke-direct {v11, p0}, Lcom/kuban/settings/Home$2;-><init>(Lcom/kuban/settings/Home;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    .line 183
    const-string v10, "Exit"

    new-instance v11, Lcom/kuban/settings/Home$3;

    invoke-direct {v11, p0}, Lcom/kuban/settings/Home$3;-><init>(Lcom/kuban/settings/Home;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 192
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 193
    .local v1, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_58
.end method

.method public mobile()Z
    .registers 6

    .prologue
    .line 198
    const v2, 0x7f03000d

    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->setContentView(I)V

    .line 199
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 200
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v2, "Mobile Connection Detected!"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 202
    const-string v2, "\nA mobile connections is not advised.\n\nWould you like to continue any way?\n"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 203
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 204
    const-string v3, "Yes"

    new-instance v4, Lcom/kuban/settings/Home$4;

    invoke-direct {v4, p0}, Lcom/kuban/settings/Home$4;-><init>(Lcom/kuban/settings/Home;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 209
    const-string v3, "No"

    new-instance v4, Lcom/kuban/settings/Home$5;

    invoke-direct {v4, p0}, Lcom/kuban/settings/Home$5;-><init>(Lcom/kuban/settings/Home;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 215
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 216
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 218
    const/4 v2, 0x1

    return v2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    .prologue
    .line 71
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 72
    const v1, 0x7f03000d

    invoke-virtual {p0, v1}, Lcom/kuban/settings/Home;->setContentView(I)V

    .line 73
    const v1, 0x7f0c0022

    invoke-virtual {p0, v1}, Lcom/kuban/settings/Home;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 74
    .local v0, gridView:Landroid/widget/GridView;
    iget-object v1, p0, Lcom/kuban/settings/Home;->adapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    new-instance v1, Lcom/kuban/settings/Home$1;

    invoke-direct {v1, p0}, Lcom/kuban/settings/Home$1;-><init>(Lcom/kuban/settings/Home;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x0

    .line 91
    const/4 v1, 0x5

    invoke-virtual {p0, v1}, Lcom/kuban/settings/Home;->requestWindowFeature(I)Z

    .line 92
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    new-instance v1, Landroid/os/StrictMode$ThreadPolicy$Builder;

    invoke-direct {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;-><init>()V

    .line 94
    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->detectNetwork()Landroid/os/StrictMode$ThreadPolicy$Builder;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Landroid/os/StrictMode$ThreadPolicy$Builder;->build()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v1

    .line 93
    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 96
    const v1, 0x7f03000d

    invoke-virtual {p0, v1}, Lcom/kuban/settings/Home;->setContentView(I)V

    .line 97
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/kuban/settings/Home;->setHasPermanentMenuKey(Landroid/content/Context;Z)V

    .line 98
    new-instance v1, Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;

    invoke-direct {v1, p0}, Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/kuban/settings/Home;->eventsData:Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;

    .line 99
    iget-object v1, p0, Lcom/kuban/settings/Home;->eventsData:Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/TimeOpenedDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 100
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "events"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 101
    .local v8, cursor:Landroid/database/Cursor;
    invoke-virtual {p0, v8}, Lcom/kuban/settings/Home;->startManagingCursor(Landroid/database/Cursor;)V

    .line 102
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_7c

    .line 104
    :try_start_43
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 105
    .local v10, values:Landroid/content/ContentValues;
    const-string v1, "time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 106
    const-string v1, "events"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 107
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_5e
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_5e} :catch_ab

    .line 117
    .end local v10           #values:Landroid/content/ContentValues;
    :goto_5e
    invoke-virtual {p0, v8}, Lcom/kuban/settings/Home;->stopManagingCursor(Landroid/database/Cursor;)V

    .line 118
    :goto_61
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_9b

    .line 123
    :try_start_67
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    const-string v2, "su"

    invoke-virtual {v1, v2}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_70
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_70} :catch_a6

    .line 127
    :goto_70
    new-instance v1, Lcom/kuban/settings/utils/ConnectivityCheck;

    invoke-direct {v1}, Lcom/kuban/settings/utils/ConnectivityCheck;-><init>()V

    .line 128
    invoke-static {p0}, Lcom/kuban/settings/utils/ConnectivityCheck;->isNetAvailable(Landroid/content/Context;)Z

    .line 129
    invoke-virtual {p0}, Lcom/kuban/settings/Home;->isOnline()Z

    .line 130
    return-void

    .line 111
    :cond_7c
    :try_start_7c
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 112
    .restart local v10       #values:Landroid/content/ContentValues;
    const-string v1, "time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 113
    const-string v1, "events"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 114
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_98
    .catch Ljava/lang/Exception; {:try_start_7c .. :try_end_98} :catch_99

    goto :goto_5e

    .line 115
    .end local v10           #values:Landroid/content/ContentValues;
    :catch_99
    move-exception v1

    goto :goto_5e

    .line 119
    :cond_9b
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    sput-wide v1, Lcom/kuban/settings/Home;->time:J

    .line 120
    invoke-virtual {p0, v8}, Lcom/kuban/settings/Home;->stopManagingCursor(Landroid/database/Cursor;)V

    goto :goto_61

    .line 124
    :catch_a6
    move-exception v9

    .line 125
    .local v9, e:Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_70

    .line 108
    .end local v9           #e:Ljava/io/IOException;
    :catch_ab
    move-exception v1

    goto :goto_5e
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 347
    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 348
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 349
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 354
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 355
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 356
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 357
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 358
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 359
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 360
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 411
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :goto_2b
    return v3

    .line 363
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4b

    .line 364
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 365
    .local v2, in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 369
    .end local v2           #in:Landroid/content/Intent;
    :cond_4b
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_6a

    .line 370
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 371
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 372
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 375
    .end local v2           #in:Landroid/content/Intent;
    :cond_6a
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-ne v4, v5, :cond_89

    .line 376
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Change Log"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 381
    .end local v2           #in:Landroid/content/Intent;
    :cond_89
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_a8

    .line 382
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 383
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 387
    .end local v2           #in:Landroid/content/Intent;
    :cond_a8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_c8

    .line 388
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 389
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 393
    .end local v2           #in:Landroid/content/Intent;
    :cond_c8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_e8

    .line 394
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 395
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 396
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 399
    .end local v2           #in:Landroid/content/Intent;
    :cond_e8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_108

    .line 400
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 401
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 405
    .end local v2           #in:Landroid/content/Intent;
    :cond_108
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_128

    .line 406
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 407
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 408
    invoke-virtual {p0, v2}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 411
    .end local v2           #in:Landroid/content/Intent;
    :cond_128
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method
