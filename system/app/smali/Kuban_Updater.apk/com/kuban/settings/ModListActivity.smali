.class public Lcom/kuban/settings/ModListActivity;
.super Landroid/app/Activity;
.source "ModListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/ModListActivity$ModAsync;
    }
.end annotation


# static fields
.field public static final DAYS_DIFF:Ljava/lang/String; = "604800000"

.field static final KEY_ICON:Ljava/lang/String; = "Icon"

.field static final KEY_ITEM:Ljava/lang/String; = "Element"

.field static final KEY_ITEM2:Ljava/lang/String; = "Tab"

.field public static final KEY_ITEM_DESC:Ljava/lang/String; = "Desc"

.field public static final KEY_ITEM_DL:Ljava/lang/String; = "No"

.field public static final KEY_ITEM_FOLDER:Ljava/lang/String; = "Folder"

.field public static final KEY_ITEM_INSTALL:Ljava/lang/String; = "No"

.field public static final KEY_ITEM_NAME:Ljava/lang/String; = "Name"

.field public static final KEY_ITEM_NEW:Ljava/lang/String; = "Date"

.field public static final KEY_ITEM_PREVIEW:Ljava/lang/String; = "Preview"

.field public static final KEY_ITEM_PREVIEW2:Ljava/lang/String; = "Preview2"

.field public static final KEY_ITEM_PREVIEW3:Ljava/lang/String; = "Preview3"

.field public static final KEY_ITEM_THUMB:Ljava/lang/String; = "ThumbnailUrl"

.field public static final KEY_ITEM_URL:Ljava/lang/String; = "Url"

.field static final KEY_NAME:Ljava/lang/String; = "TabName"

.field static final KEY_TITLE:Ljava/lang/String; = "Rom"

.field static final KEY_URL:Ljava/lang/String; = "Url"

.field static final MENUTITLE:Ljava/lang/String;

.field public static dataFromAsyncTask:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field public static menuposition:I


# instance fields
.field TITLE:Ljava/lang/String;

.field actions:[Ljava/lang/String;

.field gridadapter:Landroid/widget/SimpleAdapter;

.field gridmenu:Ljava/lang/Boolean;

.field icon:I

.field list:Landroid/widget/ListView;

.field mainadapter:Lcom/kuban/settings/adapters/ListItemAdapter;

.field final menuItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field menuadapter:Landroid/widget/SimpleAdapter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 59
    const/4 v0, 0x0

    sput-object v0, Lcom/kuban/settings/ModListActivity;->MENUTITLE:Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    sput v0, Lcom/kuban/settings/ModListActivity;->menuposition:I

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/ModListActivity;->menuItems:Ljava/util/ArrayList;

    .line 68
    const v0, 0x7f020021

    iput v0, p0, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 69
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/ModListActivity;->gridmenu:Ljava/lang/Boolean;

    .line 40
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 15
    .parameter "newConfig"

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    .line 75
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 76
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity;->gridmenu:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 77
    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ModListActivity;->setContentView(I)V

    .line 78
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 79
    .local v8, in:Landroid/content/Intent;
    const-string v0, "TabName"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 80
    .local v10, passed_name:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 81
    const v0, 0x7f0c0022

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ModListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/GridView;

    .line 82
    .local v7, gridView:Landroid/widget/GridView;
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity;->gridadapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    new-instance v0, Lcom/kuban/settings/ModListActivity$1;

    invoke-direct {v0, p0}, Lcom/kuban/settings/ModListActivity$1;-><init>(Lcom/kuban/settings/ModListActivity;)V

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 157
    .end local v7           #gridView:Landroid/widget/GridView;
    .end local v8           #in:Landroid/content/Intent;
    .end local v10           #passed_name:Ljava/lang/String;
    :goto_3a
    return-void

    .line 96
    :cond_3b
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ModListActivity;->setContentView(I)V

    .line 97
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 98
    const v0, 0x7f0c0023

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ModListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kuban/settings/ModListActivity;->list:Landroid/widget/ListView;

    .line 99
    new-instance v0, Lcom/kuban/settings/adapters/ListItemAdapter;

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity;->menuItems:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lcom/kuban/settings/adapters/ListItemAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/kuban/settings/ModListActivity;->mainadapter:Lcom/kuban/settings/adapters/ListItemAdapter;

    .line 100
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity;->mainadapter:Lcom/kuban/settings/adapters/ListItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/kuban/settings/ModListActivity$2;

    invoke-direct {v1, p0}, Lcom/kuban/settings/ModListActivity$2;-><init>(Lcom/kuban/settings/ModListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 126
    new-instance v0, Lcom/kuban/settings/ModListActivity$3;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/kuban/settings/ModListActivity;->dataFromAsyncTask:Ljava/util/ArrayList;

    const v4, 0x7f030007

    .line 127
    new-array v5, v12, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "Url"

    aput-object v6, v5, v1

    const-string v1, "Icon"

    aput-object v1, v5, v11

    .line 128
    new-array v6, v12, [I

    fill-array-data v6, :array_a6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/kuban/settings/ModListActivity$3;-><init>(Lcom/kuban/settings/ModListActivity;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 126
    iput-object v0, p0, Lcom/kuban/settings/ModListActivity;->menuadapter:Landroid/widget/SimpleAdapter;

    .line 135
    new-instance v9, Lcom/kuban/settings/ModListActivity$4;

    invoke-direct {v9, p0}, Lcom/kuban/settings/ModListActivity$4;-><init>(Lcom/kuban/settings/ModListActivity;)V

    .line 154
    .local v9, navigationListener:Landroid/app/ActionBar$OnNavigationListener;
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity;->menuadapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1, v9}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 155
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v1, Lcom/kuban/settings/ModListActivity;->menuposition:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_3a

    .line 128
    :array_a6
    .array-data 0x4
        0x15t 0x0t 0xct 0x7ft
        0x18t 0x0t 0xct 0x7ft
    .end array-data
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 160
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->requestWindowFeature(I)Z

    .line 161
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 162
    const v2, 0x7f03000e

    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->setContentView(I)V

    .line 163
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 164
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 165
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 166
    .local v0, in:Landroid/content/Intent;
    const-string v2, "TabName"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 167
    .local v1, passed_name:Ljava/lang/String;
    new-instance v2, Lcom/kuban/settings/adapters/MenuAsync;

    invoke-direct {v2}, Lcom/kuban/settings/adapters/MenuAsync;-><init>()V

    new-array v3, v5, [Ljava/lang/String;

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/kuban/settings/adapters/MenuAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 168
    new-instance v2, Lcom/kuban/settings/ModListActivity$ModAsync;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/kuban/settings/ModListActivity$ModAsync;-><init>(Lcom/kuban/settings/ModListActivity;Lcom/kuban/settings/ModListActivity$ModAsync;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/kuban/settings/ModListActivity$ModAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 169
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 386
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 387
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 392
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 393
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 394
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 395
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 396
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 397
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 398
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 464
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :goto_2b
    return v3

    .line 401
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4e

    .line 402
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 403
    .local v2, in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 404
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 405
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 408
    .end local v2           #in:Landroid/content/Intent;
    :cond_4e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_70

    .line 409
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 410
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 412
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 415
    .end local v2           #in:Landroid/content/Intent;
    :cond_70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-ne v4, v5, :cond_92

    .line 416
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 417
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Change Log"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 418
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 419
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 422
    .end local v2           #in:Landroid/content/Intent;
    :cond_92
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_b5

    .line 423
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 424
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 425
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 426
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 429
    .end local v2           #in:Landroid/content/Intent;
    :cond_b5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_d8

    .line 430
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 431
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 432
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 433
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 436
    .end local v2           #in:Landroid/content/Intent;
    :cond_d8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_fb

    .line 437
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 438
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 439
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 440
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 443
    .end local v2           #in:Landroid/content/Intent;
    :cond_fb
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_11e

    .line 444
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 445
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 446
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 447
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 450
    .end local v2           #in:Landroid/content/Intent;
    :cond_11e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_141

    .line 451
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 452
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 454
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 457
    .end local v2           #in:Landroid/content/Intent;
    :cond_141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x102002c

    if-ne v4, v5, :cond_162

    .line 458
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/Home;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 459
    .restart local v2       #in:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 460
    invoke-virtual {p0}, Lcom/kuban/settings/ModListActivity;->finish()V

    .line 461
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ModListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 464
    .end local v2           #in:Landroid/content/Intent;
    :cond_162
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method
