.class public Lcom/kuban/settings/DownloadedItemsActivity;
.super Landroid/app/Activity;
.source "DownloadedItemsActivity.java"


# static fields
.field static final DL_ITEM:Ljava/lang/String; = "filename"

.field static final KEY_ICON:Ljava/lang/String; = "Icon"

.field static final KEY_NAME:Ljava/lang/String; = "ModName"

.field static final KEY_TAB:Ljava/lang/String; = "Tab"

.field static final KEY_URL:Ljava/lang/String; = "Url"

.field private static final TAG:Ljava/lang/String; = "KUBAN UPDATER DEBUG"


# instance fields
.field adapter:Lcom/kuban/settings/adapters/ListItemAdapter;

.field downloadFolder:Ljava/lang/String;

.field items:[Ljava/lang/String;

.field list:Landroid/widget/ListView;

.field menuposition:I

.field root:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/kuban/settings/DownloadedItemsActivity;->menuposition:I

    .line 32
    return-void
.end method


# virtual methods
.method ListDir(Ljava/io/File;)V
    .registers 13
    .parameter "dl_dir"

    .prologue
    const/4 v10, 0x0

    .line 69
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 70
    .local v2, dirs:[Ljava/lang/String;
    if-eqz v2, :cond_5b

    .line 72
    const v8, 0x102000a

    invoke-virtual {p0, v8}, Lcom/kuban/settings/DownloadedItemsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 73
    .local v3, dl_listview:Landroid/widget/ListView;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v4, fillMaps:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v5, 0x0

    .local v5, i:I
    :goto_16
    array-length v8, v2

    if-lt v5, v8, :cond_5c

    .line 79
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v8, 0x7f03000c

    invoke-direct {v0, p0, v8, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 80
    .local v0, adapter:Landroid/widget/ListAdapter;
    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 81
    invoke-virtual {v3, v10}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 82
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    new-instance v8, Lcom/kuban/settings/DownloadedItemsActivity$1;

    invoke-direct {v8, p0, v3}, Lcom/kuban/settings/DownloadedItemsActivity$1;-><init>(Lcom/kuban/settings/DownloadedItemsActivity;Landroid/widget/ListView;)V

    invoke-virtual {v3, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 112
    const v8, 0x7f0c0016

    invoke-virtual {p0, v8}, Lcom/kuban/settings/DownloadedItemsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 113
    .local v1, btn:Landroid/widget/Button;
    const v8, 0x7f0c0002

    invoke-virtual {p0, v8}, Lcom/kuban/settings/DownloadedItemsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    .line 114
    .local v6, installbtn:Landroid/widget/Button;
    invoke-virtual {v1, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 115
    invoke-virtual {v6, v10}, Landroid/widget/Button;->setEnabled(Z)V

    .line 116
    new-instance v8, Lcom/kuban/settings/DownloadedItemsActivity$2;

    invoke-direct {v8, p0, v3}, Lcom/kuban/settings/DownloadedItemsActivity$2;-><init>(Lcom/kuban/settings/DownloadedItemsActivity;Landroid/widget/ListView;)V

    invoke-virtual {v1, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    new-instance v8, Lcom/kuban/settings/DownloadedItemsActivity$3;

    invoke-direct {v8, p0, v3}, Lcom/kuban/settings/DownloadedItemsActivity$3;-><init>(Lcom/kuban/settings/DownloadedItemsActivity;Landroid/widget/ListView;)V

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    .end local v0           #adapter:Landroid/widget/ListAdapter;
    .end local v1           #btn:Landroid/widget/Button;
    .end local v3           #dl_listview:Landroid/widget/ListView;
    .end local v4           #fillMaps:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v5           #i:I
    .end local v6           #installbtn:Landroid/widget/Button;
    :cond_5b
    return-void

    .line 75
    .restart local v3       #dl_listview:Landroid/widget/ListView;
    .restart local v4       #fillMaps:Ljava/util/List;,"Ljava/util/List<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v5       #i:I
    :cond_5c
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 76
    .local v7, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v8, "filename"

    aget-object v9, v2, v5

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    add-int/lit8 v5, v5, 0x1

    goto :goto_16
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/kuban/settings/DownloadedItemsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 59
    invoke-virtual {p0}, Lcom/kuban/settings/DownloadedItemsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    const-string v5, "Manage Mods"

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 60
    const v4, 0x7f030006

    invoke-virtual {p0, v4}, Lcom/kuban/settings/DownloadedItemsActivity;->setContentView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/kuban/settings/DownloadedItemsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 62
    .local v2, prefs:Landroid/content/SharedPreferences;
    const-string v4, "downloadDir"

    const-string v5, "kubandownloads"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/kuban/settings/DownloadedItemsActivity;->downloadFolder:Ljava/lang/String;

    .line 63
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/kuban/settings/DownloadedItemsActivity;->downloadFolder:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 64
    .local v1, dlfolder:Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 65
    .local v3, root:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v0, dl_dir:Ljava/io/File;
    invoke-virtual {p0, v0}, Lcom/kuban/settings/DownloadedItemsActivity;->ListDir(Ljava/io/File;)V

    .line 67
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 49
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/DownloadedItemsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kuban/settings/Home;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v0, in:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0}, Lcom/kuban/settings/DownloadedItemsActivity;->finish()V

    .line 52
    invoke-virtual {p0, v0}, Lcom/kuban/settings/DownloadedItemsActivity;->startActivity(Landroid/content/Intent;)V

    .line 53
    const/4 v1, 0x1

    return v1
.end method
