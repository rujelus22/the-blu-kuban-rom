.class Lcom/kuban/settings/Home$HomeAsync$2;
.super Ljava/lang/Object;
.source "Home.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/Home$HomeAsync;->onPostExecute(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/kuban/settings/Home$HomeAsync;


# direct methods
.method constructor <init>(Lcom/kuban/settings/Home$HomeAsync;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/Home$HomeAsync$2;->this$1:Lcom/kuban/settings/Home$HomeAsync;

    .line 320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 11
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const v3, 0x7f0c001a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, name:Ljava/lang/String;
    const v3, 0x7f0c001c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, url:Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/kuban/settings/Home$HomeAsync$2;->this$1:Lcom/kuban/settings/Home$HomeAsync;

    #getter for: Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;
    invoke-static {v3}, Lcom/kuban/settings/Home$HomeAsync;->access$3(Lcom/kuban/settings/Home$HomeAsync;)Lcom/kuban/settings/Home;

    move-result-object v3

    invoke-virtual {v3}, Lcom/kuban/settings/Home;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/kuban/settings/ModListActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 327
    .local v0, in:Landroid/content/Intent;
    const-string v3, "TabName"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    const-string v3, "Url"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    iget-object v3, p0, Lcom/kuban/settings/Home$HomeAsync$2;->this$1:Lcom/kuban/settings/Home$HomeAsync;

    #getter for: Lcom/kuban/settings/Home$HomeAsync;->this$0:Lcom/kuban/settings/Home;
    invoke-static {v3}, Lcom/kuban/settings/Home$HomeAsync;->access$3(Lcom/kuban/settings/Home$HomeAsync;)Lcom/kuban/settings/Home;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/kuban/settings/Home;->startActivity(Landroid/content/Intent;)V

    .line 330
    return-void
.end method
