.class Lcom/kuban/settings/DownloadedItemsActivity$3;
.super Ljava/lang/Object;
.source "DownloadedItemsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/DownloadedItemsActivity;->ListDir(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/DownloadedItemsActivity;

.field private final synthetic val$dl_listview:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/kuban/settings/DownloadedItemsActivity;Landroid/widget/ListView;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    iput-object p2, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->val$dl_listview:Landroid/widget/ListView;

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 16
    .parameter "v"

    .prologue
    .line 153
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v11}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 154
    .local v0, cntChoice:I
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v11}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v9

    .line 155
    .local v9, sparseBooleanArray:Landroid/util/SparseBooleanArray;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    .local v7, s:Ljava/lang/StringBuilder;
    const-string v6, "install_zip(\""

    .line 157
    .local v6, prefix:Ljava/lang/String;
    const-string v10, "\");"

    .line 158
    .local v10, suffix:Ljava/lang/String;
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v11}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 159
    .local v8, selected:Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    .line 160
    .local v3, folder:Ljava/io/File;
    const/4 v4, 0x0

    .local v4, i:I
    :goto_24
    if-lt v4, v0, :cond_78

    .line 171
    if-eqz v7, :cond_67

    .line 173
    :try_start_28
    const-string v11, "KUBAN UPDATER DEBUG"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "444444 su -c /sbin/echo \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\' >> /cache/recovery/extendedcommand"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "su -c /sbin/echo \'"

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "\' >> /cache/recovery/extendedcommand"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 175
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v11

    const-string v12, "su -c /sbin/reboot recovery"

    invoke-virtual {v11, v12}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_67
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_67} :catch_c9

    .line 180
    :cond_67
    :goto_67
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v11}, Lcom/kuban/settings/DownloadedItemsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 181
    .local v5, intent:Landroid/content/Intent;
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v11}, Lcom/kuban/settings/DownloadedItemsActivity;->finish()V

    .line 182
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v11, v5}, Lcom/kuban/settings/DownloadedItemsActivity;->startActivity(Landroid/content/Intent;)V

    .line 183
    return-void

    .line 161
    .end local v5           #intent:Landroid/content/Intent;
    :cond_78
    invoke-virtual {v9, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v11

    if-eqz v11, :cond_c5

    .line 162
    iget-object v11, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v11, v4}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 163
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$3;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    iget-object v12, v12, Lcom/kuban/settings/DownloadedItemsActivity;->downloadFolder:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, fileName:Ljava/lang/String;
    const-string v11, "/mnt"

    const-string v12, ""

    invoke-virtual {v2, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 165
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 160
    .end local v2           #fileName:Ljava/lang/String;
    :cond_c5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_24

    .line 176
    :catch_c9
    move-exception v1

    .line 177
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_67
.end method
