.class public Lcom/kuban/settings/adapters/MenuAsync;
.super Landroid/os/AsyncTask;
.source "MenuAsync.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# static fields
.field static final KEY_ICON:Ljava/lang/String; = "Icon"

.field static final KEY_ITEM2:Ljava/lang/String; = "Tab"

.field static final KEY_NAME:Ljava/lang/String; = "TabName"

.field static final KEY_URL:Ljava/lang/String; = "Url"


# instance fields
.field icon:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 24
    const v0, 0x7f020021

    iput v0, p0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 18
    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/adapters/MenuAsync;->doInBackground([Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 19
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 31
    const/4 v15, 0x0

    aget-object v6, p1, v15

    .line 32
    .local v6, identifier:Ljava/lang/String;
    const-string v15, "kuban.updater.parts"

    invoke-static {v15}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    .local v1, BASEURL:Ljava/lang/String;
    move-object v3, v1

    .line 34
    .local v3, URL:Ljava/lang/String;
    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    .line 36
    :cond_f
    const/4 v15, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x8

    move/from16 v0, v16

    invoke-virtual {v1, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 37
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .local v2, Items:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v10, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v10}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 39
    .local v10, parser:Lcom/kuban/settings/utils/XMLParser;
    invoke-virtual {v10, v3}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 40
    .local v14, xml:Ljava/lang/String;
    invoke-virtual {v10, v14}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v4

    .line 41
    .local v4, doc:Lorg/w3c/dom/Document;
    const-string v15, "Tab"

    invoke-interface {v4, v15}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 42
    .local v9, nl:Lorg/w3c/dom/NodeList;
    const/4 v12, 0x0

    .local v12, t:I
    :goto_35
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v15

    if-lt v12, v15, :cond_3c

    .line 117
    return-object v2

    .line 43
    :cond_3c
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 44
    .local v7, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v9, v12}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    check-cast v5, Lorg/w3c/dom/Element;

    .line 45
    .local v5, e:Lorg/w3c/dom/Element;
    const-string v15, "Url"

    invoke-virtual {v10, v5, v15}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 46
    .local v8, name:Ljava/lang/String;
    const-string v15, ".xml"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 47
    const-string v15, "_"

    const-string v16, " "

    move-object/from16 v0, v16

    invoke-virtual {v8, v15, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    .line 48
    const-string v15, "Url"

    invoke-virtual {v7, v15, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    move-object v11, v6

    .line 50
    .local v11, passed_name:Ljava/lang/String;
    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6f

    .line 51
    sput v12, Lcom/kuban/settings/ModListActivity;->menuposition:I

    .line 53
    :cond_6f
    const-string v15, "Icon"

    invoke-virtual {v10, v5, v15}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 54
    .local v13, value:Ljava/lang/String;
    const-string v15, "1"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_84

    .line 55
    const v15, 0x7f020021

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 57
    :cond_84
    const-string v15, "2"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_93

    .line 58
    const v15, 0x7f02002c

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 60
    :cond_93
    const-string v15, "3"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a2

    .line 61
    const v15, 0x7f02002e

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 63
    :cond_a2
    const-string v15, "4"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b1

    .line 64
    const v15, 0x7f02002f

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 66
    :cond_b1
    const-string v15, "5"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c0

    .line 67
    const v15, 0x7f020030

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 69
    :cond_c0
    const-string v15, "6"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_cf

    .line 70
    const v15, 0x7f020031

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 72
    :cond_cf
    const-string v15, "7"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_de

    .line 73
    const v15, 0x7f020032

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 75
    :cond_de
    const-string v15, "8"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_ed

    .line 76
    const v15, 0x7f020033

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 78
    :cond_ed
    const-string v15, "9"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_fc

    .line 79
    const v15, 0x7f020034

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 81
    :cond_fc
    const-string v15, "10"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_10b

    .line 82
    const v15, 0x7f020022

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 84
    :cond_10b
    const-string v15, "11"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_11a

    .line 85
    const v15, 0x7f020023

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 87
    :cond_11a
    const-string v15, "12"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_129

    .line 88
    const v15, 0x7f020024

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 90
    :cond_129
    const-string v15, "13"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_138

    .line 91
    const v15, 0x7f020025

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 93
    :cond_138
    const-string v15, "14"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_147

    .line 94
    const v15, 0x7f020026

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 96
    :cond_147
    const-string v15, "15"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_156

    .line 97
    const v15, 0x7f020027

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 99
    :cond_156
    const-string v15, "16"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_165

    .line 100
    const v15, 0x7f020028

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 102
    :cond_165
    const-string v15, "17"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_174

    .line 103
    const v15, 0x7f020029

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 105
    :cond_174
    const-string v15, "18"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_183

    .line 106
    const v15, 0x7f02002a

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 108
    :cond_183
    const-string v15, "19"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_192

    .line 109
    const v15, 0x7f02002b

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 111
    :cond_192
    const-string v15, "20"

    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1a1

    .line 112
    const v15, 0x7f02002d

    move-object/from16 v0, p0

    iput v15, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    .line 114
    :cond_1a1
    const-string v15, "Icon"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/kuban/settings/adapters/MenuAsync;->icon:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_35
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/adapters/MenuAsync;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    sput-object p1, Lcom/kuban/settings/ModListActivity;->dataFromAsyncTask:Ljava/util/ArrayList;

    .line 122
    sput-object p1, Lcom/kuban/settings/NewestMods;->dataFromAsyncTask:Ljava/util/ArrayList;

    .line 123
    return-void
.end method

.method protected onPreExecute()V
    .registers 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 28
    return-void
.end method
