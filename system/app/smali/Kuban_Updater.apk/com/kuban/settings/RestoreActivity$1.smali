.class Lcom/kuban/settings/RestoreActivity$1;
.super Ljava/lang/Object;
.source "RestoreActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/RestoreActivity;->ListDir(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/RestoreActivity;

.field private final synthetic val$dl_listview:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/kuban/settings/RestoreActivity;Landroid/widget/ListView;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/RestoreActivity$1;->this$0:Lcom/kuban/settings/RestoreActivity;

    iput-object p2, p0, Lcom/kuban/settings/RestoreActivity$1;->val$dl_listview:Landroid/widget/ListView;

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 15
    .parameter "v"

    .prologue
    .line 63
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 64
    .local v0, cntChoice:I
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v8

    .line 65
    .local v8, sparseBooleanArray:Landroid/util/SparseBooleanArray;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .local v6, s:Ljava/lang/StringBuilder;
    const-string v5, "install_zip(\""

    .line 67
    .local v5, prefix:Ljava/lang/String;
    const-string v9, "\");"

    .line 68
    .local v9, suffix:Ljava/lang/String;
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 69
    .local v7, selected:Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_20
    if-lt v3, v0, :cond_5a

    .line 79
    if-eqz v6, :cond_49

    .line 81
    :try_start_24
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "su -c /sbin/echo \'"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\' >> /cache/recovery/extendedcommand"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 82
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v10

    const-string v11, "su -c /sbin/reboot recovery"

    invoke-virtual {v10, v11}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_49} :catch_94

    .line 87
    :cond_49
    :goto_49
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->this$0:Lcom/kuban/settings/RestoreActivity;

    invoke-virtual {v10}, Lcom/kuban/settings/RestoreActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 88
    .local v4, intent:Landroid/content/Intent;
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->this$0:Lcom/kuban/settings/RestoreActivity;

    invoke-virtual {v10}, Lcom/kuban/settings/RestoreActivity;->finish()V

    .line 89
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->this$0:Lcom/kuban/settings/RestoreActivity;

    invoke-virtual {v10, v4}, Lcom/kuban/settings/RestoreActivity;->startActivity(Landroid/content/Intent;)V

    .line 90
    return-void

    .line 70
    .end local v4           #intent:Landroid/content/Intent;
    :cond_5a
    invoke-virtual {v8, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v10

    if-eqz v10, :cond_91

    .line 71
    iget-object v10, p0, Lcom/kuban/settings/RestoreActivity$1;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v10, v3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 72
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/kuban/settings/RestoreActivity$1;->this$0:Lcom/kuban/settings/RestoreActivity;

    iget-object v11, v11, Lcom/kuban/settings/RestoreActivity;->tozipdir:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, fileName:Ljava/lang/String;
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .end local v2           #fileName:Ljava/lang/String;
    :cond_91
    add-int/lit8 v3, v3, 0x1

    goto :goto_20

    .line 83
    :catch_94
    move-exception v1

    .line 84
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_49
.end method
