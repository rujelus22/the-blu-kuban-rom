.class public Lcom/kuban/settings/InstalledApplicationActivity;
.super Landroid/app/ListActivity;
.source "InstalledApplicationActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;,
        Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;
    }
.end annotation


# static fields
.field static final KEY_NAME:Ljava/lang/String; = "TabName"

.field private static appList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/kuban/settings/utils/AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static appListAdapter:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

.field private static selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Ljava/util/List;)V
    .registers 1
    .parameter

    .prologue
    .line 45
    sput-object p0, Lcom/kuban/settings/InstalledApplicationActivity;->appList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$1()Ljava/util/List;
    .registers 1

    .prologue
    .line 45
    sget-object v0, Lcom/kuban/settings/InstalledApplicationActivity;->appList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2(Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;)V
    .registers 1
    .parameter

    .prologue
    .line 47
    sput-object p0, Lcom/kuban/settings/InstalledApplicationActivity;->appListAdapter:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    return-void
.end method

.method static synthetic access$3()Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;
    .registers 1

    .prologue
    .line 47
    sget-object v0, Lcom/kuban/settings/InstalledApplicationActivity;->appListAdapter:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    return-object v0
.end method

.method static synthetic access$4()Lcom/kuban/settings/utils/AppInfo;
    .registers 1

    .prologue
    .line 46
    sget-object v0, Lcom/kuban/settings/InstalledApplicationActivity;->selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;

    return-object v0
.end method

.method public static deleteDirectory(Ljava/io/File;)V
    .registers 4
    .parameter "dir"

    .prologue
    .line 324
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "su -c /sbin/rm -r "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_16} :catch_17

    .line 327
    :goto_16
    return-void

    .line 326
    :catch_17
    move-exception v0

    goto :goto_16
.end method

.method private getVersion(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "string"

    .prologue
    .line 243
    const-string v1, "0"

    .line 245
    .local v1, version:Ljava/lang/String;
    :try_start_2
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 246
    .local v0, pInfo:Landroid/content/pm/PackageInfo;
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_d} :catch_e

    .line 248
    .end local v0           #pInfo:Landroid/content/pm/PackageInfo;
    :goto_d
    return-object v1

    .line 247
    :catch_e
    move-exception v2

    goto :goto_d
.end method


# virtual methods
.method public nokubandelete()V
    .registers 7

    .prologue
    .line 306
    :try_start_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 307
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const-string v2, "\nCan Not Uninstall\n\nThe Kuban Updater can not be uninstalled with The Kuban Updater\n"

    .line 309
    .local v2, msg:Ljava/lang/String;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 310
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 311
    const-string v4, "!! There is a problem !!"

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    .line 312
    const-string v4, "Ok"

    new-instance v5, Lcom/kuban/settings/InstalledApplicationActivity$8;

    invoke-direct {v5, p0}, Lcom/kuban/settings/InstalledApplicationActivity$8;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 317
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 318
    .local v1, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_27} :catch_28

    .line 321
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    .end local v1           #dialog:Landroid/app/AlertDialog;
    .end local v2           #msg:Ljava/lang/String;
    :goto_27
    return-void

    .line 320
    :catch_28
    move-exception v3

    goto :goto_27
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter "newConfig"

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 53
    const v0, 0x7f03000a

    invoke-virtual {p0, v0}, Lcom/kuban/settings/InstalledApplicationActivity;->setContentView(I)V

    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/kuban/settings/InstalledApplicationActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 55
    new-instance v0, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    invoke-direct {v0, p0}, Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/kuban/settings/InstalledApplicationActivity;->appListAdapter:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    .line 56
    sget-object v0, Lcom/kuban/settings/InstalledApplicationActivity;->appListAdapter:Lcom/kuban/settings/InstalledApplicationActivity$AppListAdapter;

    invoke-virtual {p0, v0}, Lcom/kuban/settings/InstalledApplicationActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 57
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 10
    .parameter "savedInstanceState"

    .prologue
    const/4 v7, 0x0

    .line 60
    const/4 v5, 0x5

    invoke-virtual {p0, v5}, Lcom/kuban/settings/InstalledApplicationActivity;->requestWindowFeature(I)Z

    .line 61
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 63
    const v5, 0x7f03000a

    invoke-virtual {p0, v5}, Lcom/kuban/settings/InstalledApplicationActivity;->setContentView(I)V

    .line 64
    const-string v5, "kuban.updater.parts"

    invoke-static {v5}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, URL:Ljava/lang/String;
    new-instance v3, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v3}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 66
    .local v3, parser:Lcom/kuban/settings/utils/XMLParser;
    invoke-virtual {v3, v0}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 67
    .local v4, xml:Ljava/lang/String;
    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5e

    .line 68
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 69
    .local v2, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v5, "Http Error!"

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 71
    const-string v5, "\nThere was an Http error in your request.\nPlease try again\n"

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 72
    invoke-virtual {v5, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 73
    const-string v6, "Retry"

    new-instance v7, Lcom/kuban/settings/InstalledApplicationActivity$1;

    invoke-direct {v7, p0}, Lcom/kuban/settings/InstalledApplicationActivity$1;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 80
    const-string v6, "Exit"

    new-instance v7, Lcom/kuban/settings/InstalledApplicationActivity$2;

    invoke-direct {v7, p0}, Lcom/kuban/settings/InstalledApplicationActivity$2;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 89
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 90
    .local v1, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 95
    .end local v1           #alertDialog:Landroid/app/AlertDialog;
    .end local v2           #alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    :goto_5d
    return-void

    .line 93
    :cond_5e
    new-instance v5, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;)V

    new-array v6, v7, [Ljava/lang/Void;

    invoke-virtual {v5, v6}, Lcom/kuban/settings/InstalledApplicationActivity$loadAppInfo;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_5d
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 332
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 333
    const/4 v1, 0x1

    return v1
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 14
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 187
    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 188
    sget-object v5, Lcom/kuban/settings/InstalledApplicationActivity;->appList:Ljava/util/List;

    invoke-interface {v5, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/kuban/settings/utils/AppInfo;

    sput-object v5, Lcom/kuban/settings/InstalledApplicationActivity;->selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;

    .line 189
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 190
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    sget-object v5, Lcom/kuban/settings/InstalledApplicationActivity;->selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;

    invoke-virtual {v5}, Lcom/kuban/settings/utils/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 191
    .local v2, fullapp:Ljava/lang/String;
    const-string v5, "/data/data/"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 192
    .local v4, pack:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\nVersion: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v4}, Lcom/kuban/settings/InstalledApplicationActivity;->getVersion(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Install Location:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/kuban/settings/InstalledApplicationActivity;->selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;

    invoke-virtual {v6}, Lcom/kuban/settings/utils/AppInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 193
    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Data Directory:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/data/data/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/kuban/settings/InstalledApplicationActivity;->selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;

    invoke-virtual {v6}, Lcom/kuban/settings/utils/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 192
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 194
    .local v3, msg:Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 195
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 196
    sget-object v6, Lcom/kuban/settings/InstalledApplicationActivity;->selectedAppInfo:Lcom/kuban/settings/utils/AppInfo;

    iget-object v6, v6, Lcom/kuban/settings/utils/AppInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 197
    const-string v6, "Launch"

    new-instance v7, Lcom/kuban/settings/InstalledApplicationActivity$3;

    invoke-direct {v7, p0}, Lcom/kuban/settings/InstalledApplicationActivity$3;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 210
    const-string v6, "Uninstall"

    new-instance v7, Lcom/kuban/settings/InstalledApplicationActivity$4;

    invoke-direct {v7, p0}, Lcom/kuban/settings/InstalledApplicationActivity$4;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    .line 234
    const-string v6, "Cancel"

    new-instance v7, Lcom/kuban/settings/InstalledApplicationActivity$5;

    invoke-direct {v7, p0}, Lcom/kuban/settings/InstalledApplicationActivity$5;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 240
    .local v1, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 241
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 338
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 339
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 340
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 341
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 342
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 343
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 410
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :goto_2b
    return v3

    .line 347
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4e

    .line 348
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 349
    .local v2, in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 351
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 354
    .end local v2           #in:Landroid/content/Intent;
    :cond_4e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_70

    .line 355
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 356
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 358
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 361
    .end local v2           #in:Landroid/content/Intent;
    :cond_70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-ne v4, v5, :cond_92

    .line 362
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 363
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Change Log"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 364
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 365
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 368
    .end local v2           #in:Landroid/content/Intent;
    :cond_92
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_b5

    .line 369
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 370
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 372
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 375
    .end local v2           #in:Landroid/content/Intent;
    :cond_b5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_d8

    .line 376
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 379
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 382
    .end local v2           #in:Landroid/content/Intent;
    :cond_d8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_fb

    .line 383
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 384
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 386
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 389
    .end local v2           #in:Landroid/content/Intent;
    :cond_fb
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_11e

    .line 390
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 391
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 392
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 393
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 396
    .end local v2           #in:Landroid/content/Intent;
    :cond_11e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_141

    .line 397
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 398
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "TabName"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 400
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 403
    .end local v2           #in:Landroid/content/Intent;
    :cond_141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x102002c

    if-ne v4, v5, :cond_162

    .line 404
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/Home;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 405
    .restart local v2       #in:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 406
    invoke-virtual {p0}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 407
    invoke-virtual {p0, v2}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 410
    .end local v2           #in:Landroid/content/Intent;
    :cond_162
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method

.method protected systemPopUp()V
    .registers 11

    .prologue
    const v9, 0x7f020045

    .line 253
    const-string v7, "layout_inflater"

    invoke-virtual {p0, v7}, Lcom/kuban/settings/InstalledApplicationActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 254
    .local v4, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f030016

    const v7, 0x7f0c0044

    invoke-virtual {p0, v7}, Lcom/kuban/settings/InstalledApplicationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v4, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 255
    .local v5, layout:Landroid/view/View;
    const v7, 0x7f0c0033

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 256
    .local v6, text:Landroid/widget/TextView;
    const/16 v7, 0x11

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setGravity(I)V

    .line 257
    const-string v7, "This is a system app.\n\nAre you sure you want to delete it?\n\nThis can not be undone easily."

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    const v7, 0x7f0c0005

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 259
    .local v2, image:Landroid/widget/ImageView;
    invoke-virtual {v2, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 260
    const v7, 0x7f0c0041

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 261
    .local v3, image2:Landroid/widget/ImageView;
    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 262
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 263
    .local v1, builder:Landroid/app/AlertDialog$Builder;
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    .line 264
    const-string v8, "Uninstall"

    new-instance v9, Lcom/kuban/settings/InstalledApplicationActivity$6;

    invoke-direct {v9, p0}, Lcom/kuban/settings/InstalledApplicationActivity$6;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    .line 295
    const-string v8, "Cancel"

    new-instance v9, Lcom/kuban/settings/InstalledApplicationActivity$7;

    invoke-direct {v9, p0}, Lcom/kuban/settings/InstalledApplicationActivity$7;-><init>(Lcom/kuban/settings/InstalledApplicationActivity;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 300
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 301
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 302
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 303
    return-void
.end method
