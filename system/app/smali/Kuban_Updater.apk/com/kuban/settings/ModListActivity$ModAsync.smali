.class Lcom/kuban/settings/ModListActivity$ModAsync;
.super Landroid/os/AsyncTask;
.source "ModListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/ModListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/ModListActivity;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/ModListActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 170
    iput-object p1, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/ModListActivity;Lcom/kuban/settings/ModListActivity$ModAsync;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/kuban/settings/ModListActivity$ModAsync;-><init>(Lcom/kuban/settings/ModListActivity;)V

    return-void
.end method

.method static synthetic access$3(Lcom/kuban/settings/ModListActivity$ModAsync;)Lcom/kuban/settings/ModListActivity;
    .registers 2
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    return-object v0
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/ModListActivity$ModAsync;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .registers 26
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/kuban/settings/ModListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v12

    .line 178
    .local v12, in:Landroid/content/Intent;
    const-string v21, "kuban.updater.parts"

    invoke-static/range {v21 .. v21}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, BASEURL:Ljava/lang/String;
    if-eqz v3, :cond_15

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 181
    :cond_15
    const/16 v21, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v22

    add-int/lit8 v22, v22, -0x8

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 182
    const-string v21, "Url"

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 183
    .local v16, passed_url:Ljava/lang/String;
    const-string v21, "%s%s"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v3, v22, v23

    const/16 v23, 0x1

    aput-object v16, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 184
    .local v18, query:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/kuban/settings/ModListActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 185
    .local v17, prefs:Landroid/content/SharedPreferences;
    const-string v21, "downloadDir"

    const-string v22, "kubandownloads"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 186
    .local v6, downloadFolder:Ljava/lang/String;
    const-string v21, "daydiff"

    const-string v22, "604800000"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 187
    .local v4, daysdiff:Ljava/lang/String;
    new-instance v15, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v15}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 188
    .local v15, parser:Lcom/kuban/settings/utils/XMLParser;
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 189
    .local v20, xml:Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v5

    .line 190
    .local v5, doc:Lorg/w3c/dom/Document;
    const-string v21, "Tab"

    move-object/from16 v0, v21

    invoke-interface {v5, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 191
    .local v8, grid:Lorg/w3c/dom/NodeList;
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    .line 192
    .local v9, grid2:I
    if-lez v9, :cond_33b

    .line 193
    const-string v21, "Tab"

    move-object/from16 v0, v21

    invoke-interface {v5, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 194
    const/4 v11, 0x0

    .local v11, i2:I
    :goto_95
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    move/from16 v0, v21

    if-lt v11, v0, :cond_aa

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    .line 283
    .end local v11           #i2:I
    :goto_a9
    return-object v21

    .line 195
    .restart local v11       #i2:I
    :cond_aa
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 196
    .local v13, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v8, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    check-cast v7, Lorg/w3c/dom/Element;

    .line 197
    .local v7, e:Lorg/w3c/dom/Element;
    const-string v21, "Icon"

    move-object/from16 v0, v21

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 198
    .local v19, value:Ljava/lang/String;
    const-string v21, "1"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d8

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020021

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 201
    :cond_d8
    const-string v21, "2"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f3

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f02002c

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 204
    :cond_f3
    const-string v21, "3"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10e

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f02002e

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 207
    :cond_10e
    const-string v21, "4"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_129

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f02002f

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 210
    :cond_129
    const-string v21, "5"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_144

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020030

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 213
    :cond_144
    const-string v21, "6"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_15f

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020031

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 216
    :cond_15f
    const-string v21, "7"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_17a

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020032

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 219
    :cond_17a
    const-string v21, "8"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_195

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020033

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 222
    :cond_195
    const-string v21, "9"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1b0

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020034

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 225
    :cond_1b0
    const-string v21, "10"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1cb

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020022

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 228
    :cond_1cb
    const-string v21, "11"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1e6

    .line 229
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020023

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 231
    :cond_1e6
    const-string v21, "12"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_201

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020024

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 234
    :cond_201
    const-string v21, "13"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_21c

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020025

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 237
    :cond_21c
    const-string v21, "14"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_237

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020026

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 240
    :cond_237
    const-string v21, "15"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_252

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020027

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 243
    :cond_252
    const-string v21, "16"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_26d

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020028

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 246
    :cond_26d
    const-string v21, "17"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_288

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f020029

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 249
    :cond_288
    const-string v21, "18"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2a3

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f02002a

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 252
    :cond_2a3
    const-string v21, "19"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2be

    .line 253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f02002b

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 255
    :cond_2be
    const-string v21, "20"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2d9

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const v22, 0x7f02002d

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/kuban/settings/ModListActivity;->icon:I

    .line 258
    :cond_2d9
    const-string v21, "TabName"

    const-string v22, "TabName"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    const-string v21, "Url"

    const-string v22, "Url"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    const-string v21, "Icon"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/kuban/settings/ModListActivity;->icon:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-static/range {v22 .. v22}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    iput-object v0, v1, Lcom/kuban/settings/ModListActivity;->gridmenu:Ljava/lang/Boolean;

    .line 194
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_95

    .line 266
    .end local v7           #e:Lorg/w3c/dom/Element;
    .end local v11           #i2:I
    .end local v13           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v19           #value:Ljava/lang/String;
    :cond_33b
    const-string v21, "Element"

    move-object/from16 v0, v21

    invoke-interface {v5, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v14

    .line 267
    .local v14, nl:Lorg/w3c/dom/NodeList;
    const/4 v10, 0x0

    .local v10, i:I
    :goto_344
    invoke-interface {v14}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    move/from16 v0, v21

    if-lt v10, v0, :cond_35a

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    goto/16 :goto_a9

    .line 268
    :cond_35a
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 269
    .restart local v13       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14, v10}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    check-cast v7, Lorg/w3c/dom/Element;

    .line 270
    .restart local v7       #e:Lorg/w3c/dom/Element;
    const-string v21, "Name"

    const-string v22, "Name"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    const-string v21, "Desc"

    const-string v22, "Desc"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    const-string v21, "Url"

    const-string v22, "Url"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    const-string v21, "Date"

    const-string v22, "Date"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    const-string v21, "No"

    const-string v22, "No"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    const-string v21, "ThumbnailUrl"

    const-string v22, "ThumbnailUrl"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    const-string v21, "Preview"

    const-string v22, "Preview"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    const-string v21, "Preview2"

    const-string v22, "Preview2"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    const-string v21, "Preview3"

    const-string v22, "Preview3"

    move-object/from16 v0, v22

    invoke-virtual {v15, v7, v0}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v13, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    const-string v21, "Folder"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    const-string v21, "604800000"

    move-object/from16 v0, v21

    invoke-virtual {v13, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_344
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/ModListActivity$ModAsync;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 16
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v3, 0x3

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 288
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v0, v6}, Lcom/kuban/settings/ModListActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 289
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->gridmenu:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_72

    .line 290
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    const v1, 0x7f03000d

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ModListActivity;->setContentView(I)V

    .line 291
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v0}, Lcom/kuban/settings/ModListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 292
    .local v8, in:Landroid/content/Intent;
    const-string v0, "TabName"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 293
    .local v10, passed_name:Ljava/lang/String;
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 294
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    const v1, 0x7f0c0022

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ModListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/GridView;

    .line 295
    .local v7, gridView:Landroid/widget/GridView;
    iget-object v11, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    new-instance v0, Lcom/kuban/settings/ModListActivity$ModAsync$1;

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v1}, Lcom/kuban/settings/ModListActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f030008

    .line 296
    new-array v5, v3, [Ljava/lang/String;

    const-string v1, "TabName"

    aput-object v1, v5, v6

    const-string v1, "Url"

    aput-object v1, v5, v12

    const-string v1, "Icon"

    aput-object v1, v5, v13

    .line 297
    new-array v6, v3, [I

    fill-array-data v6, :array_f8

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v6}, Lcom/kuban/settings/ModListActivity$ModAsync$1;-><init>(Lcom/kuban/settings/ModListActivity$ModAsync;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 295
    iput-object v0, v11, Lcom/kuban/settings/ModListActivity;->gridadapter:Landroid/widget/SimpleAdapter;

    .line 304
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->gridadapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 305
    new-instance v0, Lcom/kuban/settings/ModListActivity$ModAsync$2;

    invoke-direct {v0, p0}, Lcom/kuban/settings/ModListActivity$ModAsync$2;-><init>(Lcom/kuban/settings/ModListActivity$ModAsync;)V

    invoke-virtual {v7, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 380
    .end local v7           #gridView:Landroid/widget/GridView;
    .end local v8           #in:Landroid/content/Intent;
    .end local v10           #passed_name:Ljava/lang/String;
    :goto_71
    return-void

    .line 318
    :cond_72
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 319
    iget-object v11, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    new-instance v0, Lcom/kuban/settings/ModListActivity$ModAsync$3;

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v1}, Lcom/kuban/settings/ModListActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/kuban/settings/ModListActivity;->dataFromAsyncTask:Ljava/util/ArrayList;

    const v4, 0x7f030007

    .line 320
    new-array v5, v13, [Ljava/lang/String;

    const-string v1, "Url"

    aput-object v1, v5, v6

    const-string v1, "Icon"

    aput-object v1, v5, v12

    .line 321
    new-array v6, v13, [I

    fill-array-data v6, :array_102

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/kuban/settings/ModListActivity$ModAsync$3;-><init>(Lcom/kuban/settings/ModListActivity$ModAsync;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 319
    iput-object v0, v11, Lcom/kuban/settings/ModListActivity;->menuadapter:Landroid/widget/SimpleAdapter;

    .line 328
    new-instance v9, Lcom/kuban/settings/ModListActivity$ModAsync$4;

    invoke-direct {v9, p0}, Lcom/kuban/settings/ModListActivity$ModAsync$4;-><init>(Lcom/kuban/settings/ModListActivity$ModAsync;)V

    .line 347
    .local v9, navigationListener:Landroid/app/ActionBar$OnNavigationListener;
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v1, v1, Lcom/kuban/settings/ModListActivity;->menuadapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1, v9}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 348
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-virtual {v0}, Lcom/kuban/settings/ModListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v1, Lcom/kuban/settings/ModListActivity;->menuposition:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 349
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    const v1, 0x7f03000e

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ModListActivity;->setContentView(I)V

    .line 350
    iget-object v1, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    const v2, 0x7f0c0023

    invoke-virtual {v0, v2}, Lcom/kuban/settings/ModListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, v1, Lcom/kuban/settings/ModListActivity;->list:Landroid/widget/ListView;

    .line 351
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    new-instance v1, Lcom/kuban/settings/adapters/ListItemAdapter;

    iget-object v2, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    invoke-direct {v1, v2, p1}, Lcom/kuban/settings/adapters/ListItemAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v1, v0, Lcom/kuban/settings/ModListActivity;->mainadapter:Lcom/kuban/settings/adapters/ListItemAdapter;

    .line 352
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v1, v1, Lcom/kuban/settings/ModListActivity;->mainadapter:Lcom/kuban/settings/adapters/ListItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 353
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    iget-object v0, v0, Lcom/kuban/settings/ModListActivity;->list:Landroid/widget/ListView;

    new-instance v1, Lcom/kuban/settings/ModListActivity$ModAsync$5;

    invoke-direct {v1, p0}, Lcom/kuban/settings/ModListActivity$ModAsync$5;-><init>(Lcom/kuban/settings/ModListActivity$ModAsync;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_71

    .line 297
    nop

    :array_f8
    .array-data 0x4
        0x1at 0x0t 0xct 0x7ft
        0x1ct 0x0t 0xct 0x7ft
        0x19t 0x0t 0xct 0x7ft
    .end array-data

    .line 321
    :array_102
    .array-data 0x4
        0x15t 0x0t 0xct 0x7ft
        0x18t 0x0t 0xct 0x7ft
    .end array-data
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 172
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 173
    iget-object v0, p0, Lcom/kuban/settings/ModListActivity$ModAsync;->this$0:Lcom/kuban/settings/ModListActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ModListActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 174
    return-void
.end method
