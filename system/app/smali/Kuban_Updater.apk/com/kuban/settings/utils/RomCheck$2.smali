.class Lcom/kuban/settings/utils/RomCheck$2;
.super Ljava/util/TimerTask;
.source "RomCheck.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/utils/RomCheck;->settingCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/utils/RomCheck;

.field private final synthetic val$timer2:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/kuban/settings/utils/RomCheck;Ljava/util/Timer;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iput-object p2, p0, Lcom/kuban/settings/utils/RomCheck$2;->val$timer2:Ljava/util/Timer;

    .line 128
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .line 131
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/RomCheck;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 132
    .local v0, prefs:Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    const-string v2, "romCheck_Interval"

    const-string v3, "54000000"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/kuban/settings/utils/RomCheck;->RomCheckInterval:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iget-object v2, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iget-object v2, v2, Lcom/kuban/settings/utils/RomCheck;->RomCheckInterval:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/kuban/settings/utils/RomCheck;->RomCheckIntervalTime:I

    .line 134
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    const-string v2, "romCheck"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v1, Lcom/kuban/settings/utils/RomCheck;->RomCheckOn:Z

    .line 135
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    iget-boolean v1, v1, Lcom/kuban/settings/utils/RomCheck;->RomCheckOn:Z

    if-eqz v1, :cond_3d

    .line 136
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->this$0:Lcom/kuban/settings/utils/RomCheck;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/RomCheck;->checkTimer()V

    .line 137
    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck$2;->val$timer2:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 139
    :cond_3d
    return-void
.end method
