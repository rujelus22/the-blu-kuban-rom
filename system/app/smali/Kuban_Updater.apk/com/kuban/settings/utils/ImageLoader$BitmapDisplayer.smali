.class Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/utils/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BitmapDisplayer"
.end annotation


# instance fields
.field bitmap:Landroid/graphics/Bitmap;

.field photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

.field final synthetic this$0:Lcom/kuban/settings/utils/ImageLoader;


# direct methods
.method public constructor <init>(Lcom/kuban/settings/utils/ImageLoader;Landroid/graphics/Bitmap;Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)V
    .registers 4
    .parameter
    .parameter "b"
    .parameter "p"

    .prologue
    .line 130
    iput-object p1, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->bitmap:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    iget-object v1, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/ImageLoader;->imageViewReused(Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 138
    :goto_a
    return-void

    .line 134
    :cond_b
    iget-object v0, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_19

    .line 135
    iget-object v0, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    iget-object v0, v0, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->imageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_a

    .line 137
    :cond_19
    iget-object v0, p0, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    iget-object v0, v0, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->imageView:Landroid/widget/ImageView;

    const v1, 0x7f02001f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_a
.end method
