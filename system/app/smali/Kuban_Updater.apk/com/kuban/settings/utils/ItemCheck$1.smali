.class Lcom/kuban/settings/utils/ItemCheck$1;
.super Ljava/lang/Object;
.source "ItemCheck.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/utils/ItemCheck;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/utils/ItemCheck;


# direct methods
.method constructor <init>(Lcom/kuban/settings/utils/ItemCheck;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 78
    iget-object v1, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/ItemCheck;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 79
    .local v0, prefs:Landroid/content/SharedPreferences;
    iget-object v1, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    const-string v2, "itemCheck_Interval"

    const-string v3, "54000000"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/kuban/settings/utils/ItemCheck;->ItemCheckInterval:Ljava/lang/String;

    .line 80
    iget-object v1, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    iget-object v2, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    iget-object v2, v2, Lcom/kuban/settings/utils/ItemCheck;->ItemCheckInterval:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/kuban/settings/utils/ItemCheck;->ItemCheckIntervalTime:I

    .line 81
    iget-object v1, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    invoke-virtual {v1}, Lcom/kuban/settings/utils/ItemCheck;->checkTimer()V

    .line 82
    iget-object v1, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    #getter for: Lcom/kuban/settings/utils/ItemCheck;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/kuban/settings/utils/ItemCheck;->access$0(Lcom/kuban/settings/utils/ItemCheck;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    #getter for: Lcom/kuban/settings/utils/ItemCheck;->runnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/kuban/settings/utils/ItemCheck;->access$1(Lcom/kuban/settings/utils/ItemCheck;)Ljava/lang/Runnable;

    move-result-object v2

    iget-object v3, p0, Lcom/kuban/settings/utils/ItemCheck$1;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    iget v3, v3, Lcom/kuban/settings/utils/ItemCheck;->ItemCheckIntervalTime:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    return-void
.end method
