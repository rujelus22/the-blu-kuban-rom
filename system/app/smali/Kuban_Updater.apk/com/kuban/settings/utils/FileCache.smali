.class public Lcom/kuban/settings/utils/FileCache;
.super Ljava/lang/Object;
.source "FileCache.java"


# instance fields
.field private cacheDir:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 12
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    const-string v2, ".KubanCache"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/kuban/settings/utils/FileCache;->cacheDir:Ljava/io/File;

    .line 15
    :goto_1c
    iget-object v0, p0, Lcom/kuban/settings/utils/FileCache;->cacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_29

    .line 16
    iget-object v0, p0, Lcom/kuban/settings/utils/FileCache;->cacheDir:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 17
    :cond_29
    return-void

    .line 14
    :cond_2a
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/FileCache;->cacheDir:Ljava/io/File;

    goto :goto_1c
.end method


# virtual methods
.method public clear()V
    .registers 5

    .prologue
    .line 24
    iget-object v2, p0, Lcom/kuban/settings/utils/FileCache;->cacheDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 25
    .local v1, files:[Ljava/io/File;
    if-nez v1, :cond_9

    .line 29
    :cond_8
    return-void

    .line 27
    :cond_9
    array-length v3, v1

    const/4 v2, 0x0

    :goto_b
    if-ge v2, v3, :cond_8

    aget-object v0, v1, v2

    .line 28
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 27
    add-int/lit8 v2, v2, 0x1

    goto :goto_b
.end method

.method public getFile(Ljava/lang/String;)Ljava/io/File;
    .registers 5
    .parameter "url"

    .prologue
    .line 19
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 20
    .local v1, filename:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/kuban/settings/utils/FileCache;->cacheDir:Ljava/io/File;

    invoke-direct {v0, v2, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 21
    .local v0, f:Ljava/io/File;
    return-object v0
.end method
