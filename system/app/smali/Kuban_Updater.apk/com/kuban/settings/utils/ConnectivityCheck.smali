.class public Lcom/kuban/settings/utils/ConnectivityCheck;
.super Ljava/lang/Object;
.source "ConnectivityCheck.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized isNetAvailable(Landroid/content/Context;)Z
    .registers 15
    .parameter "context"

    .prologue
    .line 11
    const-class v13, Lcom/kuban/settings/utils/ConnectivityCheck;

    monitor-enter v13

    const/4 v0, 0x0

    .line 12
    .local v0, isNetAvailable:Z
    if-eqz p0, :cond_5c

    .line 13
    :try_start_6
    const-string v12, "connectivity"

    invoke-virtual {p0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 14
    .local v1, mgr:Landroid/net/ConnectivityManager;
    if-eqz v1, :cond_5c

    .line 15
    const/4 v3, 0x0

    .line 16
    .local v3, mobileNetwork:Z
    const/4 v10, 0x0

    .line 17
    .local v10, wifiNetwork:Z
    const/4 v7, 0x0

    .line 18
    .local v7, wiMaxNetwork:Z
    const/4 v4, 0x0

    .line 19
    .local v4, mobileNetworkConnecetd:Z
    const/4 v11, 0x0

    .line 20
    .local v11, wifiNetworkConnecetd:Z
    const/4 v8, 0x0

    .line 21
    .local v8, wiMaxNetworkConnected:Z
    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 22
    .local v2, mobileInfo:Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v5

    .line 23
    .local v5, mobileSubtype:I
    const/4 v12, 0x1

    invoke-virtual {v1, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v9

    .line 24
    .local v9, wifiInfo:Landroid/net/NetworkInfo;
    const/4 v12, 0x6

    invoke-virtual {v1, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v6

    .line 25
    .local v6, wiMaxInfo:Landroid/net/NetworkInfo;
    if-eqz v2, :cond_2f

    .line 26
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v3

    .line 27
    :cond_2f
    if-eqz v9, :cond_35

    .line 28
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v10

    .line 29
    :cond_35
    if-eqz v6, :cond_3b

    .line 30
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v7

    .line 31
    :cond_3b
    if-nez v10, :cond_41

    if-nez v3, :cond_41

    if-eqz v7, :cond_4d

    .line 32
    :cond_41
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    .line 33
    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v11

    .line 34
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    .line 36
    :cond_4d
    sput-boolean v11, Lcom/kuban/settings/Home;->wifi:Z

    .line 37
    sput-boolean v4, Lcom/kuban/settings/Home;->mobile:Z

    .line 38
    sput v5, Lcom/kuban/settings/Home;->mobileType:I

    .line 39
    sput-boolean v8, Lcom/kuban/settings/Home;->wimax:Z
    :try_end_55
    .catchall {:try_start_6 .. :try_end_55} :catchall_60

    .line 40
    if-nez v4, :cond_5e

    if-nez v11, :cond_5e

    if-nez v8, :cond_5e

    const/4 v0, 0x0

    .line 43
    .end local v1           #mgr:Landroid/net/ConnectivityManager;
    .end local v2           #mobileInfo:Landroid/net/NetworkInfo;
    .end local v3           #mobileNetwork:Z
    .end local v4           #mobileNetworkConnecetd:Z
    .end local v5           #mobileSubtype:I
    .end local v6           #wiMaxInfo:Landroid/net/NetworkInfo;
    .end local v7           #wiMaxNetwork:Z
    .end local v8           #wiMaxNetworkConnected:Z
    .end local v9           #wifiInfo:Landroid/net/NetworkInfo;
    .end local v10           #wifiNetwork:Z
    .end local v11           #wifiNetworkConnecetd:Z
    :cond_5c
    :goto_5c
    monitor-exit v13

    return v0

    .line 40
    .restart local v1       #mgr:Landroid/net/ConnectivityManager;
    .restart local v2       #mobileInfo:Landroid/net/NetworkInfo;
    .restart local v3       #mobileNetwork:Z
    .restart local v4       #mobileNetworkConnecetd:Z
    .restart local v5       #mobileSubtype:I
    .restart local v6       #wiMaxInfo:Landroid/net/NetworkInfo;
    .restart local v7       #wiMaxNetwork:Z
    .restart local v8       #wiMaxNetworkConnected:Z
    .restart local v9       #wifiInfo:Landroid/net/NetworkInfo;
    .restart local v10       #wifiNetwork:Z
    .restart local v11       #wifiNetworkConnecetd:Z
    :cond_5e
    const/4 v0, 0x1

    goto :goto_5c

    .line 11
    .end local v1           #mgr:Landroid/net/ConnectivityManager;
    .end local v2           #mobileInfo:Landroid/net/NetworkInfo;
    .end local v3           #mobileNetwork:Z
    .end local v4           #mobileNetworkConnecetd:Z
    .end local v5           #mobileSubtype:I
    .end local v6           #wiMaxInfo:Landroid/net/NetworkInfo;
    .end local v7           #wiMaxNetwork:Z
    .end local v8           #wiMaxNetworkConnected:Z
    .end local v9           #wifiInfo:Landroid/net/NetworkInfo;
    .end local v10           #wifiNetwork:Z
    .end local v11           #wifiNetworkConnecetd:Z
    :catchall_60
    move-exception v12

    monitor-exit v13

    throw v12
.end method
