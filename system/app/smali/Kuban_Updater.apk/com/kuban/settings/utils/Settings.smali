.class public Lcom/kuban/settings/utils/Settings;
.super Landroid/preference/PreferenceActivity;
.source "Settings.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    invoke-virtual {p0}, Lcom/kuban/settings/utils/Settings;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 17
    const/high16 v0, 0x7f05

    invoke-virtual {p0, v0}, Lcom/kuban/settings/utils/Settings;->addPreferencesFromResource(I)V

    .line 18
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 21
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/utils/Settings;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kuban/settings/Home;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 22
    .local v0, in:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 23
    invoke-virtual {p0}, Lcom/kuban/settings/utils/Settings;->finish()V

    .line 24
    invoke-virtual {p0, v0}, Lcom/kuban/settings/utils/Settings;->startActivity(Landroid/content/Intent;)V

    .line 25
    const/4 v1, 0x1

    return v1
.end method
