.class Lcom/kuban/settings/utils/ItemCheck$getItems;
.super Landroid/os/AsyncTask;
.source "ItemCheck.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/utils/ItemCheck;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "getItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field URL:Ljava/lang/String;

.field downloadFolder:Ljava/lang/String;

.field menuItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field parser:Lcom/kuban/settings/utils/XMLParser;

.field prefs:Landroid/content/SharedPreferences;

.field final synthetic this$0:Lcom/kuban/settings/utils/ItemCheck;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/utils/ItemCheck;)V
    .registers 5
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 100
    const-string v0, "kuban.updater.parts"

    invoke-static {v0}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->URL:Ljava/lang/String;

    .line 101
    new-instance v0, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v0}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    .line 102
    invoke-virtual {p1}, Lcom/kuban/settings/utils/ItemCheck;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->prefs:Landroid/content/SharedPreferences;

    .line 103
    iget-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "downloadDir"

    const-string v2, "kubandownloads"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->downloadFolder:Ljava/lang/String;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->menuItems:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/utils/ItemCheck;Lcom/kuban/settings/utils/ItemCheck$getItems;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/kuban/settings/utils/ItemCheck$getItems;-><init>(Lcom/kuban/settings/utils/ItemCheck;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/utils/ItemCheck$getItems;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .registers 34
    .parameter "urls"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 108
    :try_start_0
    const-string v26, "kuban.updater.parts"

    invoke-static/range {v26 .. v26}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, BASEURL:Ljava/lang/String;
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 111
    :cond_b
    const/16 v26, 0x0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v27

    add-int/lit8 v27, v27, -0x8

    move/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->URL:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 113
    .local v25, xml:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v10

    .line 114
    .local v10, doc:Lorg/w3c/dom/Document;
    const-string v26, "Tab"

    move-object/from16 v0, v26

    invoke-interface {v10, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v20

    .line 115
    .local v20, nl:Lorg/w3c/dom/NodeList;
    new-instance v21, Ljava/text/SimpleDateFormat;

    const-string v26, "MM-dd-yyyy"

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 116
    .local v21, sdf:Ljava/text/SimpleDateFormat;
    new-instance v14, Ljava/util/Date;

    invoke-direct {v14}, Ljava/util/Date;-><init>()V

    .line 117
    .local v14, itemDate:Ljava/util/Date;
    const/16 v22, 0x0

    .local v22, t:I
    :goto_53
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v26

    move/from16 v0, v22

    move/from16 v1, v26

    if-lt v0, v1, :cond_7a

    .line 124
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_67
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z
    :try_end_6a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_6a} :catch_2bd

    move-result v27

    if-nez v27, :cond_c3

    .line 167
    .end local v3           #BASEURL:Ljava/lang/String;
    .end local v10           #doc:Lorg/w3c/dom/Document;
    .end local v14           #itemDate:Ljava/util/Date;
    .end local v20           #nl:Lorg/w3c/dom/NodeList;
    .end local v21           #sdf:Ljava/text/SimpleDateFormat;
    .end local v22           #t:I
    .end local v25           #xml:Ljava/lang/String;
    :goto_6d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck;->Items:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    return-object v26

    .line 118
    .restart local v3       #BASEURL:Ljava/lang/String;
    .restart local v10       #doc:Lorg/w3c/dom/Document;
    .restart local v14       #itemDate:Ljava/util/Date;
    .restart local v20       #nl:Lorg/w3c/dom/NodeList;
    .restart local v21       #sdf:Ljava/text/SimpleDateFormat;
    .restart local v22       #t:I
    .restart local v25       #xml:Ljava/lang/String;
    :cond_7a
    :try_start_7a
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 119
    .local v18, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    check-cast v11, Lorg/w3c/dom/Element;

    .line 120
    .local v11, e:Lorg/w3c/dom/Element;
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v27, v0

    const-string v28, "Url"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v0, v11, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 121
    .local v5, NEWURL:Ljava/lang/String;
    const-string v26, "Url"

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->menuItems:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    add-int/lit8 v22, v22, 0x1

    goto :goto_53

    .line 124
    .end local v5           #NEWURL:Ljava/lang/String;
    .end local v11           #e:Lorg/w3c/dom/Element;
    .end local v18           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_c3
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/util/HashMap;

    .line 125
    .restart local v18       #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v27

    invoke-interface/range {v27 .. v27}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :cond_d1
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_67

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 126
    .local v4, FINALURL:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v10

    .line 128
    const-string v28, "Element"

    move-object/from16 v0, v28

    invoke-interface {v10, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v20

    .line 129
    const/4 v13, 0x0

    .local v13, i:I
    :goto_100
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v28

    move/from16 v0, v28

    if-ge v13, v0, :cond_d1

    .line 130
    new-instance v19, Ljava/util/HashMap;

    invoke-direct/range {v19 .. v19}, Ljava/util/HashMap;-><init>()V

    .line 131
    .local v19, map3:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    .line 132
    .local v12, e2:Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v28, v0

    const-string v29, "Date"

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 133
    .local v15, itemdate:Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v14

    .line 134
    invoke-virtual {v14}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 135
    .local v6, date:J
    const/16 v24, 0x0

    .line 136
    .local v24, timefinal:Ljava/lang/String;
    const/4 v9, 0x0

    .line 137
    .local v9, datefinal:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Lcom/kuban/settings/utils/ItemCheck;->time:J

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v23

    .line 138
    .local v23, time2:Ljava/lang/String;
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 139
    .local v8, date2:Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x6

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_16a

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_16a

    .line 140
    const/16 v28, 0x0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v29

    add-int/lit8 v29, v29, -0x6

    move-object/from16 v0, v23

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    .line 142
    :cond_16a
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x6

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_18c

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v28

    if-nez v28, :cond_18c

    .line 143
    const/16 v28, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v29

    add-int/lit8 v29, v29, -0x6

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 145
    :cond_18c
    invoke-static/range {v24 .. v24}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 146
    .local v17, longtime:Ljava/lang/Long;
    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    .line 147
    .local v16, longdate:Ljava/lang/Long;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    cmp-long v28, v28, v30

    if-gez v28, :cond_2b9

    .line 148
    const-string v28, "Name"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Name"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const-string v28, "Desc"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Desc"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const-string v28, "Url"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Url"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v28, "Date"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Date"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v28, "No"

    const-string v29, "No"

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    const-string v28, "ThumbnailUrl"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "ThumbnailUrl"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    const-string v28, "Preview"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Preview"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    const-string v28, "Preview2"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Preview2"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const-string v28, "Preview3"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->parser:Lcom/kuban/settings/utils/XMLParser;

    move-object/from16 v29, v0

    const-string v30, "Preview3"

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const-string v28, "Folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->downloadFolder:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck;->Items:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2b9
    .catch Ljava/lang/Exception; {:try_start_7a .. :try_end_2b9} :catch_2bd

    .line 129
    :cond_2b9
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_100

    .line 164
    .end local v3           #BASEURL:Ljava/lang/String;
    .end local v4           #FINALURL:Ljava/lang/String;
    .end local v6           #date:J
    .end local v8           #date2:Ljava/lang/String;
    .end local v9           #datefinal:Ljava/lang/String;
    .end local v10           #doc:Lorg/w3c/dom/Document;
    .end local v12           #e2:Lorg/w3c/dom/Element;
    .end local v13           #i:I
    .end local v14           #itemDate:Ljava/util/Date;
    .end local v15           #itemdate:Ljava/lang/String;
    .end local v16           #longdate:Ljava/lang/Long;
    .end local v17           #longtime:Ljava/lang/Long;
    .end local v18           #map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v19           #map3:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v20           #nl:Lorg/w3c/dom/NodeList;
    .end local v21           #sdf:Ljava/text/SimpleDateFormat;
    .end local v22           #t:I
    .end local v23           #time2:Ljava/lang/String;
    .end local v24           #timefinal:Ljava/lang/String;
    .end local v25           #xml:Ljava/lang/String;
    :catch_2bd
    move-exception v11

    .line 165
    .local v11, e:Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/kuban/settings/utils/ItemCheck;->TAG:Ljava/lang/String;

    move-object/from16 v26, v0

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Something went wrong on the NewItem Check: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6d
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/utils/ItemCheck$getItems;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v1, 0x1

    .line 171
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 173
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_13

    .line 174
    iget-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    invoke-virtual {v0, p1}, Lcom/kuban/settings/utils/ItemCheck;->notificationsingle(Ljava/util/ArrayList;)V

    .line 179
    :cond_f
    :goto_f
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 180
    return-void

    .line 176
    :cond_13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_f

    .line 177
    iget-object v0, p0, Lcom/kuban/settings/utils/ItemCheck$getItems;->this$0:Lcom/kuban/settings/utils/ItemCheck;

    invoke-virtual {v0, p1}, Lcom/kuban/settings/utils/ItemCheck;->notificationmultiple(Ljava/util/ArrayList;)V

    goto :goto_f
.end method

.method protected onPreExecute()V
    .registers 1

    .prologue
    .line 98
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 99
    return-void
.end method
