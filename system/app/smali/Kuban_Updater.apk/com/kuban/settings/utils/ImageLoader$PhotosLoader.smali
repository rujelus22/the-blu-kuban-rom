.class Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/utils/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PhotosLoader"
.end annotation


# instance fields
.field photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

.field final synthetic this$0:Lcom/kuban/settings/utils/ImageLoader;


# direct methods
.method constructor <init>(Lcom/kuban/settings/utils/ImageLoader;Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)V
    .registers 3
    .parameter
    .parameter "photoToLoad"

    .prologue
    .line 105
    iput-object p1, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p2, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    .line 107
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 110
    iget-object v3, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    iget-object v4, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    invoke-virtual {v3, v4}, Lcom/kuban/settings/utils/ImageLoader;->imageViewReused(Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 119
    :cond_a
    :goto_a
    return-void

    .line 112
    :cond_b
    iget-object v3, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    iget-object v4, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    iget-object v4, v4, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->url:Ljava/lang/String;

    #calls: Lcom/kuban/settings/utils/ImageLoader;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v3, v4}, Lcom/kuban/settings/utils/ImageLoader;->access$0(Lcom/kuban/settings/utils/ImageLoader;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 113
    .local v2, bmp:Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    iget-object v3, v3, Lcom/kuban/settings/utils/ImageLoader;->memoryCache:Lcom/kuban/settings/utils/MemoryCache;

    iget-object v4, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    iget-object v4, v4, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->url:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Lcom/kuban/settings/utils/MemoryCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 114
    iget-object v3, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    iget-object v4, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    invoke-virtual {v3, v4}, Lcom/kuban/settings/utils/ImageLoader;->imageViewReused(Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 116
    new-instance v1, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;

    iget-object v3, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->this$0:Lcom/kuban/settings/utils/ImageLoader;

    iget-object v4, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    invoke-direct {v1, v3, v2, v4}, Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;-><init>(Lcom/kuban/settings/utils/ImageLoader;Landroid/graphics/Bitmap;Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;)V

    .line 117
    .local v1, bd:Lcom/kuban/settings/utils/ImageLoader$BitmapDisplayer;
    iget-object v3, p0, Lcom/kuban/settings/utils/ImageLoader$PhotosLoader;->photoToLoad:Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;

    iget-object v3, v3, Lcom/kuban/settings/utils/ImageLoader$PhotoToLoad;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 118
    .local v0, a:Landroid/app/Activity;
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_a
.end method
