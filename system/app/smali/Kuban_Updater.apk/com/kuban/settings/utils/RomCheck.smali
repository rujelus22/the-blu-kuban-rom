.class public Lcom/kuban/settings/utils/RomCheck;
.super Landroid/app/Service;
.source "RomCheck.java"


# static fields
.field static final KEY_UPDATE:Ljava/lang/String; = "Update"

.field static final KEY_VERSION:Ljava/lang/String; = "Version"

.field public static noti:Ljava/lang/String;


# instance fields
.field RomCheckInterval:Ljava/lang/String;

.field RomCheckIntervalTime:I

.field RomCheckOn:Z

.field RomCheckPreference:Z

.field URL:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private runnable:Ljava/lang/Runnable;

.field running:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 32
    const-string v0, "1"

    sput-object v0, Lcom/kuban/settings/utils/RomCheck;->noti:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->URL:Ljava/lang/String;

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->handler:Landroid/os/Handler;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/kuban/settings/utils/RomCheck;->running:I

    .line 49
    new-instance v0, Lcom/kuban/settings/utils/RomCheck$1;

    invoke-direct {v0, p0}, Lcom/kuban/settings/utils/RomCheck$1;-><init>(Lcom/kuban/settings/utils/RomCheck;)V

    iput-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->runnable:Ljava/lang/Runnable;

    .line 24
    return-void
.end method

.method static synthetic access$0(Lcom/kuban/settings/utils/RomCheck;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/kuban/settings/utils/RomCheck;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->runnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public checkTimer()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-virtual {p0}, Lcom/kuban/settings/utils/RomCheck;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 61
    .local v0, prefs:Landroid/content/SharedPreferences;
    const-string v1, "newItemCheck"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/kuban/settings/utils/RomCheck;->RomCheckOn:Z

    .line 62
    iget-boolean v1, p0, Lcom/kuban/settings/utils/RomCheck;->RomCheckOn:Z

    if-eqz v1, :cond_1c

    .line 63
    invoke-virtual {p0}, Lcom/kuban/settings/utils/RomCheck;->romCheck()V

    .line 64
    const/4 v1, 0x1

    iput v1, p0, Lcom/kuban/settings/utils/RomCheck;->running:I

    .line 68
    :goto_1b
    return-void

    .line 66
    :cond_1c
    iput v2, p0, Lcom/kuban/settings/utils/RomCheck;->running:I

    goto :goto_1b
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .registers 1

    .prologue
    .line 41
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 7
    .parameter "intent"
    .parameter "startid"

    .prologue
    .line 44
    const-string v0, "kuban.updater.check"

    invoke-static {v0}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->URL:Ljava/lang/String;

    .line 45
    iget v0, p0, Lcom/kuban/settings/utils/RomCheck;->running:I

    if-nez v0, :cond_15

    .line 46
    iget-object v0, p0, Lcom/kuban/settings/utils/RomCheck;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/kuban/settings/utils/RomCheck;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 48
    :cond_15
    return-void
.end method

.method public romCheck()V
    .registers 32

    .prologue
    .line 72
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 73
    .local v4, UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v22, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct/range {v22 .. v22}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 74
    .local v22, parser:Lcom/kuban/settings/utils/XMLParser;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/utils/RomCheck;->URL:Ljava/lang/String;

    move-object/from16 v29, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 75
    .local v28, xml:Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v9

    .line 76
    .local v9, doc:Lorg/w3c/dom/Document;
    const-string v29, "Update"

    move-object/from16 v0, v29

    invoke-interface {v9, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v24

    .line 77
    .local v24, update:Lorg/w3c/dom/NodeList;
    const/4 v11, 0x0

    .local v11, i:I
    :goto_29
    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v29

    move/from16 v0, v29

    if-lt v11, v0, :cond_32

    .line 111
    .end local v4           #UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v9           #doc:Lorg/w3c/dom/Document;
    .end local v11           #i:I
    .end local v22           #parser:Lcom/kuban/settings/utils/XMLParser;
    .end local v24           #update:Lorg/w3c/dom/NodeList;
    .end local v28           #xml:Ljava/lang/String;
    :goto_31
    return-void

    .line 78
    .restart local v4       #UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v9       #doc:Lorg/w3c/dom/Document;
    .restart local v11       #i:I
    .restart local v22       #parser:Lcom/kuban/settings/utils/XMLParser;
    .restart local v24       #update:Lorg/w3c/dom/NodeList;
    .restart local v28       #xml:Ljava/lang/String;
    :cond_32
    new-instance v25, Ljava/util/HashMap;

    invoke-direct/range {v25 .. v25}, Ljava/util/HashMap;-><init>()V

    .line 79
    .local v25, updateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    move-object/from16 v0, v24

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    check-cast v10, Lorg/w3c/dom/Element;

    .line 80
    .local v10, e:Lorg/w3c/dom/Element;
    const-string v29, "Version"

    const-string v30, "Version"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v10, v1}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    move-object/from16 v0, v25

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v29, "Version"

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/String;

    .line 83
    .local v20, onlineVersion:Ljava/lang/String;
    const-string v29, "kuban.updater.version"

    invoke-static/range {v29 .. v29}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 84
    .local v13, localversion:Ljava/lang/String;
    const-string v29, "\\."

    const-string v30, ""

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 85
    const-string v29, "\\."

    const-string v30, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 86
    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 87
    .local v14, lv:I
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    .line 88
    .local v21, ov:I
    move/from16 v0, v21

    if-le v0, v14, :cond_10f

    .line 89
    const/16 v16, 0x1

    .line 90
    .local v16, not_ID:I
    const-string v19, "notification"

    .line 91
    .local v19, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/kuban/settings/utils/RomCheck;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/app/NotificationManager;

    .line 92
    .local v15, mNotificationManager:Landroid/app/NotificationManager;
    const v12, 0x7f02001d

    .line 93
    .local v12, icon:I
    const-string v23, "New Update ready to download"

    .line 94
    .local v23, tickerText:Ljava/lang/CharSequence;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 95
    .local v26, when:J
    new-instance v17, Landroid/app/Notification;

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move-wide/from16 v2, v26

    invoke-direct {v0, v12, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 96
    .local v17, notification:Landroid/app/Notification;
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/utils/RomCheck;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 97
    .local v8, context:Landroid/content/Context;
    const-string v7, "Blu Kuban Updater"

    .line 98
    .local v7, contentTitle:Ljava/lang/CharSequence;
    const-string v6, "There is an Update ready to download"

    .line 99
    .local v6, contentText:Ljava/lang/CharSequence;
    new-instance v18, Landroid/content/Intent;

    const-class v29, Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    .local v18, notificationIntent:Landroid/content/Intent;
    sget-object v29, Lcom/kuban/settings/utils/RomCheck;->noti:Ljava/lang/String;

    sget-object v30, Lcom/kuban/settings/utils/RomCheck;->noti:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v18

    move/from16 v3, v30

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 102
    .local v5, contentIntent:Landroid/app/PendingIntent;
    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v7, v6, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 103
    move-object/from16 v0, v17

    iget v0, v0, Landroid/app/Notification;->flags:I

    move/from16 v29, v0

    or-int/lit8 v29, v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, v17

    iput v0, v1, Landroid/app/Notification;->flags:I

    .line 104
    move-object/from16 v0, v17

    iget v0, v0, Landroid/app/Notification;->defaults:I

    move/from16 v29, v0

    or-int/lit8 v29, v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, v17

    iput v0, v1, Landroid/app/Notification;->defaults:I

    .line 105
    const/16 v29, 0x1

    move/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v15, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_10f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_10f} :catch_113

    .line 77
    .end local v5           #contentIntent:Landroid/app/PendingIntent;
    .end local v6           #contentText:Ljava/lang/CharSequence;
    .end local v7           #contentTitle:Ljava/lang/CharSequence;
    .end local v8           #context:Landroid/content/Context;
    .end local v12           #icon:I
    .end local v15           #mNotificationManager:Landroid/app/NotificationManager;
    .end local v16           #not_ID:I
    .end local v17           #notification:Landroid/app/Notification;
    .end local v18           #notificationIntent:Landroid/content/Intent;
    .end local v19           #ns:Ljava/lang/String;
    .end local v23           #tickerText:Ljava/lang/CharSequence;
    .end local v26           #when:J
    :cond_10f
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_29

    .line 108
    .end local v4           #UpdateItems:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v9           #doc:Lorg/w3c/dom/Document;
    .end local v10           #e:Lorg/w3c/dom/Element;
    .end local v11           #i:I
    .end local v13           #localversion:Ljava/lang/String;
    .end local v14           #lv:I
    .end local v20           #onlineVersion:Ljava/lang/String;
    .end local v21           #ov:I
    .end local v22           #parser:Lcom/kuban/settings/utils/XMLParser;
    .end local v24           #update:Lorg/w3c/dom/NodeList;
    .end local v25           #updateMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v28           #xml:Ljava/lang/String;
    :catch_113
    move-exception v10

    .line 109
    .local v10, e:Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_31
.end method
