.class public Lcom/kuban/settings/TestActivity;
.super Landroid/os/AsyncTask;
.source "TestActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/widget/ImageView;",
        "Ljava/lang/Void;",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field imageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kuban/settings/TestActivity;->imageView:Landroid/widget/ImageView;

    .line 12
    return-void
.end method

.method private download_Image(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter "urlString"

    .prologue
    .line 26
    const/4 v1, 0x0

    .line 28
    .local v1, image:Landroid/graphics/drawable/Drawable;
    :try_start_1
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 29
    .local v3, url:Ljava/net/URL;
    invoke-virtual {v3}, Ljava/net/URL;->getContent()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/InputStream;

    .line 30
    .local v2, is:Ljava/io/InputStream;
    const-string v4, "src"

    invoke-static {v2, v4}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_11
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_11} :catch_18
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_11} :catch_1b

    move-result-object v1

    .line 36
    .end local v2           #is:Ljava/io/InputStream;
    .end local v3           #url:Ljava/net/URL;
    :goto_12
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 37
    return-object v1

    .line 31
    :catch_18
    move-exception v0

    .line 32
    .local v0, e:Ljava/net/MalformedURLException;
    const/4 v1, 0x0

    goto :goto_12

    .line 33
    .end local v0           #e:Ljava/net/MalformedURLException;
    :catch_1b
    move-exception v0

    .line 34
    .local v0, e:Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_12
.end method


# virtual methods
.method protected varargs doInBackground([Landroid/widget/ImageView;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter "imageViews"

    .prologue
    .line 18
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/kuban/settings/TestActivity;->imageView:Landroid/widget/ImageView;

    .line 19
    iget-object v0, p0, Lcom/kuban/settings/TestActivity;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/kuban/settings/TestActivity;->download_Image(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Landroid/widget/ImageView;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/TestActivity;->doInBackground([Landroid/widget/ImageView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .parameter "result"

    .prologue
    .line 23
    iget-object v0, p0, Lcom/kuban/settings/TestActivity;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 24
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/TestActivity;->onPostExecute(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
