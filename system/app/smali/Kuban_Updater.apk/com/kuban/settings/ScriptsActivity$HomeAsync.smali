.class Lcom/kuban/settings/ScriptsActivity$HomeAsync;
.super Landroid/os/AsyncTask;
.source "ScriptsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/ScriptsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomeAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/ScriptsActivity;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/ScriptsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/ScriptsActivity;Lcom/kuban/settings/ScriptsActivity$HomeAsync;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/kuban/settings/ScriptsActivity$HomeAsync;-><init>(Lcom/kuban/settings/ScriptsActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .registers 13
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 71
    const-string v8, "kuban.updater.parts"

    invoke-static {v8}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, BASEURL:Ljava/lang/String;
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 74
    :cond_b
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    const/4 v9, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/kuban/settings/ScriptsActivity;->URL:Ljava/lang/String;

    .line 75
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v9, v8, Lcom/kuban/settings/ScriptsActivity;->URL:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "scripts.xml"

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/kuban/settings/ScriptsActivity;->URL:Ljava/lang/String;

    .line 76
    new-instance v5, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v5}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 77
    .local v5, parser:Lcom/kuban/settings/utils/XMLParser;
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v8, v8, Lcom/kuban/settings/ScriptsActivity;->URL:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 78
    .local v7, xml:Ljava/lang/String;
    invoke-virtual {v5, v7}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 79
    .local v1, doc:Lorg/w3c/dom/Document;
    const-string v8, "script"

    invoke-interface {v1, v8}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 80
    .local v4, nl:Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_4b
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-lt v3, v8, :cond_56

    .line 89
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v8, v8, Lcom/kuban/settings/ScriptsActivity;->scriptItems:Ljava/util/ArrayList;

    return-object v8

    .line 81
    :cond_56
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 82
    .local v6, scriptmap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    .line 83
    .local v2, e:Lorg/w3c/dom/Element;
    const-string v8, "name"

    const-string v9, "name"

    invoke-virtual {v5, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string v8, "description"

    const-string v9, "description"

    invoke-virtual {v5, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    const-string v8, "sbin"

    const-string v9, "sbin"

    invoke-virtual {v5, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string v8, "scriptCode"

    const-string v9, "scriptCode"

    invoke-virtual {v5, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v8, v8, Lcom/kuban/settings/ScriptsActivity;->scriptItems:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v3, v3, 0x1

    goto :goto_4b
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 93
    iget-object v1, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    invoke-virtual {v1, v2}, Lcom/kuban/settings/ScriptsActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 94
    new-array v4, v3, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v4, v2

    const/4 v1, 0x1

    const-string v2, "description"

    aput-object v2, v4, v1

    const/4 v1, 0x2

    const-string v2, "scriptCode"

    aput-object v2, v4, v1

    const/4 v1, 0x3

    const-string v2, "sbin"

    aput-object v2, v4, v1

    .line 95
    .local v4, from:[Ljava/lang/String;
    new-array v5, v3, [I

    fill-array-data v5, :array_42

    .line 96
    .local v5, to:[I
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v1, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    const v3, 0x7f030014

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 97
    .local v0, adapter:Landroid/widget/SimpleAdapter;
    iget-object v1, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    const v2, 0x7f0c0037

    invoke-virtual {v1, v2}, Lcom/kuban/settings/ScriptsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 98
    .local v6, script_listview:Landroid/widget/ListView;
    iget-object v1, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v1, v1, Lcom/kuban/settings/ScriptsActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 99
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    return-void

    .line 95
    :array_42
    .array-data 0x4
        0x38t 0x0t 0xct 0x7ft
        0x39t 0x0t 0xct 0x7ft
        0x3at 0x0t 0xct 0x7ft
        0x3bt 0x0t 0xct 0x7ft
    .end array-data
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 66
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 67
    iget-object v0, p0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->this$0:Lcom/kuban/settings/ScriptsActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ScriptsActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 68
    return-void
.end method
