.class Lcom/kuban/settings/DownloadedItemsActivity$2;
.super Ljava/lang/Object;
.source "DownloadedItemsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/DownloadedItemsActivity;->ListDir(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/DownloadedItemsActivity;

.field private final synthetic val$dl_listview:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/kuban/settings/DownloadedItemsActivity;Landroid/widget/ListView;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    iput-object p2, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->val$dl_listview:Landroid/widget/ListView;

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 17
    .parameter "v"

    .prologue
    .line 119
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getCount()I

    move-result v1

    .line 120
    .local v1, cntChoice:I
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v9

    .line 121
    .local v9, sparseBooleanArray:Landroid/util/SparseBooleanArray;
    const/4 v6, 0x0

    .local v6, i:I
    :goto_d
    if-lt v6, v1, :cond_20

    .line 145
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v12}, Lcom/kuban/settings/DownloadedItemsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 146
    .local v7, intent:Landroid/content/Intent;
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v12}, Lcom/kuban/settings/DownloadedItemsActivity;->finish()V

    .line 147
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v12, v7}, Lcom/kuban/settings/DownloadedItemsActivity;->startActivity(Landroid/content/Intent;)V

    .line 148
    return-void

    .line 122
    .end local v7           #intent:Landroid/content/Intent;
    :cond_20
    invoke-virtual {v9, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v12

    if-eqz v12, :cond_99

    .line 123
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 124
    .local v8, selected:Ljava/lang/String;
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->val$dl_listview:Landroid/widget/ListView;

    invoke-virtual {v12, v6}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 125
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    .line 126
    .local v5, folder:Ljava/io/File;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    iget-object v13, v13, Lcom/kuban/settings/DownloadedItemsActivity;->downloadFolder:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 127
    .local v4, fileName:Ljava/lang/String;
    const-string v12, "/mnt"

    const-string v13, ""

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 128
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    .local v0, File:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_9d

    .line 131
    :try_start_7a
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "su -c /sbin/rm -r "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_90
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_90} :catch_bb

    .line 133
    :goto_90
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_99

    .line 134
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 121
    .end local v0           #File:Ljava/io/File;
    .end local v4           #fileName:Ljava/lang/String;
    .end local v5           #folder:Ljava/io/File;
    .end local v8           #selected:Ljava/lang/String;
    :cond_99
    :goto_99
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_d

    .line 137
    .restart local v0       #File:Ljava/io/File;
    .restart local v4       #fileName:Ljava/lang/String;
    .restart local v5       #folder:Ljava/io/File;
    .restart local v8       #selected:Ljava/lang/String;
    :cond_9d
    iget-object v12, p0, Lcom/kuban/settings/DownloadedItemsActivity$2;->this$0:Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-virtual {v12}, Lcom/kuban/settings/DownloadedItemsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 138
    .local v2, context:Landroid/content/Context;
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "There was an error deleting "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 139
    .local v10, text:Ljava/lang/CharSequence;
    const/4 v3, 0x0

    .line 140
    .local v3, duration:I
    invoke-static {v2, v10, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    .line 141
    .local v11, toast:Landroid/widget/Toast;
    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    goto :goto_99

    .line 132
    .end local v2           #context:Landroid/content/Context;
    .end local v3           #duration:I
    .end local v10           #text:Ljava/lang/CharSequence;
    .end local v11           #toast:Landroid/widget/Toast;
    :catch_bb
    move-exception v12

    goto :goto_90
.end method
