.class Lcom/kuban/settings/InstalledApplicationActivity$6;
.super Ljava/lang/Object;
.source "InstalledApplicationActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/InstalledApplicationActivity;->systemPopUp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field getDir:Ljava/lang/String;

.field final synthetic this$0:Lcom/kuban/settings/InstalledApplicationActivity;


# direct methods
.method constructor <init>(Lcom/kuban/settings/InstalledApplicationActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 265
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$4()Lcom/kuban/settings/utils/AppInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/kuban/settings/utils/AppInfo;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->getDir:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 14
    .parameter "dialog"
    .parameter "id"

    .prologue
    .line 267
    invoke-static {}, Lcom/kuban/settings/InstalledApplicationActivity;->access$4()Lcom/kuban/settings/utils/AppInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/kuban/settings/utils/AppInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 268
    .local v6, packname:Ljava/lang/String;
    const-string v1, "/data/data/"

    .line 269
    .local v1, datadir:Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    .local v2, dir:Ljava/io/File;
    new-instance v0, Ljava/io/File;

    iget-object v8, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->getDir:Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 271
    .local v0, apk:Ljava/io/File;
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    .local v5, intent:Landroid/content/Intent;
    const-string v8, "android.intent.category.HOME"

    invoke-virtual {v5, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 273
    iget-object v8, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v8}, Lcom/kuban/settings/InstalledApplicationActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/high16 v9, 0x1

    invoke-virtual {v8, v5, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 274
    .local v7, resolveInfo:Landroid/content/pm/ResolveInfo;
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 276
    .local v3, homePackage:Ljava/lang/String;
    :try_start_43
    invoke-static {v2}, Lcom/kuban/settings/InstalledApplicationActivity;->deleteDirectory(Ljava/io/File;)V

    .line 277
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_90

    .line 278
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "su -c pkill -9 "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 279
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "su -c /sbin/rm -r "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_78
    .catch Ljava/lang/Exception; {:try_start_43 .. :try_end_78} :catch_c8

    .line 290
    :cond_78
    :goto_78
    new-instance v4, Landroid/content/Intent;

    iget-object v8, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v8}, Lcom/kuban/settings/InstalledApplicationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-class v9, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v4, v8, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    .local v4, in:Landroid/content/Intent;
    iget-object v8, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v8}, Lcom/kuban/settings/InstalledApplicationActivity;->finish()V

    .line 292
    iget-object v8, p0, Lcom/kuban/settings/InstalledApplicationActivity$6;->this$0:Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-virtual {v8, v4}, Lcom/kuban/settings/InstalledApplicationActivity;->startActivity(Landroid/content/Intent;)V

    .line 293
    return-void

    .line 282
    .end local v4           #in:Landroid/content/Intent;
    :cond_90
    const-wide/16 v8, 0x1f4

    :try_start_92
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 283
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_78

    .line 284
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "su -c pkill -9 "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 285
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "su -c /sbin/rm -r "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_c7
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_c7} :catch_c8

    goto :goto_78

    .line 289
    :catch_c8
    move-exception v8

    goto :goto_78
.end method
