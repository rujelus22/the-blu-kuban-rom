.class public Lcom/kuban/settings/ScriptsActivity;
.super Landroid/app/Activity;
.source "ScriptsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/ScriptsActivity$HomeAsync;
    }
.end annotation


# static fields
.field static final KEY_CODE:Ljava/lang/String; = "scriptCode"

.field static final KEY_DESC:Ljava/lang/String; = "description"

.field static final KEY_NAME:Ljava/lang/String; = "name"

.field static final KEY_SBIN:Ljava/lang/String; = "sbin"

.field static final KEY_SCRIPT:Ljava/lang/String; = "script"


# instance fields
.field URL:Ljava/lang/String;

.field listener:Landroid/widget/AdapterView$OnItemClickListener;

.field logDirectory:Ljava/io/File;

.field final scriptItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/kuban/settings/ScriptsActivity;->URL:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kuban/settings/ScriptsActivity;->scriptItems:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Lcom/kuban/settings/ScriptsActivity$1;

    invoke-direct {v0, p0}, Lcom/kuban/settings/ScriptsActivity$1;-><init>(Lcom/kuban/settings/ScriptsActivity;)V

    iput-object v0, p0, Lcom/kuban/settings/ScriptsActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 35
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 9
    .parameter "newConfig"

    .prologue
    const/4 v3, 0x4

    .line 47
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 48
    const v1, 0x7f030013

    invoke-virtual {p0, v1}, Lcom/kuban/settings/ScriptsActivity;->setContentView(I)V

    .line 49
    new-array v4, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v4, v1

    const/4 v1, 0x1

    const-string v2, "description"

    aput-object v2, v4, v1

    const/4 v1, 0x2

    const-string v2, "scriptCode"

    aput-object v2, v4, v1

    const/4 v1, 0x3

    const-string v2, "sbin"

    aput-object v2, v4, v1

    .line 50
    .local v4, from:[Ljava/lang/String;
    new-array v5, v3, [I

    fill-array-data v5, :array_42

    .line 51
    .local v5, to:[I
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v2, p0, Lcom/kuban/settings/ScriptsActivity;->scriptItems:Ljava/util/ArrayList;

    const v3, 0x7f030014

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 52
    .local v0, adapter:Landroid/widget/SimpleAdapter;
    const v1, 0x7f0c0037

    invoke-virtual {p0, v1}, Lcom/kuban/settings/ScriptsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 53
    .local v6, script_listview:Landroid/widget/ListView;
    iget-object v1, p0, Lcom/kuban/settings/ScriptsActivity;->listener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 55
    return-void

    .line 50
    :array_42
    .array-data 0x4
        0x38t 0x0t 0xct 0x7ft
        0x39t 0x0t 0xct 0x7ft
        0x3at 0x0t 0xct 0x7ft
        0x3bt 0x0t 0xct 0x7ft
    .end array-data
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 58
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ScriptsActivity;->requestWindowFeature(I)Z

    .line 59
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 61
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/kuban/settings/ScriptsActivity;->setContentView(I)V

    .line 62
    new-instance v0, Lcom/kuban/settings/ScriptsActivity$HomeAsync;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/kuban/settings/ScriptsActivity$HomeAsync;-><init>(Lcom/kuban/settings/ScriptsActivity;Lcom/kuban/settings/ScriptsActivity$HomeAsync;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ScriptsActivity$HomeAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 63
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 138
    .local v0, inflater:Landroid/view/MenuInflater;
    const/high16 v1, 0x7f0b

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 139
    const/4 v1, 0x1

    return v1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 8
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 144
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004e

    if-ne v4, v5, :cond_2c

    .line 145
    new-instance v1, Landroid/app/Dialog;

    const v4, 0x7f0a0001

    invoke-direct {v1, p0, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 146
    .local v1, dialog:Landroid/app/Dialog;
    const/high16 v4, 0x7f03

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->setContentView(I)V

    .line 147
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 148
    const v4, 0x7f0c0001

    invoke-virtual {v1, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 149
    .local v0, aboutWebView:Landroid/webkit/WebView;
    const-string v4, "file:///android_asset/about.html"

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 216
    .end local v0           #aboutWebView:Landroid/webkit/WebView;
    .end local v1           #dialog:Landroid/app/Dialog;
    :goto_2b
    return v3

    .line 153
    :cond_2c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004a

    if-ne v4, v5, :cond_4e

    .line 154
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/InstalledApplicationActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    .local v2, in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Installed Apps"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 157
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 160
    .end local v2           #in:Landroid/content/Intent;
    :cond_4e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004b

    if-ne v4, v5, :cond_70

    .line 161
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ScriptsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 162
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Scripts"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 164
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 167
    .end local v2           #in:Landroid/content/Intent;
    :cond_70
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004d

    if-ne v4, v5, :cond_92

    .line 168
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 169
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Change Log"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 171
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2b

    .line 174
    .end local v2           #in:Landroid/content/Intent;
    :cond_92
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0031

    if-ne v4, v5, :cond_b5

    .line 175
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "ROM Information"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 178
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 181
    .end local v2           #in:Landroid/content/Intent;
    :cond_b5
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0049

    if-ne v4, v5, :cond_d8

    .line 182
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/DownloadedItemsActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 183
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Manage Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 185
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 188
    .end local v2           #in:Landroid/content/Intent;
    :cond_d8
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c004c

    if-ne v4, v5, :cond_fb

    .line 189
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/BackupActivity;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Backup/Restore"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 192
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 195
    .end local v2           #in:Landroid/content/Intent;
    :cond_fb
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0047

    if-ne v4, v5, :cond_11e

    .line 196
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/utils/Settings;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 197
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Settings"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 198
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 199
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 202
    .end local v2           #in:Landroid/content/Intent;
    :cond_11e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0c0048

    if-ne v4, v5, :cond_141

    .line 203
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/NewestMods;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 204
    .restart local v2       #in:Landroid/content/Intent;
    const-string v4, "name"

    const-string v5, "Newest Mods"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 206
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 209
    .end local v2           #in:Landroid/content/Intent;
    :cond_141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x102002c

    if-ne v4, v5, :cond_162

    .line 210
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/kuban/settings/Home;

    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    .restart local v2       #in:Landroid/content/Intent;
    const/high16 v4, 0x400

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 212
    invoke-virtual {p0}, Lcom/kuban/settings/ScriptsActivity;->finish()V

    .line 213
    invoke-virtual {p0, v2}, Lcom/kuban/settings/ScriptsActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2b

    .line 216
    .end local v2           #in:Landroid/content/Intent;
    :cond_162
    const/4 v3, 0x0

    goto/16 :goto_2b
.end method
