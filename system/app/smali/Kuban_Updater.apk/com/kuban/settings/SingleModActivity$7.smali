.class Lcom/kuban/settings/SingleModActivity$7;
.super Ljava/lang/Object;
.source "SingleModActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kuban/settings/SingleModActivity;->zipnotmatch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/SingleModActivity;

.field private final synthetic val$name:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    iput-object p2, p0, Lcom/kuban/settings/SingleModActivity$7;->val$name:Ljava/lang/String;

    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 13
    .parameter "dialog"
    .parameter "id"

    .prologue
    const/4 v9, 0x0

    .line 426
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    iget-object v7, v7, Lcom/kuban/settings/SingleModActivity;->downloadFolder:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/kuban/settings/SingleModActivity$7;->val$name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, file:Ljava/lang/String;
    const-string v6, "/mnt"

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 428
    .local v3, fileName:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 429
    .local v0, File:Ljava/io/File;
    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    invoke-virtual {v6}, Lcom/kuban/settings/SingleModActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v7, 0x80

    invoke-virtual {v6, v7}, Landroid/view/Window;->clearFlags(I)V

    .line 431
    :try_start_44
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "su -c /sbin/rm -r "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 432
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_63

    .line 433
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_63
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_63} :catch_96

    .line 436
    :cond_63
    :goto_63
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_9f

    .line 437
    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    invoke-virtual {v6}, Lcom/kuban/settings/SingleModActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, " There was an Error deleting the mod!!"

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 438
    .local v5, toast:Landroid/widget/Toast;
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 444
    :goto_78
    new-instance v4, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 445
    .local v4, intent:Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-string v7, "com.kuban.settings"

    const-string v8, "com.kuban.settings.Home"

    invoke-direct {v6, v7, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 446
    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    invoke-virtual {v6}, Lcom/kuban/settings/SingleModActivity;->finish()V

    .line 447
    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    invoke-virtual {v6, v4}, Lcom/kuban/settings/SingleModActivity;->startActivity(Landroid/content/Intent;)V

    .line 448
    return-void

    .line 435
    .end local v4           #intent:Landroid/content/Intent;
    .end local v5           #toast:Landroid/widget/Toast;
    :catch_96
    move-exception v1

    .local v1, e:Ljava/lang/Exception;
    const-string v6, "KUBAN UPDATER DEBUG"

    const-string v7, "Error deleting the file"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_63

    .line 441
    .end local v1           #e:Ljava/lang/Exception;
    :cond_9f
    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity$7;->this$0:Lcom/kuban/settings/SingleModActivity;

    invoke-virtual {v6}, Lcom/kuban/settings/SingleModActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity$7;->val$name:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " Has been deleted!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    .line 442
    .restart local v5       #toast:Landroid/widget/Toast;
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_78
.end method
