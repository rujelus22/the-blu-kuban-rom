.class Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;
.super Landroid/os/AsyncTask;
.source "RomInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/RomInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadFileAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/RomInfoActivity;


# direct methods
.method constructor <init>(Lcom/kuban/settings/RomInfoActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 275
    iput-object p1, p0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .registers 23
    .parameter "aurl"

    .prologue
    .line 286
    :try_start_0
    new-instance v13, Ljava/net/URL;

    const/4 v14, 0x0

    aget-object v14, p1, v14

    invoke-direct {v13, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 287
    .local v13, url:Ljava/net/URL;
    invoke-virtual {v13}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    .line 288
    .local v2, conexion:Ljava/net/URLConnection;
    invoke-virtual {v2}, Ljava/net/URLConnection;->connect()V

    .line 289
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    new-instance v15, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v15, v14, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    .line 290
    sget-object v14, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static {v14}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 291
    .local v6, fileExtenstion:Ljava/lang/String;
    sget-object v14, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/4 v15, 0x0

    invoke-static {v14, v15, v6}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 292
    .local v9, name:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    iget-object v14, v14, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    invoke-direct {v5, v14, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 293
    .local v5, file:Ljava/io/File;
    invoke-virtual {v2}, Ljava/net/URLConnection;->getContentLength()I

    move-result v8

    .line 294
    .local v8, lenghtOfFile:I
    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-virtual {v13}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v14

    invoke-direct {v7, v14}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 295
    .local v7, input:Ljava/io/InputStream;
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 296
    .local v10, output:Ljava/io/OutputStream;
    const/16 v14, 0x400

    new-array v4, v14, [B

    .line 297
    .local v4, data:[B
    const-wide/16 v11, 0x0

    .line 298
    .local v11, total:J
    :goto_5a
    invoke-virtual {v7, v4}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, count:I
    const/4 v14, -0x1

    if-ne v3, v14, :cond_6c

    .line 303
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    .line 304
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V

    .line 305
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 307
    .end local v2           #conexion:Ljava/net/URLConnection;
    .end local v3           #count:I
    .end local v4           #data:[B
    .end local v5           #file:Ljava/io/File;
    .end local v6           #fileExtenstion:Ljava/lang/String;
    .end local v7           #input:Ljava/io/InputStream;
    .end local v8           #lenghtOfFile:I
    .end local v9           #name:Ljava/lang/String;
    .end local v10           #output:Ljava/io/OutputStream;
    .end local v11           #total:J
    .end local v13           #url:Ljava/net/URL;
    :goto_6a
    const/4 v14, 0x0

    return-object v14

    .line 299
    .restart local v2       #conexion:Ljava/net/URLConnection;
    .restart local v3       #count:I
    .restart local v4       #data:[B
    .restart local v5       #file:Ljava/io/File;
    .restart local v6       #fileExtenstion:Ljava/lang/String;
    .restart local v7       #input:Ljava/io/InputStream;
    .restart local v8       #lenghtOfFile:I
    .restart local v9       #name:Ljava/lang/String;
    .restart local v10       #output:Ljava/io/OutputStream;
    .restart local v11       #total:J
    .restart local v13       #url:Ljava/net/URL;
    :cond_6c
    int-to-long v14, v3

    add-long/2addr v11, v14

    .line 300
    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v17, 0x64

    mul-long v17, v17, v11

    int-to-long v0, v8

    move-wide/from16 v19, v0

    div-long v17, v17, v19

    move-wide/from16 v0, v17

    long-to-int v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->publishProgress([Ljava/lang/Object;)V

    .line 301
    const/4 v14, 0x0

    invoke-virtual {v10, v4, v14, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_98
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_98} :catch_99

    goto :goto_5a

    .line 306
    .end local v2           #conexion:Ljava/net/URLConnection;
    .end local v3           #count:I
    .end local v4           #data:[B
    .end local v5           #file:Ljava/io/File;
    .end local v6           #fileExtenstion:Ljava/lang/String;
    .end local v7           #input:Ljava/io/InputStream;
    .end local v8           #lenghtOfFile:I
    .end local v9           #name:Ljava/lang/String;
    .end local v10           #output:Ljava/io/OutputStream;
    .end local v11           #total:J
    .end local v13           #url:Ljava/net/URL;
    :catch_99
    move-exception v14

    goto :goto_6a
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .registers 29
    .parameter "unused"

    .prologue
    .line 315
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Lcom/kuban/settings/RomInfoActivity;->dismissDialog(I)V

    .line 316
    const-string v17, "notification"

    .line 317
    .local v17, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/kuban/settings/RomInfoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/app/NotificationManager;

    .line 318
    .local v14, mNotificationManager:Landroid/app/NotificationManager;
    const/4 v2, 0x1

    .line 319
    .local v2, HELLO_ID:I
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/app/NotificationManager;->cancel(I)V

    .line 320
    sget-object v23, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 321
    .local v9, fileExtenstion:Ljava/lang/String;
    sget-object v23, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v9}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 322
    .local v16, name:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    new-instance v24, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->downloadFolder:Ljava/lang/String;

    move-object/from16 v26, v0

    invoke-direct/range {v24 .. v26}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    .line 323
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity;->cacheDirectory:Ljava/io/File;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-direct {v8, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 326
    .local v8, file:Ljava/io/File;
    :try_start_6d
    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 327
    .local v11, in:Ljava/io/InputStream;
    const-string v23, "MD5"

    invoke-static/range {v23 .. v23}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v15

    .line 328
    .local v15, md:Ljava/security/MessageDigest;
    const/16 v23, 0x2000

    move/from16 v0, v23

    new-array v4, v0, [B

    .line 330
    .local v4, buf:[B
    :goto_7e
    invoke-virtual {v11, v4}, Ljava/io/InputStream;->read([B)I

    move-result v13

    .local v13, len:I
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_129

    .line 333
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    .line 334
    invoke-virtual {v15}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 335
    .local v5, bytes:[B
    new-instance v19, Ljava/lang/StringBuilder;

    array-length v0, v5

    move/from16 v23, v0

    mul-int/lit8 v23, v23, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 336
    .local v19, sb:Ljava/lang/StringBuilder;
    array-length v0, v5

    move/from16 v24, v0

    const/16 v23, 0x0

    :goto_a2
    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_134

    .line 340
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_ab
    .catch Ljava/io/FileNotFoundException; {:try_start_6d .. :try_end_ab} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_6d .. :try_end_ab} :catch_193
    .catch Ljava/io/IOException; {:try_start_6d .. :try_end_ab} :catch_195

    move-result-object v10

    .line 342
    .local v10, hex:Ljava/lang/String;
    :try_start_ac
    new-instance v21, Ljava/net/URL;

    new-instance v23, Ljava/lang/StringBuilder;

    sget-object v24, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v24, ".md5sum"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 343
    .local v21, url:Ljava/net/URL;
    new-instance v12, Ljava/io/BufferedReader;

    new-instance v23, Ljava/io/InputStreamReader;

    invoke-virtual/range {v21 .. v21}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v23

    invoke-direct {v12, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 345
    .local v12, in3:Ljava/io/BufferedReader;
    :goto_da
    invoke-virtual {v12}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v20

    .local v20, str:Ljava/lang/String;
    if-nez v20, :cond_15c

    .line 354
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_e3
    .catch Ljava/net/MalformedURLException; {:try_start_ac .. :try_end_e3} :catch_17d
    .catch Ljava/io/IOException; {:try_start_ac .. :try_end_e3} :catch_18b
    .catch Ljava/io/FileNotFoundException; {:try_start_ac .. :try_end_e3} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_ac .. :try_end_e3} :catch_193

    .line 358
    .end local v12           #in3:Ljava/io/BufferedReader;
    .end local v20           #str:Ljava/lang/String;
    .end local v21           #url:Ljava/net/URL;
    :goto_e3
    :try_start_e3
    new-instance v23, Ljava/lang/StringBuilder;

    sget-object v24, Lcom/kuban/settings/RomInfoActivity;->URL2:Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v24, ".md5sum"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_f7
    .catch Ljava/io/FileNotFoundException; {:try_start_e3 .. :try_end_f7} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_e3 .. :try_end_f7} :catch_193
    .catch Ljava/io/IOException; {:try_start_e3 .. :try_end_f7} :catch_195

    move-result-object v22

    .line 360
    .local v22, url50:Ljava/lang/String;
    const/16 v23, 0x0

    :try_start_fa
    invoke-static/range {v23 .. v23}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 362
    new-instance v23, Ljava/net/URL;

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljava/net/HttpURLConnection;

    .line 363
    .local v6, con:Ljava/net/HttpURLConnection;
    const-string v23, "HEAD"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 364
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v23

    const/16 v24, 0xc8

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_128

    .line 367
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/kuban/settings/RomInfoActivity;->noMD5tomatch()V
    :try_end_128
    .catch Ljava/lang/Exception; {:try_start_fa .. :try_end_128} :catch_18e
    .catch Ljava/io/FileNotFoundException; {:try_start_fa .. :try_end_128} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_fa .. :try_end_128} :catch_193
    .catch Ljava/io/IOException; {:try_start_fa .. :try_end_128} :catch_195

    .line 377
    .end local v4           #buf:[B
    .end local v5           #bytes:[B
    .end local v6           #con:Ljava/net/HttpURLConnection;
    .end local v10           #hex:Ljava/lang/String;
    .end local v11           #in:Ljava/io/InputStream;
    .end local v13           #len:I
    .end local v15           #md:Ljava/security/MessageDigest;
    .end local v19           #sb:Ljava/lang/StringBuilder;
    .end local v22           #url50:Ljava/lang/String;
    :cond_128
    :goto_128
    return-void

    .line 331
    .restart local v4       #buf:[B
    .restart local v11       #in:Ljava/io/InputStream;
    .restart local v13       #len:I
    .restart local v15       #md:Ljava/security/MessageDigest;
    :cond_129
    const/16 v23, 0x0

    :try_start_12b
    move/from16 v0, v23

    invoke-virtual {v15, v4, v0, v13}, Ljava/security/MessageDigest;->update([BII)V

    goto/16 :goto_7e

    .line 374
    .end local v4           #buf:[B
    .end local v11           #in:Ljava/io/InputStream;
    .end local v13           #len:I
    .end local v15           #md:Ljava/security/MessageDigest;
    :catch_132
    move-exception v23

    goto :goto_128

    .line 336
    .restart local v4       #buf:[B
    .restart local v5       #bytes:[B
    .restart local v11       #in:Ljava/io/InputStream;
    .restart local v13       #len:I
    .restart local v15       #md:Ljava/security/MessageDigest;
    .restart local v19       #sb:Ljava/lang/StringBuilder;
    :cond_134
    aget-byte v3, v5, v23

    .line 337
    .local v3, b:B
    const-string v25, "0123456789ABCDEF"

    and-int/lit16 v0, v3, 0xf0

    move/from16 v26, v0

    shr-int/lit8 v26, v26, 0x4

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->charAt(I)C

    move-result v25

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 338
    const-string v25, "0123456789ABCDEF"

    and-int/lit8 v26, v3, 0xf

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->charAt(I)C

    move-result v25

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_158
    .catch Ljava/io/FileNotFoundException; {:try_start_12b .. :try_end_158} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_12b .. :try_end_158} :catch_193
    .catch Ljava/io/IOException; {:try_start_12b .. :try_end_158} :catch_195

    .line 336
    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_a2

    .line 346
    .end local v3           #b:B
    .restart local v10       #hex:Ljava/lang/String;
    .restart local v12       #in3:Ljava/io/BufferedReader;
    .restart local v20       #str:Ljava/lang/String;
    .restart local v21       #url:Ljava/net/URL;
    :cond_15c
    :try_start_15c
    const-string v23, " "

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    aget-object v18, v23, v24

    .line 347
    .local v18, pulledhex:Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_180

    .line 348
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/kuban/settings/RomInfoActivity;->md5Match()V

    goto/16 :goto_da

    .line 356
    .end local v12           #in3:Ljava/io/BufferedReader;
    .end local v18           #pulledhex:Ljava/lang/String;
    .end local v20           #str:Ljava/lang/String;
    .end local v21           #url:Ljava/net/URL;
    :catch_17d
    move-exception v23

    goto/16 :goto_e3

    .line 351
    .restart local v12       #in3:Ljava/io/BufferedReader;
    .restart local v18       #pulledhex:Ljava/lang/String;
    .restart local v20       #str:Ljava/lang/String;
    .restart local v21       #url:Ljava/net/URL;
    :cond_180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/kuban/settings/RomInfoActivity;->md5NotMatch()V
    :try_end_189
    .catch Ljava/net/MalformedURLException; {:try_start_15c .. :try_end_189} :catch_17d
    .catch Ljava/io/IOException; {:try_start_15c .. :try_end_189} :catch_18b
    .catch Ljava/io/FileNotFoundException; {:try_start_15c .. :try_end_189} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_15c .. :try_end_189} :catch_193

    goto/16 :goto_da

    .line 357
    .end local v12           #in3:Ljava/io/BufferedReader;
    .end local v18           #pulledhex:Ljava/lang/String;
    .end local v20           #str:Ljava/lang/String;
    .end local v21           #url:Ljava/net/URL;
    :catch_18b
    move-exception v23

    goto/16 :goto_e3

    .line 370
    .restart local v22       #url50:Ljava/lang/String;
    :catch_18e
    move-exception v7

    .line 371
    .local v7, e:Ljava/lang/Exception;
    :try_start_18f
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_192
    .catch Ljava/io/FileNotFoundException; {:try_start_18f .. :try_end_192} :catch_132
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_18f .. :try_end_192} :catch_193
    .catch Ljava/io/IOException; {:try_start_18f .. :try_end_192} :catch_195

    goto :goto_128

    .line 375
    .end local v4           #buf:[B
    .end local v5           #bytes:[B
    .end local v7           #e:Ljava/lang/Exception;
    .end local v10           #hex:Ljava/lang/String;
    .end local v11           #in:Ljava/io/InputStream;
    .end local v13           #len:I
    .end local v15           #md:Ljava/security/MessageDigest;
    .end local v19           #sb:Ljava/lang/StringBuilder;
    .end local v22           #url50:Ljava/lang/String;
    :catch_193
    move-exception v23

    goto :goto_128

    .line 376
    :catch_195
    move-exception v23

    goto :goto_128
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 279
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 280
    iget-object v0, p0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/kuban/settings/RomInfoActivity;->showDialog(I)V

    .line 281
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->onProgressUpdate([Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/String;)V
    .registers 4
    .parameter "progress"

    .prologue
    .line 310
    iget-object v0, p0, Lcom/kuban/settings/RomInfoActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/RomInfoActivity;

    #getter for: Lcom/kuban/settings/RomInfoActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/kuban/settings/RomInfoActivity;->access$0(Lcom/kuban/settings/RomInfoActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 311
    return-void
.end method
