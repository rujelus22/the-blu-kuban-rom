.class public Lcom/kuban/settings/SingleModActivity;
.super Landroid/app/Activity;
.source "SingleModActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;
    }
.end annotation


# static fields
.field public static final DIALOG_DOWNLOAD_PROGRESS:I = 0x0

.field static final KEY_ITEM_NAME:Ljava/lang/String; = "Name"

.field static final KEY_ITEM_PREVIEW:Ljava/lang/String; = "Preview"

.field static final KEY_ITEM_PREVIEW2:Ljava/lang/String; = "Preview2"

.field static final KEY_ITEM_PREVIEW3:Ljava/lang/String; = "Preview3"

.field public static final KEY_ITEM_URL:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "KUBAN UPDATER DEBUG"


# instance fields
.field final context:Landroid/content/Context;

.field date:Ljava/lang/String;

.field description:Ljava/lang/String;

.field private downloadButton:Landroid/widget/Button;

.field downloadFolder:Ljava/lang/String;

.field installed:Ljava/lang/String;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field name:Ljava/lang/String;

.field preview:Ljava/lang/String;

.field preview2:Ljava/lang/String;

.field preview3:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/kuban/settings/SingleModActivity;->KEY_ITEM_URL:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    iput-object p0, p0, Lcom/kuban/settings/SingleModActivity;->context:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$0(Lcom/kuban/settings/SingleModActivity;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public apkMD5notmatch()V
    .registers 10

    .prologue
    .line 483
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    .line 484
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 485
    .local v3, in2:Landroid/content/Intent;
    const-string v6, "Url"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 486
    .local v5, url2:Ljava/lang/String;
    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 487
    .local v2, fileExtenstion:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v5, v6, v2}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 488
    .local v4, name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 489
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "MD5 Does not match!!"

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 492
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\nWould you Still like to install "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " now?\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 493
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 494
    const-string v7, "Yes"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$10;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$10;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 502
    const-string v7, "No"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$11;

    invoke-direct {v8, p0}, Lcom/kuban/settings/SingleModActivity$11;-><init>(Lcom/kuban/settings/SingleModActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 508
    const-string v7, "Delete"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$12;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$12;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 534
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 535
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 536
    return-void
.end method

.method public apkMatch()V
    .registers 10

    .prologue
    .line 370
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    .line 371
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 372
    .local v3, in2:Landroid/content/Intent;
    const-string v6, "Url"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 373
    .local v5, url2:Ljava/lang/String;
    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, fileExtenstion:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v5, v6, v2}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 375
    .local v4, name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 376
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "MD5 Match: File ready"

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 379
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\nWould you like to install "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " now?\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 380
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 381
    const-string v7, "Yes"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$3;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$3;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 389
    const-string v7, "No"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$4;

    invoke-direct {v8, p0}, Lcom/kuban/settings/SingleModActivity$4;-><init>(Lcom/kuban/settings/SingleModActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 395
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 396
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 397
    return-void
.end method

.method public noMD5tomatch()V
    .registers 10

    .prologue
    .line 454
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    .line 455
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 456
    .local v3, in2:Landroid/content/Intent;
    const-string v6, "Url"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 457
    .local v5, url2:Ljava/lang/String;
    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 458
    .local v2, fileExtenstion:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v5, v6, v2}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 459
    .local v4, name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 460
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "No MD5 online to match the file to!!"

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 462
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\nWould you still like to reboot and install "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " now?\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 463
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 464
    const-string v7, "Yes"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$8;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$8;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 473
    const-string v7, "No"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$9;

    invoke-direct {v8, p0}, Lcom/kuban/settings/SingleModActivity$9;-><init>(Lcom/kuban/settings/SingleModActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 479
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 480
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 481
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "v"

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 151
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 152
    .local v0, in:Landroid/content/Intent;
    const-string v2, "Url"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, url:Ljava/lang/String;
    new-instance v2, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;

    invoke-direct {v2, p0}, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;-><init>(Lcom/kuban/settings/SingleModActivity;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v3}, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 154
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 78
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 14
    .parameter "savedInstanceState"

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 82
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v8, 0x7f030015

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->setContentView(I)V

    .line 84
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8, v10}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 85
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 87
    .local v3, in:Landroid/content/Intent;
    :try_start_16
    const-string v8, "Name"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->name:Ljava/lang/String;

    .line 88
    const-string v8, "Date"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->date:Ljava/lang/String;

    .line 89
    const-string v8, "No"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->installed:Ljava/lang/String;

    .line 90
    const-string v8, "Desc"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->description:Ljava/lang/String;

    .line 91
    const-string v8, "Preview"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview:Ljava/lang/String;

    .line 92
    const-string v8, "Preview2"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview2:Ljava/lang/String;

    .line 93
    const-string v8, "Preview3"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview3:Ljava/lang/String;
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_4e} :catch_192

    .line 96
    :goto_4e
    :try_start_4e
    const-string v8, "Name"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->name:Ljava/lang/String;

    .line 97
    const-string v8, "Date"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->date:Ljava/lang/String;

    .line 98
    const-string v8, "No"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->installed:Ljava/lang/String;

    .line 99
    const-string v8, "Desc"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->description:Ljava/lang/String;

    .line 100
    const-string v8, "Preview"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview:Ljava/lang/String;

    .line 101
    const-string v8, "Preview2"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview2:Ljava/lang/String;

    .line 102
    const-string v8, "Preview3"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview3:Ljava/lang/String;
    :try_end_86
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_86} :catch_18f

    .line 105
    :goto_86
    :try_start_86
    const-string v8, "Name"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->name:Ljava/lang/String;

    .line 106
    const-string v8, "Date"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->date:Ljava/lang/String;

    .line 107
    const-string v8, "No"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->installed:Ljava/lang/String;

    .line 108
    const-string v8, "Desc"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->description:Ljava/lang/String;

    .line 109
    const-string v8, "Preview"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview:Ljava/lang/String;

    .line 110
    const-string v8, "Preview2"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview2:Ljava/lang/String;

    .line 111
    const-string v8, "Preview3"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview3:Ljava/lang/String;
    :try_end_be
    .catch Ljava/lang/Exception; {:try_start_86 .. :try_end_be} :catch_18c

    .line 113
    :goto_be
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->name:Ljava/lang/String;

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 114
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 115
    .local v4, prefs:Landroid/content/SharedPreferences;
    const-string v8, "downloadDir"

    const-string v9, "kubandownloads"

    invoke-interface {v4, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->downloadFolder:Ljava/lang/String;

    .line 116
    const v8, 0x7f0c0040

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 117
    .local v0, imageView:Landroid/widget/ImageView;
    const v8, 0x7f0c0041

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 118
    .local v1, imageView2:Landroid/widget/ImageView;
    const v8, 0x7f0c0042

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 119
    .local v2, imageView3:Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview:Ljava/lang/String;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 120
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview2:Ljava/lang/String;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 121
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->preview3:Ljava/lang/String;

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 122
    new-instance v8, Lcom/kuban/settings/adapters/SingleItemImageAsync;

    invoke-direct {v8}, Lcom/kuban/settings/adapters/SingleItemImageAsync;-><init>()V

    new-array v9, v10, [Landroid/widget/ImageView;

    aput-object v0, v9, v11

    invoke-virtual {v8, v9}, Lcom/kuban/settings/adapters/SingleItemImageAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 123
    new-instance v8, Lcom/kuban/settings/adapters/SingleItemImageAsync;

    invoke-direct {v8}, Lcom/kuban/settings/adapters/SingleItemImageAsync;-><init>()V

    new-array v9, v10, [Landroid/widget/ImageView;

    aput-object v1, v9, v11

    invoke-virtual {v8, v9}, Lcom/kuban/settings/adapters/SingleItemImageAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 124
    new-instance v8, Lcom/kuban/settings/adapters/SingleItemImageAsync;

    invoke-direct {v8}, Lcom/kuban/settings/adapters/SingleItemImageAsync;-><init>()V

    new-array v9, v10, [Landroid/widget/ImageView;

    aput-object v2, v9, v11

    invoke-virtual {v8, v9}, Lcom/kuban/settings/adapters/SingleItemImageAsync;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 125
    const v8, 0x7f0c0010

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 126
    .local v5, txtDate:Landroid/widget/TextView;
    const v8, 0x7f0c003c

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 127
    .local v7, txtInstalled:Landroid/widget/TextView;
    const v8, 0x7f0c003d

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 128
    .local v6, txtDesc:Landroid/widget/TextView;
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->installed:Ljava/lang/String;

    const-string v9, "Yes"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_186

    .line 129
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Installed: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/kuban/settings/SingleModActivity;->installed:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_15c
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Date Updated: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/kuban/settings/SingleModActivity;->date:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->description:Ljava/lang/String;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    const v8, 0x7f0c0043

    invoke-virtual {p0, v8}, Lcom/kuban/settings/SingleModActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lcom/kuban/settings/SingleModActivity;->downloadButton:Landroid/widget/Button;

    .line 138
    iget-object v8, p0, Lcom/kuban/settings/SingleModActivity;->downloadButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    return-void

    .line 132
    :cond_186
    const-string v8, "Installed: No"

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_15c

    .line 112
    .end local v0           #imageView:Landroid/widget/ImageView;
    .end local v1           #imageView2:Landroid/widget/ImageView;
    .end local v2           #imageView3:Landroid/widget/ImageView;
    .end local v4           #prefs:Landroid/content/SharedPreferences;
    .end local v5           #txtDate:Landroid/widget/TextView;
    .end local v6           #txtDesc:Landroid/widget/TextView;
    .end local v7           #txtInstalled:Landroid/widget/TextView;
    :catch_18c
    move-exception v8

    goto/16 :goto_be

    .line 103
    :catch_18f
    move-exception v8

    goto/16 :goto_86

    .line 94
    :catch_192
    move-exception v8

    goto/16 :goto_4e
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 27
    .parameter "id"

    .prologue
    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/SingleModActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v22

    move-object/from16 v0, v22

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v22, v0

    invoke-static/range {v22 .. v22}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v17

    .line 159
    .local v17, orintation:Ljava/lang/String;
    const-string v22, "2"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2d

    .line 160
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    .line 166
    :cond_27
    :goto_27
    packed-switch p1, :pswitch_data_122

    .line 196
    const/16 v22, 0x0

    :goto_2c
    return-object v22

    .line 162
    :cond_2d
    const-string v22, "1"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_27

    .line 163
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    goto :goto_27

    .line 168
    :pswitch_43
    new-instance v3, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->downloadFolder:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 169
    .local v3, cacheDirectory:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_5f

    .line 170
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 171
    :cond_5f
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v10

    .line 172
    .local v10, in2:Landroid/content/Intent;
    const-string v22, "Url"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 173
    .local v19, url2:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 174
    .local v8, fileExtenstion:Ljava/lang/String;
    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-static {v0, v1, v8}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 175
    .local v12, name:Ljava/lang/String;
    new-instance v22, Landroid/app/ProgressDialog;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v22, v0

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "Downloading "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 178
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 179
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/app/ProgressDialog;->show()V

    .line 180
    const-string v16, "notification"

    .line 181
    .local v16, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/kuban/settings/SingleModActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/NotificationManager;

    .line 182
    .local v11, mNotificationManager:Landroid/app/NotificationManager;
    const v9, 0x7f02001d

    .line 183
    .local v9, icon:I
    new-instance v22, Ljava/lang/StringBuilder;

    const-string v23, "Now Downloading "

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 184
    .local v18, tickerText:Ljava/lang/CharSequence;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 185
    .local v20, when:J
    new-instance v14, Landroid/app/Notification;

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-direct {v14, v9, v0, v1, v2}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 186
    .local v14, notification:Landroid/app/Notification;
    invoke-virtual/range {p0 .. p0}, Lcom/kuban/settings/SingleModActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 187
    .local v7, context:Landroid/content/Context;
    const-string v6, "Blu Kuban Updater"

    .line 188
    .local v6, contentTitle:Ljava/lang/CharSequence;
    const-string v5, "Download in progress"

    .line 189
    .local v5, contentText:Ljava/lang/CharSequence;
    new-instance v15, Landroid/content/Intent;

    const-class v22, Lcom/kuban/settings/Home;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v15, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 190
    .local v15, notificationIntent:Landroid/content/Intent;
    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v15, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 191
    .local v4, contentIntent:Landroid/app/PendingIntent;
    invoke-virtual {v14, v7, v6, v5, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 192
    const/4 v13, 0x1

    .line 193
    .local v13, not_ID:I
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0, v14}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    move-object/from16 v22, v0

    goto/16 :goto_2c

    .line 166
    :pswitch_data_122
    .packed-switch 0x0
        :pswitch_43
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "item"

    .prologue
    .line 142
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kuban/settings/Home;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    .local v0, in:Landroid/content/Intent;
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->finish()V

    .line 145
    invoke-virtual {p0, v0}, Lcom/kuban/settings/SingleModActivity;->startActivity(Landroid/content/Intent;)V

    .line 146
    const/4 v1, 0x1

    return v1
.end method

.method public zipMatch()V
    .registers 10

    .prologue
    .line 341
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    .line 342
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 343
    .local v3, in2:Landroid/content/Intent;
    const-string v6, "Url"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 344
    .local v5, url2:Ljava/lang/String;
    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 345
    .local v2, fileExtenstion:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v5, v6, v2}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 346
    .local v4, name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 347
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "MD5 Match: File ready"

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 349
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\nWould you like to reboot and install "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " now?\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 350
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 351
    const-string v7, "Yes"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$1;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$1;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 360
    const-string v7, "No"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$2;

    invoke-direct {v8, p0}, Lcom/kuban/settings/SingleModActivity$2;-><init>(Lcom/kuban/settings/SingleModActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 366
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 367
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 368
    return-void
.end method

.method public zipnotmatch()V
    .registers 10

    .prologue
    .line 399
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, Lcom/kuban/settings/SingleModActivity;->setRequestedOrientation(I)V

    .line 400
    invoke-virtual {p0}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 401
    .local v3, in2:Landroid/content/Intent;
    const-string v6, "Url"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 402
    .local v5, url2:Ljava/lang/String;
    invoke-static {v5}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, fileExtenstion:Ljava/lang/String;
    const/4 v6, 0x0

    invoke-static {v5, v6, v2}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 404
    .local v4, name:Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/kuban/settings/SingleModActivity;->context:Landroid/content/Context;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 405
    .local v1, alertDialogBuilder:Landroid/app/AlertDialog$Builder;
    const-string v6, "MD5 Does not match!!"

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 407
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\nWould you still like to reboot and install "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " now?\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 408
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 409
    const-string v7, "Yes"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$5;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$5;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 418
    const-string v7, "No"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$6;

    invoke-direct {v8, p0}, Lcom/kuban/settings/SingleModActivity$6;-><init>(Lcom/kuban/settings/SingleModActivity;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    .line 424
    const-string v7, "Delete"

    new-instance v8, Lcom/kuban/settings/SingleModActivity$7;

    invoke-direct {v8, p0, v4}, Lcom/kuban/settings/SingleModActivity$7;-><init>(Lcom/kuban/settings/SingleModActivity;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 450
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 451
    .local v0, alertDialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 452
    return-void
.end method
