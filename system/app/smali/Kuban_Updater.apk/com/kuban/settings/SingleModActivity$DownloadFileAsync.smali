.class Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;
.super Landroid/os/AsyncTask;
.source "SingleModActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/SingleModActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DownloadFileAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/SingleModActivity;


# direct methods
.method constructor <init>(Lcom/kuban/settings/SingleModActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 199
    iput-object p1, p0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .registers 26
    .parameter "aurl"

    .prologue
    .line 210
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->downloadFolder:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 211
    .local v2, cacheDirectory:Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 212
    .local v8, in2:Landroid/content/Intent;
    const-string v17, "Url"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 213
    .local v16, url2:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 214
    .local v7, fileExtenstion:Ljava/lang/String;
    const/16 v17, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v0, v1, v7}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 215
    .local v11, name:Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v2, v11}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 216
    .local v6, file:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_8e

    .line 222
    :try_start_44
    new-instance v15, Ljava/net/URL;

    const/16 v17, 0x0

    aget-object v17, p1, v17

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 223
    .local v15, url:Ljava/net/URL;
    invoke-virtual {v15}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    .line 224
    .local v3, conexion:Ljava/net/URLConnection;
    invoke-virtual {v3}, Ljava/net/URLConnection;->connect()V

    .line 225
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_5f

    .line 226
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 227
    :cond_5f
    invoke-virtual {v3}, Ljava/net/URLConnection;->getContentLength()I

    move-result v10

    .line 228
    .local v10, lenghtOfFile:I
    new-instance v9, Ljava/io/BufferedInputStream;

    invoke-virtual {v15}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 229
    .local v9, input:Ljava/io/InputStream;
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 231
    .local v12, output:Ljava/io/OutputStream;
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v5, v0, [B

    .line 232
    .local v5, data:[B
    const-wide/16 v13, 0x0

    .line 233
    .local v13, total:J
    :goto_7b
    invoke-virtual {v9, v5}, Ljava/io/InputStream;->read([B)I

    move-result v4

    .local v4, count:I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-ne v4, v0, :cond_91

    .line 238
    invoke-virtual {v12}, Ljava/io/OutputStream;->flush()V

    .line 239
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V

    .line 240
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    .line 243
    .end local v3           #conexion:Ljava/net/URLConnection;
    .end local v4           #count:I
    .end local v5           #data:[B
    .end local v9           #input:Ljava/io/InputStream;
    .end local v10           #lenghtOfFile:I
    .end local v12           #output:Ljava/io/OutputStream;
    .end local v13           #total:J
    .end local v15           #url:Ljava/net/URL;
    :cond_8e
    :goto_8e
    const/16 v17, 0x0

    return-object v17

    .line 234
    .restart local v3       #conexion:Ljava/net/URLConnection;
    .restart local v4       #count:I
    .restart local v5       #data:[B
    .restart local v9       #input:Ljava/io/InputStream;
    .restart local v10       #lenghtOfFile:I
    .restart local v12       #output:Ljava/io/OutputStream;
    .restart local v13       #total:J
    .restart local v15       #url:Ljava/net/URL;
    :cond_91
    int-to-long v0, v4

    move-wide/from16 v17, v0

    add-long v13, v13, v17

    .line 235
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/16 v20, 0x64

    mul-long v20, v20, v13

    int-to-long v0, v10

    move-wide/from16 v22, v0

    div-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->publishProgress([Ljava/lang/Object;)V

    .line 236
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v5, v0, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_cb
    .catch Ljava/lang/Exception; {:try_start_44 .. :try_end_cb} :catch_cc

    goto :goto_7b

    .line 241
    .end local v3           #conexion:Ljava/net/URLConnection;
    .end local v4           #count:I
    .end local v5           #data:[B
    .end local v9           #input:Ljava/io/InputStream;
    .end local v10           #lenghtOfFile:I
    .end local v12           #output:Ljava/io/OutputStream;
    .end local v13           #total:J
    .end local v15           #url:Ljava/net/URL;
    :catch_cc
    move-exception v17

    goto :goto_8e
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .registers 32
    .parameter "unused"

    .prologue
    .line 251
    const-string v19, "notification"

    .line 252
    .local v19, ns:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/kuban/settings/SingleModActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/NotificationManager;

    .line 253
    .local v16, mNotificationManager:Landroid/app/NotificationManager;
    const/4 v2, 0x1

    .line 254
    .local v2, HELLO_ID:I
    const/16 v26, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Lcom/kuban/settings/SingleModActivity;->dismissDialog(I)V

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    .line 257
    .local v13, in2:Landroid/content/Intent;
    const-string v26, "Url"

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 258
    .local v24, url2:Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 259
    .local v10, fileExtenstion:Ljava/lang/String;
    const/16 v26, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-static {v0, v1, v10}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 262
    .local v18, name:Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity;->downloadFolder:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v6, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 263
    .local v6, cacheDirectory:Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v26

    if-nez v26, :cond_69

    .line 264
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 265
    :cond_69
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v9, v6, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 269
    .local v9, file:Ljava/io/File;
    :try_start_70
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 270
    .local v12, in:Ljava/io/InputStream;
    const-string v26, "MD5"

    invoke-static/range {v26 .. v26}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v17

    .line 272
    .local v17, md:Ljava/security/MessageDigest;
    const/16 v26, 0x2000

    move/from16 v0, v26

    new-array v4, v0, [B

    .line 274
    .local v4, buf:[B
    :goto_81
    invoke-virtual {v12, v4}, Ljava/io/InputStream;->read([B)I

    move-result v15

    .local v15, len:I
    const/16 v26, -0x1

    move/from16 v0, v26

    if-ne v15, v0, :cond_132

    .line 277
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V

    .line 279
    invoke-virtual/range {v17 .. v17}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    .line 281
    .local v5, bytes:[B
    new-instance v21, Ljava/lang/StringBuilder;

    array-length v0, v5

    move/from16 v26, v0

    mul-int/lit8 v26, v26, 0x2

    move-object/from16 v0, v21

    move/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 282
    .local v21, sb:Ljava/lang/StringBuilder;
    array-length v0, v5

    move/from16 v27, v0

    const/16 v26, 0x0

    :goto_a5
    move/from16 v0, v26

    move/from16 v1, v27

    if-lt v0, v1, :cond_13f

    .line 286
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_ae
    .catch Ljava/io/FileNotFoundException; {:try_start_70 .. :try_end_ae} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_70 .. :try_end_ae} :catch_1b8
    .catch Ljava/io/IOException; {:try_start_70 .. :try_end_ae} :catch_1d7

    move-result-object v11

    .line 288
    .local v11, hex:Ljava/lang/String;
    :try_start_af
    new-instance v23, Ljava/net/URL;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v27, ".md5sum"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 289
    .local v23, url:Ljava/net/URL;
    new-instance v14, Ljava/io/BufferedReader;

    new-instance v26, Ljava/io/InputStreamReader;

    invoke-virtual/range {v23 .. v23}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v26

    invoke-direct {v14, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 291
    .local v14, in3:Ljava/io/BufferedReader;
    :goto_db
    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v22

    .local v22, str:Ljava/lang/String;
    if-nez v22, :cond_167

    .line 310
    invoke-virtual {v12}, Ljava/io/InputStream;->close()V
    :try_end_e4
    .catch Ljava/net/MalformedURLException; {:try_start_af .. :try_end_e4} :catch_192
    .catch Ljava/io/IOException; {:try_start_af .. :try_end_e4} :catch_1a0
    .catch Ljava/io/FileNotFoundException; {:try_start_af .. :try_end_e4} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_af .. :try_end_e4} :catch_1b8

    .line 315
    .end local v14           #in3:Ljava/io/BufferedReader;
    .end local v22           #str:Ljava/lang/String;
    .end local v23           #url:Ljava/net/URL;
    :goto_e4
    :try_start_e4
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v27, ".md5sum"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_f6
    .catch Ljava/io/FileNotFoundException; {:try_start_e4 .. :try_end_f6} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_e4 .. :try_end_f6} :catch_1b8
    .catch Ljava/io/IOException; {:try_start_e4 .. :try_end_f6} :catch_1d7

    move-result-object v25

    .line 317
    .local v25, url50:Ljava/lang/String;
    const/16 v26, 0x0

    :try_start_f9
    invoke-static/range {v26 .. v26}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 319
    new-instance v26, Ljava/net/URL;

    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 320
    .local v7, con:Ljava/net/HttpURLConnection;
    const-string v26, "HEAD"

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v26

    const/16 v27, 0xc8

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_131

    .line 324
    const-string v26, "apk"

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_1c6

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->apkMD5notmatch()V
    :try_end_131
    .catch Ljava/lang/Exception; {:try_start_f9 .. :try_end_131} :catch_1d1
    .catch Ljava/io/FileNotFoundException; {:try_start_f9 .. :try_end_131} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_f9 .. :try_end_131} :catch_1b8
    .catch Ljava/io/IOException; {:try_start_f9 .. :try_end_131} :catch_1d7

    .line 338
    .end local v4           #buf:[B
    .end local v5           #bytes:[B
    .end local v7           #con:Ljava/net/HttpURLConnection;
    .end local v11           #hex:Ljava/lang/String;
    .end local v12           #in:Ljava/io/InputStream;
    .end local v15           #len:I
    .end local v17           #md:Ljava/security/MessageDigest;
    .end local v21           #sb:Ljava/lang/StringBuilder;
    .end local v25           #url50:Ljava/lang/String;
    :cond_131
    :goto_131
    return-void

    .line 275
    .restart local v4       #buf:[B
    .restart local v12       #in:Ljava/io/InputStream;
    .restart local v15       #len:I
    .restart local v17       #md:Ljava/security/MessageDigest;
    :cond_132
    const/16 v26, 0x0

    :try_start_134
    move-object/from16 v0, v17

    move/from16 v1, v26

    invoke-virtual {v0, v4, v1, v15}, Ljava/security/MessageDigest;->update([BII)V

    goto/16 :goto_81

    .line 335
    .end local v4           #buf:[B
    .end local v12           #in:Ljava/io/InputStream;
    .end local v15           #len:I
    .end local v17           #md:Ljava/security/MessageDigest;
    :catch_13d
    move-exception v26

    goto :goto_131

    .line 282
    .restart local v4       #buf:[B
    .restart local v5       #bytes:[B
    .restart local v12       #in:Ljava/io/InputStream;
    .restart local v15       #len:I
    .restart local v17       #md:Ljava/security/MessageDigest;
    .restart local v21       #sb:Ljava/lang/StringBuilder;
    :cond_13f
    aget-byte v3, v5, v26

    .line 283
    .local v3, b:B
    const-string v28, "0123456789ABCDEF"

    and-int/lit16 v0, v3, 0xf0

    move/from16 v29, v0

    shr-int/lit8 v29, v29, 0x4

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->charAt(I)C

    move-result v28

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    const-string v28, "0123456789ABCDEF"

    and-int/lit8 v29, v3, 0xf

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->charAt(I)C

    move-result v28

    move-object/from16 v0, v21

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_163
    .catch Ljava/io/FileNotFoundException; {:try_start_134 .. :try_end_163} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_134 .. :try_end_163} :catch_1b8
    .catch Ljava/io/IOException; {:try_start_134 .. :try_end_163} :catch_1d7

    .line 282
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_a5

    .line 292
    .end local v3           #b:B
    .restart local v11       #hex:Ljava/lang/String;
    .restart local v14       #in3:Ljava/io/BufferedReader;
    .restart local v22       #str:Ljava/lang/String;
    .restart local v23       #url:Ljava/net/URL;
    :cond_167
    :try_start_167
    const-string v26, " "

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x0

    aget-object v20, v26, v27

    .line 293
    .local v20, pulledhex:Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_1a3

    .line 294
    const-string v26, "apk"

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_195

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->apkMatch()V

    goto/16 :goto_db

    .line 312
    .end local v14           #in3:Ljava/io/BufferedReader;
    .end local v20           #pulledhex:Ljava/lang/String;
    .end local v22           #str:Ljava/lang/String;
    .end local v23           #url:Ljava/net/URL;
    :catch_192
    move-exception v26

    goto/16 :goto_e4

    .line 298
    .restart local v14       #in3:Ljava/io/BufferedReader;
    .restart local v20       #pulledhex:Ljava/lang/String;
    .restart local v22       #str:Ljava/lang/String;
    .restart local v23       #url:Ljava/net/URL;
    :cond_195
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->zipMatch()V

    goto/16 :goto_db

    .line 313
    .end local v14           #in3:Ljava/io/BufferedReader;
    .end local v20           #pulledhex:Ljava/lang/String;
    .end local v22           #str:Ljava/lang/String;
    .end local v23           #url:Ljava/net/URL;
    :catch_1a0
    move-exception v26

    goto/16 :goto_e4

    .line 302
    .restart local v14       #in3:Ljava/io/BufferedReader;
    .restart local v20       #pulledhex:Ljava/lang/String;
    .restart local v22       #str:Ljava/lang/String;
    .restart local v23       #url:Ljava/net/URL;
    :cond_1a3
    const-string v26, "apk"

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_1bb

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->apkMD5notmatch()V

    goto/16 :goto_db

    .line 336
    .end local v4           #buf:[B
    .end local v5           #bytes:[B
    .end local v11           #hex:Ljava/lang/String;
    .end local v12           #in:Ljava/io/InputStream;
    .end local v14           #in3:Ljava/io/BufferedReader;
    .end local v15           #len:I
    .end local v17           #md:Ljava/security/MessageDigest;
    .end local v20           #pulledhex:Ljava/lang/String;
    .end local v21           #sb:Ljava/lang/StringBuilder;
    .end local v22           #str:Ljava/lang/String;
    .end local v23           #url:Ljava/net/URL;
    :catch_1b8
    move-exception v26

    goto/16 :goto_131

    .line 306
    .restart local v4       #buf:[B
    .restart local v5       #bytes:[B
    .restart local v11       #hex:Ljava/lang/String;
    .restart local v12       #in:Ljava/io/InputStream;
    .restart local v14       #in3:Ljava/io/BufferedReader;
    .restart local v15       #len:I
    .restart local v17       #md:Ljava/security/MessageDigest;
    .restart local v20       #pulledhex:Ljava/lang/String;
    .restart local v21       #sb:Ljava/lang/StringBuilder;
    .restart local v22       #str:Ljava/lang/String;
    .restart local v23       #url:Ljava/net/URL;
    :cond_1bb
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->zipnotmatch()V
    :try_end_1c4
    .catch Ljava/net/MalformedURLException; {:try_start_167 .. :try_end_1c4} :catch_192
    .catch Ljava/io/IOException; {:try_start_167 .. :try_end_1c4} :catch_1a0
    .catch Ljava/io/FileNotFoundException; {:try_start_167 .. :try_end_1c4} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_167 .. :try_end_1c4} :catch_1b8

    goto/16 :goto_db

    .line 327
    .end local v14           #in3:Ljava/io/BufferedReader;
    .end local v20           #pulledhex:Ljava/lang/String;
    .end local v22           #str:Ljava/lang/String;
    .end local v23           #url:Ljava/net/URL;
    .restart local v7       #con:Ljava/net/HttpURLConnection;
    .restart local v25       #url50:Ljava/lang/String;
    :cond_1c6
    :try_start_1c6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/kuban/settings/SingleModActivity;->noMD5tomatch()V
    :try_end_1cf
    .catch Ljava/lang/Exception; {:try_start_1c6 .. :try_end_1cf} :catch_1d1
    .catch Ljava/io/FileNotFoundException; {:try_start_1c6 .. :try_end_1cf} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1c6 .. :try_end_1cf} :catch_1b8
    .catch Ljava/io/IOException; {:try_start_1c6 .. :try_end_1cf} :catch_1d7

    goto/16 :goto_131

    .line 331
    .end local v7           #con:Ljava/net/HttpURLConnection;
    :catch_1d1
    move-exception v8

    .line 332
    .local v8, e:Ljava/lang/Exception;
    :try_start_1d2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1d5
    .catch Ljava/io/FileNotFoundException; {:try_start_1d2 .. :try_end_1d5} :catch_13d
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1d2 .. :try_end_1d5} :catch_1b8
    .catch Ljava/io/IOException; {:try_start_1d2 .. :try_end_1d5} :catch_1d7

    goto/16 :goto_131

    .line 337
    .end local v4           #buf:[B
    .end local v5           #bytes:[B
    .end local v8           #e:Ljava/lang/Exception;
    .end local v11           #hex:Ljava/lang/String;
    .end local v12           #in:Ljava/io/InputStream;
    .end local v15           #len:I
    .end local v17           #md:Ljava/security/MessageDigest;
    .end local v21           #sb:Ljava/lang/StringBuilder;
    .end local v25           #url50:Ljava/lang/String;
    :catch_1d7
    move-exception v26

    goto/16 :goto_131
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 203
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 204
    iget-object v0, p0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/kuban/settings/SingleModActivity;->showDialog(I)V

    .line 205
    return-void
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->onProgressUpdate([Ljava/lang/String;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/String;)V
    .registers 4
    .parameter "progress"

    .prologue
    .line 246
    iget-object v0, p0, Lcom/kuban/settings/SingleModActivity$DownloadFileAsync;->this$0:Lcom/kuban/settings/SingleModActivity;

    #getter for: Lcom/kuban/settings/SingleModActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/kuban/settings/SingleModActivity;->access$0(Lcom/kuban/settings/SingleModActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 247
    return-void
.end method
