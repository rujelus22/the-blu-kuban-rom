.class Lcom/kuban/settings/ScriptsActivity$1;
.super Ljava/lang/Object;
.source "ScriptsActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/ScriptsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/ScriptsActivity;


# direct methods
.method constructor <init>(Lcom/kuban/settings/ScriptsActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/kuban/settings/ScriptsActivity$1;->this$0:Lcom/kuban/settings/ScriptsActivity;

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 17
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const v8, 0x7f0c003a

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, code:Ljava/lang/String;
    const v8, 0x7f0c0038

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 106
    .local v4, name:Ljava/lang/String;
    const v8, 0x7f0c003b

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 107
    .local v5, sbin:Ljava/lang/String;
    const-string v3, "/logs"

    .line 108
    .local v3, logFolder:Ljava/lang/String;
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$1;->this$0:Lcom/kuban/settings/ScriptsActivity;

    new-instance v9, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-direct {v9, v10, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v9, v8, Lcom/kuban/settings/ScriptsActivity;->logDirectory:Ljava/io/File;

    .line 109
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$1;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v8, v8, Lcom/kuban/settings/ScriptsActivity;->logDirectory:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_53

    .line 110
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$1;->this$0:Lcom/kuban/settings/ScriptsActivity;

    iget-object v8, v8, Lcom/kuban/settings/ScriptsActivity;->logDirectory:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 112
    :cond_53
    const-string v8, "True"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8f

    .line 114
    :try_start_5b
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "su -c /sbin/"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 115
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$1;->this$0:Lcom/kuban/settings/ScriptsActivity;

    invoke-virtual {v8}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 116
    .local v1, context:Landroid/content/Context;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Running "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 117
    .local v6, text:Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 118
    .local v2, duration:I
    invoke-static {v1, v6, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 119
    .local v7, toast:Landroid/widget/Toast;
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V
    :try_end_8e
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_8e} :catch_c5

    .line 132
    .end local v1           #context:Landroid/content/Context;
    .end local v2           #duration:I
    .end local v6           #text:Ljava/lang/CharSequence;
    .end local v7           #toast:Landroid/widget/Toast;
    :goto_8e
    return-void

    .line 124
    :cond_8f
    :try_start_8f
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "su -c "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 125
    iget-object v8, p0, Lcom/kuban/settings/ScriptsActivity$1;->this$0:Lcom/kuban/settings/ScriptsActivity;

    invoke-virtual {v8}, Lcom/kuban/settings/ScriptsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 126
    .restart local v1       #context:Landroid/content/Context;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Running "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 127
    .restart local v6       #text:Ljava/lang/CharSequence;
    const/4 v2, 0x0

    .line 128
    .restart local v2       #duration:I
    invoke-static {v1, v6, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    .line 129
    .restart local v7       #toast:Landroid/widget/Toast;
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V
    :try_end_c2
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_c2} :catch_c3

    goto :goto_8e

    .line 130
    .end local v1           #context:Landroid/content/Context;
    .end local v2           #duration:I
    .end local v6           #text:Ljava/lang/CharSequence;
    .end local v7           #toast:Landroid/widget/Toast;
    :catch_c3
    move-exception v8

    goto :goto_8e

    .line 120
    :catch_c5
    move-exception v8

    goto :goto_8e
.end method
