.class Lcom/kuban/settings/ChangelogActivity$HomeAsync;
.super Landroid/os/AsyncTask;
.source "ChangelogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kuban/settings/ChangelogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HomeAsync"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kuban/settings/ChangelogActivity;


# direct methods
.method private constructor <init>(Lcom/kuban/settings/ChangelogActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/kuban/settings/ChangelogActivity;Lcom/kuban/settings/ChangelogActivity$HomeAsync;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/kuban/settings/ChangelogActivity$HomeAsync;-><init>(Lcom/kuban/settings/ChangelogActivity;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/ArrayList;
    .registers 13
    .parameter "params"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 61
    const-string v8, "kuban.updater.parts"

    invoke-static {v8}, Lcom/kuban/settings/utils/GetProperty;->getSystemProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, BASEURL:Ljava/lang/String;
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 64
    :cond_b
    iget-object v8, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    const/4 v9, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x8

    invoke-virtual {v0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/kuban/settings/ChangelogActivity;->URL:Ljava/lang/String;

    .line 65
    iget-object v8, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v9, v8, Lcom/kuban/settings/ChangelogActivity;->URL:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v10, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, "Changelog.xml"

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/kuban/settings/ChangelogActivity;->URL:Ljava/lang/String;

    .line 66
    new-instance v6, Lcom/kuban/settings/utils/XMLParser;

    invoke-direct {v6}, Lcom/kuban/settings/utils/XMLParser;-><init>()V

    .line 67
    .local v6, parser:Lcom/kuban/settings/utils/XMLParser;
    iget-object v8, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v8, v8, Lcom/kuban/settings/ChangelogActivity;->URL:Ljava/lang/String;

    invoke-virtual {v6, v8}, Lcom/kuban/settings/utils/XMLParser;->getXmlFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 68
    .local v7, xml:Ljava/lang/String;
    invoke-virtual {v6, v7}, Lcom/kuban/settings/utils/XMLParser;->getDomElement(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v1

    .line 69
    .local v1, doc:Lorg/w3c/dom/Document;
    const-string v8, "Element"

    invoke-interface {v1, v8}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 70
    .local v5, nl:Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_4b
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-lt v3, v8, :cond_56

    .line 78
    iget-object v8, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v8, v8, Lcom/kuban/settings/ChangelogActivity;->menuItems2:Ljava/util/ArrayList;

    return-object v8

    .line 71
    :cond_56
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 72
    .local v4, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    .line 73
    .local v2, e:Lorg/w3c/dom/Element;
    const-string v8, "Name"

    const-string v9, "Name"

    invoke-virtual {v6, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-string v8, "Changelog"

    const-string v9, "Changelog"

    invoke-virtual {v6, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string v8, "Date"

    const-string v9, "Date"

    invoke-virtual {v6, v2, v9}, Lcom/kuban/settings/utils/XMLParser;->getValue(Lorg/w3c/dom/Element;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v8, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v8, v8, Lcom/kuban/settings/ChangelogActivity;->menuItems2:Ljava/util/ArrayList;

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v3, v3, 0x1

    goto :goto_4b
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    check-cast p1, Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, result:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ChangelogActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 83
    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    const v1, 0x7f030010

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ChangelogActivity;->setContentView(I)V

    .line 84
    iget-object v1, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    const v2, 0x7f0c0023

    invoke-virtual {v0, v2}, Lcom/kuban/settings/ChangelogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, v1, Lcom/kuban/settings/ChangelogActivity;->list:Landroid/widget/ListView;

    .line 85
    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    new-instance v1, Lcom/kuban/settings/adapters/ChangelogAdapter;

    iget-object v2, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    invoke-direct {v1, v2, p1}, Lcom/kuban/settings/adapters/ChangelogAdapter;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v1, v0, Lcom/kuban/settings/ChangelogActivity;->adapter:Lcom/kuban/settings/adapters/ChangelogAdapter;

    .line 86
    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v0, v0, Lcom/kuban/settings/ChangelogActivity;->list:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    iget-object v1, v1, Lcom/kuban/settings/ChangelogActivity;->adapter:Lcom/kuban/settings/adapters/ChangelogAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    return-void
.end method

.method protected onPreExecute()V
    .registers 3

    .prologue
    .line 56
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 57
    iget-object v0, p0, Lcom/kuban/settings/ChangelogActivity$HomeAsync;->this$0:Lcom/kuban/settings/ChangelogActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/kuban/settings/ChangelogActivity;->setProgressBarIndeterminateVisibility(Z)V

    .line 58
    return-void
.end method
