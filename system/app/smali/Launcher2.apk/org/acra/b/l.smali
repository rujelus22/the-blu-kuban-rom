.class final enum Lorg/acra/b/l;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lorg/acra/b/l;

.field public static final enum b:Lorg/acra/b/l;

.field public static final enum c:Lorg/acra/b/l;

.field public static final enum d:Lorg/acra/b/l;

.field private static final synthetic e:[Lorg/acra/b/l;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lorg/acra/b/l;

    const-string v1, "AVC"

    invoke-direct {v0, v1, v2}, Lorg/acra/b/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/acra/b/l;->a:Lorg/acra/b/l;

    new-instance v0, Lorg/acra/b/l;

    const-string v1, "H263"

    invoke-direct {v0, v1, v3}, Lorg/acra/b/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/acra/b/l;->b:Lorg/acra/b/l;

    new-instance v0, Lorg/acra/b/l;

    const-string v1, "MPEG4"

    invoke-direct {v0, v1, v4}, Lorg/acra/b/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/acra/b/l;->c:Lorg/acra/b/l;

    new-instance v0, Lorg/acra/b/l;

    const-string v1, "AAC"

    invoke-direct {v0, v1, v5}, Lorg/acra/b/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/acra/b/l;->d:Lorg/acra/b/l;

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [Lorg/acra/b/l;

    sget-object v1, Lorg/acra/b/l;->a:Lorg/acra/b/l;

    aput-object v1, v0, v2

    sget-object v1, Lorg/acra/b/l;->b:Lorg/acra/b/l;

    aput-object v1, v0, v3

    sget-object v1, Lorg/acra/b/l;->c:Lorg/acra/b/l;

    aput-object v1, v0, v4

    sget-object v1, Lorg/acra/b/l;->d:Lorg/acra/b/l;

    aput-object v1, v0, v5

    sput-object v0, Lorg/acra/b/l;->e:[Lorg/acra/b/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/acra/b/l;
    .registers 2
    .parameter

    .prologue
    .line 35
    const-class v0, Lorg/acra/b/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/acra/b/l;

    return-object v0
.end method

.method public static values()[Lorg/acra/b/l;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Lorg/acra/b/l;->e:[Lorg/acra/b/l;

    invoke-virtual {v0}, [Lorg/acra/b/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/acra/b/l;

    return-object v0
.end method
