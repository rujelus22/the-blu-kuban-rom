.class final Lorg/acra/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lorg/acra/l;->a:Landroid/content/Context;

    .line 51
    return-void
.end method

.method private declared-synchronized a(Ljava/io/Reader;)Lorg/acra/b/c;
    .registers 16
    .parameter

    .prologue
    .line 162
    monitor-enter p0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 163
    const/16 v0, 0x28

    :try_start_6
    new-array v3, v0, [C

    .line 164
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 165
    const/4 v0, 0x1

    .line 167
    new-instance v9, Lorg/acra/b/c;

    invoke-direct {v9}, Lorg/acra/b/c;-><init>()V

    .line 168
    new-instance v10, Ljava/io/BufferedReader;

    const/16 v7, 0x2000

    invoke-direct {v10, p1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move v7, v0

    move v0, v1

    move v1, v4

    move v4, v5

    move v5, v6

    move-object v6, v3

    .line 171
    :goto_1d
    invoke-virtual {v10}, Ljava/io/BufferedReader;->read()I

    move-result v3

    .line 172
    const/4 v8, -0x1

    if-eq v3, v8, :cond_143

    .line 173
    int-to-char v3, v3

    .line 177
    array-length v8, v6

    if-ne v2, v8, :cond_1a6

    .line 178
    array-length v8, v6

    mul-int/lit8 v8, v8, 0x2

    new-array v8, v8, [C

    .line 179
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v6, v11, v8, v12, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    :goto_32
    const/4 v6, 0x2

    if-ne v5, v6, :cond_19f

    .line 183
    const/16 v6, 0x10

    invoke-static {v3, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    .line 184
    if-ltz v6, :cond_49

    .line 185
    shl-int/lit8 v4, v4, 0x4

    add-int/2addr v6, v4

    .line 186
    add-int/lit8 v4, v1, 0x1

    const/4 v1, 0x4

    if-ge v4, v1, :cond_19c

    move v1, v4

    move v4, v6

    move-object v6, v8

    .line 187
    goto :goto_1d

    .line 189
    :cond_49
    const/4 v5, 0x4

    if-gt v1, v5, :cond_57

    .line 191
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "luni.09"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_54
    .catchall {:try_start_6 .. :try_end_54} :catchall_54

    .line 162
    :catchall_54
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_57
    move v5, v4

    move v4, v1

    .line 193
    :goto_59
    const/4 v6, 0x0

    .line 194
    add-int/lit8 v1, v2, 0x1

    int-to-char v11, v5

    :try_start_5d
    aput-char v11, v8, v2

    .line 195
    const/16 v2, 0xa

    if-eq v3, v2, :cond_67

    const/16 v2, 0x85

    if-ne v3, v2, :cond_195

    :cond_67
    move v2, v6

    .line 196
    :goto_68
    const/4 v6, 0x1

    if-ne v2, v6, :cond_a8

    .line 200
    const/4 v2, 0x0

    .line 201
    sparse-switch v3, :sswitch_data_1aa

    .line 301
    :cond_6f
    :goto_6f
    const/4 v6, 0x0

    .line 302
    const/4 v7, 0x4

    if-ne v2, v7, :cond_76

    .line 304
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 306
    :cond_76
    add-int/lit8 v7, v1, 0x1

    aput-char v3, v8, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v7

    move v7, v6

    move-object v6, v8

    goto :goto_1d

    .line 203
    :sswitch_81
    const/4 v2, 0x3

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 204
    goto :goto_1d

    .line 207
    :sswitch_89
    const/4 v2, 0x5

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 208
    goto :goto_1d

    .line 210
    :sswitch_91
    const/16 v3, 0x8

    .line 211
    goto :goto_6f

    .line 213
    :sswitch_94
    const/16 v3, 0xc

    .line 214
    goto :goto_6f

    .line 216
    :sswitch_97
    const/16 v3, 0xa

    .line 217
    goto :goto_6f

    .line 219
    :sswitch_9a
    const/16 v3, 0xd

    .line 220
    goto :goto_6f

    .line 222
    :sswitch_9d
    const/16 v3, 0x9

    .line 223
    goto :goto_6f

    .line 225
    :sswitch_a0
    const/4 v2, 0x2

    .line 226
    const/4 v4, 0x0

    move-object v6, v8

    move v5, v2

    move v2, v1

    move v1, v4

    .line 227
    goto/16 :goto_1d

    .line 230
    :cond_a8
    sparse-switch v3, :sswitch_data_1d0

    .line 284
    :cond_ab
    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_13a

    .line 285
    const/4 v6, 0x3

    if-ne v2, v6, :cond_b5

    .line 286
    const/4 v2, 0x5

    .line 289
    :cond_b5
    if-eqz v1, :cond_18d

    if-eq v1, v0, :cond_18d

    const/4 v6, 0x5

    if-eq v2, v6, :cond_18d

    .line 290
    const/4 v6, -0x1

    if-ne v0, v6, :cond_13a

    .line 293
    const/4 v2, 0x4

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 294
    goto/16 :goto_1d

    .line 233
    :sswitch_c8
    if-eqz v7, :cond_ab

    .line 235
    :cond_ca
    invoke-virtual {v10}, Ljava/io/BufferedReader;->read()I

    move-result v3

    .line 236
    const/4 v6, -0x1

    if-eq v3, v6, :cond_18d

    .line 237
    int-to-char v3, v3

    .line 242
    const/16 v6, 0xd

    if-eq v3, v6, :cond_18d

    const/16 v6, 0xa

    if-eq v3, v6, :cond_18d

    const/16 v6, 0x85

    if-ne v3, v6, :cond_ca

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 243
    goto/16 :goto_1d

    .line 250
    :sswitch_e6
    const/4 v3, 0x3

    if-ne v2, v3, :cond_f2

    .line 251
    const/4 v2, 0x5

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 252
    goto/16 :goto_1d

    .line 257
    :cond_f2
    :sswitch_f2
    const/4 v6, 0x0

    .line 258
    const/4 v2, 0x1

    .line 259
    if-gtz v1, :cond_fa

    if-nez v1, :cond_116

    if-nez v0, :cond_116

    .line 260
    :cond_fa
    const/4 v3, -0x1

    if-ne v0, v3, :cond_fe

    move v0, v1

    .line 263
    :cond_fe
    new-instance v3, Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct {v3, v8, v7, v1}, Ljava/lang/String;-><init>([CII)V

    .line 264
    const-class v1, Lorg/acra/ReportField;

    const/4 v7, 0x0

    invoke-virtual {v3, v7, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v1

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Lorg/acra/b/c;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 266
    :cond_116
    const/4 v0, -0x1

    .line 267
    const/4 v1, 0x0

    move v7, v2

    move v2, v1

    move v1, v4

    move v4, v5

    move v5, v6

    move-object v6, v8

    .line 268
    goto/16 :goto_1d

    .line 270
    :sswitch_120
    const/4 v3, 0x4

    if-ne v2, v3, :cond_124

    move v0, v1

    .line 273
    :cond_124
    const/4 v2, 0x1

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 274
    goto/16 :goto_1d

    .line 277
    :sswitch_12d
    const/4 v6, -0x1

    if-ne v0, v6, :cond_ab

    .line 278
    const/4 v2, 0x0

    move v0, v1

    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    .line 280
    goto/16 :goto_1d

    .line 297
    :cond_13a
    const/4 v6, 0x5

    if-eq v2, v6, :cond_140

    const/4 v6, 0x3

    if-ne v2, v6, :cond_6f

    .line 298
    :cond_140
    const/4 v2, 0x0

    goto/16 :goto_6f

    .line 308
    :cond_143
    const/4 v3, 0x2

    if-ne v5, v3, :cond_151

    const/4 v3, 0x4

    if-gt v1, v3, :cond_151

    .line 310
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "luni.08"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_151
    const/4 v1, -0x1

    if-ne v0, v1, :cond_18b

    if-lez v2, :cond_18b

    move v1, v2

    .line 315
    :goto_157
    if-ltz v1, :cond_189

    .line 316
    new-instance v3, Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {v3, v6, v0, v2}, Ljava/lang/String;-><init>([CII)V

    .line 317
    const-class v0, Lorg/acra/ReportField;

    const/4 v2, 0x0

    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/acra/ReportField;

    .line 318
    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 319
    const/4 v2, 0x1

    if-ne v5, v2, :cond_186

    .line 320
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u0000"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 322
    :cond_186
    invoke-virtual {v9, v0, v1}, Lorg/acra/b/c;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_189
    .catchall {:try_start_5d .. :try_end_189} :catchall_54

    .line 325
    :cond_189
    monitor-exit p0

    return-object v9

    :cond_18b
    move v1, v0

    goto :goto_157

    :cond_18d
    move-object v6, v8

    move v13, v1

    move v1, v4

    move v4, v5

    move v5, v2

    move v2, v13

    goto/16 :goto_1d

    :cond_195
    move v2, v1

    move v1, v4

    move v4, v5

    move v5, v6

    move-object v6, v8

    goto/16 :goto_1d

    :cond_19c
    move v5, v6

    goto/16 :goto_59

    :cond_19f
    move v13, v2

    move v2, v5

    move v5, v4

    move v4, v1

    move v1, v13

    goto/16 :goto_68

    :cond_1a6
    move-object v8, v6

    goto/16 :goto_32

    .line 201
    nop

    :sswitch_data_1aa
    .sparse-switch
        0xa -> :sswitch_89
        0xd -> :sswitch_81
        0x62 -> :sswitch_91
        0x66 -> :sswitch_94
        0x6e -> :sswitch_97
        0x72 -> :sswitch_9a
        0x74 -> :sswitch_9d
        0x75 -> :sswitch_a0
        0x85 -> :sswitch_89
    .end sparse-switch

    .line 230
    :sswitch_data_1d0
    .sparse-switch
        0xa -> :sswitch_e6
        0xd -> :sswitch_f2
        0x21 -> :sswitch_c8
        0x23 -> :sswitch_c8
        0x3a -> :sswitch_12d
        0x3d -> :sswitch_12d
        0x5c -> :sswitch_120
        0x85 -> :sswitch_f2
    .end sparse-switch
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x20

    const/4 v1, 0x0

    .line 336
    .line 337
    if-nez p2, :cond_78

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_78

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v5, :cond_78

    .line 338
    const-string v0, "\\ "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    const/4 v0, 0x1

    .line 342
    :goto_17
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_77

    .line 343
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 344
    packed-switch v2, :pswitch_data_7a

    .line 358
    :pswitch_24
    const-string v3, "\\#!=:"

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-gez v3, :cond_30

    if-eqz p2, :cond_35

    if-ne v2, v5, :cond_35

    .line 359
    :cond_30
    const/16 v3, 0x5c

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 361
    :cond_35
    if-lt v2, v5, :cond_59

    const/16 v3, 0x7e

    if-gt v2, v3, :cond_59

    .line 362
    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 342
    :goto_3e
    add-int/lit8 v0, v0, 0x1

    goto :goto_17

    .line 346
    :pswitch_41
    const-string v2, "\\t"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3e

    .line 349
    :pswitch_47
    const-string v2, "\\n"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3e

    .line 352
    :pswitch_4d
    const-string v2, "\\f"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3e

    .line 355
    :pswitch_53
    const-string v2, "\\r"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3e

    .line 364
    :cond_59
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    .line 365
    const-string v2, "\\u"

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v2, v1

    .line 366
    :goto_63
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    rsub-int/lit8 v4, v4, 0x4

    if-ge v2, v4, :cond_73

    .line 367
    const-string v4, "0"

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    add-int/lit8 v2, v2, 0x1

    goto :goto_63

    .line 369
    :cond_73
    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3e

    .line 373
    :cond_77
    return-void

    :cond_78
    move v0, v1

    goto :goto_17

    .line 344
    :pswitch_data_7a
    .packed-switch 0x9
        :pswitch_41
        :pswitch_47
        :pswitch_24
        :pswitch_4d
        :pswitch_53
    .end packed-switch
.end method

.method private static a(Ljava/io/BufferedInputStream;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 117
    :cond_1
    invoke-virtual {p0}, Ljava/io/BufferedInputStream;->read()I

    move-result v1

    int-to-byte v1, v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_15

    .line 118
    const/16 v2, 0x23

    if-eq v1, v2, :cond_15

    const/16 v2, 0xa

    if-eq v1, v2, :cond_15

    const/16 v2, 0x3d

    if-ne v1, v2, :cond_16

    .line 130
    :cond_15
    :goto_15
    return v0

    .line 121
    :cond_16
    const/16 v2, 0x15

    if-ne v1, v2, :cond_1

    .line 122
    const/4 v0, 0x1

    goto :goto_15
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lorg/acra/b/c;
    .registers 6
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lorg/acra/l;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    .line 64
    if-nez v1, :cond_1d

    .line 65
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid crash report fileName : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1d
    :try_start_1d
    new-instance v0, Ljava/io/BufferedInputStream;

    const/16 v2, 0x2000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 70
    const v2, 0x7fffffff

    invoke-virtual {v0, v2}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 71
    invoke-static {v0}, Lorg/acra/l;->a(Ljava/io/BufferedInputStream;)Z

    move-result v2

    .line 72
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->reset()V

    .line 74
    if-nez v2, :cond_42

    .line 75
    new-instance v2, Ljava/io/InputStreamReader;

    const-string v3, "ISO8859-1"

    invoke-direct {v2, v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lorg/acra/l;->a(Ljava/io/Reader;)Lorg/acra/b/c;
    :try_end_3d
    .catchall {:try_start_1d .. :try_end_3d} :catchall_4f

    move-result-object v0

    .line 80
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    :goto_41
    return-object v0

    .line 77
    :cond_42
    :try_start_42
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v2}, Lorg/acra/l;->a(Ljava/io/Reader;)Lorg/acra/b/c;
    :try_end_4a
    .catchall {:try_start_42 .. :try_end_4a} :catchall_4f

    move-result-object v0

    .line 80
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    goto :goto_41

    :catchall_4f
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    throw v0
.end method

.method public final a(Lorg/acra/b/c;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lorg/acra/l;->a:Landroid/content/Context;

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    .line 97
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v0, 0xc8

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 98
    new-instance v4, Ljava/io/OutputStreamWriter;

    const-string v0, "ISO8859_1"

    invoke-direct {v4, v2, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Lorg/acra/b/c;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1d
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 101
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/acra/ReportField;

    invoke-virtual {v1}, Lorg/acra/ReportField;->toString()Ljava/lang/String;

    move-result-object v1

    .line 102
    const/4 v6, 0x1

    invoke-static {v3, v1, v6}, Lorg/acra/l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 103
    const/16 v1, 0x3d

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 104
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v3, v0, v1}, Lorg/acra/l;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    .line 105
    const-string v0, "\n"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 107
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_56
    .catchall {:try_start_7 .. :try_end_56} :catchall_57

    goto :goto_1d

    .line 111
    :catchall_57
    move-exception v0

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    throw v0

    .line 109
    :cond_5c
    :try_start_5c
    invoke-virtual {v4}, Ljava/io/OutputStreamWriter;->flush()V
    :try_end_5f
    .catchall {:try_start_5c .. :try_end_5f} :catchall_57

    .line 111
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 112
    return-void
.end method
