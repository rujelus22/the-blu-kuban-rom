.class final Lorg/acra/e/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/client/HttpRequestRetryHandler;


# instance fields
.field private final a:Lorg/apache/http/params/HttpParams;

.field private final b:I


# direct methods
.method private constructor <init>(Lorg/apache/http/params/HttpParams;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lorg/acra/e/d;->a:Lorg/apache/http/params/HttpParams;

    .line 56
    iput p2, p0, Lorg/acra/e/d;->b:I

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/http/params/HttpParams;IB)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lorg/acra/e/d;-><init>(Lorg/apache/http/params/HttpParams;I)V

    return-void
.end method


# virtual methods
.method public final retryRequest(Ljava/io/IOException;ILorg/apache/http/protocol/HttpContext;)Z
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    instance-of v0, p1, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_5f

    .line 62
    iget v0, p0, Lorg/acra/e/d;->b:I

    if-gt p2, v0, :cond_45

    .line 64
    iget-object v0, p0, Lorg/acra/e/d;->a:Lorg/apache/http/params/HttpParams;

    if-eqz v0, :cond_39

    .line 65
    iget-object v0, p0, Lorg/acra/e/d;->a:Lorg/apache/http/params/HttpParams;

    invoke-static {v0}, Lorg/apache/http/params/HttpConnectionParams;->getSoTimeout(Lorg/apache/http/params/HttpParams;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 66
    iget-object v1, p0, Lorg/acra/e/d;->a:Lorg/apache/http/params/HttpParams;

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 67
    invoke-static {}, Lorg/acra/e/c;->a()Lorg/acra/c/a;

    move-result-object v1

    sget-object v2, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SocketTimeOut - increasing time out to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " millis and trying again"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lorg/acra/c/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :goto_37
    const/4 v0, 0x1

    .line 78
    :goto_38
    return v0

    .line 69
    :cond_39
    invoke-static {}, Lorg/acra/e/c;->a()Lorg/acra/c/a;

    move-result-object v0

    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v2, "SocketTimeOut - no HttpParams, cannot increase time out. Trying again with current settings"

    invoke-interface {v0, v1, v2}, Lorg/acra/c/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_37

    .line 75
    :cond_45
    invoke-static {}, Lorg/acra/e/c;->a()Lorg/acra/c/a;

    move-result-object v0

    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SocketTimeOut but exceeded max number of retries : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lorg/acra/e/d;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/acra/c/a;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_5f
    const/4 v0, 0x0

    goto :goto_38
.end method
