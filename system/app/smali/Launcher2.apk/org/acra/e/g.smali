.class public final Lorg/acra/e/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lorg/acra/e/g;->a:Landroid/content/Context;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/pm/PackageInfo;
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 57
    iget-object v1, p0, Lorg/acra/e/g;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 58
    if-nez v1, :cond_a

    .line 70
    :goto_9
    return-object v0

    .line 63
    :cond_a
    :try_start_a
    iget-object v2, p0, Lorg/acra/e/g;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_14
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_14} :catch_16
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_14} :catch_32

    move-result-object v0

    goto :goto_9

    .line 65
    :catch_16
    move-exception v1

    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to find PackageInfo for current App : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lorg/acra/e/g;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 70
    :catch_32
    move-exception v1

    goto :goto_9
.end method

.method public final a(Ljava/lang/String;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 39
    iget-object v1, p0, Lorg/acra/e/g;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 40
    if-nez v1, :cond_a

    .line 49
    :cond_9
    :goto_9
    return v0

    .line 45
    :cond_a
    :try_start_a
    iget-object v2, p0, Lorg/acra/e/g;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_13} :catch_18

    move-result v1

    if-nez v1, :cond_9

    const/4 v0, 0x1

    goto :goto_9

    .line 49
    :catch_18
    move-exception v1

    goto :goto_9
.end method
