.class final Lorg/acra/n;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lorg/acra/ErrorReporter;


# direct methods
.method constructor <init>(Lorg/acra/ErrorReporter;)V
    .registers 2
    .parameter

    .prologue
    .line 616
    iput-object p1, p0, Lorg/acra/n;->a:Lorg/acra/ErrorReporter;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 10

    .prologue
    const-wide/16 v7, 0xbb8

    const/4 v6, 0x0

    .line 620
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 621
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 622
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 623
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    .line 624
    const-wide/16 v0, 0x0

    .line 625
    :goto_16
    cmp-long v0, v0, v7

    if-gez v0, :cond_31

    .line 628
    const-wide/16 v0, 0xbb8

    :try_start_1c
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1f
    .catch Ljava/lang/InterruptedException; {:try_start_1c .. :try_end_1f} :catch_28

    .line 632
    :goto_1f
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 633
    invoke-virtual {v2, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    sub-long/2addr v0, v3

    goto :goto_16

    .line 629
    :catch_28
    move-exception v0

    .line 630
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Interrupted while waiting for Toast to end."

    invoke-static {v1, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1f

    .line 635
    :cond_31
    invoke-static {}, Lorg/acra/ErrorReporter;->b()Z

    .line 636
    return-void
.end method
