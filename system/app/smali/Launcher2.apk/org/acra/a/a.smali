.class public interface abstract annotation Lorg/acra/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lorg/acra/a/a;
        A = 0x0
        B = 0x0
        C = 0x0
        D = ""
        E = 0x1388
        F = false
        G = true
        H = {}
        I = ""
        J = 0x64
        K = "https://docs.google.com/spreadsheet/formResponse?formkey=%s&ifq"
        a = {}
        b = {}
        c = 0xbb8
        d = {}
        e = true
        f = true
        g = 0x5
        h = false
        j = ""
        k = "ACRA-NULL-STRING"
        l = "ACRA-NULL-STRING"
        m = false
        n = {
            "-t",
            "100",
            "-v",
            "time"
        }
        o = ""
        p = 0x3
        q = .enum Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;
        r = 0x0
        s = 0x0
        t = 0x1080027
        u = 0x0
        v = 0x0
        w = 0x0
        x = 0x1080078
        y = 0x0
        z = 0x0
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Documented;
.end annotation

.annotation runtime Ljava/lang/annotation/Inherited;
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->TYPE:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract A()I
.end method

.method public abstract B()I
.end method

.method public abstract C()I
.end method

.method public abstract D()Ljava/lang/String;
.end method

.method public abstract E()I
.end method

.method public abstract F()Z
.end method

.method public abstract G()Z
.end method

.method public abstract H()[Ljava/lang/String;
.end method

.method public abstract I()Ljava/lang/String;
.end method

.method public abstract J()I
.end method

.method public abstract K()Ljava/lang/String;
.end method

.method public abstract a()[Ljava/lang/String;
.end method

.method public abstract b()[Ljava/lang/String;
.end method

.method public abstract c()I
.end method

.method public abstract d()[Lorg/acra/ReportField;
.end method

.method public abstract e()Z
.end method

.method public abstract f()Z
.end method

.method public abstract g()I
.end method

.method public abstract h()Z
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public abstract l()Ljava/lang/String;
.end method

.method public abstract m()Z
.end method

.method public abstract n()[Ljava/lang/String;
.end method

.method public abstract o()Ljava/lang/String;
.end method

.method public abstract p()I
.end method

.method public abstract q()Lorg/acra/ReportingInteractionMode;
.end method

.method public abstract r()I
.end method

.method public abstract s()I
.end method

.method public abstract t()I
.end method

.method public abstract u()I
.end method

.method public abstract v()I
.end method

.method public abstract w()I
.end method

.method public abstract x()I
.end method

.method public abstract y()I
.end method

.method public abstract z()I
.end method
