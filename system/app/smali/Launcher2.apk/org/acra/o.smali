.class final Lorg/acra/o;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lorg/acra/p;

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Z

.field final synthetic e:Lorg/acra/ErrorReporter;


# direct methods
.method constructor <init>(Lorg/acra/ErrorReporter;Lorg/acra/p;ZLjava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 646
    iput-object p1, p0, Lorg/acra/o;->e:Lorg/acra/ErrorReporter;

    iput-object p2, p0, Lorg/acra/o;->a:Lorg/acra/p;

    iput-boolean p3, p0, Lorg/acra/o;->b:Z

    iput-object p4, p0, Lorg/acra/o;->c:Ljava/lang/String;

    iput-boolean p5, p0, Lorg/acra/o;->d:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 4

    .prologue
    .line 652
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Waiting for Toast + worker..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    :goto_7
    invoke-static {}, Lorg/acra/ErrorReporter;->c()Z

    move-result v0

    if-eqz v0, :cond_19

    iget-object v0, p0, Lorg/acra/o;->a:Lorg/acra/p;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lorg/acra/o;->a:Lorg/acra/p;

    invoke-virtual {v0}, Lorg/acra/p;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 655
    :cond_19
    const-wide/16 v0, 0x64

    :try_start_1b
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1e
    .catch Ljava/lang/InterruptedException; {:try_start_1b .. :try_end_1e} :catch_1f

    goto :goto_7

    .line 656
    :catch_1f
    move-exception v0

    .line 657
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Error : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    .line 661
    :cond_28
    iget-boolean v0, p0, Lorg/acra/o;->b:Z

    if-eqz v0, :cond_3a

    .line 666
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "About to create DIALOG from #handleException"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    iget-object v0, p0, Lorg/acra/o;->e:Lorg/acra/ErrorReporter;

    iget-object v1, p0, Lorg/acra/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lorg/acra/ErrorReporter;->a(Ljava/lang/String;)V

    .line 670
    :cond_3a
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wait for Toast + worker ended. Kill Application ? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lorg/acra/o;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget-boolean v0, p0, Lorg/acra/o;->d:Z

    if-eqz v0, :cond_59

    .line 673
    iget-object v0, p0, Lorg/acra/o;->e:Lorg/acra/ErrorReporter;

    invoke-static {v0}, Lorg/acra/ErrorReporter;->b(Lorg/acra/ErrorReporter;)V

    .line 675
    :cond_59
    return-void
.end method
