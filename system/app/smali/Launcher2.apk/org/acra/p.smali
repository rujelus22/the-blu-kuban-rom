.class final Lorg/acra/p;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Z

.field private final c:Z

.field private final d:Lorg/acra/i;

.field private final e:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 42
    new-instance v0, Lorg/acra/i;

    invoke-direct {v0}, Lorg/acra/i;-><init>()V

    iput-object v0, p0, Lorg/acra/p;->d:Lorg/acra/i;

    .line 61
    iput-object p1, p0, Lorg/acra/p;->a:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lorg/acra/p;->e:Ljava/util/List;

    .line 63
    iput-boolean p3, p0, Lorg/acra/p;->b:Z

    .line 64
    iput-boolean p4, p0, Lorg/acra/p;->c:Z

    .line 65
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 198
    invoke-virtual {p0, p1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    move-result v0

    .line 199
    if-nez v0, :cond_1a

    .line 200
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not delete error report : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_1a
    return-void
.end method

.method private a(Landroid/content/Context;Z)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 120
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v2, "#checkAndSendReports - start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    new-instance v1, Lorg/acra/j;

    invoke-direct {v1, p1}, Lorg/acra/j;-><init>(Landroid/content/Context;)V

    .line 122
    invoke-virtual {v1}, Lorg/acra/j;->a()[Ljava/lang/String;

    move-result-object v2

    .line 123
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 127
    array-length v3, v2

    move v1, v0

    :goto_16
    if-ge v1, v3, :cond_69

    aget-object v4, v2, v1

    .line 128
    if-eqz p2, :cond_26

    iget-object v5, p0, Lorg/acra/p;->d:Lorg/acra/i;

    sget-object v5, Lorg/acra/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4e

    .line 129
    :cond_26
    const/4 v5, 0x5

    if-ge v0, v5, :cond_69

    .line 133
    sget-object v5, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Sending file "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :try_start_3d
    new-instance v5, Lorg/acra/l;

    invoke-direct {v5, p1}, Lorg/acra/l;-><init>(Landroid/content/Context;)V

    .line 140
    invoke-virtual {v5, v4}, Lorg/acra/l;->a(Ljava/lang/String;)Lorg/acra/b/c;

    move-result-object v5

    .line 141
    invoke-direct {p0, v5}, Lorg/acra/p;->a(Lorg/acra/b/c;)V

    .line 142
    invoke-static {p1, v4}, Lorg/acra/p;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_4c
    .catch Ljava/lang/RuntimeException; {:try_start_3d .. :try_end_4c} :catch_51
    .catch Ljava/io/IOException; {:try_start_3d .. :try_end_4c} :catch_71
    .catch Lorg/acra/d/f; {:try_start_3d .. :try_end_4c} :catch_8a

    .line 158
    add-int/lit8 v0, v0, 0x1

    .line 127
    :cond_4e
    add-int/lit8 v1, v1, 0x1

    goto :goto_16

    .line 143
    :catch_51
    move-exception v0

    .line 144
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to send crash reports for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 145
    invoke-static {p1, v4}, Lorg/acra/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 160
    :cond_69
    :goto_69
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "#checkAndSendReports - finish"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void

    .line 148
    :catch_71
    move-exception v0

    .line 149
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load crash report for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 150
    invoke-static {p1, v4}, Lorg/acra/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_69

    .line 153
    :catch_8a
    move-exception v0

    .line 154
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to send crash report for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_69
.end method

.method private a(Lorg/acra/b/c;)V
    .registers 8
    .parameter

    .prologue
    .line 174
    invoke-static {}, Lorg/acra/ACRA;->isDebuggable()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->G()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 175
    :cond_10
    const/4 v0, 0x0

    .line 176
    iget-object v1, p0, Lorg/acra/p;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/acra/d/e;

    .line 178
    :try_start_24
    invoke-interface {v0, p1}, Lorg/acra/d/e;->a(Lorg/acra/b/c;)V
    :try_end_27
    .catch Lorg/acra/d/f; {:try_start_24 .. :try_end_27} :catch_2a

    .line 181
    const/4 v0, 0x1

    move v1, v0

    .line 192
    goto :goto_18

    .line 182
    :catch_2a
    move-exception v3

    .line 183
    if-nez v1, :cond_2e

    .line 184
    throw v3

    .line 187
    :cond_2e
    sget-object v3, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ReportSender of class "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " failed but other senders completed their task. ACRA will not send this report again."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_18

    .line 195
    :cond_51
    return-void
.end method


# virtual methods
.method public final run()V
    .registers 9

    .prologue
    .line 74
    iget-boolean v0, p0, Lorg/acra/p;->c:Z

    if-eqz v0, :cond_69

    .line 75
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Mark all pending reports as approved."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lorg/acra/j;

    iget-object v1, p0, Lorg/acra/p;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/acra/j;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lorg/acra/j;->a()[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v2, :cond_69

    aget-object v3, v1, v0

    iget-object v4, p0, Lorg/acra/p;->d:Lorg/acra/i;

    invoke-static {v3}, Lorg/acra/i;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_66

    new-instance v4, Ljava/io/File;

    iget-object v5, p0, Lorg/acra/p;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-direct {v4, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v5, ".stacktrace"

    const-string v6, "-approved.stacktrace"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lorg/acra/p;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_66

    sget-object v3, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Could not rename approved report from "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " to "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_66
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 77
    :cond_69
    iget-object v0, p0, Lorg/acra/p;->a:Landroid/content/Context;

    iget-boolean v1, p0, Lorg/acra/p;->b:Z

    invoke-direct {p0, v0, v1}, Lorg/acra/p;->a(Landroid/content/Context;Z)V

    .line 78
    return-void
.end method
