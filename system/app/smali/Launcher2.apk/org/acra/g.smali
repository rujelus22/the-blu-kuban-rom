.class final Lorg/acra/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lorg/acra/f;


# direct methods
.method constructor <init>(Lorg/acra/f;)V
    .registers 2
    .parameter

    .prologue
    .line 129
    iput-object p1, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-static {v0}, Lorg/acra/f;->a(Lorg/acra/f;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_ab

    iget-object v0, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-static {v0}, Lorg/acra/f;->a(Lorg/acra/f;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    :goto_16
    iget-object v1, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-static {v1}, Lorg/acra/f;->b(Lorg/acra/f;)Landroid/content/SharedPreferences;

    move-result-object v1

    if-eqz v1, :cond_af

    iget-object v1, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-static {v1}, Lorg/acra/f;->c(Lorg/acra/f;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_af

    .line 139
    iget-object v1, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-static {v1}, Lorg/acra/f;->c(Lorg/acra/f;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    iget-object v2, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-static {v2}, Lorg/acra/f;->b(Lorg/acra/f;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 141
    const-string v3, "acra.user.email"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 142
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    :goto_46
    new-instance v2, Lorg/acra/l;

    iget-object v3, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-virtual {v3}, Lorg/acra/f;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/acra/l;-><init>(Landroid/content/Context;)V

    .line 149
    :try_start_51
    sget-object v3, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Add user comment to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lorg/acra/g;->a:Lorg/acra/f;

    iget-object v5, v5, Lorg/acra/f;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v3, p0, Lorg/acra/g;->a:Lorg/acra/f;

    iget-object v3, v3, Lorg/acra/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lorg/acra/l;->a(Ljava/lang/String;)Lorg/acra/b/c;

    move-result-object v3

    .line 151
    sget-object v4, Lorg/acra/ReportField;->USER_COMMENT:Lorg/acra/ReportField;

    invoke-virtual {v3, v4, v0}, Lorg/acra/b/c;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lorg/acra/ReportField;->USER_EMAIL:Lorg/acra/ReportField;

    invoke-virtual {v3, v0, v1}, Lorg/acra/b/c;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lorg/acra/g;->a:Lorg/acra/f;

    iget-object v0, v0, Lorg/acra/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lorg/acra/l;->a(Lorg/acra/b/c;Ljava/lang/String;)V
    :try_end_82
    .catch Ljava/io/IOException; {:try_start_51 .. :try_end_82} :catch_b2

    .line 159
    :goto_82
    sget-object v0, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v1, "About to start SenderWorker from CrashReportDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-static {}, Lorg/acra/ACRA;->getErrorReporter()Lorg/acra/ErrorReporter;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/acra/ErrorReporter;->a(ZZ)Lorg/acra/p;

    .line 163
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/c;

    move-result-object v0

    invoke-virtual {v0}, Lorg/acra/c;->u()I

    move-result v0

    .line 164
    if-eqz v0, :cond_a5

    .line 165
    iget-object v1, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-virtual {v1}, Lorg/acra/f;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/acra/e/i;->a(Landroid/content/Context;I)V

    .line 167
    :cond_a5
    iget-object v0, p0, Lorg/acra/g;->a:Lorg/acra/f;

    invoke-virtual {v0}, Lorg/acra/f;->finish()V

    .line 168
    return-void

    .line 134
    :cond_ab
    const-string v0, ""

    goto/16 :goto_16

    .line 144
    :cond_af
    const-string v1, ""

    goto :goto_46

    .line 154
    :catch_b2
    move-exception v0

    .line 155
    sget-object v1, Lorg/acra/ACRA;->LOG_TAG:Ljava/lang/String;

    const-string v2, "User comment not added: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_82
.end method
