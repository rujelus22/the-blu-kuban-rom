.class public final Lcom/anddoes/launcher/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Lcom/anddoes/launcher/preference/c;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/ArrayList;

.field public g:Ljava/util/ArrayList;

.field public h:I

.field public i:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/preference/c;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    .line 29
    iput-object p1, p0, Lcom/anddoes/launcher/p;->d:Lcom/anddoes/launcher/preference/c;

    .line 30
    iput-object p2, p0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/anddoes/launcher/p;->b:Ljava/lang/String;

    .line 32
    iput-boolean p4, p0, Lcom/anddoes/launcher/p;->c:Z

    .line 33
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Comparator;
    .registers 2
    .parameter

    .prologue
    .line 89
    const-string v0, "INSTALL_DATE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 90
    sget-object v0, Lcom/android/launcher2/gb;->n:Ljava/util/Comparator;

    .line 96
    :goto_a
    return-object v0

    .line 91
    :cond_b
    const-string v0, "INSTALL_DATE_ASC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 92
    sget-object v0, Lcom/android/launcher2/gb;->o:Ljava/util/Comparator;

    goto :goto_a

    .line 93
    :cond_16
    const-string v0, "MOSTLY_USED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 94
    sget-object v0, Lcom/android/launcher2/gb;->m:Ljava/util/Comparator;

    goto :goto_a

    .line 96
    :cond_21
    sget-object v0, Lcom/android/launcher2/gb;->i:Ljava/util/Comparator;

    goto :goto_a
.end method

.method private f()V
    .registers 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 37
    iget-object v0, p0, Lcom/anddoes/launcher/p;->d:Lcom/anddoes/launcher/preference/c;

    iget-object v1, p0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/preference/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    .line 39
    :cond_e
    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 250
    iget-boolean v0, p0, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v0, :cond_f

    .line 251
    const-string v0, "VERTICAL_CONTINUOUS"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 252
    iput v2, p0, Lcom/anddoes/launcher/p;->h:I

    .line 261
    :cond_f
    :goto_f
    return-void

    .line 254
    :cond_10
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    .line 255
    mul-int v1, p1, p2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 254
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/anddoes/launcher/p;->h:I

    .line 256
    iget v0, p0, Lcom/anddoes/launcher/p;->h:I

    if-gtz v0, :cond_f

    .line 257
    iput v2, p0, Lcom/anddoes/launcher/p;->h:I

    goto :goto_f
.end method

.method public final a(Lcom/android/launcher2/h;)V
    .registers 7
    .parameter

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v0, :cond_1b

    .line 201
    iget-object v2, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_11
    if-lt v1, v4, :cond_1c

    const/4 v0, -0x1

    .line 202
    :goto_14
    if-ltz v0, :cond_1b

    .line 203
    iget-object v1, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 206
    :cond_1b
    return-void

    .line 201
    :cond_1c
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    iget-object v0, v0, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    move v0, v1

    goto :goto_14

    :cond_30
    add-int/lit8 v1, v1, 0x1

    goto :goto_11
.end method

.method public final a(Lcom/android/launcher2/h;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 156
    iget-boolean v1, p0, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v1, :cond_2a

    .line 157
    const/4 v1, 0x0

    .line 159
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->a()Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 183
    :cond_c
    :goto_c
    if-eqz v0, :cond_2a

    .line 184
    iget-object v0, p0, Lcom/anddoes/launcher/p;->i:Ljava/util/Comparator;

    if-nez v0, :cond_18

    .line 185
    invoke-static {p2}, Lcom/anddoes/launcher/p;->a(Ljava/lang/String;)Ljava/util/Comparator;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/p;->i:Ljava/util/Comparator;

    .line 188
    :cond_18
    :try_start_18
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/anddoes/launcher/p;->i:Ljava/util/Comparator;

    invoke-static {v0, p1, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 189
    if-gez v0, :cond_2a

    .line 190
    iget-object v1, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    add-int/lit8 v0, v0, 0x1

    neg-int v0, v0

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_2a} :catch_89

    .line 197
    :cond_2a
    :goto_2a
    return-void

    .line 161
    :cond_2b
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->b()Z

    move-result v2

    if-eqz v2, :cond_39

    .line 162
    iget v2, p1, Lcom/android/launcher2/h;->g:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_c

    :cond_37
    move v0, v1

    goto :goto_c

    .line 165
    :cond_39
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->c()Z

    move-result v2

    if-eqz v2, :cond_46

    .line 166
    iget v2, p1, Lcom/android/launcher2/h;->g:I

    and-int/lit8 v2, v2, 0x1

    if-eq v2, v0, :cond_37

    goto :goto_c

    .line 169
    :cond_46
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->d()Z

    move-result v2

    if-eqz v2, :cond_5b

    .line 170
    iget-wide v2, p1, Lcom/android/launcher2/h;->e:J

    const-wide/32 v4, 0xf731400

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_37

    goto :goto_c

    .line 173
    :cond_5b
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->e()Z

    move-result v2

    if-eqz v2, :cond_78

    .line 174
    iget-object v2, p0, Lcom/anddoes/launcher/p;->d:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v2}, Lcom/anddoes/launcher/preference/c;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 175
    invoke-virtual {p1}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_37

    goto :goto_c

    .line 179
    :cond_78
    iget-object v0, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 180
    invoke-virtual {p1}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_c

    :catch_89
    move-exception v0

    goto :goto_2a
.end method

.method public final a(Ljava/util/ArrayList;)V
    .registers 6
    .parameter

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->b()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 101
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 102
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 108
    :cond_15
    return-void

    .line 102
    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 103
    iget v2, v0, Lcom/android/launcher2/h;->g:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_f

    .line 104
    iget-object v2, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f
.end method

.method public final a(Ljava/util/List;Landroid/content/pm/PackageManager;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/anddoes/launcher/p;->c:Z

    if-nez v0, :cond_1b

    .line 230
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->a()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    .line 244
    :cond_11
    iget-object v0, p0, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    .line 245
    new-instance v1, Lcom/android/launcher2/hh;

    invoke-direct {v1, p2}, Lcom/android/launcher2/hh;-><init>(Landroid/content/pm/PackageManager;)V

    .line 244
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 248
    :cond_1b
    return-void

    .line 233
    :cond_1c
    iget-object v0, p0, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 234
    iget-object v0, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 235
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 236
    const/4 v0, 0x0

    :goto_2e
    if-ge v0, v2, :cond_11

    .line 237
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 238
    invoke-static {v3}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 239
    invoke-static {v4, v1}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_43

    .line 240
    iget-object v4, p0, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_43
    add-int/lit8 v0, v0, 0x1

    goto :goto_2e
.end method

.method public final a()Z
    .registers 3

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/anddoes/launcher/p;->f()V

    .line 43
    const-string v0, "LIST_ALL"

    iget-object v1, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(IILjava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 264
    iget-boolean v0, p0, Lcom/anddoes/launcher/p;->c:Z

    if-nez v0, :cond_f

    .line 265
    const-string v0, "VERTICAL_CONTINUOUS"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 266
    iput v2, p0, Lcom/anddoes/launcher/p;->h:I

    .line 275
    :cond_f
    :goto_f
    return-void

    .line 268
    :cond_10
    iget-object v0, p0, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    .line 269
    mul-int v1, p1, p2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 268
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/anddoes/launcher/p;->h:I

    .line 270
    iget v0, p0, Lcom/anddoes/launcher/p;->h:I

    if-gtz v0, :cond_f

    .line 271
    iput v2, p0, Lcom/anddoes/launcher/p;->h:I

    goto :goto_f
.end method

.method public final b(Ljava/util/ArrayList;)V
    .registers 6
    .parameter

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->c()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 112
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 113
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 119
    :cond_15
    return-void

    .line 113
    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 114
    iget v2, v0, Lcom/android/launcher2/h;->g:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-eq v2, v3, :cond_f

    .line 115
    iget-object v2, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f
.end method

.method public final b()Z
    .registers 3

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/anddoes/launcher/p;->f()V

    .line 48
    const-string v0, "LIST_DOWNLOADED"

    iget-object v1, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/util/ArrayList;)V
    .registers 8
    .parameter

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->d()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 123
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 130
    :cond_15
    return-void

    .line 124
    :cond_16
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 125
    iget-wide v2, v0, Lcom/android/launcher2/h;->e:J

    const-wide/32 v4, 0xf731400

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_f

    .line 126
    iget-object v2, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f
.end method

.method public final c()Z
    .registers 3

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/anddoes/launcher/p;->f()V

    .line 53
    const-string v0, "LIST_SYSTEM"

    iget-object v1, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final d(Ljava/util/ArrayList;)V
    .registers 6
    .parameter

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/anddoes/launcher/p;->e()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 134
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 135
    iget-object v0, p0, Lcom/anddoes/launcher/p;->d:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/c;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 136
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1b
    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_22

    .line 142
    :cond_21
    return-void

    .line 136
    :cond_22
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 137
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1b

    .line 138
    iget-object v3, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1b
.end method

.method public final d()Z
    .registers 3

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/anddoes/launcher/p;->f()V

    .line 58
    const-string v0, "LIST_NEW"

    iget-object v1, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final e(Ljava/util/ArrayList;)V
    .registers 6
    .parameter

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/anddoes/launcher/p;->f()V

    .line 146
    iget-object v0, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 147
    iget-object v0, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_14
    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 153
    return-void

    .line 148
    :cond_1b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 149
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 150
    iget-object v3, p0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_14
.end method

.method public final e()Z
    .registers 3

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/anddoes/launcher/p;->f()V

    .line 63
    const-string v0, "LIST_UNGROUPED"

    iget-object v1, p0, Lcom/anddoes/launcher/p;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
