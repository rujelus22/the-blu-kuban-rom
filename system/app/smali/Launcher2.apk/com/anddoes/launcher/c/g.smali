.class public Lcom/anddoes/launcher/c/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/res/Resources;

.field b:Landroid/content/res/AssetManager;

.field c:Ljava/util/HashMap;

.field public d:Ljava/util/List;

.field public e:Ljava/util/List;

.field public f:F

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/c/g;->d:Ljava/util/List;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/c/g;->e:Ljava/util/List;

    .line 54
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/anddoes/launcher/c/g;->f:F

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 58
    iput-object p2, p0, Lcom/anddoes/launcher/c/g;->g:Ljava/lang/String;

    .line 60
    :try_start_1c
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    .line 61
    iget-object v0, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/c/g;->b:Landroid/content/res/AssetManager;
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_2c} :catch_2d

    .line 66
    :goto_2c
    return-void

    .line 63
    :catch_2d
    move-exception v0

    iput-object v2, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    .line 64
    iput-object v2, p0, Lcom/anddoes/launcher/c/g;->b:Landroid/content/res/AssetManager;

    goto :goto_2c
.end method

.method public static a(I)I
    .registers 4
    .parameter

    .prologue
    .line 232
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    invoke-virtual {v0, p0}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 154
    const-string v0, "drawable"

    invoke-virtual {p0, p1, v0}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 155
    invoke-virtual {p0, v0, p2}, Lcom/anddoes/launcher/c/g;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/anddoes/launcher/c/g;Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 329
    invoke-direct {p0, p1, p2}, Lcom/anddoes/launcher/c/g;->b(Ljava/lang/String;I)V

    return-void
.end method

.method private b(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 245
    const-string v0, "appfilter"

    const-string v3, "xml"

    invoke-virtual {p0, v0, v3}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 246
    iget-object v3, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v3, :cond_2c

    if-eqz v0, :cond_2c

    .line 247
    iget-object v3, p0, Lcom/anddoes/launcher/c/g;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 248
    iget-object v3, p0, Lcom/anddoes/launcher/c/g;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 251
    :try_start_1a
    iget-object v3, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v1

    .line 252
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getEventType()I
    :try_end_23
    .catchall {:try_start_1a .. :try_end_23} :catchall_f6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1a .. :try_end_23} :catch_c0
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_23} :catch_d2
    .catch Ljava/lang/RuntimeException; {:try_start_1a .. :try_end_23} :catch_e4

    move-result v0

    .line 253
    :goto_24
    const/4 v3, 0x1

    if-ne v0, v3, :cond_2d

    .line 292
    if-eqz v1, :cond_2c

    .line 294
    :try_start_29
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_2c
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_2c} :catch_ff

    .line 300
    :cond_2c
    :goto_2c
    return-void

    .line 254
    :cond_2d
    const/4 v3, 0x2

    if-ne v0, v3, :cond_5b

    .line 255
    :try_start_30
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 256
    const-string v3, "item"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_60

    .line 257
    const/4 v0, 0x0

    const-string v3, "component"

    invoke-interface {v1, v0, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 258
    const/4 v3, 0x0

    const-string v4, "drawable"

    invoke-interface {v1, v3, v4}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 259
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5b

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5b

    .line 260
    iget-object v4, p0, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    invoke-virtual {v4, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    :cond_5b
    :goto_5b
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    goto :goto_24

    .line 262
    :cond_60
    const-string v3, "iconback"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_85

    move v0, v2

    .line 263
    :goto_69
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v3

    if-ge v0, v3, :cond_5b

    .line 264
    invoke-interface {v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 265
    const-string v4, "img"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_82

    .line 266
    invoke-interface {v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/anddoes/launcher/c/g;->b(Ljava/lang/String;I)V

    .line 263
    :cond_82
    add-int/lit8 v0, v0, 0x1

    goto :goto_69

    .line 269
    :cond_85
    const-string v3, "iconupon"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_aa

    move v0, v2

    .line 270
    :goto_8e
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v3

    if-ge v0, v3, :cond_5b

    .line 271
    invoke-interface {v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 272
    const-string v4, "img"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a7

    .line 273
    invoke-interface {v1, v0}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, p1}, Lcom/anddoes/launcher/c/g;->c(Ljava/lang/String;I)V

    .line 270
    :cond_a7
    add-int/lit8 v0, v0, 0x1

    goto :goto_8e

    .line 276
    :cond_aa
    const-string v3, "scale"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_af
    .catchall {:try_start_30 .. :try_end_af} :catchall_f6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_30 .. :try_end_af} :catch_c0
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_af} :catch_d2
    .catch Ljava/lang/RuntimeException; {:try_start_30 .. :try_end_af} :catch_e4

    move-result v0

    if-eqz v0, :cond_5b

    .line 278
    const/4 v0, 0x0

    :try_start_b3
    const-string v3, "factor"

    const/high16 v4, 0x3f80

    invoke-interface {v1, v0, v3, v4}, Landroid/content/res/XmlResourceParser;->getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/c/g;->f:F
    :try_end_bd
    .catchall {:try_start_b3 .. :try_end_bd} :catchall_f6
    .catch Ljava/lang/Exception; {:try_start_b3 .. :try_end_bd} :catch_be
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b3 .. :try_end_bd} :catch_c0
    .catch Ljava/io/IOException; {:try_start_b3 .. :try_end_bd} :catch_d2
    .catch Ljava/lang/RuntimeException; {:try_start_b3 .. :try_end_bd} :catch_e4

    goto :goto_5b

    :catch_be
    move-exception v0

    goto :goto_5b

    .line 285
    :catch_c0
    move-exception v0

    .line 286
    :try_start_c1
    const-string v2, "Apex.Theme"

    const-string v3, "Got exception parsing appfilter."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_c8
    .catchall {:try_start_c1 .. :try_end_c8} :catchall_f6

    .line 292
    if-eqz v1, :cond_2c

    .line 294
    :try_start_ca
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_cd
    .catch Ljava/lang/Exception; {:try_start_ca .. :try_end_cd} :catch_cf

    goto/16 :goto_2c

    .line 295
    :catch_cf
    move-exception v0

    goto/16 :goto_2c

    .line 287
    :catch_d2
    move-exception v0

    .line 288
    :try_start_d3
    const-string v2, "Apex.Theme"

    const-string v3, "Got exception parsing appfilter."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_da
    .catchall {:try_start_d3 .. :try_end_da} :catchall_f6

    .line 292
    if-eqz v1, :cond_2c

    .line 294
    :try_start_dc
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_df
    .catch Ljava/lang/Exception; {:try_start_dc .. :try_end_df} :catch_e1

    goto/16 :goto_2c

    .line 295
    :catch_e1
    move-exception v0

    goto/16 :goto_2c

    .line 289
    :catch_e4
    move-exception v0

    .line 290
    :try_start_e5
    const-string v2, "Apex.Theme"

    const-string v3, "Got exception parsing appfilter."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_ec
    .catchall {:try_start_e5 .. :try_end_ec} :catchall_f6

    .line 292
    if-eqz v1, :cond_2c

    .line 294
    :try_start_ee
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_f1
    .catch Ljava/lang/Exception; {:try_start_ee .. :try_end_f1} :catch_f3

    goto/16 :goto_2c

    .line 295
    :catch_f3
    move-exception v0

    goto/16 :goto_2c

    .line 291
    :catchall_f6
    move-exception v0

    .line 292
    if-eqz v1, :cond_fc

    .line 294
    :try_start_f9
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V
    :try_end_fc
    .catch Ljava/lang/Exception; {:try_start_f9 .. :try_end_fc} :catch_fd

    .line 298
    :cond_fc
    :goto_fc
    throw v0

    :catch_fd
    move-exception v1

    goto :goto_fc

    :catch_ff
    move-exception v0

    goto/16 :goto_2c
.end method

.method static synthetic b(Lcom/anddoes/launcher/c/g;Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 342
    invoke-direct {p0, p1, p2}, Lcom/anddoes/launcher/c/g;->c(Ljava/lang/String;I)V

    return-void
.end method

.method private b(Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 330
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 331
    const-string v0, "drawable"

    invoke-virtual {p0, p1, v0}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 332
    if-eqz v0, :cond_19

    .line 333
    invoke-virtual {p0, v0, p2}, Lcom/anddoes/launcher/c/g;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 334
    if-eqz v0, :cond_19

    .line 335
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    :cond_19
    return-void
.end method

.method private c(Ljava/lang/String;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 343
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 344
    const-string v0, "drawable"

    invoke-virtual {p0, p1, v0}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 345
    if-eqz v0, :cond_19

    .line 346
    invoke-virtual {p0, v0, p2}, Lcom/anddoes/launcher/c/g;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 347
    if-eqz v0, :cond_19

    .line 348
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_19
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Ljava/lang/String;)I
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 78
    .line 79
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v1, :cond_13

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 81
    :try_start_b
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/anddoes/launcher/c/g;->g:Ljava/lang/String;

    invoke-virtual {v1, p1, p2, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_12} :catch_14

    move-result v0

    .line 86
    :cond_13
    :goto_13
    return v0

    .line 83
    :catch_14
    move-exception v1

    goto :goto_13
.end method

.method public final a(II)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 163
    .line 164
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v1, :cond_f

    if-eqz p1, :cond_f

    .line 166
    if-nez p2, :cond_10

    .line 167
    :try_start_9
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 177
    :cond_f
    :goto_f
    return-object v0

    .line 169
    :cond_10
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, p1, p2}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_15} :catch_19
    .catch Ljava/lang/StackOverflowError; {:try_start_9 .. :try_end_15} :catch_17

    move-result-object v0

    goto :goto_f

    .line 174
    :catch_17
    move-exception v1

    goto :goto_f

    .line 172
    :catch_19
    move-exception v1

    goto :goto_f
.end method

.method public final a(Landroid/content/pm/ResolveInfo;I)Landroid/graphics/drawable/Drawable;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 181
    if-eqz p1, :cond_21

    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_21

    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v0, :cond_21

    .line 182
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    const-string v2, "_"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 184
    :goto_20
    return-object v0

    :cond_21
    const/4 v0, 0x0

    goto :goto_20
.end method

.method public final a(Ljava/lang/String;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 90
    .line 91
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v1, :cond_13

    .line 93
    :try_start_5
    const-string v1, "bool"

    invoke-virtual {p0, p1, v1}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 94
    if-eqz v1, :cond_13

    .line 95
    iget-object v2, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_12} :catch_14

    move-result v0

    .line 101
    :cond_13
    :goto_13
    return v0

    .line 98
    :catch_14
    move-exception v1

    goto :goto_13
.end method

.method public final b(Ljava/lang/String;)I
    .registers 5
    .parameter

    .prologue
    const/high16 v0, -0x8000

    .line 105
    .line 106
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v1, :cond_14

    .line 108
    :try_start_6
    const-string v1, "color"

    invoke-virtual {p0, p1, v1}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 109
    if-eqz v1, :cond_14

    .line 110
    iget-object v2, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_13} :catch_15

    move-result v0

    .line 116
    :cond_14
    :goto_14
    return v0

    .line 113
    :catch_15
    move-exception v1

    goto :goto_14
.end method

.method protected final b(Landroid/content/pm/ResolveInfo;I)Landroid/graphics/drawable/Drawable;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 188
    iget-object v0, p0, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    if-nez v0, :cond_47

    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    invoke-direct {p0, p2}, Lcom/anddoes/launcher/c/g;->b(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/anddoes/launcher/c/g;->b:Landroid/content/res/AssetManager;

    if-eqz v0, :cond_47

    :try_start_1a
    iget-object v0, p0, Lcom/anddoes/launcher/c/g;->b:Landroid/content/res/AssetManager;

    const-string v1, "appfilter.xml"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_47

    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    new-instance v2, Lcom/anddoes/launcher/c/h;

    invoke-direct {v2, p0, p2}, Lcom/anddoes/launcher/c/h;-><init>(Lcom/anddoes/launcher/c/g;I)V

    invoke-interface {v1, v2}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    new-instance v2, Ljava/io/InputStreamReader;

    const-string v3, "UTF-8"

    invoke-direct {v2, v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-interface {v1, v0}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_47} :catch_97

    .line 191
    :cond_47
    :goto_47
    if-eqz p1, :cond_95

    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_95

    .line 192
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v0, :cond_95

    .line 193
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_95

    .line 194
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ComponentInfo{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 195
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_95

    .line 197
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 200
    :goto_94
    return-object v0

    :cond_95
    const/4 v0, 0x0

    goto :goto_94

    :catch_97
    move-exception v0

    goto :goto_47
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 120
    .line 121
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v1, :cond_13

    .line 123
    :try_start_5
    const-string v1, "string"

    invoke-virtual {p0, p1, v1}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 124
    if-eqz v1, :cond_13

    .line 125
    iget-object v2, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_12} :catch_14

    move-result-object v0

    .line 131
    :cond_13
    :goto_13
    return-object v0

    .line 128
    :catch_14
    move-exception v1

    goto :goto_13
.end method

.method public final d(Ljava/lang/String;)[Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 135
    .line 136
    iget-object v1, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    if-eqz v1, :cond_13

    .line 138
    :try_start_5
    const-string v1, "array"

    invoke-virtual {p0, p1, v1}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 139
    if-eqz v1, :cond_13

    .line 140
    iget-object v2, p0, Lcom/anddoes/launcher/c/g;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_12} :catch_14

    move-result-object v0

    .line 146
    :cond_13
    :goto_13
    return-object v0

    .line 143
    :catch_14
    move-exception v1

    goto :goto_13
.end method

.method public final e(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .registers 3
    .parameter

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/c/g;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
