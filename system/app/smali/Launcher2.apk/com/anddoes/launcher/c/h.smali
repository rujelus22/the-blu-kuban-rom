.class final Lcom/anddoes/launcher/c/h;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/c/g;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/c/g;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 359
    iput-object p1, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/anddoes/launcher/c/h;->b:I

    .line 360
    iput p2, p0, Lcom/anddoes/launcher/c/h;->b:I

    .line 361
    return-void
.end method


# virtual methods
.method public final characters([CII)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 414
    return-void
.end method

.method public final endDocument()V
    .registers 1

    .prologue
    .line 373
    return-void
.end method

.method public final endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 409
    return-void
.end method

.method public final startDocument()V
    .registers 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    iget-object v0, v0, Lcom/anddoes/launcher/c/g;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 367
    iget-object v0, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    iget-object v0, v0, Lcom/anddoes/launcher/c/g;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 368
    return-void
.end method

.method public final startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v4, 0xa

    const/4 v0, 0x1

    .line 379
    const-string v1, "item"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 380
    const-string v0, "component"

    invoke-interface {p4, v0}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 381
    const-string v1, "drawable"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 382
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2a

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2a

    .line 383
    iget-object v2, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    iget-object v2, v2, Lcom/anddoes/launcher/c/g;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    :cond_2a
    :goto_2a
    return-void

    .line 385
    :cond_2b
    const-string v1, "iconback"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_52

    .line 386
    :goto_33
    if-ge v0, v4, :cond_2a

    .line 387
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "img"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 388
    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 389
    iget-object v2, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    iget v3, p0, Lcom/anddoes/launcher/c/h;->b:I

    invoke-static {v2, v1, v3}, Lcom/anddoes/launcher/c/g;->a(Lcom/anddoes/launcher/c/g;Ljava/lang/String;I)V

    .line 386
    add-int/lit8 v0, v0, 0x1

    goto :goto_33

    .line 391
    :cond_52
    const-string v1, "iconupon"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_79

    .line 392
    :goto_5a
    if-ge v0, v4, :cond_2a

    .line 393
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "img"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 394
    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 395
    iget-object v2, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    iget v3, p0, Lcom/anddoes/launcher/c/h;->b:I

    invoke-static {v2, v1, v3}, Lcom/anddoes/launcher/c/g;->b(Lcom/anddoes/launcher/c/g;Ljava/lang/String;I)V

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_5a

    .line 397
    :cond_79
    const-string v0, "scale"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 399
    :try_start_81
    iget-object v0, p0, Lcom/anddoes/launcher/c/h;->a:Lcom/anddoes/launcher/c/g;

    const-string v1, "factor"

    invoke-interface {p4, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, v0, Lcom/anddoes/launcher/c/g;->f:F
    :try_end_8f
    .catch Ljava/lang/Exception; {:try_start_81 .. :try_end_8f} :catch_90

    goto :goto_2a

    :catch_90
    move-exception v0

    goto :goto_2a
.end method
