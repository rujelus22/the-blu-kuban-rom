.class public final Lcom/anddoes/launcher/au;
.super Lcom/anddoes/launcher/i;
.source "SourceFile"


# static fields
.field private static f:Landroid/appwidget/AppWidgetProviderInfo;


# direct methods
.method private constructor <init>()V
    .registers 2

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/anddoes/launcher/i;-><init>()V

    .line 16
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/anddoes/launcher/au;->i:I

    .line 17
    const/4 v0, 0x4

    iput v0, p0, Lcom/anddoes/launcher/au;->n:I

    .line 18
    const/4 v0, 0x1

    iput v0, p0, Lcom/anddoes/launcher/au;->o:I

    .line 19
    const v0, 0x7f070020

    iput v0, p0, Lcom/anddoes/launcher/au;->a:I

    .line 20
    const v0, 0x7f02002f

    iput v0, p0, Lcom/anddoes/launcher/au;->b:I

    .line 21
    const v0, 0x7f030039

    iput v0, p0, Lcom/anddoes/launcher/au;->c:I

    .line 22
    const v0, 0x7f02007a

    iput v0, p0, Lcom/anddoes/launcher/au;->d:I

    .line 23
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/anddoes/launcher/au;
    .registers 5
    .parameter

    .prologue
    .line 26
    new-instance v0, Lcom/anddoes/launcher/au;

    invoke-direct {v0}, Lcom/anddoes/launcher/au;-><init>()V

    .line 27
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "QuickSearchBar"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/anddoes/launcher/au;->e:Landroid/content/ComponentName;

    .line 28
    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;)Landroid/appwidget/AppWidgetProviderInfo;
    .registers 6
    .parameter

    .prologue
    .line 32
    const-class v1, Lcom/anddoes/launcher/au;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    if-nez v0, :cond_4f

    .line 33
    new-instance v0, Landroid/appwidget/AppWidgetProviderInfo;

    invoke-direct {v0}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    .line 35
    sput-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "QuickSearchBar"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 36
    sget-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    const v2, 0x7f070020

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    .line 37
    sget-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    const v2, 0x7f02002f

    iput v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    .line 38
    sget-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    const v2, 0x7f030039

    iput v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    .line 39
    sget-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    const v2, 0x7f02007a

    iput v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    .line 41
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->f()F

    move-result v0

    .line 42
    sget-object v2, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    const/high16 v3, 0x4220

    mul-float/2addr v3, v0

    float-to-int v3, v3

    iput v3, v2, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    .line 43
    sget-object v2, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;

    const/high16 v3, 0x438c

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    .line 45
    :cond_4f
    sget-object v0, Lcom/anddoes/launcher/au;->f:Landroid/appwidget/AppWidgetProviderInfo;
    :try_end_51
    .catchall {:try_start_3 .. :try_end_51} :catchall_53

    monitor-exit v1

    return-object v0

    .line 32
    :catchall_53
    move-exception v0

    monitor-exit v1

    throw v0
.end method
