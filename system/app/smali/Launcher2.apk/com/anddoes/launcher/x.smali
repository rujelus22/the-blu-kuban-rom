.class final Lcom/anddoes/launcher/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/w;


# direct methods
.method constructor <init>(Lcom/anddoes/launcher/w;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/anddoes/launcher/x;->a:Lcom/anddoes/launcher/w;

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .registers 6
    .parameter

    .prologue
    .line 162
    const/4 v1, 0x0

    .line 164
    :try_start_1
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/accounts/Account;
    :try_end_7
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_7} :catch_12
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_7} :catch_1c
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_7} :catch_26

    .line 172
    :goto_7
    if-eqz v0, :cond_11

    array-length v1, v0

    if-lez v1, :cond_11

    .line 173
    iget-object v1, p0, Lcom/anddoes/launcher/x;->a:Lcom/anddoes/launcher/w;

    invoke-static {v1, v0}, Lcom/anddoes/launcher/w;->a(Lcom/anddoes/launcher/w;[Landroid/accounts/Account;)V

    .line 175
    :cond_11
    return-void

    .line 165
    :catch_12
    move-exception v0

    .line 166
    const-string v2, "Notifier"

    const-string v3, "Got OperationCanceledException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_7

    .line 167
    :catch_1c
    move-exception v0

    .line 168
    const-string v2, "Notifier"

    const-string v3, "Got OperationCanceledException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_7

    .line 169
    :catch_26
    move-exception v0

    .line 170
    const-string v2, "Notifier"

    const-string v3, "Got OperationCanceledException"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_7
.end method
