.class public Lcom/anddoes/launcher/PreviewPane;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/android/launcher2/bt;
.implements Lcom/android/launcher2/bx;


# instance fields
.field private a:Lcom/anddoes/launcher/Launcher;

.field private b:Lcom/anddoes/launcher/Preview;

.field private c:Ljava/util/List;

.field private d:Landroid/animation/AnimatorSet;

.field private e:Z

.field private f:Z

.field private g:Landroid/view/ViewPropertyAnimator;

.field private h:Lcom/android/launcher2/bk;

.field private final i:Lcom/android/launcher2/cx;

.field private j:I

.field private k:I

.field private l:I

.field private m:Lcom/anddoes/launcher/ag;

.field private n:Landroid/widget/ImageView;

.field private o:I

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/PreviewPane;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/anddoes/launcher/PreviewPane;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->c:Ljava/util/List;

    .line 43
    iput-boolean v1, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    .line 44
    iput-boolean v1, p0, Lcom/anddoes/launcher/PreviewPane;->f:Z

    .line 47
    new-instance v0, Lcom/android/launcher2/cx;

    invoke-direct {v0}, Lcom/android/launcher2/cx;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->i:Lcom/android/launcher2/cx;

    .line 56
    iput-boolean v1, p0, Lcom/anddoes/launcher/PreviewPane;->p:Z

    .line 68
    check-cast p1, Lcom/anddoes/launcher/Launcher;

    iput-object p1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    .line 69
    return-void
.end method

.method private a(I)V
    .registers 4
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    .line 171
    if-ltz p1, :cond_e

    if-lt p1, v0, :cond_10

    .line 172
    :cond_e
    div-int/lit8 p1, v0, 0x2

    .line 174
    :cond_10
    iget v0, p0, Lcom/anddoes/launcher/PreviewPane;->l:I

    if-eq p1, v0, :cond_29

    .line 175
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iput p1, v0, Lcom/android/launcher2/Workspace;->al:I

    .line 176
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v0, v0, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/preference/h;->b(I)V

    .line 177
    iput p1, p0, Lcom/anddoes/launcher/PreviewPane;->l:I

    .line 179
    :cond_29
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget v1, p0, Lcom/anddoes/launcher/PreviewPane;->l:I

    iput v1, v0, Lcom/anddoes/launcher/Preview;->a:I

    .line 180
    return-void
.end method

.method private declared-synchronized a(Landroid/view/View;)V
    .registers 11
    .parameter

    .prologue
    .line 604
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->p:Z

    if-nez v0, :cond_b1

    .line 605
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const v2, 0x1060012

    const-string v3, "outline_color"

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/anddoes/launcher/PreviewPane;->i:Lcom/android/launcher2/cx;

    invoke-virtual {v3, v1, v0, v2}, Lcom/android/launcher2/cx;->a(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;I)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 606
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 607
    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v2}, Lcom/anddoes/launcher/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 608
    const/4 v2, 0x0

    aget v2, v0, v2

    .line 609
    const/4 v3, 0x1

    aget v3, v0, v3

    .line 610
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/anddoes/launcher/ag;

    .line 611
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0b006a

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 612
    new-instance v7, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    invoke-direct {v7, v4, v6, v8, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 614
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->h:Lcom/android/launcher2/bk;

    sget v4, Lcom/android/launcher2/bk;->a:I

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v8

    move-object v4, p0

    invoke-virtual/range {v0 .. v8}, Lcom/android/launcher2/bk;->a(Landroid/graphics/Bitmap;IILcom/android/launcher2/bt;Ljava/lang/Object;Landroid/graphics/Point;Landroid/graphics/Rect;F)V

    .line 615
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 616
    iget v0, v5, Lcom/anddoes/launcher/ag;->e:I

    sget v1, Lcom/anddoes/launcher/ag;->b:I

    if-ne v0, v1, :cond_b3

    .line 617
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 618
    check-cast p1, Landroid/widget/ImageView;

    .line 619
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 623
    :goto_9c
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    if-eqz v0, :cond_a9

    .line 624
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    iget-object v1, v1, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/Preview;->removeView(Landroid/view/View;)V

    .line 626
    :cond_a9
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->invalidate()V

    .line 627
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->p:Z
    :try_end_b1
    .catchall {:try_start_1 .. :try_end_b1} :catchall_b8

    .line 629
    :cond_b1
    monitor-exit p0

    return-void

    .line 621
    :cond_b3
    const/4 v0, 0x4

    :try_start_b4
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V
    :try_end_b7
    .catchall {:try_start_b4 .. :try_end_b7} :catchall_b8

    goto :goto_9c

    .line 604
    :catchall_b8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/anddoes/launcher/PreviewPane;)V
    .registers 4
    .parameter

    .prologue
    .line 219
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_5f

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->N()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/preference/h;->a(I)V

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/Preview;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    iget-object v2, v2, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/Preview;->removeView(Landroid/view/View;)V

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/PreviewPane;->b(I)Lcom/anddoes/launcher/ag;

    move-result-object v0

    iget-object v1, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v0, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/Preview;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/Preview;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->g()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->requestLayout()V

    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->h()V

    :cond_5f
    return-void
.end method

.method private b(I)Lcom/anddoes/launcher/ag;
    .registers 7
    .parameter

    .prologue
    .line 202
    iget v0, p0, Lcom/anddoes/launcher/PreviewPane;->j:I

    iget v1, p0, Lcom/anddoes/launcher/PreviewPane;->k:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 203
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f030024

    iget-object v3, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 211
    new-instance v2, Lcom/anddoes/launcher/ag;

    invoke-direct {v2, p0, v1, p1}, Lcom/anddoes/launcher/ag;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;I)V

    .line 212
    invoke-virtual {v2}, Lcom/anddoes/launcher/ag;->a()Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 214
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 215
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    return-object v2
.end method

.method static synthetic b(Lcom/anddoes/launcher/PreviewPane;)V
    .registers 2
    .parameter

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    return-void
.end method

.method static synthetic c(Lcom/anddoes/launcher/PreviewPane;)V
    .registers 1
    .parameter

    .prologue
    .line 411
    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->i()V

    return-void
.end method

.method static synthetic d(Lcom/anddoes/launcher/PreviewPane;)Z
    .registers 2
    .parameter

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->f:Z

    return v0
.end method

.method static synthetic e(Lcom/anddoes/launcher/PreviewPane;)V
    .registers 1
    .parameter

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->f()V

    return-void
.end method

.method private f()V
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 95
    iput-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->f:Z

    .line 97
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 98
    if-lez v2, :cond_13

    move v1, v0

    .line 99
    :goto_c
    if-lt v1, v2, :cond_14

    .line 102
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 104
    :cond_13
    return-void

    .line 100
    :cond_14
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 99
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c
.end method

.method private g()V
    .registers 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_17

    .line 197
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    iget-object v1, v1, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/Preview;->addView(Landroid/view/View;)V

    .line 199
    :cond_17
    return-void
.end method

.method private h()V
    .registers 6

    .prologue
    .line 273
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v2

    .line 274
    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    .line 275
    const/4 v0, 0x0

    move v1, v0

    :goto_c
    if-lt v1, v3, :cond_f

    .line 282
    return-void

    .line 276
    :cond_f
    invoke-virtual {v2, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 277
    instance-of v4, v0, Lcom/android/launcher2/CellLayout;

    if-eqz v4, :cond_1c

    .line 278
    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 279
    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->k()V

    .line 275
    :cond_1c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_c
.end method

.method private i()V
    .registers 3

    .prologue
    const/16 v1, 0x22

    .line 412
    const/16 v0, 0x80

    invoke-static {v0, v1, v1, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/PreviewPane;->setBackgroundColor(I)V

    .line 413
    return-void
.end method

.method private static setPivotsForZoom$5359e7dd(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    const/high16 v1, 0x4000

    .line 545
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotX(F)V

    .line 546
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/view/View;->setPivotY(F)V

    .line 547
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 13

    .prologue
    const/4 v6, -0x2

    const/high16 v11, 0x3f00

    const/4 v10, 0x0

    const/4 v2, 0x0

    const/high16 v9, 0x3f80

    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    .line 551
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->removeAllViews()V

    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->f()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget v0, v0, Lcom/android/launcher2/Workspace;->al:I

    iput v0, p0, Lcom/anddoes/launcher/PreviewPane;->l:I

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/ja;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getHeight()I

    move-result v0

    iget v3, p0, Lcom/anddoes/launcher/PreviewPane;->o:I

    int-to-float v3, v3

    mul-float/2addr v3, v9

    const/high16 v4, 0x42c8

    div-float/2addr v3, v4

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Lcom/anddoes/launcher/PreviewPane;->j:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Lcom/anddoes/launcher/PreviewPane;->k:I

    iget v0, p0, Lcom/anddoes/launcher/PreviewPane;->j:I

    if-lez v0, :cond_dc

    iget v0, p0, Lcom/anddoes/launcher/PreviewPane;->k:I

    if-lez v0, :cond_dc

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v4, v3, v3}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v3

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    move v1, v2

    :goto_73
    if-lt v1, v3, :cond_192

    iget v0, p0, Lcom/anddoes/launcher/PreviewPane;->l:I

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/PreviewPane;->a(I)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    const v1, 0x7f020026

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v0, Lcom/anddoes/launcher/ag;

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    sget v3, Lcom/anddoes/launcher/ag;->b:I

    invoke-direct {v0, p0, v1, v3}, Lcom/anddoes/launcher/ag;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;I)V

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/Preview;->addView(Landroid/view/View;)V

    sget v0, Lcom/anddoes/launcher/ag;->a:I

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/PreviewPane;->b(I)Lcom/anddoes/launcher/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    iget-object v0, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    const v1, 0x7f020075

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->m:Lcom/anddoes/launcher/ag;

    iget-object v0, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    new-instance v1, Lcom/anddoes/launcher/ah;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/ah;-><init>(Lcom/anddoes/launcher/PreviewPane;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->g()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->requestLayout()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->invalidate()V

    .line 553
    :cond_dc
    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_eb

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    :cond_eb
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_f7

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    :cond_f7
    const-string v0, "FLY"

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1ea

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    const v3, 0x7f0a0009

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    const v4, 0x7f0a000d

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_270

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    int-to-long v5, v1

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-static {p0}, Lcom/anddoes/launcher/PreviewPane;->setPivotsForZoom$5359e7dd(Landroid/view/View;)V

    new-instance v4, Lcom/android/launcher2/kk;

    invoke-direct {v4}, Lcom/android/launcher2/kk;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v4, Lcom/anddoes/launcher/ak;

    invoke-direct {v4, p0, p0}, Lcom/anddoes/launcher/ak;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v10}, Landroid/view/View;->setAlpha(F)V

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_278

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v2

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v4, 0x3fc0

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v3, Lcom/anddoes/launcher/al;

    invoke-direct {v3, p0, p0}, Lcom/anddoes/launcher/al;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    new-instance v2, Lcom/anddoes/launcher/am;

    invoke-direct {v2, p0, p0}, Lcom/anddoes/launcher/am;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 554
    :goto_191
    return-void

    .line 552
    :cond_192
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    iget v6, p0, Lcom/anddoes/launcher/PreviewPane;->j:I

    iget v7, p0, Lcom/anddoes/launcher/PreviewPane;->k:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/android/launcher2/ja;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v4, v6}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/android/launcher2/ja;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->c:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v0, v0, Lcom/anddoes/launcher/Launcher;->a:Landroid/view/LayoutInflater;

    const v7, 0x7f030024

    iget-object v8, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0, v7, v8, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v7, Lcom/anddoes/launcher/ag;

    invoke-direct {v7, p0, v0, v1}, Lcom/anddoes/launcher/ag;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;I)V

    invoke-virtual {v7}, Lcom/anddoes/launcher/ag;->a()Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v6, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v6, v0}, Lcom/anddoes/launcher/Preview;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_73

    .line 553
    :cond_1ea
    invoke-virtual {p0, v10}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p0, v10}, Landroid/view/View;->setTranslationY(F)V

    invoke-virtual {p0, v9}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p0, v9}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p0, v9}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    const-string v0, "FADE"

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22f

    invoke-virtual {p0, v10}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/anddoes/launcher/an;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/an;-><init>(Lcom/anddoes/launcher/PreviewPane;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_191

    :cond_22f
    const-string v0, "SCALE"

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_269

    invoke-virtual {p0, v11}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p0, v11}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/anddoes/launcher/ao;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/ao;-><init>(Lcom/anddoes/launcher/PreviewPane;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_191

    :cond_269
    iput-boolean v2, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->i()V

    goto/16 :goto_191

    :array_270
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    :array_278
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 727
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->p:Z

    .line 728
    return-void
.end method

.method public final a(Lcom/anddoes/launcher/ag;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 241
    iget v1, p1, Lcom/anddoes/launcher/ag;->e:I

    sget v2, Lcom/anddoes/launcher/ag;->b:I

    if-ne v1, v2, :cond_26

    .line 242
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget v0, v0, Lcom/anddoes/launcher/Preview;->a:I

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/PreviewPane;->a(I)V

    .line 243
    iget-object v0, p1, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    .line 244
    const v1, 0x7f020026

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 267
    :cond_18
    :goto_18
    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->g()V

    .line 268
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->requestLayout()V

    .line 269
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->invalidate()V

    .line 270
    return-void

    .line 246
    :cond_26
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v1}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    .line 247
    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    .line 248
    const/4 v3, 0x1

    if-ne v2, v3, :cond_39

    .line 250
    iget-object v1, p1, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_18

    .line 251
    :cond_39
    iget v3, p1, Lcom/anddoes/launcher/ag;->e:I

    if-ltz v3, :cond_18

    iget v3, p1, Lcom/anddoes/launcher/ag;->e:I

    if-ge v3, v2, :cond_18

    .line 252
    iget v2, p1, Lcom/anddoes/launcher/ag;->e:I

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Workspace;->i(I)V

    .line 253
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v2, p1, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/Preview;->removeView(Landroid/view/View;)V

    .line 254
    iget v1, p0, Lcom/anddoes/launcher/PreviewPane;->l:I

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/PreviewPane;->a(I)V

    move v1, v0

    .line 255
    :goto_53
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_5f

    .line 264
    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->h()V

    goto :goto_18

    .line 256
    :cond_5f
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/Preview;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/ag;

    .line 258
    iget v2, v0, Lcom/anddoes/launcher/ag;->e:I

    sget v3, Lcom/anddoes/launcher/ag;->a:I

    if-eq v2, v3, :cond_83

    .line 259
    iget v2, v0, Lcom/anddoes/launcher/ag;->e:I

    sget v3, Lcom/anddoes/launcher/ag;->b:I

    if-eq v2, v3, :cond_83

    .line 260
    iget v2, v0, Lcom/anddoes/launcher/ag;->e:I

    iget v3, p1, Lcom/anddoes/launcher/ag;->e:I

    if-le v2, v3, :cond_83

    .line 261
    iget v2, v0, Lcom/anddoes/launcher/ag;->e:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/anddoes/launcher/ag;->e:I

    .line 255
    :cond_83
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_53
.end method

.method public final a(Lcom/android/launcher2/bz;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 637
    iput-boolean v2, p1, Lcom/android/launcher2/bz;->k:Z

    .line 638
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/anddoes/launcher/ag;

    .line 641
    iget v1, v0, Lcom/anddoes/launcher/ag;->e:I

    sget v3, Lcom/anddoes/launcher/ag;->b:I

    if-ne v1, v3, :cond_74

    .line 642
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    .line 644
    :goto_15
    iget-object v3, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v3, p1, v1}, Lcom/anddoes/launcher/Preview;->a(Lcom/android/launcher2/bz;I)I

    move-result v3

    .line 645
    iget v1, v0, Lcom/anddoes/launcher/ag;->e:I

    sget v4, Lcom/anddoes/launcher/ag;->b:I

    if-ne v1, v4, :cond_41

    .line 646
    invoke-direct {p0, v3}, Lcom/anddoes/launcher/PreviewPane;->a(I)V

    .line 647
    iget-object v1, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    check-cast v1, Landroid/widget/ImageView;

    .line 648
    const v3, 0x7f020026

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 665
    :cond_2e
    :goto_2e
    iget-object v0, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 666
    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->g()V

    .line 667
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->requestLayout()V

    .line 668
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->invalidate()V

    .line 669
    return-void

    .line 650
    :cond_41
    iget v1, v0, Lcom/anddoes/launcher/ag;->e:I

    if-eq v1, v3, :cond_2e

    .line 651
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v1}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    iget v4, v0, Lcom/anddoes/launcher/ag;->e:I

    invoke-virtual {v1, v4, v3}, Lcom/android/launcher2/Workspace;->a(II)V

    .line 652
    iget v1, v0, Lcom/anddoes/launcher/ag;->e:I

    .line 654
    iget v4, v0, Lcom/anddoes/launcher/ag;->e:I

    if-le v4, v3, :cond_5b

    .line 656
    iget v1, v0, Lcom/anddoes/launcher/ag;->e:I

    move v5, v1

    move v1, v3

    move v3, v5

    :cond_5b
    move v4, v1

    .line 658
    :goto_5c
    if-le v4, v3, :cond_62

    .line 662
    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->h()V

    goto :goto_2e

    .line 659
    :cond_62
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v1, v4}, Lcom/anddoes/launcher/Preview;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/ag;

    .line 660
    iput v4, v1, Lcom/anddoes/launcher/ag;->e:I

    .line 658
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_5c

    :cond_74
    move v1, v2

    goto :goto_15
.end method

.method public final a(Lcom/android/launcher2/bz;Landroid/graphics/PointF;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 713
    return-void
.end method

.method public final a([I)V
    .registers 3
    .parameter

    .prologue
    .line 708
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 709
    return-void
.end method

.method public final b()V
    .registers 11

    .prologue
    const/high16 v3, 0x3f00

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/high16 v2, 0x3f80

    const/4 v7, 0x1

    .line 557
    invoke-virtual {p0}, Lcom/anddoes/launcher/PreviewPane;->getVisibility()I

    move-result v0

    if-nez v0, :cond_b8

    .line 558
    invoke-virtual {p0, v8}, Lcom/anddoes/launcher/PreviewPane;->setBackgroundColor(I)V

    .line 559
    iput-boolean v7, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    .line 560
    iput-boolean v7, p0, Lcom/anddoes/launcher/PreviewPane;->f:Z

    invoke-virtual {p0}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    :cond_23
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    :cond_2f
    const-string v0, "FLY"

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b9

    iput-boolean v7, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-static {p0}, Lcom/anddoes/launcher/PreviewPane;->setPivotsForZoom$5359e7dd(Landroid/view/View;)V

    invoke-virtual {p0}, Landroid/view/View;->getScaleX()F

    move-result v2

    invoke-virtual {p0}, Landroid/view/View;->getScaleY()F

    move-result v3

    new-array v4, v9, [F

    fill-array-data v4, :array_134

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    int-to-long v5, v1

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v1

    new-instance v4, Lcom/android/launcher2/kj;

    invoke-direct {v4}, Lcom/android/launcher2/kj;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v4, Lcom/anddoes/launcher/ap;

    invoke-direct {v4, p0, p0, v2, v3}, Lcom/anddoes/launcher/ap;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;FF)V

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-array v2, v9, [F

    fill-array-data v2, :array_13c

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    const v3, 0x7f0a000a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v3, v0

    invoke-virtual {v2, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v0, Lcom/anddoes/launcher/aq;

    invoke-direct {v0, p0, p0}, Lcom/anddoes/launcher/aq;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v0, Lcom/anddoes/launcher/ar;

    invoke-direct {v0, p0, p0}, Lcom/anddoes/launcher/ar;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    new-array v3, v9, [Landroid/animation/Animator;

    aput-object v1, v3, v8

    aput-object v2, v3, v7

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->d:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 562
    :cond_b8
    :goto_b8
    return-void

    .line 560
    :cond_b9
    const-string v0, "FADE"

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ec

    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/anddoes/launcher/ai;

    invoke-direct {v1, p0, p0}, Lcom/anddoes/launcher/ai;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_b8

    :cond_ec
    const-string v0, "SCALE"

    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_125

    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleX(F)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setScaleY(F)V

    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    new-instance v1, Lcom/anddoes/launcher/aj;

    invoke-direct {v1, p0, p0}, Lcom/anddoes/launcher/aj;-><init>(Lcom/anddoes/launcher/PreviewPane;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->g:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_b8

    :cond_125
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v8, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    iget-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->f:Z

    if-eqz v0, :cond_b8

    invoke-direct {p0}, Lcom/anddoes/launcher/PreviewPane;->f()V

    goto :goto_b8

    :array_134
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data

    :array_13c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final b(Lcom/android/launcher2/bz;)V
    .registers 2
    .parameter

    .prologue
    .line 673
    return-void
.end method

.method public final c(Lcom/android/launcher2/bz;)V
    .registers 6
    .parameter

    .prologue
    .line 677
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/anddoes/launcher/ag;

    .line 678
    const/4 v1, 0x0

    .line 680
    iget v2, v0, Lcom/anddoes/launcher/ag;->e:I

    sget v3, Lcom/anddoes/launcher/ag;->b:I

    if-ne v2, v3, :cond_13

    .line 681
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    .line 683
    :cond_13
    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v2, p1, v1}, Lcom/anddoes/launcher/Preview;->a(Lcom/android/launcher2/bz;I)I

    move-result v1

    .line 684
    iget v2, v0, Lcom/anddoes/launcher/ag;->e:I

    sget v3, Lcom/anddoes/launcher/ag;->b:I

    if-ne v2, v3, :cond_29

    .line 685
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iput v1, v0, Lcom/anddoes/launcher/Preview;->a:I

    .line 689
    :goto_23
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->requestLayout()V

    .line 690
    return-void

    .line 687
    :cond_29
    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    iget-object v0, v0, Lcom/anddoes/launcher/ag;->d:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Lcom/anddoes/launcher/Preview;->a(Landroid/view/View;I)V

    goto :goto_23
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->F()Z

    move-result v0

    return v0
.end method

.method public final d(Lcom/android/launcher2/bz;)V
    .registers 2
    .parameter

    .prologue
    .line 694
    return-void
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 717
    const/4 v0, 0x0

    return v0
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 722
    return-void
.end method

.method public final e(Lcom/android/launcher2/bz;)Z
    .registers 3
    .parameter

    .prologue
    .line 703
    const/4 v0, 0x1

    return v0
.end method

.method public final getDropTargetDelegate$2b911dda()Lcom/android/launcher2/bx;
    .registers 2

    .prologue
    .line 698
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/anddoes/launcher/PreviewPane;->e:Z

    if-eqz v0, :cond_5

    .line 92
    :cond_4
    :goto_4
    return-void

    .line 83
    :cond_5
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Preview;->getChildCount()I

    move-result v1

    .line 84
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v1, :cond_4

    .line 85
    iget-object v2, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    invoke-virtual {v2, v0}, Lcom/anddoes/launcher/Preview;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 86
    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 87
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/anddoes/launcher/Launcher;->c(Z)V

    .line 88
    iget-object v1, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v1}, Lcom/anddoes/launcher/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Workspace;->o(I)V

    goto :goto_4

    .line 84
    :cond_2a
    add-int/lit8 v0, v0, 0x1

    goto :goto_c
.end method

.method public onFinishInflate()V
    .registers 3

    .prologue
    .line 73
    const v0, 0x7f0d004a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/PreviewPane;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/Preview;

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->b:Lcom/anddoes/launcher/Preview;

    .line 74
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->g()Lcom/android/launcher2/bk;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->h:Lcom/android/launcher2/bk;

    .line 75
    iget-object v0, p0, Lcom/anddoes/launcher/PreviewPane;->a:Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v0}, Lcom/anddoes/launcher/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/PreviewPane;->o:I

    .line 76
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 3
    .parameter

    .prologue
    .line 599
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/PreviewPane;->a(Landroid/view/View;)V

    .line 600
    const/4 v0, 0x1

    return v0
.end method
