.class public final Lcom/anddoes/launcher/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/android/launcher2/Launcher;

.field b:Landroid/content/Context;

.field c:Ljava/util/List;

.field public d:[Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field private f:Ljava/util/List;

.field private g:[I

.field private h:[I

.field private i:[Ljava/lang/String;

.field private j:[I

.field private k:[I

.field private l:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 12
    .parameter

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x9

    const/4 v7, 0x0

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "APP_DRAWER"

    aput-object v1, v0, v7

    .line 30
    const-string v1, "SHOW_PREVIEWS"

    aput-object v1, v0, v4

    .line 31
    const-string v1, "GOTO_DEFAULT_SCREEN"

    aput-object v1, v0, v5

    .line 32
    const-string v1, "SHOW_NOTIFICATIONS"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "TOGGLE_STATUS_BAR"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 33
    const-string v2, "TOGGLE_DOCK"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "RECENT_APPS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 34
    const-string v2, "APEX_MENU"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "APEX_SETTINGS"

    aput-object v2, v0, v1

    .line 35
    const-string v1, "LOCK_DESKTOP"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/anddoes/launcher/s;->d:[Ljava/lang/String;

    .line 37
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_19a

    .line 43
    iput-object v0, p0, Lcom/anddoes/launcher/s;->g:[I

    .line 45
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1b2

    .line 51
    iput-object v0, p0, Lcom/anddoes/launcher/s;->h:[I

    .line 53
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ic_allapps"

    aput-object v1, v0, v7

    .line 54
    const-string v1, "ic_show_preview"

    aput-object v1, v0, v4

    .line 55
    const-string v1, "ic_movetodefault"

    aput-object v1, v0, v5

    .line 56
    const-string v1, "ic_notifications"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "ic_statusbar"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 57
    const-string v2, "ic_toggledock"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ic_recentapps"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 58
    const-string v2, "ic_menu"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ic_settings"

    aput-object v2, v0, v1

    .line 59
    const-string v1, "ic_lock"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/anddoes/launcher/s;->i:[Ljava/lang/String;

    .line 61
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "GOTO_SCREEN1"

    aput-object v1, v0, v7

    .line 62
    const-string v1, "GOTO_SCREEN2"

    aput-object v1, v0, v4

    const-string v1, "GOTO_SCREEN3"

    aput-object v1, v0, v5

    .line 63
    const-string v1, "GOTO_SCREEN4"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "GOTO_SCREEN5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 64
    const-string v2, "GOTO_SCREEN6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GOTO_SCREEN7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 65
    const-string v2, "GOTO_SCREEN8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GOTO_SCREEN9"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/anddoes/launcher/s;->e:[Ljava/lang/String;

    .line 67
    new-array v0, v3, [I

    fill-array-data v0, :array_1ca

    .line 71
    iput-object v0, p0, Lcom/anddoes/launcher/s;->j:[I

    .line 73
    new-array v0, v3, [I

    fill-array-data v0, :array_1e0

    .line 77
    iput-object v0, p0, Lcom/anddoes/launcher/s;->k:[I

    .line 79
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "ic_moveto1"

    aput-object v1, v0, v7

    .line 80
    const-string v1, "ic_moveto2"

    aput-object v1, v0, v4

    const-string v1, "ic_moveto3"

    aput-object v1, v0, v5

    .line 81
    const-string v1, "ic_moveto4"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "ic_moveto5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    .line 82
    const-string v2, "ic_moveto6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ic_moveto7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    .line 83
    const-string v2, "ic_moveto8"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ic_moveto9"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/anddoes/launcher/s;->l:[Ljava/lang/String;

    .line 86
    iput-object p1, p0, Lcom/anddoes/launcher/s;->b:Landroid/content/Context;

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    .line 88
    instance-of v0, p1, Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_fa

    .line 89
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    .line 91
    :cond_fa
    iget-object v0, p0, Lcom/anddoes/launcher/s;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/s;->f:Ljava/util/List;

    move v6, v7

    .line 94
    :goto_10f
    iget-object v0, p0, Lcom/anddoes/launcher/s;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lt v6, v0, :cond_11a

    .line 100
    :goto_114
    iget-object v0, p0, Lcom/anddoes/launcher/s;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lt v7, v0, :cond_15a

    .line 106
    return-void

    .line 95
    :cond_11a
    iget-object v9, p0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    new-instance v0, Lcom/anddoes/launcher/t;

    iget-object v1, p0, Lcom/anddoes/launcher/s;->d:[Ljava/lang/String;

    aget-object v2, v1, v6

    iget-object v1, p0, Lcom/anddoes/launcher/s;->g:[I

    aget v1, v1, v6

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 96
    iget-object v1, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    if-nez v1, :cond_149

    iget-object v1, p0, Lcom/anddoes/launcher/s;->h:[I

    aget v1, v1, v6

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 98
    :goto_136
    iget-object v1, p0, Lcom/anddoes/launcher/s;->h:[I

    aget v1, v1, v6

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/t;-><init>(Lcom/anddoes/launcher/s;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 95
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_10f

    .line 97
    :cond_149
    iget-object v1, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v4, p0, Lcom/anddoes/launcher/s;->h:[I

    aget v4, v4, v6

    iget-object v5, p0, Lcom/anddoes/launcher/s;->i:[Ljava/lang/String;

    aget-object v5, v5, v6

    invoke-virtual {v1, v4, v5}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_136

    .line 101
    :cond_15a
    iget-object v6, p0, Lcom/anddoes/launcher/s;->f:Ljava/util/List;

    new-instance v0, Lcom/anddoes/launcher/t;

    iget-object v1, p0, Lcom/anddoes/launcher/s;->e:[Ljava/lang/String;

    aget-object v2, v1, v7

    iget-object v1, p0, Lcom/anddoes/launcher/s;->j:[I

    aget v1, v1, v7

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 102
    iget-object v1, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    if-nez v1, :cond_188

    iget-object v1, p0, Lcom/anddoes/launcher/s;->k:[I

    aget v1, v1, v7

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 104
    :goto_176
    iget-object v1, p0, Lcom/anddoes/launcher/s;->k:[I

    aget v1, v1, v7

    invoke-virtual {v8, v1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/t;-><init>(Lcom/anddoes/launcher/s;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 101
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    add-int/lit8 v7, v7, 0x1

    goto :goto_114

    .line 103
    :cond_188
    iget-object v1, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v4, p0, Lcom/anddoes/launcher/s;->k:[I

    aget v4, v4, v7

    iget-object v5, p0, Lcom/anddoes/launcher/s;->l:[Ljava/lang/String;

    aget-object v5, v5, v7

    invoke-virtual {v1, v4, v5}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    goto :goto_176

    .line 37
    nop

    :array_19a
    .array-data 0x4
        0x2dt 0x0t 0x7t 0x7ft
        0x32t 0x0t 0x7t 0x7ft
        0x33t 0x0t 0x7t 0x7ft
        0x2ft 0x0t 0x7t 0x7ft
        0x30t 0x0t 0x7t 0x7ft
        0xedt 0x0t 0x7t 0x7ft
        0xeet 0x0t 0x7t 0x7ft
        0xb0t 0x0t 0x7t 0x7ft
        0x2et 0x0t 0x7t 0x7ft
        0x31t 0x0t 0x7t 0x7ft
    .end array-data

    .line 45
    :array_1b2
    .array-data 0x4
        0x24t 0x0t 0x2t 0x7ft
        0x61t 0x0t 0x2t 0x7ft
        0x52t 0x0t 0x2t 0x7ft
        0x53t 0x0t 0x2t 0x7ft
        0x62t 0x0t 0x2t 0x7ft
        0x63t 0x0t 0x2t 0x7ft
        0x54t 0x0t 0x2t 0x7ft
        0x41t 0x0t 0x2t 0x7ft
        0x55t 0x0t 0x2t 0x7ft
        0x3ft 0x0t 0x2t 0x7ft
    .end array-data

    .line 67
    :array_1ca
    .array-data 0x4
        0x34t 0x0t 0x7t 0x7ft
        0x35t 0x0t 0x7t 0x7ft
        0x36t 0x0t 0x7t 0x7ft
        0x37t 0x0t 0x7t 0x7ft
        0x38t 0x0t 0x7t 0x7ft
        0x39t 0x0t 0x7t 0x7ft
        0x3at 0x0t 0x7t 0x7ft
        0x3bt 0x0t 0x7t 0x7ft
        0x3ct 0x0t 0x7t 0x7ft
    .end array-data

    .line 73
    :array_1e0
    .array-data 0x4
        0x49t 0x0t 0x2t 0x7ft
        0x4at 0x0t 0x2t 0x7ft
        0x4bt 0x0t 0x2t 0x7ft
        0x4ct 0x0t 0x2t 0x7ft
        0x4dt 0x0t 0x2t 0x7ft
        0x4et 0x0t 0x2t 0x7ft
        0x4ft 0x0t 0x2t 0x7ft
        0x50t 0x0t 0x2t 0x7ft
        0x51t 0x0t 0x2t 0x7ft
    .end array-data
.end method


# virtual methods
.method public final a()Landroid/widget/ListAdapter;
    .registers 5

    .prologue
    .line 172
    iget-object v0, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/anddoes/launcher/s;->a:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_18
    iget-object v0, p0, Lcom/anddoes/launcher/s;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_26

    .line 173
    :cond_20
    new-instance v0, Lcom/anddoes/launcher/u;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/u;-><init>(Lcom/anddoes/launcher/s;)V

    return-object v0

    .line 172
    :cond_26
    iget-object v0, p0, Lcom/anddoes/launcher/s;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/t;

    add-int/lit8 v3, v1, 0x1

    if-lt v2, v3, :cond_43

    iget-object v3, p0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3f

    iget-object v3, p0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3f
    :goto_3f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_18

    :cond_43
    iget-object v3, p0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3f

    iget-object v3, p0, Lcom/anddoes/launcher/s;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_3f
.end method
