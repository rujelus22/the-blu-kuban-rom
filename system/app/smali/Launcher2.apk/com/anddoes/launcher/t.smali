.class public final Lcom/anddoes/launcher/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Landroid/graphics/drawable/Drawable;

.field final synthetic d:Lcom/anddoes/launcher/s;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/s;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 216
    iput-object p1, p0, Lcom/anddoes/launcher/t;->d:Lcom/anddoes/launcher/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput-object p2, p0, Lcom/anddoes/launcher/t;->a:Ljava/lang/String;

    .line 218
    iput-object p3, p0, Lcom/anddoes/launcher/t;->b:Ljava/lang/String;

    .line 219
    iput-object p4, p0, Lcom/anddoes/launcher/t;->c:Landroid/graphics/drawable/Drawable;

    .line 220
    iput-object p5, p0, Lcom/anddoes/launcher/t;->e:Ljava/lang/String;

    .line 221
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .registers 6

    .prologue
    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 241
    const-string v1, "android.intent.extra.shortcut.NAME"

    iget-object v2, p0, Lcom/anddoes/launcher/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    const-string v1, "android.intent.extra.shortcut.INTENT"

    iget-object v2, p0, Lcom/anddoes/launcher/t;->d:Lcom/anddoes/launcher/s;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.anddoes.launcher.ACTION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/anddoes/launcher/s;->b:Landroid/content/Context;

    const-class v4, Lcom/anddoes/launcher/Launcher;

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "LAUNCHER_ACTION"

    iget-object v4, p0, Lcom/anddoes/launcher/t;->a:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 243
    new-instance v1, Landroid/content/Intent$ShortcutIconResource;

    invoke-direct {v1}, Landroid/content/Intent$ShortcutIconResource;-><init>()V

    .line 244
    const-string v2, "com.anddoes.launcher"

    iput-object v2, v1, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    .line 245
    iget-object v2, p0, Lcom/anddoes/launcher/t;->e:Ljava/lang/String;

    iput-object v2, v1, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    .line 247
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 248
    return-object v0
.end method
