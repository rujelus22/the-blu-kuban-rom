.class public final Lcom/anddoes/launcher/r;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field static a:Landroid/graphics/Rect;

.field static b:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    const/4 v0, 0x0

    .line 25
    sput-object v0, Lcom/anddoes/launcher/r;->a:Landroid/graphics/Rect;

    .line 26
    sput-object v0, Lcom/anddoes/launcher/r;->b:Landroid/graphics/Rect;

    .line 23
    return-void
.end method

.method public static a(Landroid/widget/ImageView;)Landroid/graphics/Rect;
    .registers 6
    .parameter

    .prologue
    .line 104
    sget-object v0, Landroid/graphics/Insets;->NONE:Landroid/graphics/Insets;

    .line 105
    invoke-virtual {p0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_10

    .line 106
    invoke-virtual {p0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getLayoutInsets()Landroid/graphics/Insets;

    move-result-object v0

    .line 108
    :cond_10
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Insets;->left:I

    iget v3, v0, Landroid/graphics/Insets;->top:I

    iget v4, v0, Landroid/graphics/Insets;->right:I

    iget v0, v0, Landroid/graphics/Insets;->bottom:I

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 109
    return-object v1
.end method

.method private static a(Lcom/android/launcher2/Launcher;I)Landroid/graphics/Rect;
    .registers 10
    .parameter
    .parameter

    .prologue
    const v7, 0x7f0b007e

    const v6, 0x7f0b007c

    const v5, 0x7f0b007a

    const v2, 0x7f0b0078

    .line 66
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 67
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 68
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 69
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    .line 70
    invoke-virtual {v0, v3, v4}, Landroid/view/Display;->getCurrentSizeRange(Landroid/graphics/Point;Landroid/graphics/Point;)V

    .line 71
    if-nez p1, :cond_5c

    .line 72
    sget-object v0, Lcom/anddoes/launcher/r;->a:Landroid/graphics/Rect;

    if-nez v0, :cond_59

    .line 73
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 74
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 75
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 76
    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 77
    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int v0, v4, v0

    sub-int v2, v0, v2

    .line 78
    iget v0, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v5

    sub-int v3, v0, v6

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 80
    sput-object v0, Lcom/anddoes/launcher/r;->a:Landroid/graphics/Rect;

    .line 81
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v4

    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v5

    move v6, p1

    .line 80
    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Landroid/graphics/Rect;Landroid/content/res/Resources;IIIII)V

    .line 84
    :cond_59
    sget-object v0, Lcom/anddoes/launcher/r;->a:Landroid/graphics/Rect;

    .line 100
    :goto_5b
    return-object v0

    .line 85
    :cond_5c
    const/4 v0, 0x1

    if-ne p1, v0, :cond_94

    .line 86
    sget-object v0, Lcom/anddoes/launcher/r;->b:Landroid/graphics/Rect;

    if-nez v0, :cond_91

    .line 87
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 88
    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 89
    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 90
    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 91
    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int v0, v3, v0

    sub-int v2, v0, v2

    .line 92
    iget v0, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v5

    sub-int v3, v0, v6

    .line 93
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 94
    sput-object v0, Lcom/anddoes/launcher/r;->b:Landroid/graphics/Rect;

    .line 95
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v4

    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v5

    move v6, p1

    .line 94
    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/CellLayout;->a(Landroid/graphics/Rect;Landroid/content/res/Resources;IIIII)V

    .line 98
    :cond_91
    sget-object v0, Lcom/anddoes/launcher/r;->b:Landroid/graphics/Rect;

    goto :goto_5b

    .line 100
    :cond_94
    const/4 v0, 0x0

    goto :goto_5b
.end method

.method public static a(Landroid/appwidget/AppWidgetHostView;Lcom/android/launcher2/Launcher;II)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/anddoes/launcher/r;->a(Lcom/android/launcher2/Launcher;I)Landroid/graphics/Rect;

    move-result-object v0

    .line 43
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/anddoes/launcher/r;->a(Lcom/android/launcher2/Launcher;I)Landroid/graphics/Rect;

    move-result-object v1

    .line 44
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v5, v2, Landroid/util/DisplayMetrics;->density:F

    .line 47
    iget v2, v0, Landroid/graphics/Rect;->left:I

    .line 48
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 49
    iget v4, v0, Landroid/graphics/Rect;->right:I

    .line 50
    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    .line 51
    mul-int/2addr v2, p2

    add-int/lit8 v6, p2, -0x1

    mul-int/2addr v4, v6

    add-int/2addr v2, v4

    int-to-float v2, v2

    div-float/2addr v2, v5

    float-to-int v4, v2

    .line 52
    mul-int v2, p3, v3

    add-int/lit8 v3, p3, -0x1

    mul-int/2addr v0, v3

    add-int/2addr v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v5

    float-to-int v3, v0

    .line 55
    iget v0, v1, Landroid/graphics/Rect;->left:I

    .line 56
    iget v6, v1, Landroid/graphics/Rect;->top:I

    .line 57
    iget v2, v1, Landroid/graphics/Rect;->right:I

    .line 58
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    .line 59
    mul-int/2addr v0, p2

    add-int/lit8 v7, p2, -0x1

    mul-int/2addr v2, v7

    add-int/2addr v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v5

    float-to-int v2, v0

    .line 60
    mul-int v0, p3, v6

    add-int/lit8 v6, p3, -0x1

    mul-int/2addr v1, v6

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v5

    float-to-int v5, v0

    .line 62
    const/4 v1, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/appwidget/AppWidgetHostView;->updateAppWidgetSize(Landroid/os/Bundle;IIII)V

    .line 63
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Landroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 29
    .line 30
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 29
    invoke-static {p1, v2, v2, v0, v1}, Landroid/app/ActivityOptions;->makeScaleUpAnimation(Landroid/view/View;IIII)Landroid/app/ActivityOptions;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method public static a(Landroid/content/Context;ILandroid/content/ComponentName;)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p1, p2}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetIdIfAllowed(ILandroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method
