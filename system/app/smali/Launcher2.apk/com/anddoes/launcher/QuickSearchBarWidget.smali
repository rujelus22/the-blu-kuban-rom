.class public Lcom/anddoes/launcher/QuickSearchBarWidget;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private a:Lcom/anddoes/launcher/Launcher;

.field private b:Z

.field private c:I

.field private d:I

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;

.field private g:Landroid/view/View$OnLongClickListener;

.field private h:Landroid/widget/RelativeLayout;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/ImageView;

.field private k:Landroid/widget/ImageView;

.field private l:Lcom/android/launcher2/HolographicLinearLayout;

.field private m:Lcom/android/launcher2/HolographicLinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    iput-boolean v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->b:Z

    .line 25
    iput v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->c:I

    .line 26
    iput v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->d:I

    .line 27
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    .line 28
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    .line 39
    check-cast p1, Lcom/anddoes/launcher/Launcher;

    iput-object p1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->a:Lcom/anddoes/launcher/Launcher;

    .line 40
    return-void
.end method

.method private a(Z)V
    .registers 5
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 140
    if-eqz p1, :cond_17

    .line 141
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HolographicLinearLayout;->setImageView(Landroid/widget/ImageView;)V

    .line 149
    :goto_16
    return-void

    .line 145
    :cond_17
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/HolographicLinearLayout;->setImageView(Landroid/widget/ImageView;)V

    goto :goto_16
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 6

    .prologue
    const/16 v4, 0x8

    .line 44
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 45
    const v0, 0x7f0d004e

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->h:Landroid/widget/RelativeLayout;

    .line 46
    const v0, 0x7f0d0051

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    .line 47
    const v0, 0x7f0d006b

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->j:Landroid/widget/ImageView;

    .line 48
    const v0, 0x7f0d003d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->k:Landroid/widget/ImageView;

    .line 49
    const v0, 0x7f0d0052

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/HolographicLinearLayout;

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    .line 50
    const v0, 0x7f0d0053

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/HolographicLinearLayout;

    iput-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    .line 51
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 52
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 53
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingRight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 54
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingBottom()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 55
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 56
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 57
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingRight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 58
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingBottom()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 60
    const-string v0, "ICE_CREAM_SANDWICH"

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->a:Lcom/anddoes/launcher/Launcher;

    iget-object v1, v1, Lcom/anddoes/launcher/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 61
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->h:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_155

    const v0, 0x7f020079

    :goto_aa
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 62
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    if-eqz v1, :cond_15a

    const v0, 0x7f02002d

    :goto_b4
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 63
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->j:Landroid/widget/ImageView;

    if-eqz v1, :cond_15f

    const v0, 0x7f02002f

    :goto_be
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 64
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->k:Landroid/widget/ImageView;

    if-eqz v1, :cond_164

    const v0, 0x7f020031

    :goto_c8
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 66
    invoke-virtual {p0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 67
    const-string v0, "search"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 68
    invoke-virtual {v0}, Landroid/app/SearchManager;->getGlobalSearchActivity()Landroid/content/ComponentName;

    move-result-object v2

    .line 69
    if-nez v2, :cond_f1

    .line 70
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v0, v4}, Lcom/android/launcher2/HolographicLinearLayout;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v0, v4}, Lcom/android/launcher2/HolographicLinearLayout;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 75
    :cond_f1
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.speech.action.WEB_SEARCH"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_10c

    .line 77
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v0, v4}, Lcom/android/launcher2/HolographicLinearLayout;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 80
    :cond_10c
    const/4 v0, 0x0

    .line 81
    if-eqz v2, :cond_113

    .line 82
    invoke-static {v1, v2}, Lcom/anddoes/launcher/Launcher;->a(Landroid/content/Context;Landroid/content/ComponentName;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 84
    :cond_113
    if-eqz v0, :cond_169

    const/4 v0, 0x1

    :goto_116
    iput-boolean v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->b:Z

    .line 85
    iget-boolean v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->b:Z

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->a(Z)V

    .line 86
    iget-boolean v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->b:Z

    if-eqz v0, :cond_16b

    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    :goto_123
    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->c:I

    .line 87
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->i:Landroid/widget/ImageView;

    instance-of v2, v0, Lcom/android/launcher2/HolographicLinearLayout;

    if-eqz v2, :cond_16e

    check-cast v0, Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/HolographicLinearLayout;->a()V

    .line 88
    :cond_13a
    :goto_13a
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_14e

    .line 89
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->d:I

    .line 91
    :cond_14e
    invoke-virtual {p0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->requestLayout()V

    .line 92
    invoke-virtual {p0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->invalidate()V

    .line 93
    return-void

    .line 61
    :cond_155
    const v0, 0x7f020078

    goto/16 :goto_aa

    .line 62
    :cond_15a
    const v0, 0x7f02002e

    goto/16 :goto_b4

    .line 63
    :cond_15f
    const v0, 0x7f020030

    goto/16 :goto_be

    .line 64
    :cond_164
    const v0, 0x7f020032

    goto/16 :goto_c8

    .line 84
    :cond_169
    const/4 v0, 0x0

    goto :goto_116

    .line 86
    :cond_16b
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->j:Landroid/widget/ImageView;

    goto :goto_123

    .line 87
    :cond_16e
    instance-of v0, v1, Lcom/android/launcher2/HolographicImageView;

    if-eqz v0, :cond_13a

    move-object v0, v1

    check-cast v0, Lcom/android/launcher2/HolographicImageView;

    invoke-virtual {v0}, Lcom/android/launcher2/HolographicImageView;->a()V

    goto :goto_13a
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_d

    .line 164
    invoke-virtual {p0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/v;->c(Landroid/content/Context;)V

    .line 166
    :cond_d
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 3
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->g:Landroid/view/View$OnLongClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public onMeasure(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 103
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 104
    iget v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->c:I

    iget v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->d:I

    add-int/2addr v1, v2

    .line 105
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    .line 106
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    if-le v0, v1, :cond_43

    .line 107
    iget-boolean v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->b:Z

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->a(Z)V

    .line 108
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    .line 109
    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 110
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    .line 108
    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/android/launcher2/HolographicLinearLayout;->setPadding(IIII)V

    .line 111
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    .line 112
    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    .line 113
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    .line 111
    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/android/launcher2/HolographicLinearLayout;->setPadding(IIII)V

    .line 136
    :goto_3f
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 137
    return-void

    .line 115
    :cond_43
    iget v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->c:I

    iget v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->d:I

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_78

    .line 116
    invoke-direct {p0, v3}, Lcom/anddoes/launcher/QuickSearchBarWidget;->a(Z)V

    .line 120
    :goto_4d
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->a:Lcom/anddoes/launcher/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/LauncherApplication;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7e

    .line 121
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    .line 122
    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingLeft()I

    move-result v1

    .line 123
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingRight()I

    move-result v2

    .line 121
    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/android/launcher2/HolographicLinearLayout;->setPadding(IIII)V

    .line 124
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    .line 125
    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingLeft()I

    move-result v1

    .line 126
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingRight()I

    move-result v2

    .line 124
    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/android/launcher2/HolographicLinearLayout;->setPadding(IIII)V

    goto :goto_3f

    .line 118
    :cond_78
    iget-boolean v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->b:Z

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/QuickSearchBarWidget;->a(Z)V

    goto :goto_4d

    .line 128
    :cond_7e
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    .line 129
    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingTop()I

    move-result v1

    .line 130
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingBottom()I

    move-result v2

    .line 128
    invoke-virtual {v0, v3, v1, v3, v2}, Lcom/android/launcher2/HolographicLinearLayout;->setPadding(IIII)V

    .line 131
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    .line 132
    iget-object v1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingTop()I

    move-result v1

    .line 133
    iget-object v2, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/HolographicLinearLayout;->getPaddingBottom()I

    move-result v2

    .line 131
    invoke-virtual {v0, v3, v1, v3, v2}, Lcom/android/launcher2/HolographicLinearLayout;->setPadding(IIII)V

    goto :goto_3f
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .registers 3
    .parameter

    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 155
    iput-object p1, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->g:Landroid/view/View$OnLongClickListener;

    .line 156
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->l:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/HolographicLinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 157
    iget-object v0, p0, Lcom/anddoes/launcher/QuickSearchBarWidget;->m:Lcom/android/launcher2/HolographicLinearLayout;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/HolographicLinearLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 158
    return-void
.end method
