.class public Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Intent;

.field private b:Landroid/content/pm/PackageManager;

.field private c:Landroid/appwidget/AppWidgetManager;

.field private d:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b:Landroid/content/pm/PackageManager;

    .line 43
    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->c:Landroid/appwidget/AppWidgetManager;

    .line 33
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 275
    if-eqz p2, :cond_d

    .line 276
    :goto_2
    const-string v0, "appWidgetId"

    iget v1, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->d:I

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 277
    invoke-virtual {p0, p1, p2}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->setResult(ILandroid/content/Intent;)V

    .line 278
    return-void

    .line 275
    :cond_d
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    goto :goto_2
.end method

.method private a(Landroid/appwidget/AppWidgetProviderInfo;Lcom/anddoes/launcher/appwidgetpicker/f;Ljava/util/List;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 209
    iget-object v0, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_37

    .line 220
    :try_start_10
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b:Landroid/content/pm/PackageManager;

    iget-object v2, p1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 221
    iget-object v2, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 222
    iget-object v3, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b:Landroid/content/pm/PackageManager;

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 223
    new-instance v3, Lcom/anddoes/launcher/appwidgetpicker/b;

    invoke-direct {v3, v0, v2}, Lcom/anddoes/launcher/appwidgetpicker/b;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 224
    iput-object v1, v3, Lcom/anddoes/launcher/appwidgetpicker/b;->a:Ljava/lang/String;

    .line 225
    invoke-virtual {v3, p2}, Lcom/anddoes/launcher/appwidgetpicker/b;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    .line 226
    invoke-interface {p3, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_36
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_10 .. :try_end_36} :catch_4f

    .line 229
    :goto_36
    return-void

    .line 210
    :cond_37
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/f;

    .line 211
    instance-of v3, v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    if-eqz v3, :cond_a

    .line 212
    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    .line 213
    iget-object v3, v0, Lcom/anddoes/launcher/appwidgetpicker/b;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 214
    invoke-virtual {v0, p2}, Lcom/anddoes/launcher/appwidgetpicker/b;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    goto :goto_36

    .line 229
    :catch_4f
    move-exception v0

    goto :goto_36
.end method

.method private a(Ljava/util/List;)V
    .registers 9
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 100
    const-string v0, "customInfo"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 101
    if-eqz v3, :cond_18

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_26

    .line 102
    :cond_18
    const-string v0, "AppWidgetPickActivity"

    const-string v2, "EXTRA_CUSTOM_INFO not present."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    move-object v1, v3

    .line 142
    :goto_21
    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, v2, p1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(Ljava/util/List;Ljava/util/List;ZLjava/util/List;)V

    .line 143
    return-void

    .line 106
    :cond_26
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v4

    .line 107
    :goto_2b
    if-lt v2, v6, :cond_3e

    .line 116
    const-string v0, "customExtras"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 117
    if-nez v2, :cond_64

    .line 119
    const-string v0, "AppWidgetPickActivity"

    const-string v3, "EXTRA_CUSTOM_INFO without EXTRA_CUSTOM_EXTRAS"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 120
    goto :goto_21

    .line 108
    :cond_3e
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 109
    if-eqz v0, :cond_4a

    instance-of v0, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-nez v0, :cond_60

    .line 111
    :cond_4a
    const-string v0, "AppWidgetPickActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "error using EXTRA_CUSTOM_INFO index="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 112
    goto :goto_21

    .line 107
    :cond_60
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2b

    .line 123
    :cond_64
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 124
    if-eq v6, v5, :cond_b1

    .line 125
    const-string v0, "AppWidgetPickActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "list size mismatch: EXTRA_CUSTOM_INFO: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 126
    const-string v4, " EXTRA_CUSTOM_EXTRAS: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    move-object v1, v3

    .line 127
    goto :goto_21

    .line 131
    :cond_8b
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 132
    if-eqz v0, :cond_97

    instance-of v0, v0, Landroid/os/Bundle;

    if-nez v0, :cond_ae

    .line 135
    :cond_97
    const-string v0, "AppWidgetPickActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "error using EXTRA_CUSTOM_EXTRAS index="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 136
    goto/16 :goto_21

    .line 130
    :cond_ae
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    :cond_b1
    if-lt v4, v5, :cond_8b

    move-object v0, v2

    move-object v1, v3

    goto/16 :goto_21
.end method

.method private a(Ljava/util/List;Ljava/util/List;ZLjava/util/List;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 169
    if-nez p1, :cond_4

    .line 199
    :cond_3
    return-void

    .line 170
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 171
    const/4 v0, 0x0

    move v3, v0

    :goto_a
    if-ge v3, v4, :cond_3

    .line 172
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 174
    iget-object v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    .line 177
    iget v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    if-eqz v1, :cond_82

    .line 178
    iget-object v1, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b:Landroid/content/pm/PackageManager;

    iget-object v6, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iget v7, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    invoke-virtual {v1, v6, v7, v2}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 179
    if-nez v1, :cond_4e

    .line 180
    const-string v6, "AppWidgetPickActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Can\'t load icon drawable 0x"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 181
    const-string v8, " for provider: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 180
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_4e
    :goto_4e
    new-instance v6, Lcom/anddoes/launcher/appwidgetpicker/f;

    invoke-direct {v6, v5, v1}, Lcom/anddoes/launcher/appwidgetpicker/f;-><init>(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 186
    iget-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/anddoes/launcher/appwidgetpicker/f;->e:Ljava/lang/String;

    .line 187
    iget-object v1, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/anddoes/launcher/appwidgetpicker/f;->f:Ljava/lang/String;

    .line 189
    if-eqz p2, :cond_6d

    .line 190
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    iput-object v1, v6, Lcom/anddoes/launcher/appwidgetpicker/f;->g:Landroid/os/Bundle;

    .line 193
    :cond_6d
    if-eqz p3, :cond_7e

    .line 194
    new-instance v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    invoke-direct {v0, v6}, Lcom/anddoes/launcher/appwidgetpicker/b;-><init>(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    invoke-virtual {v0, v6}, Lcom/anddoes/launcher/appwidgetpicker/b;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    invoke-interface {p4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    :goto_7a
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_a

    .line 196
    :cond_7e
    invoke-direct {p0, v0, v6, p4}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(Landroid/appwidget/AppWidgetProviderInfo;Lcom/anddoes/launcher/appwidgetpicker/f;Ljava/util/List;)V

    goto :goto_7a

    :cond_82
    move-object v1, v2

    goto :goto_4e
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    .line 159
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(ILandroid/content/Intent;)V

    .line 160
    invoke-virtual {p0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->finish()V

    .line 161
    return-void
.end method

.method public final a(Lcom/anddoes/launcher/appwidgetpicker/f;)V
    .registers 5
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a:Landroid/content/Intent;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    iget-object v0, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->e:Ljava/lang/String;

    if-eqz v0, :cond_2d

    iget-object v0, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->f:Ljava/lang/String;

    if-eqz v0, :cond_2d

    iget-object v0, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->f:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->g:Landroid/os/Bundle;

    if-eqz v0, :cond_1f

    iget-object v0, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->g:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 148
    :cond_1f
    :goto_1f
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 151
    const/4 v0, -0x1

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(ILandroid/content/Intent;)V

    .line 155
    :goto_29
    invoke-virtual {p0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->finish()V

    .line 156
    return-void

    .line 146
    :cond_2d
    const-string v0, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "android.intent.extra.shortcut.NAME"

    iget-object v2, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    goto :goto_1f

    .line 153
    :cond_3a
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(ILandroid/content/Intent;)V

    goto :goto_29
.end method

.method protected final b()Ljava/util/List;
    .registers 6

    .prologue
    .line 238
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 240
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->c:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(Ljava/util/List;Ljava/util/List;ZLjava/util/List;)V

    .line 241
    invoke-direct {p0, v1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(Ljava/util/List;)V

    .line 243
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_17
    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_26

    .line 250
    new-instance v0, Lcom/anddoes/launcher/appwidgetpicker/a;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/appwidgetpicker/a;-><init>(Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 258
    return-object v1

    .line 243
    :cond_26
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/f;

    .line 244
    instance-of v3, v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    if-eqz v3, :cond_17

    .line 245
    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    iget-object v3, v0, Lcom/anddoes/launcher/appwidgetpicker/b;->b:Ljava/util/List;

    new-instance v4, Lcom/anddoes/launcher/appwidgetpicker/c;

    invoke-direct {v4, v0}, Lcom/anddoes/launcher/appwidgetpicker/c;-><init>(Lcom/anddoes/launcher/appwidgetpicker/b;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_17
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 57
    const-string v0, "android.intent.extra.INTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 58
    instance-of v2, v0, Landroid/content/Intent;

    if-eqz v2, :cond_3f

    .line 59
    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a:Landroid/content/Intent;

    .line 65
    :goto_17
    invoke-virtual {p0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b:Landroid/content/pm/PackageManager;

    .line 66
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->c:Landroid/appwidget/AppWidgetManager;

    .line 69
    invoke-direct {p0, v4, v3}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(ILandroid/content/Intent;)V

    .line 72
    const-string v0, "appWidgetId"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 73
    const-string v0, "appWidgetId"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->d:I

    .line 79
    :goto_36
    new-instance v0, Lcom/anddoes/launcher/appwidgetpicker/e;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/appwidgetpicker/e;-><init>(Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;)V

    invoke-virtual {v0, v3}, Lcom/anddoes/launcher/appwidgetpicker/e;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    .line 80
    return-void

    .line 61
    :cond_3f
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a:Landroid/content/Intent;

    .line 62
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a:Landroid/content/Intent;

    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_17

    .line 76
    :cond_50
    invoke-virtual {p0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->finish()V

    goto :goto_36
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 85
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 86
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/AppWidgetPickActivity"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 87
    return-void
.end method
