.class public final Lcom/anddoes/launcher/appwidgetpicker/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private a:Lcom/anddoes/launcher/appwidgetpicker/d;

.field private b:Ljava/util/List;

.field private c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

.field private d:Landroid/app/AlertDialog;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    .line 31
    invoke-virtual {p1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->b:Ljava/util/List;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/anddoes/launcher/appwidgetpicker/f;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    if-eqz p1, :cond_8

    instance-of v0, p1, Lcom/anddoes/launcher/appwidgetpicker/b;

    if-eqz v0, :cond_6f

    .line 36
    :cond_8
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    if-nez p1, :cond_3e

    .line 39
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    const v2, 0x7f07003d

    invoke-virtual {v0, v2}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 40
    new-instance v0, Lcom/anddoes/launcher/appwidgetpicker/d;

    iget-object v2, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    iget-object v3, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->b:Ljava/util/List;

    invoke-direct {v0, v2, v3}, Lcom/anddoes/launcher/appwidgetpicker/d;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->a:Lcom/anddoes/launcher/appwidgetpicker/d;

    .line 41
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->a:Lcom/anddoes/launcher/appwidgetpicker/d;

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 42
    iput-boolean v5, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->e:Z

    .line 56
    :goto_2f
    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 57
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->d:Landroid/app/AlertDialog;

    .line 58
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 62
    :goto_3d
    return-void

    :cond_3e
    move-object v0, p1

    .line 44
    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    .line 45
    iget-object v2, v0, Lcom/anddoes/launcher/appwidgetpicker/b;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v5, :cond_57

    .line 46
    iget-object v1, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/appwidgetpicker/b;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/f;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    goto :goto_3d

    .line 50
    :cond_57
    iget-object v2, p1, Lcom/anddoes/launcher/appwidgetpicker/f;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 51
    new-instance v2, Lcom/anddoes/launcher/appwidgetpicker/d;

    iget-object v3, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/appwidgetpicker/b;->b:Ljava/util/List;

    invoke-direct {v2, v3, v0}, Lcom/anddoes/launcher/appwidgetpicker/d;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v2, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->a:Lcom/anddoes/launcher/appwidgetpicker/d;

    .line 52
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->a:Lcom/anddoes/launcher/appwidgetpicker/d;

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    iput-boolean v4, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->e:Z

    goto :goto_2f

    .line 60
    :cond_6f
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    invoke-virtual {v0, p1}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    goto :goto_3d
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->e:Z

    if-eqz v0, :cond_a

    .line 80
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->c:Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/appwidgetpicker/AppWidgetPickActivity;->a()V

    .line 85
    :goto_9
    return-void

    .line 82
    :cond_a
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/appwidgetpicker/e;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    goto :goto_9
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 71
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/e;->a:Lcom/anddoes/launcher/appwidgetpicker/d;

    invoke-virtual {v0, p2}, Lcom/anddoes/launcher/appwidgetpicker/d;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/f;

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/appwidgetpicker/e;->a(Lcom/anddoes/launcher/appwidgetpicker/f;)V

    .line 72
    return-void
.end method
