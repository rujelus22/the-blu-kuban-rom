.class public final Lcom/anddoes/launcher/appwidgetpicker/d;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 23
    iput-object p1, p0, Lcom/anddoes/launcher/appwidgetpicker/d;->a:Landroid/content/Context;

    .line 24
    iput-object p2, p0, Lcom/anddoes/launcher/appwidgetpicker/d;->b:Ljava/util/List;

    .line 25
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 29
    if-nez p2, :cond_18

    .line 30
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/d;->a:Landroid/content/Context;

    .line 31
    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 30
    check-cast v0, Landroid/view/LayoutInflater;

    .line 32
    const v1, 0x7f030018

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 34
    :cond_18
    iget-object v0, p0, Lcom/anddoes/launcher/appwidgetpicker/d;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/appwidgetpicker/f;

    .line 35
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 36
    if-eqz v0, :cond_8a

    .line 37
    const v1, 0x1020016

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 38
    if-eqz v1, :cond_37

    .line 39
    invoke-virtual {v0}, Lcom/anddoes/launcher/appwidgetpicker/f;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    :cond_37
    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 43
    if-eqz v1, :cond_6c

    .line 44
    instance-of v2, v0, Lcom/anddoes/launcher/appwidgetpicker/b;

    if-eqz v2, :cond_8f

    move-object v2, v0

    .line 45
    check-cast v2, Lcom/anddoes/launcher/appwidgetpicker/b;

    iget-object v2, v2, Lcom/anddoes/launcher/appwidgetpicker/b;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 46
    if-le v2, v6, :cond_8b

    .line 47
    iget-object v3, p0, Lcom/anddoes/launcher/appwidgetpicker/d;->a:Landroid/content/Context;

    const v4, 0x7f07003e

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 57
    :cond_6c
    :goto_6c
    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 58
    if-eqz v1, :cond_8a

    invoke-virtual {v0}, Lcom/anddoes/launcher/appwidgetpicker/f;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_8a

    .line 59
    invoke-virtual {v0}, Lcom/anddoes/launcher/appwidgetpicker/f;->b()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v2, p0, Lcom/anddoes/launcher/appwidgetpicker/d;->a:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/android/launcher2/jj;->b(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 62
    :cond_8a
    return-object p2

    .line 50
    :cond_8b
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6c

    .line 53
    :cond_8f
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6c
.end method
