.class final Lcom/anddoes/launcher/ac;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/w;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/w;)V
    .registers 2
    .parameter

    .prologue
    .line 484
    iput-object p1, p0, Lcom/anddoes/launcher/ac;->a:Lcom/anddoes/launcher/w;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/w;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ac;-><init>(Lcom/anddoes/launcher/w;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 488
    iget-object v0, p0, Lcom/anddoes/launcher/ac;->a:Lcom/anddoes/launcher/w;

    invoke-static {v0}, Lcom/anddoes/launcher/w;->d(Lcom/anddoes/launcher/w;)Lcom/android/launcher2/Launcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v0

    if-nez v0, :cond_e

    .line 500
    :cond_d
    :goto_d
    return-void

    .line 491
    :cond_e
    const-string v0, "package"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 492
    const-string v0, "class"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 493
    const-string v0, "count"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 494
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 497
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "-"

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 498
    iget-object v3, p0, Lcom/anddoes/launcher/ac;->a:Lcom/anddoes/launcher/w;

    invoke-static {v3}, Lcom/anddoes/launcher/w;->e(Lcom/anddoes/launcher/w;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 499
    iget-object v3, p0, Lcom/anddoes/launcher/ac;->a:Lcom/anddoes/launcher/w;

    if-lez v0, :cond_54

    :goto_50
    invoke-static {v3, v2, v0}, Lcom/anddoes/launcher/w;->a(Lcom/anddoes/launcher/w;Ljava/lang/String;I)V

    goto :goto_d

    :cond_54
    move v0, v1

    goto :goto_50
.end method
