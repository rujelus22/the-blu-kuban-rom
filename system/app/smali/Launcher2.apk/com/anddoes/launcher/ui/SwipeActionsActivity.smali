.class public Lcom/anddoes/launcher/ui/SwipeActionsActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private a:Lcom/android/launcher2/LauncherApplication;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/Spinner;

.field private h:Landroid/widget/Spinner;

.field private i:Lcom/android/launcher2/jd;

.field private j:Ljava/lang/String;

.field private k:Lcom/anddoes/launcher/preference/bb;

.field private l:[Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->j:Ljava/lang/String;

    .line 66
    iput-boolean v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->C:Z

    .line 67
    iput-boolean v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->D:Z

    .line 69
    iput-boolean v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->E:Z

    .line 70
    iput-boolean v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->F:Z

    .line 27
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 153
    move v0, v1

    :goto_2
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->l:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_9

    move v0, v1

    .line 158
    :cond_8
    return v0

    .line 154
    :cond_9
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->l:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private a()V
    .registers 8

    .prologue
    const/4 v6, 0x4

    const v5, 0x7f0700f3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 267
    const-string v0, "LAUNCH_APP"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 268
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_53

    .line 269
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->e:Landroid/widget/TextView;

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->o:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v5, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    :goto_2c
    const-string v0, "LAUNCH_APP"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 280
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_80

    .line 281
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->f:Landroid/widget/TextView;

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->w:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v5, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    :goto_52
    return-void

    .line 271
    :cond_53
    const-string v0, "LAUNCH_SHORTCUT"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 272
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7a

    .line 273
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->e:Landroid/widget/TextView;

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->s:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v5, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2c

    .line 276
    :cond_7a
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2c

    .line 283
    :cond_80
    const-string v0, "LAUNCH_SHORTCUT"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 284
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a7

    .line 285
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->f:Landroid/widget/TextView;

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->A:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v5, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_52

    .line 288
    :cond_a7
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_52
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/SwipeActionsActivity;Landroid/widget/Spinner;I)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    .line 214
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->g:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_9d

    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->l:[Ljava/lang/String;

    aget-object v0, v0, p2

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->E:Z

    if-nez v0, :cond_60

    const-string v0, "LAUNCH_APP"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_66

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_up_app_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->o:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "swipe_up_intent_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v3, v3, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->p:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "swipe_up_component_"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v5, v5, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->n:Ljava/lang/String;

    iget-object v7, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->q:Ljava/lang/String;

    iget-object v8, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->r:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_60
    :goto_60
    iput-boolean v9, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->E:Z

    :cond_62
    :goto_62
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a()V

    return-void

    :cond_66
    const-string v0, "LAUNCH_SHORTCUT"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_60

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_up_shortcut_name_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_shortcut_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->b(Ljava/lang/String;)V

    goto :goto_60

    :cond_9d
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->h:Landroid/widget/Spinner;

    if-ne p1, v0, :cond_62

    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->l:[Ljava/lang/String;

    aget-object v0, v0, p2

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->F:Z

    if-nez v0, :cond_fc

    const-string v0, "LAUNCH_APP"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_100

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_down_app_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->w:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "swipe_down_intent_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v3, v3, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->x:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "swipe_down_component_"

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v5, v5, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->v:Ljava/lang/String;

    iget-object v7, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->y:Ljava/lang/String;

    iget-object v8, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->z:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_fc
    :goto_fc
    iput-boolean v9, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->F:Z

    goto/16 :goto_62

    :cond_100
    const-string v0, "LAUNCH_SHORTCUT"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_fc

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "swipe_down_shortcut_name_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_shortcut_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->b(Ljava/lang/String;)V

    goto :goto_fc
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 295
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 297
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 307
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 309
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    .line 248
    iput-object p1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->j:Ljava/lang/String;

    .line 249
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 251
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 252
    const v2, 0x7f070176

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 255
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 256
    const v2, 0x7f020037

    invoke-static {p0, v2}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 259
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 260
    const-string v2, "android.intent.extra.INTENT"

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 261
    const-string v2, "android.intent.extra.TITLE"

    const v3, 0x7f070291

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 262
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 263
    const/4 v0, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 264
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x5

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 313
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3b

    .line 314
    if-ne p1, v1, :cond_4b

    .line 316
    :try_start_9
    sget-object v0, Lcom/anddoes/launcher/ui/AppPickerActivity;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 317
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->b:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 318
    sget-object v2, Lcom/anddoes/launcher/ui/AppPickerActivity;->f:Ljava/lang/String;

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 319
    sget-object v3, Lcom/anddoes/launcher/ui/AppPickerActivity;->h:Ljava/lang/String;

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 320
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3b

    .line 321
    const-string v4, "swipe_up_app_"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 322
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->C:Z

    .line 323
    iput-object v3, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->n:Ljava/lang/String;

    .line 324
    iput-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->o:Ljava/lang/String;

    .line 325
    iput-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->p:Ljava/lang/String;

    .line 332
    :goto_38
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a()V
    :try_end_3b
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_3b} :catch_49

    .line 365
    :cond_3b
    :goto_3b
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 366
    :goto_3e
    return-void

    .line 327
    :cond_3f
    const/4 v0, 0x1

    :try_start_40
    iput-boolean v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->D:Z

    .line 328
    iput-object v3, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->v:Ljava/lang/String;

    .line 329
    iput-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->w:Ljava/lang/String;

    .line 330
    iput-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->x:Ljava/lang/String;
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_40 .. :try_end_48} :catch_49

    goto :goto_38

    .line 334
    :catch_49
    move-exception v0

    goto :goto_3b

    .line 336
    :cond_4b
    const/4 v0, 0x4

    if-ne p1, v0, :cond_92

    .line 337
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 338
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 339
    if-eqz v0, :cond_75

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 340
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 341
    const-class v1, Lcom/anddoes/launcher/ui/ActPickerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 342
    invoke-virtual {p0, v0, v3}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_3e

    .line 344
    :cond_75
    const/4 v0, 0x5

    :try_start_76
    invoke-virtual {p0, p3, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_79
    .catch Landroid/content/ActivityNotFoundException; {:try_start_76 .. :try_end_79} :catch_7a
    .catch Ljava/lang/SecurityException; {:try_start_76 .. :try_end_79} :catch_86

    goto :goto_3e

    :catch_7a
    move-exception v0

    const v0, 0x7f070279

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3e

    :catch_86
    move-exception v0

    const v0, 0x7f070178

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_3e

    .line 347
    :cond_92
    if-eq p1, v3, :cond_96

    if-ne p1, v2, :cond_3b

    .line 348
    :cond_96
    const-string v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 349
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 350
    if-eqz v0, :cond_cb

    if-eqz v1, :cond_cb

    .line 351
    const-string v2, "android.intent.action.CALL_PRIVILEGED"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b9

    .line 352
    const-string v2, "android.intent.action.CALL"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 354
    :cond_b9
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->j:Ljava/lang/String;

    const-string v3, "swipe_up_shortcut_name_"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d0

    .line 355
    iput-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->s:Ljava/lang/String;

    .line 356
    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->t:Ljava/lang/String;

    .line 362
    :cond_cb
    :goto_cb
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a()V

    goto/16 :goto_3b

    .line 358
    :cond_d0
    iput-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->A:Ljava/lang/String;

    .line 359
    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->B:Ljava/lang/String;

    goto :goto_cb
.end method

.method public onClick(Landroid/view/View;)V
    .registers 8
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->c:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_174

    .line 170
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->b:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17c

    .line 173
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    iget-object v3, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "swipe_up_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const-string v0, "LAUNCH_APP"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17d

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->C:Z

    if-eqz v0, :cond_17d

    .line 175
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_component_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 176
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->n:Ljava/lang/String;

    .line 175
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_app_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 178
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->o:Ljava/lang/String;

    .line 177
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 180
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->p:Ljava/lang/String;

    .line 179
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c2

    .line 182
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_pkg_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_act_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_c2
    :goto_c2
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    iget-object v3, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "swipe_down_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    const-string v0, "LAUNCH_APP"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1bd

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->D:Z

    if-eqz v0, :cond_1bd

    .line 193
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_component_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 194
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->v:Ljava/lang/String;

    .line 193
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_app_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 196
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->w:Ljava/lang/String;

    .line 195
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 198
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->x:Ljava/lang/String;

    .line 197
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->y:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_174

    .line 200
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_pkg_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_act_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_174
    :goto_174
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a:Lcom/android/launcher2/LauncherApplication;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    .line 210
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->finish()V

    .line 212
    :cond_17c
    return-void

    .line 185
    :cond_17d
    const-string v0, "LAUNCH_SHORTCUT"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 186
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_shortcut_name_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 187
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->s:Ljava/lang/String;

    .line 186
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_shortcut_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 189
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->t:Ljava/lang/String;

    .line 188
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_c2

    .line 203
    :cond_1bd
    const-string v0, "LAUNCH_SHORTCUT"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_174

    .line 204
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_shortcut_name_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 205
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->A:Ljava/lang/String;

    .line 204
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_shortcut_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 207
    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->B:Ljava/lang/String;

    .line 206
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_174
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 74
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a:Lcom/android/launcher2/LauncherApplication;

    .line 76
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    if-eqz v0, :cond_1a

    .line 77
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    instance-of v0, v0, Lcom/android/launcher2/jd;

    if-nez v0, :cond_1d

    .line 78
    :cond_1a
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->finish()V

    .line 80
    :cond_1d
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->g:Lcom/android/launcher2/di;

    check-cast v0, Lcom/android/launcher2/jd;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    .line 81
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->setContentView(I)V

    .line 83
    const v0, 0x7f0d0022

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->b:Landroid/widget/Button;

    .line 84
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    const v0, 0x7f0d0021

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->c:Landroid/widget/Button;

    .line 86
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v0, 0x7f0d0003

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->d:Landroid/widget/TextView;

    .line 89
    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-object v0, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    if-nez v0, :cond_263

    const-string v0, ""

    :goto_60
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    const v0, 0x7f0d0057

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->e:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0d0059

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->f:Landroid/widget/TextView;

    .line 93
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->l:[Ljava/lang/String;

    .line 95
    const v0, 0x7f0d0056

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->g:Landroid/widget/Spinner;

    .line 96
    const v0, 0x7f0d0058

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->h:Landroid/widget/Spinner;

    .line 98
    new-instance v0, Lcom/anddoes/launcher/preference/bb;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/bb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    .line 99
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_app_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->o:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->p:Ljava/lang/String;

    .line 102
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_component_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->n:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_pkg_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->q:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_act_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->r:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_shortcut_name_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->s:Ljava/lang/String;

    .line 106
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_up_shortcut_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->t:Ljava/lang/String;

    .line 108
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v1, v1, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/bb;->b(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    .line 109
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_app_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->w:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->x:Ljava/lang/String;

    .line 111
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_component_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->v:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_pkg_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->y:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_act_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->z:Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_shortcut_name_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->A:Ljava/lang/String;

    .line 115
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->k:Lcom/anddoes/launcher/preference/bb;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "swipe_down_shortcut_intent_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-wide v2, v2, Lcom/android/launcher2/jd;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/anddoes/launcher/preference/bb;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->B:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->g:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->m:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 118
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->g:Landroid/widget/Spinner;

    new-instance v1, Lcom/anddoes/launcher/ui/ao;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/ui/ao;-><init>(Lcom/anddoes/launcher/ui/SwipeActionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 133
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->h:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->u:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 134
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->h:Landroid/widget/Spinner;

    new-instance v1, Lcom/anddoes/launcher/ui/ap;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/ui/ap;-><init>(Lcom/anddoes/launcher/ui/SwipeActionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 149
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->a()V

    .line 150
    return-void

    .line 89
    :cond_263
    iget-object v0, p0, Lcom/anddoes/launcher/ui/SwipeActionsActivity;->i:Lcom/android/launcher2/jd;

    iget-object v0, v0, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    goto/16 :goto_60
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 164
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/SwipeActions"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 165
    return-void
.end method
