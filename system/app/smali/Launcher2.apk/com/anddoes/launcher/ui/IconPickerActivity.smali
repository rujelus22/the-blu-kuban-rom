.class public Lcom/anddoes/launcher/ui/IconPickerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/anddoes/launcher/c/i;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/IconPickerActivity;)Lcom/anddoes/launcher/c/i;
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    return-object v0
.end method

.method static synthetic b(Lcom/anddoes/launcher/ui/IconPickerActivity;)I
    .registers 2
    .parameter

    .prologue
    .line 37
    iget v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->a:I

    return v0
.end method

.method static synthetic c(Lcom/anddoes/launcher/ui/IconPickerActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f03001a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->setContentView(I)V

    .line 46
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/ui/IconPickerActivity;->setResult(I)V

    .line 48
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 49
    if-nez v0, :cond_16

    .line 50
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->finish()V

    .line 53
    :cond_16
    invoke-static {p0}, Lcom/anddoes/launcher/v;->e(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->a:I

    .line 54
    const-string v1, "com.anddoes.launcher.THEME_PACKAGE_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    .line 55
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3d

    .line 56
    const v1, 0x7f070279

    invoke-static {p0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 57
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->finish()V

    .line 60
    :cond_3d
    const-string v1, "com.anddoes.launcher.THEME_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->c:Ljava/lang/String;

    .line 61
    const-string v1, "com.anddoes.launcher.THEME_TYPE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->d:Ljava/lang/String;

    .line 62
    const-string v0, "apex_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b0

    .line 63
    new-instance v0, Lcom/anddoes/launcher/c/b;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    .line 74
    :goto_60
    iget-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6d

    .line 75
    iget-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 77
    :cond_6d
    iget-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->m()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 78
    if-eqz v0, :cond_7c

    .line 79
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 82
    :cond_7c
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 83
    const/high16 v1, 0x105

    .line 82
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 84
    const v0, 0x7f0d0034

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 85
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 86
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setColumnWidth(I)V

    .line 87
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setStretchMode(I)V

    .line 88
    div-int/lit8 v2, v1, 0x3

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 89
    div-int/lit8 v2, v1, 0x3

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    .line 90
    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 91
    new-instance v2, Lcom/anddoes/launcher/ui/ag;

    invoke-direct {v2, p0, p0, v1}, Lcom/anddoes/launcher/ui/ag;-><init>(Lcom/anddoes/launcher/ui/IconPickerActivity;Landroid/content/Context;I)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 92
    return-void

    .line 64
    :cond_b0
    const-string v0, "adw_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c4

    .line 65
    new-instance v0, Lcom/anddoes/launcher/c/a;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    goto :goto_60

    .line 66
    :cond_c4
    const-string v0, "lp_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d8

    .line 67
    new-instance v0, Lcom/anddoes/launcher/c/f;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    goto :goto_60

    .line 68
    :cond_d8
    const-string v0, "go_theme"

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 69
    new-instance v0, Lcom/anddoes/launcher/c/c;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    goto/16 :goto_60

    .line 71
    :cond_ed
    new-instance v0, Lcom/anddoes/launcher/c/b;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/c/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    .line 72
    iget-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->e:Lcom/anddoes/launcher/c/i;

    invoke-virtual {v0}, Lcom/anddoes/launcher/c/i;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/IconPickerActivity;->c:Ljava/lang/String;

    goto/16 :goto_60
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 104
    const/4 v1, 0x0

    .line 106
    :try_start_6
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/Adapter;

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_12} :catch_21

    .line 109
    :goto_12
    if-eqz v0, :cond_24

    .line 110
    const-string v1, "icon"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 111
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/anddoes/launcher/ui/IconPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 115
    :goto_1d
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/IconPickerActivity;->finish()V

    .line 116
    return-void

    :catch_21
    move-exception v0

    move-object v0, v1

    goto :goto_12

    .line 113
    :cond_24
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/anddoes/launcher/ui/IconPickerActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_1d
.end method

.method public onStart()V
    .registers 3

    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 98
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/IconPicker"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 99
    return-void
.end method
