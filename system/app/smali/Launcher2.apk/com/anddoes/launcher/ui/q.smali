.class final Lcom/anddoes/launcher/ui/q;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field b:Ljava/util/List;

.field final synthetic c:Lcom/anddoes/launcher/ui/DrawerConfigActivity;

.field private final d:Landroid/widget/AbsListView$LayoutParams;

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)V
    .registers 6
    .parameter

    .prologue
    .line 354
    iput-object p1, p0, Lcom/anddoes/launcher/ui/q;->c:Lcom/anddoes/launcher/ui/DrawerConfigActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/q;->a:Ljava/util/List;

    .line 350
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/q;->b:Ljava/util/List;

    .line 355
    invoke-virtual {p1}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 356
    const/high16 v1, 0x105

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/anddoes/launcher/ui/q;->e:I

    .line 357
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    .line 358
    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 357
    iput-object v1, p0, Lcom/anddoes/launcher/ui/q;->d:Landroid/widget/AbsListView$LayoutParams;

    .line 360
    iget-object v1, p0, Lcom/anddoes/launcher/ui/q;->a:Ljava/util/List;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    iget-object v1, p0, Lcom/anddoes/launcher/ui/q;->b:Ljava/util/List;

    const v2, 0x7f07004b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    iget-object v1, p0, Lcom/anddoes/launcher/ui/q;->a:Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    iget-object v1, p0, Lcom/anddoes/launcher/ui/q;->b:Ljava/util/List;

    const v2, 0x7f07004c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    return-void
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/anddoes/launcher/ui/q;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 373
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 378
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 384
    if-nez p2, :cond_4f

    .line 386
    iget-object v0, p0, Lcom/anddoes/launcher/ui/q;->c:Lcom/anddoes/launcher/ui/DrawerConfigActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 387
    const v1, 0x7f030020

    .line 386
    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 388
    iget-object v1, p0, Lcom/anddoes/launcher/ui/q;->d:Landroid/widget/AbsListView$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 389
    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 390
    iget v1, p0, Lcom/anddoes/launcher/ui/q;->e:I

    div-int/lit8 v1, v1, 0x6

    iget v2, p0, Lcom/anddoes/launcher/ui/q;->e:I

    div-int/lit8 v2, v2, 0x4

    iget v3, p0, Lcom/anddoes/launcher/ui/q;->e:I

    div-int/lit8 v3, v3, 0x4

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 391
    iget-object v1, p0, Lcom/anddoes/launcher/ui/q;->c:Lcom/anddoes/launcher/ui/DrawerConfigActivity;

    const v2, 0x1030044

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    move-object p2, v0

    .line 395
    :goto_34
    iget-object v0, p0, Lcom/anddoes/launcher/ui/q;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget v0, p0, Lcom/anddoes/launcher/ui/q;->e:I

    div-int/lit8 v0, v0, 0x6

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 398
    if-nez p1, :cond_52

    const v0, 0x7f02007b

    .line 397
    :goto_4b
    invoke-virtual {p2, v0, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 400
    return-object p2

    .line 393
    :cond_4f
    check-cast p2, Landroid/widget/TextView;

    goto :goto_34

    .line 399
    :cond_52
    const v0, 0x7f020034

    goto :goto_4b
.end method
