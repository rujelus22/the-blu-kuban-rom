.class public Lcom/anddoes/launcher/ui/GroupConfigActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;

.field public static d:Ljava/lang/String;

.field public static e:Ljava/lang/String;

.field public static f:Ljava/lang/String;

.field public static g:Ljava/lang/String;

.field public static h:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/anddoes/launcher/preference/c;

.field private C:Landroid/widget/CheckBox;

.field private D:[Ljava/lang/String;

.field private E:Lcom/android/launcher2/LauncherApplication;

.field public i:Ljava/util/ArrayList;

.field private j:Lcom/android/launcher2/da;

.field private k:Ljava/util/ArrayList;

.field private l:Landroid/view/LayoutInflater;

.field private m:Landroid/widget/ListView;

.field private n:Lcom/anddoes/launcher/ui/ad;

.field private o:Ljava/util/HashMap;

.field private p:Ljava/lang/String;

.field private q:Landroid/widget/EditText;

.field private r:Landroid/widget/Spinner;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:[Ljava/lang/String;

.field private w:[Ljava/lang/String;

.field private x:Z

.field private y:Lcom/anddoes/launcher/preference/h;

.field private z:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 54
    const-string v0, "extra_title"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a:Ljava/lang/String;

    .line 55
    const-string v0, "extra_init_list"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b:Ljava/lang/String;

    .line 56
    const-string v0, "extra_result"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->c:Ljava/lang/String;

    .line 57
    const-string v0, "extra_return_arg"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d:Ljava/lang/String;

    .line 58
    const-string v0, "extra_result_name"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->e:Ljava/lang/String;

    .line 59
    const-string v0, "extra_init_name"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->f:Ljava/lang/String;

    .line 60
    const-string v0, "extra_name_list"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->g:Ljava/lang/String;

    .line 61
    const-string v0, "extra_is_app_group"

    sput-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->h:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    .line 49
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 248
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->w:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_c

    .line 253
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->w:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :cond_b
    return v0

    .line 249
    :cond_c
    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->w:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private a()V
    .registers 12

    .prologue
    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 322
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 323
    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_c9

    .line 324
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->E:Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->b()Lcom/android/launcher2/gb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/gb;->j()Lcom/android/launcher2/d;

    move-result-object v0

    iget-object v0, v0, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 325
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->y:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->A()Ljava/lang/String;

    move-result-object v0

    .line 327
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->C:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    .line 328
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    .line 329
    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 330
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 332
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v2, v1

    .line 333
    :goto_42
    if-lt v2, v7, :cond_65

    .line 343
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8e

    .line 347
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_64

    .line 348
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5e
    :goto_5e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_a2

    .line 388
    :cond_64
    return-void

    .line 334
    :cond_65
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 335
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v8

    .line 336
    invoke-static {v8, v5}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_7f

    .line 337
    if-eqz v4, :cond_85

    iget-object v9, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->D:[Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_85

    .line 338
    :cond_7f
    invoke-static {v8, v6}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8a

    .line 339
    :cond_85
    iget-object v8, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_8a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_42

    .line 343
    :cond_8e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 344
    iget-object v3, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4a

    .line 348
    :cond_a2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 349
    array-length v4, v6

    move v2, v1

    :goto_aa
    if-ge v2, v4, :cond_5e

    aget-object v5, v6, v2

    .line 350
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c6

    .line 351
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5e

    .line 349
    :cond_c6
    add-int/lit8 v2, v2, 0x1

    goto :goto_aa

    .line 358
    :cond_c9
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->E:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->i:Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 359
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    .line 360
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    const-string v4, ";"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    .line 363
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v0, v1

    .line 364
    :goto_ec
    if-lt v0, v5, :cond_119

    move v0, v1

    .line 372
    :goto_ef
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_13d

    .line 376
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_64

    move v0, v1

    .line 377
    :goto_100
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_64

    .line 378
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 379
    array-length v5, v4

    move v2, v1

    :goto_114
    if-lt v2, v5, :cond_153

    .line 377
    :goto_116
    add-int/lit8 v0, v0, 0x1

    goto :goto_100

    .line 365
    :cond_119
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 366
    if-eqz v3, :cond_131

    iget-object v7, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->D:[Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_131

    .line 367
    invoke-static {v6, v4}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_13a

    .line 368
    :cond_131
    iget-object v6, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 364
    :cond_13a
    add-int/lit8 v0, v0, 0x1

    goto :goto_ec

    .line 373
    :cond_13d
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_ef

    .line 379
    :cond_153
    aget-object v6, v4, v2

    .line 380
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_165

    .line 381
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_116

    .line 379
    :cond_165
    add-int/lit8 v2, v2, 0x1

    goto :goto_114
.end method

.method private a(Landroid/view/View;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_42

    .line 259
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 260
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 264
    :goto_11
    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4e

    const/4 v0, 0x0

    :goto_22
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const v0, 0x7f0d0006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 266
    return-void

    .line 262
    :cond_42
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_11

    .line 264
    :cond_4e
    const/4 v0, 0x1

    goto :goto_22
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/GroupConfigActivity;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->w:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_23

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->q:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_22
    :goto_22
    return-void

    :cond_23
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->C:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->r:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    if-nez v0, :cond_4e

    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->q:Landroid/widget/EditText;

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_4a

    const v0, 0x7f070293

    :goto_46
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(I)V

    goto :goto_22

    :cond_4a
    const v0, 0x7f07027a

    goto :goto_46

    :cond_4e
    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->q:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->v:[Ljava/lang/String;

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_22
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/GroupConfigActivity;Landroid/view/View;I)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 256
    invoke-direct {p0, p1, p2}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    return v0
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 178
    array-length v2, p1

    move v1, v0

    :goto_3
    if-lt v1, v2, :cond_6

    .line 183
    :goto_5
    return v0

    .line 178
    :cond_6
    aget-object v3, p1, v1

    .line 179
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 180
    const/4 v0, 0x1

    goto :goto_5

    .line 178
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method private b()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 392
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 394
    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_47

    .line 395
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_10
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_25

    .line 409
    :cond_16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 410
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v4, :cond_24

    .line 411
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 413
    :cond_24
    return-object v0

    .line 395
    :cond_25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 396
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v3

    .line 397
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 398
    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_10

    .line 402
    :cond_47
    const/4 v0, 0x0

    move v1, v0

    :goto_49
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_16

    .line 403
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 404
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_72

    .line 405
    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    :cond_72
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_49
.end method

.method static synthetic b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->l:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic d(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Lcom/android/launcher2/da;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->j:Lcom/android/launcher2/da;

    return-object v0
.end method

.method static synthetic e(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->A:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Landroid/content/pm/PackageManager;
    .registers 2
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->z:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic g(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/HashMap;
    .registers 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->o:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic h(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a()V

    return-void
.end method

.method static synthetic i(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Lcom/anddoes/launcher/ui/ad;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->n:Lcom/anddoes/launcher/ui/ad;

    return-object v0
.end method


# virtual methods
.method final a(Landroid/content/ComponentName;II)[I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 417
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/appwidget/AppWidgetHostView;->getDefaultPaddingForWidget(Landroid/content/Context;Landroid/content/ComponentName;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 420
    iget v1, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p2

    iget v2, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    .line 421
    iget v2, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v2, p3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    .line 422
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/android/launcher2/CellLayout;->a(Landroid/content/res/Resources;II)[I

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const v0, 0x7f030014

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->setContentView(I)V

    .line 92
    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->setResult(I)V

    .line 93
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 94
    if-nez v3, :cond_17

    .line 95
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->finish()V

    .line 98
    :cond_17
    new-instance v0, Lcom/anddoes/launcher/preference/h;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->y:Lcom/anddoes/launcher/preference/h;

    .line 99
    new-instance v0, Lcom/anddoes/launcher/preference/c;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->B:Lcom/anddoes/launcher/preference/c;

    .line 100
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->z:Landroid/content/pm/PackageManager;

    .line 101
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07027d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->A:Ljava/lang/String;

    .line 102
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->u:Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->h:Ljava/lang/String;

    invoke-virtual {v3, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    .line 104
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->E:Lcom/android/launcher2/LauncherApplication;

    .line 105
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->E:Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->a()Lcom/android/launcher2/da;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->j:Lcom/android/launcher2/da;

    .line 107
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->l:Landroid/view/LayoutInflater;

    .line 108
    const v0, 0x7f0d002b

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->C:Landroid/widget/CheckBox;

    .line 110
    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_153

    .line 111
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->B:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/c;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->D:[Ljava/lang/String;

    .line 115
    :goto_7f
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    .line 116
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    if-nez v0, :cond_8f

    .line 117
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    .line 119
    :cond_8f
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a()V

    .line 121
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_9d

    .line 123
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 126
    :cond_9d
    const v0, 0x7f0d002a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->q:Landroid/widget/EditText;

    .line 127
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->f:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    .line 128
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    if-nez v0, :cond_1a6

    .line 129
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    .line 133
    :goto_b8
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->q:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 134
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->g:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->t:Ljava/lang/String;

    .line 135
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->t:Ljava/lang/String;

    if-nez v0, :cond_cf

    .line 136
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->t:Ljava/lang/String;

    .line 139
    :cond_cf
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->m:Landroid/widget/ListView;

    .line 140
    new-instance v0, Lcom/anddoes/launcher/ui/ad;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/ui/ad;-><init>(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->n:Lcom/anddoes/launcher/ui/ad;

    .line 141
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->n:Lcom/anddoes/launcher/ui/ad;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 142
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 143
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 144
    const v0, 0x7f0d0029

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->r:Landroid/widget/Spinner;

    .line 145
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_1b0

    .line 146
    const v0, 0x7f0c0001

    .line 145
    :goto_108
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->w:[Ljava/lang/String;

    .line 147
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_1b5

    .line 148
    const/high16 v0, 0x7f0c

    .line 147
    :goto_118
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->v:[Ljava/lang/String;

    .line 150
    new-instance v0, Landroid/widget/ArrayAdapter;

    .line 151
    const v1, 0x1090008

    iget-object v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->v:[Ljava/lang/String;

    .line 150
    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 152
    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 153
    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->r:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 154
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->r:Landroid/widget/Spinner;

    new-instance v1, Lcom/anddoes/launcher/ui/z;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/ui/z;-><init>(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 166
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->r:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->p:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 168
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->C:Landroid/widget/CheckBox;

    new-instance v1, Lcom/anddoes/launcher/ui/aa;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/ui/aa;-><init>(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 175
    return-void

    .line 113
    :cond_153
    iget-object v4, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->B:Lcom/anddoes/launcher/preference/c;

    const-string v1, ""

    invoke-virtual {v4}, Lcom/anddoes/launcher/preference/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_165

    invoke-virtual {v4}, Lcom/anddoes/launcher/preference/c;->b()Ljava/lang/String;

    move-result-object v0

    :cond_165
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1ba

    const-string v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    move-object v0, v1

    move v1, v2

    :goto_174
    if-lt v1, v6, :cond_180

    :goto_176
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->D:[Ljava/lang/String;

    goto/16 :goto_7f

    :cond_180
    aget-object v2, v5, v1

    invoke-virtual {v4, v2}, Lcom/anddoes/launcher/preference/c;->b(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1a3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Lcom/anddoes/launcher/preference/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1a3
    add-int/lit8 v1, v1, 0x1

    goto :goto_174

    .line 131
    :cond_1a6
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto/16 :goto_b8

    .line 146
    :cond_1b0
    const v0, 0x7f0c0003

    goto/16 :goto_108

    .line 148
    :cond_1b5
    const v0, 0x7f0c0002

    goto/16 :goto_118

    :cond_1ba
    move-object v0, v1

    goto :goto_176
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 194
    const/4 v0, 0x1

    const v1, 0x7f070008

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 196
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 197
    const/4 v0, 0x2

    const v1, 0x7f07000b

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 199
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 201
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 219
    if-ltz p3, :cond_11

    iget-boolean v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_c
    if-ge p3, v0, :cond_11

    .line 220
    invoke-direct {p0, p2, p3}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Landroid/view/View;I)V

    .line 222
    :cond_11
    return-void

    .line 219
    :cond_12
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_c
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 12
    .parameter

    .prologue
    const v9, 0x7f070007

    const v8, 0x7f070005

    const v7, 0x1080027

    const/4 v1, 0x1

    .line 206
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_f0

    .line 214
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_15
    return v0

    .line 208
    :pswitch_16
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->q:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ";"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_54

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f070055

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/anddoes/launcher/ui/ab;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/ui/ab;-><init>(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V

    invoke-virtual {v0, v9, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_52
    move v0, v1

    .line 209
    goto :goto_15

    .line 208
    :cond_54
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_68

    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->t:Ljava/lang/String;

    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_66
    if-lt v0, v4, :cond_a9

    :cond_68
    iget-object v0, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->w:[Ljava/lang/String;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->r:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v3

    aget-object v0, v0, v3

    const-string v3, "LIST_CUSTOM"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7e

    invoke-direct {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b()Ljava/lang/String;

    move-result-object v0

    :cond_7e
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    iget-object v4, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->u:Ljava/lang/String;

    if-eqz v4, :cond_8e

    sget-object v4, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_8e
    sget-object v4, Lcom/anddoes/launcher/ui/GroupConfigActivity;->c:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    if-eqz v2, :cond_9a

    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->e:Ljava/lang/String;

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_9a
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->h:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->x:Z

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v3}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->finish()V

    goto :goto_52

    :cond_a9
    aget-object v5, v3, v0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_e5

    iget-object v6, p0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->s:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_e5

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f070056

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/anddoes/launcher/ui/ac;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/ui/ac;-><init>(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V

    invoke-virtual {v0, v9, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_52

    :cond_e5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_66

    .line 211
    :pswitch_e9
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->finish()V

    move v0, v1

    .line 212
    goto/16 :goto_15

    .line 206
    nop

    :pswitch_data_f0
    .packed-switch 0x1
        :pswitch_e9
        :pswitch_16
    .end packed-switch
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 188
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 189
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/GroupConfig"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 190
    return-void
.end method
