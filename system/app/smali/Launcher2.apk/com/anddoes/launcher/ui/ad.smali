.class final Lcom/anddoes/launcher/ui/ad;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/ui/GroupConfigActivity;


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/ui/GroupConfigActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 427
    iput-object p1, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 428
    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/ad;)Lcom/anddoes/launcher/ui/GroupConfigActivity;
    .registers 2
    .parameter

    .prologue
    .line 425
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 431
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_12
    return v0

    :cond_13
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_12
.end method

.method public final getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 435
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 436
    :goto_12
    if-ltz p1, :cond_16

    if-lt p1, v0, :cond_21

    .line 437
    :cond_16
    const/4 v0, 0x0

    .line 439
    :goto_17
    return-object v0

    .line 435
    :cond_18
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_12

    .line 439
    :cond_21
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_17

    :cond_34
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_17
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 443
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 444
    :goto_12
    if-ltz p1, :cond_16

    if-lt p1, v0, :cond_29

    .line 445
    :cond_16
    const-string v0, "GroupConfigActivity"

    const-string v1, "Position out of bounds in List Adapter"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    const-wide/16 v0, -0x1

    .line 448
    :goto_1f
    return-wide v0

    .line 443
    :cond_20
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_12

    .line 448
    :cond_29
    int-to-long v0, p1

    goto :goto_1f
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 452
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_41

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 453
    :goto_15
    if-lt p1, v0, :cond_4a

    .line 454
    const-string v0, "GroupConfigActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid view position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", actual size is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    const/4 p2, 0x0

    .line 502
    :cond_40
    :goto_40
    return-object p2

    .line 452
    :cond_41
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_15

    .line 458
    :cond_4a
    if-nez p2, :cond_d8

    .line 460
    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->c(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Z

    move-result v0

    if-eqz v0, :cond_d4

    const v0, 0x7f030005

    :goto_5d
    invoke-virtual {v1, v0, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 462
    new-instance v0, Lcom/anddoes/launcher/ui/af;

    invoke-direct {v0, p0, p2}, Lcom/anddoes/launcher/ui/af;-><init>(Lcom/anddoes/launcher/ui/ad;Landroid/view/View;)V

    .line 463
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 468
    :goto_6a
    invoke-virtual {p0, p1}, Lcom/anddoes/launcher/ui/ad;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 469
    if-eqz v0, :cond_40

    .line 470
    const-string v3, ""

    .line 471
    const-string v2, ""

    .line 472
    instance-of v4, v0, Lcom/android/launcher2/h;

    if-eqz v4, :cond_e0

    .line 473
    check-cast v0, Lcom/android/launcher2/h;

    .line 474
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->a()Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Lcom/android/launcher2/da;

    move-result-object v3

    iget-object v4, v0, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/da;->a(Landroid/content/Intent;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 475
    iget-object v2, v0, Lcom/android/launcher2/h;->b:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 476
    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v0

    move-object v3, v2

    move-object v2, v0

    .line 494
    :cond_99
    :goto_99
    iget-object v0, v1, Lcom/anddoes/launcher/ui/af;->c:Landroid/widget/TextView;

    if-nez v0, :cond_aa

    iget-object v0, v1, Lcom/anddoes/launcher/ui/af;->a:Landroid/view/View;

    const v4, 0x7f0d0005

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/anddoes/launcher/ui/af;->c:Landroid/widget/TextView;

    :cond_aa
    iget-object v0, v1, Lcom/anddoes/launcher/ui/af;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->c()Landroid/widget/CheckBox;

    move-result-object v3

    iget-object v0, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->g(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 496
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->c()Landroid/widget/CheckBox;

    move-result-object v0

    new-instance v1, Lcom/anddoes/launcher/ui/ae;

    invoke-direct {v1, p0, p1}, Lcom/anddoes/launcher/ui/ae;-><init>(Lcom/anddoes/launcher/ui/ad;I)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_40

    .line 461
    :cond_d4
    const v0, 0x7f030038

    goto :goto_5d

    .line 465
    :cond_d8
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/ui/af;

    move-object v1, v0

    goto :goto_6a

    .line 477
    :cond_e0
    instance-of v4, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v4, :cond_13f

    .line 478
    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 479
    iget v2, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    if-lez v2, :cond_103

    .line 480
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->a()Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Lcom/android/launcher2/da;

    move-result-object v3

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    iget v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher2/da;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 482
    :cond_103
    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 483
    iget-object v3, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iget v6, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a(Landroid/content/ComponentName;II)[I

    move-result-object v3

    .line 484
    iget-object v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 485
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->b()Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v5}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->e(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    aget v7, v3, v9

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    .line 486
    aget v3, v3, v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v8

    .line 485
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v3, v0

    goto/16 :goto_99

    .line 487
    :cond_13f
    instance-of v4, v0, Landroid/content/pm/ResolveInfo;

    if-eqz v4, :cond_99

    .line 488
    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 489
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->a()Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Lcom/android/launcher2/da;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/launcher2/da;->a(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 490
    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 491
    iget-object v3, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->f(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/af;->b()Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/anddoes/launcher/ui/ad;->a:Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-static {v4}, Lcom/anddoes/launcher/ui/GroupConfigActivity;->e(Lcom/anddoes/launcher/ui/GroupConfigActivity;)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v3, v0

    goto/16 :goto_99
.end method
