.class public Lcom/anddoes/launcher/ui/CircleIndicator;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/hz;


# instance fields
.field private a:F

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/ui/CircleIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    iput v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    .line 28
    iput-boolean v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->j:Z

    .line 29
    iput-boolean v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->k:Z

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    .line 39
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    invoke-static {}, Lcom/anddoes/launcher/v;->d()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    .line 43
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 44
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 45
    invoke-static {}, Lcom/anddoes/launcher/v;->d()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x4020

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    .line 46
    return-void
.end method

.method private a(I)I
    .registers 8
    .parameter

    .prologue
    .line 209
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 211
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 213
    const/high16 v0, 0x4000

    if-eq v2, v0, :cond_10

    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->d:I

    if-nez v0, :cond_12

    :cond_10
    move v0, v1

    .line 226
    :cond_11
    :goto_11
    return v0

    .line 218
    :cond_12
    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->d:I

    .line 219
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    .line 220
    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    iget v5, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    mul-float/2addr v4, v5

    .line 219
    add-float/2addr v3, v4

    .line 220
    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iget v4, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    mul-float/2addr v0, v4

    .line 219
    add-float/2addr v0, v3

    .line 220
    const/high16 v3, 0x3f80

    .line 219
    add-float/2addr v0, v3

    float-to-int v0, v0

    .line 222
    const/high16 v3, -0x8000

    if-ne v2, v3, :cond_11

    .line 223
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_11
.end method

.method private b(I)I
    .registers 6
    .parameter

    .prologue
    .line 237
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 239
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 241
    const/high16 v1, 0x4000

    if-ne v2, v1, :cond_d

    .line 252
    :goto_c
    return v0

    .line 246
    :cond_d
    const/high16 v1, 0x4000

    iget v3, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    mul-float/2addr v1, v3

    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingBottom()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    const/high16 v3, 0x3f80

    add-float/2addr v1, v3

    float-to-int v1, v1

    .line 248
    const/high16 v3, -0x8000

    if-ne v2, v3, :cond_2b

    .line 249
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_c

    :cond_2b
    move v0, v1

    goto :goto_c
.end method


# virtual methods
.method public getFillColor()I
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getOrientation()I
    .registers 2

    .prologue
    .line 80
    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    return v0
.end method

.method public getRadius()F
    .registers 2

    .prologue
    .line 107
    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    return v0
.end method

.method public getStrokeColor()I
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getStrokeWidth()F
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter

    .prologue
    const/high16 v9, 0x4000

    .line 126
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 128
    iget v5, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->d:I

    .line 129
    if-nez v5, :cond_a

    .line 185
    :goto_9
    return-void

    .line 137
    :cond_a
    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    if-nez v0, :cond_6a

    .line 138
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getWidth()I

    move-result v3

    .line 139
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingLeft()I

    move-result v2

    .line 140
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingRight()I

    move-result v1

    .line 141
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingTop()I

    move-result v0

    .line 149
    :goto_1e
    iget v4, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    const/high16 v6, 0x40c0

    mul-float/2addr v6, v4

    .line 150
    int-to-float v0, v0

    iget v4, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    add-float/2addr v4, v0

    .line 151
    int-to-float v0, v2

    iget v7, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    add-float/2addr v0, v7

    .line 152
    iget-boolean v7, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->j:Z

    if-eqz v7, :cond_3a

    .line 153
    sub-int v2, v3, v2

    sub-int v1, v2, v1

    int-to-float v1, v1

    div-float/2addr v1, v9

    int-to-float v2, v5

    mul-float/2addr v2, v6

    div-float/2addr v2, v9

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 160
    :cond_3a
    const/4 v1, 0x0

    move v3, v1

    :goto_3c
    if-lt v3, v5, :cond_7b

    .line 173
    iget-boolean v1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->k:Z

    if-eqz v1, :cond_91

    iget v1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->f:I

    :goto_44
    int-to-float v1, v1

    mul-float/2addr v1, v6

    .line 174
    iget-boolean v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->k:Z

    if-nez v2, :cond_5a

    iget v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->h:I

    if-eqz v2, :cond_5a

    .line 175
    iget v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->g:I

    int-to-float v2, v2

    const/high16 v3, 0x3f80

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->h:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v6

    add-float/2addr v1, v2

    .line 177
    :cond_5a
    iget v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    if-nez v2, :cond_94

    .line 178
    add-float/2addr v0, v1

    .line 184
    :goto_5f
    iget v1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    mul-float/2addr v1, v9

    float-to-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_9

    .line 143
    :cond_6a
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getHeight()I

    move-result v3

    .line 144
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingTop()I

    move-result v2

    .line 145
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingBottom()I

    move-result v1

    .line 146
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->getPaddingLeft()I

    move-result v0

    goto :goto_1e

    .line 161
    :cond_7b
    int-to-float v1, v3

    mul-float/2addr v1, v6

    add-float/2addr v1, v0

    .line 162
    iget v2, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    if-nez v2, :cond_8f

    move v2, v1

    move v1, v4

    .line 167
    :goto_84
    iget v7, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    iget-object v8, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 160
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3c

    :cond_8f
    move v2, v4

    .line 166
    goto :goto_84

    .line 173
    :cond_91
    iget v1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->e:I

    goto :goto_44

    .line 182
    :cond_94
    add-float/2addr v0, v1

    move v10, v0

    move v0, v4

    move v4, v10

    goto :goto_5f
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 194
    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    if-nez v0, :cond_10

    .line 195
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/CircleIndicator;->a(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/anddoes/launcher/ui/CircleIndicator;->b(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/ui/CircleIndicator;->setMeasuredDimension(II)V

    .line 199
    :goto_f
    return-void

    .line 197
    :cond_10
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/CircleIndicator;->b(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/anddoes/launcher/ui/CircleIndicator;->a(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/ui/CircleIndicator;->setMeasuredDimension(II)V

    goto :goto_f
.end method

.method public setCentered(Z)V
    .registers 2
    .parameter

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->j:Z

    .line 50
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 51
    return-void
.end method

.method public setCurrentPage(I)V
    .registers 3
    .parameter

    .prologue
    .line 261
    if-ltz p1, :cond_11

    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->d:I

    if-ge p1, v0, :cond_11

    iget v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->e:I

    if-eq v0, p1, :cond_11

    .line 262
    iput p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->e:I

    .line 263
    iput p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->f:I

    .line 264
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 266
    :cond_11
    return-void
.end method

.method public setFillColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 60
    return-void
.end method

.method public setOrientation(I)V
    .registers 4
    .parameter

    .prologue
    .line 67
    packed-switch p1, :pswitch_data_12

    .line 75
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Orientation must be either HORIZONTAL or VERTICAL."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :pswitch_b
    iput p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->i:I

    .line 71
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->requestLayout()V

    .line 72
    return-void

    .line 67
    nop

    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method public setPageCount(I)V
    .registers 2
    .parameter

    .prologue
    .line 256
    iput p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->d:I

    .line 257
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->requestLayout()V

    .line 258
    return-void
.end method

.method public setRadius(F)V
    .registers 2
    .parameter

    .prologue
    .line 102
    iput p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->a:F

    .line 103
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 104
    return-void
.end method

.method public setSnap(Z)V
    .registers 2
    .parameter

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->k:Z

    .line 112
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 113
    return-void
.end method

.method public setStrokeColor(I)V
    .registers 3
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 85
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 86
    return-void
.end method

.method public setStrokeWidth(F)V
    .registers 3
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/anddoes/launcher/ui/CircleIndicator;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 94
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/CircleIndicator;->invalidate()V

    .line 95
    return-void
.end method
