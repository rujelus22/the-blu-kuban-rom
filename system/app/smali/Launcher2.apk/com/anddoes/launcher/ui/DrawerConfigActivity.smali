.class public Lcom/anddoes/launcher/ui/DrawerConfigActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private a:Lcom/anddoes/launcher/preference/c;

.field private b:Lcom/anddoes/launcher/ui/DragableListView;

.field private c:Landroid/view/LayoutInflater;

.field private d:Lcom/anddoes/launcher/ui/r;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)Lcom/anddoes/launcher/preference/c;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .registers 7

    .prologue
    .line 176
    const-string v1, ""

    .line 177
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 178
    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 179
    array-length v3, v2

    const/4 v0, 0x0

    :goto_10
    if-lt v0, v3, :cond_13

    .line 182
    return-object v1

    .line 179
    :cond_13
    aget-object v4, v2, v0

    .line 180
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ";"

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v5, v4}, Lcom/anddoes/launcher/preference/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_10
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/DrawerConfigActivity;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 190
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07004e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07004f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000e

    new-instance v2, Lcom/anddoes/launcher/ui/o;

    invoke-direct {v2, p0, p1}, Lcom/anddoes/launcher/ui/o;-><init>(Lcom/anddoes/launcher/ui/DrawerConfigActivity;I)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000f

    new-instance v2, Lcom/anddoes/launcher/ui/p;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/ui/p;-><init>(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 130
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 131
    sget-object v2, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a:Ljava/lang/String;

    if-eqz p1, :cond_28

    .line 132
    const v0, 0x7f07004b

    .line 131
    :goto_e
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->g:Ljava/lang/String;

    invoke-direct {p0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 135
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 136
    return-void

    .line 132
    :cond_28
    const v0, 0x7f07004c

    goto :goto_e
.end method

.method static synthetic b(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)Landroid/view/LayoutInflater;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->c:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 187
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    .line 188
    return-void
.end method

.method static synthetic b(Lcom/anddoes/launcher/ui/DrawerConfigActivity;I)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 105
    packed-switch p1, :pswitch_data_10

    move v0, v1

    :goto_6
    return v0

    :pswitch_7
    invoke-direct {p0, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a(Z)V

    goto :goto_6

    :pswitch_b
    invoke-direct {p0, v1}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a(Z)V

    goto :goto_6

    nop

    :pswitch_data_10
    .packed-switch 0x2
        :pswitch_7
        :pswitch_b
    .end packed-switch
.end method

.method static synthetic c(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b()V

    return-void
.end method

.method static synthetic d(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)Lcom/anddoes/launcher/ui/r;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 120
    const/4 v0, -0x1

    if-ne p2, v0, :cond_6a

    .line 121
    if-ne p1, v4, :cond_6b

    .line 122
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->e:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/anddoes/launcher/ui/GroupConfigActivity;->c:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;->h:Ljava/lang/String;

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v5}, Lcom/anddoes/launcher/preference/c;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    const-string v6, "all_groups"

    invoke-virtual {v5, v6, v4}, Lcom/anddoes/launcher/preference/c;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "group_isapp_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/anddoes/launcher/preference/c;->b(Ljava/lang/String;Z)V

    iget-object v3, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v3, v2, v0}, Lcom/anddoes/launcher/preference/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    invoke-virtual {v0, v2}, Lcom/anddoes/launcher/ui/r;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b()V

    .line 127
    :cond_6a
    :goto_6a
    return-void

    .line 123
    :cond_6b
    const/4 v0, 0x2

    if-ne p1, v0, :cond_6a

    .line 124
    sget-object v0, Lcom/anddoes/launcher/ui/GroupConfigActivity;->e:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/anddoes/launcher/ui/GroupConfigActivity;->c:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/anddoes/launcher/ui/MultiPickerActivity;->d:Ljava/lang/String;

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v3, v2, v0}, Lcom/anddoes/launcher/preference/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v3, v2, v1}, Lcom/anddoes/launcher/preference/c;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    invoke-virtual {v1, v2, v0}, Lcom/anddoes/launcher/ui/r;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b()V

    goto :goto_6a
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f03002e

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->setContentView(I)V

    .line 57
    new-instance v0, Lcom/anddoes/launcher/preference/c;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    .line 58
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->c:Landroid/view/LayoutInflater;

    .line 59
    const v0, 0x7f0d005a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/ui/DragableListView;

    iput-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b:Lcom/anddoes/launcher/ui/DragableListView;

    .line 60
    new-instance v0, Lcom/anddoes/launcher/ui/r;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/ui/r;-><init>(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)V

    iput-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    .line 61
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b:Lcom/anddoes/launcher/ui/DragableListView;

    invoke-virtual {v0, p0}, Lcom/anddoes/launcher/ui/DragableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 62
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b:Lcom/anddoes/launcher/ui/DragableListView;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/DragableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b:Lcom/anddoes/launcher/ui/DragableListView;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/DragableListView;->setDragListener(Lcom/anddoes/launcher/ui/l;)V

    .line 64
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->b:Lcom/anddoes/launcher/ui/DragableListView;

    iget-object v1, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/DragableListView;->setDropListener(Lcom/anddoes/launcher/ui/m;)V

    .line 65
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 81
    const/4 v0, 0x1

    const v1, 0x7f07004a

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 82
    const v1, 0x1080033

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 83
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 84
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->d:Lcom/anddoes/launcher/ui/r;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/r;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 76
    iget-object v1, v0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    iget-boolean v0, v0, Lcom/anddoes/launcher/p;->c:Z

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;->a:Ljava/lang/String;

    const v4, 0x7f07004d

    invoke-virtual {p0, v4}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;->g:Ljava/lang/String;

    invoke-direct {p0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v4, v1}, Lcom/anddoes/launcher/preference/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v3, Lcom/anddoes/launcher/ui/GroupConfigActivity;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->a:Lcom/anddoes/launcher/preference/c;

    invoke-virtual {v4, v1}, Lcom/anddoes/launcher/preference/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/anddoes/launcher/ui/GroupConfigActivity;->h:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v0, 0x2

    invoke-virtual {p0, v2, v0}, Lcom/anddoes/launcher/ui/DrawerConfigActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 77
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    .line 89
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_28

    .line 102
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_b
    return v0

    .line 91
    :pswitch_c
    new-instance v0, Lcom/anddoes/launcher/ui/q;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/ui/q;-><init>(Lcom/anddoes/launcher/ui/DrawerConfigActivity;)V

    .line 92
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 93
    new-instance v2, Lcom/anddoes/launcher/ui/n;

    invoke-direct {v2, p0, v0}, Lcom/anddoes/launcher/ui/n;-><init>(Lcom/anddoes/launcher/ui/DrawerConfigActivity;Lcom/anddoes/launcher/ui/q;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 100
    const/4 v0, 0x1

    goto :goto_b

    .line 89
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_c
    .end packed-switch
.end method

.method protected onStart()V
    .registers 3

    .prologue
    .line 69
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 70
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const-string v1, "/DrawerConfig"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    .line 71
    return-void
.end method
