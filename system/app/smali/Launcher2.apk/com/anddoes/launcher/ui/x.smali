.class final Lcom/anddoes/launcher/ui/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# instance fields
.field final synthetic a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

.field private b:Lcom/anddoes/launcher/ui/u;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 651
    iput-object p1, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 651
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/x;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;)V

    return-void
.end method


# virtual methods
.method final a()Landroid/app/Dialog;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 658
    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->a(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_17

    .line 659
    :cond_15
    const/4 v0, 0x0

    .line 673
    :goto_16
    return-object v0

    .line 661
    :cond_17
    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v1, 0x7f070112

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 662
    new-instance v1, Lcom/anddoes/launcher/ui/u;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-direct {v1, v2, v3}, Lcom/anddoes/launcher/ui/u;-><init>(Lcom/anddoes/launcher/ui/EditShortcutActivity;B)V

    iput-object v1, p0, Lcom/anddoes/launcher/ui/x;->b:Lcom/anddoes/launcher/ui/u;

    .line 664
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 665
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 666
    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->b:Lcom/anddoes/launcher/ui/u;

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 667
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 669
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 670
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 671
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 672
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    goto :goto_16
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 678
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 690
    if-ltz p2, :cond_a

    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->b:Lcom/anddoes/launcher/ui/u;

    invoke-virtual {v0}, Lcom/anddoes/launcher/ui/u;->getCount()I

    move-result v0

    if-lt p2, v0, :cond_b

    .line 709
    :cond_a
    :goto_a
    return-void

    .line 693
    :cond_b
    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->b:Lcom/anddoes/launcher/ui/u;

    invoke-virtual {v0, p2}, Lcom/anddoes/launcher/ui/u;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/ui/v;

    .line 694
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 695
    iget-object v2, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->c(Lcom/anddoes/launcher/ui/EditShortcutActivity;)Z

    move-result v2

    if-nez v2, :cond_4c

    const-string v2, "adw_theme"

    iget-object v3, v0, Lcom/anddoes/launcher/ui/v;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 696
    const-string v2, "org.adw.launcher.icons.ACTION_PICK_ICON"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 697
    iget-object v2, v0, Lcom/anddoes/launcher/ui/v;->d:Ljava/lang/String;

    iget-object v0, v0, Lcom/anddoes/launcher/ui/v;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 705
    :goto_36
    :try_start_36
    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/ui/EditShortcutActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_3c} :catch_3d

    goto :goto_a

    .line 707
    :catch_3d
    move-exception v0

    iget-object v0, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const v1, 0x7f070151

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_a

    .line 699
    :cond_4c
    iget-object v2, p0, Lcom/anddoes/launcher/ui/x;->a:Lcom/anddoes/launcher/ui/EditShortcutActivity;

    const-class v3, Lcom/anddoes/launcher/ui/IconPickerActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 700
    const-string v2, "com.anddoes.launcher.THEME_PACKAGE_NAME"

    iget-object v3, v0, Lcom/anddoes/launcher/ui/v;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 701
    const-string v2, "com.anddoes.launcher.THEME_TYPE"

    iget-object v3, v0, Lcom/anddoes/launcher/ui/v;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 702
    const-string v2, "com.anddoes.launcher.THEME_NAME"

    iget-object v0, v0, Lcom/anddoes/launcher/ui/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_36
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 683
    return-void
.end method

.method public final onShow(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 713
    return-void
.end method
