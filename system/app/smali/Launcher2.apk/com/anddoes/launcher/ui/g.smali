.class final Lcom/anddoes/launcher/ui/g;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field a:Landroid/util/SparseBooleanArray;

.field final synthetic b:Lcom/anddoes/launcher/ui/AppPickerActivity;


# direct methods
.method public constructor <init>(Lcom/anddoes/launcher/ui/AppPickerActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 208
    iput-object p1, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 209
    invoke-static {p1}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 210
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1, v0}, Landroid/util/SparseBooleanArray;-><init>(I)V

    iput-object v1, p0, Lcom/anddoes/launcher/ui/g;->a:Landroid/util/SparseBooleanArray;

    .line 211
    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/ui/g;)Lcom/anddoes/launcher/ui/AppPickerActivity;
    .registers 2
    .parameter

    .prologue
    .line 204
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    return-object v0
.end method

.method private a(I)Lcom/android/launcher2/h;
    .registers 3
    .parameter

    .prologue
    .line 218
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 219
    if-ltz p1, :cond_e

    if-lt p1, v0, :cond_10

    .line 220
    :cond_e
    const/4 v0, 0x0

    .line 222
    :goto_f
    return-object v0

    :cond_10
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    goto :goto_f
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/g;->a(I)Lcom/android/launcher2/h;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter

    .prologue
    .line 226
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 227
    if-ltz p1, :cond_e

    if-lt p1, v0, :cond_18

    .line 228
    :cond_e
    const-string v0, "AppPickerActivity"

    const-string v1, "Position out of bounds in List Adapter"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const-wide/16 v0, -0x1

    .line 231
    :goto_17
    return-wide v0

    :cond_18
    int-to-long v0, p1

    goto :goto_17
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 235
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_38

    .line 236
    const-string v0, "AppPickerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid view position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", actual size is: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v2}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    const/4 p2, 0x0

    .line 281
    :cond_37
    :goto_37
    return-object p2

    .line 240
    :cond_38
    if-nez p2, :cond_d0

    .line 242
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->b(Lcom/anddoes/launcher/ui/AppPickerActivity;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030006

    invoke-virtual {v0, v1, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 243
    new-instance v0, Lcom/anddoes/launcher/ui/i;

    invoke-direct {v0, p0, p2}, Lcom/anddoes/launcher/ui/i;-><init>(Lcom/anddoes/launcher/ui/g;Landroid/view/View;)V

    .line 244
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 249
    :goto_50
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/ui/g;->a(I)Lcom/android/launcher2/h;

    move-result-object v2

    .line 250
    if-eqz v2, :cond_37

    .line 251
    iget-object v0, v1, Lcom/anddoes/launcher/ui/i;->b:Landroid/widget/ImageView;

    if-nez v0, :cond_67

    iget-object v0, v1, Lcom/anddoes/launcher/ui/i;->a:Landroid/view/View;

    const v3, 0x7f0d0004

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/anddoes/launcher/ui/i;->b:Landroid/widget/ImageView;

    :cond_67
    iget-object v0, v1, Lcom/anddoes/launcher/ui/i;->b:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/AppPickerActivity;->c(Lcom/anddoes/launcher/ui/AppPickerActivity;)Lcom/android/launcher2/da;

    move-result-object v3

    iget-object v4, v2, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v3, v4}, Lcom/android/launcher2/da;->a(Landroid/content/Intent;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 252
    iget-object v0, v1, Lcom/anddoes/launcher/ui/i;->c:Landroid/widget/TextView;

    if-nez v0, :cond_89

    iget-object v0, v1, Lcom/anddoes/launcher/ui/i;->a:Landroid/view/View;

    const v3, 0x7f0d0005

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/anddoes/launcher/ui/i;->c:Landroid/widget/TextView;

    :cond_89
    iget-object v0, v1, Lcom/anddoes/launcher/ui/i;->c:Landroid/widget/TextView;

    iget-object v3, v2, Lcom/android/launcher2/h;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->d(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d9

    .line 254
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->d(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d9

    .line 255
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->a:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 256
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/anddoes/launcher/ui/AppPickerActivity;->a(Lcom/anddoes/launcher/ui/AppPickerActivity;Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/anddoes/launcher/ui/AppPickerActivity;->b(Lcom/anddoes/launcher/ui/AppPickerActivity;Ljava/lang/String;)V

    .line 258
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/i;->a()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 274
    :goto_c2
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/i;->a()Landroid/widget/RadioButton;

    move-result-object v0

    new-instance v1, Lcom/anddoes/launcher/ui/h;

    invoke-direct {v1, p0, p1}, Lcom/anddoes/launcher/ui/h;-><init>(Lcom/anddoes/launcher/ui/g;I)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_37

    .line 246
    :cond_d0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/ui/i;

    move-object v1, v0

    goto/16 :goto_50

    .line 259
    :cond_d9
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->e(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13a

    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/ui/AppPickerActivity;->f(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_13a

    .line 260
    iget-object v0, v2, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/AppPickerActivity;->e(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13a

    .line 261
    iget-object v0, v2, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-static {v3}, Lcom/anddoes/launcher/ui/AppPickerActivity;->f(Lcom/anddoes/launcher/ui/AppPickerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13a

    .line 262
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-virtual {v2}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/anddoes/launcher/ui/AppPickerActivity;->c(Lcom/anddoes/launcher/ui/AppPickerActivity;Ljava/lang/String;)V

    .line 263
    iget-object v0, v2, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    if-eqz v0, :cond_132

    .line 264
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    iget-object v2, v2, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->toURI()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/anddoes/launcher/ui/AppPickerActivity;->d(Lcom/anddoes/launcher/ui/AppPickerActivity;Ljava/lang/String;)V

    .line 268
    :goto_125
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->a:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 269
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/i;->a()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_c2

    .line 266
    :cond_132
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->b:Lcom/anddoes/launcher/ui/AppPickerActivity;

    const-string v2, ""

    invoke-static {v0, v2}, Lcom/anddoes/launcher/ui/AppPickerActivity;->d(Lcom/anddoes/launcher/ui/AppPickerActivity;Ljava/lang/String;)V

    goto :goto_125

    .line 271
    :cond_13a
    iget-object v0, p0, Lcom/anddoes/launcher/ui/g;->a:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 272
    invoke-virtual {v1}, Lcom/anddoes/launcher/ui/i;->a()Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_c2
.end method
