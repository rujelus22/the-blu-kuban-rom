.class public final Lcom/anddoes/launcher/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final AppsCustomizePagedView:[I = null

.field public static final AppsCustomizePagedView_maxAppCellCountX:I = 0x0

.field public static final AppsCustomizePagedView_maxAppCellCountY:I = 0x1

.field public static final AppsCustomizePagedView_widgetCellHeightGap:I = 0x3

.field public static final AppsCustomizePagedView_widgetCellWidthGap:I = 0x2

.field public static final AppsCustomizePagedView_widgetCountX:I = 0x4

.field public static final AppsCustomizePagedView_widgetCountY:I = 0x5

.field public static final CellLayout:[I = null

.field public static final CellLayout_cellHeight:I = 0x1

.field public static final CellLayout_cellWidth:I = 0x0

.field public static final CellLayout_heightGap:I = 0x3

.field public static final CellLayout_maxGap:I = 0x4

.field public static final CellLayout_widthGap:I = 0x2

.field public static final DrawableStateProxyView:[I = null

.field public static final DrawableStateProxyView_sourceViewId:I = 0x0

.field public static final Extra:[I = null

.field public static final Extra_key:I = 0x0

.field public static final Extra_value:I = 0x1

.field public static final Favorite:[I = null

.field public static final Favorite_className:I = 0x0

.field public static final Favorite_container:I = 0x2

.field public static final Favorite_icon:I = 0x8

.field public static final Favorite_packageName:I = 0x1

.field public static final Favorite_screen:I = 0x3

.field public static final Favorite_spanX:I = 0x6

.field public static final Favorite_spanY:I = 0x7

.field public static final Favorite_title:I = 0x9

.field public static final Favorite_uri:I = 0xa

.field public static final Favorite_x:I = 0x4

.field public static final Favorite_y:I = 0x5

.field public static final HandleView:[I = null

.field public static final HandleView_direction:I = 0x0

.field public static final HolographicLinearLayout:[I = null

.field public static final HolographicLinearLayout_sourceImageViewId:I = 0x0

.field public static final Hotseat:[I = null

.field public static final Hotseat_cellCountX:I = 0x0

.field public static final Hotseat_cellCountY:I = 0x1

.field public static final IconListPreference:[I = null

.field public static final IconListPreference_entryIcons:I = 0x0

.field public static final PagedView:[I = null

.field public static final PagedView_pageLayoutHeightGap:I = 0x1

.field public static final PagedView_pageLayoutPaddingBottom:I = 0x3

.field public static final PagedView_pageLayoutPaddingLeft:I = 0x4

.field public static final PagedView_pageLayoutPaddingRight:I = 0x5

.field public static final PagedView_pageLayoutPaddingTop:I = 0x2

.field public static final PagedView_pageLayoutWidthGap:I = 0x0

.field public static final PagedView_pageSpacing:I = 0x6

.field public static final PagedView_scrollIndicatorPaddingLeft:I = 0x7

.field public static final PagedView_scrollIndicatorPaddingRight:I = 0x8

.field public static final StrokedTextView:[I = null

.field public static final StrokedTextView_strokeColor:I = 0x0

.field public static final StrokedTextView_strokeTextColor:I = 0x1

.field public static final StrokedTextView_strokeWidth:I = 0x2

.field public static final Workspace:[I = null

.field public static final Workspace_cellCountX:I = 0x1

.field public static final Workspace_cellCountY:I = 0x2

.field public static final Workspace_defaultScreen:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2495
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_66

    sput-object v0, Lcom/anddoes/launcher/at;->AppsCustomizePagedView:[I

    .line 2620
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_76

    sput-object v0, Lcom/anddoes/launcher/at;->CellLayout:[I

    .line 2725
    new-array v0, v3, [I

    .line 2726
    const v1, 0x7f010002

    aput v1, v0, v2

    .line 2725
    sput-object v0, Lcom/anddoes/launcher/at;->DrawableStateProxyView:[I

    .line 2756
    new-array v0, v4, [I

    fill-array-data v0, :array_84

    sput-object v0, Lcom/anddoes/launcher/at;->Extra:[I

    .line 2817
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_8c

    sput-object v0, Lcom/anddoes/launcher/at;->Favorite:[I

    .line 2979
    new-array v0, v3, [I

    .line 2980
    const v1, 0x7f010001

    aput v1, v0, v2

    .line 2979
    sput-object v0, Lcom/anddoes/launcher/at;->HandleView:[I

    .line 3011
    new-array v0, v3, [I

    .line 3012
    const v1, 0x7f01000e

    aput v1, v0, v2

    .line 3011
    sput-object v0, Lcom/anddoes/launcher/at;->HolographicLinearLayout:[I

    .line 3043
    new-array v0, v4, [I

    fill-array-data v0, :array_a6

    sput-object v0, Lcom/anddoes/launcher/at;->Hotseat:[I

    .line 3088
    new-array v0, v3, [I

    .line 3089
    const/high16 v1, 0x7f01

    aput v1, v0, v2

    .line 3088
    sput-object v0, Lcom/anddoes/launcher/at;->IconListPreference:[I

    .line 3128
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_ae

    sput-object v0, Lcom/anddoes/launcher/at;->PagedView:[I

    .line 3299
    new-array v0, v5, [I

    fill-array-data v0, :array_c4

    sput-object v0, Lcom/anddoes/launcher/at;->StrokedTextView:[I

    .line 3367
    new-array v0, v5, [I

    fill-array-data v0, :array_ce

    sput-object v0, Lcom/anddoes/launcher/at;->Workspace:[I

    .line 2473
    return-void

    .line 2495
    :array_66
    .array-data 0x4
        0x18t 0x0t 0x1t 0x7ft
        0x19t 0x0t 0x1t 0x7ft
        0x1at 0x0t 0x1t 0x7ft
        0x1bt 0x0t 0x1t 0x7ft
        0x1ct 0x0t 0x1t 0x7ft
        0x1dt 0x0t 0x1t 0x7ft
    .end array-data

    .line 2620
    :array_76
    .array-data 0x4
        0x6t 0x0t 0x1t 0x7ft
        0x7t 0x0t 0x1t 0x7ft
        0x8t 0x0t 0x1t 0x7ft
        0x9t 0x0t 0x1t 0x7ft
        0xat 0x0t 0x1t 0x7ft
    .end array-data

    .line 2756
    :array_84
    .array-data 0x4
        0x29t 0x0t 0x1t 0x7ft
        0x2at 0x0t 0x1t 0x7ft
    .end array-data

    .line 2817
    :array_8c
    .array-data 0x4
        0x1et 0x0t 0x1t 0x7ft
        0x1ft 0x0t 0x1t 0x7ft
        0x20t 0x0t 0x1t 0x7ft
        0x21t 0x0t 0x1t 0x7ft
        0x22t 0x0t 0x1t 0x7ft
        0x23t 0x0t 0x1t 0x7ft
        0x24t 0x0t 0x1t 0x7ft
        0x25t 0x0t 0x1t 0x7ft
        0x26t 0x0t 0x1t 0x7ft
        0x27t 0x0t 0x1t 0x7ft
        0x28t 0x0t 0x1t 0x7ft
    .end array-data

    .line 3043
    :array_a6
    .array-data 0x4
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
    .end array-data

    .line 3128
    :array_ae
    .array-data 0x4
        0xft 0x0t 0x1t 0x7ft
        0x10t 0x0t 0x1t 0x7ft
        0x11t 0x0t 0x1t 0x7ft
        0x12t 0x0t 0x1t 0x7ft
        0x13t 0x0t 0x1t 0x7ft
        0x14t 0x0t 0x1t 0x7ft
        0x15t 0x0t 0x1t 0x7ft
        0x16t 0x0t 0x1t 0x7ft
        0x17t 0x0t 0x1t 0x7ft
    .end array-data

    .line 3299
    :array_c4
    .array-data 0x4
        0xbt 0x0t 0x1t 0x7ft
        0xct 0x0t 0x1t 0x7ft
        0xdt 0x0t 0x1t 0x7ft
    .end array-data

    .line 3367
    :array_ce
    .array-data 0x4
        0x3t 0x0t 0x1t 0x7ft
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
    .end array-data
.end method
