.class public final Lcom/anddoes/launcher/preference/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field public A:Ljava/lang/String;

.field public B:I

.field public C:Ljava/lang/String;

.field public D:Z

.field public E:Z

.field public F:I

.field public G:Ljava/lang/String;

.field public H:Ljava/lang/String;

.field public I:Z

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Ljava/lang/String;

.field public N:Ljava/lang/String;

.field public O:Z

.field public P:Ljava/lang/String;

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:I

.field public U:I

.field public V:Ljava/lang/String;

.field public W:Ljava/lang/String;

.field public X:Z

.field public Y:Ljava/lang/String;

.field public Z:Z

.field public a:Lcom/anddoes/launcher/preference/h;

.field public aA:Ljava/lang/String;

.field public aB:Ljava/lang/String;

.field public aC:Ljava/lang/String;

.field public aD:Ljava/lang/String;

.field public aE:Z

.field public aF:Z

.field public aG:Z

.field public aH:Z

.field public aI:Z

.field public aJ:Z

.field public aK:Z

.field public aL:Z

.field public aM:Z

.field public aN:Z

.field public aO:Z

.field public aP:Z

.field public aQ:Z

.field public aR:Ljava/lang/String;

.field public aS:Z

.field public aT:Ljava/lang/String;

.field public aU:Z

.field public aV:Ljava/lang/String;

.field public aW:Z

.field public aX:Z

.field public aY:Z

.field public aZ:Z

.field public aa:Z

.field public ab:I

.field public ac:Z

.field public ad:Ljava/lang/String;

.field public ae:Z

.field public af:Z

.field public ag:Ljava/lang/String;

.field public ah:Ljava/lang/String;

.field public ai:Z

.field public aj:Z

.field public ak:Z

.field public al:Ljava/lang/String;

.field public am:Ljava/lang/String;

.field public an:Z

.field public ao:Z

.field public ap:Ljava/lang/String;

.field public aq:Ljava/lang/String;

.field public ar:Ljava/lang/String;

.field public as:Ljava/lang/String;

.field public at:Ljava/lang/String;

.field public au:Ljava/lang/String;

.field public av:Ljava/lang/String;

.field public aw:Ljava/lang/String;

.field public ax:Z

.field public ay:Z

.field public az:Z

.field public b:Z

.field public ba:Ljava/lang/String;

.field public bb:Z

.field public bc:Z

.field public bd:Ljava/lang/String;

.field public be:J

.field private bf:Lcom/android/launcher2/Launcher;

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:I

.field public l:Ljava/lang/String;

.field public m:Z

.field public n:Ljava/lang/String;

.field public o:Z

.field public p:Ljava/lang/String;

.field public q:Z

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Z

.field public u:Z

.field public v:Z

.field public w:Ljava/lang/String;

.field public x:I

.field public y:I

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/android/launcher2/Launcher;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->m:Z

    .line 28
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->o:Z

    .line 30
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->q:Z

    .line 33
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->t:Z

    .line 34
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->u:Z

    .line 35
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->v:Z

    .line 55
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->O:Z

    .line 57
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->Q:Z

    .line 58
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->R:Z

    .line 59
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->S:Z

    .line 65
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->X:Z

    .line 67
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->Z:Z

    .line 68
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aa:Z

    .line 70
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->ac:Z

    .line 72
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ae:Z

    .line 73
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->af:Z

    .line 77
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->ai:Z

    .line 78
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aj:Z

    .line 79
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ak:Z

    .line 97
    const-string v0, "apex_theme"

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aA:Ljava/lang/String;

    .line 132
    iput-object p1, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    .line 133
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 136
    new-instance v0, Lcom/anddoes/launcher/preference/h;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-direct {v0, v2}, Lcom/anddoes/launcher/preference/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    .line 137
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v2, "desktop_locked"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->b:Z

    .line 139
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_391

    .line 140
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->g()I

    move-result v0

    .line 139
    :goto_20
    iput v0, p0, Lcom/anddoes/launcher/preference/f;->c:I

    .line 142
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_399

    .line 143
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->h()I

    move-result v0

    .line 142
    :goto_2e
    iput v0, p0, Lcom/anddoes/launcher/preference/f;->d:I

    .line 145
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->e:Ljava/lang/String;

    .line 146
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->f:Ljava/lang/String;

    .line 147
    const-string v0, "NONE"

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->g:Ljava/lang/String;

    .line 148
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->h:Ljava/lang/String;

    .line 149
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->i:Z

    .line 150
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->m()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->j:Z

    .line 151
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->n()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->k:I

    .line 152
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->l:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->p()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->m:Z

    .line 154
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->n:Ljava/lang/String;

    .line 155
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->o:Z

    .line 156
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    .line 157
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->q:Z

    .line 158
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->r:Ljava/lang/String;

    .line 159
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->w()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->s:Ljava/lang/String;

    .line 160
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->x()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->t:Z

    .line 161
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->y()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->u:Z

    .line 162
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->z()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->v:Z

    .line 164
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->w:Ljava/lang/String;

    .line 165
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-boolean v0, v0, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v0, :cond_3a1

    .line 166
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->F()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->x:I

    .line 167
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->G()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->y:I

    .line 173
    :goto_da
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->J()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->z:Ljava/lang/String;

    .line 174
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->L()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->B:I

    .line 176
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->M()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    .line 177
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->N()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->D:Z

    .line 178
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->O()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->E:Z

    .line 179
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->Q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->G:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->P()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->F:I

    .line 181
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->R()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    .line 182
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->S()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->I:Z

    .line 183
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->T()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->J:Z

    .line 184
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->U()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->K:Z

    .line 185
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->V()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->L:Z

    .line 186
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->W()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->M:Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->X()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->N:Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->Y()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->O:Z

    .line 189
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->Z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->P:Ljava/lang/String;

    .line 190
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aa()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->Q:Z

    .line 191
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ab()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->R:Z

    .line 192
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ac()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->S:Z

    .line 194
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ad()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->T:I

    .line 195
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ae()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->U:I

    .line 196
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->af()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->V:Ljava/lang/String;

    .line 197
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->W:Ljava/lang/String;

    .line 198
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ah()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->X:Z

    .line 199
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ai()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->Y:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aj()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->Z:Z

    .line 201
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ak()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aa:Z

    .line 202
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->al()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->ab:I

    .line 203
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->am()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ac:Z

    .line 204
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->an()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ao()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ae:Z

    .line 206
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ae:Z

    if-nez v0, :cond_3b3

    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ap()Z

    move-result v0

    if-nez v0, :cond_3b3

    move v0, v1

    :goto_1e7
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->af:Z

    .line 208
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aq()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ag:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ar()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    .line 210
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->as()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ai:Z

    .line 211
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->at()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aj:Z

    .line 212
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->au()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ak:Z

    .line 214
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->av()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->al:Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aw()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->am:Ljava/lang/String;

    .line 216
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ax()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->an:Z

    .line 217
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ay()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ao:Z

    .line 218
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->az()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aA()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aq:Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aB()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ar:Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->as:Ljava/lang/String;

    .line 222
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aD()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->at:Ljava/lang/String;

    .line 223
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aE()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->au:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->av:Ljava/lang/String;

    .line 225
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aG()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aw:Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aH()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ax:Z

    .line 227
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aI()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ay:Z

    .line 228
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aJ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->az:Z

    .line 230
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v1, "theme_icon_type"

    const-string v2, "apex_theme"

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aA:Ljava/lang/String;

    .line 231
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aK()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aB:Ljava/lang/String;

    .line 232
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aC:Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aM()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aD:Ljava/lang/String;

    .line 235
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aN()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aE:Z

    .line 236
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aO()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aF:Z

    .line 237
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aP()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aG:Z

    .line 238
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aX()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aO:Z

    .line 239
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aS()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aJ:Z

    .line 240
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aT()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aK:Z

    .line 241
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aU()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aL:Z

    .line 242
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aQ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aH:Z

    .line 243
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aR()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aI:Z

    .line 244
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aV()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aM:Z

    .line 245
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aW()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aN:Z

    .line 246
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aY()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aP:Z

    .line 247
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aZ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aQ:Z

    .line 248
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ba()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aR:Ljava/lang/String;

    .line 249
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bb()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aS:Z

    .line 250
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aT:Ljava/lang/String;

    .line 251
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bd()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aU:Z

    .line 252
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->be()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aV:Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bf()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aW:Z

    .line 254
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bg()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aX:Z

    .line 255
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bh()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aY:Z

    .line 256
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bi()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aZ:Z

    .line 257
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bj()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ba:Ljava/lang/String;

    .line 258
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bl()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->bb:Z

    .line 259
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->bc:Z

    .line 261
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->bd:Ljava/lang/String;

    .line 262
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    const-string v1, "last_check_update"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/preference/h;->e(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/anddoes/launcher/preference/f;->be:J

    .line 264
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 265
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 266
    return-void

    .line 141
    :cond_391
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->d()I

    move-result v0

    goto/16 :goto_20

    .line 144
    :cond_399
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->e()I

    move-result v0

    goto/16 :goto_2e

    .line 169
    :cond_3a1
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->C()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->x:I

    .line 170
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->D()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->y:I

    goto/16 :goto_da

    .line 206
    :cond_3b3
    const/4 v0, 0x1

    goto/16 :goto_1e7
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 270
    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 271
    return-void
.end method

.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 276
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701e7

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 277
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->J()V

    .line 471
    :cond_1a
    :goto_1a
    return-void

    .line 280
    :cond_1b
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f0

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_47

    .line 281
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2}, Lcom/anddoes/launcher/preference/h;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/anddoes/launcher/preference/f;->h:Ljava/lang/String;

    .line 282
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v2

    iget-object v3, p0, Lcom/anddoes/launcher/preference/f;->h:Ljava/lang/String;

    const-string v4, "NONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_45

    :goto_42
    iput-boolean v0, v2, Lcom/android/launcher2/Workspace;->aq:Z

    goto :goto_1a

    :cond_45
    move v0, v1

    goto :goto_42

    .line 283
    :cond_47
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f1

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6a

    .line 284
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->i:Z

    .line 285
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->i:Z

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setElasticScrolling(Z)V

    goto :goto_1a

    .line 286
    :cond_6a
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f2

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_82

    .line 287
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->m()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->j:Z

    goto :goto_1a

    .line 288
    :cond_82
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a5

    .line 289
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->n()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->k:I

    .line 290
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget v1, p0, Lcom/anddoes/launcher/preference/f;->k:I

    iput v1, v0, Lcom/android/launcher2/Workspace;->at:I

    goto/16 :goto_1a

    .line 291
    :cond_a5
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f4

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c8

    .line 292
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->l:Ljava/lang/String;

    .line 293
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget-object v1, p0, Lcom/anddoes/launcher/preference/f;->l:Ljava/lang/String;

    iput-object v1, v0, Lcom/android/launcher2/Workspace;->au:Ljava/lang/String;

    goto/16 :goto_1a

    .line 294
    :cond_c8
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f5

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e6

    .line 295
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->p()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->m:Z

    .line 296
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->D()V

    goto/16 :goto_1a

    .line 297
    :cond_e6
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f6

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_108

    .line 298
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->n:Ljava/lang/String;

    .line 299
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->m()V

    goto/16 :goto_1a

    .line 300
    :cond_108
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f7

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_135

    .line 301
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f8

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_135

    .line 302
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701f9

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_154

    .line 303
    :cond_135
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->r()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->o:Z

    .line 304
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->p:Ljava/lang/String;

    .line 305
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->t()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->q:Z

    .line 306
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->y()V

    goto/16 :goto_1a

    .line 307
    :cond_154
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701fc

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16d

    .line 308
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->v()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->r:Ljava/lang/String;

    goto/16 :goto_1a

    .line 309
    :cond_16d
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f0701ff

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_186

    .line 310
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->x()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->t:Z

    goto/16 :goto_1a

    .line 311
    :cond_186
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070200

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a4

    .line 312
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->y()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->u:Z

    .line 313
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->B()V

    goto/16 :goto_1a

    .line 314
    :cond_1a4
    const-string v2, "drawer_hidden_apps"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1bd

    .line 315
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->A()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->w:Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->l()V

    goto/16 :goto_1a

    .line 317
    :cond_1bd
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07020e

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1db

    .line 318
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->L()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->B:I

    .line 319
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->E()V

    goto/16 :goto_1a

    .line 320
    :cond_1db
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07020f

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f4

    .line 321
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->M()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->C:Ljava/lang/String;

    goto/16 :goto_1a

    .line 322
    :cond_1f4
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070210

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_215

    .line 323
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->N()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->D:Z

    .line 324
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->D:Z

    iput-boolean v1, v0, Lcom/android/launcher2/AppsCustomizePagedView;->aq:Z

    goto/16 :goto_1a

    .line 325
    :cond_215
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070211

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_237

    .line 326
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->O()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->E:Z

    .line 327
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->E:Z

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->setElasticScrolling(Z)V

    goto/16 :goto_1a

    .line 328
    :cond_237
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070212

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_258

    .line 329
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->P()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->F:I

    .line 330
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    iget v1, p0, Lcom/anddoes/launcher/preference/f;->F:I

    iput v1, v0, Lcom/android/launcher2/AppsCustomizePagedView;->at:I

    goto/16 :goto_1a

    .line 331
    :cond_258
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070213

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_278

    .line 332
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->Q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->G:Ljava/lang/String;

    .line 333
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->c:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->m()V

    goto/16 :goto_1a

    .line 334
    :cond_278
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070215

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_28f

    .line 335
    const-string v2, "drawer_tab_icon_act"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_29e

    .line 336
    :cond_28f
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->R()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->H:Ljava/lang/String;

    .line 337
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->H()V

    goto/16 :goto_1a

    .line 338
    :cond_29e
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07021c

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b7

    .line 339
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->W()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->M:Ljava/lang/String;

    goto/16 :goto_1a

    .line 340
    :cond_2b7
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07021d

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d0

    .line 341
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->X()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->N:Ljava/lang/String;

    goto/16 :goto_1a

    .line 342
    :cond_2d0
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07021e

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2fd

    .line 343
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07021f

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2fd

    .line 344
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070220

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31c

    .line 345
    :cond_2fd
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->Y()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->O:Z

    .line 346
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->Z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->P:Ljava/lang/String;

    .line 347
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aa()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->Q:Z

    .line 348
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->z()V

    goto/16 :goto_1a

    .line 349
    :cond_31c
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070227

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_335

    .line 350
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ah()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->X:Z

    goto/16 :goto_1a

    .line 351
    :cond_335
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f070229

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_360

    .line 352
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aj()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->Z:Z

    .line 353
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 354
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->Z:Z

    iput-boolean v1, v0, Lcom/android/launcher2/Hotseat;->aq:Z

    goto/16 :goto_1a

    .line 356
    :cond_360
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07022a

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_38c

    .line 357
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ak()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aa:Z

    .line 358
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 359
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->aa:Z

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Hotseat;->setElasticScrolling(Z)V

    goto/16 :goto_1a

    .line 361
    :cond_38c
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07022b

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3b7

    .line 362
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->al()I

    move-result v0

    iput v0, p0, Lcom/anddoes/launcher/preference/f;->ab:I

    .line 363
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 364
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    iget v1, p0, Lcom/anddoes/launcher/preference/f;->ab:I

    iput v1, v0, Lcom/android/launcher2/Hotseat;->at:I

    goto/16 :goto_1a

    .line 366
    :cond_3b7
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07022c

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3d5

    .line 367
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07022d

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3f8

    .line 368
    :cond_3d5
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->am()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ac:Z

    .line 369
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->an()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ad:Ljava/lang/String;

    .line 370
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    if-eqz v0, :cond_1a

    .line 371
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Hotseat;->e()V

    goto/16 :goto_1a

    .line 373
    :cond_3f8
    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v3, 0x7f07022f

    invoke-virtual {v2, v3}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_41e

    .line 374
    iget-boolean v2, p0, Lcom/anddoes/launcher/preference/f;->ae:Z

    if-nez v2, :cond_41c

    iget-object v2, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2}, Lcom/anddoes/launcher/preference/h;->ap()Z

    move-result v2

    if-nez v2, :cond_41c

    :goto_413
    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->af:Z

    .line 375
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->C()V

    goto/16 :goto_1a

    :cond_41c
    move v0, v1

    .line 374
    goto :goto_413

    .line 376
    :cond_41e
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070233

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_437

    .line 377
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->as()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ai:Z

    goto/16 :goto_1a

    .line 378
    :cond_437
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070237

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_455

    .line 379
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->av()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->al:Ljava/lang/String;

    .line 380
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->A()V

    goto/16 :goto_1a

    .line 381
    :cond_455
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070238

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_46e

    .line 382
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aw()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->am:Ljava/lang/String;

    goto/16 :goto_1a

    .line 383
    :cond_46e
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070239

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_487

    .line 384
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ax()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->an:Z

    goto/16 :goto_1a

    .line 385
    :cond_487
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07023a

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a0

    .line 386
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ay()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ao:Z

    goto/16 :goto_1a

    .line 387
    :cond_4a0
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07023b

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4b9

    .line 388
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->az()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ap:Ljava/lang/String;

    goto/16 :goto_1a

    .line 389
    :cond_4b9
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07023d

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d2

    .line 390
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aA()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aq:Ljava/lang/String;

    goto/16 :goto_1a

    .line 391
    :cond_4d2
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07023e

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4eb

    .line 392
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aB()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->ar:Ljava/lang/String;

    goto/16 :goto_1a

    .line 393
    :cond_4eb
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07023f

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_504

    .line 394
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aC()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->as:Ljava/lang/String;

    goto/16 :goto_1a

    .line 395
    :cond_504
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070240

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_51d

    .line 396
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aD()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->at:Ljava/lang/String;

    goto/16 :goto_1a

    .line 397
    :cond_51d
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070241

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_536

    .line 398
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aE()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->au:Ljava/lang/String;

    goto/16 :goto_1a

    .line 399
    :cond_536
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070242

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_54f

    .line 400
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aF()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->av:Ljava/lang/String;

    goto/16 :goto_1a

    .line 401
    :cond_54f
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070243

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_568

    .line 402
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aG()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aw:Ljava/lang/String;

    goto/16 :goto_1a

    .line 403
    :cond_568
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070244

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_581

    .line 404
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aH()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ax:Z

    goto/16 :goto_1a

    .line 405
    :cond_581
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070245

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_59a

    .line 406
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aI()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->ay:Z

    goto/16 :goto_1a

    .line 407
    :cond_59a
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070246

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5b3

    .line 408
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aJ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->az:Z

    goto/16 :goto_1a

    .line 409
    :cond_5b3
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070249

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5cc

    .line 410
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aN()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aE:Z

    goto/16 :goto_1a

    .line 411
    :cond_5cc
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07024a

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5e5

    .line 412
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aO()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aF:Z

    goto/16 :goto_1a

    .line 413
    :cond_5e5
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07024b

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5fe

    .line 414
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aP()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aG:Z

    goto/16 :goto_1a

    .line 415
    :cond_5fe
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070253

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_617

    .line 416
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aX()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aO:Z

    goto/16 :goto_1a

    .line 417
    :cond_617
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07024e

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_630

    .line 418
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aS()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aJ:Z

    goto/16 :goto_1a

    .line 419
    :cond_630
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07024f

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_649

    .line 420
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aT()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aK:Z

    goto/16 :goto_1a

    .line 421
    :cond_649
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070250

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_662

    .line 422
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aU()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aL:Z

    goto/16 :goto_1a

    .line 423
    :cond_662
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07024c

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_67b

    .line 424
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aQ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aH:Z

    goto/16 :goto_1a

    .line 425
    :cond_67b
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07024d

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_694

    .line 426
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aR()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aI:Z

    goto/16 :goto_1a

    .line 427
    :cond_694
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070251

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6ad

    .line 428
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aV()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aM:Z

    goto/16 :goto_1a

    .line 429
    :cond_6ad
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070252

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6c6

    .line 430
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aW()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aN:Z

    goto/16 :goto_1a

    .line 431
    :cond_6c6
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070254

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6df

    .line 432
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aY()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aP:Z

    goto/16 :goto_1a

    .line 433
    :cond_6df
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070256

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_708

    .line 434
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aZ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aQ:Z

    .line 435
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->aQ:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->a(Z)V

    .line 436
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 437
    :cond_708
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070257

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_728

    .line 438
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ba()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aR:Ljava/lang/String;

    .line 439
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 440
    :cond_728
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070258

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_751

    .line 441
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bb()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aS:Z

    .line 442
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->aS:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->b(Z)V

    .line 443
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 444
    :cond_751
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070259

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_771

    .line 445
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aT:Ljava/lang/String;

    .line 446
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 447
    :cond_771
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07025a

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_79a

    .line 448
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bd()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aU:Z

    .line 449
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->aU:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->c(Z)V

    .line 450
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 451
    :cond_79a
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07025b

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7ba

    .line 452
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->be()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->aV:Ljava/lang/String;

    .line 453
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 454
    :cond_7ba
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07025c

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7e3

    .line 455
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bf()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aW:Z

    .line 456
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/f;->aW:Z

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/w;->d(Z)V

    .line 457
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->r:Lcom/anddoes/launcher/w;

    invoke-virtual {v0}, Lcom/anddoes/launcher/w;->b()V

    goto/16 :goto_1a

    .line 458
    :cond_7e3
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07025e

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7fc

    .line 459
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bg()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aX:Z

    goto/16 :goto_1a

    .line 460
    :cond_7fc
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07025f

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_815

    .line 461
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bh()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aY:Z

    goto/16 :goto_1a

    .line 462
    :cond_815
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070260

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_82e

    .line 463
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bi()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->aZ:Z

    goto/16 :goto_1a

    .line 464
    :cond_82e
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070264

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_847

    .line 465
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bl()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->bb:Z

    goto/16 :goto_1a

    .line 466
    :cond_847
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f070265

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_860

    .line 467
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/f;->bc:Z

    goto/16 :goto_1a

    .line 468
    :cond_860
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->bf:Lcom/android/launcher2/Launcher;

    const v1, 0x7f07026c

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 469
    iget-object v0, p0, Lcom/anddoes/launcher/preference/f;->a:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/anddoes/launcher/preference/f;->bd:Ljava/lang/String;

    goto/16 :goto_1a
.end method
