.class public Lcom/anddoes/launcher/preference/PreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Z

.field private e:Lcom/anddoes/launcher/preference/h;

.field private f:Lcom/anddoes/launcher/preference/b;

.field private g:Z

.field private h:Lcom/android/launcher2/LauncherApplication;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->b:Ljava/lang/String;

    .line 88
    iput v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->c:I

    .line 89
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->d:Z

    .line 91
    iput-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->f:Lcom/anddoes/launcher/preference/b;

    .line 92
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->g:Z

    .line 94
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    .line 99
    iput-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->l:Ljava/lang/String;

    .line 100
    iput-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->m:Ljava/lang/String;

    .line 71
    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/preference/PreferencesActivity;)Lcom/anddoes/launcher/preference/h;
    .registers 2
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    .line 1265
    const v0, 0x7f070010

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1267
    const-string v1, "0 x"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 1268
    const-string v1, "0 x"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 1270
    :cond_28
    const-string v1, "x 0"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_45

    .line 1271
    const-string v1, "x 0"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "x "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 1273
    :cond_45
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f070014

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 243
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 244
    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 245
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 246
    invoke-virtual {v0, p2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 247
    const v1, 0x7f07000e

    .line 248
    new-instance v2, Lcom/anddoes/launcher/preference/i;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/i;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    .line 247
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 253
    const v1, 0x7f07000f

    .line 254
    new-instance v2, Lcom/anddoes/launcher/preference/aa;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/aa;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    .line 253
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 259
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .registers 7
    .parameter

    .prologue
    .line 292
    if-nez p1, :cond_3

    .line 317
    :goto_2
    return-void

    .line 295
    :cond_3
    const/4 v0, 0x0

    .line 297
    :try_start_4
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "images"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 298
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_18

    .line 299
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 301
    :cond_18
    new-instance v2, Ljava/io/File;

    const-string v3, "dock_bg.png"

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 302
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_29
    .catchall {:try_start_4 .. :try_end_29} :catchall_45
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_29} :catch_3c

    .line 303
    :try_start_29
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p1, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 304
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z
    :try_end_35
    .catchall {:try_start_29 .. :try_end_35} :catchall_53
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_35} :catch_55

    .line 309
    :try_start_35
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_38
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_38} :catch_51

    .line 316
    :cond_38
    :goto_38
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    .line 305
    :catch_3c
    move-exception v1

    .line 309
    :goto_3d
    if-eqz v0, :cond_38

    .line 310
    :try_start_3f
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_42} :catch_43

    goto :goto_38

    .line 313
    :catch_43
    move-exception v0

    goto :goto_38

    .line 307
    :catchall_45
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 309
    :goto_49
    if-eqz v1, :cond_4e

    .line 310
    :try_start_4b
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4e
    .catch Ljava/lang/Exception; {:try_start_4b .. :try_end_4e} :catch_4f

    .line 315
    :cond_4e
    :goto_4e
    throw v0

    :catch_4f
    move-exception v1

    goto :goto_4e

    :catch_51
    move-exception v0

    goto :goto_38

    .line 307
    :catchall_53
    move-exception v0

    goto :goto_49

    .line 305
    :catch_55
    move-exception v0

    move-object v0, v1

    goto :goto_3d
.end method

.method private a(Landroid/preference/Preference;)V
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x5

    .line 652
    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-nez v1, :cond_2b

    .line 653
    if-eqz p1, :cond_2b

    instance-of v1, p1, Landroid/preference/ListPreference;

    if-eqz v1, :cond_2b

    .line 654
    check-cast p1, Landroid/preference/ListPreference;

    .line 655
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    .line 656
    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v3

    .line 657
    array-length v1, v2

    if-le v1, v6, :cond_2b

    array-length v1, v3

    if-le v1, v6, :cond_2b

    .line 658
    new-array v4, v6, [Ljava/lang/CharSequence;

    move v1, v0

    .line 659
    :goto_1f
    if-lt v1, v6, :cond_2c

    .line 662
    invoke-virtual {p1, v4}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 663
    new-array v1, v6, [Ljava/lang/CharSequence;

    .line 664
    :goto_26
    if-lt v0, v6, :cond_33

    .line 667
    invoke-virtual {p1, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 671
    :cond_2b
    return-void

    .line 660
    :cond_2c
    aget-object v5, v2, v1

    aput-object v5, v4, v1

    .line 659
    add-int/lit8 v1, v1, 0x1

    goto :goto_1f

    .line 665
    :cond_33
    aget-object v2, v3, v0

    aput-object v2, v1, v0

    .line 664
    add-int/lit8 v0, v0, 0x1

    goto :goto_26
.end method

.method private a(Landroid/preference/PreferenceManager;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1548
    invoke-virtual {p0, p2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1549
    if-eqz v0, :cond_d

    .line 1550
    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1552
    :cond_d
    return-void
.end method

.method private a(Landroid/preference/PreferenceManager;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1538
    invoke-virtual {p0, p2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 1539
    if-eqz v0, :cond_19

    .line 1540
    invoke-virtual {p0, p3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1541
    if-eqz v1, :cond_19

    .line 1542
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 1545
    :cond_19
    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/preference/PreferencesActivity;Landroid/preference/PreferenceManager;Landroid/preference/Preference;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const v4, 0x7f0701fc

    const v7, 0x7f070007

    const v6, 0x108009b

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1104
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->d:Z

    if-nez v0, :cond_189

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    if-eqz p2, :cond_189

    instance-of v3, p2, Landroid/preference/ListPreference;

    if-eqz v3, :cond_18a

    invoke-static {p2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    :cond_1c
    :goto_1c
    const v3, 0x7f0701e7

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f0701ee

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f0701ef

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f0701f4

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    invoke-virtual {p0, v4}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f0701fd

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f0701ff

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070201

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f07020a

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f07020b

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f07020c

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f07020f

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070213

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070216

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070221

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070222

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070223

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070224

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070225

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070226

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070227

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070228

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070231

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070232

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070234

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070235

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f07025f

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_185

    const v3, 0x7f070263

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_193

    :cond_185
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    :cond_189
    :goto_189
    return-void

    :cond_18a
    instance-of v3, p2, Lcom/anddoes/launcher/preference/NumberPickerPreference;

    if-eqz v3, :cond_1c

    invoke-direct {p0, p2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->c(Landroid/preference/Preference;)V

    goto/16 :goto_1c

    :cond_193
    const v3, 0x7f0701fb

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20c

    invoke-virtual {p0, v4}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v3}, Lcom/anddoes/launcher/preference/h;->u()Z

    move-result v3

    if-eqz v3, :cond_1f2

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v3, "ALWAYS"

    invoke-virtual {v1, v3}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_1c6

    const-string v1, "ALWAYS"

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    const v1, 0x7f070090

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(I)V

    :cond_1c6
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0, v2}, Lcom/anddoes/launcher/preference/h;->a(Z)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f07

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07008e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/anddoes/launcher/preference/y;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/y;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v7, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_1ed
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto :goto_189

    :cond_1f2
    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v4, "LANDSCAPE_ONLY"

    invoke-virtual {v3, v4}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;)V

    if-eqz v0, :cond_206

    const-string v3, "LANDSCAPE_ONLY"

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    const v3, 0x7f070091

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setSummary(I)V

    :cond_206
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/preference/h;->a(Z)V

    goto :goto_1ed

    :cond_20c
    const v3, 0x7f0701e8

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22c

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_22c
    const v3, 0x7f0701eb

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24c

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->f()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_24c
    const v3, 0x7f070204

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26c

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->B()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_26c
    const v3, 0x7f070207

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_28c

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->E()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_28c
    const v3, 0x7f0701f0

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b7

    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    if-eqz v0, :cond_189

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->k()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NONE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto/16 :goto_189

    :cond_2b7
    const v3, 0x7f07020d

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2cd

    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->p(Landroid/preference/PreferenceManager;)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_2cd
    const v3, 0x7f070217

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2f4

    const v3, 0x7f070218

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2f4

    const v3, 0x7f070219

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_34c

    :cond_2f4
    iget-boolean v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->g:Z

    if-nez v3, :cond_189

    check-cast p2, Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v3}, Lcom/anddoes/launcher/preference/h;->T()Z

    move-result v3

    iget-object v4, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v4}, Lcom/anddoes/launcher/preference/h;->U()Z

    move-result v4

    iget-object v5, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v5}, Lcom/anddoes/launcher/preference/h;->V()Z

    move-result v5

    if-nez v3, :cond_34a

    if-nez v4, :cond_34a

    if-nez v5, :cond_34a

    iput-boolean v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->g:Z

    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v3, v0, v2}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;Z)V

    invoke-virtual {p2, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->g:Z

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f070005

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0700b6

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v3, Lcom/anddoes/launcher/preference/z;

    invoke-direct {v3, p0}, Lcom/anddoes/launcher/preference/z;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v7, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move v0, v1

    :goto_342
    if-eqz v0, :cond_189

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_34a
    move v0, v2

    goto :goto_342

    :cond_34c
    const v1, 0x7f07022e

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_362

    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->q(Landroid/preference/PreferenceManager;)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_362
    const v1, 0x7f070262

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_37e

    const v0, 0x7f070166

    const v1, 0x7f070167

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(II)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iput-boolean v2, v0, Lcom/android/launcher2/LauncherApplication;->f:Z

    goto/16 :goto_189

    :cond_37e
    const v1, 0x7f070265

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_189

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->bn()Z

    move-result v1

    const-string v2, "acra.enable"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->bn()Z

    move-result v1

    const-string v2, "enable_analytics"

    invoke-virtual {v0, v2, v1}, Lcom/anddoes/launcher/preference/h;->b(Ljava/lang/String;Z)V

    goto/16 :goto_189
.end method

.method static synthetic a(Lcom/anddoes/launcher/preference/PreferencesActivity;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 568
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, p1, p2}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/anddoes/launcher/preference/PreferencesActivity;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->d:Z

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 601
    iput-object p1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->j:Ljava/lang/String;

    .line 602
    iput-object p2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->k:Ljava/lang/String;

    .line 603
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 605
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 606
    const v2, 0x7f070176

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 609
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 610
    const v2, 0x7f020037

    invoke-static {p0, v2}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 613
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK_ACTIVITY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 614
    const-string v2, "android.intent.extra.INTENT"

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 615
    const-string v2, "android.intent.extra.TITLE"

    const v3, 0x7f070291

    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 616
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 617
    const/4 v0, 0x6

    invoke-virtual {p0, v1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 618
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 577
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ui/AppPickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 579
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1, p2, v4}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 580
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 581
    iput-object p2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->l:Ljava/lang/String;

    .line 582
    iput-object p3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->m:Ljava/lang/String;

    .line 585
    :cond_18
    sget-object v2, Lcom/anddoes/launcher/ui/AppPickerActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 586
    sget-object v2, Lcom/anddoes/launcher/ui/AppPickerActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v3, p1, v4}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 588
    sget-object v2, Lcom/anddoes/launcher/ui/AppPickerActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 589
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, p3, v4}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 592
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, p4, v4}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 594
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 595
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, p5, v4}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 598
    return-void
.end method

.method private a(Z)V
    .registers 8
    .parameter

    .prologue
    .line 621
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bo()Ljava/lang/String;

    move-result-object v4

    .line 622
    if-nez p1, :cond_24

    const-string v0, "NEVER"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_40

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    .line 623
    const-string v1, "last_check_update"

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/preference/h;->e(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_40

    .line 625
    :cond_24
    :try_start_24
    new-instance v0, Lcom/anddoes/launcher/k;

    iget v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->c:I

    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->b:Ljava/lang/String;

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/k;-><init>(Landroid/app/Activity;ILjava/lang/String;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 626
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const-string v3, "last_check_update"

    invoke-virtual {v0, v3, v1, v2}, Lcom/anddoes/launcher/preference/h;->a(Ljava/lang/String;J)V
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_40} :catch_41

    .line 631
    :cond_40
    :goto_40
    return-void

    :catch_41
    move-exception v0

    goto :goto_40
.end method

.method static synthetic b(Lcom/anddoes/launcher/preference/PreferencesActivity;)Lcom/anddoes/launcher/preference/b;
    .registers 2
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->f:Lcom/anddoes/launcher/preference/b;

    return-object v0
.end method

.method private static b(Landroid/preference/Preference;)V
    .registers 2
    .parameter

    .prologue
    .line 1523
    instance-of v0, p0, Landroid/preference/ListPreference;

    if-eqz v0, :cond_e

    move-object v0, p0

    .line 1524
    check-cast v0, Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1526
    :cond_e
    return-void
.end method

.method public static c()V
    .registers 0

    .prologue
    .line 1516
    return-void
.end method

.method private c(Landroid/preference/Preference;)V
    .registers 4
    .parameter

    .prologue
    .line 1530
    instance-of v0, p1, Lcom/anddoes/launcher/preference/NumberPickerPreference;

    if-eqz v0, :cond_30

    .line 1531
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f070014

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object v0, p1

    .line 1532
    check-cast v0, Lcom/anddoes/launcher/preference/NumberPickerPreference;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/NumberPickerPreference;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1531
    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1534
    :cond_30
    return-void
.end method

.method static synthetic c(Lcom/anddoes/launcher/preference/PreferencesActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    return v0
.end method

.method private d()V
    .registers 2

    .prologue
    .line 122
    invoke-static {p0}, Lcom/anddoes/launcher/a/e;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/anddoes/launcher/a/e;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    .line 123
    return-void
.end method

.method static synthetic d(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 620
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Z)V

    return-void
.end method

.method private p(Landroid/preference/PreferenceManager;)V
    .registers 4
    .parameter

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->K()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VERTICAL_PAGINATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1311
    const v1, 0x7f070213

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1312
    if-eqz v1, :cond_1f

    .line 1313
    if-eqz v0, :cond_20

    const/4 v0, 0x0

    :goto_1c
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1315
    :cond_1f
    return-void

    .line 1313
    :cond_20
    const/4 v0, 0x1

    goto :goto_1c
.end method

.method private q(Landroid/preference/PreferenceManager;)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1328
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->ao()Z

    move-result v3

    .line 1329
    const v0, 0x7f070223

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1330
    if-eqz v4, :cond_1b

    .line 1331
    if-eqz v3, :cond_ec

    move v0, v1

    :goto_18
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1333
    :cond_1b
    const v0, 0x7f070224

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1334
    if-eqz v4, :cond_2e

    .line 1335
    if-eqz v3, :cond_ef

    move v0, v1

    :goto_2b
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1337
    :cond_2e
    const v0, 0x7f070225

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1338
    if-eqz v4, :cond_41

    .line 1339
    if-eqz v3, :cond_f2

    move v0, v1

    :goto_3e
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1341
    :cond_41
    const v0, 0x7f070226

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1342
    if-eqz v4, :cond_54

    .line 1343
    if-eqz v3, :cond_f5

    move v0, v1

    :goto_51
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1345
    :cond_54
    const v0, 0x7f070227

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1346
    if-eqz v4, :cond_67

    .line 1347
    if-eqz v3, :cond_f8

    move v0, v1

    :goto_64
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1349
    :cond_67
    const v0, 0x7f070228

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1350
    if-eqz v4, :cond_7a

    .line 1351
    if-eqz v3, :cond_fb

    move v0, v1

    :goto_77
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1353
    :cond_7a
    const v0, 0x7f070229

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1354
    if-eqz v4, :cond_8d

    .line 1355
    if-eqz v3, :cond_fe

    move v0, v1

    :goto_8a
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1357
    :cond_8d
    const v0, 0x7f07022a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1358
    if-eqz v4, :cond_a0

    .line 1359
    if-eqz v3, :cond_100

    move v0, v1

    :goto_9d
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1361
    :cond_a0
    const v0, 0x7f07022b

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1362
    if-eqz v4, :cond_b3

    .line 1363
    if-eqz v3, :cond_102

    move v0, v1

    :goto_b0
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1365
    :cond_b3
    const v0, 0x7f07022c

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1366
    if-eqz v4, :cond_c6

    .line 1367
    if-eqz v3, :cond_104

    move v0, v1

    :goto_c3
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1369
    :cond_c6
    const v0, 0x7f07022d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    .line 1370
    if-eqz v4, :cond_d9

    .line 1371
    if-eqz v3, :cond_106

    move v0, v1

    :goto_d6
    invoke-virtual {v4, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1373
    :cond_d9
    const v0, 0x7f07022f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1374
    if-eqz v0, :cond_eb

    .line 1375
    if-eqz v3, :cond_108

    :goto_e8
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1377
    :cond_eb
    return-void

    :cond_ec
    move v0, v2

    .line 1331
    goto/16 :goto_18

    :cond_ef
    move v0, v2

    .line 1335
    goto/16 :goto_2b

    :cond_f2
    move v0, v2

    .line 1339
    goto/16 :goto_3e

    :cond_f5
    move v0, v2

    .line 1343
    goto/16 :goto_51

    :cond_f8
    move v0, v2

    .line 1347
    goto/16 :goto_64

    :cond_fb
    move v0, v2

    .line 1351
    goto/16 :goto_77

    :cond_fe
    move v0, v2

    .line 1355
    goto :goto_8a

    :cond_100
    move v0, v2

    .line 1359
    goto :goto_9d

    :cond_102
    move v0, v2

    .line 1363
    goto :goto_b0

    :cond_104
    move v0, v2

    .line 1367
    goto :goto_c3

    :cond_106
    move v0, v2

    .line 1371
    goto :goto_d6

    :cond_108
    move v1, v2

    .line 1375
    goto :goto_e8
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 280
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->showDialog(I)V

    .line 281
    return-void
.end method

.method public final a(Landroid/preference/PreferenceManager;)V
    .registers 7
    .parameter

    .prologue
    const v4, 0x7f090003

    .line 634
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 635
    const v0, 0x7f0701fe

    const v1, 0x7f070200

    invoke-direct {p0, p1, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 637
    :cond_16
    const v0, 0x7f0701e5

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 638
    if-eqz v0, :cond_39

    .line 639
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/anddoes/launcher/Launcher;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 640
    const-string v2, "com.anddoes.launcher.ACTION"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 641
    const-string v2, "LAUNCHER_ACTION"

    const-string v3, "MANAGE_SCREENS"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 642
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 644
    :cond_39
    const v0, 0x7f0701f4

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/Preference;)V

    .line 645
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 646
    if-eqz v0, :cond_5a

    .line 647
    const v0, 0x7f0701fa

    const v1, 0x7f0701fb

    invoke-direct {p0, p1, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 649
    :cond_5a
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 285
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->dismissDialog(I)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_4} :catch_5

    .line 289
    :goto_4
    return-void

    :catch_5
    move-exception v0

    goto :goto_4
.end method

.method public final b(Landroid/preference/PreferenceManager;)V
    .registers 6
    .parameter

    .prologue
    const v3, 0x7f07021a

    const v2, 0x7f070214

    .line 674
    const v0, 0x7f070203

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 675
    if-eqz v0, :cond_1b

    .line 676
    new-instance v1, Lcom/anddoes/launcher/preference/ab;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/ab;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 686
    :cond_1b
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 687
    if-eqz v0, :cond_2d

    .line 688
    new-instance v1, Lcom/anddoes/launcher/preference/ac;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/ac;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 696
    :cond_2d
    const v0, 0x7f070213

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/Preference;)V

    .line 697
    const v0, 0x7f070215

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 699
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-nez v0, :cond_49

    .line 701
    invoke-direct {p0, p1, v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 707
    :goto_48
    return-void

    .line 703
    :cond_49
    const v0, 0x7f070217

    invoke-direct {p0, p1, v2, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 704
    const v0, 0x7f070218

    invoke-direct {p0, p1, v2, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 705
    const v0, 0x7f070219

    invoke-direct {p0, p1, v2, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    goto :goto_48
.end method

.method public final c(Landroid/preference/PreferenceManager;)V
    .registers 3
    .parameter

    .prologue
    .line 710
    const v0, 0x7f070228

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 711
    return-void
.end method

.method public final d(Landroid/preference/PreferenceManager;)V
    .registers 4
    .parameter

    .prologue
    .line 714
    const v0, 0x7f070232

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 715
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-nez v0, :cond_13

    .line 716
    const v0, 0x7f070230

    const v1, 0x7f070233

    invoke-direct {p0, p1, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 718
    :cond_13
    return-void
.end method

.method public final e(Landroid/preference/PreferenceManager;)V
    .registers 6
    .parameter

    .prologue
    const v3, 0x7f070241

    const v2, 0x7f070240

    const v1, 0x7f07023c

    .line 721
    const v0, 0x7f070238

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 722
    const v0, 0x7f07023d

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 723
    const v0, 0x7f07023e

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 724
    const v0, 0x7f07023f

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 725
    invoke-direct {p0, p1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 726
    invoke-direct {p0, p1, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 727
    const v0, 0x7f070242

    invoke-direct {p0, p1, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 728
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-nez v0, :cond_37

    .line 729
    invoke-direct {p0, p1, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 730
    invoke-direct {p0, p1, v1, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 732
    :cond_37
    return-void
.end method

.method public final f(Landroid/preference/PreferenceManager;)V
    .registers 8
    .parameter

    .prologue
    const v5, 0x7f070264

    const v4, 0x7f07025d

    const v3, 0x7f07025b

    const/4 v0, 0x0

    .line 735
    const v1, 0x7f070261

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 736
    if-eqz v1, :cond_1f

    .line 737
    new-instance v2, Lcom/anddoes/launcher/preference/ad;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/ad;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 765
    :cond_1f
    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-eqz v1, :cond_a3

    .line 766
    const v1, 0x7f070257

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 767
    if-eqz v1, :cond_38

    .line 768
    new-instance v2, Lcom/anddoes/launcher/preference/ae;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/ae;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 775
    :cond_38
    const v1, 0x7f070259

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 776
    if-eqz v1, :cond_4d

    .line 777
    new-instance v2, Lcom/anddoes/launcher/preference/af;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/af;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 784
    :cond_4d
    invoke-static {p0}, Lcom/anddoes/launcher/q;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_85

    .line 785
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 786
    if-eqz v1, :cond_65

    .line 787
    new-instance v2, Lcom/anddoes/launcher/preference/ag;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/ag;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 810
    :cond_65
    :goto_65
    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 811
    if-eqz v1, :cond_84

    .line 812
    invoke-static {}, Lcom/anddoes/launcher/v;->b()Z

    move-result v2

    if-nez v2, :cond_7e

    .line 813
    const-string v2, "android.permission.BIND_APPWIDGET"

    invoke-static {p0, v2}, Lcom/anddoes/launcher/v;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7e

    const/4 v0, 0x1

    .line 812
    :cond_7e
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 814
    invoke-direct {p0, p1, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;I)V

    .line 816
    :cond_84
    return-void

    .line 795
    :cond_85
    const v1, 0x7f07025a

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 796
    if-eqz v1, :cond_95

    .line 797
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 799
    :cond_95
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 800
    if-eqz v1, :cond_65

    .line 801
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_65

    .line 805
    :cond_a3
    const v1, 0x7f070248

    const v2, 0x7f070250

    invoke-direct {p0, p1, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 806
    const v1, 0x7f070247

    const v2, 0x7f070255

    invoke-direct {p0, p1, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 807
    const v1, 0x7f070260

    invoke-direct {p0, p1, v4, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    .line 808
    const v1, 0x7f07025f

    invoke-direct {p0, p1, v4, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/preference/PreferenceManager;II)V

    goto :goto_65
.end method

.method public final g(Landroid/preference/PreferenceManager;)V
    .registers 4
    .parameter

    .prologue
    .line 819
    const v0, 0x7f070266

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 820
    if-eqz v0, :cond_15

    .line 821
    new-instance v1, Lcom/anddoes/launcher/preference/ah;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/ah;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 862
    :cond_15
    const v0, 0x7f070267

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 863
    if-eqz v0, :cond_2a

    .line 864
    new-instance v1, Lcom/anddoes/launcher/preference/j;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/j;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 907
    :cond_2a
    const v0, 0x7f070268

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 908
    if-eqz v0, :cond_3f

    .line 909
    new-instance v1, Lcom/anddoes/launcher/preference/m;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/m;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 938
    :cond_3f
    const v0, 0x7f070269

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 939
    if-eqz v0, :cond_54

    .line 940
    new-instance v1, Lcom/anddoes/launcher/preference/p;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/p;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 981
    :cond_54
    const v0, 0x7f07026a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 982
    if-eqz v0, :cond_69

    .line 983
    new-instance v1, Lcom/anddoes/launcher/preference/s;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/s;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1025
    :cond_69
    return-void
.end method

.method public final h(Landroid/preference/PreferenceManager;)V
    .registers 11
    .parameter

    .prologue
    const/high16 v8, 0x1000

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1028
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-eqz v0, :cond_e9

    const v0, 0x7f070154

    :goto_b
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1030
    const v1, 0x7f070155

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1031
    const v1, 0x7f07026b

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1032
    if-eqz v1, :cond_6d

    .line 1033
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 1034
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 1035
    const v3, 0x7f070156

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    iget-object v5, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->b:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1036
    const v3, 0x7f070157

    new-array v4, v7, [Ljava/lang/Object;

    iget v2, v2, Landroid/text/format/Time;->year:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1037
    iget-boolean v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-eqz v2, :cond_ee

    .line 1038
    const v2, 0x7f030023

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 1039
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/anddoes/launcher/ui/ApexLauncherProActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1047
    :cond_6d
    :goto_6d
    const v1, 0x7f07026e

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1048
    if-eqz v1, :cond_9e

    .line 1049
    const v2, 0x7f070160

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {p0, v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1050
    const v2, 0x7f070161

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-virtual {p0, v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1051
    new-instance v0, Lcom/anddoes/launcher/preference/v;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/v;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1070
    :cond_9e
    const v0, 0x7f07026f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1071
    if-eqz v0, :cond_b3

    .line 1072
    new-instance v1, Lcom/anddoes/launcher/preference/w;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/w;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1087
    :cond_b3
    const v0, 0x7f070270

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1088
    if-eqz v0, :cond_d3

    .line 1089
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "http://blog.anddoes.com/apex-launcher-faq/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1090
    invoke-virtual {v1, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1091
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    .line 1093
    :cond_d3
    const v0, 0x7f07026d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1094
    if-eqz v0, :cond_e8

    .line 1095
    new-instance v1, Lcom/anddoes/launcher/preference/x;

    invoke-direct {v1, p0}, Lcom/anddoes/launcher/preference/x;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1102
    :cond_e8
    return-void

    .line 1029
    :cond_e9
    const v0, 0x7f070272

    goto/16 :goto_b

    .line 1041
    :cond_ee
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    .line 1042
    const-string v4, "http://www.apexlauncher.com/"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1041
    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1043
    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1044
    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setIntent(Landroid/content/Intent;)V

    goto/16 :goto_6d
.end method

.method public final i(Landroid/preference/PreferenceManager;)V
    .registers 5
    .parameter

    .prologue
    .line 1241
    const v0, 0x7f0701e8

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1242
    if-eqz v0, :cond_1a

    .line 1243
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1245
    :cond_1a
    const v0, 0x7f0701eb

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1246
    if-eqz v0, :cond_34

    .line 1247
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1249
    :cond_34
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1250
    if-eqz v0, :cond_50

    .line 1251
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->k()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NONE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 1253
    :cond_50
    const v0, 0x7f0701ee

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1254
    const v0, 0x7f0701ef

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1256
    const v0, 0x7f0701f0

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1257
    const v0, 0x7f0701f4

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1258
    const v0, 0x7f0701f6

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1259
    const v0, 0x7f0701f8

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1260
    const v0, 0x7f0701fc

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1261
    const v0, 0x7f0701fd

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1262
    return-void
.end method

.method public final j(Landroid/preference/PreferenceManager;)V
    .registers 7
    .parameter

    .prologue
    const v3, 0x7f070215

    .line 1277
    const v0, 0x7f070204

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1278
    if-eqz v0, :cond_1d

    .line 1279
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->B()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1281
    :cond_1d
    const v0, 0x7f070207

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1282
    if-eqz v0, :cond_37

    .line 1283
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1}, Lcom/anddoes/launcher/preference/h;->E()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1286
    :cond_37
    const v0, 0x7f07020a

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1287
    const v0, 0x7f07020b

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1288
    const v0, 0x7f07020c

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1289
    const v0, 0x7f07020d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1290
    const v0, 0x7f07020f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1291
    const v0, 0x7f070213

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1292
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->p(Landroid/preference/PreferenceManager;)V

    .line 1294
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    .line 1295
    if-eqz v0, :cond_c1

    .line 1296
    const-string v1, "APP"

    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2}, Lcom/anddoes/launcher/preference/h;->R()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_fa

    .line 1297
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "drawer_tab_icon_app"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1298
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_ec

    const v1, 0x7f0700b0

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_be
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1304
    :cond_c1
    :goto_c1
    const v0, 0x7f07021c

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1305
    const v0, 0x7f07021d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1306
    const v0, 0x7f07021f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1307
    return-void

    .line 1299
    :cond_ec
    const v2, 0x7f0700f3

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_be

    .line 1301
    :cond_fa
    invoke-virtual {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto :goto_c1
.end method

.method public final k(Landroid/preference/PreferenceManager;)V
    .registers 3
    .parameter

    .prologue
    .line 1318
    const v0, 0x7f070223

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->c(Landroid/preference/Preference;)V

    .line 1319
    const v0, 0x7f070224

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->c(Landroid/preference/Preference;)V

    .line 1320
    const v0, 0x7f070225

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1321
    const v0, 0x7f070226

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1322
    const v0, 0x7f070228

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1323
    const v0, 0x7f07022d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1324
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->q(Landroid/preference/PreferenceManager;)V

    .line 1325
    return-void
.end method

.method public final l(Landroid/preference/PreferenceManager;)V
    .registers 3
    .parameter

    .prologue
    .line 1380
    const v0, 0x7f070231

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1381
    const v0, 0x7f070232

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1382
    return-void
.end method

.method public final m(Landroid/preference/PreferenceManager;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0700f3

    const v5, 0x7f0700f0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1385
    const v0, 0x7f070237

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1386
    const v0, 0x7f07023b

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1389
    const v0, 0x7f070238

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1390
    if-eqz v1, :cond_55

    .line 1391
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aw()Ljava/lang/String;

    move-result-object v0

    .line 1392
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18e

    .line 1393
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "home_key_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1394
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_184

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_52
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1405
    :cond_55
    :goto_55
    const v0, 0x7f07023d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1406
    if-eqz v1, :cond_85

    .line 1407
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aA()Ljava/lang/String;

    move-result-object v0

    .line 1408
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d0

    .line 1409
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "pinch_in_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1410
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1c6

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_82
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1421
    :cond_85
    :goto_85
    const v0, 0x7f07023e

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1422
    if-eqz v1, :cond_b5

    .line 1423
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aB()Ljava/lang/String;

    move-result-object v0

    .line 1424
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_212

    .line 1425
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "swipe_up_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1426
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_208

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_b2
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1437
    :cond_b5
    :goto_b5
    const v0, 0x7f07023f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1438
    if-eqz v1, :cond_e5

    .line 1439
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aC()Ljava/lang/String;

    move-result-object v0

    .line 1440
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_254

    .line 1441
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "swipe_down_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1442
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_24a

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_e2
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1453
    :cond_e5
    :goto_e5
    const v0, 0x7f070240

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1454
    if-eqz v1, :cond_115

    .line 1455
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aD()Ljava/lang/String;

    move-result-object v0

    .line 1456
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_296

    .line 1457
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "two_finger_swipe_up_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1458
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_28c

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_112
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1469
    :cond_115
    :goto_115
    const v0, 0x7f070241

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1470
    if-eqz v1, :cond_145

    .line 1471
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aE()Ljava/lang/String;

    move-result-object v0

    .line 1472
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2d8

    .line 1473
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "two_finger_swipe_down_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1474
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2ce

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_142
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1485
    :cond_145
    :goto_145
    const v0, 0x7f070242

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    .line 1486
    if-eqz v1, :cond_175

    .line 1487
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->aF()Ljava/lang/String;

    move-result-object v0

    .line 1488
    const-string v2, "LAUNCH_APP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31a

    .line 1489
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "desktop_double_tap_action_app"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1490
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_310

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_172
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1501
    :cond_175
    :goto_175
    const v0, 0x7f070243

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1502
    return-void

    .line 1395
    :cond_184
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_52

    .line 1396
    :cond_18e
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b6

    .line 1397
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "home_key_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1398
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1ad

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1a8
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_55

    .line 1399
    :cond_1ad
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1a8

    .line 1401
    :cond_1b6
    const v0, 0x7f070238

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_55

    .line 1411
    :cond_1c6
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_82

    .line 1412
    :cond_1d0
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f8

    .line 1413
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "pinch_in_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1414
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1ef

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1ea
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_85

    .line 1415
    :cond_1ef
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1ea

    .line 1417
    :cond_1f8
    const v0, 0x7f07023d

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_85

    .line 1427
    :cond_208
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_b2

    .line 1428
    :cond_212
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23a

    .line 1429
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "swipe_up_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1430
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_231

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_22c
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_b5

    .line 1431
    :cond_231
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_22c

    .line 1433
    :cond_23a
    const v0, 0x7f07023e

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_b5

    .line 1443
    :cond_24a
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_e2

    .line 1444
    :cond_254
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_27c

    .line 1445
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "swipe_down_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1446
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_273

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_26e
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_e5

    .line 1447
    :cond_273
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_26e

    .line 1449
    :cond_27c
    const v0, 0x7f07023f

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_e5

    .line 1459
    :cond_28c
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_112

    .line 1460
    :cond_296
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2be

    .line 1461
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "two_finger_swipe_up_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1462
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2b5

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2b0
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_115

    .line 1463
    :cond_2b5
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2b0

    .line 1465
    :cond_2be
    const v0, 0x7f070240

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_115

    .line 1475
    :cond_2ce
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_142

    .line 1476
    :cond_2d8
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_300

    .line 1477
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "two_finger_swipe_down_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1478
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2f7

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2f2
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_145

    .line 1479
    :cond_2f7
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2f2

    .line 1481
    :cond_300
    const v0, 0x7f070241

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_145

    .line 1491
    :cond_310
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_172

    .line 1492
    :cond_31a
    const-string v2, "LAUNCH_SHORTCUT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_342

    .line 1493
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "desktop_double_tap_action_shortcut_name"

    invoke-virtual {v0, v2, v7}, Lcom/anddoes/launcher/preference/h;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1494
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_339

    invoke-virtual {p0, v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_334
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_175

    .line 1495
    :cond_339
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v6, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_334

    .line 1497
    :cond_342
    const v0, 0x7f070242

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    goto/16 :goto_175
.end method

.method public final n(Landroid/preference/PreferenceManager;)V
    .registers 4
    .parameter

    .prologue
    .line 1505
    const v0, 0x7f070261

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 1506
    if-eqz v0, :cond_16

    .line 1507
    invoke-static {p0}, Lcom/anddoes/launcher/v;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1508
    if-eqz v1, :cond_16

    .line 1509
    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1512
    :cond_16
    const v0, 0x7f070262

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1513
    return-void
.end method

.method public final o(Landroid/preference/PreferenceManager;)V
    .registers 3
    .parameter

    .prologue
    .line 1519
    const v0, 0x7f07026c

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Landroid/preference/Preference;)V

    .line 1520
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x7

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 321
    const/4 v0, -0x1

    if-ne p2, v0, :cond_77

    .line 322
    if-ne p1, v1, :cond_7b

    .line 324
    :try_start_a
    sget-object v0, Lcom/anddoes/launcher/ui/AppPickerActivity;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 325
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->b:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 326
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_21

    .line 327
    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, v0, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_21
    sget-object v0, Lcom/anddoes/launcher/ui/AppPickerActivity;->g:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 331
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->h:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 332
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_38

    .line 333
    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, v0, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_38
    sget-object v0, Lcom/anddoes/launcher/ui/AppPickerActivity;->e:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 337
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_77

    .line 338
    sget-object v1, Lcom/anddoes/launcher/ui/AppPickerActivity;->f:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 339
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_55

    .line 340
    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v2, v0, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_55
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_77

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_77

    .line 343
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_77
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_77} :catch_19b

    .line 412
    :cond_77
    :goto_77
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 413
    :goto_7a
    return-void

    .line 349
    :cond_7b
    const/4 v0, 0x2

    if-ne p1, v0, :cond_8c

    .line 350
    if-eqz p3, :cond_77

    .line 351
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/anddoes/launcher/v;->b(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 352
    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/graphics/Bitmap;)V

    goto :goto_77

    .line 354
    :cond_8c
    const/4 v0, 0x3

    if-ne p1, v0, :cond_9b

    .line 355
    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 356
    invoke-direct {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Landroid/graphics/Bitmap;)V

    goto :goto_77

    .line 357
    :cond_9b
    const/4 v0, 0x4

    if-ne p1, v0, :cond_f6

    .line 358
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_77

    .line 360
    invoke-static {p0, v0}, Lcom/anddoes/launcher/v;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 361
    if-eqz v2, :cond_77

    .line 362
    const/4 v0, 0x0

    .line 364
    :try_start_ab
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "images"

    invoke-direct {v1, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 365
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_bf

    .line 366
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 368
    :cond_bf
    new-instance v3, Ljava/io/File;

    const-string v4, "folder_bg.png"

    invoke-direct {v3, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 369
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_d0
    .catchall {:try_start_ab .. :try_end_d0} :catchall_ec
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_d0} :catch_e3

    .line 370
    :try_start_d0
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v2, v0, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 371
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/android/launcher2/LauncherApplication;->f:Z
    :try_end_dc
    .catchall {:try_start_d0 .. :try_end_dc} :catchall_194
    .catch Ljava/lang/Exception; {:try_start_d0 .. :try_end_dc} :catch_197

    .line 376
    :try_start_dc
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_df
    .catch Ljava/lang/Exception; {:try_start_dc .. :try_end_df} :catch_191

    .line 383
    :cond_df
    :goto_df
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_77

    .line 372
    :catch_e3
    move-exception v1

    .line 376
    :goto_e4
    if-eqz v0, :cond_df

    .line 377
    :try_start_e6
    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_e9
    .catch Ljava/lang/Exception; {:try_start_e6 .. :try_end_e9} :catch_ea

    goto :goto_df

    .line 380
    :catch_ea
    move-exception v0

    goto :goto_df

    .line 374
    :catchall_ec
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 376
    :goto_f0
    if-eqz v1, :cond_f5

    .line 377
    :try_start_f2
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_f5
    .catch Ljava/lang/Exception; {:try_start_f2 .. :try_end_f5} :catch_18e

    .line 382
    :cond_f5
    :goto_f5
    throw v0

    .line 386
    :cond_f6
    const/4 v0, 0x5

    if-ne p1, v0, :cond_108

    .line 387
    sget-object v0, Lcom/anddoes/launcher/ui/MultiPickerActivity;->c:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 388
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    const-string v2, "drawer_hidden_apps"

    invoke-virtual {v1, v2, v0}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    .line 389
    :cond_108
    const/4 v0, 0x6

    if-ne p1, v0, :cond_153

    .line 390
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 391
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 392
    if-eqz v0, :cond_133

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_133

    .line 393
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 394
    const-class v1, Lcom/anddoes/launcher/ui/ActPickerActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 395
    invoke-virtual {p0, v0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_7a

    .line 397
    :cond_133
    const/4 v0, 0x7

    :try_start_134
    invoke-virtual {p0, p3, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_137
    .catch Landroid/content/ActivityNotFoundException; {:try_start_134 .. :try_end_137} :catch_139
    .catch Ljava/lang/SecurityException; {:try_start_134 .. :try_end_137} :catch_146

    goto/16 :goto_7a

    :catch_139
    move-exception v0

    const v0, 0x7f070279

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7a

    :catch_146
    move-exception v0

    const v0, 0x7f070178

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7a

    .line 400
    :cond_153
    if-eq p1, v3, :cond_157

    if-ne p1, v2, :cond_77

    .line 401
    :cond_157
    const-string v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 402
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 403
    if-eqz v0, :cond_77

    if-eqz v1, :cond_77

    .line 404
    const-string v2, "android.intent.action.CALL_PRIVILEGED"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17a

    .line 405
    const-string v2, "android.intent.action.CALL"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 407
    :cond_17a
    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->j:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 408
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/anddoes/launcher/preference/h;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_77

    :catch_18e
    move-exception v1

    goto/16 :goto_f5

    :catch_191
    move-exception v0

    goto/16 :goto_df

    .line 374
    :catchall_194
    move-exception v0

    goto/16 :goto_f0

    .line 372
    :catch_197
    move-exception v0

    move-object v0, v1

    goto/16 :goto_e4

    .line 347
    :catch_19b
    move-exception v0

    goto/16 :goto_77
.end method

.method public onBuildHeaders(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 170
    const v0, 0x7f060009

    invoke-virtual {p0, v0, p1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->loadHeadersFromResource(ILjava/util/List;)V

    .line 171
    iput-object p1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    .line 173
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->d()V

    .line 174
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ApexService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 175
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 104
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    new-instance v0, Lcom/anddoes/launcher/preference/h;

    invoke-direct {v0, p0}, Lcom/anddoes/launcher/preference/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    .line 107
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->bj()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/anddoes/launcher/v;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    iput-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    .line 110
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->d()V

    .line 112
    :try_start_1f
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 113
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    iput-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->b:Ljava/lang/String;

    .line 114
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iput v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->c:I
    :try_end_34
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_34} :catch_38

    .line 118
    :goto_34
    invoke-direct {p0, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Z)V

    .line 119
    return-void

    :catch_38
    move-exception v0

    goto :goto_34
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 263
    packed-switch p1, :pswitch_data_36

    .line 276
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_8
    return-object v0

    .line 266
    :pswitch_9
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 267
    sget v1, Landroid/app/ProgressDialog;->STYLE_SPINNER:I

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 268
    const v1, 0x7f070015

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 269
    const v1, 0x7f070017

    invoke-virtual {p0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 270
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 271
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_8

    .line 274
    :pswitch_2b
    new-instance v0, Lcom/anddoes/launcher/preference/ao;

    invoke-direct {v0, p0, v2}, Lcom/anddoes/launcher/preference/ao;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;B)V

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/ao;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_8

    .line 263
    nop

    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_9
        :pswitch_2b
    .end packed-switch
.end method

.method public onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 207
    if-eqz p1, :cond_15

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0d0077

    cmp-long v0, v0, v2

    if-nez v0, :cond_15

    .line 208
    const v0, 0x7f070001

    const v1, 0x7f070168

    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(II)V

    .line 228
    :goto_14
    return-void

    .line 209
    :cond_15
    if-eqz p1, :cond_2e

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0d0075

    cmp-long v0, v0, v2

    if-nez v0, :cond_2e

    .line 210
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ui/ApexLauncherProActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 211
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    .line 212
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->finish()V

    goto :goto_14

    .line 213
    :cond_2e
    if-eqz p1, :cond_44

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0d0071

    cmp-long v0, v0, v2

    if-nez v0, :cond_44

    .line 214
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/anddoes/launcher/ui/ThemeListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_14

    .line 217
    :cond_44
    if-eqz p1, :cond_69

    iget-wide v0, p1, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v2, 0x7f0d0076

    cmp-long v0, v0, v2

    if-nez v0, :cond_69

    .line 218
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-eqz v0, :cond_6d

    const v0, 0x7f070154

    :goto_56
    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 221
    const v1, 0x7f070155

    const/4 v2, 0x1

    :try_start_5e
    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_69} :catch_71

    .line 226
    :cond_69
    :goto_69
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onHeaderClick(Landroid/preference/PreferenceActivity$Header;I)V

    goto :goto_14

    .line 219
    :cond_6d
    const v0, 0x7f070272

    goto :goto_56

    .line 223
    :catch_71
    move-exception v1

    iput-object v0, p1, Landroid/preference/PreferenceActivity$Header;->title:Ljava/lang/CharSequence;

    goto :goto_69
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_c

    .line 233
    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->onBackPressed()V

    .line 235
    :cond_c
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 159
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 160
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->d:Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_10

    .line 161
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->h:Lcom/android/launcher2/LauncherApplication;

    iget-object v0, v0, Lcom/android/launcher2/LauncherApplication;->d:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->K()V

    .line 163
    :cond_10
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 429
    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->d:Z

    if-eqz v1, :cond_8

    move v0, v6

    .line 565
    :cond_7
    :goto_7
    return v0

    .line 433
    :cond_8
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 434
    const v2, 0x7f070215

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 435
    const-string v0, "APP"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 436
    const-string v1, "drawer_tab_icon_app"

    .line 437
    const-string v2, "drawer_tab_icon_pkg"

    .line 438
    const-string v3, "drawer_tab_icon_act"

    .line 439
    const-string v4, "drawer_tab_icon_component"

    .line 440
    const-string v5, "drawer_tab_icon_intent"

    move-object v0, p0

    .line 436
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2f
    move v0, v6

    .line 442
    goto :goto_7

    .line 443
    :cond_31
    const v2, 0x7f070228

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 444
    const-string v0, "CUSTOM"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 445
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->showDialog(I)V

    :cond_4a
    move v0, v6

    .line 447
    goto :goto_7

    .line 448
    :cond_4c
    const v2, 0x7f070232

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8a

    .line 449
    const-string v1, "CUSTOM"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7c

    .line 450
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 451
    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 454
    const v2, 0x7f070170

    :try_start_70
    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 453
    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 454
    const/4 v2, 0x4

    .line 453
    invoke-virtual {p0, v1, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_70 .. :try_end_7c} :catch_7e

    :cond_7c
    :goto_7c
    move v0, v6

    .line 460
    goto :goto_7

    .line 456
    :catch_7e
    move-exception v1

    .line 457
    const v1, 0x7f070151

    .line 456
    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 457
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_7c

    .line 461
    :cond_8a
    const v2, 0x7f070238

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c8

    .line 462
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 463
    const-string v1, "home_key_action_app"

    .line 464
    const-string v2, "home_key_action_pkg"

    .line 465
    const-string v3, "home_key_action_act"

    .line 466
    const-string v4, "home_key_action_component"

    .line 467
    const-string v5, "home_key_action_intent"

    move-object v0, p0

    .line 463
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b1
    :goto_b1
    move v0, v6

    .line 472
    goto/16 :goto_7

    .line 468
    :cond_b4
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 469
    const-string v0, "home_key_action_shortcut_name"

    .line 470
    const-string v1, "home_key_action_shortcut_intent"

    .line 469
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_b1

    .line 473
    :cond_c8
    const v2, 0x7f07023d

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_106

    .line 474
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f2

    .line 475
    const-string v1, "pinch_in_action_app"

    .line 476
    const-string v2, "pinch_in_action_pkg"

    .line 477
    const-string v3, "pinch_in_action_act"

    .line 478
    const-string v4, "pinch_in_action_component"

    .line 479
    const-string v5, "pinch_in_action_intent"

    move-object v0, p0

    .line 475
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_ef
    :goto_ef
    move v0, v6

    .line 484
    goto/16 :goto_7

    .line 480
    :cond_f2
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ef

    .line 481
    const-string v0, "pinch_in_action_shortcut_name"

    .line 482
    const-string v1, "pinch_in_action_shortcut_intent"

    .line 481
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_ef

    .line 485
    :cond_106
    const v2, 0x7f07023e

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_144

    .line 486
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_130

    .line 487
    const-string v1, "swipe_up_action_app"

    .line 488
    const-string v2, "swipe_up_action_pkg"

    .line 489
    const-string v3, "swipe_up_action_act"

    .line 490
    const-string v4, "swipe_up_action_component"

    .line 491
    const-string v5, "swipe_up_action_intent"

    move-object v0, p0

    .line 487
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_12d
    :goto_12d
    move v0, v6

    .line 496
    goto/16 :goto_7

    .line 492
    :cond_130
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12d

    .line 493
    const-string v0, "swipe_up_action_shortcut_name"

    .line 494
    const-string v1, "swipe_up_action_shortcut_intent"

    .line 493
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_12d

    .line 497
    :cond_144
    const v2, 0x7f07023f

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_182

    .line 498
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16e

    .line 499
    const-string v1, "swipe_down_action_app"

    .line 500
    const-string v2, "swipe_down_action_pkg"

    .line 501
    const-string v3, "swipe_down_action_act"

    .line 502
    const-string v4, "swipe_down_action_component"

    .line 503
    const-string v5, "swipe_down_action_intent"

    move-object v0, p0

    .line 499
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_16b
    :goto_16b
    move v0, v6

    .line 508
    goto/16 :goto_7

    .line 504
    :cond_16e
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16b

    .line 505
    const-string v0, "swipe_down_action_shortcut_name"

    .line 506
    const-string v1, "swipe_down_action_shortcut_intent"

    .line 505
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_16b

    .line 509
    :cond_182
    const v2, 0x7f070240

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c0

    .line 510
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1ac

    .line 511
    const-string v1, "two_finger_swipe_up_action_app"

    .line 512
    const-string v2, "two_finger_swipe_up_action_pkg"

    .line 513
    const-string v3, "two_finger_swipe_up_action_act"

    .line 514
    const-string v4, "two_finger_swipe_up_action_component"

    .line 515
    const-string v5, "two_finger_swipe_up_action_intent"

    move-object v0, p0

    .line 511
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1a9
    :goto_1a9
    move v0, v6

    .line 520
    goto/16 :goto_7

    .line 516
    :cond_1ac
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a9

    .line 517
    const-string v0, "two_finger_swipe_up_action_shortcut_name"

    .line 518
    const-string v1, "two_finger_swipe_up_action_shortcut_intent"

    .line 517
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1a9

    .line 521
    :cond_1c0
    const v2, 0x7f070241

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1fe

    .line 522
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1ea

    .line 523
    const-string v1, "two_finger_swipe_down_action_app"

    .line 524
    const-string v2, "two_finger_swipe_down_action_pkg"

    .line 525
    const-string v3, "two_finger_swipe_down_action_act"

    .line 526
    const-string v4, "two_finger_swipe_down_action_component"

    .line 527
    const-string v5, "two_finger_swipe_down_action_intent"

    move-object v0, p0

    .line 523
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1e7
    :goto_1e7
    move v0, v6

    .line 532
    goto/16 :goto_7

    .line 528
    :cond_1ea
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e7

    .line 529
    const-string v0, "two_finger_swipe_down_action_shortcut_name"

    .line 530
    const-string v1, "two_finger_swipe_down_action_shortcut_intent"

    .line 529
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1e7

    .line 533
    :cond_1fe
    const v2, 0x7f070242

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23c

    .line 534
    const-string v0, "LAUNCH_APP"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_228

    .line 535
    const-string v1, "desktop_double_tap_action_app"

    .line 536
    const-string v2, "desktop_double_tap_action_pkg"

    .line 537
    const-string v3, "desktop_double_tap_action_act"

    .line 538
    const-string v4, "desktop_double_tap_action_component"

    .line 539
    const-string v5, "desktop_double_tap_action_intent"

    move-object v0, p0

    .line 535
    invoke-direct/range {v0 .. v5}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_225
    :goto_225
    move v0, v6

    .line 544
    goto/16 :goto_7

    .line 540
    :cond_228
    const-string v0, "LAUNCH_SHORTCUT"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_225

    .line 541
    const-string v0, "desktop_double_tap_action_shortcut_name"

    .line 542
    const-string v1, "desktop_double_tap_action_shortcut_intent"

    .line 541
    invoke-direct {p0, v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_225

    .line 545
    :cond_23c
    const v2, 0x7f070264

    invoke-virtual {p0, v2}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 546
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 547
    check-cast p2, Ljava/lang/Boolean;

    .line 548
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_267

    .line 549
    invoke-static {}, Lcom/anddoes/launcher/v;->e()Z

    move-result v1

    if-nez v1, :cond_27b

    .line 550
    const v1, 0x7f070139

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_7

    .line 555
    :cond_267
    :try_start_267
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "RootHelper"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 556
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_27b

    .line 557
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_27b
    .catch Ljava/lang/Exception; {:try_start_267 .. :try_end_27b} :catch_27e

    :cond_27b
    :goto_27b
    move v0, v6

    .line 562
    goto/16 :goto_7

    :catch_27e
    move-exception v0

    goto :goto_27b
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 139
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 140
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->a()I

    move-result v1

    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/h;->b()I

    move-result v0

    if-le v0, v1, :cond_12

    const/4 v0, 0x0

    :cond_12
    if-nez v0, :cond_1d

    div-int/lit8 v0, v1, 0x2

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->e:Lcom/anddoes/launcher/preference/h;

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/preference/h;->b(I)V

    .line 141
    :cond_1d
    return-void
.end method

.method public onStart()V
    .registers 3

    .prologue
    .line 132
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    .line 134
    new-instance v0, Lcom/anddoes/launcher/preference/b;

    iget-boolean v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    invoke-direct {v0, p0, v1}, Lcom/anddoes/launcher/preference/b;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->f:Lcom/anddoes/launcher/preference/b;

    .line 135
    return-void
.end method

.method public setListAdapter(Landroid/widget/ListAdapter;)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 180
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    if-nez v0, :cond_13

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    move v1, v2

    .line 184
    :goto_d
    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lt v1, v0, :cond_35

    .line 188
    :cond_13
    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    if-eqz v0, :cond_28

    .line 189
    const/4 v1, 0x0

    .line 190
    :goto_18
    iget-object v0, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_44

    move-object v0, v1

    .line 197
    :cond_21
    if-eqz v0, :cond_28

    .line 198
    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 202
    :cond_28
    new-instance v0, Lcom/anddoes/launcher/preference/ap;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    iget-boolean v2, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->i:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/anddoes/launcher/preference/ap;-><init>(Landroid/content/Context;Ljava/util/List;Z)V

    invoke-super {p0, v0}, Landroid/preference/PreferenceActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 203
    return-void

    .line 185
    :cond_35
    iget-object v3, p0, Lcom/anddoes/launcher/preference/PreferencesActivity;->a:Ljava/util/List;

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d

    .line 191
    :cond_44
    invoke-interface {p1, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity$Header;

    .line 192
    iget-wide v3, v0, Landroid/preference/PreferenceActivity$Header;->id:J

    const-wide/32 v5, 0x7f0d0075

    cmp-long v3, v3, v5

    if-eqz v3, :cond_21

    .line 190
    add-int/lit8 v2, v2, 0x1

    goto :goto_18
.end method
