.class final Lcom/anddoes/launcher/preference/ay;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/anddoes/launcher/preference/PreferencesActivity;


# direct methods
.method private constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 2022
    iput-object p1, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2024
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/anddoes/launcher/preference/ay;->a:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/anddoes/launcher/preference/PreferencesActivity;B)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2022
    invoke-direct {p0, p1}, Lcom/anddoes/launcher/preference/ay;-><init>(Lcom/anddoes/launcher/preference/PreferencesActivity;)V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2036
    :try_start_2
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b(Lcom/anddoes/launcher/preference/PreferencesActivity;)Lcom/anddoes/launcher/preference/b;

    move-result-object v3

    new-instance v0, Ljava/io/File;

    sget-object v4, Lcom/anddoes/launcher/preference/b;->c:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, v3, Lcom/anddoes/launcher/preference/b;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "_preferences"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/anddoes/launcher/preference/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    sget-object v5, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v6, "apex_settings"

    invoke-static {v6}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_a6

    invoke-static {v4, v0}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    :goto_43
    iget-boolean v3, v3, Lcom/anddoes/launcher/preference/b;->b:Z

    if-eqz v3, :cond_a4

    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v5, "DrawerGroups"

    invoke-static {v5}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_70

    new-instance v4, Ljava/io/File;

    sget-object v5, Lcom/anddoes/launcher/preference/b;->c:Ljava/io/File;

    const-string v6, "DrawerGroups"

    invoke-static {v6}, Lcom/anddoes/launcher/preference/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v0, :cond_9c

    invoke-static {v3, v4}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_9c

    move v0, v1

    :cond_70
    :goto_70
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/anddoes/launcher/preference/b;->d:Ljava/io/File;

    const-string v5, "SwipeActions"

    invoke-static {v5}, Lcom/anddoes/launcher/preference/b;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_a4

    new-instance v4, Ljava/io/File;

    sget-object v5, Lcom/anddoes/launcher/preference/b;->c:Ljava/io/File;

    const-string v6, "SwipeActions"

    invoke-static {v6}, Lcom/anddoes/launcher/preference/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-eqz v0, :cond_9e

    invoke-static {v3, v4}, Lcom/anddoes/launcher/v;->a(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_9e

    :goto_98
    iput-boolean v1, p0, Lcom/anddoes/launcher/preference/ay;->a:Z
    :try_end_9a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_9a} :catch_a0

    .line 2041
    :goto_9a
    const/4 v0, 0x0

    return-object v0

    :cond_9c
    move v0, v2

    .line 2036
    goto :goto_70

    :cond_9e
    move v1, v2

    goto :goto_98

    .line 2038
    :catch_a0
    move-exception v0

    iput-boolean v2, p0, Lcom/anddoes/launcher/preference/ay;->a:Z

    goto :goto_9a

    :cond_a4
    move v1, v0

    goto :goto_98

    :cond_a6
    move v0, v1

    goto :goto_43
.end method


# virtual methods
.method protected final varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/anddoes/launcher/preference/ay;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-static {v0, v4}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Lcom/anddoes/launcher/preference/PreferencesActivity;Z)V

    iget-object v0, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->b()V

    iget-boolean v0, p0, Lcom/anddoes/launcher/preference/ay;->a:Z

    if-eqz v0, :cond_70

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x108009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x7f07

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v3, 0x7f070145

    invoke-virtual {v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v3, 0x7f070167

    invoke-virtual {v2, v3}, Lcom/anddoes/launcher/preference/PreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000e

    new-instance v2, Lcom/anddoes/launcher/preference/az;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/az;-><init>(Lcom/anddoes/launcher/preference/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f07000f

    new-instance v2, Lcom/anddoes/launcher/preference/ba;

    invoke-direct {v2, p0}, Lcom/anddoes/launcher/preference/ba;-><init>(Lcom/anddoes/launcher/preference/ay;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    :goto_6f
    return-void

    :cond_70
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const v1, 0x7f070151

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_6f
.end method

.method protected final onPreExecute()V
    .registers 3

    .prologue
    .line 2028
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a(Lcom/anddoes/launcher/preference/PreferencesActivity;Z)V

    .line 2029
    iget-object v0, p0, Lcom/anddoes/launcher/preference/ay;->b:Lcom/anddoes/launcher/preference/PreferencesActivity;

    invoke-virtual {v0}, Lcom/anddoes/launcher/preference/PreferencesActivity;->a()V

    .line 2030
    return-void
.end method
