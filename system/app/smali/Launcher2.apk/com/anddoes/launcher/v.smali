.class public final Lcom/anddoes/launcher/v;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(FFF)F
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    const/4 v3, 0x0

    .line 472
    sub-float v1, p2, p1

    .line 473
    const/high16 v0, 0x43c8

    div-float v0, p0, v0

    sub-float/2addr v0, v4

    mul-float v2, v0, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    .line 475
    cmpl-float v2, v1, v3

    if-lez v2, :cond_19

    cmpl-float v2, v0, p2

    if-lez v2, :cond_19

    move v0, p2

    .line 477
    :cond_19
    cmpg-float v1, v1, v3

    if-gez v1, :cond_22

    cmpg-float v1, v0, p2

    if-gez v1, :cond_22

    .line 479
    :goto_21
    return p2

    :cond_22
    move p2, v0

    goto :goto_21
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 15
    .parameter
    .parameter

    .prologue
    .line 283
    const/4 v1, 0x0

    .line 284
    const/4 v0, 0x0

    .line 285
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x105

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v3, v2

    .line 287
    :try_start_d
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 289
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 290
    const/4 v2, 0x1

    iput-boolean v2, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 291
    const/4 v2, 0x0

    invoke-static {v1, v2, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 292
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 293
    const/4 v2, 0x1

    .line 294
    :goto_25
    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/2addr v5, v6

    int-to-double v5, v5

    .line 295
    const-wide/high16 v7, 0x3ff0

    int-to-double v9, v2

    const-wide/high16 v11, 0x4000

    invoke-static {v9, v10, v11, v12}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v9

    div-double/2addr v7, v9

    .line 294
    mul-double/2addr v5, v7

    .line 295
    mul-int v7, v3, v3

    int-to-double v7, v7

    cmpl-double v5, v5, v7

    if-gtz v5, :cond_62

    .line 298
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 299
    const/4 v3, 0x1

    if-le v2, v3, :cond_65

    .line 302
    add-int/lit8 v2, v2, -0x1

    .line 303
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 304
    iput v2, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 305
    const/4 v2, 0x0

    invoke-static {v1, v2, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 309
    :goto_56
    if-eqz v0, :cond_5c

    .line 311
    invoke-static {v0, p0}, Lcom/android/launcher2/jj;->b(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_5b
    .catchall {:try_start_d .. :try_end_5b} :catchall_73
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_5b} :catch_6a

    move-result-object v0

    .line 317
    :cond_5c
    if-eqz v1, :cond_61

    .line 318
    :try_start_5e
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_61
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_61} :catch_7c

    .line 324
    :cond_61
    :goto_61
    return-object v0

    .line 296
    :cond_62
    add-int/lit8 v2, v2, 0x1

    goto :goto_25

    .line 307
    :cond_65
    :try_start_65
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_73
    .catch Ljava/lang/Exception; {:try_start_65 .. :try_end_68} :catch_6a

    move-result-object v0

    goto :goto_56

    .line 313
    :catch_6a
    move-exception v2

    .line 317
    if-eqz v1, :cond_61

    .line 318
    :try_start_6d
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_70
    .catch Ljava/lang/Exception; {:try_start_6d .. :try_end_70} :catch_71

    goto :goto_61

    .line 321
    :catch_71
    move-exception v1

    goto :goto_61

    .line 315
    :catchall_73
    move-exception v0

    .line 317
    if-eqz v1, :cond_79

    .line 318
    :try_start_76
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_79
    .catch Ljava/lang/Exception; {:try_start_76 .. :try_end_79} :catch_7a

    .line 323
    :cond_79
    :goto_79
    throw v0

    :catch_7a
    move-exception v1

    goto :goto_79

    :catch_7c
    move-exception v1

    goto :goto_61
.end method

.method public static a(Landroid/content/ComponentName;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 169
    if-eqz p0, :cond_22

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    :goto_21
    return-object v0

    :cond_22
    const-string v0, ""

    goto :goto_21
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 82
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    if-nez v0, :cond_e

    .line 85
    const-string v0, ""

    .line 87
    :cond_e
    return-object v0
.end method

.method public static a(Landroid/content/pm/PackageManager;Landroid/content/ComponentName;)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 139
    .line 141
    const/4 v1, 0x0

    :try_start_2
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_5} :catch_15

    move-result-object v1

    .line 145
    :goto_6
    if-eqz v1, :cond_14

    .line 146
    invoke-virtual {v1, p0}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 147
    if-nez v0, :cond_14

    .line 148
    iget-object v0, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 151
    :cond_14
    return-object v0

    :catch_15
    move-exception v1

    move-object v1, v0

    goto :goto_6
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 176
    const-string v0, ""

    .line 177
    instance-of v1, p0, Landroid/content/ComponentName;

    if-eqz v1, :cond_d

    .line 178
    check-cast p0, Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/anddoes/launcher/v;->a(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_c
    :goto_c
    return-object v0

    .line 179
    :cond_d
    instance-of v1, p0, Landroid/content/pm/ResolveInfo;

    if-eqz v1, :cond_37

    .line 180
    check-cast p0, Landroid/content/pm/ResolveInfo;

    .line 181
    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v1, :cond_c

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    .line 184
    :cond_37
    instance-of v1, p0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v1, :cond_c

    .line 185
    check-cast p0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 186
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v0, :cond_48

    .line 187
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-static {v0}, Lcom/anddoes/launcher/v;->a(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    goto :goto_c

    .line 189
    :cond_48
    iget-object v0, p0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    goto :goto_c
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://play.google.com/store/apps/details?id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    const-string v1, "google"

    const-string v2, "amazon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://www.amazon.com/gp/mas/dl/android?p="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_28
    return-object v0
.end method

.method public static a()Z
    .registers 2

    .prologue
    .line 77
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 213
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 215
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 214
    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    .line 216
    :goto_f
    return v0

    .line 214
    :cond_10
    const/4 v0, 0x0

    .line 216
    goto :goto_f
.end method

.method public static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 156
    const/4 v1, 0x0

    :try_start_2
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_7

    .line 157
    const/4 v0, 0x1

    .line 160
    :goto_6
    return v0

    :catch_7
    move-exception v1

    goto :goto_6
.end method

.method public static a(Landroid/content/pm/ResolveInfo;Landroid/content/pm/ResolveInfo;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 196
    if-nez p0, :cond_6

    if-nez p1, :cond_6

    .line 197
    const/4 v0, 0x1

    .line 209
    :goto_5
    return v0

    .line 199
    :cond_6
    if-eqz p0, :cond_a

    if-nez p1, :cond_c

    .line 200
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 202
    :cond_c
    const-string v0, ""

    const-string v1, ""

    .line 203
    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_18

    .line 204
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 206
    :cond_18
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v2, :cond_20

    .line 207
    iget-object v1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 209
    :cond_20
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method public static a(Lcom/android/launcher2/Launcher;J)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 240
    .line 241
    if-nez p0, :cond_5

    .line 259
    :cond_4
    :goto_4
    return v0

    .line 244
    :cond_5
    iget-object v2, p0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v2, v2, Lcom/anddoes/launcher/preference/f;->v:Z

    if-eqz v2, :cond_d

    move v0, v1

    .line 245
    goto :goto_4

    .line 247
    :cond_d
    const-wide/16 v2, -0x64

    cmp-long v2, p1, v2

    if-eqz v2, :cond_19

    .line 248
    const-wide/16 v2, -0x1

    cmp-long v2, p1, v2

    if-nez v2, :cond_4

    .line 249
    :cond_19
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->f()F

    move-result v2

    .line 250
    invoke-virtual {p0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 251
    const v4, 0x7f0a0001

    .line 250
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 251
    const/high16 v3, 0x3f00

    .line 250
    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 252
    invoke-static {}, Lcom/android/launcher2/Launcher;->c()I

    move-result v3

    invoke-virtual {p0, p1, p2, v3}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v3

    .line 253
    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->j()V

    .line 254
    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getCellHeight()I

    move-result v3

    .line 255
    if-ge v3, v2, :cond_4

    move v0, v1

    .line 256
    goto :goto_4
.end method

.method public static a(Ljava/io/File;Ljava/io/File;)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 388
    const/4 v6, 0x0

    .line 389
    if-eqz p0, :cond_6c

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_6c

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_6c

    .line 393
    :try_start_10
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_18
    .catchall {:try_start_10 .. :try_end_18} :catchall_49
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_18} :catch_38

    move-result-object v1

    .line 394
    :try_start_19
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_21
    .catchall {:try_start_19 .. :try_end_21} :catchall_5e
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_21} :catch_6a

    move-result-object v0

    .line 395
    const-wide/16 v2, 0x0

    :try_start_24
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_2b
    .catchall {:try_start_24 .. :try_end_2b} :catchall_64
    .catch Ljava/lang/Exception; {:try_start_24 .. :try_end_2b} :catch_6a

    .line 396
    const/4 v2, 0x1

    .line 401
    if-eqz v1, :cond_31

    .line 402
    :try_start_2e
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 404
    :cond_31
    if-eqz v0, :cond_5a

    .line 405
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_36
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_36} :catch_59

    move v0, v2

    .line 412
    :goto_37
    return v0

    :catch_38
    move-exception v1

    move-object v1, v0

    .line 401
    :goto_3a
    if-eqz v1, :cond_3f

    .line 402
    :try_start_3c
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 404
    :cond_3f
    if-eqz v0, :cond_6c

    .line 405
    invoke-virtual {v0}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_44
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_44} :catch_46

    move v0, v6

    goto :goto_37

    .line 407
    :catch_46
    move-exception v0

    move v0, v6

    goto :goto_37

    .line 399
    :catchall_49
    move-exception v1

    move-object v2, v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    .line 401
    :goto_4e
    if-eqz v2, :cond_53

    .line 402
    :try_start_50
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 404
    :cond_53
    if-eqz v1, :cond_58

    .line 405
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_58
    .catch Ljava/lang/Exception; {:try_start_50 .. :try_end_58} :catch_5c

    .line 410
    :cond_58
    :goto_58
    throw v0

    :catch_59
    move-exception v0

    :cond_5a
    move v0, v2

    goto :goto_37

    :catch_5c
    move-exception v1

    goto :goto_58

    .line 399
    :catchall_5e
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4e

    :catchall_64
    move-exception v2

    move-object v7, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4e

    :catch_6a
    move-exception v2

    goto :goto_3a

    :cond_6c
    move v0, v6

    goto :goto_37
.end method

.method public static a(Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 483
    array-length v2, p1

    move v1, v0

    :goto_3
    if-lt v1, v2, :cond_6

    .line 488
    :goto_5
    return v0

    .line 483
    :cond_6
    aget-object v3, p1, v1

    .line 484
    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 485
    const/4 v0, 0x1

    goto :goto_5

    .line 483
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public static b(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 328
    .line 331
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_8
    .catchall {:try_start_1 .. :try_end_8} :catchall_1d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_8} :catch_13

    move-result-object v1

    .line 332
    :try_start_9
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_2b
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_c} :catch_2d

    move-result-object v0

    .line 337
    if-eqz v1, :cond_12

    .line 338
    :try_start_f
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_12} :catch_29

    .line 344
    :cond_12
    :goto_12
    return-object v0

    :catch_13
    move-exception v1

    move-object v1, v0

    .line 337
    :goto_15
    if-eqz v1, :cond_12

    .line 338
    :try_start_17
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1a} :catch_1b

    goto :goto_12

    .line 341
    :catch_1b
    move-exception v1

    goto :goto_12

    .line 335
    :catchall_1d
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    .line 337
    :goto_21
    if-eqz v1, :cond_26

    .line 338
    :try_start_23
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_26} :catch_27

    .line 343
    :cond_26
    :goto_26
    throw v0

    :catch_27
    move-exception v1

    goto :goto_26

    :catch_29
    move-exception v1

    goto :goto_12

    .line 335
    :catchall_2b
    move-exception v0

    goto :goto_21

    :catch_2d
    move-exception v2

    goto :goto_15
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 108
    invoke-static {}, Lcom/anddoes/launcher/v;->c()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    .line 109
    if-eqz v2, :cond_1b

    .line 111
    :try_start_f
    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 112
    const-string v4, "android"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 120
    :cond_1b
    :goto_1b
    return-object v0

    .line 115
    :cond_1c
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_28
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_f .. :try_end_28} :catch_2a

    move-result-object v0

    goto :goto_1b

    :catch_2a
    move-exception v1

    goto :goto_1b
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 463
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 464
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3d

    const-string v0, "_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_35

    new-instance v0, Ljava/util/Locale;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-direct {v0, v3, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_23
    iput-object v0, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 465
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 468
    :goto_34
    return-void

    .line 464
    :cond_35
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    goto :goto_23

    .line 468
    :catch_3b
    move-exception v0

    goto :goto_34

    .line 464
    :cond_3d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;
    :try_end_40
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_40} :catch_3b

    move-result-object v0

    goto :goto_23
.end method

.method public static b()Z
    .registers 2

    .prologue
    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public static c()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 100
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    return-object v0
.end method

.method public static c(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 263
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/LauncherApplication;->e:Z

    .line 264
    return-void
.end method

.method public static d()I
    .registers 2

    .prologue
    .line 236
    const/high16 v0, 0x3f80

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->f()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static d(Landroid/content/Context;)Landroid/net/Uri;
    .registers 4
    .parameter

    .prologue
    .line 372
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "images/folder_bg.png"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_1d

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_1f

    .line 374
    :cond_1d
    const/4 v0, 0x0

    .line 376
    :goto_1e
    return-object v0

    :cond_1f
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1e
.end method

.method public static e(Landroid/content/Context;)I
    .registers 5
    .parameter

    .prologue
    const/16 v0, 0x140

    .line 492
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 493
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 494
    const/high16 v3, 0x7f09

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 495
    sparse-switch v1, :sswitch_data_2e

    .line 511
    int-to-float v0, v1

    const/high16 v1, 0x3fc0

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 514
    :goto_1f
    :sswitch_1f
    return v0

    .line 497
    :sswitch_20
    const/16 v0, 0xa0

    goto :goto_1f

    .line 499
    :sswitch_23
    const/16 v0, 0xf0

    goto :goto_1f

    .line 505
    :sswitch_26
    const/16 v0, 0x1e0

    goto :goto_1f

    .line 507
    :sswitch_29
    const/16 v0, 0x280

    goto :goto_1f

    :cond_2c
    move v0, v1

    .line 514
    goto :goto_1f

    .line 495
    :sswitch_data_2e
    .sparse-switch
        0x78 -> :sswitch_20
        0xa0 -> :sswitch_23
        0xd5 -> :sswitch_1f
        0xf0 -> :sswitch_1f
        0x140 -> :sswitch_26
        0x1e0 -> :sswitch_29
    .end sparse-switch
.end method

.method public static e()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 418
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    const-string v2, "su"

    invoke-virtual {v1, v2}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    .line 419
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 420
    new-instance v3, Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 421
    const-string v4, "id\n"

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 425
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 427
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 428
    if-nez v3, :cond_2c

    .line 443
    :cond_2b
    :goto_2b
    return v0

    .line 431
    :cond_2c
    const-string v4, "exit\n"

    invoke-virtual {v2, v4}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 433
    const-string v2, "uid=0"

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 436
    invoke-virtual {v1}, Ljava/lang/Process;->waitFor()I

    .line 437
    invoke-virtual {v1}, Ljava/lang/Process;->exitValue()I
    :try_end_42
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_42} :catch_49

    move-result v1

    const/16 v2, 0xff

    if-eq v1, v2, :cond_2b

    .line 443
    const/4 v0, 0x1

    goto :goto_2b

    .line 441
    :catch_49
    move-exception v1

    goto :goto_2b
.end method
