.class public final Lcom/android/launcher2/hu;
.super Lcom/android/launcher2/ih;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/PagedView;


# direct methods
.method protected constructor <init>(Lcom/android/launcher2/PagedView;)V
    .registers 2
    .parameter

    .prologue
    .line 2919
    iput-object p1, p0, Lcom/android/launcher2/hu;->a:Lcom/android/launcher2/PagedView;

    invoke-direct {p0, p1}, Lcom/android/launcher2/ih;-><init>(Lcom/android/launcher2/PagedView;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;F)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/high16 v3, 0x3f80

    .line 2922
    iget-object v0, p0, Lcom/android/launcher2/hu;->a:Lcom/android/launcher2/PagedView;

    iget-object v1, p0, Lcom/android/launcher2/hu;->a:Lcom/android/launcher2/PagedView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/hu;->a:Lcom/android/launcher2/PagedView;

    iget v1, v1, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v0, v1

    .line 2923
    const/4 v1, 0x0

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_27

    .line 2924
    sub-float v1, v3, p2

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 2928
    :goto_1e
    int-to-float v0, v0

    mul-float/2addr v0, p2

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2929
    return-void

    .line 2926
    :cond_27
    add-float v1, v3, p2

    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    goto :goto_1e
.end method
