.class public final Lcom/android/launcher2/hw;
.super Lcom/android/launcher2/ih;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/PagedView;


# direct methods
.method protected constructor <init>(Lcom/android/launcher2/PagedView;)V
    .registers 2
    .parameter

    .prologue
    .line 2948
    iput-object p1, p0, Lcom/android/launcher2/hw;->a:Lcom/android/launcher2/PagedView;

    invoke-direct {p0, p1}, Lcom/android/launcher2/ih;-><init>(Lcom/android/launcher2/PagedView;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;F)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2951
    const/high16 v0, 0x42b4

    mul-float v1, v0, p2

    .line 2952
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_27

    const/4 v0, 0x0

    :goto_a
    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotX(F)V

    .line 2953
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x3f00

    mul-float/2addr v0, v2

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotY(F)V

    .line 2954
    invoke-virtual {p1, v1}, Landroid/view/View;->setRotationY(F)V

    .line 2955
    iget-object v0, p0, Lcom/android/launcher2/hw;->a:Lcom/android/launcher2/PagedView;

    iget v0, v0, Lcom/android/launcher2/PagedView;->q:F

    sget v1, Lcom/android/launcher2/PagedView;->ay:F

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setCameraDistance(F)V

    .line 2956
    return-void

    .line 2952
    :cond_27
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    goto :goto_a
.end method
