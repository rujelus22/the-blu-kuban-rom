.class final Lcom/android/launcher2/cr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/co;

.field private final synthetic b:I


# direct methods
.method constructor <init>(Lcom/android/launcher2/co;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/cr;->a:Lcom/android/launcher2/co;

    iput p2, p0, Lcom/android/launcher2/cr;->b:I

    .line 281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 283
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 284
    iget-object v1, p0, Lcom/android/launcher2/cr;->a:Lcom/android/launcher2/co;

    sub-float v2, v4, v0

    const v3, 0x3e99999a

    mul-float/2addr v2, v3

    add-float/2addr v2, v4

    iget v3, p0, Lcom/android/launcher2/cr;->b:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iput v2, v1, Lcom/android/launcher2/co;->c:F

    .line 285
    iget-object v1, p0, Lcom/android/launcher2/cr;->a:Lcom/android/launcher2/co;

    sub-float v0, v4, v0

    const v2, 0x3e19999a

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    iget v2, p0, Lcom/android/launcher2/cr;->b:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    iput v0, v1, Lcom/android/launcher2/co;->d:F

    .line 286
    iget-object v0, p0, Lcom/android/launcher2/cr;->a:Lcom/android/launcher2/co;

    invoke-static {v0}, Lcom/android/launcher2/co;->a(Lcom/android/launcher2/co;)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    if-eqz v0, :cond_3b

    .line 287
    iget-object v0, p0, Lcom/android/launcher2/cr;->a:Lcom/android/launcher2/co;

    invoke-static {v0}, Lcom/android/launcher2/co;->a(Lcom/android/launcher2/co;)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->invalidate()V

    .line 289
    :cond_3b
    return-void
.end method
