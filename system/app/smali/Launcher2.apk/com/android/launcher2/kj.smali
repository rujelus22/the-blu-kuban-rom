.class public final Lcom/android/launcher2/kj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field private final a:Lcom/android/launcher2/ke;

.field private final b:Landroid/view/animation/DecelerateInterpolator;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 1782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783
    new-instance v0, Lcom/android/launcher2/ke;

    invoke-direct {v0}, Lcom/android/launcher2/ke;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/kj;->a:Lcom/android/launcher2/ke;

    .line 1784
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x4040

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/kj;->b:Landroid/view/animation/DecelerateInterpolator;

    .line 1782
    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .registers 4
    .parameter

    .prologue
    .line 1787
    iget-object v0, p0, Lcom/android/launcher2/kj;->b:Landroid/view/animation/DecelerateInterpolator;

    iget-object v1, p0, Lcom/android/launcher2/kj;->a:Lcom/android/launcher2/ke;

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ke;->getInterpolation(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method
