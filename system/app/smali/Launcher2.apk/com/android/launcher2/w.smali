.class final Lcom/android/launcher2/w;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/u;


# direct methods
.method constructor <init>(Lcom/android/launcher2/u;)V
    .registers 2
    .parameter

    .prologue
    .line 2430
    iput-object p1, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    .line 2435
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v0, v0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v0, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_1d

    .line 2436
    iget-object v0, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v0, v0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v1, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    monitor-enter v1
    :try_end_13
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_13} :catch_73

    .line 2437
    :try_start_13
    iget-object v0, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v0, v0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v0, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    .line 2436
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_13 .. :try_end_1d} :catchall_70

    .line 2440
    :cond_1d
    :try_start_1d
    iget-object v0, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v0, v0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v0, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-eqz v0, :cond_69

    .line 2442
    iget-object v0, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v0, v0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v1, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    monitor-enter v1
    :try_end_30
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_30} :catch_73

    .line 2443
    :try_start_30
    iget-object v0, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v0, v0, Lcom/android/launcher2/u;->b:Lcom/android/launcher2/x;

    iget-object v0, v0, Lcom/android/launcher2/x;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/y;

    .line 2442
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_30 .. :try_end_3d} :catchall_75

    .line 2445
    :try_start_3d
    iget-object v1, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v2, v0, Lcom/android/launcher2/y;->c:Ljava/lang/Object;

    iget-object v3, v0, Lcom/android/launcher2/y;->d:Lcom/android/launcher2/af;

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher2/u;->a(Ljava/lang/Object;Lcom/android/launcher2/af;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2446
    if-eqz v1, :cond_69

    .line 2447
    new-instance v2, Ljava/lang/ref/SoftReference;

    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 2448
    iget-object v3, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    iget-object v3, v3, Lcom/android/launcher2/u;->a:Ljava/util/HashMap;

    iget-object v4, v0, Lcom/android/launcher2/y;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2449
    new-instance v2, Lcom/android/launcher2/v;

    iget-object v3, p0, Lcom/android/launcher2/w;->a:Lcom/android/launcher2/u;

    invoke-direct {v2, v3, v0, v1}, Lcom/android/launcher2/v;-><init>(Lcom/android/launcher2/u;Lcom/android/launcher2/y;Landroid/graphics/Bitmap;)V

    .line 2450
    iget-object v0, v0, Lcom/android/launcher2/y;->b:Lcom/android/launcher2/PagedViewWidget;

    invoke-virtual {v0}, Lcom/android/launcher2/PagedViewWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2451
    invoke-virtual {v0, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2454
    :cond_69
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2461
    :goto_6f
    return-void

    .line 2436
    :catchall_70
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2461
    :catch_73
    move-exception v0

    goto :goto_6f

    .line 2442
    :catchall_75
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_78
    .catch Ljava/lang/InterruptedException; {:try_start_3d .. :try_end_78} :catch_73
.end method
