.class public Lcom/android/launcher2/FocusOnlyTabWidget;
.super Landroid/widget/TabWidget;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/TabWidget;-><init>(Landroid/content/Context;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/TabWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TabWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .registers 5
    .parameter

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getTabCount()I

    move-result v1

    .line 50
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_9

    .line 55
    const/4 v0, -0x1

    :cond_8
    return v0

    .line 51
    :cond_9
    invoke-virtual {p0, v0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v2, p1, :cond_8

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method public getSelectedTab()Landroid/view/View;
    .registers 5

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getTabCount()I

    move-result v2

    .line 39
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-lt v1, v2, :cond_a

    .line 45
    const/4 v0, 0x0

    :cond_9
    return-object v0

    .line 40
    :cond_a
    invoke-virtual {p0, v1}, Lcom/android/launcher2/FocusOnlyTabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-nez v3, :cond_9

    .line 39
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 82
    if-ne p1, p0, :cond_11

    if-eqz p2, :cond_11

    invoke-virtual {p0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getTabCount()I

    move-result v0

    if-lez v0, :cond_11

    .line 83
    invoke-virtual {p0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getSelectedTab()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 86
    :cond_11
    return-void
.end method

.method protected onMeasure(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 90
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-eqz v1, :cond_b

    .line 91
    invoke-super {p0, p1, p2}, Landroid/widget/TabWidget;->onMeasure(II)V

    .line 102
    :goto_a
    return-void

    :cond_b
    move v1, v0

    move v2, v0

    .line 94
    :goto_d
    invoke-virtual {p0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getChildCount()I

    move-result v3

    if-lt v0, v3, :cond_17

    .line 100
    invoke-virtual {p0, v2, v1}, Lcom/android/launcher2/FocusOnlyTabWidget;->setMeasuredDimension(II)V

    goto :goto_a

    .line 95
    :cond_17
    invoke-virtual {p0, v0}, Lcom/android/launcher2/FocusOnlyTabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 96
    invoke-virtual {v1, p1, p2}, Landroid/view/View;->measure(II)V

    .line 97
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 98
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_d
.end method
