.class public Lcom/android/launcher2/AppsCustomizePagedView;
.super Lcom/android/launcher2/is;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lcom/android/launcher2/bt;
.implements Lcom/android/launcher2/hn;
.implements Lcom/android/launcher2/ip;
.implements Lcom/android/launcher2/ir;


# static fields
.field private static ba:F

.field private static bn:J


# instance fields
.field a:Lcom/android/launcher2/ki;

.field private aG:Lcom/android/launcher2/Launcher;

.field private aH:Lcom/android/launcher2/bk;

.field private final aI:Landroid/view/LayoutInflater;

.field private final aJ:Landroid/content/pm/PackageManager;

.field private aK:I

.field private aL:Lcom/android/launcher2/PagedViewIcon;

.field private aM:Ljava/util/ArrayList;

.field private aN:Ljava/util/ArrayList;

.field private aO:Landroid/graphics/Canvas;

.field private aP:Lcom/android/launcher2/da;

.field private aQ:I

.field private aR:I

.field private aS:I

.field private aT:I

.field private aU:I

.field private aV:I

.field private aW:I

.field private aX:I

.field private final aY:F

.field private aZ:Lcom/android/launcher2/ik;

.field b:Ljava/util/ArrayList;

.field private bb:Ljava/lang/Runnable;

.field private bc:Ljava/lang/Runnable;

.field private bd:Z

.field private be:Z

.field private bf:Ljava/util/ArrayList;

.field private bg:Ljava/util/ArrayList;

.field private bh:Lcom/android/launcher2/AppsCustomizeTabHost;

.field private bi:Lcom/anddoes/launcher/p;

.field private bj:[Ljava/lang/String;

.field private bk:J

.field private bl:F

.field private bm:F

.field c:I

.field d:I

.field e:Lcom/android/launcher2/iw;

.field f:Lcom/android/launcher2/ah;

.field g:Lcom/android/launcher2/it;

.field h:Lcom/android/launcher2/al;

.field i:Lcom/android/launcher2/al;

.field j:Lcom/android/launcher2/iy;

.field k:Lcom/android/launcher2/iy;

.field l:Lcom/android/launcher2/it;

.field m:Lcom/android/launcher2/u;

.field protected n:F

.field protected o:F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 298
    const v0, 0x45cb2000

    sput v0, Lcom/android/launcher2/AppsCustomizePagedView;->ba:F

    .line 1937
    const-wide/16 v0, 0x190

    sput-wide v0, Lcom/android/launcher2/AppsCustomizePagedView;->bn:J

    .line 252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 341
    invoke-direct {p0, p1, p2}, Lcom/android/launcher2/is;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 273
    iput v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    .line 291
    const/high16 v0, 0x3e80

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aY:F

    .line 297
    new-instance v0, Lcom/android/launcher2/ki;

    const/high16 v1, 0x3f00

    invoke-direct {v0, v1}, Lcom/android/launcher2/ki;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->a:Lcom/android/launcher2/ki;

    .line 310
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bb:Ljava/lang/Runnable;

    .line 311
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bc:Ljava/lang/Runnable;

    .line 316
    iput v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    .line 317
    iput v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    .line 318
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    .line 319
    iput-boolean v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bd:Z

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bf:Ljava/util/ArrayList;

    .line 326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bg:Ljava/util/ArrayList;

    .line 329
    new-instance v0, Lcom/android/launcher2/ah;

    invoke-direct {v0}, Lcom/android/launcher2/ah;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->f:Lcom/android/launcher2/ah;

    .line 330
    new-instance v0, Lcom/android/launcher2/it;

    invoke-direct {v0}, Lcom/android/launcher2/it;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->g:Lcom/android/launcher2/it;

    .line 331
    new-instance v0, Lcom/android/launcher2/al;

    invoke-direct {v0}, Lcom/android/launcher2/al;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->h:Lcom/android/launcher2/al;

    .line 334
    new-instance v0, Lcom/android/launcher2/al;

    invoke-direct {v0}, Lcom/android/launcher2/al;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->i:Lcom/android/launcher2/al;

    .line 335
    new-instance v0, Lcom/android/launcher2/iy;

    invoke-direct {v0}, Lcom/android/launcher2/iy;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->j:Lcom/android/launcher2/iy;

    .line 336
    new-instance v0, Lcom/android/launcher2/iy;

    invoke-direct {v0}, Lcom/android/launcher2/iy;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->k:Lcom/android/launcher2/iy;

    .line 337
    new-instance v0, Lcom/android/launcher2/it;

    invoke-direct {v0}, Lcom/android/launcher2/it;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->l:Lcom/android/launcher2/it;

    .line 1933
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bj:[Ljava/lang/String;

    .line 1934
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bk:J

    .line 2039
    const v0, 0x3f060a92

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->n:F

    .line 2040
    const v0, 0x3f860a92

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->o:F

    .line 342
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aI:Landroid/view/LayoutInflater;

    .line 343
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    .line 344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    .line 345
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    .line 346
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->a()Lcom/android/launcher2/da;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aP:Lcom/android/launcher2/da;

    .line 347
    new-instance v0, Lcom/android/launcher2/u;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/u;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;B)V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->m:Lcom/android/launcher2/u;

    .line 348
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aO:Landroid/graphics/Canvas;

    .line 349
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    .line 352
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 354
    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aR:I

    .line 356
    sget-object v0, Lcom/anddoes/launcher/at;->AppsCustomizePagedView:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 357
    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aS:I

    .line 358
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aT:I

    .line 360
    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 359
    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aW:I

    .line 362
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 361
    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aX:I

    .line 363
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    .line 364
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    .line 365
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 366
    new-instance v0, Lcom/android/launcher2/ik;

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/launcher2/ik;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    .line 370
    iput-boolean v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ac:Z

    .line 376
    return-void
.end method

.method private E()V
    .registers 6

    .prologue
    .line 992
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 993
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_17

    .line 1005
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1006
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1007
    return-void

    .line 994
    :cond_17
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/i;

    .line 995
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/i;->cancel(Z)Z

    .line 996
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 997
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aa:Ljava/util/ArrayList;

    iget v3, v0, Lcom/android/launcher2/i;->b:I

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1000
    iget v0, v0, Lcom/android/launcher2/i;->b:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1001
    instance-of v2, v0, Lcom/android/launcher2/in;

    if-eqz v2, :cond_6

    .line 1002
    check-cast v0, Lcom/android/launcher2/in;

    invoke-virtual {v0}, Lcom/android/launcher2/in;->a()V

    goto :goto_6
.end method

.method private F()V
    .registers 2

    .prologue
    .line 1687
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->r()Z

    move-result v0

    if-nez v0, :cond_a

    .line 1690
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->requestLayout()V

    .line 1695
    :goto_9
    return-void

    .line 1692
    :cond_a
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->E()V

    .line 1693
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->A()V

    goto :goto_9
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    return-object v0
.end method

.method private a(Landroid/view/View;Ljava/lang/Object;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewWidget;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2520
    .line 2522
    if-eqz p1, :cond_74

    instance-of v0, p1, Lcom/android/launcher2/PagedViewWidget;

    if-eqz v0, :cond_74

    .line 2523
    check-cast p1, Lcom/android/launcher2/PagedViewWidget;

    .line 2524
    invoke-virtual {p1}, Lcom/android/launcher2/PagedViewWidget;->a()V

    .line 2529
    :goto_d
    instance-of v0, p2, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v0, :cond_83

    .line 2531
    check-cast p2, Landroid/appwidget/AppWidgetProviderInfo;

    .line 2532
    new-instance v1, Lcom/android/launcher2/iw;

    invoke-direct {v1, p2}, Lcom/android/launcher2/iw;-><init>(Landroid/appwidget/AppWidgetProviderInfo;)V

    .line 2534
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    iget-object v4, p2, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v4, :cond_81

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_81

    const-string v0, "QuickSearchBar"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_81

    move v0, v2

    :goto_39
    if-eqz v0, :cond_42

    move-object v0, v1

    .line 2535
    check-cast v0, Lcom/android/launcher2/iw;

    .line 2536
    const/16 v4, 0x3e9

    iput v4, v0, Lcom/android/launcher2/iw;->i:I

    .line 2540
    :cond_42
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-static {v0, p2}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v0

    .line 2541
    aget v4, v0, v3

    iput v4, v1, Lcom/android/launcher2/iu;->n:I

    .line 2542
    aget v4, v0, v2

    iput v4, v1, Lcom/android/launcher2/iu;->o:I

    .line 2543
    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-static {v4, p2}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v4

    .line 2544
    aget v3, v4, v3

    iput v3, v1, Lcom/android/launcher2/iu;->p:I

    .line 2545
    aget v2, v4, v2

    iput v2, v1, Lcom/android/launcher2/iu;->q:I

    .line 2547
    invoke-virtual {p1, p2, v0}, Lcom/android/launcher2/PagedViewWidget;->a(Landroid/appwidget/AppWidgetProviderInfo;[I)V

    .line 2548
    invoke-virtual {p1, v1}, Lcom/android/launcher2/PagedViewWidget;->setTag(Ljava/lang/Object;)V

    .line 2549
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewWidget;->setShortPressListener(Lcom/android/launcher2/ir;)V

    .line 2561
    :cond_67
    :goto_67
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewWidget;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2562
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewWidget;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2563
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewWidget;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2564
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewWidget;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2565
    return-object p1

    .line 2526
    :cond_74
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aI:Landroid/view/LayoutInflater;

    .line 2527
    const v1, 0x7f03000c

    .line 2526
    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedViewWidget;

    move-object p1, v0

    goto :goto_d

    :cond_81
    move v0, v3

    .line 2534
    goto :goto_39

    .line 2550
    :cond_83
    instance-of v0, p2, Landroid/content/pm/ResolveInfo;

    if-eqz v0, :cond_67

    .line 2552
    check-cast p2, Landroid/content/pm/ResolveInfo;

    .line 2553
    new-instance v0, Lcom/android/launcher2/iv;

    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-direct {v0, v1}, Lcom/android/launcher2/iv;-><init>(Landroid/content/pm/ActivityInfo;)V

    .line 2554
    iput v2, v0, Lcom/android/launcher2/iu;->i:I

    .line 2555
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 2556
    iget-object v3, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2555
    iput-object v1, v0, Lcom/android/launcher2/iu;->a:Landroid/content/ComponentName;

    .line 2557
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    invoke-virtual {p1, v1, p2}, Lcom/android/launcher2/PagedViewWidget;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)V

    .line 2558
    invoke-virtual {p1, v0}, Lcom/android/launcher2/PagedViewWidget;->setTag(Ljava/lang/Object;)V

    .line 2559
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/launcher2/PagedViewWidget;->setShortPressListener(Lcom/android/launcher2/ir;)V

    goto :goto_67
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizePagedView;Landroid/view/View;Ljava/lang/Object;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewWidget;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2519
    invoke-direct {p0, p1, p2, p3}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/View;Ljava/lang/Object;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewWidget;

    move-result-object v0

    return-object v0
.end method

.method private a(FFIIF)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2108
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->G:I

    int-to-float v2, v2

    mul-float/2addr v2, p5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 2109
    if-le p3, v3, :cond_39

    move v2, v1

    .line 2110
    :goto_d
    if-le p4, v3, :cond_10

    move v0, v1

    .line 2111
    :cond_10
    iget-boolean v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ap:Z

    if-eqz v3, :cond_3b

    if-eqz v0, :cond_38

    .line 2113
    :goto_16
    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->C:I

    .line 2114
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->B:F

    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ap:Z

    if-eqz v0, :cond_3e

    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->A:F

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    :goto_25
    add-float/2addr v0, v2

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->B:F

    .line 2115
    iput p1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->y:F

    .line 2116
    iput p2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->A:F

    .line 2117
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->z:F

    .line 2119
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    if-le v0, v1, :cond_38

    .line 2120
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->u()V

    .line 2122
    :cond_38
    return-void

    :cond_39
    move v2, v0

    .line 2109
    goto :goto_d

    .line 2111
    :cond_3b
    if-eqz v2, :cond_38

    goto :goto_16

    .line 2114
    :cond_3e
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->y:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    goto :goto_25
.end method

.method private a(IILcom/anddoes/launcher/p;Z)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1362
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    mul-int/2addr v0, v1

    .line 1363
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v2, "VERTICAL_CONTINUOUS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1364
    iget-object v0, p3, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1367
    :cond_19
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1368
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v1}, Lcom/android/launcher2/ik;->getContentWidth()I

    move-result v1

    .line 1369
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    sub-int/2addr v1, v2

    .line 1370
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aW:I

    mul-int/2addr v2, v3

    .line 1369
    sub-int/2addr v1, v2

    .line 1370
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    .line 1369
    div-int v2, v1, v2

    .line 1371
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v1}, Lcom/android/launcher2/ik;->getContentHeight()I

    move-result v1

    .line 1372
    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    sub-int/2addr v1, v3

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    sub-int/2addr v1, v3

    .line 1373
    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aX:I

    mul-int/2addr v3, v4

    .line 1372
    sub-int/2addr v1, v3

    .line 1373
    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    .line 1372
    div-int v3, v1, v3

    .line 1376
    sub-int v1, p2, p1

    mul-int v4, v1, v0

    move v1, v4

    .line 1377
    :goto_53
    add-int v5, v4, v0

    iget-object v6, p3, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-lt v1, v5, :cond_a7

    .line 1382
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->I:Z

    if-nez v0, :cond_13c

    .line 1383
    sub-int v6, p2, p1

    .line 1387
    :goto_6b
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v1, "VERTICAL_CONTINUOUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 1388
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1389
    const/4 v0, 0x0

    :goto_7e
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    if-lt v0, v4, :cond_b3

    .line 1392
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    rem-int/2addr v1, v0

    .line 1393
    if-lez v1, :cond_8d

    .line 1394
    const/4 v0, 0x0

    :goto_88
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    sub-int/2addr v4, v1

    if-lt v0, v4, :cond_c4

    .line 1398
    :cond_8d
    const/4 v0, 0x0

    :goto_8e
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    if-lt v0, v1, :cond_cf

    .line 1401
    invoke-virtual {p0, v6}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jk;

    .line 1402
    new-instance v1, Lcom/android/launcher2/s;

    invoke-direct {v1, p0, v7, v2, v3}, Lcom/android/launcher2/s;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;Ljava/util/ArrayList;II)V

    .line 1403
    invoke-virtual {v0}, Lcom/android/launcher2/jk;->a()V

    .line 1404
    invoke-virtual {v0, v1}, Lcom/android/launcher2/jk;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1405
    invoke-virtual {v0}, Lcom/android/launcher2/jk;->b()V

    .line 1460
    :goto_a6
    return-void

    .line 1378
    :cond_a7
    iget-object v5, p3, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1377
    add-int/lit8 v1, v1, 0x1

    goto :goto_53

    .line 1390
    :cond_b3
    const/4 v4, 0x0

    new-instance v5, Lcom/android/launcher2/t;

    iget v8, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    iget v9, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aX:I

    sub-int/2addr v8, v9

    invoke-direct {v5, p0, v8}, Lcom/android/launcher2/t;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    invoke-virtual {v7, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1389
    add-int/lit8 v0, v0, 0x1

    goto :goto_7e

    .line 1395
    :cond_c4
    new-instance v4, Lcom/android/launcher2/t;

    invoke-direct {v4, p0, v3}, Lcom/android/launcher2/t;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1394
    add-int/lit8 v0, v0, 0x1

    goto :goto_88

    .line 1399
    :cond_cf
    new-instance v1, Lcom/android/launcher2/t;

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aX:I

    sub-int/2addr v4, v5

    invoke-direct {v1, p0, v4}, Lcom/android/launcher2/t;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1398
    add-int/lit8 v0, v0, 0x1

    goto :goto_8e

    .line 1407
    :cond_df
    invoke-virtual {p0, v6}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/in;

    .line 1410
    invoke-virtual {v4}, Lcom/android/launcher2/in;->getCellCountX()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/android/launcher2/in;->setColumnCount(I)V

    .line 1411
    const/4 v0, 0x0

    :goto_ed
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_ff

    .line 1433
    new-instance v0, Lcom/android/launcher2/q;

    move-object v1, p0

    move/from16 v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher2/q;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;IILcom/android/launcher2/in;ZILjava/util/ArrayList;)V

    invoke-virtual {v4, v0}, Lcom/android/launcher2/in;->setOnLayoutListener(Ljava/lang/Runnable;)V

    goto :goto_a6

    .line 1412
    :cond_ff
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1413
    const/4 v5, 0x0

    invoke-direct {p0, v5, v1, v4}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/View;Ljava/lang/Object;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewWidget;

    move-result-object v1

    .line 1416
    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    rem-int v5, v0, v5

    .line 1417
    iget v8, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    div-int v8, v0, v8

    .line 1418
    new-instance v9, Landroid/widget/GridLayout$LayoutParams;

    .line 1419
    sget-object v10, Landroid/widget/GridLayout;->LEFT:Landroid/widget/GridLayout$Alignment;

    invoke-static {v8, v10}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v10

    .line 1420
    sget-object v11, Landroid/widget/GridLayout;->TOP:Landroid/widget/GridLayout$Alignment;

    invoke-static {v5, v11}, Landroid/widget/GridLayout;->spec(ILandroid/widget/GridLayout$Alignment;)Landroid/widget/GridLayout$Spec;

    move-result-object v11

    .line 1418
    invoke-direct {v9, v10, v11}, Landroid/widget/GridLayout$LayoutParams;-><init>(Landroid/widget/GridLayout$Spec;Landroid/widget/GridLayout$Spec;)V

    .line 1421
    iput v2, v9, Landroid/widget/GridLayout$LayoutParams;->width:I

    .line 1422
    iput v3, v9, Landroid/widget/GridLayout$LayoutParams;->height:I

    .line 1423
    const/16 v10, 0x33

    invoke-virtual {v9, v10}, Landroid/widget/GridLayout$LayoutParams;->setGravity(I)V

    .line 1424
    if-lez v5, :cond_130

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aW:I

    iput v5, v9, Landroid/widget/GridLayout$LayoutParams;->leftMargin:I

    .line 1425
    :cond_130
    if-lez v8, :cond_136

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aX:I

    iput v5, v9, Landroid/widget/GridLayout$LayoutParams;->topMargin:I

    .line 1426
    :cond_136
    invoke-virtual {v4, v1, v9}, Lcom/android/launcher2/in;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1411
    add-int/lit8 v0, v0, 0x1

    goto :goto_ed

    :cond_13c
    move v6, p2

    goto/16 :goto_6b
.end method

.method static synthetic a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1344
    invoke-static/range {p0 .. p5}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    return-void
.end method

.method private a(Landroid/view/View;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 882
    if-nez p2, :cond_10

    if-eqz p3, :cond_10

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-eq p1, v0, :cond_15

    .line 883
    instance-of v0, p1, Lcom/android/launcher2/DeleteDropTarget;

    if-nez v0, :cond_15

    .line 886
    :cond_10
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->p()V

    .line 888
    :cond_15
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->d(Z)V

    .line 889
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1108
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 1109
    const/4 v0, 0x0

    :goto_5
    if-lt v0, v1, :cond_8

    .line 1112
    return-void

    .line 1110
    :cond_8
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1109
    add-int/lit8 v0, v0, 0x1

    goto :goto_5
.end method

.method private a(Lcom/anddoes/launcher/p;)V
    .registers 11
    .parameter

    .prologue
    const/high16 v8, -0x8000

    const/4 v2, 0x0

    const/4 v7, -0x1

    const/high16 v3, 0x3f80

    .line 2005
    iget-boolean v0, p1, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v0, :cond_23

    iget-object v0, p1, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    :goto_1d
    move v1, v2

    .line 2007
    :goto_1e
    iget v3, p1, Lcom/anddoes/launcher/p;->h:I

    if-lt v1, v3, :cond_37

    .line 2029
    return-void

    .line 2005
    :cond_23
    iget-object v0, p1, Lcom/anddoes/launcher/p;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_1d

    .line 2008
    :cond_37
    const-string v3, "VERTICAL_CONTINUOUS"

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_93

    .line 2009
    new-instance v3, Lcom/android/launcher2/jk;

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-direct {v3, v4}, Lcom/android/launcher2/jk;-><init>(Landroid/content/Context;)V

    .line 2010
    iget-boolean v4, p1, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v4, :cond_87

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->M:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/launcher2/jk;->a(III)V

    :goto_5b
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    invoke-virtual {v3, v4, v2, v5, v2}, Lcom/android/launcher2/jk;->setPadding(IIII)V

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v4

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v5

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getPageContentWidth()I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/android/launcher2/jk;->setMinimumWidth(I)V

    invoke-virtual {v3, v4, v5}, Lcom/android/launcher2/jk;->measure(II)V

    .line 2011
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    .line 2012
    invoke-direct {v4, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2011
    invoke-virtual {p0, v3, v4}, Lcom/android/launcher2/AppsCustomizePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2007
    :goto_84
    add-int/lit8 v1, v1, 0x1

    goto :goto_1e

    .line 2010
    :cond_87
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aW:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aX:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/android/launcher2/jk;->a(III)V

    goto :goto_5b

    .line 2014
    :cond_93
    iget-boolean v3, p1, Lcom/anddoes/launcher/p;->c:Z

    if-nez v3, :cond_ae

    .line 2015
    new-instance v3, Lcom/android/launcher2/in;

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    .line 2016
    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    .line 2015
    invoke-direct {v3, v4, v5, v6}, Lcom/android/launcher2/in;-><init>(Landroid/content/Context;II)V

    .line 2017
    invoke-direct {p0, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->setupPage(Lcom/android/launcher2/in;)V

    .line 2018
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    .line 2019
    invoke-direct {v4, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2018
    invoke-virtual {p0, v3, v4}, Lcom/android/launcher2/AppsCustomizePagedView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_84

    .line 2021
    :cond_ae
    new-instance v3, Lcom/android/launcher2/ik;

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    .line 2022
    const-string v5, "VERTICAL_CONTINUOUS"

    iget-object v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v6, v6, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v6, v6, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 2021
    invoke-direct {v3, v4, v5, v2}, Lcom/android/launcher2/ik;-><init>(Landroid/content/Context;ZB)V

    .line 2023
    iput v0, v3, Lcom/android/launcher2/ik;->i:I

    .line 2024
    invoke-direct {p0, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->setupPage(Lcom/android/launcher2/ik;)V

    .line 2025
    invoke-virtual {p0, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->addView(Landroid/view/View;)V

    goto :goto_84
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizePagedView;ILjava/util/ArrayList;II)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 1258
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_48

    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->g(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0xc8

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-instance v0, Lcom/android/launcher2/af;

    new-instance v5, Lcom/android/launcher2/o;

    invoke-direct {v5, p0, v1}, Lcom/android/launcher2/o;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    new-instance v6, Lcom/android/launcher2/p;

    invoke-direct {v6, p0}, Lcom/android/launcher2/p;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;)V

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/android/launcher2/af;-><init>(ILjava/util/ArrayList;IILcom/android/launcher2/ae;Lcom/android/launcher2/ae;)V

    new-instance v1, Lcom/android/launcher2/i;

    sget-object v2, Lcom/android/launcher2/ag;->a:Lcom/android/launcher2/ag;

    invoke-direct {v1, p1, v2}, Lcom/android/launcher2/i;-><init>(ILcom/android/launcher2/ag;)V

    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->h(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher2/i;->a(I)V

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/android/launcher2/af;

    aput-object v0, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher2/i;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_48
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/i;

    iget v2, v0, Lcom/android/launcher2/i;->b:I

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    invoke-virtual {p0, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->d(I)I

    move-result v3

    if-lt v2, v3, :cond_60

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    invoke-virtual {p0, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->e(I)I

    move-result v3

    if-le v2, v3, :cond_67

    :cond_60
    invoke-virtual {v0, v7}, Lcom/android/launcher2/i;->cancel(Z)Z

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_7

    :cond_67
    invoke-direct {p0, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->h(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/launcher2/i;->a(I)V

    goto :goto_7
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizePagedView;Lcom/android/launcher2/af;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 1489
    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/af;)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/AppsCustomizePagedView;Lcom/android/launcher2/i;Lcom/android/launcher2/af;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1462
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/android/launcher2/i;->a()V

    :cond_5
    iget-object v2, p2, Lcom/android/launcher2/af;->b:Ljava/util/ArrayList;

    iget-object v3, p2, Lcom/android/launcher2/af;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_f
    if-lt v1, v4, :cond_12

    :cond_11
    return-void

    :cond_12
    if-eqz p1, :cond_1d

    invoke-virtual {p1}, Lcom/android/launcher2/i;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p1}, Lcom/android/launcher2/i;->a()V

    :cond_1d
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->m:Lcom/android/launcher2/u;

    invoke-static {v5}, Lcom/anddoes/launcher/v;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/launcher2/u;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_33

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-eqz v8, :cond_3a

    :cond_33
    invoke-virtual {v6, v5, p2}, Lcom/android/launcher2/u;->a(Ljava/lang/Object;Lcom/android/launcher2/af;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Lcom/android/launcher2/u;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_3a
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f
.end method

.method private a(Lcom/android/launcher2/af;)V
    .registers 9
    .parameter

    .prologue
    .line 1490
    iget-boolean v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->be:Z

    if-eqz v1, :cond_a

    .line 1491
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bf:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1523
    :goto_9
    return-void

    .line 1495
    :cond_a
    :try_start_a
    iget v1, p1, Lcom/android/launcher2/af;->a:I

    .line 1496
    invoke-virtual {p0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/in;

    .line 1498
    if-eqz v1, :cond_32

    .line 1499
    iget-object v2, p1, Lcom/android/launcher2/af;->b:Ljava/util/ArrayList;

    .line 1500
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1501
    const/4 v2, 0x0

    move v4, v2

    :goto_1c
    if-lt v4, v5, :cond_42

    .line 1508
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v3, "VERTICAL_CONTINUOUS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2f

    .line 1509
    invoke-virtual {v1}, Lcom/android/launcher2/in;->c()V

    .line 1511
    :cond_2f
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->invalidate()V

    .line 1514
    :cond_32
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1515
    :goto_38
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z
    :try_end_3b
    .catchall {:try_start_a .. :try_end_3b} :catchall_71

    move-result v1

    if-nez v1, :cond_61

    .line 1521
    invoke-virtual {p1}, Lcom/android/launcher2/af;->a()V

    goto :goto_9

    .line 1502
    :cond_42
    :try_start_42
    invoke-virtual {v1, v4}, Lcom/android/launcher2/in;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/android/launcher2/PagedViewWidget;

    move-object v3, v0

    .line 1503
    if-eqz v3, :cond_5d

    .line 1504
    iget-object v2, p1, Lcom/android/launcher2/af;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1505
    new-instance v6, Lcom/android/launcher2/ca;

    invoke-direct {v6, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x1

    invoke-virtual {v3, v6, v2}, Lcom/android/launcher2/PagedViewWidget;->a(Lcom/android/launcher2/ca;I)V

    .line 1501
    :cond_5d
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1c

    .line 1516
    :cond_61
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/i;

    .line 1517
    iget v3, v1, Lcom/android/launcher2/i;->b:I

    .line 1518
    invoke-direct {p0, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->h(I)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/android/launcher2/i;->a(I)V
    :try_end_70
    .catchall {:try_start_42 .. :try_end_70} :catchall_71

    goto :goto_38

    .line 1520
    :catchall_71
    move-exception v1

    .line 1521
    invoke-virtual {p1}, Lcom/android/launcher2/af;->a()V

    .line 1522
    throw v1
.end method

.method static synthetic b(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/ik;
    .registers 2
    .parameter

    .prologue
    .line 292
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    return-object v0
.end method

.method private static b(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x3f80

    .line 1345
    if-eqz p1, :cond_21

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    add-int v2, p2, p4

    add-int v3, p3, p5

    invoke-virtual {p0, p2, p3, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1346
    :cond_21
    return-void
.end method

.method private b(Lcom/android/launcher2/h;)Z
    .registers 4
    .parameter

    .prologue
    .line 1947
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->w:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1948
    const/4 v0, 0x0

    .line 1953
    :goto_d
    return v0

    .line 1950
    :cond_e
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bj:[Ljava/lang/String;

    if-nez v0, :cond_20

    .line 1951
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->w:Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bj:[Ljava/lang/String;

    .line 1953
    :cond_20
    invoke-virtual {p1}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bj:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/anddoes/launcher/v;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_d
.end method

.method static synthetic c(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/da;
    .registers 2
    .parameter

    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aP:Lcom/android/launcher2/da;

    return-object v0
.end method

.method static synthetic d(Lcom/android/launcher2/AppsCustomizePagedView;)I
    .registers 2
    .parameter

    .prologue
    .line 287
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aR:I

    return v0
.end method

.method private d(Ljava/util/ArrayList;)V
    .registers 7
    .parameter

    .prologue
    .line 1721
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1722
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    if-lt v1, v2, :cond_9

    .line 1731
    return-void

    .line 1723
    :cond_9
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1724
    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/h;)Z

    move-result v3

    if-nez v3, :cond_27

    .line 1725
    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    sget-object v4, Lcom/android/launcher2/gb;->i:Ljava/util/Comparator;

    invoke-static {v3, v0, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v3

    .line 1726
    if-gez v3, :cond_27

    .line 1727
    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    add-int/lit8 v3, v3, 0x1

    neg-int v3, v3

    invoke-virtual {v4, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1722
    :cond_27
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6
.end method

.method static synthetic e(Lcom/android/launcher2/AppsCustomizePagedView;)Landroid/content/pm/PackageManager;
    .registers 2
    .parameter

    .prologue
    .line 270
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method private e(Ljava/util/ArrayList;)V
    .registers 10
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1767
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    .line 1768
    :goto_6
    if-lt v3, v4, :cond_9

    .line 1775
    return-void

    .line 1769
    :cond_9
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1770
    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    iget-object v0, v0, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    move v1, v2

    :goto_1c
    if-lt v1, v7, :cond_2a

    const/4 v0, -0x1

    .line 1771
    :goto_1f
    if-ltz v0, :cond_26

    .line 1772
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1768
    :cond_26
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_6

    .line 1770
    :cond_2a
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    iget-object v0, v0, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    move v0, v1

    goto :goto_1f

    :cond_3e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c
.end method

.method static synthetic f(Lcom/android/launcher2/AppsCustomizePagedView;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 252
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private f(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 713
    if-nez p1, :cond_16

    .line 715
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    .line 716
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    .line 718
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    if-nez v1, :cond_20

    .line 720
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bc:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 721
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bb:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 741
    :cond_16
    :goto_16
    iput v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    .line 742
    iput v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    .line 743
    iput-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    .line 744
    invoke-static {}, Lcom/android/launcher2/PagedViewWidget;->b()V

    .line 745
    return-void

    .line 722
    :cond_20
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3a

    .line 724
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    if-eq v0, v3, :cond_34

    .line 725
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->d()Lcom/android/launcher2/fx;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    invoke-virtual {v0, v1}, Lcom/android/launcher2/fx;->deleteAppWidgetId(I)V

    .line 729
    :cond_34
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bb:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_16

    .line 730
    :cond_3a
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_16

    .line 732
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    if-eq v1, v3, :cond_4e

    .line 733
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->d()Lcom/android/launcher2/fx;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    invoke-virtual {v1, v2}, Lcom/android/launcher2/fx;->deleteAppWidgetId(I)V

    .line 737
    :cond_4e
    iget-object v0, v0, Lcom/android/launcher2/iw;->u:Landroid/appwidget/AppWidgetHostView;

    .line 738
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    goto :goto_16
.end method

.method private g(I)I
    .registers 6
    .parameter

    .prologue
    .line 1215
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    .line 1216
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->v:I

    if-ltz v1, :cond_33

    .line 1217
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->v:I

    move v1, v0

    .line 1223
    :goto_9
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1224
    const v0, 0x7fffffff

    .line 1225
    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_25

    .line 1230
    sub-int v1, p1, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1231
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, v1, v0

    return v0

    .line 1226
    :cond_25
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/i;

    .line 1227
    iget v0, v0, Lcom/android/launcher2/i;->b:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    goto :goto_12

    :cond_33
    move v1, v0

    goto :goto_9
.end method

.method static synthetic g(Lcom/android/launcher2/AppsCustomizePagedView;)Z
    .registers 2
    .parameter

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->be:Z

    return v0
.end method

.method private getMiddleComponentIndexOnCurrentPage()I
    .registers 2

    .prologue
    .line 399
    const/4 v0, -0x1

    return v0
.end method

.method private static getPageForComponent$134621()I
    .registers 1

    .prologue
    .line 444
    const/4 v0, 0x0

    return v0
.end method

.method private getTabHost()Lcom/android/launcher2/AppsCustomizeTabHost;
    .registers 3

    .prologue
    .line 1837
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    const v1, 0x7f0d003e

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/AppsCustomizeTabHost;

    return-object v0
.end method

.method private h(I)I
    .registers 5
    .parameter

    .prologue
    const/16 v1, 0x13

    const/4 v0, 0x1

    .line 1240
    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->g(I)I

    move-result v2

    .line 1241
    if-gtz v2, :cond_a

    .line 1246
    :goto_9
    return v0

    .line 1243
    :cond_a
    if-gt v2, v0, :cond_e

    move v0, v1

    .line 1244
    goto :goto_9

    :cond_e
    move v0, v1

    .line 1246
    goto :goto_9
.end method

.method static synthetic h(Lcom/android/launcher2/AppsCustomizePagedView;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 325
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bg:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Lcom/android/launcher2/AppsCustomizePagedView;)I
    .registers 2
    .parameter

    .prologue
    .line 289
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    return v0
.end method

.method private setupPage(Lcom/android/launcher2/ik;)V
    .registers 8
    .parameter

    .prologue
    const/high16 v5, -0x8000

    const/4 v4, 0x0

    .line 1115
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iput v0, p1, Lcom/android/launcher2/ik;->a:I

    iput v1, p1, Lcom/android/launcher2/ik;->b:I

    invoke-virtual {p1}, Lcom/android/launcher2/ik;->requestLayout()V

    .line 1116
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->M:I

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    invoke-virtual {p1, v0, v1}, Lcom/android/launcher2/ik;->a(II)V

    .line 1117
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->I()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 1118
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    .line 1119
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    .line 1118
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/android/launcher2/ik;->setPadding(IIII)V

    .line 1130
    :goto_28
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/ViewGroup;I)V

    .line 1131
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1132
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1133
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getPageContentWidth()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/android/launcher2/ik;->setMinimumWidth(I)V

    .line 1134
    invoke-virtual {p1, v0, v1}, Lcom/android/launcher2/ik;->measure(II)V

    .line 1135
    invoke-static {p1, v4}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/ViewGroup;I)V

    .line 1136
    return-void

    .line 1121
    :cond_4b
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    .line 1122
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    .line 1121
    invoke-virtual {p1, v0, v4, v1, v4}, Lcom/android/launcher2/ik;->setPadding(IIII)V

    .line 1123
    invoke-virtual {p1}, Lcom/android/launcher2/ik;->getChildrenLayout()Lcom/android/launcher2/im;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    .line 1124
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    .line 1123
    invoke-virtual {v0, v4, v1, v4, v2}, Lcom/android/launcher2/im;->setPadding(IIII)V

    goto :goto_28
.end method

.method private setupPage(Lcom/android/launcher2/in;)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, -0x8000

    .line 1315
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    .line 1316
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    .line 1315
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/android/launcher2/in;->setPadding(IIII)V

    .line 1319
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1320
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1321
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getPageContentWidth()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/android/launcher2/in;->setMinimumWidth(I)V

    .line 1322
    invoke-virtual {p1, v0, v1}, Lcom/android/launcher2/in;->measure(II)V

    .line 1323
    return-void
.end method


# virtual methods
.method final a(I)Landroid/view/View;
    .registers 3
    .parameter

    .prologue
    .line 1568
    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->b(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;Lcom/android/launcher2/h;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewIcon;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2570
    if-eqz p1, :cond_18

    instance-of v0, p1, Lcom/android/launcher2/PagedViewIcon;

    if-eqz v0, :cond_18

    .line 2571
    check-cast p1, Lcom/android/launcher2/PagedViewIcon;

    .line 2576
    :goto_8
    invoke-virtual {p1, p2, p0}, Lcom/android/launcher2/PagedViewIcon;->a(Lcom/android/launcher2/h;Lcom/android/launcher2/ip;)V

    .line 2577
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2578
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewIcon;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2579
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewIcon;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2580
    invoke-virtual {p1, p0}, Lcom/android/launcher2/PagedViewIcon;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2581
    return-object p1

    .line 2573
    :cond_18
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aI:Landroid/view/LayoutInflater;

    .line 2574
    const v1, 0x7f030009

    const/4 v2, 0x0

    .line 2573
    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/PagedViewIcon;

    move-object p1, v0

    goto :goto_8
.end method

.method public final a(F)V
    .registers 2
    .parameter

    .prologue
    .line 910
    return-void
.end method

.method public final a(IZ)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1544
    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    .line 1546
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->I:Z

    if-eqz v0, :cond_189

    .line 1547
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_189

    .line 1548
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_ee

    .line 1558
    :goto_20
    iget-boolean v0, v3, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v0, :cond_17c

    .line 1559
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    mul-int/2addr v4, v0

    sub-int v0, p1, v1

    mul-int/2addr v0, v4

    add-int/2addr v4, v0

    iget-object v5, v3, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    const-string v5, "VERTICAL_CONTINUOUS"

    iget-object v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v6, v6, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v6, v6, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_185

    iget-object v0, v3, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v4, v2

    move v5, v0

    :goto_4d
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->I:Z

    if-nez v0, :cond_56

    sub-int/2addr p1, v1

    :cond_56
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v1, "VERTICAL_CONTINUOUS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14a

    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jk;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v1}, Lcom/android/launcher2/ik;->getContentWidth()I

    move-result v1

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    sub-int/2addr v1, v6

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    sub-int/2addr v1, v6

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    add-int/lit8 v6, v6, -0x1

    iget v8, p0, Lcom/android/launcher2/AppsCustomizePagedView;->M:I

    mul-int/2addr v6, v8

    sub-int/2addr v1, v6

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    div-int v8, v1, v6

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v1}, Lcom/android/launcher2/ik;->getMeasuredHeight()I

    move-result v9

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget-object v10, p0, Lcom/android/launcher2/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-boolean v1, v1, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v1, :cond_102

    const v1, 0x7f0701bf

    :goto_95
    invoke-virtual {v10, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v6, v1, :cond_181

    iget-object v10, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-boolean v10, v10, Lcom/android/launcher2/Launcher;->q:Z

    if-nez v10, :cond_ad

    add-int/lit8 v10, v1, 0x1

    if-le v6, v10, :cond_181

    :cond_ad
    iget-object v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-boolean v6, v6, Lcom/android/launcher2/Launcher;->q:Z

    if-eqz v6, :cond_106

    :goto_b3
    iput v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    invoke-virtual {v0, v2}, Lcom/android/launcher2/jk;->setVerticalSpacing(I)V

    move v6, v7

    :goto_b9
    add-int/lit8 v10, v1, -0x1

    iget v11, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    mul-int/2addr v10, v11

    sub-int/2addr v9, v10

    div-int/2addr v9, v1

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    :goto_c5
    if-lt v4, v5, :cond_109

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_cc
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    if-lt v1, v4, :cond_117

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    rem-int/2addr v3, v1

    if-lez v3, :cond_db

    move v1, v2

    :goto_d6
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    sub-int/2addr v4, v3

    if-lt v1, v4, :cond_127

    :cond_db
    :goto_db
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    if-lt v2, v1, :cond_136

    invoke-virtual {v0}, Lcom/android/launcher2/jk;->a()V

    new-instance v1, Lcom/android/launcher2/r;

    invoke-direct {v1, p0, v10, v8, v9}, Lcom/android/launcher2/r;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;Ljava/util/ArrayList;II)V

    invoke-virtual {v0, v1}, Lcom/android/launcher2/jk;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0}, Lcom/android/launcher2/jk;->b()V

    .line 1563
    :goto_ed
    return-void

    .line 1548
    :cond_ee
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 1549
    iget v5, v0, Lcom/anddoes/launcher/p;->h:I

    add-int/2addr v5, v1

    if-ge p1, v5, :cond_fc

    move-object v3, v0

    .line 1551
    goto/16 :goto_20

    .line 1553
    :cond_fc
    iget v0, v0, Lcom/anddoes/launcher/p;->h:I

    add-int/2addr v0, v1

    move v1, v0

    goto/16 :goto_1a

    .line 1559
    :cond_102
    const v1, 0x7f0701bd

    goto :goto_95

    :cond_106
    add-int/lit8 v1, v1, 0x1

    goto :goto_b3

    :cond_109
    iget-object v1, v3, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/h;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_c5

    :cond_117
    new-instance v4, Lcom/android/launcher2/t;

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    iget v11, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    sub-int/2addr v5, v11

    invoke-direct {v4, p0, v5}, Lcom/android/launcher2/t;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    invoke-virtual {v10, v2, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_cc

    :cond_127
    new-instance v4, Lcom/android/launcher2/t;

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    sub-int v5, v9, v5

    invoke-direct {v4, p0, v5}, Lcom/android/launcher2/t;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_d6

    :cond_136
    new-instance v3, Lcom/android/launcher2/t;

    if-eqz v6, :cond_148

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    sub-int/2addr v1, v4

    :goto_13f
    invoke-direct {v3, p0, v1}, Lcom/android/launcher2/t;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;I)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_db

    :cond_148
    move v1, v7

    goto :goto_13f

    :cond_14a
    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ik;

    invoke-virtual {v0}, Lcom/android/launcher2/ik;->a()V

    move v2, v4

    :goto_154
    if-lt v2, v5, :cond_15b

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v9}, Lcom/android/launcher2/ik;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_ed

    :cond_15b
    iget-object v1, v3, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/h;

    invoke-virtual {p0, v9, v1, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/View;Lcom/android/launcher2/h;Landroid/view/ViewGroup;)Lcom/android/launcher2/PagedViewIcon;

    move-result-object v1

    sub-int v6, v2, v4

    iget v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    rem-int v7, v6, v7

    iget v8, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    div-int/2addr v6, v8

    new-instance v8, Lcom/android/launcher2/il;

    invoke-direct {v8, v7, v6}, Lcom/android/launcher2/il;-><init>(II)V

    invoke-virtual {v0, v1, v2, v8}, Lcom/android/launcher2/ik;->a(Landroid/view/View;ILcom/android/launcher2/il;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_154

    .line 1561
    :cond_17c
    invoke-direct {p0, v1, p1, v3, p2}, Lcom/android/launcher2/AppsCustomizePagedView;->a(IILcom/anddoes/launcher/p;Z)V

    goto/16 :goto_ed

    :cond_181
    move v1, v6

    move v6, v2

    goto/16 :goto_b9

    :cond_185
    move v5, v4

    move v4, v0

    goto/16 :goto_4d

    :cond_189
    move v1, v2

    goto/16 :goto_20
.end method

.method public final a(Landroid/content/ComponentName;)V
    .registers 6
    .parameter

    .prologue
    .line 1994
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1995
    const/4 v0, 0x0

    move v1, v0

    :goto_8
    if-lt v1, v2, :cond_b

    .line 2002
    :goto_a
    return-void

    .line 1996
    :cond_b
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1997
    iget-object v3, v0, Lcom/android/launcher2/h;->f:Landroid/content/ComponentName;

    invoke-virtual {v3, p1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 1998
    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/h;)V

    goto :goto_a

    .line 1995
    :cond_1f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8
.end method

.method protected final a(Landroid/view/MotionEvent;)V
    .registers 12
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/high16 v5, 0x3f80

    const/4 v9, 0x0

    .line 2052
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->Z:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 2053
    const/4 v1, -0x1

    if-ne v0, v1, :cond_f

    .line 2104
    :cond_e
    :goto_e
    return-void

    .line 2056
    :cond_f
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 2057
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 2058
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->y:F

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v3, v0

    .line 2059
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->A:F

    sub-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v4, v0

    .line 2060
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->G:I

    if-le v3, v0, :cond_83

    move v0, v6

    .line 2061
    :goto_2e
    iget v8, p0, Lcom/android/launcher2/AppsCustomizePagedView;->G:I

    if-le v4, v8, :cond_85

    .line 2062
    :goto_32
    if-nez v0, :cond_36

    if-eqz v6, :cond_39

    .line 2064
    :cond_36
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->x()V

    .line 2067
    :cond_39
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ap:Z

    if-eqz v0, :cond_87

    .line 2068
    int-to-float v0, v4

    invoke-static {v0, v9}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_e

    .line 2071
    div-int v0, v3, v4

    int-to-float v0, v0

    .line 2079
    :goto_47
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 2081
    const-string v6, "VERTICAL_CONTINUOUS"

    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v7, v7, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5f

    .line 2082
    const/16 v6, 0xf

    if-gt v4, v6, :cond_e

    .line 2086
    :cond_5f
    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->o:F

    cmpl-float v6, v0, v6

    if-gtz v6, :cond_e

    .line 2090
    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->n:F

    cmpl-float v6, v0, v6

    if-lez v6, :cond_92

    .line 2098
    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->n:F

    sub-float/2addr v0, v6

    .line 2099
    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->o:F

    iget v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->n:F

    sub-float/2addr v6, v7

    div-float/2addr v0, v6

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 2100
    const/high16 v6, 0x4080

    mul-float/2addr v0, v6

    add-float/2addr v5, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/AppsCustomizePagedView;->a(FFIIF)V

    goto :goto_e

    :cond_83
    move v0, v7

    .line 2060
    goto :goto_2e

    :cond_85
    move v6, v7

    .line 2061
    goto :goto_32

    .line 2073
    :cond_87
    int-to-float v0, v3

    invoke-static {v0, v9}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_e

    .line 2076
    div-int v0, v4, v3

    int-to-float v0, v0

    goto :goto_47

    :cond_92
    move-object v0, p0

    .line 2102
    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/AppsCustomizePagedView;->a(FFIIF)V

    goto/16 :goto_e
.end method

.method public final a(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 704
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    if-eqz v0, :cond_8

    .line 706
    invoke-direct {p0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->f(Z)V

    .line 708
    :cond_8
    new-instance v2, Lcom/android/launcher2/iw;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/iw;

    invoke-direct {v2, v0}, Lcom/android/launcher2/iw;-><init>(Lcom/android/launcher2/iw;)V

    iput-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    .line 709
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    iget-object v3, v2, Lcom/android/launcher2/iw;->t:Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, v3, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-nez v0, :cond_4e

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    iget-object v4, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v4, :cond_4f

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    const/4 v0, 0x1

    :goto_32
    if-nez v0, :cond_4e

    iput v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    new-instance v0, Lcom/android/launcher2/l;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/l;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;Lcom/android/launcher2/iw;)V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bc:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bc:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    new-instance v0, Lcom/android/launcher2/m;

    invoke-direct {v0, p0, v3, v2}, Lcom/android/launcher2/m;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;Landroid/appwidget/AppWidgetProviderInfo;Lcom/android/launcher2/iw;)V

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bb:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bb:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->post(Ljava/lang/Runnable;)Z

    .line 710
    :cond_4e
    return-void

    :cond_4f
    move v0, v1

    .line 709
    goto :goto_32
.end method

.method public final a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 930
    if-eqz p3, :cond_4

    .line 957
    :goto_3
    return-void

    .line 932
    :cond_4
    invoke-direct {p0, p1, v2, p4}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/View;ZZ)V

    .line 936
    if-nez p4, :cond_39

    .line 938
    instance-of v0, p1, Lcom/android/launcher2/Workspace;

    if-eqz v0, :cond_41

    .line 939
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->s()I

    move-result v0

    .line 940
    check-cast p1, Lcom/android/launcher2/Workspace;

    .line 941
    invoke-virtual {p1, v0}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    .line 942
    iget-object v1, p2, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v1, Lcom/android/launcher2/di;

    .line 943
    if-eqz v0, :cond_41

    .line 944
    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->a(Lcom/android/launcher2/di;)V

    .line 946
    const/4 v3, 0x0

    iget v4, v1, Lcom/android/launcher2/di;->n:I

    iget v1, v1, Lcom/android/launcher2/di;->o:I

    invoke-virtual {v0, v3, v4, v1}, Lcom/android/launcher2/CellLayout;->a([III)Z

    move-result v0

    if-eqz v0, :cond_3f

    move v0, v2

    .line 949
    :goto_30
    if-eqz v0, :cond_37

    .line 950
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/Launcher;->a(Z)V

    .line 953
    :cond_37
    iput-boolean v2, p2, Lcom/android/launcher2/bz;->k:Z

    .line 955
    :cond_39
    invoke-direct {p0, p4}, Lcom/android/launcher2/AppsCustomizePagedView;->f(Z)V

    .line 956
    iput-boolean v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bd:Z

    goto :goto_3

    .line 946
    :cond_3f
    const/4 v0, 0x1

    goto :goto_30

    :cond_41
    move v0, v2

    goto :goto_30
.end method

.method public final a(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/bk;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1648
    iput-object p1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    .line 1649
    iget-object v0, p1, Lcom/android/launcher2/Launcher;->b:Lcom/android/launcher2/AppsCustomizeTabHost;

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    .line 1650
    iput-object p2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aH:Lcom/android/launcher2/bk;

    .line 1651
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->D:Z

    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aq:Z

    .line 1652
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v1, "VERTICAL_PAGINATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ap:Z

    .line 1653
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->E:Z

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->setElasticScrolling(Z)V

    .line 1654
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->m()V

    .line 1655
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->F:I

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->at:I

    .line 1656
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    .line 1657
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->y:I

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    .line 1658
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->x:I

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    .line 1659
    const-string v0, "VERTICAL_CONTINUOUS"

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 1660
    const v0, 0x3f060a92

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->o:F

    .line 1661
    const v0, 0x3ec90fdb

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->n:F

    .line 1663
    :cond_67
    return-void
.end method

.method public final a(Lcom/android/launcher2/Launcher;ZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 898
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->be:Z

    .line 899
    if-eqz p3, :cond_8

    .line 900
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->E()V

    .line 902
    :cond_8
    return-void
.end method

.method public final a(Lcom/android/launcher2/PagedViewIcon;)V
    .registers 3
    .parameter

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    if-eqz v0, :cond_9

    .line 1879
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    invoke-virtual {v0}, Lcom/android/launcher2/PagedViewIcon;->b()V

    .line 1881
    :cond_9
    iput-object p1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    .line 1882
    return-void
.end method

.method public final a(Lcom/android/launcher2/h;)V
    .registers 5
    .parameter

    .prologue
    .line 1983
    iget v0, p1, Lcom/android/launcher2/h;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/android/launcher2/h;->a:I

    .line 1984
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->l:Lcom/anddoes/launcher/preference/a;

    invoke-virtual {p1}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v1

    iget v2, p1, Lcom/android/launcher2/h;->a:I

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/preference/a;->b(Ljava/lang/String;I)V

    .line 1985
    const-string v0, "MOSTLY_USED"

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 1986
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_29
    :goto_29
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_33

    .line 1989
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->A()V

    .line 1991
    :cond_32
    return-void

    .line 1986
    :cond_33
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 1987
    iget-boolean v2, v0, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v2, :cond_29

    iget-object v0, v0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    sget-object v2, Lcom/android/launcher2/gb;->m:Ljava/util/Comparator;

    invoke-static {v0, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_29
.end method

.method public final a(Ljava/util/ArrayList;)V
    .registers 10
    .parameter

    .prologue
    .line 1735
    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Ljava/util/ArrayList;)V

    .line 1736
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_f

    .line 1737
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1738
    const/4 v0, 0x0

    move v2, v0

    :goto_d
    if-lt v2, v3, :cond_13

    .line 1750
    :cond_f
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->F()V

    .line 1751
    return-void

    .line 1739
    :cond_13
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1740
    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/h;)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 1741
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_27
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_31

    .line 1738
    :cond_2d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 1741
    :cond_31
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/p;

    .line 1742
    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v5, v5, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->z:Ljava/lang/String;

    invoke-virtual {v1, v0, v5}, Lcom/anddoes/launcher/p;->a(Lcom/android/launcher2/h;Ljava/lang/String;)V

    .line 1743
    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v7, v7, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/anddoes/launcher/p;->a(IILjava/lang/String;)V

    goto :goto_27
.end method

.method public final a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 526
    if-eqz p1, :cond_6

    .line 527
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->c()V

    .line 541
    :goto_5
    return-void

    .line 535
    :cond_6
    new-instance v0, Lcom/android/launcher2/j;

    invoke-direct {v0, p0}, Lcom/android/launcher2/j;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;)V

    .line 539
    const-wide/16 v1, 0x5dc

    .line 535
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_5
.end method

.method protected final a_(III)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1043
    invoke-super {p0, p1, p2, p3}, Lcom/android/launcher2/is;->a_(III)V

    .line 1044
    if-gez p1, :cond_59

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getPageCount()I

    move-result v1

    add-int/lit8 p1, v1, -0x1

    :cond_c
    :goto_c
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v1, :cond_4c

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4c

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->I:Z

    if-eqz v1, :cond_35

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_61

    move-object v0, v2

    :cond_31
    if-eqz v0, :cond_35

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    :cond_35
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    if-eqz v0, :cond_4c

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    iget-object v0, v0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4c

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    iget-object v1, v1, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->setCurrentTabFromContent(Ljava/lang/String;)V

    .line 1047
    :cond_4c
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1048
    :goto_52
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_71

    .line 1058
    return-void

    .line 1044
    :cond_59
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getPageCount()I

    move-result v1

    if-lt p1, v1, :cond_c

    move p1, v0

    goto :goto_c

    :cond_61
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    iget v5, v0, Lcom/anddoes/launcher/p;->h:I

    add-int/2addr v5, v1

    if-lt p1, v5, :cond_31

    iget v0, v0, Lcom/anddoes/launcher/p;->h:I

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_2a

    .line 1049
    :cond_71
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/i;

    .line 1050
    iget v2, v0, Lcom/android/launcher2/i;->b:I

    .line 1051
    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->v:I

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    if-le v3, v4, :cond_83

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    if-ge v2, v3, :cond_8d

    .line 1052
    :cond_83
    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->v:I

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    if-ge v3, v4, :cond_95

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    if-gt v2, v3, :cond_95

    .line 1053
    :cond_8d
    invoke-direct {p0, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->h(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/android/launcher2/i;->a(I)V

    goto :goto_52

    .line 1055
    :cond_95
    const/16 v2, 0x13

    invoke-virtual {v0, v2}, Lcom/android/launcher2/i;->a(I)V

    goto :goto_52
.end method

.method protected final b(I)I
    .registers 3
    .parameter

    .prologue
    .line 1573
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected final b()V
    .registers 3

    .prologue
    .line 380
    invoke-super {p0}, Lcom/android/launcher2/is;->b()V

    .line 381
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->Q:Z

    .line 383
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 384
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 385
    const v1, 0x7f0a000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c8

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->setDragSlopeThreshold(F)V

    .line 386
    return-void
.end method

.method protected final b(F)V
    .registers 2
    .parameter

    .prologue
    .line 1623
    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->c(F)V

    .line 1624
    return-void
.end method

.method protected final b(Landroid/view/MotionEvent;)V
    .registers 2
    .parameter

    .prologue
    .line 638
    return-void
.end method

.method public final b(Lcom/android/launcher2/Launcher;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 914
    iput-boolean v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->be:Z

    .line 915
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 918
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bf:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 919
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_35

    .line 922
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bg:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 923
    if-eqz p3, :cond_3f

    move v0, v1

    :goto_28
    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->U:Z

    .line 924
    return-void

    .line 915
    :cond_2b
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/af;

    .line 916
    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/af;)V

    goto :goto_9

    .line 919
    :cond_35
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 920
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1a

    .line 923
    :cond_3f
    const/4 v0, 0x1

    goto :goto_28
.end method

.method public final b(Ljava/util/ArrayList;)V
    .registers 10
    .parameter

    .prologue
    .line 1779
    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->e(Ljava/util/ArrayList;)V

    .line 1780
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_f

    .line 1781
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1782
    const/4 v0, 0x0

    move v2, v0

    :goto_d
    if-lt v2, v3, :cond_13

    .line 1792
    :cond_f
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->F()V

    .line 1793
    return-void

    .line 1783
    :cond_13
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1784
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_21
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2b

    .line 1782
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_d

    .line 1784
    :cond_2b
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/p;

    .line 1785
    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/p;->a(Lcom/android/launcher2/h;)V

    .line 1786
    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v7, v7, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/anddoes/launcher/p;->a(IILjava/lang/String;)V

    goto :goto_21
.end method

.method public final b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 906
    return-void
.end method

.method protected final b(Landroid/view/View;)Z
    .registers 15
    .parameter

    .prologue
    const/high16 v3, 0x3fa0

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 845
    invoke-super {p0, p1}, Lcom/android/launcher2/is;->b(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v10

    .line 873
    :goto_c
    return v0

    .line 847
    :cond_d
    instance-of v0, p1, Lcom/android/launcher2/PagedViewIcon;

    if-eqz v0, :cond_2f

    .line 848
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;Lcom/android/launcher2/bt;)V

    .line 857
    :cond_23
    new-instance v0, Lcom/android/launcher2/n;

    invoke-direct {v0, p0}, Lcom/android/launcher2/n;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;)V

    .line 871
    const-wide/16 v1, 0x96

    .line 857
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v11

    .line 873
    goto :goto_c

    .line 849
    :cond_2f
    instance-of v0, p1, Lcom/android/launcher2/PagedViewWidget;

    if-eqz v0, :cond_23

    .line 850
    iput-boolean v11, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bd:Z

    const v0, 0x7f0d0014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/android/launcher2/iu;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_53

    iput-boolean v10, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bd:Z

    move v0, v10

    :goto_4f
    if-nez v0, :cond_23

    move v0, v10

    .line 851
    goto :goto_c

    .line 850
    :cond_53
    const/high16 v8, 0x3f80

    instance-of v0, v6, Lcom/android/launcher2/iw;

    if-eqz v0, :cond_112

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    if-nez v0, :cond_5f

    move v0, v10

    goto :goto_4f

    :cond_5f
    iget-object v9, p0, Lcom/android/launcher2/AppsCustomizePagedView;->e:Lcom/android/launcher2/iw;

    iget v4, v9, Lcom/android/launcher2/iu;->n:I

    iget v5, v9, Lcom/android/launcher2/iu;->o:I

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, v4, v5, v11}, Lcom/android/launcher2/Workspace;->a(IIZ)[I

    move-result-object v1

    invoke-virtual {v7}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher2/ca;

    invoke-virtual {v8}, Lcom/android/launcher2/ca;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    aget v2, v1, v10

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v8}, Lcom/android/launcher2/ca;->getIntrinsicHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-int v0, v0

    aget v1, v1, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->m:Lcom/android/launcher2/u;

    iget-object v1, v9, Lcom/android/launcher2/iw;->a:Landroid/content/ComponentName;

    iget v2, v9, Lcom/android/launcher2/iw;->f:I

    iget v3, v9, Lcom/android/launcher2/iw;->g:I

    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher2/u;->a(Landroid/content/ComponentName;IIIII)Landroid/graphics/Bitmap;

    move-result-object v2

    const/16 v0, 0x9

    new-array v0, v0, [F

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v3, v12, v12, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {v8}, Lcom/android/launcher2/ca;->getIntrinsicWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v8}, Lcom/android/launcher2/ca;->getIntrinsicHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-direct {v4, v12, v12, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->START:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v3, v4, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    aget v6, v0, v10

    move-object v4, v9

    :goto_cd
    instance-of v0, v4, Lcom/android/launcher2/iw;

    if-eqz v0, :cond_e6

    move-object v0, v4

    check-cast v0, Lcom/android/launcher2/iw;

    iget v0, v0, Lcom/android/launcher2/iw;->f:I

    if-eqz v0, :cond_e6

    const/16 v0, 0xff

    invoke-static {v10, v0}, Landroid/graphics/TableMaskFilter;->CreateClipTable(II)Landroid/graphics/TableMaskFilter;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    :cond_e6
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v2, v0, v1, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v8

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->x()V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0, v4, v8}, Lcom/android/launcher2/Workspace;->a(Lcom/android/launcher2/iu;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aH:Lcom/android/launcher2/bk;

    sget v5, Lcom/android/launcher2/bk;->b:I

    move-object v1, v7

    move-object v3, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/launcher2/bk;->a(Landroid/view/View;Landroid/graphics/Bitmap;Lcom/android/launcher2/bt;Ljava/lang/Object;IF)V

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    move v0, v11

    goto/16 :goto_4f

    :cond_112
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/iv;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aP:Lcom/android/launcher2/da;

    iget-object v0, v0, Lcom/android/launcher2/iv;->b:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/da;->a(Landroid/content/pm/ActivityInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aO:Landroid/graphics/Canvas;

    invoke-virtual {v2, v1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aO:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    move v2, v10

    move v3, v10

    invoke-static/range {v0 .. v5}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aO:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aO:Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    iput v11, v6, Lcom/android/launcher2/iu;->o:I

    iput v11, v6, Lcom/android/launcher2/iu;->n:I

    move-object v4, v6

    move-object v2, v1

    move v6, v8

    goto/16 :goto_cd
.end method

.method public final c()V
    .registers 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 545
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 547
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v0

    .line 548
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 549
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 550
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_62

    .line 569
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/anddoes/launcher/au;->b(Landroid/content/Context;)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 571
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    .line 572
    new-instance v1, Lcom/android/launcher2/hh;

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    invoke-direct {v1, v2}, Lcom/android/launcher2/hh;-><init>(Landroid/content/pm/PackageManager;)V

    .line 571
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 573
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->g:Lcom/android/launcher2/LauncherApplication;

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/android/launcher2/LauncherApplication;->i:Ljava/util/ArrayList;

    .line 574
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_5e

    .line 575
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_58
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10a

    .line 581
    :cond_5e
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->F()V

    .line 582
    return-void

    .line 550
    :cond_62
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    .line 551
    iget v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    if-lez v3, :cond_d4

    iget v3, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    if-lez v3, :cond_d4

    .line 553
    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-static {v3, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v3

    .line 554
    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-static {v4, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)[I

    move-result-object v4

    .line 555
    aget v5, v3, v7

    aget v6, v4, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 556
    aget v3, v3, v8

    aget v4, v4, v8

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 557
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v4

    if-gt v5, v4, :cond_9e

    .line 558
    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v4

    if-gt v3, v4, :cond_9e

    .line 559
    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_22

    .line 561
    :cond_9e
    const-string v3, "AppsCustomizePagedView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Widget "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " can not fit on this device ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 562
    iget v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 561
    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_22

    .line 565
    :cond_d4
    const-string v3, "AppsCustomizePagedView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Widget "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has invalid dimensions ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 566
    iget v5, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 565
    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_22

    .line 575
    :cond_10a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 576
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2, v3}, Lcom/anddoes/launcher/p;->a(Ljava/util/List;Landroid/content/pm/PackageManager;)V

    .line 577
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, Lcom/anddoes/launcher/p;->b(IILjava/lang/String;)V

    goto/16 :goto_58
.end method

.method protected final c(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1579
    invoke-super {p0, p1}, Lcom/android/launcher2/is;->c(I)V

    .line 1581
    const/4 v0, 0x0

    .line 1582
    const-string v2, "CARD_STACK"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 1583
    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->l(I)V

    .line 1613
    :goto_12
    if-eqz v0, :cond_17

    .line 1618
    invoke-virtual {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->f(I)V

    .line 1620
    :cond_17
    return-void

    .line 1584
    :cond_18
    const-string v2, "TABLET"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 1585
    new-instance v1, Lcom/android/launcher2/ig;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ig;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ig;->a(I)V

    goto :goto_12

    .line 1586
    :cond_2b
    const-string v2, "CUBE_IN"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3e

    .line 1587
    new-instance v1, Lcom/android/launcher2/hw;

    invoke-direct {v1, p0}, Lcom/android/launcher2/hw;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/hw;->a(I)V

    goto :goto_12

    .line 1588
    :cond_3e
    const-string v2, "CUBE"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_51

    .line 1589
    new-instance v1, Lcom/android/launcher2/hx;

    invoke-direct {v1, p0}, Lcom/android/launcher2/hx;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/hx;->a(I)V

    goto :goto_12

    .line 1591
    :cond_51
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->M()Z

    move-result v2

    if-eqz v2, :cond_e5

    .line 1592
    const-string v2, "ACCORDION"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6c

    .line 1593
    new-instance v1, Lcom/android/launcher2/hu;

    invoke-direct {v1, p0}, Lcom/android/launcher2/hu;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/hu;->a(I)V

    goto :goto_12

    .line 1594
    :cond_6c
    const-string v2, "CROSS"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7f

    .line 1595
    new-instance v1, Lcom/android/launcher2/hv;

    invoke-direct {v1, p0}, Lcom/android/launcher2/hv;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/hv;->a(I)V

    goto :goto_12

    .line 1596
    :cond_7f
    const-string v2, "FLIP"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_92

    .line 1597
    new-instance v1, Lcom/android/launcher2/hy;

    invoke-direct {v1, p0}, Lcom/android/launcher2/hy;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/hy;->a(I)V

    goto :goto_12

    .line 1598
    :cond_92
    const-string v2, "OVERLAP"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a6

    .line 1599
    new-instance v1, Lcom/android/launcher2/ia;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ia;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ia;->a(I)V

    goto/16 :goto_12

    .line 1600
    :cond_a6
    const-string v2, "ROTATE"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_ba

    .line 1601
    new-instance v1, Lcom/android/launcher2/ic;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ic;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ic;->a(I)V

    goto/16 :goto_12

    .line 1602
    :cond_ba
    const-string v2, "SCALE"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_ce

    .line 1603
    new-instance v1, Lcom/android/launcher2/ie;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ie;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ie;->a(I)V

    goto/16 :goto_12

    .line 1604
    :cond_ce
    const-string v2, "WHEEL"

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e2

    .line 1605
    new-instance v1, Lcom/android/launcher2/ii;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ii;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v1, p1}, Lcom/android/launcher2/ii;->a(I)V

    goto/16 :goto_12

    :cond_e2
    move v0, v1

    .line 1607
    goto/16 :goto_12

    :cond_e5
    move v0, v1

    .line 1610
    goto/16 :goto_12
.end method

.method public final c(Ljava/util/ArrayList;)V
    .registers 10
    .parameter

    .prologue
    .line 1800
    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->e(Ljava/util/ArrayList;)V

    .line 1801
    invoke-direct {p0, p1}, Lcom/android/launcher2/AppsCustomizePagedView;->d(Ljava/util/ArrayList;)V

    .line 1802
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_12

    .line 1803
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1804
    const/4 v0, 0x0

    move v2, v0

    :goto_10
    if-lt v2, v3, :cond_16

    .line 1815
    :cond_12
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->F()V

    .line 1816
    return-void

    .line 1805
    :cond_16
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1806
    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/h;)Z

    move-result v1

    if-nez v1, :cond_30

    .line 1807
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v1, v1, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_34

    .line 1804
    :cond_30
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_10

    .line 1807
    :cond_34
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/anddoes/launcher/p;

    .line 1808
    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v5, v5, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->z:Ljava/lang/String;

    iget-boolean v6, v1, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v6, :cond_4a

    invoke-virtual {v1, v0}, Lcom/anddoes/launcher/p;->a(Lcom/android/launcher2/h;)V

    invoke-virtual {v1, v0, v5}, Lcom/anddoes/launcher/p;->a(Lcom/android/launcher2/h;Ljava/lang/String;)V

    .line 1809
    :cond_4a
    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget-object v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v7, v7, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v7, v7, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/anddoes/launcher/p;->a(IILjava/lang/String;)V

    goto :goto_2a
.end method

.method public final c_()V
    .registers 2

    .prologue
    .line 1821
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    .line 1834
    return-void
.end method

.method protected final d(I)I
    .registers 5
    .parameter

    .prologue
    .line 1898
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    .line 1899
    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1900
    add-int/lit8 v2, p1, -0x2

    sub-int/2addr v0, v1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1901
    return v0
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 969
    const/4 v0, 0x1

    return v0
.end method

.method protected final e(I)I
    .registers 5
    .parameter

    .prologue
    .line 1905
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    .line 1906
    const/4 v1, 0x5

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1907
    add-int/lit8 v2, p1, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1908
    add-int/lit8 v0, v0, -0x1

    .line 1907
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1909
    return v0
.end method

.method public final e()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 962
    const/4 v0, 0x0

    invoke-direct {p0, v0, v2, v2}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Landroid/view/View;ZZ)V

    .line 963
    invoke-direct {p0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->f(Z)V

    .line 964
    iput-boolean v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bd:Z

    .line 965
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 749
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bd:Z

    if-nez v0, :cond_8

    .line 750
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->f(Z)V

    .line 752
    :cond_8
    return-void
.end method

.method protected final f(I)V
    .registers 7
    .parameter

    .prologue
    .line 1958
    iget-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aq:Z

    if-nez v0, :cond_54

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    const-string v1, "HORIZONTAL_PAGINATED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 1959
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->V:I

    if-ltz v0, :cond_1c

    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->V:I

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->w:I

    if-le v0, v1, :cond_54

    .line 1960
    :cond_1c
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->V:I

    if-gez v0, :cond_55

    const/4 v0, 0x0

    .line 1964
    :goto_21
    invoke-virtual {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1965
    invoke-virtual {p0, p1, v1, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(ILandroid/view/View;I)F

    move-result v2

    .line 1966
    const/high16 v3, -0x3e40

    mul-float/2addr v2, v3

    .line 1967
    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->q:F

    sget v4, Lcom/android/launcher2/AppsCustomizePagedView;->ba:F

    mul-float/2addr v3, v4

    invoke-virtual {v1, v3}, Landroid/view/View;->setCameraDistance(F)V

    .line 1968
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    if-nez v0, :cond_5c

    const/high16 v0, 0x3f40

    :goto_3d
    mul-float/2addr v0, v3

    invoke-virtual {v1, v0}, Landroid/view/View;->setPivotX(F)V

    .line 1969
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x3f00

    mul-float/2addr v0, v3

    invoke-virtual {v1, v0}, Landroid/view/View;->setPivotY(F)V

    .line 1970
    const/high16 v0, 0x3f80

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1971
    invoke-virtual {v1, v2}, Landroid/view/View;->setRotationY(F)V

    .line 1974
    :cond_54
    return-void

    .line 1960
    :cond_55
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_21

    .line 1968
    :cond_5c
    const/high16 v0, 0x3e80

    goto :goto_3d
.end method

.method public final f_()V
    .registers 3

    .prologue
    .line 1527
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->removeAllViews()V

    .line 1528
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->E()V

    .line 1530
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->I:Z

    if-eqz v0, :cond_2d

    .line 1531
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1532
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_19
    if-gez v1, :cond_1c

    .line 1540
    :cond_1b
    :goto_1b
    return-void

    .line 1533
    :cond_1c
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/anddoes/launcher/p;)V

    .line 1532
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_19

    .line 1536
    :cond_2d
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    if-eqz v0, :cond_1b

    .line 1537
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/anddoes/launcher/p;)V

    goto :goto_1b
.end method

.method protected final g()V
    .registers 2

    .prologue
    .line 1636
    invoke-super {p0}, Lcom/android/launcher2/is;->g()V

    .line 1637
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->U:Z

    .line 1640
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    .line 1641
    return-void
.end method

.method public getContent()Landroid/view/View;
    .registers 2

    .prologue
    .line 893
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1914
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_2b

    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->v:I

    .line 1915
    :goto_9
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070057

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1914
    :cond_2b
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->u:I

    goto :goto_9
.end method

.method public getPageContentWidth()I
    .registers 2

    .prologue
    .line 1631
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aQ:I

    return v0
.end method

.method getSaveInstanceStateIndex()I
    .registers 3

    .prologue
    .line 426
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_b

    .line 427
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMiddleComponentIndexOnCurrentPage()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    .line 429
    :cond_b
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    return v0
.end method

.method public final i()V
    .registers 6

    .prologue
    .line 1843
    const-string v0, "AppsCustomizePagedView"

    const-string v1, "mApps"

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1844
    const-string v1, "AppsCustomizePagedView"

    const-string v0, "mWidgets"

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, " size="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_31
    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_38

    .line 1845
    return-void

    .line 1844
    :cond_38
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v3, :cond_9f

    check-cast v0, Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   label=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" previewImage="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resizeMode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->resizeMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " configure="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " initialLayout="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minWidth="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " minHeight="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_31

    :cond_9f
    instance-of v3, v0, Landroid/content/pm/ResolveInfo;

    if-eqz v3, :cond_31

    check-cast v0, Landroid/content/pm/ResolveInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "   label=\""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aJ:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\" icon="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, v0, Landroid/content/pm/ResolveInfo;->icon:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_31
.end method

.method public final j()V
    .registers 1

    .prologue
    .line 1871
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->E()V

    .line 1872
    return-void
.end method

.method public final k()V
    .registers 2

    .prologue
    .line 1885
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    if-eqz v0, :cond_c

    .line 1886
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    invoke-virtual {v0}, Lcom/android/launcher2/PagedViewIcon;->b()V

    .line 1887
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    .line 1889
    :cond_c
    return-void
.end method

.method public final l()V
    .registers 3

    .prologue
    .line 1940
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bj:[Ljava/lang/String;

    .line 1941
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 1942
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->b()Lcom/android/launcher2/gb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/gb;->j()Lcom/android/launcher2/d;

    move-result-object v0

    iget-object v0, v0, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->setApps(Ljava/util/ArrayList;)V

    .line 1943
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->A()V

    .line 1944
    return-void
.end method

.method public final m()V
    .registers 3

    .prologue
    .line 1977
    const-string v0, "VERTICAL_PAGINATED"

    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 1978
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v0, v0, Lcom/anddoes/launcher/preference/f;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->au:Ljava/lang/String;

    .line 1980
    :cond_16
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 9
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 587
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->l()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 588
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->j()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 626
    :cond_16
    :goto_16
    return-void

    .line 590
    :cond_17
    instance-of v0, p1, Lcom/android/launcher2/PagedViewIcon;

    if-eqz v0, :cond_45

    .line 592
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 595
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    if-eqz v1, :cond_2a

    .line 596
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aL:Lcom/android/launcher2/PagedViewIcon;

    invoke-virtual {v1}, Lcom/android/launcher2/PagedViewIcon;->a()V

    .line 602
    :cond_2a
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1, v5}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 603
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, v0, Lcom/android/launcher2/h;->c:Landroid/content/Intent;

    invoke-virtual {v1, p1, v2, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    .line 604
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->e:Landroid/os/Handler;

    new-instance v2, Lcom/android/launcher2/k;

    invoke-direct {v2, p0, v0}, Lcom/android/launcher2/k;-><init>(Lcom/android/launcher2/AppsCustomizePagedView;Lcom/android/launcher2/h;)V

    .line 608
    const-wide/16 v3, 0x3e8

    .line 604
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_16

    .line 609
    :cond_45
    instance-of v0, p1, Lcom/android/launcher2/PagedViewWidget;

    if-eqz v0, :cond_16

    .line 611
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07027b

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 612
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 615
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v1, v0

    .line 616
    const v0, 0x7f0d0014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 617
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 618
    const-string v3, "translationY"

    new-array v4, v5, [F

    aput v1, v4, v6

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 619
    const-wide/16 v3, 0x7d

    invoke-virtual {v1, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 620
    const-string v3, "translationY"

    new-array v4, v5, [F

    const/4 v5, 0x0

    aput v5, v4, v6

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 621
    const-wide/16 v3, 0x64

    invoke-virtual {v0, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 622
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 623
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 624
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_16
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    .prologue
    .line 974
    invoke-super {p0}, Lcom/android/launcher2/is;->onDetachedFromWindow()V

    .line 975
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->E()V

    .line 976
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 2126
    const-string v1, "HORIZONTAL_PAGINATED"

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_87

    .line 2127
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    .line 2129
    if-nez v1, :cond_2a

    .line 2130
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bk:J

    .line 2131
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    iput v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bl:F

    .line 2132
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    iput v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bm:F

    .line 2135
    :cond_2a
    if-eq v1, v0, :cond_2f

    .line 2136
    const/4 v2, 0x6

    if-ne v1, v2, :cond_87

    .line 2137
    :cond_2f
    iget-wide v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bk:J

    sget-wide v4, Lcom/android/launcher2/AppsCustomizePagedView;->bn:J

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_87

    .line 2138
    iput-boolean v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->F:Z

    .line 2139
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 2140
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bl:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    .line 2141
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bm:F

    sub-float v4, v2, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    .line 2142
    if-ne v1, v0, :cond_87

    .line 2143
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->G:I

    mul-int/lit8 v1, v1, 0x3

    if-lt v4, v1, :cond_87

    if-le v4, v3, :cond_87

    .line 2144
    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bm:F

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_79

    .line 2145
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->ar:Ljava/lang/String;

    const-string v3, "SWIPE_UP"

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2149
    :goto_74
    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bk:J

    .line 2156
    :goto_78
    return v0

    .line 2147
    :cond_79
    iget-object v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v2, v2, Lcom/anddoes/launcher/preference/f;->as:Ljava/lang/String;

    const-string v3, "SWIPE_DOWN"

    invoke-virtual {v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_74

    .line 2156
    :cond_87
    invoke-super {p0, p1}, Lcom/android/launcher2/is;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_78
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 629
    invoke-static {p1, p2, p3}, Lcom/android/launcher2/cb;->a(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .registers 15
    .parameter
    .parameter

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/high16 v9, -0x8000

    const v1, 0x7fffffff

    .line 509
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 510
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 511
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->r()Z

    move-result v0

    if-nez v0, :cond_d2

    .line 512
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d2

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aN:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d2

    .line 513
    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->q()V

    .line 514
    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->setMeasuredDimension(II)V

    .line 515
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aS:I

    if-ltz v0, :cond_107

    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aS:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_35
    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aT:I

    if-ltz v4, :cond_3f

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aT:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_3f
    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->M:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->N:I

    invoke-virtual {v4, v5, v6}, Lcom/android/launcher2/ik;->a(II)V

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    iget v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->K:I

    iget v6, p0, Lcom/android/launcher2/AppsCustomizePagedView;->I:I

    iget v7, p0, Lcom/android/launcher2/AppsCustomizePagedView;->L:I

    iget v8, p0, Lcom/android/launcher2/AppsCustomizePagedView;->J:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/android/launcher2/ik;->setPadding(IIII)V

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v4}, Lcom/android/launcher2/ik;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v4}, Lcom/android/launcher2/ik;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    sub-int v5, v2, v5

    iget v6, v4, Lcom/android/launcher2/ik;->e:I

    add-int/2addr v5, v6

    iget v6, v4, Lcom/android/launcher2/ik;->c:I

    iget v7, v4, Lcom/android/launcher2/ik;->e:I

    add-int/2addr v6, v7

    div-int/2addr v5, v6

    invoke-static {v11, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v4, Lcom/android/launcher2/ik;->a:I

    invoke-virtual {v4}, Lcom/android/launcher2/ik;->getPaddingTop()I

    move-result v0

    invoke-virtual {v4}, Lcom/android/launcher2/ik;->getPaddingBottom()I

    move-result v5

    add-int/2addr v0, v5

    sub-int v0, v3, v0

    iget v3, v4, Lcom/android/launcher2/ik;->f:I

    add-int/2addr v0, v3

    iget v3, v4, Lcom/android/launcher2/ik;->d:I

    iget v5, v4, Lcom/android/launcher2/ik;->f:I

    add-int/2addr v3, v5

    div-int/2addr v0, v3

    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v4, Lcom/android/launcher2/ik;->b:I

    invoke-virtual {v4}, Lcom/android/launcher2/ik;->requestLayout()V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_e6

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v3, v0, v1}, Lcom/android/launcher2/ik;->measure(II)V

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aZ:Lcom/android/launcher2/ik;

    invoke-virtual {v0}, Lcom/android/launcher2/ik;->getContentWidth()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aQ:I

    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->getTabHost()Lcom/android/launcher2/AppsCustomizeTabHost;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->d()Z

    move-result v0

    iget v1, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aK:I

    invoke-static {v10, v10}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->c(IZ)V

    .line 518
    :cond_d2
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->e()Z

    move-result v0

    if-eqz v0, :cond_e2

    .line 519
    iget v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aQ:I

    sub-int v0, v2, v0

    div-int/lit8 v0, v0, 0x2

    .line 520
    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ai:I

    iput v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->ah:I

    .line 522
    :cond_e2
    invoke-super {p0, p1, p2}, Lcom/android/launcher2/is;->onMeasure(II)V

    .line 523
    return-void

    .line 515
    :cond_e6
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v5, v5, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v5}, Lcom/anddoes/launcher/p;->a(IILjava/lang/String;)V

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aU:I

    iget v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aV:I

    iget-object v5, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v5, v5, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v5, v5, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v5}, Lcom/anddoes/launcher/p;->b(IILjava/lang/String;)V

    goto :goto_9e

    :cond_107
    move v0, v1

    goto/16 :goto_35
.end method

.method public setApps(Ljava/util/ArrayList;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1699
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1700
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v1, v2

    .line 1701
    :goto_b
    if-lt v1, v3, :cond_2a

    .line 1708
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    sget-object v1, Lcom/android/launcher2/gb;->i:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1709
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_26

    .line 1710
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_20
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4d

    .line 1716
    :cond_26
    invoke-direct {p0}, Lcom/android/launcher2/AppsCustomizePagedView;->F()V

    .line 1717
    return-void

    .line 1702
    :cond_2a
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/h;

    .line 1703
    invoke-direct {p0, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->b(Lcom/android/launcher2/h;)Z

    move-result v4

    if-nez v4, :cond_49

    .line 1704
    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1705
    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->l:Lcom/anddoes/launcher/preference/a;

    invoke-virtual {v0}, Lcom/android/launcher2/h;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Lcom/anddoes/launcher/preference/a;->a(Ljava/lang/String;I)I

    move-result v4

    iput v4, v0, Lcom/android/launcher2/h;->a:I

    .line 1701
    :cond_49
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1710
    :cond_4d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 1711
    iget-object v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aM:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v3, v3, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v3, v3, Lcom/anddoes/launcher/preference/f;->z:Ljava/lang/String;

    iget-boolean v4, v0, Lcom/anddoes/launcher/p;->c:Z

    if-eqz v4, :cond_7d

    invoke-virtual {v0}, Lcom/anddoes/launcher/p;->a()Z

    move-result v4

    if-eqz v4, :cond_8b

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v4, v0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    :goto_6c
    invoke-static {v3}, Lcom/anddoes/launcher/p;->a(Ljava/lang/String;)Ljava/util/Comparator;

    move-result-object v2

    iput-object v2, v0, Lcom/anddoes/launcher/p;->i:Ljava/util/Comparator;

    iget-object v2, v0, Lcom/anddoes/launcher/p;->i:Ljava/util/Comparator;

    if-eqz v2, :cond_7d

    iget-object v2, v0, Lcom/anddoes/launcher/p;->f:Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/anddoes/launcher/p;->i:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1712
    :cond_7d
    iget v2, p0, Lcom/android/launcher2/AppsCustomizePagedView;->O:I

    iget v3, p0, Lcom/android/launcher2/AppsCustomizePagedView;->P:I

    iget-object v4, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v4, v4, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v4, v4, Lcom/anddoes/launcher/preference/f;->A:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v4}, Lcom/anddoes/launcher/p;->a(IILjava/lang/String;)V

    goto :goto_20

    .line 1711
    :cond_8b
    invoke-virtual {v0}, Lcom/anddoes/launcher/p;->b()Z

    move-result v4

    if-eqz v4, :cond_9a

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v4}, Lcom/anddoes/launcher/p;->a(Ljava/util/ArrayList;)V

    goto :goto_6c

    :cond_9a
    invoke-virtual {v0}, Lcom/anddoes/launcher/p;->c()Z

    move-result v4

    if-eqz v4, :cond_a9

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v4}, Lcom/anddoes/launcher/p;->b(Ljava/util/ArrayList;)V

    goto :goto_6c

    :cond_a9
    invoke-virtual {v0}, Lcom/anddoes/launcher/p;->d()Z

    move-result v4

    if-eqz v4, :cond_b8

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v4}, Lcom/anddoes/launcher/p;->c(Ljava/util/ArrayList;)V

    goto :goto_6c

    :cond_b8
    invoke-virtual {v0}, Lcom/anddoes/launcher/p;->e()Z

    move-result v4

    if-eqz v4, :cond_c7

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v4}, Lcom/anddoes/launcher/p;->d(Ljava/util/ArrayList;)V

    goto :goto_6c

    :cond_c7
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v4}, Lcom/anddoes/launcher/p;->e(Ljava/util/ArrayList;)V

    goto :goto_6c
.end method

.method public setContentType(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1015
    .line 1016
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    iget-object v0, v0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    move v0, v2

    .line 1019
    :goto_11
    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    if-eqz v0, :cond_31

    .line 1021
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bh:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v0, v0, Lcom/android/launcher2/AppsCustomizeTabHost;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_20
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_32

    .line 1030
    :goto_26
    iget-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->aG:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->I:Z

    if-eqz v0, :cond_48

    .line 1037
    :goto_2e
    invoke-virtual {p0, v1, v3}, Lcom/android/launcher2/AppsCustomizePagedView;->c(IZ)V

    .line 1040
    :cond_31
    return-void

    .line 1021
    :cond_32
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/anddoes/launcher/p;

    .line 1022
    iget-object v5, v0, Lcom/anddoes/launcher/p;->a:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_43

    .line 1023
    iput-object v0, p0, Lcom/android/launcher2/AppsCustomizePagedView;->bi:Lcom/anddoes/launcher/p;

    goto :goto_26

    .line 1026
    :cond_43
    iget v0, v0, Lcom/anddoes/launcher/p;->h:I

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_20

    :cond_48
    move v1, v2

    goto :goto_2e

    :cond_4a
    move v0, v3

    goto :goto_11
.end method
