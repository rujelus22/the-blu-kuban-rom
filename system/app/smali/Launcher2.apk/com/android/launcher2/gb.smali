.class public final Lcom/android/launcher2/gb;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static F:I

.field private static G:I

.field private static final H:Ljava/text/Collator;

.field private static I:I

.field private static J:I

.field static final a:Ljava/util/ArrayList;

.field static final b:Ljava/lang/Object;

.field static final c:Ljava/util/HashMap;

.field static final d:Ljava/util/ArrayList;

.field static final e:Ljava/util/ArrayList;

.field static final f:Ljava/util/HashMap;

.field static final g:Ljava/util/HashMap;

.field public static final i:Ljava/util/Comparator;

.field public static final j:Ljava/util/Comparator;

.field public static final k:Ljava/util/Comparator;

.field public static final m:Ljava/util/Comparator;

.field public static final n:Ljava/util/Comparator;

.field public static final o:Ljava/util/Comparator;

.field private static final x:Landroid/os/HandlerThread;

.field private static final y:Landroid/os/Handler;


# instance fields
.field private A:Z

.field private B:Ljava/lang/ref/WeakReference;

.field private C:Lcom/android/launcher2/d;

.field private D:Lcom/android/launcher2/da;

.field private E:Landroid/graphics/Bitmap;

.field protected h:I

.field protected l:Lcom/anddoes/launcher/c/l;

.field private final p:Z

.field private q:I

.field private r:I

.field private final s:Lcom/android/launcher2/LauncherApplication;

.field private final t:Ljava/lang/Object;

.field private u:Lcom/android/launcher2/az;

.field private v:Lcom/android/launcher2/gp;

.field private w:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 91
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "launcher-loader"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 93
    sput-object v0, Lcom/android/launcher2/gb;->x:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 95
    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/android/launcher2/gb;->x:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/android/launcher2/gb;->y:Landroid/os/Handler;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    sput-object v0, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    .line 137
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    .line 2396
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/gb;->H:Ljava/text/Collator;

    .line 2398
    new-instance v0, Lcom/android/launcher2/gc;

    invoke-direct {v0}, Lcom/android/launcher2/gc;-><init>()V

    .line 2397
    sput-object v0, Lcom/android/launcher2/gb;->i:Ljava/util/Comparator;

    .line 2408
    new-instance v0, Lcom/android/launcher2/gg;

    invoke-direct {v0}, Lcom/android/launcher2/gg;-><init>()V

    .line 2407
    sput-object v0, Lcom/android/launcher2/gb;->j:Ljava/util/Comparator;

    .line 2416
    new-instance v0, Lcom/android/launcher2/gh;

    invoke-direct {v0}, Lcom/android/launcher2/gh;-><init>()V

    .line 2415
    sput-object v0, Lcom/android/launcher2/gb;->k:Ljava/util/Comparator;

    .line 2520
    new-instance v0, Lcom/android/launcher2/gi;

    invoke-direct {v0}, Lcom/android/launcher2/gi;-><init>()V

    .line 2519
    sput-object v0, Lcom/android/launcher2/gb;->m:Ljava/util/Comparator;

    .line 2529
    new-instance v0, Lcom/android/launcher2/gj;

    invoke-direct {v0}, Lcom/android/launcher2/gj;-><init>()V

    .line 2528
    sput-object v0, Lcom/android/launcher2/gb;->n:Ljava/util/Comparator;

    .line 2538
    new-instance v0, Lcom/android/launcher2/gk;

    invoke-direct {v0}, Lcom/android/launcher2/gk;-><init>()V

    .line 2537
    sput-object v0, Lcom/android/launcher2/gb;->o:Ljava/util/Comparator;

    .line 76
    return-void
.end method

.method constructor <init>(Lcom/android/launcher2/LauncherApplication;Lcom/android/launcher2/da;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 169
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    .line 87
    new-instance v0, Lcom/android/launcher2/az;

    invoke-direct {v0}, Lcom/android/launcher2/az;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/gb;->u:Lcom/android/launcher2/az;

    .line 170
    invoke-static {}, Landroid/os/Environment;->isExternalStorageEmulated()Z

    move-result v0

    if-eqz v0, :cond_50

    const/4 v0, 0x0

    :goto_18
    iput-boolean v0, p0, Lcom/android/launcher2/gb;->p:Z

    .line 171
    iput-object p1, p0, Lcom/android/launcher2/gb;->s:Lcom/android/launcher2/LauncherApplication;

    .line 172
    new-instance v0, Lcom/android/launcher2/d;

    invoke-direct {v0, p2}, Lcom/android/launcher2/d;-><init>(Lcom/android/launcher2/da;)V

    iput-object v0, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    .line 173
    iput-object p2, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    .line 176
    iget-object v0, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    invoke-virtual {v0}, Lcom/android/launcher2/da;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 175
    invoke-static {v0, p1}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/gb;->E:Landroid/graphics/Bitmap;

    .line 178
    invoke-virtual {p1}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 179
    const v1, 0x7f0a0010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/gb;->r:I

    .line 180
    const v1, 0x7f0a0011

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/gb;->q:I

    .line 181
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 182
    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    iput v0, p0, Lcom/android/launcher2/gb;->h:I

    .line 183
    return-void

    .line 170
    :cond_50
    const/4 v0, 0x1

    goto :goto_18
.end method

.method static a(JIII)I
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 536
    long-to-int v0, p0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 537
    and-int/lit16 v1, p2, 0xff

    shl-int/lit8 v1, v1, 0x10

    .line 536
    or-int/2addr v0, v1

    .line 537
    and-int/lit16 v1, p3, 0xff

    shl-int/lit8 v1, v1, 0x8

    .line 536
    or-int/2addr v0, v1

    .line 537
    and-int/lit16 v1, p4, 0xff

    .line 536
    or-int/2addr v0, v1

    return v0
.end method

.method static a(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;
    .registers 4
    .parameter

    .prologue
    .line 2422
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v0, :cond_12

    .line 2423
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425
    :goto_11
    return-object v0

    :cond_12
    new-instance v0, Landroid/content/ComponentName;

    iget-object v1, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v2, p0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_11
.end method

.method static a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2197
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 2205
    const/4 v1, 0x0

    :try_start_5
    array-length v2, v0

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2204
    invoke-static {v0, p2}, Lcom/android/launcher2/jj;->a(Landroid/graphics/Bitmap;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_d} :catch_f

    move-result-object v0

    .line 2207
    :goto_e
    return-object v0

    :catch_f
    move-exception v0

    const/4 v0, 0x0

    goto :goto_e
.end method

.method static a(Landroid/content/Context;Ljava/util/HashMap;J)Lcom/android/launcher2/cu;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x0

    .line 432
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 433
    sget-object v1, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    .line 434
    const-string v3, "_id=? and (itemType=? or itemType=?)"

    .line 435
    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    .line 436
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    .line 433
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 439
    :try_start_1f
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 440
    const-string v0, "itemType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 441
    const-string v3, "title"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 442
    const-string v4, "container"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 443
    const-string v5, "screen"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 444
    const-string v6, "cellX"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 445
    const-string v7, "cellY"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 448
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_84

    .line 454
    :goto_50
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    .line 455
    iput-wide p2, v2, Lcom/android/launcher2/cu;->h:J

    .line 456
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v3, v0

    iput-wide v3, v2, Lcom/android/launcher2/cu;->j:J

    .line 457
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/launcher2/cu;->k:I

    .line 458
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/launcher2/cu;->l:I

    .line 459
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/android/launcher2/cu;->m:I
    :try_end_71
    .catchall {:try_start_1f .. :try_end_71} :catchall_7a

    .line 464
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 467
    :goto_74
    return-object v2

    .line 450
    :pswitch_75
    :try_start_75
    invoke-static {p1, p2, p3}, Lcom/android/launcher2/gb;->b(Ljava/util/HashMap;J)Lcom/android/launcher2/cu;
    :try_end_78
    .catchall {:try_start_75 .. :try_end_78} :catchall_7a

    move-result-object v2

    goto :goto_50

    .line 463
    :catchall_7a
    move-exception v0

    .line 464
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 465
    throw v0

    .line 464
    :cond_7f
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_74

    .line 448
    nop

    :pswitch_data_84
    .packed-switch 0x2
        :pswitch_75
    .end packed-switch
.end method

.method static synthetic a(Ljava/util/HashMap;J)Lcom/android/launcher2/cu;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2385
    invoke-static {p0, p1, p2}, Lcom/android/launcher2/gb;->b(Ljava/util/HashMap;J)Lcom/android/launcher2/cu;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/android/launcher2/jd;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 2131
    .line 2132
    new-instance v2, Lcom/android/launcher2/jd;

    invoke-direct {v2}, Lcom/android/launcher2/jd;-><init>()V

    .line 2133
    iput v6, v2, Lcom/android/launcher2/jd;->i:I

    .line 2137
    invoke-interface {p1, p7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 2139
    invoke-interface {p1, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 2140
    packed-switch v1, :pswitch_data_82

    .line 2186
    iget-object v0, p0, Lcom/android/launcher2/gb;->E:Landroid/graphics/Bitmap;

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2187
    iput-boolean v6, v2, Lcom/android/launcher2/jd;->d:Z

    .line 2188
    iput-boolean v7, v2, Lcom/android/launcher2/jd;->c:Z

    .line 2191
    :cond_21
    :goto_21
    iput-object v0, v2, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    .line 2192
    return-object v2

    .line 2142
    :pswitch_24
    invoke-interface {p1, p4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2143
    invoke-interface {p1, p5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2144
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 2145
    iput-boolean v7, v2, Lcom/android/launcher2/jd;->c:Z

    .line 2149
    :try_start_32
    iget-object v1, p0, Lcom/android/launcher2/gb;->l:Lcom/anddoes/launcher/c/l;

    if-eqz v1, :cond_80

    .line 2150
    iget-object v1, p0, Lcom/android/launcher2/gb;->l:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v1, v3, v4}, Lcom/anddoes/launcher/c/l;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2152
    :goto_3c
    if-nez v1, :cond_65

    .line 2153
    invoke-virtual {v5, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    .line 2154
    if-eqz v1, :cond_54

    .line 2155
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v3, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 2157
    iget-object v4, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    invoke-virtual {v4, v1, v3}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2156
    invoke-static {v1, p2}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_53
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_53} :catch_7e

    move-result-object v0

    .line 2166
    :cond_54
    :goto_54
    if-nez v0, :cond_5a

    .line 2167
    invoke-static {p1, p6, p2}, Lcom/android/launcher2/gb;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2170
    :cond_5a
    if-nez v0, :cond_21

    .line 2171
    iget-object v0, p0, Lcom/android/launcher2/gb;->E:Landroid/graphics/Bitmap;

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2172
    iput-boolean v6, v2, Lcom/android/launcher2/jd;->d:Z

    goto :goto_21

    .line 2160
    :cond_65
    :try_start_65
    invoke-static {v1, p2}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_68
    .catch Ljava/lang/Exception; {:try_start_65 .. :try_end_68} :catch_7e

    move-result-object v0

    goto :goto_54

    .line 2176
    :pswitch_6a
    invoke-static {p1, p6, p2}, Lcom/android/launcher2/gb;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2177
    if-nez v0, :cond_7b

    .line 2178
    iget-object v0, p0, Lcom/android/launcher2/gb;->E:Landroid/graphics/Bitmap;

    invoke-static {v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2179
    iput-boolean v7, v2, Lcom/android/launcher2/jd;->c:Z

    .line 2180
    iput-boolean v6, v2, Lcom/android/launcher2/jd;->d:Z

    goto :goto_21

    .line 2182
    :cond_7b
    iput-boolean v6, v2, Lcom/android/launcher2/jd;->c:Z

    goto :goto_21

    .line 2162
    :catch_7e
    move-exception v1

    goto :goto_54

    :cond_80
    move-object v1, v0

    goto :goto_3c

    .line 2140
    :pswitch_data_82
    .packed-switch 0x0
        :pswitch_24
        :pswitch_6a
    .end packed-switch
.end method

.method static synthetic a(Lcom/android/launcher2/gb;Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/android/launcher2/jd;
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2127
    invoke-direct/range {p0 .. p7}, Lcom/android/launcher2/gb;->a(Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/android/launcher2/jd;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;)Ljava/util/ArrayList;
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 391
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 392
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 393
    sget-object v1, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 394
    const-string v5, "itemType"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "container"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    .line 395
    const-string v5, "screen"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "cellX"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "cellY"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    .line 396
    const-string v5, "spanX"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "spanY"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    .line 393
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 398
    const-string v0, "itemType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 399
    const-string v2, "container"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    .line 400
    const-string v3, "screen"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    .line 401
    const-string v4, "cellX"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 402
    const-string v5, "cellY"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    .line 403
    const-string v7, "spanX"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 404
    const-string v8, "spanY"

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v8

    .line 407
    :goto_62
    :try_start_62
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_a8
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_65} :catch_a0

    move-result v9

    if-nez v9, :cond_6c

    .line 422
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 425
    :goto_6b
    return-object v6

    .line 408
    :cond_6c
    :try_start_6c
    new-instance v9, Lcom/android/launcher2/di;

    invoke-direct {v9}, Lcom/android/launcher2/di;-><init>()V

    .line 409
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, v9, Lcom/android/launcher2/di;->l:I

    .line 410
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, v9, Lcom/android/launcher2/di;->m:I

    .line 411
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, v9, Lcom/android/launcher2/di;->n:I

    .line 412
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, v9, Lcom/android/launcher2/di;->o:I

    .line 413
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    int-to-long v10, v10

    iput-wide v10, v9, Lcom/android/launcher2/di;->j:J

    .line 414
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, v9, Lcom/android/launcher2/di;->i:I

    .line 415
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    iput v10, v9, Lcom/android/launcher2/di;->k:I

    .line 417
    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_9f
    .catchall {:try_start_6c .. :try_end_9f} :catchall_a8
    .catch Ljava/lang/Exception; {:try_start_6c .. :try_end_9f} :catch_a0

    goto :goto_62

    .line 420
    :catch_a0
    move-exception v0

    :try_start_a1
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V
    :try_end_a4
    .catchall {:try_start_a1 .. :try_end_a4} :catchall_a8

    .line 422
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_6b

    .line 421
    :catchall_a8
    move-exception v0

    .line 422
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 423
    throw v0
.end method

.method static a(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 6
    .parameter

    .prologue
    .line 2007
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2008
    sget-object v2, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 2009
    :try_start_8
    sget-object v0, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_e
    :goto_e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_16

    .line 2008
    monitor-exit v2

    .line 2018
    return-object v1

    .line 2009
    :cond_16
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 2010
    instance-of v4, v0, Lcom/android/launcher2/jd;

    if-eqz v4, :cond_e

    .line 2011
    check-cast v0, Lcom/android/launcher2/jd;

    .line 2012
    iget-object v4, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-static {v4}, Lcom/android/launcher2/di;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 2013
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_31
    .catchall {:try_start_8 .. :try_end_31} :catchall_32

    goto :goto_e

    .line 2008
    :catchall_32
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static a(IIII)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 554
    sput p0, Lcom/android/launcher2/gb;->F:I

    .line 555
    sput p1, Lcom/android/launcher2/gb;->G:I

    .line 556
    sput p2, Lcom/android/launcher2/gb;->I:I

    .line 557
    sput p3, Lcom/android/launcher2/gb;->J:I

    .line 558
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/ContentValues;Lcom/android/launcher2/di;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 261
    iget-wide v4, p2, Lcom/android/launcher2/di;->h:J

    .line 262
    invoke-static {v4, v5}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v2

    .line 263
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 265
    new-instance v0, Lcom/android/launcher2/gn;

    move-object v3, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/android/launcher2/gn;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;JLcom/android/launcher2/di;Ljava/lang/String;)V

    .line 297
    invoke-static {v0}, Lcom/android/launcher2/gb;->b(Ljava/lang/Runnable;)V

    .line 298
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/android/launcher2/cu;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 599
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 601
    new-instance v1, Lcom/android/launcher2/gf;

    invoke-direct {v1, v0, p1}, Lcom/android/launcher2/gf;-><init>(Landroid/content/ContentResolver;Lcom/android/launcher2/cu;)V

    .line 623
    invoke-static {v1}, Lcom/android/launcher2/gb;->b(Ljava/lang/Runnable;)V

    .line 624
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/android/launcher2/di;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 362
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 363
    invoke-virtual {p1, v0}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;)V

    .line 364
    iget v1, p1, Lcom/android/launcher2/di;->l:I

    iget v2, p1, Lcom/android/launcher2/di;->m:I

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;II)V

    .line 365
    const-string v1, "updateItemInDatabase"

    invoke-static {p0, v0, p1, v1}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Landroid/content/ContentValues;Lcom/android/launcher2/di;Ljava/lang/String;)V

    .line 366
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 250
    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_13

    .line 252
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-static/range {v0 .. v7}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V

    .line 257
    :goto_12
    return-void

    .line 255
    :cond_13
    invoke-static/range {p0 .. p6}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    goto :goto_12
.end method

.method static a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 332
    iput-wide p2, p1, Lcom/android/launcher2/di;->j:J

    .line 333
    iput p5, p1, Lcom/android/launcher2/di;->l:I

    .line 334
    iput p6, p1, Lcom/android/launcher2/di;->m:I

    .line 335
    iput p7, p1, Lcom/android/launcher2/di;->n:I

    .line 336
    iput p8, p1, Lcom/android/launcher2/di;->o:I

    .line 340
    instance-of v0, p0, Lcom/android/launcher2/Launcher;

    if-eqz v0, :cond_70

    if-gez p4, :cond_70

    .line 341
    const-wide/16 v0, -0x65

    cmp-long v0, p2, v0

    if-nez v0, :cond_70

    move-object v0, p0

    .line 342
    check-cast v0, Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Lcom/android/launcher2/Hotseat;->a(II)I

    move-result v0

    iput v0, p1, Lcom/android/launcher2/di;->k:I

    .line 347
    :goto_23
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 348
    const-string v1, "container"

    iget-wide v2, p1, Lcom/android/launcher2/di;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 349
    const-string v1, "cellX"

    iget v2, p1, Lcom/android/launcher2/di;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 350
    const-string v1, "cellY"

    iget v2, p1, Lcom/android/launcher2/di;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 351
    const-string v1, "spanX"

    iget v2, p1, Lcom/android/launcher2/di;->n:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 352
    const-string v1, "spanY"

    iget v2, p1, Lcom/android/launcher2/di;->o:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 353
    const-string v1, "screen"

    iget v2, p1, Lcom/android/launcher2/di;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 355
    const-string v1, "moveItemInDatabase"

    invoke-static {p0, v0, p1, v1}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Landroid/content/ContentValues;Lcom/android/launcher2/di;Ljava/lang/String;)V

    .line 356
    return-void

    .line 344
    :cond_70
    iput p4, p1, Lcom/android/launcher2/di;->k:I

    goto :goto_23
.end method

.method static a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 476
    iput-wide p2, p1, Lcom/android/launcher2/di;->j:J

    .line 477
    iput p5, p1, Lcom/android/launcher2/di;->l:I

    .line 478
    iput p6, p1, Lcom/android/launcher2/di;->m:I

    .line 485
    iput p4, p1, Lcom/android/launcher2/di;->k:I

    .line 488
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 489
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 490
    invoke-virtual {p1, v1}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;)V

    .line 492
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    .line 493
    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->c()Lcom/android/launcher2/LauncherProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherProvider;->a()J

    move-result-wide v3

    iput-wide v3, p1, Lcom/android/launcher2/di;->h:J

    .line 494
    const-string v0, "_id"

    iget-wide v3, p1, Lcom/android/launcher2/di;->h:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 495
    iget v0, p1, Lcom/android/launcher2/di;->l:I

    iget v3, p1, Lcom/android/launcher2/di;->m:I

    invoke-static {v1, v0, v3}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;II)V

    .line 497
    new-instance v0, Lcom/android/launcher2/gd;

    invoke-direct {v0, v2, p7, v1, p1}, Lcom/android/launcher2/gd;-><init>(Landroid/content/ContentResolver;ZLandroid/content/ContentValues;Lcom/android/launcher2/di;)V

    .line 528
    invoke-static {v0}, Lcom/android/launcher2/gb;->b(Ljava/lang/Runnable;)V

    .line 529
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/gb;Ljava/lang/Runnable;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/android/launcher2/gb;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/gb;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/android/launcher2/gb;->w:Z

    return-void
.end method

.method private static a(Lcom/android/launcher2/hb;)V
    .registers 2
    .parameter

    .prologue
    .line 1883
    sget-object v0, Lcom/android/launcher2/gb;->y:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1884
    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .registers 4
    .parameter

    .prologue
    .line 188
    sget-object v0, Lcom/android/launcher2/gb;->x:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_12

    .line 190
    iget-object v0, p0, Lcom/android/launcher2/gb;->u:Lcom/android/launcher2/az;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/az;->a(Ljava/lang/Runnable;)V

    .line 194
    :goto_11
    return-void

    .line 192
    :cond_12
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_11
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Z
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 373
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 374
    sget-object v1, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    .line 375
    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "title"

    aput-object v3, v2, v5

    const-string v3, "intent"

    aput-object v3, v2, v6

    const-string v3, "title=? and intent=?"

    .line 376
    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {p2, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x0

    .line 374
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 379
    :try_start_24
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_2c

    move-result v1

    .line 381
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 383
    return v1

    .line 380
    :catchall_2c
    move-exception v1

    .line 381
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 382
    throw v1
.end method

.method static synthetic a(Lcom/android/launcher2/gb;)Z
    .registers 2
    .parameter

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/android/launcher2/gb;->z:Z

    return v0
.end method

.method public static b()I
    .registers 1

    .prologue
    .line 541
    sget v0, Lcom/android/launcher2/gb;->F:I

    return v0
.end method

.method private static b(Ljava/util/HashMap;J)Lcom/android/launcher2/cu;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2387
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/cu;

    .line 2388
    if-nez v0, :cond_18

    .line 2390
    new-instance v0, Lcom/android/launcher2/cu;

    invoke-direct {v0}, Lcom/android/launcher2/cu;-><init>()V

    .line 2391
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393
    :cond_18
    return-object v0
.end method

.method static b(Landroid/content/Context;Lcom/android/launcher2/di;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 566
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 567
    iget-wide v1, p1, Lcom/android/launcher2/di;->h:J

    invoke-static {v1, v2}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v1

    .line 568
    new-instance v2, Lcom/android/launcher2/ge;

    invoke-direct {v2, v0, v1, p1}, Lcom/android/launcher2/ge;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/android/launcher2/di;)V

    .line 592
    invoke-static {v2}, Lcom/android/launcher2/gb;->b(Ljava/lang/Runnable;)V

    .line 593
    return-void
.end method

.method static b(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 305
    iput-wide p2, p1, Lcom/android/launcher2/di;->j:J

    .line 306
    iput p5, p1, Lcom/android/launcher2/di;->l:I

    .line 307
    iput p6, p1, Lcom/android/launcher2/di;->m:I

    .line 315
    iput p4, p1, Lcom/android/launcher2/di;->k:I

    .line 318
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 319
    const-string v1, "container"

    iget-wide v2, p1, Lcom/android/launcher2/di;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 320
    const-string v1, "cellX"

    iget v2, p1, Lcom/android/launcher2/di;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 321
    const-string v1, "cellY"

    iget v2, p1, Lcom/android/launcher2/di;->m:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 322
    const-string v1, "screen"

    iget v2, p1, Lcom/android/launcher2/di;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 324
    const-string v1, "moveItemInDatabase"

    invoke-static {p0, v0, p1, v1}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Landroid/content/ContentValues;Lcom/android/launcher2/di;Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method static synthetic b(Lcom/android/launcher2/gb;)V
    .registers 2
    .parameter

    .prologue
    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/gb;->z:Z

    return-void
.end method

.method private static b(Ljava/lang/Runnable;)V
    .registers 3
    .parameter

    .prologue
    .line 199
    sget-object v0, Lcom/android/launcher2/gb;->x:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getThreadId()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v1

    if-ne v0, v1, :cond_10

    .line 200
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 205
    :goto_f
    return-void

    .line 203
    :cond_10
    sget-object v0, Lcom/android/launcher2/gb;->y:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_f
.end method

.method public static c()I
    .registers 1

    .prologue
    .line 545
    sget v0, Lcom/android/launcher2/gb;->G:I

    return v0
.end method

.method static synthetic c(Lcom/android/launcher2/gb;)Lcom/android/launcher2/az;
    .registers 2
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/launcher2/gb;->u:Lcom/android/launcher2/az;

    return-object v0
.end method

.method static synthetic d(Lcom/android/launcher2/gb;)Z
    .registers 2
    .parameter

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/android/launcher2/gb;->A:Z

    return v0
.end method

.method static synthetic e(Lcom/android/launcher2/gb;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lcom/android/launcher2/gb;)Z
    .registers 2
    .parameter

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/android/launcher2/gb;->w:Z

    return v0
.end method

.method static synthetic g(Lcom/android/launcher2/gb;)Ljava/lang/ref/WeakReference;
    .registers 2
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic h(Lcom/android/launcher2/gb;)Lcom/android/launcher2/gp;
    .registers 2
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    return-object v0
.end method

.method static synthetic i(Lcom/android/launcher2/gb;)V
    .registers 2
    .parameter

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    return-void
.end method

.method static synthetic j(Lcom/android/launcher2/gb;)Lcom/android/launcher2/LauncherApplication;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/android/launcher2/gb;->s:Lcom/android/launcher2/LauncherApplication;

    return-object v0
.end method

.method static synthetic k()Ljava/text/Collator;
    .registers 1

    .prologue
    .line 2396
    sget-object v0, Lcom/android/launcher2/gb;->H:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic k(Lcom/android/launcher2/gb;)V
    .registers 5
    .parameter

    .prologue
    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_d
    sget-object v3, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    sget-object v3, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    monitor-exit v2
    :try_end_18
    .catchall {:try_start_d .. :try_end_18} :catchall_21

    new-instance v2, Lcom/android/launcher2/gm;

    invoke-direct {v2, p0, v0, v1}, Lcom/android/launcher2/gm;-><init>(Lcom/android/launcher2/gb;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-direct {p0, v2}, Lcom/android/launcher2/gb;->a(Ljava/lang/Runnable;)V

    return-void

    :catchall_21
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static synthetic l()Landroid/os/HandlerThread;
    .registers 1

    .prologue
    .line 91
    sget-object v0, Lcom/android/launcher2/gb;->x:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic l(Lcom/android/launcher2/gb;)V
    .registers 2
    .parameter

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/gb;->A:Z

    return-void
.end method

.method static synthetic m(Lcom/android/launcher2/gb;)Lcom/android/launcher2/d;
    .registers 2
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    return-object v0
.end method

.method private m()V
    .registers 2

    .prologue
    .line 715
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/gb;->a(Z)V

    .line 720
    invoke-virtual {p0}, Lcom/android/launcher2/gb;->d()V

    .line 721
    return-void
.end method

.method static synthetic n(Lcom/android/launcher2/gb;)I
    .registers 2
    .parameter

    .prologue
    .line 82
    iget v0, p0, Lcom/android/launcher2/gb;->q:I

    return v0
.end method

.method private n()Z
    .registers 4

    .prologue
    .line 758
    const/4 v0, 0x0

    .line 759
    iget-object v1, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    .line 760
    if-eqz v1, :cond_f

    .line 761
    invoke-virtual {v1}, Lcom/android/launcher2/gp;->a()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 762
    const/4 v0, 0x1

    .line 764
    :cond_c
    invoke-virtual {v1}, Lcom/android/launcher2/gp;->c()V

    .line 766
    :cond_f
    return v0
.end method

.method static synthetic o(Lcom/android/launcher2/gb;)Lcom/android/launcher2/da;
    .registers 2
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    return-object v0
.end method

.method static synthetic p(Lcom/android/launcher2/gb;)I
    .registers 2
    .parameter

    .prologue
    .line 83
    iget v0, p0, Lcom/android/launcher2/gb;->r:I

    return v0
.end method


# virtual methods
.method final a(Landroid/content/Context;Landroid/content/Intent;)Lcom/android/launcher2/jd;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2276
    const-string v1, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 2277
    const-string v2, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2278
    const-string v2, "android.intent.extra.shortcut.ICON"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 2280
    if-nez v1, :cond_20

    .line 2282
    const-string v1, "Launcher.Model"

    const-string v2, "Can\'t construct ShorcutInfo with null intent"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2339
    :goto_1f
    return-object v4

    .line 2286
    :cond_20
    const-string v3, "android.intent.action.CALL_PRIVILEGED"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 2287
    const-string v3, "android.intent.action.CALL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2291
    :cond_31
    const/4 v6, 0x0

    .line 2294
    if-eqz v2, :cond_62

    instance-of v3, v2, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_62

    .line 2295
    new-instance v3, Lcom/android/launcher2/ca;

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v3, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {v3, p1}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    move v3, v5

    move-object v12, v4

    move-object v4, v2

    move-object v2, v12

    .line 2322
    :goto_47
    new-instance v6, Lcom/android/launcher2/jd;

    invoke-direct {v6}, Lcom/android/launcher2/jd;-><init>()V

    .line 2324
    if-nez v4, :cond_56

    .line 2325
    iget-object v4, p0, Lcom/android/launcher2/gb;->E:Landroid/graphics/Bitmap;

    invoke-static {v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 2329
    iput-boolean v5, v6, Lcom/android/launcher2/jd;->d:Z

    .line 2332
    :cond_56
    iput-object v4, v6, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    .line 2334
    iput-object v8, v6, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 2335
    iput-object v1, v6, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    .line 2336
    iput-boolean v3, v6, Lcom/android/launcher2/jd;->c:Z

    .line 2337
    iput-object v2, v6, Lcom/android/launcher2/jd;->e:Landroid/content/Intent$ShortcutIconResource;

    move-object v4, v6

    .line 2339
    goto :goto_1f

    .line 2298
    :cond_62
    const-string v2, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    .line 2299
    if-eqz v3, :cond_c2

    instance-of v2, v3, Landroid/content/Intent$ShortcutIconResource;

    if-eqz v2, :cond_c2

    .line 2301
    :try_start_6e
    move-object v0, v3

    check-cast v0, Landroid/content/Intent$ShortcutIconResource;

    move-object v2, v0
    :try_end_72
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_72} :catch_a6

    .line 2303
    :try_start_72
    iget-object v7, p0, Lcom/android/launcher2/gb;->l:Lcom/anddoes/launcher/c/l;

    if-eqz v7, :cond_c0

    .line 2304
    iget-object v7, p0, Lcom/android/launcher2/gb;->l:Lcom/anddoes/launcher/c/l;

    iget-object v9, v2, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    iget-object v10, v2, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    invoke-virtual {v7, v9, v10}, Lcom/anddoes/launcher/c/l;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 2306
    :goto_80
    if-nez v7, :cond_a0

    .line 2307
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 2308
    iget-object v9, v2, Landroid/content/Intent$ShortcutIconResource;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v7

    .line 2310
    iget-object v9, v2, Landroid/content/Intent$ShortcutIconResource;->resourceName:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 2312
    iget-object v10, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    invoke-virtual {v10, v7, v9}, Lcom/android/launcher2/da;->a(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 2311
    invoke-static {v7, p1}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    move v3, v6

    goto :goto_47

    .line 2314
    :cond_a0
    invoke-static {v7, p1}, Lcom/android/launcher2/jj;->a(Landroid/graphics/drawable/Drawable;Landroid/content/Context;)Landroid/graphics/Bitmap;
    :try_end_a3
    .catch Ljava/lang/Exception; {:try_start_72 .. :try_end_a3} :catch_be

    move-result-object v4

    move v3, v6

    goto :goto_47

    .line 2317
    :catch_a6
    move-exception v2

    move-object v2, v4

    :goto_a8
    const-string v7, "Launcher.Model"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Could not load shortcut icon: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v6

    goto :goto_47

    :catch_be
    move-exception v7

    goto :goto_a8

    :cond_c0
    move-object v7, v4

    goto :goto_80

    :cond_c2
    move-object v2, v4

    move v3, v6

    goto :goto_47
.end method

.method final a(Landroid/content/Context;Landroid/content/Intent;III)Lcom/android/launcher2/jd;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2213
    invoke-virtual {p0, p1, p2}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Landroid/content/Intent;)Lcom/android/launcher2/jd;

    move-result-object v1

    .line 2214
    if-nez v1, :cond_8

    .line 2215
    const/4 v1, 0x0

    .line 2219
    :goto_7
    return-object v1

    .line 2217
    :cond_8
    const-wide/16 v2, -0x64

    const/4 v7, 0x1

    move-object v0, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v7}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIIIZ)V

    goto :goto_7
.end method

.method public final a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;)Lcom/android/launcher2/jd;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 2026
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v6, v5

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/gb;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;)Lcom/android/launcher2/jd;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;)Lcom/android/launcher2/jd;
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2036
    const/4 v4, 0x0

    .line 2037
    new-instance v3, Lcom/android/launcher2/jd;

    invoke-direct {v3}, Lcom/android/launcher2/jd;-><init>()V

    .line 2039
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    .line 2040
    if-nez v5, :cond_e

    .line 2041
    const/4 v1, 0x0

    .line 2121
    :goto_d
    return-object v1

    .line 2045
    :cond_e
    :try_start_e
    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 2046
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z
    :try_end_1b
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_e .. :try_end_1b} :catch_1f

    if-nez v1, :cond_38

    .line 2049
    const/4 v1, 0x0

    goto :goto_d

    .line 2052
    :catch_1f
    move-exception v1

    const-string v1, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "getPackInfo failed for package "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2066
    :cond_38
    const/4 v1, 0x0

    .line 2067
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    .line 2068
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct {v2, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2069
    const-string v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2070
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2071
    const/4 v7, 0x0

    invoke-virtual {p1, v2, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 2072
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v1

    :cond_5d
    :goto_5d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_c3

    .line 2079
    if-nez v2, :cond_6a

    .line 2080
    const/4 v1, 0x0

    invoke-virtual {p1, p2, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 2082
    :cond_6a
    if-eqz v2, :cond_f0

    .line 2083
    iget-object v1, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    move-object/from16 v0, p7

    invoke-virtual {v1, v5, v2, v0}, Lcom/android/launcher2/da;->a(Landroid/content/ComponentName;Landroid/content/pm/ResolveInfo;Ljava/util/HashMap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2086
    :goto_74
    if-nez v1, :cond_7e

    .line 2087
    if-eqz p4, :cond_7e

    .line 2088
    move/from16 v0, p5

    invoke-static {p4, v0, p3}, Lcom/android/launcher2/gb;->a(Landroid/database/Cursor;ILandroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2092
    :cond_7e
    if-nez v1, :cond_89

    .line 2093
    iget-object v1, p0, Lcom/android/launcher2/gb;->E:Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2094
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/android/launcher2/jd;->d:Z

    .line 2096
    :cond_89
    iput-object v1, v3, Lcom/android/launcher2/jd;->f:Landroid/graphics/Bitmap;

    .line 2099
    if-eqz v2, :cond_a5

    .line 2100
    invoke-static {v2}, Lcom/android/launcher2/gb;->a(Landroid/content/pm/ResolveInfo;)Landroid/content/ComponentName;

    move-result-object v1

    .line 2101
    if-eqz p7, :cond_de

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_de

    .line 2102
    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    iput-object v1, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 2111
    :cond_a5
    :goto_a5
    iget-object v1, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    if-nez v1, :cond_b3

    .line 2112
    if-eqz p4, :cond_b3

    .line 2113
    move/from16 v0, p6

    invoke-interface {p4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 2117
    :cond_b3
    iget-object v1, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    if-nez v1, :cond_bd

    .line 2118
    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 2120
    :cond_bd
    const/4 v1, 0x0

    iput v1, v3, Lcom/android/launcher2/jd;->i:I

    move-object v1, v3

    .line 2121
    goto/16 :goto_d

    .line 2072
    :cond_c3
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 2073
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 2074
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 2073
    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2075
    invoke-virtual {v8, v6}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5d

    move-object v2, v1

    .line 2076
    goto :goto_5d

    .line 2104
    :cond_de
    iget-object v2, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2, p1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    .line 2105
    if-eqz p7, :cond_a5

    .line 2106
    iget-object v2, v3, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p7

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_a5

    :cond_f0
    move-object v1, v4

    goto :goto_74
.end method

.method public final a()V
    .registers 3

    .prologue
    .line 212
    sget-object v0, Lcom/android/launcher2/gb;->y:Landroid/os/Handler;

    new-instance v1, Lcom/android/launcher2/gl;

    invoke-direct {v1, p0}, Lcom/android/launcher2/gl;-><init>(Lcom/android/launcher2/gb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 218
    return-void
.end method

.method final a(Landroid/content/Context;Lcom/android/launcher2/jd;[B)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2361
    if-eqz p3, :cond_17

    .line 2364
    const/4 v2, 0x0

    :try_start_5
    array-length v3, p3

    invoke-static {p3, v2, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2365
    iget-object v3, p0, Lcom/android/launcher2/gb;->D:Lcom/android/launcher2/da;

    invoke-virtual {p2, v3}, Lcom/android/launcher2/jd;->a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2366
    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z
    :try_end_13
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_13} :catch_33

    move-result v2

    if-eqz v2, :cond_31

    :goto_16
    move v1, v0

    .line 2373
    :cond_17
    :goto_17
    if-eqz v1, :cond_30

    .line 2374
    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "going to save icon bitmap for info="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2377
    invoke-static {p1, p2}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 2379
    :cond_30
    return-void

    :cond_31
    move v0, v1

    .line 2366
    goto :goto_16

    .line 2371
    :catch_33
    move-exception v0

    goto :goto_17
.end method

.method public final a(Lcom/android/launcher2/go;)V
    .registers 4
    .parameter

    .prologue
    .line 630
    iget-object v1, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 631
    :try_start_3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    .line 630
    monitor-exit v1
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_c

    return-void

    :catchall_c
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 724
    iget-object v1, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 727
    :try_start_3
    invoke-direct {p0}, Lcom/android/launcher2/gb;->n()Z

    .line 728
    if-eqz p1, :cond_b

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/gb;->A:Z

    .line 729
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/gb;->z:Z

    .line 724
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_10

    return-void

    :catchall_10
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(ZI)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 770
    iget-object v1, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 777
    :try_start_3
    sget-object v0, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 780
    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_35

    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 783
    if-nez p1, :cond_37

    invoke-direct {p0}, Lcom/android/launcher2/gb;->n()Z

    move-result v0

    if-nez v0, :cond_37

    const/4 v0, 0x0

    .line 784
    :goto_1d
    new-instance v2, Lcom/android/launcher2/gp;

    iget-object v3, p0, Lcom/android/launcher2/gb;->s:Lcom/android/launcher2/LauncherApplication;

    invoke-direct {v2, p0, v3, v0}, Lcom/android/launcher2/gp;-><init>(Lcom/android/launcher2/gb;Landroid/content/Context;Z)V

    iput-object v2, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    .line 785
    if-ltz p2, :cond_39

    iget-boolean v0, p0, Lcom/android/launcher2/gb;->A:Z

    if-eqz v0, :cond_39

    iget-boolean v0, p0, Lcom/android/launcher2/gb;->z:Z

    if-eqz v0, :cond_39

    .line 786
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    invoke-virtual {v0, p2}, Lcom/android/launcher2/gp;->a(I)V

    .line 770
    :cond_35
    :goto_35
    monitor-exit v1

    return-void

    .line 783
    :cond_37
    const/4 v0, 0x1

    goto :goto_1d

    .line 788
    :cond_39
    sget-object v0, Lcom/android/launcher2/gb;->x:Landroid/os/HandlerThread;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/os/HandlerThread;->setPriority(I)V

    .line 789
    sget-object v0, Lcom/android/launcher2/gb;->y:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_46
    .catchall {:try_start_3 .. :try_end_46} :catchall_47

    goto :goto_35

    .line 770
    :catchall_47
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Ljava/util/HashMap;Lcom/android/launcher2/jd;Landroid/database/Cursor;I)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 2345
    iget-boolean v1, p0, Lcom/android/launcher2/gb;->p:Z

    if-nez v1, :cond_6

    .line 2358
    :cond_5
    :goto_5
    return v0

    .line 2354
    :cond_6
    iget-boolean v1, p2, Lcom/android/launcher2/jd;->c:Z

    if-nez v1, :cond_5

    iget-boolean v1, p2, Lcom/android/launcher2/jd;->d:Z

    if-nez v1, :cond_5

    .line 2355
    invoke-interface {p3, p4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2356
    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final d()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 740
    .line 741
    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1d

    .line 742
    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    .line 743
    if-eqz v0, :cond_1d

    .line 745
    invoke-interface {v0}, Lcom/android/launcher2/go;->r()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 746
    const/4 v0, 0x1

    .line 750
    :goto_16
    if-eqz v0, :cond_1c

    .line 751
    const/4 v0, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/gb;->a(ZI)V

    .line 753
    :cond_1c
    return-void

    :cond_1d
    move v0, v1

    goto :goto_16
.end method

.method final e()V
    .registers 4

    .prologue
    .line 797
    sget-object v0, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    .line 798
    sget-object v0, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 801
    sget-object v0, Lcom/android/launcher2/gb;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 803
    :cond_19
    return-void

    .line 798
    :cond_1a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 799
    iget-object v2, p0, Lcom/android/launcher2/gb;->u:Lcom/android/launcher2/az;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/az;->a(Ljava/lang/Runnable;)V

    goto :goto_e
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 806
    iget-object v1, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 807
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    if-eqz v0, :cond_c

    .line 808
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    invoke-virtual {v0}, Lcom/android/launcher2/gp;->c()V

    .line 806
    :cond_c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_e

    return-void

    :catchall_e
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 814
    iget-boolean v0, p0, Lcom/android/launcher2/gb;->A:Z

    return v0
.end method

.method final h()Z
    .registers 3

    .prologue
    .line 818
    iget-object v1, p0, Lcom/android/launcher2/gb;->t:Ljava/lang/Object;

    monitor-enter v1

    .line 819
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    if-eqz v0, :cond_f

    .line 820
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    invoke-virtual {v0}, Lcom/android/launcher2/gp;->b()Z

    move-result v0

    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_12

    .line 823
    :goto_e
    return v0

    .line 818
    :cond_f
    monitor-exit v1

    .line 823
    const/4 v0, 0x0

    goto :goto_e

    .line 818
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()V
    .registers 4

    .prologue
    .line 2490
    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mCallbacks="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2491
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.data"

    iget-object v2, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    iget-object v2, v2, Lcom/android/launcher2/d;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2492
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.added"

    iget-object v2, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    iget-object v2, v2, Lcom/android/launcher2/d;->b:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2493
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.removed"

    iget-object v2, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    iget-object v2, v2, Lcom/android/launcher2/d;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2494
    const-string v0, "Launcher.Model"

    const-string v1, "mAllAppsList.modified"

    iget-object v2, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    iget-object v2, v2, Lcom/android/launcher2/d;->d:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Lcom/android/launcher2/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2495
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    if-eqz v0, :cond_4c

    .line 2496
    iget-object v0, p0, Lcom/android/launcher2/gb;->v:Lcom/android/launcher2/gp;

    invoke-virtual {v0}, Lcom/android/launcher2/gp;->d()V

    .line 2500
    :goto_4b
    return-void

    .line 2498
    :cond_4c
    const-string v0, "Launcher.Model"

    const-string v1, "mLoaderTask=null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4b
.end method

.method public final j()Lcom/android/launcher2/d;
    .registers 2

    .prologue
    .line 2508
    iget-object v0, p0, Lcom/android/launcher2/gb;->C:Lcom/android/launcher2/d;

    return-object v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 643
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 645
    const-string v3, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 646
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 647
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_65

    .line 648
    :cond_1e
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 649
    const-string v4, "android.intent.extra.REPLACING"

    invoke-virtual {p2, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 653
    if-eqz v3, :cond_34

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_35

    .line 712
    :cond_34
    :goto_34
    return-void

    .line 658
    :cond_35
    const-string v5, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_63

    .line 659
    const-string v5, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_57

    .line 661
    if-nez v4, :cond_107

    .line 662
    const/4 v0, 0x3

    .line 674
    :goto_48
    if-eqz v0, :cond_34

    .line 675
    new-instance v4, Lcom/android/launcher2/hb;

    new-array v2, v2, [Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-direct {v4, p0, v0, v2}, Lcom/android/launcher2/hb;-><init>(Lcom/android/launcher2/gb;I[Ljava/lang/String;)V

    invoke-static {v4}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/hb;)V

    goto :goto_34

    .line 666
    :cond_57
    const-string v5, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_107

    .line 667
    if-nez v4, :cond_63

    move v0, v2

    .line 668
    goto :goto_48

    .line 670
    :cond_63
    const/4 v0, 0x2

    goto :goto_48

    .line 678
    :cond_65
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7f

    .line 680
    const-string v0, "android.intent.extra.changed_package_list"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 681
    new-instance v1, Lcom/android/launcher2/hb;

    invoke-direct {v1, p0, v2, v0}, Lcom/android/launcher2/hb;-><init>(Lcom/android/launcher2/gb;I[Ljava/lang/String;)V

    invoke-static {v1}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/hb;)V

    .line 683
    invoke-virtual {p0}, Lcom/android/launcher2/gb;->d()V

    goto :goto_34

    .line 684
    :cond_7f
    const-string v1, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_97

    .line 685
    const-string v0, "android.intent.extra.changed_package_list"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 686
    new-instance v1, Lcom/android/launcher2/hb;

    .line 687
    const/4 v2, 0x4

    invoke-direct {v1, p0, v2, v0}, Lcom/android/launcher2/hb;-><init>(Lcom/android/launcher2/gb;I[Ljava/lang/String;)V

    .line 686
    invoke-static {v1}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/hb;)V

    goto :goto_34

    .line 688
    :cond_97
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a3

    .line 690
    invoke-direct {p0}, Lcom/android/launcher2/gb;->m()V

    goto :goto_34

    .line 691
    :cond_a3
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e4

    .line 695
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 696
    iget v1, p0, Lcom/android/launcher2/gb;->h:I

    iget v2, v0, Landroid/content/res/Configuration;->mcc:I

    if-eq v1, v2, :cond_de

    .line 697
    const-string v1, "Launcher.Model"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Reload apps on config change. curr_mcc:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 698
    iget v3, v0, Landroid/content/res/Configuration;->mcc:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " prevmcc:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/launcher2/gb;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 697
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    invoke-direct {p0}, Lcom/android/launcher2/gb;->m()V

    .line 702
    :cond_de
    iget v0, v0, Landroid/content/res/Configuration;->mcc:I

    iput v0, p0, Lcom/android/launcher2/gb;->h:I

    goto/16 :goto_34

    .line 703
    :cond_e4
    const-string v1, "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f4

    .line 704
    const-string v1, "android.search.action.SEARCHABLES_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 705
    :cond_f4
    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_34

    .line 706
    iget-object v0, p0, Lcom/android/launcher2/gb;->B:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/go;

    .line 707
    if-eqz v0, :cond_34

    .line 708
    invoke-interface {v0}, Lcom/android/launcher2/go;->v()V

    goto/16 :goto_34

    :cond_107
    move v0, v1

    goto/16 :goto_48
.end method
