.class public Lcom/android/launcher2/LauncherApplication;
.super Landroid/app/Application;
.source "SourceFile"


# annotations
.annotation runtime Lorg/acra/a/a;
    d = {
        .enum Lorg/acra/ReportField;->REPORT_ID:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->INSTALLATION_ID:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->APP_VERSION_CODE:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->APP_VERSION_NAME:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->PACKAGE_NAME:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->PHONE_MODEL:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->ANDROID_VERSION:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->BUILD:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->STACK_TRACE:Lorg/acra/ReportField;,
        .enum Lorg/acra/ReportField;->CUSTOM_DATA:Lorg/acra/ReportField;
    }
    i = ""
    j = "http://apex.anddoes.com/ErrorReport.aspx"
    q = .enum Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;
.end annotation


# static fields
.field private static j:Z

.field private static k:Z

.field private static l:F

.field private static m:I

.field private static n:I


# instance fields
.field public a:Lcom/android/launcher2/gb;

.field public b:Lcom/android/launcher2/da;

.field c:Ljava/lang/ref/WeakReference;

.field public d:Lcom/android/launcher2/Launcher;

.field public e:Z

.field public f:Z

.field public g:Lcom/android/launcher2/di;

.field public h:Landroid/view/View;

.field public i:Ljava/util/ArrayList;

.field private final o:Lcom/anddoes/launcher/af;

.field private final p:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 68
    const/16 v0, 0x12c

    sput v0, Lcom/android/launcher2/LauncherApplication;->n:I

    .line 61
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 78
    iput-boolean v0, p0, Lcom/android/launcher2/LauncherApplication;->e:Z

    .line 79
    iput-boolean v0, p0, Lcom/android/launcher2/LauncherApplication;->f:Z

    .line 85
    new-instance v0, Lcom/anddoes/launcher/af;

    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/anddoes/launcher/af;-><init>(Lcom/anddoes/launcher/a;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->o:Lcom/anddoes/launcher/af;

    .line 205
    new-instance v0, Lcom/android/launcher2/ga;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/ga;-><init>(Lcom/android/launcher2/LauncherApplication;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->p:Landroid/database/ContentObserver;

    .line 61
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 245
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 246
    const/4 v1, 0x2

    .line 245
    if-ne v0, v1, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public static d()Ljava/lang/String;
    .registers 1

    .prologue
    .line 237
    const-string v0, "com.android.launcher2.prefs"

    return-object v0
.end method

.method public static e()Z
    .registers 1

    .prologue
    .line 241
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->k:Z

    return v0
.end method

.method public static f()F
    .registers 1

    .prologue
    .line 250
    sget v0, Lcom/android/launcher2/LauncherApplication;->l:F

    return v0
.end method

.method public static g()I
    .registers 1

    .prologue
    .line 254
    sget v0, Lcom/android/launcher2/LauncherApplication;->n:I

    return v0
.end method

.method public static h()Z
    .registers 1

    .prologue
    .line 263
    sget-boolean v0, Lcom/android/launcher2/LauncherApplication;->j:Z

    return v0
.end method

.method private i()Ljava/lang/String;
    .registers 4

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 180
    :try_start_4
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 179
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 181
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_f} :catch_10

    .line 184
    :goto_f
    return-object v0

    .line 183
    :catch_10
    move-exception v0

    const-string v0, "LauncherApplication"

    const-string v1, "Unable to obtain package info"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const-string v0, "Unknown"

    goto :goto_f
.end method


# virtual methods
.method public final a()Lcom/android/launcher2/da;
    .registers 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->b:Lcom/android/launcher2/da;

    return-object v0
.end method

.method final a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/gb;
    .registers 3
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/gb;->a(Lcom/android/launcher2/go;)V

    .line 217
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    return-object v0
.end method

.method final a(Lcom/android/launcher2/LauncherProvider;)V
    .registers 3
    .parameter

    .prologue
    .line 229
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->c:Ljava/lang/ref/WeakReference;

    .line 230
    return-void
.end method

.method public final b()Lcom/android/launcher2/gb;
    .registers 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    return-object v0
.end method

.method public final c()Lcom/android/launcher2/LauncherProvider;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherProvider;

    return-object v0
.end method

.method public onCreate()V
    .registers 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 89
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 91
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_10

    move v0, v1

    .line 92
    :cond_10
    sput-boolean v0, Lcom/android/launcher2/LauncherApplication;->j:Z

    if-nez v0, :cond_d5

    .line 95
    :try_start_14
    invoke-static {p0}, Lorg/acra/ACRA;->init(Landroid/app/Application;)V

    .line 96
    invoke-direct {p0}, Lcom/android/launcher2/LauncherApplication;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/anddoes/launcher/a;->a(Ljava/lang/String;)V

    sget-object v0, Lcom/anddoes/launcher/e;->a:Lcom/anddoes/launcher/e;

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/anddoes/launcher/a;->a(Lcom/anddoes/launcher/e;Ljava/lang/String;)V

    sget-object v0, Lcom/anddoes/launcher/e;->b:Lcom/anddoes/launcher/e;

    invoke-virtual {v4, v0, v2}, Lcom/anddoes/launcher/a;->a(Lcom/anddoes/launcher/e;Ljava/lang/String;)V

    sget-object v0, Lcom/anddoes/launcher/e;->c:Lcom/anddoes/launcher/e;

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Lcom/anddoes/launcher/a;->a(Lcom/anddoes/launcher/e;Ljava/lang/String;)V

    const-string v0, "enable_analytics"

    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09003d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    invoke-interface {v3, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/anddoes/launcher/a;->a(Z)V

    const-string v0, "/ApplicationCreate"

    invoke-virtual {v4, v0}, Lcom/anddoes/launcher/a;->b(Ljava/lang/String;)V

    const-string v0, "previous_app_version"

    const-string v5, "Clean install"

    invoke-interface {v3, v0, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "Clean install"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_70

    const-string v5, "last_check_update"

    invoke-interface {v3, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_70

    const-string v0, "Unknown previous version"

    :cond_70
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "previous_app_version"

    invoke-interface {v5, v6, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a7

    const-string v5, "Installation"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Installed Version: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Prevous Version: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v2, v0, v6}, Lcom/anddoes/launcher/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_a7
    const-string v0, "General"

    const-string v2, "Start up hour"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    const/16 v7, 0xb

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v6, 0x68

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v0, v2, v5, v6}, Lcom/anddoes/launcher/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->o:Lcom/anddoes/launcher/af;

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    :try_end_d5
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_d5} :catch_186

    .line 102
    :cond_d5
    :goto_d5
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090003

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 103
    sput-boolean v0, Lcom/android/launcher2/LauncherApplication;->k:Z

    if-nez v0, :cond_fb

    .line 104
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 105
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701fb

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-boolean v3, Lcom/android/launcher2/LauncherApplication;->k:Z

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/android/launcher2/LauncherApplication;->k:Z

    .line 107
    :cond_fb
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/android/launcher2/LauncherApplication;->l:F

    .line 108
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    sput v0, Lcom/android/launcher2/LauncherApplication;->m:I

    .line 110
    new-instance v0, Lcom/android/launcher2/da;

    invoke-direct {v0, p0}, Lcom/android/launcher2/da;-><init>(Lcom/android/launcher2/LauncherApplication;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->b:Lcom/android/launcher2/da;

    .line 111
    new-instance v0, Lcom/android/launcher2/gb;

    iget-object v2, p0, Lcom/android/launcher2/LauncherApplication;->b:Lcom/android/launcher2/da;

    invoke-direct {v0, p0, v2}, Lcom/android/launcher2/gb;-><init>(Lcom/android/launcher2/LauncherApplication;Lcom/android/launcher2/da;)V

    iput-object v0, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    .line 114
    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 115
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 116
    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 117
    const-string v2, "package"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 118
    iget-object v2, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 119
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 120
    const-string v2, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 121
    const-string v2, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 122
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 123
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 124
    iget-object v2, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 125
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 126
    const-string v2, "android.search.action.GLOBAL_SEARCH_ACTIVITY_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 127
    iget-object v2, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 128
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 129
    const-string v2, "android.search.action.SEARCHABLES_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 130
    iget-object v2, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/LauncherApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 133
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 134
    sget-object v2, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    .line 135
    iget-object v3, p0, Lcom/android/launcher2/LauncherApplication;->p:Landroid/database/ContentObserver;

    .line 134
    invoke-virtual {v0, v2, v1, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 136
    return-void

    :catch_186
    move-exception v0

    goto/16 :goto_d5
.end method

.method public onTerminate()V
    .registers 3

    .prologue
    .line 193
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 195
    invoke-static {p0}, Lcom/anddoes/launcher/a;->a(Landroid/content/Context;)Lcom/anddoes/launcher/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/a;->a(Z)V

    .line 196
    iget-object v0, p0, Lcom/android/launcher2/LauncherApplication;->a:Lcom/android/launcher2/gb;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/LauncherApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 198
    invoke-virtual {p0}, Lcom/android/launcher2/LauncherApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/android/launcher2/LauncherApplication;->p:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 200
    return-void
.end method
