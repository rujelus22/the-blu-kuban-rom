.class final Lcom/android/launcher2/de;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final synthetic a:Ljava/util/Set;

.field private final synthetic b:Landroid/content/SharedPreferences;

.field private final synthetic c:I


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/Set;Landroid/content/SharedPreferences;I)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p2, p0, Lcom/android/launcher2/de;->a:Ljava/util/Set;

    iput-object p3, p0, Lcom/android/launcher2/de;->b:Landroid/content/SharedPreferences;

    iput p4, p0, Lcom/android/launcher2/de;->c:I

    .line 191
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 193
    iget-object v1, p0, Lcom/android/launcher2/de;->a:Ljava/util/Set;

    monitor-enter v1

    .line 194
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/de;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 195
    const-string v2, "apps.new.page"

    iget v3, p0, Lcom/android/launcher2/de;->c:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 196
    const-string v2, "apps.new.list"

    iget-object v3, p0, Lcom/android/launcher2/de;->a:Ljava/util/Set;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 197
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 193
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1e

    return-void

    :catchall_1e
    move-exception v0

    monitor-exit v1

    throw v0
.end method
