.class public Lcom/android/launcher2/di;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public h:J

.field public i:I

.field public j:J

.field public k:I

.field public l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:I

.field public r:Z

.field s:[I


# direct methods
.method public constructor <init>()V
    .registers 5

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide v2, p0, Lcom/android/launcher2/di;->h:J

    .line 53
    iput-wide v2, p0, Lcom/android/launcher2/di;->j:J

    .line 58
    iput v1, p0, Lcom/android/launcher2/di;->k:I

    .line 63
    iput v1, p0, Lcom/android/launcher2/di;->l:I

    .line 68
    iput v1, p0, Lcom/android/launcher2/di;->m:I

    .line 73
    iput v0, p0, Lcom/android/launcher2/di;->n:I

    .line 78
    iput v0, p0, Lcom/android/launcher2/di;->o:I

    .line 83
    iput v0, p0, Lcom/android/launcher2/di;->p:I

    .line 88
    iput v0, p0, Lcom/android/launcher2/di;->q:I

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/di;->r:Z

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/di;->s:[I

    .line 100
    return-void
.end method

.method constructor <init>(Lcom/android/launcher2/di;)V
    .registers 6
    .parameter

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    const/4 v0, 0x1

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide v2, p0, Lcom/android/launcher2/di;->h:J

    .line 53
    iput-wide v2, p0, Lcom/android/launcher2/di;->j:J

    .line 58
    iput v1, p0, Lcom/android/launcher2/di;->k:I

    .line 63
    iput v1, p0, Lcom/android/launcher2/di;->l:I

    .line 68
    iput v1, p0, Lcom/android/launcher2/di;->m:I

    .line 73
    iput v0, p0, Lcom/android/launcher2/di;->n:I

    .line 78
    iput v0, p0, Lcom/android/launcher2/di;->o:I

    .line 83
    iput v0, p0, Lcom/android/launcher2/di;->p:I

    .line 88
    iput v0, p0, Lcom/android/launcher2/di;->q:I

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/di;->r:Z

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/di;->s:[I

    .line 103
    iget-wide v0, p1, Lcom/android/launcher2/di;->h:J

    iput-wide v0, p0, Lcom/android/launcher2/di;->h:J

    .line 104
    iget v0, p1, Lcom/android/launcher2/di;->l:I

    iput v0, p0, Lcom/android/launcher2/di;->l:I

    .line 105
    iget v0, p1, Lcom/android/launcher2/di;->m:I

    iput v0, p0, Lcom/android/launcher2/di;->m:I

    .line 106
    iget v0, p1, Lcom/android/launcher2/di;->n:I

    iput v0, p0, Lcom/android/launcher2/di;->n:I

    .line 107
    iget v0, p1, Lcom/android/launcher2/di;->o:I

    iput v0, p0, Lcom/android/launcher2/di;->o:I

    .line 108
    iget v0, p1, Lcom/android/launcher2/di;->k:I

    iput v0, p0, Lcom/android/launcher2/di;->k:I

    .line 109
    iget v0, p1, Lcom/android/launcher2/di;->i:I

    iput v0, p0, Lcom/android/launcher2/di;->i:I

    .line 110
    iget-wide v0, p1, Lcom/android/launcher2/di;->j:J

    iput-wide v0, p0, Lcom/android/launcher2/di;->j:J

    .line 111
    return-void
.end method

.method static a(Landroid/content/Intent;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 116
    if-eqz p0, :cond_19

    .line 117
    invoke-virtual {p0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 118
    if-nez v0, :cond_16

    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 119
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 121
    :cond_16
    if-eqz v0, :cond_19

    .line 125
    :goto_18
    return-object v0

    :cond_19
    const-string v0, ""

    goto :goto_18
.end method

.method static a(Landroid/content/ContentValues;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 146
    const-string v0, "cellX"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 147
    const-string v0, "cellY"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 148
    return-void
.end method

.method static a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 167
    if-eqz p1, :cond_b

    .line 168
    invoke-static {p1}, Lcom/android/launcher2/di;->a(Landroid/graphics/Bitmap;)[B

    move-result-object v0

    .line 169
    const-string v1, "icon"

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 171
    :cond_b
    return-void
.end method

.method static a(Landroid/graphics/Bitmap;)[B
    .registers 4
    .parameter

    .prologue
    .line 153
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    .line 154
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 156
    :try_start_10
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 157
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 158
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 159
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_20
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_20} :catch_22

    move-result-object v0

    .line 162
    :goto_21
    return-object v0

    .line 161
    :catch_22
    move-exception v0

    const-string v0, "Favorite"

    const-string v1, "Could not write icon"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const/4 v0, 0x0

    goto :goto_21
.end method


# virtual methods
.method a(Landroid/content/ContentValues;)V
    .registers 5
    .parameter

    .prologue
    .line 134
    const-string v0, "itemType"

    iget v1, p0, Lcom/android/launcher2/di;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 135
    iget-boolean v0, p0, Lcom/android/launcher2/di;->r:Z

    if-nez v0, :cond_51

    .line 136
    const-string v0, "container"

    iget-wide v1, p0, Lcom/android/launcher2/di;->j:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 137
    const-string v0, "screen"

    iget v1, p0, Lcom/android/launcher2/di;->k:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 138
    const-string v0, "cellX"

    iget v1, p0, Lcom/android/launcher2/di;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 139
    const-string v0, "cellY"

    iget v1, p0, Lcom/android/launcher2/di;->m:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 140
    const-string v0, "spanX"

    iget v1, p0, Lcom/android/launcher2/di;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 141
    const-string v0, "spanY"

    iget v1, p0, Lcom/android/launcher2/di;->o:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 143
    :cond_51
    return-void
.end method

.method b_()V
    .registers 1

    .prologue
    .line 180
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Item(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/android/launcher2/di;->h:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/di;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " container="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/android/launcher2/di;->j:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 185
    const-string v1, " screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/di;->k:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/di;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " cellY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/di;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " spanX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/di;->n:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 186
    const-string v1, " spanY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/di;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isGesture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/android/launcher2/di;->r:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dropPos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/di;->s:[I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 184
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
