.class final Lcom/android/launcher2/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/launcher2/AppsCustomizeTabHost;

.field private final synthetic b:Ljava/lang/String;

.field private final synthetic c:I


# direct methods
.method constructor <init>(Lcom/android/launcher2/AppsCustomizeTabHost;Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    iput-object p2, p0, Lcom/android/launcher2/aa;->b:Ljava/lang/String;

    iput p3, p0, Lcom/android/launcher2/aa;->c:I

    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/aa;)Lcom/android/launcher2/AppsCustomizeTabHost;
    .registers 2
    .parameter

    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    return-object v0
.end method


# virtual methods
.method public final run()V
    .registers 11

    .prologue
    const/4 v9, 0x2

    const/4 v2, -0x1

    const/high16 v8, 0x3f80

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 270
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_1e

    .line 271
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v0

    if-gtz v0, :cond_24

    .line 272
    :cond_1e
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->b(Lcom/android/launcher2/AppsCustomizeTabHost;)V

    .line 345
    :goto_23
    return-void

    .line 278
    :cond_24
    new-array v1, v9, [I

    .line 279
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->b([I)V

    .line 280
    aget v0, v1, v6

    if-ne v0, v2, :cond_3d

    aget v0, v1, v7

    if-ne v0, v2, :cond_3d

    .line 282
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->b(Lcom/android/launcher2/AppsCustomizeTabHost;)V

    goto :goto_23

    .line 285
    :cond_3d
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 286
    aget v0, v1, v6

    :goto_44
    aget v2, v1, v7

    if-le v0, v2, :cond_ba

    .line 294
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->c(Lcom/android/launcher2/AppsCustomizeTabHost;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getScrollX()I

    move-result v1

    invoke-virtual {v0, v1, v6}, Landroid/widget/FrameLayout;->scrollTo(II)V

    .line 298
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_62
    if-gez v2, :cond_cb

    .line 317
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->d(Lcom/android/launcher2/AppsCustomizeTabHost;)V

    .line 318
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-object v1, p0, Lcom/android/launcher2/aa;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->c(Lcom/android/launcher2/AppsCustomizeTabHost;)Landroid/widget/FrameLayout;

    move-result-object v0

    const-string v1, "alpha"

    new-array v2, v7, [F

    const/4 v3, 0x0

    aput v3, v2, v6

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 322
    new-instance v1, Lcom/android/launcher2/ab;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ab;-><init>(Lcom/android/launcher2/aa;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 334
    iget-object v1, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v1

    const-string v2, "alpha"

    new-array v3, v7, [F

    aput v8, v3, v6

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 335
    new-instance v2, Lcom/android/launcher2/ac;

    invoke-direct {v2, p0}, Lcom/android/launcher2/ac;-><init>(Lcom/android/launcher2/aa;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 341
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 342
    new-array v3, v9, [Landroid/animation/Animator;

    aput-object v0, v3, v6

    aput-object v1, v3, v7

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 343
    iget v0, p0, Lcom/android/launcher2/aa;->c:I

    int-to-long v0, v0

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 344
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_23

    .line 287
    :cond_ba
    iget-object v2, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_44

    .line 299
    :cond_cb
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 300
    instance-of v0, v1, Lcom/android/launcher2/ik;

    if-eqz v0, :cond_122

    move-object v0, v1

    .line 301
    check-cast v0, Lcom/android/launcher2/ik;

    invoke-virtual {v0}, Lcom/android/launcher2/ik;->b()V

    .line 305
    :cond_db
    :goto_db
    invoke-static {v6}, Lcom/android/launcher2/PagedViewWidget;->setDeletePreviewsWhenDetachedFromWindow(Z)V

    .line 306
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->a(Lcom/android/launcher2/AppsCustomizeTabHost;)Lcom/android/launcher2/AppsCustomizePagedView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizePagedView;->removeView(Landroid/view/View;)V

    .line 307
    invoke-static {v7}, Lcom/android/launcher2/PagedViewWidget;->setDeletePreviewsWhenDetachedFromWindow(Z)V

    .line 308
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->c(Lcom/android/launcher2/AppsCustomizeTabHost;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/FrameLayout;->setAlpha(F)V

    .line 309
    iget-object v0, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->c(Lcom/android/launcher2/AppsCustomizeTabHost;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 310
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 311
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 310
    invoke-direct {v0, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 312
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v0, v4, v5, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 313
    iget-object v4, p0, Lcom/android/launcher2/aa;->a:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-static {v4}, Lcom/android/launcher2/AppsCustomizeTabHost;->c(Lcom/android/launcher2/AppsCustomizeTabHost;)Landroid/widget/FrameLayout;

    move-result-object v4

    invoke-virtual {v4, v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 298
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto/16 :goto_62

    .line 302
    :cond_122
    instance-of v0, v1, Lcom/android/launcher2/in;

    if-eqz v0, :cond_db

    move-object v0, v1

    .line 303
    check-cast v0, Lcom/android/launcher2/in;

    invoke-virtual {v0}, Lcom/android/launcher2/in;->b()V

    goto :goto_db
.end method
