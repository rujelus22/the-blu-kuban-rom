.class public final Lcom/android/launcher2/ic;
.super Lcom/android/launcher2/ih;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/PagedView;


# direct methods
.method protected constructor <init>(Lcom/android/launcher2/PagedView;)V
    .registers 2
    .parameter

    .prologue
    .line 2999
    iput-object p1, p0, Lcom/android/launcher2/ic;->a:Lcom/android/launcher2/PagedView;

    invoke-direct {p0, p1}, Lcom/android/launcher2/ih;-><init>(Lcom/android/launcher2/PagedView;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;F)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x3f00

    .line 3002
    const/high16 v0, 0x4270

    mul-float/2addr v0, p2

    .line 3003
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/view/View;->setPivotX(F)V

    .line 3004
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {p1, v1}, Landroid/view/View;->setPivotY(F)V

    .line 3005
    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    .line 3006
    return-void
.end method
