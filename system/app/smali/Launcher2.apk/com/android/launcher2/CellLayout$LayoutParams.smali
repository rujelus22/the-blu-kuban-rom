.class public Lcom/android/launcher2/CellLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public g:I

.field public h:Z

.field public i:Z

.field j:I

.field k:I

.field l:Z


# direct methods
.method public constructor <init>(IIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 3041
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 3003
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 3009
    iput-boolean v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->i:Z

    .line 3042
    iput p1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    .line 3043
    iput p2, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    .line 3044
    iput p3, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 3045
    iput p4, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 3046
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 3021
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3003
    iput-boolean v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 3009
    iput-boolean v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->i:Z

    .line 3022
    iput v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 3023
    iput v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 3024
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 3027
    invoke-direct {p0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3003
    iput-boolean v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    .line 3009
    iput-boolean v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->i:Z

    .line 3028
    iput v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 3029
    iput v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 3030
    return-void
.end method


# virtual methods
.method public final a(IIII)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3049
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->h:Z

    if-eqz v0, :cond_40

    .line 3050
    iget v2, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->f:I

    .line 3051
    iget v3, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->g:I

    .line 3052
    iget-boolean v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    if-eqz v0, :cond_41

    iget v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->c:I

    .line 3053
    :goto_e
    iget-boolean v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->e:Z

    if-eqz v1, :cond_44

    iget v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->d:I

    .line 3055
    :goto_14
    mul-int v4, v2, p1

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v2, p3

    add-int/2addr v2, v4

    .line 3056
    iget v4, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v4

    iget v4, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v4

    .line 3055
    iput v2, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->width:I

    .line 3057
    mul-int v2, v3, p2

    add-int/lit8 v3, v3, -0x1

    mul-int/2addr v3, p4

    add-int/2addr v2, v3

    .line 3058
    iget v3, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->topMargin:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->bottomMargin:I

    sub-int/2addr v2, v3

    .line 3057
    iput v2, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->height:I

    .line 3059
    add-int v2, p1, p3

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->leftMargin:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 3060
    add-int v0, p2, p4

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 3062
    :cond_40
    return-void

    .line 3052
    :cond_41
    iget v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    goto :goto_e

    .line 3053
    :cond_44
    iget v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    goto :goto_14
.end method

.method public getHeight()I
    .registers 2

    .prologue
    .line 3081
    iget v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->height:I

    return v0
.end method

.method public getWidth()I
    .registers 2

    .prologue
    .line 3073
    iget v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->width:I

    return v0
.end method

.method public getX()I
    .registers 2

    .prologue
    .line 3089
    iget v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    return v0
.end method

.method public getY()I
    .registers 2

    .prologue
    .line 3097
    iget v0, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    return v0
.end method

.method public setHeight(I)V
    .registers 2
    .parameter

    .prologue
    .line 3077
    iput p1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->height:I

    .line 3078
    return-void
.end method

.method public setWidth(I)V
    .registers 2
    .parameter

    .prologue
    .line 3069
    iput p1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->width:I

    .line 3070
    return-void
.end method

.method public setX(I)V
    .registers 2
    .parameter

    .prologue
    .line 3085
    iput p1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->j:I

    .line 3086
    return-void
.end method

.method public setY(I)V
    .registers 2
    .parameter

    .prologue
    .line 3093
    iput p1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->k:I

    .line 3094
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 3065
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
