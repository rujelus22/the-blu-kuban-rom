.class final Lcom/android/launcher2/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/TimeInterpolator;


# instance fields
.field final synthetic a:Lcom/android/launcher2/DeleteDropTarget;

.field private b:I

.field private c:F

.field private final synthetic d:J

.field private final synthetic e:I


# direct methods
.method constructor <init>(Lcom/android/launcher2/DeleteDropTarget;JI)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/bh;->a:Lcom/android/launcher2/DeleteDropTarget;

    iput-wide p2, p0, Lcom/android/launcher2/bh;->d:J

    iput p4, p0, Lcom/android/launcher2/bh;->e:I

    .line 448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 449
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/bh;->b:I

    .line 450
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bh;->c:F

    return-void
.end method


# virtual methods
.method public final getInterpolation(F)F
    .registers 7
    .parameter

    .prologue
    .line 454
    iget v0, p0, Lcom/android/launcher2/bh;->b:I

    if-gez v0, :cond_14

    .line 455
    iget v0, p0, Lcom/android/launcher2/bh;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/launcher2/bh;->b:I

    .line 461
    :cond_a
    :goto_a
    const/high16 v0, 0x3f80

    iget v1, p0, Lcom/android/launcher2/bh;->c:F

    add-float/2addr v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0

    .line 456
    :cond_14
    iget v0, p0, Lcom/android/launcher2/bh;->b:I

    if-nez v0, :cond_a

    .line 457
    const/high16 v0, 0x3f00

    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v1

    .line 458
    iget-wide v3, p0, Lcom/android/launcher2/bh;->d:J

    sub-long/2addr v1, v3

    long-to-float v1, v1

    iget v2, p0, Lcom/android/launcher2/bh;->e:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 457
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/bh;->c:F

    .line 459
    iget v0, p0, Lcom/android/launcher2/bh;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/launcher2/bh;->b:I

    goto :goto_a
.end method
