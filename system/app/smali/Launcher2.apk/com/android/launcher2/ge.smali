.class final Lcom/android/launcher2/ge;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final synthetic a:Landroid/content/ContentResolver;

.field private final synthetic b:Landroid/net/Uri;

.field private final synthetic c:Lcom/android/launcher2/di;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Lcom/android/launcher2/di;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/ge;->a:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/android/launcher2/ge;->b:Landroid/net/Uri;

    iput-object p3, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    .line 568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 570
    iget-object v0, p0, Lcom/android/launcher2/ge;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/android/launcher2/ge;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 572
    sget-object v1, Lcom/android/launcher2/gb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    :try_start_b
    iget-object v0, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    iget v0, v0, Lcom/android/launcher2/di;->i:I

    sparse-switch v0, :sswitch_data_52

    .line 587
    :goto_12
    sget-object v0, Lcom/android/launcher2/gb;->c:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    iget-wide v2, v2, Lcom/android/launcher2/di;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 588
    sget-object v0, Lcom/android/launcher2/gb;->g:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    monitor-exit v1

    return-void

    .line 575
    :sswitch_28
    sget-object v0, Lcom/android/launcher2/gb;->f:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    iget-wide v2, v2, Lcom/android/launcher2/di;->h:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    sget-object v0, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_3c
    .catchall {:try_start_b .. :try_end_3c} :catchall_3d

    goto :goto_12

    .line 572
    :catchall_3d
    move-exception v0

    monitor-exit v1

    throw v0

    .line 581
    :sswitch_40
    :try_start_40
    sget-object v0, Lcom/android/launcher2/gb;->d:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_12

    .line 584
    :sswitch_48
    sget-object v2, Lcom/android/launcher2/gb;->e:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/android/launcher2/ge;->c:Lcom/android/launcher2/di;

    check-cast v0, Lcom/android/launcher2/fz;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_51
    .catchall {:try_start_40 .. :try_end_51} :catchall_3d

    goto :goto_12

    .line 573
    :sswitch_data_52
    .sparse-switch
        0x0 -> :sswitch_40
        0x1 -> :sswitch_40
        0x2 -> :sswitch_28
        0x4 -> :sswitch_48
        0x3e9 -> :sswitch_40
    .end sparse-switch
.end method
