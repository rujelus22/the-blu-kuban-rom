.class public Lcom/android/launcher2/InstallShortcutReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/ArrayList;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 51
    sput-object v0, Lcom/android/launcher2/InstallShortcutReceiver;->a:Ljava/util/ArrayList;

    .line 56
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/InstallShortcutReceiver;->b:Z

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static a()V
    .registers 1

    .prologue
    .line 105
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/InstallShortcutReceiver;->b:Z

    .line 106
    return-void
.end method

.method static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 108
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/InstallShortcutReceiver;->b:Z

    .line 109
    invoke-static {p0}, Lcom/android/launcher2/InstallShortcutReceiver;->b(Landroid/content/Context;)V

    .line 110
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/android/launcher2/df;)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 121
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->d()Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-virtual {p0, v0, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 124
    iget-object v1, p1, Lcom/android/launcher2/df;->a:Landroid/content/Intent;

    .line 125
    iget-object v3, p1, Lcom/android/launcher2/df;->b:Landroid/content/Intent;

    .line 126
    iget-object v5, p1, Lcom/android/launcher2/df;->c:Ljava/lang/String;

    .line 129
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher2/LauncherApplication;

    .line 130
    new-array v7, v9, [I

    .line 132
    monitor-enter v8

    .line 133
    :try_start_1a
    invoke-static {p0}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    .line 134
    invoke-static {p0, v5, v3}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)Z

    move-result v5

    move v10, v4

    move v0, v4

    .line 139
    :goto_24
    const/16 v4, 0x13

    if-ge v10, v4, :cond_2a

    if-eqz v0, :cond_2c

    .line 132
    :cond_2a
    monitor-exit v8

    return-void

    .line 140
    :cond_2c
    int-to-float v4, v10

    const/high16 v11, 0x4000

    div-float/2addr v4, v11

    const/high16 v11, 0x3f00

    add-float/2addr v4, v11

    float-to-int v11, v4

    rem-int/lit8 v4, v10, 0x2

    if-ne v4, v9, :cond_4b

    move v4, v9

    :goto_39
    mul-int/2addr v4, v11

    add-int/lit8 v4, v4, 0x2

    .line 141
    if-ltz v4, :cond_47

    const/16 v11, 0x9

    if-ge v4, v11, :cond_47

    move-object v0, p0

    .line 142
    invoke-static/range {v0 .. v7}, Lcom/android/launcher2/InstallShortcutReceiver;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/util/ArrayList;Landroid/content/Intent;IZLandroid/content/SharedPreferences;[I)Z
    :try_end_46
    .catchall {:try_start_1a .. :try_end_46} :catchall_4d

    move-result v0

    .line 139
    :cond_47
    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto :goto_24

    .line 140
    :cond_4b
    const/4 v4, -0x1

    goto :goto_39

    .line 132
    :catchall_4d
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;Ljava/util/ArrayList;Landroid/content/Intent;IZLandroid/content/SharedPreferences;[I)Z
    .registers 23
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 164
    const/4 v2, 0x2

    new-array v8, v2, [I

    .line 165
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v9

    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v10

    filled-new-array {v9, v10}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[Z

    const/4 v3, 0x0

    move v4, v3

    :goto_19
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v4, v3, :cond_97

    invoke-static {v8, v9, v10, v2}, Lcom/android/launcher2/CellLayout;->a([III[[Z)Z

    move-result v2

    if-eqz v2, :cond_108

    .line 166
    if-eqz p3, :cond_10c

    .line 167
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_d8

    .line 168
    const-string v2, "android.intent.action.VIEW"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 178
    :cond_34
    :goto_34
    const-string v2, "duplicate"

    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 179
    if-nez v2, :cond_41

    if-nez p5, :cond_102

    .line 182
    :cond_41
    const-string v2, "apps.new.page"

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 183
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 184
    move/from16 v0, p4

    if-ne v3, v0, :cond_5c

    .line 185
    const-string v3, "apps.new.list"

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 187
    :cond_5c
    monitor-enter v2

    .line 188
    const/4 v3, 0x0

    :try_start_5e
    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 187
    monitor-exit v2
    :try_end_6c
    .catchall {:try_start_5e .. :try_end_6c} :catchall_ff

    .line 191
    new-instance v3, Lcom/android/launcher2/de;

    const-string v4, "setNewAppsThread"

    move-object/from16 v0, p6

    move/from16 v1, p4

    invoke-direct {v3, v4, v2, v0, v1}, Lcom/android/launcher2/de;-><init>(Ljava/lang/String;Ljava/util/Set;Landroid/content/SharedPreferences;I)V

    .line 200
    invoke-virtual {v3}, Lcom/android/launcher2/de;->start()V

    .line 204
    :try_start_7a
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/LauncherApplication;

    .line 205
    invoke-virtual {v2}, Lcom/android/launcher2/LauncherApplication;->b()Lcom/android/launcher2/gb;

    move-result-object v2

    .line 207
    const/4 v3, 0x0

    aget v6, v8, v3

    const/4 v3, 0x1

    aget v7, v8, v3

    move-object v3, p0

    move-object/from16 v4, p1

    move/from16 v5, p4

    .line 205
    invoke-virtual/range {v2 .. v7}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Landroid/content/Intent;III)Lcom/android/launcher2/jd;
    :try_end_92
    .catch Ljava/lang/Exception; {:try_start_7a .. :try_end_92} :catch_10e

    move-result-object v2

    .line 208
    if-nez v2, :cond_106

    .line 209
    const/4 v2, 0x0

    .line 224
    :goto_96
    return v2

    .line 165
    :cond_97
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/launcher2/di;

    iget-wide v5, v3, Lcom/android/launcher2/di;->j:J

    const-wide/16 v11, -0x64

    cmp-long v5, v5, v11

    if-nez v5, :cond_be

    iget v5, v3, Lcom/android/launcher2/di;->k:I

    move/from16 v0, p4

    if-ne v5, v0, :cond_be

    iget v7, v3, Lcom/android/launcher2/di;->l:I

    iget v5, v3, Lcom/android/launcher2/di;->m:I

    iget v11, v3, Lcom/android/launcher2/di;->n:I

    iget v12, v3, Lcom/android/launcher2/di;->o:I

    move v6, v7

    :goto_b6
    if-ltz v6, :cond_be

    add-int v3, v7, v11

    if-ge v6, v3, :cond_be

    if-lt v6, v9, :cond_c3

    :cond_be
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_19

    :cond_c3
    move v3, v5

    :goto_c4
    if-ltz v3, :cond_cc

    add-int v13, v5, v12

    if-ge v3, v13, :cond_cc

    if-lt v3, v10, :cond_d0

    :cond_cc
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_b6

    :cond_d0
    aget-object v13, v2, v6

    const/4 v14, 0x1

    aput-boolean v14, v13, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_c4

    .line 169
    :cond_d8
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 170
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v2

    if-eqz v2, :cond_34

    .line 171
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v2

    const-string v3, "android.intent.category.LAUNCHER"

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 173
    const/high16 v2, 0x1020

    .line 172
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/16 :goto_34

    .line 187
    :catchall_ff
    move-exception v3

    monitor-exit v2

    throw v3

    .line 215
    :cond_102
    const/4 v2, 0x0

    const/4 v3, -0x1

    aput v3, p7, v2

    .line 218
    :cond_106
    :goto_106
    const/4 v2, 0x1

    goto :goto_96

    .line 221
    :cond_108
    const/4 v2, 0x0

    const/4 v3, -0x2

    aput v3, p7, v2

    .line 224
    :cond_10c
    const/4 v2, 0x0

    goto :goto_96

    .line 211
    :catch_10e
    move-exception v2

    goto :goto_106
.end method

.method static b(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 112
    sget-object v0, Lcom/android/launcher2/InstallShortcutReceiver;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 113
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_d

    .line 117
    return-void

    .line 114
    :cond_d
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/df;

    invoke-static {p0, v0}, Lcom/android/launcher2/InstallShortcutReceiver;->a(Landroid/content/Context;Lcom/android/launcher2/df;)V

    .line 115
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_6
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 72
    const-string v0, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 102
    :cond_d
    :goto_d
    return-void

    .line 76
    :cond_e
    const-string v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 77
    if-eqz v0, :cond_d

    .line 82
    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    if-nez v1, :cond_35

    .line 85
    :try_start_20
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 86
    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v3

    .line 87
    invoke-virtual {v3, v1}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_34
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_20 .. :try_end_34} :catch_58

    move-result-object v1

    .line 93
    :cond_35
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v3

    if-lez v3, :cond_52

    .line 94
    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v3

    if-lez v3, :cond_52

    .line 96
    :goto_41
    new-instance v3, Lcom/android/launcher2/df;

    invoke-direct {v3, p2, v1, v0}, Lcom/android/launcher2/df;-><init>(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Intent;)V

    .line 97
    sget-boolean v0, Lcom/android/launcher2/InstallShortcutReceiver;->b:Z

    if-nez v0, :cond_4c

    if-eqz v2, :cond_54

    .line 98
    :cond_4c
    sget-object v0, Lcom/android/launcher2/InstallShortcutReceiver;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 94
    :cond_52
    const/4 v2, 0x1

    goto :goto_41

    .line 100
    :cond_54
    invoke-static {p1, v3}, Lcom/android/launcher2/InstallShortcutReceiver;->a(Landroid/content/Context;Lcom/android/launcher2/df;)V

    goto :goto_d

    .line 89
    :catch_58
    move-exception v0

    goto :goto_d
.end method
