.class final Lcom/android/launcher2/cs;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/co;


# direct methods
.method constructor <init>(Lcom/android/launcher2/co;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    .line 291
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 294
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    invoke-static {v0}, Lcom/android/launcher2/co;->a(Lcom/android/launcher2/co;)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 295
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    invoke-static {v0}, Lcom/android/launcher2/co;->a(Lcom/android/launcher2/co;)Lcom/android/launcher2/CellLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->b(Lcom/android/launcher2/co;)V

    .line 297
    :cond_14
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v0, v0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    if-eqz v0, :cond_67

    .line 298
    const-string v0, "NONE"

    iget-object v1, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    invoke-static {v1}, Lcom/android/launcher2/co;->b(Lcom/android/launcher2/co;)Lcom/android/launcher2/Launcher;

    move-result-object v1

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-object v1, v1, Lcom/anddoes/launcher/preference/f;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 299
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v0, v0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    invoke-static {v0}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/FolderIcon;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 303
    :goto_38
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v0, v0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    iget-object v0, v0, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_67

    .line 304
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v0, v0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    invoke-static {v0}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/FolderIcon;)Landroid/widget/ImageView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v0, v0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    invoke-static {v0}, Lcom/android/launcher2/FolderIcon;->b(Lcom/android/launcher2/FolderIcon;)Lcom/android/launcher2/BubbleTextView;

    move-result-object v0

    .line 306
    new-instance v1, Lcom/android/launcher2/ca;

    iget-object v2, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v2, v2, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    iget-object v2, v2, Lcom/android/launcher2/FolderIcon;->b:Lcom/android/launcher2/cu;

    iget-object v2, v2, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    .line 305
    invoke-virtual {v0, v3, v1, v3, v3}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 309
    :cond_67
    return-void

    .line 301
    :cond_68
    iget-object v0, p0, Lcom/android/launcher2/cs;->a:Lcom/android/launcher2/co;

    iget-object v0, v0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    invoke-static {v0}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/FolderIcon;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_38
.end method
