.class public final Lcom/android/launcher2/az;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/LinkedList;

.field private b:Landroid/os/MessageQueue;

.field private c:Lcom/android/launcher2/bb;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    .line 35
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/az;->b:Landroid/os/MessageQueue;

    .line 36
    new-instance v0, Lcom/android/launcher2/bb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/bb;-><init>(Lcom/android/launcher2/az;B)V

    iput-object v0, p0, Lcom/android/launcher2/az;->c:Lcom/android/launcher2/bb;

    .line 72
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 103
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 105
    :try_start_8
    iget-object v2, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 106
    iget-object v2, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->clear()V

    .line 104
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_8 .. :try_end_13} :catchall_1e

    .line 108
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_21

    .line 111
    return-void

    .line 104
    :catchall_1e
    move-exception v0

    monitor-exit v1

    throw v0

    .line 108
    :cond_21
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 109
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_17
.end method

.method public final a(Ljava/lang/Runnable;)V
    .registers 5
    .parameter

    .prologue
    .line 76
    iget-object v1, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    monitor-enter v1

    .line 77
    :try_start_3
    iget-object v0, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_14

    .line 79
    invoke-virtual {p0}, Lcom/android/launcher2/az;->b()V

    .line 76
    :cond_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_16

    return-void

    :catchall_16
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final b()V
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1b

    .line 115
    iget-object v0, p0, Lcom/android/launcher2/az;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 116
    instance-of v0, v0, Lcom/android/launcher2/ba;

    if-eqz v0, :cond_1c

    .line 117
    iget-object v0, p0, Lcom/android/launcher2/az;->b:Landroid/os/MessageQueue;

    iget-object v1, p0, Lcom/android/launcher2/az;->c:Lcom/android/launcher2/bb;

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    .line 122
    :cond_1b
    :goto_1b
    return-void

    .line 119
    :cond_1c
    iget-object v0, p0, Lcom/android/launcher2/az;->c:Lcom/android/launcher2/bb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/bb;->sendEmptyMessage(I)Z

    goto :goto_1b
.end method
