.class final Lcom/android/launcher2/cg;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/launcher2/Folder;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Folder;)V
    .registers 2
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    .line 444
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 5
    .parameter

    .prologue
    .line 454
    iget-object v0, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/android/launcher2/Folder;->a(Lcom/android/launcher2/Folder;I)V

    .line 455
    iget-object v0, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/Folder;->setLayerType(ILandroid/graphics/Paint;)V

    .line 456
    iget-object v0, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    invoke-static {v0}, Lcom/android/launcher2/Folder;->c(Lcom/android/launcher2/Folder;)V

    .line 457
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 447
    iget-object v0, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    .line 448
    iget-object v1, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    invoke-virtual {v1}, Lcom/android/launcher2/Folder;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0702be

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 449
    iget-object v4, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    iget-object v4, v4, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    iget-object v3, v3, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    .line 448
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 447
    invoke-static {v0, v1}, Lcom/android/launcher2/Folder;->a(Lcom/android/launcher2/Folder;Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/android/launcher2/cg;->a:Lcom/android/launcher2/Folder;

    invoke-static {v0, v5}, Lcom/android/launcher2/Folder;->a(Lcom/android/launcher2/Folder;I)V

    .line 451
    return-void
.end method
