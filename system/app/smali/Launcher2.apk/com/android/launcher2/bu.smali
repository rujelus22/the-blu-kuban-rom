.class public final Lcom/android/launcher2/bu;
.super Landroid/view/View;
.source "SourceFile"


# static fields
.field private static b:F


# instance fields
.field a:Landroid/animation/ValueAnimator;

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Paint;

.field private f:I

.field private g:I

.field private h:Landroid/graphics/Point;

.field private i:Landroid/graphics/Rect;

.field private j:Lcom/android/launcher2/DragLayer;

.field private k:Z

.field private l:F

.field private m:F

.field private n:F

.field private o:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 35
    const/high16 v0, 0x3f80

    sput v0, Lcom/android/launcher2/bu;->b:F

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/Launcher;Landroid/graphics/Bitmap;IIIIF)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/bu;->h:Landroid/graphics/Point;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/bu;->j:Lcom/android/launcher2/DragLayer;

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/bu;->k:Z

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bu;->l:F

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bu;->m:F

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bu;->n:F

    .line 52
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/launcher2/bu;->o:F

    .line 68
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/bu;->j:Lcom/android/launcher2/DragLayer;

    .line 69
    iput p7, p0, Lcom/android/launcher2/bu;->o:F

    .line 71
    invoke-virtual {p0}, Lcom/android/launcher2/bu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 72
    const v1, 0x7f0b005e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v2, v1

    .line 73
    const v1, 0x7f0b005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v3, v1

    .line 74
    const v1, 0x7f0b0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 75
    int-to-float v1, p5

    add-float/2addr v0, v1

    int-to-float v1, p5

    div-float v5, v0, v1

    .line 78
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_8e

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    .line 79
    iget-object v0, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    const-wide/16 v6, 0x96

    invoke-virtual {v0, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 80
    iget-object v6, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/android/launcher2/bv;

    move-object v1, p0

    move v4, p7

    invoke-direct/range {v0 .. v5}, Lcom/android/launcher2/bv;-><init>(Lcom/android/launcher2/bu;FFFF)V

    invoke-virtual {v6, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 105
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p2, v0, v1, p5, p6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    .line 106
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p5, p6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v0}, Lcom/android/launcher2/bu;->setDragRegion(Landroid/graphics/Rect;)V

    .line 109
    iput p3, p0, Lcom/android/launcher2/bu;->f:I

    .line 110
    iput p4, p0, Lcom/android/launcher2/bu;->g:I

    .line 113
    const/4 v0, 0x0

    sget v1, Landroid/view/View$MeasureSpec;->UNSPECIFIED:I

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 114
    invoke-virtual {p0, v0, v0}, Lcom/android/launcher2/bu;->measure(II)V

    .line 115
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    .line 116
    return-void

    .line 78
    nop

    :array_8e
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method static synthetic a(Lcom/android/launcher2/bu;)F
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/android/launcher2/bu;->m:F

    return v0
.end method

.method static synthetic a(Lcom/android/launcher2/bu;F)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/android/launcher2/bu;->m:F

    return-void
.end method

.method static synthetic b(Lcom/android/launcher2/bu;)F
    .registers 2
    .parameter

    .prologue
    .line 51
    iget v0, p0, Lcom/android/launcher2/bu;->n:F

    return v0
.end method

.method static synthetic b(Lcom/android/launcher2/bu;F)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    iput p1, p0, Lcom/android/launcher2/bu;->n:F

    return-void
.end method

.method static synthetic c(Lcom/android/launcher2/bu;F)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 47
    iput p1, p0, Lcom/android/launcher2/bu;->l:F

    return-void
.end method

.method static synthetic f()F
    .registers 1

    .prologue
    .line 35
    sget v0, Lcom/android/launcher2/bu;->b:F

    return v0
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/android/launcher2/bu;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/bu;->o:F

    .line 160
    return-void
.end method

.method public final a(I)V
    .registers 5
    .parameter

    .prologue
    .line 201
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_24

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 202
    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 203
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc0

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 204
    new-instance v1, Lcom/android/launcher2/bw;

    invoke-direct {v1, p0}, Lcom/android/launcher2/bw;-><init>(Lcom/android/launcher2/bu;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 210
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 211
    return-void

    .line 201
    :array_24
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 244
    iget-object v0, p0, Lcom/android/launcher2/bu;->j:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/DragLayer;->addView(Landroid/view/View;)V

    .line 247
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/bu;->setLayerType(ILandroid/graphics/Paint;)V

    .line 250
    new-instance v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    invoke-direct {v0, v2, v2}, Lcom/android/launcher2/DragLayer$LayoutParams;-><init>(II)V

    .line 251
    iget-object v1, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    .line 252
    iget-object v1, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    .line 253
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->c:Z

    .line 254
    invoke-virtual {p0, v0}, Lcom/android/launcher2/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 255
    iget v0, p0, Lcom/android/launcher2/bu;->f:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/bu;->setTranslationX(F)V

    .line 256
    iget v0, p0, Lcom/android/launcher2/bu;->g:I

    sub-int v0, p2, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/bu;->setTranslationY(F)V

    .line 257
    iget-object v0, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 258
    return-void
.end method

.method final b(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 278
    iget v0, p0, Lcom/android/launcher2/bu;->f:I

    sub-int v0, p1, v0

    iget v1, p0, Lcom/android/launcher2/bu;->m:F

    float-to-int v1, v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/bu;->setTranslationX(F)V

    .line 279
    iget v0, p0, Lcom/android/launcher2/bu;->g:I

    sub-int v0, p2, v0

    iget v1, p0, Lcom/android/launcher2/bu;->n:F

    float-to-int v1, v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/bu;->setTranslationY(F)V

    .line 280
    return-void
.end method

.method public final b()Z
    .registers 2

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/android/launcher2/bu;->k:Z

    return v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 262
    iget-object v0, p0, Lcom/android/launcher2/bu;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 264
    :cond_11
    return-void
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 267
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/bu;->n:F

    iput v0, p0, Lcom/android/launcher2/bu;->m:F

    .line 268
    invoke-virtual {p0}, Lcom/android/launcher2/bu;->requestLayout()V

    .line 269
    return-void
.end method

.method final e()V
    .registers 3

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/android/launcher2/bu;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 285
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/bu;->setLayerType(ILandroid/graphics/Paint;)V

    .line 287
    iget-object v0, p0, Lcom/android/launcher2/bu;->j:Lcom/android/launcher2/DragLayer;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    .line 289
    :cond_10
    return-void
.end method

.method public final getDragRegion()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final getDragRegionHeight()I
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public final getDragRegionLeft()I
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public final getDragRegionTop()I
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public final getDragRegionWidth()I
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public final getDragVisualizeOffset()Landroid/graphics/Point;
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/android/launcher2/bu;->h:Landroid/graphics/Point;

    return-object v0
.end method

.method public final getInitialScale()F
    .registers 2

    .prologue
    .line 155
    iget v0, p0, Lcom/android/launcher2/bu;->o:F

    return v0
.end method

.method public final getOffsetY()F
    .registers 2

    .prologue
    .line 119
    iget v0, p0, Lcom/android/launcher2/bu;->n:F

    return v0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/high16 v5, 0x437f

    const/high16 v4, 0x3f80

    const/4 v3, 0x0

    .line 170
    iput-boolean v0, p0, Lcom/android/launcher2/bu;->k:Z

    .line 179
    iget v1, p0, Lcom/android/launcher2/bu;->l:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_62

    iget-object v1, p0, Lcom/android/launcher2/bu;->d:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_62

    .line 180
    :goto_12
    if-eqz v0, :cond_1f

    .line 181
    iget v1, p0, Lcom/android/launcher2/bu;->l:F

    sub-float v1, v4, v1

    mul-float/2addr v1, v5

    float-to-int v1, v1

    .line 182
    iget-object v2, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 184
    :cond_1f
    iget-object v1, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 185
    if-eqz v0, :cond_61

    .line 186
    iget-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/launcher2/bu;->l:F

    mul-float/2addr v1, v5

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 187
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 188
    iget-object v0, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    iget-object v1, p0, Lcom/android/launcher2/bu;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 189
    iget-object v1, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    iget-object v2, p0, Lcom/android/launcher2/bu;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 190
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 191
    iget-object v0, p0, Lcom/android/launcher2/bu;->d:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 192
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 194
    :cond_61
    return-void

    .line 179
    :cond_62
    const/4 v0, 0x0

    goto :goto_12
.end method

.method protected final onMeasure(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 164
    iget-object v0, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/bu;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/bu;->setMeasuredDimension(II)V

    .line 165
    return-void
.end method

.method public final setAlpha(F)V
    .registers 4
    .parameter

    .prologue
    .line 231
    invoke-super {p0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 232
    iget-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x437f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 233
    invoke-virtual {p0}, Lcom/android/launcher2/bu;->invalidate()V

    .line 234
    return-void
.end method

.method public final setColor(I)V
    .registers 5
    .parameter

    .prologue
    .line 214
    iget-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    if-nez v0, :cond_c

    .line 215
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    .line 217
    :cond_c
    if-eqz p1, :cond_1e

    .line 218
    iget-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, p1, v2}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 222
    :goto_1a
    invoke-virtual {p0}, Lcom/android/launcher2/bu;->invalidate()V

    .line 223
    return-void

    .line 220
    :cond_1e
    iget-object v0, p0, Lcom/android/launcher2/bu;->e:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_1a
.end method

.method public final setCrossFadeBitmap(Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lcom/android/launcher2/bu;->d:Landroid/graphics/Bitmap;

    .line 198
    return-void
.end method

.method public final setDragRegion(Landroid/graphics/Rect;)V
    .registers 2
    .parameter

    .prologue
    .line 147
    iput-object p1, p0, Lcom/android/launcher2/bu;->i:Landroid/graphics/Rect;

    .line 148
    return-void
.end method

.method public final setDragVisualizeOffset(Landroid/graphics/Point;)V
    .registers 2
    .parameter

    .prologue
    .line 139
    iput-object p1, p0, Lcom/android/launcher2/bu;->h:Landroid/graphics/Point;

    .line 140
    return-void
.end method
