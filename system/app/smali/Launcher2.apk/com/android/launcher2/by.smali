.class public final Lcom/android/launcher2/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/launcher2/bl;


# instance fields
.field a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/by;->a:I

    .line 74
    check-cast p1, Lcom/android/launcher2/Launcher;

    .line 75
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->g()Lcom/android/launcher2/bk;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/launcher2/bk;->a(Lcom/android/launcher2/bl;)V

    .line 76
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 101
    iget v0, p0, Lcom/android/launcher2/by;->a:I

    if-eqz v0, :cond_1a

    .line 102
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragExit: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/launcher2/by;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_1a
    return-void
.end method

.method public final a(Lcom/android/launcher2/bt;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 94
    iget v0, p0, Lcom/android/launcher2/by;->a:I

    if-eqz v0, :cond_1a

    .line 95
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragEnter: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/launcher2/by;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_1a
    return-void
.end method

.method final b()V
    .registers 4

    .prologue
    .line 79
    iget v0, p0, Lcom/android/launcher2/by;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/launcher2/by;->a:I

    .line 80
    iget v0, p0, Lcom/android/launcher2/by;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_21

    .line 81
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragEnter: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/launcher2/by;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_21
    return-void
.end method

.method final c()V
    .registers 4

    .prologue
    .line 86
    iget v0, p0, Lcom/android/launcher2/by;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/android/launcher2/by;->a:I

    .line 87
    iget v0, p0, Lcom/android/launcher2/by;->a:I

    if-eqz v0, :cond_20

    .line 88
    const-string v0, "DropTarget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDragExit: Drag contract violated: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/launcher2/by;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_20
    return-void
.end method
