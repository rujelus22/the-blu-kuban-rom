.class final Lcom/android/launcher2/jn;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:I

.field c:I


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 357
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method


# virtual methods
.method final a(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter

    .prologue
    .line 364
    iput-object p1, p0, Lcom/android/launcher2/jn;->a:Landroid/graphics/Bitmap;

    .line 365
    iget-object v0, p0, Lcom/android/launcher2/jn;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_7

    .line 369
    :goto_6
    return-void

    .line 367
    :cond_7
    iget-object v0, p0, Lcom/android/launcher2/jn;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/jn;->b:I

    .line 368
    iget-object v0, p0, Lcom/android/launcher2/jn;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/jn;->c:I

    goto :goto_6
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .registers 6
    .parameter

    .prologue
    .line 373
    iget-object v0, p0, Lcom/android/launcher2/jn;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_5

    .line 379
    :goto_4
    return-void

    .line 374
    :cond_5
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    .line 375
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    .line 376
    iget v2, p0, Lcom/android/launcher2/jn;->b:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 377
    iget v2, p0, Lcom/android/launcher2/jn;->c:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 378
    iget-object v2, p0, Lcom/android/launcher2/jn;->a:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_4
.end method

.method public final getOpacity()I
    .registers 2

    .prologue
    .line 383
    const/4 v0, -0x1

    return v0
.end method

.method public final setAlpha(I)V
    .registers 2
    .parameter

    .prologue
    .line 389
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 2
    .parameter

    .prologue
    .line 394
    return-void
.end method
