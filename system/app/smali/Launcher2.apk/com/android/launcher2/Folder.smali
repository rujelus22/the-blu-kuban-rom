.class public Lcom/android/launcher2/Folder;
.super Landroid/widget/LinearLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/android/launcher2/bt;
.implements Lcom/android/launcher2/bx;
.implements Lcom/android/launcher2/cv;


# static fields
.field private static M:Ljava/lang/String;

.field private static N:Ljava/lang/String;


# instance fields
.field private A:Lcom/android/launcher2/c;

.field private B:Lcom/android/launcher2/c;

.field private C:I

.field private D:Landroid/graphics/Rect;

.field private E:Z

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:F

.field private J:F

.field private K:Z

.field private L:Landroid/view/inputmethod/InputMethodManager;

.field private O:Landroid/animation/ObjectAnimator;

.field private P:Landroid/view/ActionMode$Callback;

.field private Q:I

.field protected a:Lcom/android/launcher2/bk;

.field protected b:Lcom/android/launcher2/Launcher;

.field protected c:Lcom/android/launcher2/cu;

.field public d:Lcom/android/launcher2/CellLayout;

.field e:Z

.field f:Z

.field g:Lcom/android/launcher2/FolderEditText;

.field h:Lcom/android/launcher2/hq;

.field i:Lcom/android/launcher2/hq;

.field protected j:Landroid/widget/ScrollView;

.field private k:I

.field private final l:Landroid/view/LayoutInflater;

.field private final m:Lcom/android/launcher2/da;

.field private n:I

.field private o:Z

.field private p:Lcom/android/launcher2/FolderIcon;

.field private q:I

.field private r:I

.field private s:I

.field private t:Ljava/util/ArrayList;

.field private u:Landroid/graphics/drawable/Drawable;

.field private v:Lcom/android/launcher2/jd;

.field private w:Landroid/view/View;

.field private x:[I

.field private y:[I

.field private z:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 121
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/launcher2/Folder;->n:I

    .line 81
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->o:Z

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->t:Ljava/util/ArrayList;

    .line 88
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 91
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->f:Z

    .line 92
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher2/Folder;->x:[I

    .line 93
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher2/Folder;->y:[I

    .line 94
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/launcher2/Folder;->z:[I

    .line 95
    new-instance v0, Lcom/android/launcher2/c;

    invoke-direct {v0}, Lcom/android/launcher2/c;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->A:Lcom/android/launcher2/c;

    .line 96
    new-instance v0, Lcom/android/launcher2/c;

    invoke-direct {v0}, Lcom/android/launcher2/c;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->D:Landroid/graphics/Rect;

    .line 99
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->E:Z

    .line 100
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->F:Z

    .line 101
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->G:Z

    .line 102
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->H:Z

    .line 107
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->K:Z

    .line 190
    new-instance v0, Lcom/android/launcher2/cd;

    invoke-direct {v0, p0}, Lcom/android/launcher2/cd;-><init>(Lcom/android/launcher2/Folder;)V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->P:Landroid/view/ActionMode$Callback;

    .line 598
    new-instance v0, Lcom/android/launcher2/ce;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ce;-><init>(Lcom/android/launcher2/Folder;)V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->h:Lcom/android/launcher2/hq;

    .line 701
    new-instance v0, Lcom/android/launcher2/cf;

    invoke-direct {v0, p0}, Lcom/android/launcher2/cf;-><init>(Lcom/android/launcher2/Folder;)V

    iput-object v0, p0, Lcom/android/launcher2/Folder;->i:Lcom/android/launcher2/hq;

    .line 122
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Folder;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 123
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Folder;->l:Landroid/view/LayoutInflater;

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/LauncherApplication;

    invoke-virtual {v0}, Lcom/android/launcher2/LauncherApplication;->a()Lcom/android/launcher2/da;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Folder;->m:Lcom/android/launcher2/da;

    .line 126
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 127
    const v0, 0x7f0a001c

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->q:I

    .line 128
    const v0, 0x7f0a001d

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->r:I

    .line 129
    const v0, 0x7f0a001e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->s:I

    .line 130
    iget v0, p0, Lcom/android/launcher2/Folder;->q:I

    if-ltz v0, :cond_95

    iget v0, p0, Lcom/android/launcher2/Folder;->r:I

    if-ltz v0, :cond_95

    iget v0, p0, Lcom/android/launcher2/Folder;->s:I

    if-gez v0, :cond_a8

    .line 131
    :cond_95
    invoke-static {}, Lcom/android/launcher2/gb;->b()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->q:I

    .line 132
    invoke-static {}, Lcom/android/launcher2/gb;->c()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->r:I

    .line 133
    iget v0, p0, Lcom/android/launcher2/Folder;->q:I

    iget v2, p0, Lcom/android/launcher2/Folder;->r:I

    mul-int/2addr v0, v2

    iput v0, p0, Lcom/android/launcher2/Folder;->s:I

    .line 137
    :cond_a8
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 136
    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/launcher2/Folder;->L:Landroid/view/inputmethod/InputMethodManager;

    .line 139
    const v0, 0x7f0a0017

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->k:I

    .line 141
    sget-object v0, Lcom/android/launcher2/Folder;->M:Ljava/lang/String;

    if-nez v0, :cond_cc

    .line 142
    const v0, 0x7f070275

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/Folder;->M:Ljava/lang/String;

    .line 144
    :cond_cc
    sget-object v0, Lcom/android/launcher2/Folder;->N:Ljava/lang/String;

    if-nez v0, :cond_d9

    .line 145
    const v0, 0x7f0702b8

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/Folder;->N:Ljava/lang/String;

    .line 147
    :cond_d9
    check-cast p1, Lcom/android/launcher2/Launcher;

    iput-object p1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    .line 151
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->setFocusableInTouchMode(Z)V

    .line 152
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->Q:I

    .line 153
    return-void
.end method

.method private a(I)Landroid/view/View;
    .registers 3
    .parameter

    .prologue
    .line 953
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/launcher2/ja;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;)Lcom/android/launcher2/Folder;
    .registers 4
    .parameter

    .prologue
    .line 418
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030034

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/Folder;

    return-object v0
.end method

.method static synthetic a(Lcom/android/launcher2/Folder;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 78
    iput p1, p0, Lcom/android/launcher2/Folder;->n:I

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Folder;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/Folder;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 956
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer;

    if-eqz v0, :cond_d

    invoke-virtual {v0, p0}, Lcom/android/launcher2/DragLayer;->removeView(Landroid/view/View;)V

    :cond_d
    iget-object v0, p0, Lcom/android/launcher2/Folder;->a:Lcom/android/launcher2/bk;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/bk;->b(Lcom/android/launcher2/bx;)V

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->clearFocus()V

    iget-object v0, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->requestFocus()Z

    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->o:Z

    if-eqz v0, :cond_27

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->setupContentForNumItems(I)V

    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->o:Z

    :cond_27
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    if-gt v0, v2, :cond_38

    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->E:Z

    if-nez v0, :cond_3b

    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->G:Z

    if-nez v0, :cond_3b

    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->c(Z)V

    :cond_38
    :goto_38
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->G:Z

    return-void

    :cond_3b
    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->E:Z

    if-eqz v0, :cond_38

    iput-boolean v2, p0, Lcom/android/launcher2/Folder;->F:Z

    goto :goto_38
.end method

.method static synthetic a(Lcom/android/launcher2/Folder;[I[I)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    .line 612
    const/4 v5, 0x0

    const/high16 v8, 0x41f0

    const/4 v0, 0x1

    aget v0, p2, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    if-gt v0, v1, :cond_1b

    const/4 v0, 0x1

    aget v0, p2, v0

    const/4 v1, 0x1

    aget v1, p1, v1

    if-ne v0, v1, :cond_3a

    const/4 v0, 0x0

    aget v0, p2, v0

    const/4 v1, 0x0

    aget v1, p1, v1

    if-le v0, v1, :cond_3a

    :cond_1b
    const/4 v0, 0x1

    :goto_1c
    if-eqz v0, :cond_96

    const/4 v0, 0x0

    aget v0, p1, v0

    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_3c

    const/4 v0, 0x1

    :goto_2c
    if-eqz v0, :cond_3e

    const/4 v0, 0x1

    aget v0, p1, v0

    add-int/lit8 v0, v0, 0x1

    :goto_33
    move v9, v0

    :goto_34
    const/4 v0, 0x1

    aget v0, p2, v0

    if-le v9, v0, :cond_42

    :cond_39
    return-void

    :cond_3a
    const/4 v0, 0x0

    goto :goto_1c

    :cond_3c
    const/4 v0, 0x0

    goto :goto_2c

    :cond_3e
    const/4 v0, 0x1

    aget v0, p1, v0

    goto :goto_33

    :cond_42
    const/4 v0, 0x1

    aget v0, p1, v0

    if-ne v9, v0, :cond_61

    const/4 v0, 0x0

    aget v0, p1, v0

    add-int/lit8 v0, v0, 0x1

    :goto_4c
    const/4 v1, 0x1

    aget v1, p2, v1

    if-ge v9, v1, :cond_63

    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v10, v1

    :goto_5a
    move v11, v0

    :goto_5b
    if-le v11, v10, :cond_68

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_34

    :cond_61
    const/4 v0, 0x0

    goto :goto_4c

    :cond_63
    const/4 v1, 0x0

    aget v1, p2, v1

    move v10, v1

    goto :goto_5a

    :cond_68
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v11, v9}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    const/4 v2, 0x0

    aget v2, p1, v2

    const/4 v3, 0x1

    aget v3, p1, v3

    const/16 v4, 0xe6

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    move-result v0

    if-eqz v0, :cond_105

    const/4 v0, 0x0

    aput v11, p1, v0

    const/4 v0, 0x1

    aput v9, p1, v0

    int-to-float v0, v5

    add-float/2addr v0, v8

    float-to-int v5, v0

    float-to-double v0, v8

    const-wide v2, 0x3feccccccccccccdL

    mul-double/2addr v0, v2

    double-to-float v0, v0

    :goto_91
    add-int/lit8 v1, v11, 0x1

    move v8, v0

    move v11, v1

    goto :goto_5b

    :cond_96
    const/4 v0, 0x0

    aget v0, p1, v0

    if-nez v0, :cond_c1

    const/4 v0, 0x1

    :goto_9c
    if-eqz v0, :cond_c3

    const/4 v0, 0x1

    aget v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    :goto_a3
    move v9, v0

    :goto_a4
    const/4 v0, 0x1

    aget v0, p2, v0

    if-lt v9, v0, :cond_39

    const/4 v0, 0x1

    aget v0, p1, v0

    if-ne v9, v0, :cond_c7

    const/4 v0, 0x0

    aget v0, p1, v0

    add-int/lit8 v0, v0, -0x1

    :goto_b3
    const/4 v1, 0x1

    aget v1, p2, v1

    if-le v9, v1, :cond_d0

    const/4 v1, 0x0

    move v10, v1

    :goto_ba
    move v11, v0

    :goto_bb
    if-ge v11, v10, :cond_d5

    add-int/lit8 v0, v9, -0x1

    move v9, v0

    goto :goto_a4

    :cond_c1
    const/4 v0, 0x0

    goto :goto_9c

    :cond_c3
    const/4 v0, 0x1

    aget v0, p1, v0

    goto :goto_a3

    :cond_c7
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_b3

    :cond_d0
    const/4 v1, 0x0

    aget v1, p2, v1

    move v10, v1

    goto :goto_ba

    :cond_d5
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v11, v9}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    const/4 v2, 0x0

    aget v2, p1, v2

    const/4 v3, 0x1

    aget v3, p1, v3

    const/16 v4, 0xe6

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IIIIZZ)Z

    move-result v0

    if-eqz v0, :cond_103

    const/4 v0, 0x0

    aput v11, p1, v0

    const/4 v0, 0x1

    aput v9, p1, v0

    int-to-float v0, v5

    add-float/2addr v0, v8

    float-to-int v5, v0

    float-to-double v0, v8

    const-wide v2, 0x3feccccccccccccdL

    mul-double/2addr v0, v2

    double-to-float v0, v0

    :goto_fe
    add-int/lit8 v1, v11, -0x1

    move v8, v0

    move v11, v1

    goto :goto_bb

    :cond_103
    move v0, v8

    goto :goto_fe

    :cond_105
    move v0, v8

    goto :goto_91
.end method

.method private a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 473
    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 475
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 476
    const/16 v1, 0x20

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 477
    invoke-virtual {p0, v1}, Lcom/android/launcher2/Folder;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 478
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 479
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 481
    :cond_25
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 350
    .line 351
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    .line 352
    :goto_7
    if-lt v3, v4, :cond_1d

    .line 359
    new-instance v0, Lcom/android/launcher2/ck;

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/android/launcher2/ck;-><init>(Lcom/android/launcher2/Folder;I)V

    .line 360
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 361
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v3

    move v1, v2

    .line 362
    :goto_1a
    if-lt v1, v4, :cond_2e

    .line 369
    return-void

    .line 353
    :cond_1d
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 354
    iget v5, v0, Lcom/android/launcher2/di;->l:I

    if-le v5, v1, :cond_40

    .line 355
    iget v0, v0, Lcom/android/launcher2/di;->l:I

    .line 352
    :goto_29
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_7

    .line 363
    :cond_2e
    rem-int v2, v1, v3

    .line 364
    div-int v5, v1, v3

    .line 365
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 366
    iput v2, v0, Lcom/android/launcher2/di;->l:I

    .line 367
    iput v5, v0, Lcom/android/launcher2/di;->m:I

    .line 362
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1a

    :cond_40
    move v0, v1

    goto :goto_29
.end method

.method static synthetic a(Lcom/android/launcher2/Folder;)[I
    .registers 2
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lcom/android/launcher2/Folder;->z:[I

    return-object v0
.end method

.method static synthetic b(Lcom/android/launcher2/Folder;)[I
    .registers 2
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/android/launcher2/Folder;->x:[I

    return-object v0
.end method

.method static synthetic c(Lcom/android/launcher2/Folder;)V
    .registers 3
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 483
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v1, v1}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_c
    return-void
.end method

.method private c(Z)V
    .registers 11
    .parameter

    .prologue
    .line 985
    const/4 v1, 0x0

    .line 987
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_14

    .line 988
    iget-object v0, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-object v0, v0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    move-object v1, v0

    .line 992
    :cond_14
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->j:J

    iget-object v4, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v4, v4, Lcom/android/launcher2/cu;->k:I

    invoke-virtual {v0, v2, v3, v4}, Lcom/android/launcher2/Launcher;->a(JI)Lcom/android/launcher2/CellLayout;

    move-result-object v7

    .line 993
    iget-object v0, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v7, v0}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 994
    iget-object v0, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    instance-of v0, v0, Lcom/android/launcher2/bx;

    if-eqz v0, :cond_36

    .line 995
    iget-object v2, p0, Lcom/android/launcher2/Folder;->a:Lcom/android/launcher2/bk;

    iget-object v0, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    check-cast v0, Lcom/android/launcher2/bx;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/bk;->b(Lcom/android/launcher2/bx;)V

    .line 997
    :cond_36
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/cu;)V

    .line 999
    if-eqz v1, :cond_54

    .line 1000
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->j:J

    .line 1001
    iget-object v4, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v4, v4, Lcom/android/launcher2/cu;->k:I

    iget-object v5, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v5, v5, Lcom/android/launcher2/cu;->l:I

    iget-object v6, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v6, v6, Lcom/android/launcher2/cu;->m:I

    .line 1000
    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    .line 1003
    :cond_54
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-static {v0, v2}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 1006
    if-eqz v1, :cond_88

    if-eqz p1, :cond_88

    .line 1007
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    .line 1008
    check-cast v1, Lcom/android/launcher2/jd;

    .line 1007
    invoke-virtual {v0, v7, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/ViewGroup;Lcom/android/launcher2/jd;)Landroid/view/View;

    move-result-object v1

    .line 1010
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->j:J

    iget-object v4, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v4, v4, Lcom/android/launcher2/cu;->k:I

    iget-object v5, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v5, v5, Lcom/android/launcher2/cu;->l:I

    .line 1011
    iget-object v6, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v6, v6, Lcom/android/launcher2/cu;->m:I

    iget-object v7, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v7, v7, Lcom/android/launcher2/cu;->n:I

    iget-object v8, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget v8, v8, Lcom/android/launcher2/cu;->o:I

    .line 1010
    invoke-virtual/range {v0 .. v8}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;JIIIII)V

    .line 1013
    :cond_88
    return-void
.end method

.method private c(Lcom/android/launcher2/di;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 545
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 546
    iget-object v3, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    iget v4, p1, Lcom/android/launcher2/di;->n:I

    iget v5, p1, Lcom/android/launcher2/di;->o:I

    invoke-virtual {v3, v2, v4, v5}, Lcom/android/launcher2/CellLayout;->a([III)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 547
    aget v1, v2, v1

    iput v1, p1, Lcom/android/launcher2/di;->l:I

    .line 548
    aget v1, v2, v0

    iput v1, p1, Lcom/android/launcher2/di;->m:I

    .line 551
    :goto_19
    return v0

    :cond_1a
    move v0, v1

    goto :goto_19
.end method

.method static synthetic d(Lcom/android/launcher2/Folder;)Landroid/animation/ObjectAnimator;
    .registers 2
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lcom/android/launcher2/Folder;->O:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method private d(Lcom/android/launcher2/di;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 556
    check-cast p1, Lcom/android/launcher2/jd;

    .line 558
    iget-object v1, p0, Lcom/android/launcher2/Folder;->l:Landroid/view/LayoutInflater;

    const v2, 0x7f030007

    invoke-virtual {v1, v2, p0, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/BubbleTextView;

    .line 560
    new-instance v2, Lcom/android/launcher2/ca;

    iget-object v3, p0, Lcom/android/launcher2/Folder;->m:Lcom/android/launcher2/da;

    invoke-virtual {p1, v3}, Lcom/android/launcher2/jd;->a(Lcom/android/launcher2/da;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/launcher2/ca;-><init>(Landroid/graphics/Bitmap;)V

    .line 559
    invoke-virtual {v1, v4, v2, v4, v4}, Lcom/android/launcher2/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 561
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v2, v2, Lcom/anddoes/launcher/preference/f;->ak:Z

    if-nez v2, :cond_2b

    .line 562
    iget-object v2, p1, Lcom/android/launcher2/jd;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    .line 564
    :cond_2b
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    invoke-virtual {v2, v1}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 565
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, v2, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 566
    const v3, 0x7f08000c

    const-string v4, "folder_item_text_color"

    .line 565
    invoke-virtual {v2, v3, v4}, Lcom/anddoes/launcher/c/l;->a(ILjava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/android/launcher2/BubbleTextView;->a:I

    .line 568
    invoke-virtual {v1, p1}, Lcom/android/launcher2/BubbleTextView;->setTag(Ljava/lang/Object;)V

    .line 570
    invoke-virtual {v1, p0}, Lcom/android/launcher2/BubbleTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 571
    invoke-virtual {v1, p0}, Lcom/android/launcher2/BubbleTextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 575
    iget-object v2, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    iget v3, p1, Lcom/android/launcher2/jd;->l:I

    iget v4, p1, Lcom/android/launcher2/jd;->m:I

    invoke-virtual {v2, v3, v4}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_72

    iget v2, p1, Lcom/android/launcher2/jd;->l:I

    if-ltz v2, :cond_72

    iget v2, p1, Lcom/android/launcher2/jd;->m:I

    if-ltz v2, :cond_72

    .line 576
    iget v2, p1, Lcom/android/launcher2/jd;->l:I

    iget-object v3, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v3

    if-ge v2, v3, :cond_72

    iget v2, p1, Lcom/android/launcher2/jd;->m:I

    iget-object v3, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v3

    if-lt v2, v3, :cond_81

    .line 578
    :cond_72
    const-string v2, "Launcher.Folder"

    const-string v3, "Folder order not properly persisted during bind"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->c(Lcom/android/launcher2/di;)Z

    move-result v2

    if-nez v2, :cond_81

    move v5, v0

    .line 589
    :goto_80
    return v5

    .line 585
    :cond_81
    new-instance v4, Lcom/android/launcher2/CellLayout$LayoutParams;

    iget v0, p1, Lcom/android/launcher2/jd;->l:I

    iget v2, p1, Lcom/android/launcher2/jd;->m:I

    iget v3, p1, Lcom/android/launcher2/jd;->n:I

    iget v6, p1, Lcom/android/launcher2/jd;->o:I

    invoke-direct {v4, v0, v2, v3, v6}, Lcom/android/launcher2/CellLayout$LayoutParams;-><init>(IIII)V

    .line 586
    new-instance v0, Lcom/android/launcher2/cw;

    invoke-direct {v0}, Lcom/android/launcher2/cw;-><init>()V

    invoke-virtual {v1, v0}, Lcom/android/launcher2/BubbleTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 588
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    const/4 v2, -0x1

    iget-wide v6, p1, Lcom/android/launcher2/jd;->h:J

    long-to-int v3, v6

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IILcom/android/launcher2/CellLayout$LayoutParams;Z)Z

    goto :goto_80
.end method

.method private getSuitableHeight()I
    .registers 6

    .prologue
    const/4 v2, 0x1

    .line 1156
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getDesiredHeight()I

    move-result v3

    .line 1157
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getChildCount()I

    move-result v0

    if-lez v0, :cond_60

    .line 1159
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1160
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1161
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 1160
    sub-int/2addr v1, v4

    .line 1162
    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    .line 1160
    sub-int v0, v1, v0

    .line 1166
    :goto_2d
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingBottom()I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v1, v3

    .line 1169
    iget v4, p0, Lcom/android/launcher2/Folder;->C:I

    .line 1168
    add-int/2addr v1, v4

    .line 1171
    iget v4, p0, Lcom/android/launcher2/Folder;->C:I

    add-int/2addr v3, v4

    if-lt v3, v0, :cond_7f

    .line 1172
    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/CellLayout;->a(I)I

    move-result v1

    :goto_45
    iget v3, p0, Lcom/android/launcher2/Folder;->C:I

    add-int/2addr v1, v3

    if-lt v1, v0, :cond_76

    add-int/lit8 v0, v2, -0x1

    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/CellLayout;->a(I)I

    move-result v0

    .line 1173
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 1174
    iget v1, p0, Lcom/android/launcher2/Folder;->C:I

    .line 1173
    add-int/2addr v0, v1

    .line 1177
    :goto_5f
    return v0

    .line 1164
    :cond_60
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/Workspace;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->i()Lcom/android/launcher2/Hotseat;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Hotseat;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_2d

    .line 1172
    :cond_76
    add-int/lit8 v2, v2, 0x1

    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/CellLayout;->a(I)I

    move-result v1

    goto :goto_45

    :cond_7f
    move v0, v1

    goto :goto_5f
.end method

.method public static j()Z
    .registers 1

    .prologue
    .line 828
    const/4 v0, 0x0

    return v0
.end method

.method private l()V
    .registers 10

    .prologue
    const/4 v4, 0x0

    .line 775
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemsInReadingOrder()Ljava/util/ArrayList;

    move-result-object v8

    move v7, v4

    .line 776
    :goto_6
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v7, v0, :cond_d

    .line 782
    return-void

    .line 777
    :cond_d
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 778
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/di;

    .line 779
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->h:J

    .line 780
    iget v5, v1, Lcom/android/launcher2/di;->l:I

    iget v6, v1, Lcom/android/launcher2/di;->m:I

    .line 779
    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    .line 776
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_6
.end method

.method private m()V
    .registers 12

    .prologue
    const/high16 v10, 0x3f80

    .line 832
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    .line 834
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getDesiredWidth()I

    move-result v2

    add-int v3, v1, v2

    .line 835
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->getSuitableHeight()I

    move-result v4

    .line 836
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    const v2, 0x7f0d0035

    invoke-virtual {v1, v2}, Lcom/android/launcher2/Launcher;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/DragLayer;

    .line 838
    iget-object v2, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    iget-object v5, p0, Lcom/android/launcher2/Folder;->D:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v5}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 840
    iget-object v2, p0, Lcom/android/launcher2/Folder;->D:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    .line 841
    iget-object v5, p0, Lcom/android/launcher2/Folder;->D:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerY()I

    move-result v5

    .line 842
    div-int/lit8 v6, v3, 0x2

    sub-int v6, v2, v6

    .line 843
    div-int/lit8 v2, v4, 0x2

    sub-int/2addr v5, v2

    .line 845
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/launcher2/Workspace;->getCurrentPage()I

    move-result v7

    .line 848
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/android/launcher2/Workspace;->setFinalScrollForPageChange(I)V

    .line 850
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v2}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/android/launcher2/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/launcher2/CellLayout;

    .line 851
    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v2

    .line 852
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 853
    invoke-virtual {v1, v2, v8}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;Landroid/graphics/Rect;)F

    .line 855
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/android/launcher2/Workspace;->g(I)V

    .line 858
    iget v1, v8, Landroid/graphics/Rect;->left:I

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 859
    iget v2, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v7

    add-int/2addr v2, v7

    sub-int/2addr v2, v3

    .line 858
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 860
    iget v2, v8, Landroid/graphics/Rect;->top:I

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 861
    iget v7, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v9

    add-int/2addr v7, v9

    sub-int/2addr v7, v4

    .line 860
    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 863
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v7

    if-lt v3, v7, :cond_aa

    .line 864
    iget v1, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v7

    sub-int/2addr v7, v3

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v1, v7

    .line 866
    :cond_aa
    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v7

    if-lt v4, v7, :cond_ba

    .line 867
    iget v2, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v7

    sub-int/2addr v7, v4

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v2, v7

    .line 870
    :cond_ba
    div-int/lit8 v7, v3, 0x2

    sub-int/2addr v6, v1

    add-int/2addr v6, v7

    .line 871
    div-int/lit8 v7, v4, 0x2

    sub-int/2addr v5, v2

    add-int/2addr v5, v7

    .line 872
    int-to-float v7, v6

    invoke-virtual {p0, v7}, Lcom/android/launcher2/Folder;->setPivotX(F)V

    .line 873
    int-to-float v7, v5

    invoke-virtual {p0, v7}, Lcom/android/launcher2/Folder;->setPivotY(F)V

    .line 874
    iget-object v7, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v7}, Lcom/android/launcher2/FolderIcon;->getMeasuredWidth()I

    move-result v7

    int-to-float v7, v7

    .line 875
    int-to-float v6, v6

    mul-float/2addr v6, v10

    int-to-float v8, v3

    div-float/2addr v6, v8

    mul-float/2addr v6, v7

    float-to-int v6, v6

    int-to-float v6, v6

    .line 874
    iput v6, p0, Lcom/android/launcher2/Folder;->I:F

    .line 876
    iget-object v6, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v6}, Lcom/android/launcher2/FolderIcon;->getMeasuredHeight()I

    move-result v6

    int-to-float v6, v6

    .line 877
    int-to-float v5, v5

    mul-float/2addr v5, v10

    int-to-float v7, v4

    div-float/2addr v5, v7

    mul-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v5, v5

    .line 876
    iput v5, p0, Lcom/android/launcher2/Folder;->J:F

    .line 879
    iput v3, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->width:I

    .line 880
    iput v4, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->height:I

    .line 881
    iput v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    .line 882
    iput v2, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    .line 883
    return-void
.end method

.method private n()V
    .registers 4

    .prologue
    .line 1018
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1019
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/android/launcher2/Folder;->a(I)Landroid/view/View;

    .line 1020
    if-eqz v0, :cond_39

    .line 1021
    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher2/FolderEditText;->setNextFocusDownId(I)V

    .line 1022
    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher2/FolderEditText;->setNextFocusRightId(I)V

    .line 1023
    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/android/launcher2/FolderEditText;->setNextFocusLeftId(I)V

    .line 1024
    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/android/launcher2/FolderEditText;->setNextFocusUpId(I)V

    .line 1026
    :cond_39
    return-void
.end method

.method private setupContentDimensions(I)V
    .registers 15
    .parameter

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemsInReadingOrder()Ljava/util/ArrayList;

    move-result-object v2

    .line 801
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v1

    .line 802
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    .line 821
    :cond_10
    const/4 v3, 0x0

    move v4, v1

    move v12, v0

    move v0, v3

    move v3, v12

    .line 805
    :goto_15
    if-eqz v0, :cond_37

    .line 823
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v4, v3}, Lcom/android/launcher2/CellLayout;->a(II)V

    .line 824
    const/4 v0, 0x2

    new-array v11, v0, [I

    if-nez v2, :cond_d2

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemsInReadingOrder()Ljava/util/ArrayList;

    move-result-object v0

    move-object v9, v0

    :goto_26
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->removeAllViews()V

    const/4 v0, 0x0

    move v10, v0

    :goto_2d
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v10, v0, :cond_78

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 825
    return-void

    .line 808
    :cond_37
    mul-int v0, v4, v3

    if-ge v0, p1, :cond_5a

    .line 810
    if-le v4, v3, :cond_41

    iget v0, p0, Lcom/android/launcher2/Folder;->r:I

    if-ne v3, v0, :cond_56

    :cond_41
    iget v0, p0, Lcom/android/launcher2/Folder;->q:I

    if-ge v4, v0, :cond_56

    .line 811
    add-int/lit8 v1, v4, 0x1

    move v0, v3

    .line 815
    :goto_48
    if-nez v0, :cond_4c

    add-int/lit8 v0, v0, 0x1

    .line 821
    :cond_4c
    :goto_4c
    if-ne v1, v4, :cond_10

    if-ne v0, v3, :cond_10

    const/4 v3, 0x1

    move v4, v1

    move v12, v0

    move v0, v3

    move v3, v12

    goto :goto_15

    .line 813
    :cond_56
    add-int/lit8 v0, v3, 0x1

    move v1, v4

    goto :goto_48

    .line 816
    :cond_5a
    add-int/lit8 v0, v3, -0x1

    mul-int/2addr v0, v4

    if-lt v0, p1, :cond_6a

    if-lt v3, v4, :cond_6a

    .line 817
    const/4 v0, 0x0

    add-int/lit8 v1, v3, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v4

    goto :goto_4c

    .line 818
    :cond_6a
    add-int/lit8 v0, v4, -0x1

    mul-int/2addr v0, v3

    if-lt v0, p1, :cond_d5

    .line 819
    const/4 v0, 0x0

    add-int/lit8 v1, v4, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v0, v3

    goto :goto_4c

    .line 824
    :cond_78
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/View;

    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v11}, Lcom/android/launcher2/CellLayout;->a([I)Z

    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/android/launcher2/CellLayout$LayoutParams;

    const/4 v0, 0x0

    aget v0, v11, v0

    iput v0, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    const/4 v0, 0x1

    aget v0, v11, v0

    iput v0, v8, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    invoke-virtual {v7}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/di;

    iget v0, v1, Lcom/android/launcher2/di;->l:I

    const/4 v2, 0x0

    aget v2, v11, v2

    if-ne v0, v2, :cond_a9

    iget v0, v1, Lcom/android/launcher2/di;->m:I

    const/4 v2, 0x1

    aget v2, v11, v2

    if-eq v0, v2, :cond_c1

    :cond_a9
    const/4 v0, 0x0

    aget v0, v11, v0

    iput v0, v1, Lcom/android/launcher2/di;->l:I

    const/4 v0, 0x1

    aget v0, v11, v0

    iput v0, v1, Lcom/android/launcher2/di;->m:I

    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v2, v2, Lcom/android/launcher2/cu;->h:J

    const/4 v4, 0x0

    iget v5, v1, Lcom/android/launcher2/di;->l:I

    iget v6, v1, Lcom/android/launcher2/di;->m:I

    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    :cond_c1
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    const/4 v2, -0x1

    iget-wide v3, v1, Lcom/android/launcher2/di;->h:J

    long-to-int v3, v3

    const/4 v5, 0x1

    move-object v1, v7

    move-object v4, v8

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IILcom/android/launcher2/CellLayout$LayoutParams;Z)Z

    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto/16 :goto_2d

    :cond_d2
    move-object v9, v2

    goto/16 :goto_26

    :cond_d5
    move v0, v3

    move v1, v4

    goto/16 :goto_4c
.end method

.method private setupContentForNumItems(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 893
    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->setupContentDimensions(I)V

    .line 895
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    .line 896
    if-nez v0, :cond_17

    .line 897
    new-instance v0, Lcom/android/launcher2/DragLayer$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/android/launcher2/DragLayer$LayoutParams;-><init>(II)V

    .line 898
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/launcher2/DragLayer$LayoutParams;->c:Z

    .line 899
    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 901
    :cond_17
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->m()V

    .line 902
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/android/launcher2/bz;ZZ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 727
    if-eqz p4, :cond_32

    .line 728
    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->F:Z

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->H:Z

    if-nez v0, :cond_10

    .line 729
    invoke-direct {p0, v2}, Lcom/android/launcher2/Folder;->c(Z)V

    .line 742
    :cond_10
    :goto_10
    if-eq p1, p0, :cond_22

    .line 743
    iget-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->b()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 744
    iget-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 745
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->h()V

    .line 748
    :cond_22
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->F:Z

    .line 749
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->E:Z

    .line 750
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->H:Z

    .line 751
    iput-object v3, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    .line 752
    iput-object v3, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    .line 753
    iput-boolean v1, p0, Lcom/android/launcher2/Folder;->f:Z

    .line 757
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->l()V

    .line 758
    return-void

    .line 733
    :cond_32
    iget-object v0, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0, p2}, Lcom/android/launcher2/FolderIcon;->a(Lcom/android/launcher2/bz;)V

    .line 737
    iget-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 738
    iput-boolean v2, p0, Lcom/android/launcher2/Folder;->G:Z

    goto :goto_10
.end method

.method public final a(Lcom/android/launcher2/bz;)V
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x1

    .line 1030
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    instance-of v0, v0, Lcom/android/launcher2/h;

    if-eqz v0, :cond_6a

    .line 1032
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/h;

    invoke-virtual {v0}, Lcom/android/launcher2/h;->a()Lcom/android/launcher2/jd;

    move-result-object v0

    .line 1033
    iput v5, v0, Lcom/android/launcher2/jd;->n:I

    .line 1034
    iput v5, v0, Lcom/android/launcher2/jd;->o:I

    move-object v6, v0

    .line 1040
    :goto_15
    iget-object v0, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    if-ne v6, v0, :cond_64

    .line 1041
    iget-object v0, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jd;

    .line 1042
    iget-object v1, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/android/launcher2/CellLayout$LayoutParams;

    .line 1043
    iget-object v1, p0, Lcom/android/launcher2/Folder;->z:[I

    aget v1, v1, v9

    iput v1, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->a:I

    iput v1, v0, Lcom/android/launcher2/jd;->l:I

    .line 1044
    iget-object v1, p0, Lcom/android/launcher2/Folder;->z:[I

    aget v1, v1, v5

    iput v1, v4, Lcom/android/launcher2/CellLayout$LayoutParams;->b:I

    iput v1, v0, Lcom/android/launcher2/jd;->l:I

    .line 1045
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    const/4 v2, -0x1

    iget-wide v7, v6, Lcom/android/launcher2/jd;->h:J

    long-to-int v3, v7

    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->a(Landroid/view/View;IILcom/android/launcher2/CellLayout$LayoutParams;Z)Z

    .line 1046
    iget-object v0, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    invoke-virtual {v0}, Lcom/android/launcher2/bu;->b()Z

    move-result v0

    if-eqz v0, :cond_70

    .line 1047
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    iget-object v1, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/DragLayer;->a(Lcom/android/launcher2/bu;Landroid/view/View;)V

    .line 1052
    :goto_59
    iput-boolean v5, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 1053
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->setupContentDimensions(I)V

    .line 1054
    iput-boolean v5, p0, Lcom/android/launcher2/Folder;->f:Z

    .line 1056
    :cond_64
    iget-object v0, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-virtual {v0, v6}, Lcom/android/launcher2/cu;->a(Lcom/android/launcher2/di;)V

    .line 1057
    return-void

    .line 1036
    :cond_6a
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/jd;

    move-object v6, v0

    goto :goto_15

    .line 1049
    :cond_70
    iput-boolean v9, p1, Lcom/android/launcher2/bz;->k:Z

    .line 1050
    iget-object v0, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_59
.end method

.method public final a(Lcom/android/launcher2/bz;Landroid/graphics/PointF;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 767
    return-void
.end method

.method final a(Lcom/android/launcher2/cu;)V
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 372
    iput-object p1, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    .line 373
    iget-object v3, p1, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    .line 374
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 375
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/launcher2/Folder;->setupContentForNumItems(I)V

    .line 376
    invoke-direct {p0, v3}, Lcom/android/launcher2/Folder;->a(Ljava/util/ArrayList;)V

    move v1, v0

    move v2, v0

    .line 378
    :goto_16
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_4d

    .line 388
    invoke-direct {p0, v2}, Lcom/android/launcher2/Folder;->setupContentForNumItems(I)V

    .line 393
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_23
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_63

    .line 398
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 399
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->n()V

    .line 400
    iget-object v0, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/cu;->a(Lcom/android/launcher2/cv;)V

    .line 402
    sget-object v0, Lcom/android/launcher2/Folder;->M:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-object v1, v1, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_74

    .line 403
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-object v1, v1, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    .line 407
    :goto_49
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->l()V

    .line 408
    return-void

    .line 379
    :cond_4d
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jd;

    .line 380
    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->d(Lcom/android/launcher2/di;)Z

    move-result v5

    if-nez v5, :cond_60

    .line 381
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    :goto_5c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_16

    .line 383
    :cond_60
    add-int/lit8 v2, v2, 0x1

    goto :goto_5c

    .line 393
    :cond_63
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/di;

    .line 394
    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-virtual {v2, v0}, Lcom/android/launcher2/cu;->b(Lcom/android/launcher2/di;)V

    .line 395
    iget-object v2, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v2, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto :goto_23

    .line 405
    :cond_74
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_49
.end method

.method public final a(Lcom/android/launcher2/di;)V
    .registers 9
    .parameter

    .prologue
    .line 1060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 1063
    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->f:Z

    if-eqz v0, :cond_8

    .line 1072
    :goto_7
    return-void

    .line 1064
    :cond_8
    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->c(Lcom/android/launcher2/di;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1066
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->setupContentForNumItems(I)V

    .line 1067
    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->c(Lcom/android/launcher2/di;)Z

    .line 1069
    :cond_1a
    invoke-direct {p0, p1}, Lcom/android/launcher2/Folder;->d(Lcom/android/launcher2/di;)Z

    .line 1071
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-wide v2, v1, Lcom/android/launcher2/cu;->h:J

    const/4 v4, 0x0

    iget v5, p1, Lcom/android/launcher2/di;->l:I

    iget v6, p1, Lcom/android/launcher2/di;->m:I

    move-object v1, p1

    .line 1070
    invoke-static/range {v0 .. v6}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;JIII)V

    goto :goto_7
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter

    .prologue
    .line 1108
    return-void
.end method

.method public final a(Z)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x2

    const v6, 0x3f666666

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 495
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/DragLayer;

    if-nez v0, :cond_f

    .line 527
    :goto_e
    return-void

    .line 496
    :cond_f
    const-string v0, "alpha"

    new-array v1, v5, [F

    const/4 v2, 0x0

    aput v2, v1, v4

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 497
    const-string v1, "scaleX"

    new-array v2, v5, [F

    aput v6, v2, v4

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 498
    const-string v2, "scaleY"

    new-array v3, v5, [F

    aput v6, v3, v4

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 500
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    aput-object v2, v3, v7

    invoke-static {p0, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 499
    iput-object v0, p0, Lcom/android/launcher2/Folder;->O:Landroid/animation/ObjectAnimator;

    .line 502
    new-instance v1, Lcom/android/launcher2/ci;

    invoke-direct {v1, p0, p1}, Lcom/android/launcher2/ci;-><init>(Lcom/android/launcher2/Folder;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 516
    iget v1, p0, Lcom/android/launcher2/Folder;->k:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 517
    const/4 v1, 0x0

    invoke-virtual {p0, v7, v1}, Lcom/android/launcher2/Folder;->setLayerType(ILandroid/graphics/Paint;)V

    .line 518
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->buildLayer()V

    .line 519
    new-instance v1, Lcom/android/launcher2/cj;

    invoke-direct {v1, p0, v0}, Lcom/android/launcher2/cj;-><init>(Lcom/android/launcher2/Folder;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Folder;->post(Ljava/lang/Runnable;)Z

    goto :goto_e
.end method

.method public final a([I)V
    .registers 3
    .parameter

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/android/launcher2/DragLayer;->a(Landroid/view/View;[I)V

    .line 1135
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->K:Z

    return v0
.end method

.method public final b(Z)Ljava/util/ArrayList;
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1115
    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->e:Z

    if-eqz v0, :cond_15

    .line 1116
    iget-object v0, p0, Lcom/android/launcher2/Folder;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v1, v2

    .line 1117
    :goto_b
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v0

    if-lt v1, v0, :cond_18

    .line 1128
    iput-boolean v2, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 1130
    :cond_15
    iget-object v0, p0, Lcom/android/launcher2/Folder;->t:Ljava/util/ArrayList;

    return-object v0

    :cond_18
    move v3, v2

    .line 1118
    :goto_19
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v0

    if-lt v3, v0, :cond_25

    .line 1117
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 1119
    :cond_25
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v3, v1}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v4

    .line 1120
    if-eqz v4, :cond_3e

    .line 1121
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jd;

    .line 1122
    iget-object v5, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    if-ne v0, v5, :cond_39

    if-eqz p1, :cond_3e

    .line 1123
    :cond_39
    iget-object v0, p0, Lcom/android/launcher2/Folder;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1118
    :cond_3e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_19
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 266
    iget-object v0, p0, Lcom/android/launcher2/Folder;->L:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 267
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->f()V

    .line 268
    return-void
.end method

.method public final b(Lcom/android/launcher2/bz;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 593
    iget-object v0, p0, Lcom/android/launcher2/Folder;->y:[I

    const/4 v1, 0x0

    aput v2, v0, v1

    .line 594
    iget-object v0, p0, Lcom/android/launcher2/Folder;->y:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 595
    iget-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 596
    return-void
.end method

.method public final b(Lcom/android/launcher2/di;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 1075
    iput-boolean v5, p0, Lcom/android/launcher2/Folder;->e:Z

    .line 1078
    iget-object v0, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    if-ne p1, v0, :cond_9

    .line 1089
    :cond_8
    :goto_8
    return-void

    :cond_9
    move v0, v1

    .line 1079
    :goto_a
    iget-object v2, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v2}, Lcom/android/launcher2/CellLayout;->getCountY()I

    move-result v2

    if-lt v0, v2, :cond_28

    const/4 v0, 0x0

    .line 1080
    :goto_13
    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 1081
    iget v0, p0, Lcom/android/launcher2/Folder;->n:I

    if-ne v0, v5, :cond_45

    .line 1082
    iput-boolean v5, p0, Lcom/android/launcher2/Folder;->o:Z

    .line 1086
    :goto_1e
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    if-gt v0, v5, :cond_8

    .line 1087
    invoke-direct {p0, v5}, Lcom/android/launcher2/Folder;->c(Z)V

    goto :goto_8

    :cond_28
    move v2, v1

    .line 1079
    :goto_29
    iget-object v3, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getCountX()I

    move-result v3

    if-lt v2, v3, :cond_34

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_34
    iget-object v3, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3, v2, v0}, Lcom/android/launcher2/CellLayout;->c(II)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-ne v4, p1, :cond_42

    move-object v0, v3

    goto :goto_13

    :cond_42
    add-int/lit8 v2, v2, 0x1

    goto :goto_29

    .line 1084
    :cond_45
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->setupContentForNumItems(I)V

    goto :goto_1e
.end method

.method public final c(Lcom/android/launcher2/bz;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 657
    iget v0, p1, Lcom/android/launcher2/bz;->a:I

    iget v1, p1, Lcom/android/launcher2/bz;->b:I

    iget v2, p1, Lcom/android/launcher2/bz;->c:I

    iget v4, p1, Lcom/android/launcher2/bz;->d:I

    iget-object v5, p1, Lcom/android/launcher2/bz;->f:Lcom/android/launcher2/bu;

    const/4 v6, 0x2

    new-array v6, v6, [F

    sub-int/2addr v0, v2

    sub-int/2addr v1, v4

    invoke-virtual {v5}, Lcom/android/launcher2/bu;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    int-to-float v0, v0

    aput v0, v6, v7

    invoke-virtual {v5}, Lcom/android/launcher2/bu;->getDragRegion()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    aput v0, v6, v3

    .line 658
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    aget v1, v6, v7

    float-to-int v1, v1

    aget v2, v6, v3

    float-to-int v2, v2

    .line 659
    iget-object v4, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v4

    add-int/2addr v2, v4

    iget-object v5, p0, Lcom/android/launcher2/Folder;->x:[I

    move v4, v3

    .line 658
    invoke-virtual/range {v0 .. v5}, Lcom/android/launcher2/CellLayout;->b(IIII[I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/Folder;->x:[I

    .line 661
    iget-object v0, p0, Lcom/android/launcher2/Folder;->x:[I

    aget v0, v0, v7

    iget-object v1, p0, Lcom/android/launcher2/Folder;->y:[I

    aget v1, v1, v7

    if-ne v0, v1, :cond_59

    iget-object v0, p0, Lcom/android/launcher2/Folder;->x:[I

    aget v0, v0, v3

    iget-object v1, p0, Lcom/android/launcher2/Folder;->y:[I

    aget v1, v1, v3

    if-eq v0, v1, :cond_7c

    .line 662
    :cond_59
    iget-object v0, p0, Lcom/android/launcher2/Folder;->A:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 663
    iget-object v0, p0, Lcom/android/launcher2/Folder;->A:Lcom/android/launcher2/c;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->h:Lcom/android/launcher2/hq;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/c;->a(Lcom/android/launcher2/hq;)V

    .line 664
    iget-object v0, p0, Lcom/android/launcher2/Folder;->A:Lcom/android/launcher2/c;

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/c;->a(J)V

    .line 665
    iget-object v0, p0, Lcom/android/launcher2/Folder;->y:[I

    iget-object v1, p0, Lcom/android/launcher2/Folder;->x:[I

    aget v1, v1, v7

    aput v1, v0, v7

    .line 666
    iget-object v0, p0, Lcom/android/launcher2/Folder;->y:[I

    iget-object v1, p0, Lcom/android/launcher2/Folder;->x:[I

    aget v1, v1, v3

    aput v1, v0, v3

    .line 668
    :cond_7c
    aget v0, v6, v3

    iget-object v1, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getBottom()I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/Folder;->Q:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_96

    .line 669
    iget-object v0, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    iget v1, p0, Lcom/android/launcher2/Folder;->Q:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v7, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 673
    :cond_95
    :goto_95
    return-void

    .line 670
    :cond_96
    aget v0, v6, v3

    iget-object v1, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getTop()I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/Folder;->Q:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_95

    .line 671
    iget-object v0, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    iget v1, p0, Lcom/android/launcher2/Folder;->Q:I

    neg-int v1, v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v7, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_95
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 791
    const/4 v0, 0x1

    return v0
.end method

.method public final d(Lcom/android/launcher2/bz;)V
    .registers 5
    .parameter

    .prologue
    .line 718
    iget-boolean v0, p1, Lcom/android/launcher2/bz;->e:Z

    if-nez v0, :cond_12

    .line 719
    iget-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->i:Lcom/android/launcher2/hq;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/c;->a(Lcom/android/launcher2/hq;)V

    .line 720
    iget-object v0, p0, Lcom/android/launcher2/Folder;->B:Lcom/android/launcher2/c;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v1, v2}, Lcom/android/launcher2/c;->a(J)V

    .line 722
    :cond_12
    iget-object v0, p0, Lcom/android/launcher2/Folder;->A:Lcom/android/launcher2/c;

    invoke-virtual {v0}, Lcom/android/launcher2/c;->a()V

    .line 723
    return-void
.end method

.method public final d()Z
    .registers 2

    .prologue
    .line 762
    const/4 v0, 0x1

    return v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 325
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .registers 1

    .prologue
    .line 772
    return-void
.end method

.method public final e(Lcom/android/launcher2/bz;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 537
    iget-object v0, p1, Lcom/android/launcher2/bz;->g:Ljava/lang/Object;

    check-cast v0, Lcom/android/launcher2/di;

    .line 538
    iget v0, v0, Lcom/android/launcher2/di;->i:I

    .line 539
    if-eqz v0, :cond_b

    .line 540
    if-ne v0, v1, :cond_d

    :cond_b
    move v0, v1

    .line 541
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    .line 539
    goto :goto_c
.end method

.method public final f()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 271
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    sget-object v1, Lcom/android/launcher2/Folder;->N:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 275
    iget-object v1, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/cu;->a(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    invoke-static {v1, v2}, Lcom/android/launcher2/gb;->a(Landroid/content/Context;Lcom/android/launcher2/di;)V

    .line 280
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0702c2

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 279
    invoke-direct {p0, v0}, Lcom/android/launcher2/Folder;->a(Ljava/lang/String;)V

    .line 284
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->requestFocus()Z

    .line 286
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0, v3, v3}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    .line 287
    iput-boolean v3, p0, Lcom/android/launcher2/Folder;->K:Z

    .line 288
    return-void
.end method

.method public final g()V
    .registers 9

    .prologue
    const/4 v7, 0x2

    const v1, 0x3f4ccccd

    const/high16 v6, 0x3f80

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 434
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/DragLayer;

    if-eqz v0, :cond_1c

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Folder;->setScaleX(F)V

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Folder;->setScaleY(F)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->setAlpha(F)V

    iput v4, p0, Lcom/android/launcher2/Folder;->n:I

    .line 436
    :cond_1c
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lcom/android/launcher2/DragLayer;

    if-nez v0, :cond_25

    .line 470
    :goto_24
    return-void

    .line 437
    :cond_25
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->m()V

    .line 438
    const-string v0, "alpha"

    new-array v1, v5, [F

    aput v6, v1, v4

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 439
    const-string v1, "scaleX"

    new-array v2, v5, [F

    aput v6, v2, v4

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 440
    const-string v2, "scaleY"

    new-array v3, v5, [F

    aput v6, v3, v4

    invoke-static {v2, v3}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 442
    const/4 v3, 0x3

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    aput-object v2, v3, v7

    invoke-static {p0, v3}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 441
    iput-object v0, p0, Lcom/android/launcher2/Folder;->O:Landroid/animation/ObjectAnimator;

    .line 444
    new-instance v1, Lcom/android/launcher2/cg;

    invoke-direct {v1, p0}, Lcom/android/launcher2/cg;-><init>(Lcom/android/launcher2/Folder;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 459
    iget v1, p0, Lcom/android/launcher2/Folder;->k:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 460
    const/4 v1, 0x0

    invoke-virtual {p0, v7, v1}, Lcom/android/launcher2/Folder;->setLayerType(ILandroid/graphics/Paint;)V

    .line 461
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->buildLayer()V

    .line 462
    new-instance v1, Lcom/android/launcher2/ch;

    invoke-direct {v1, p0, v0}, Lcom/android/launcher2/ch;-><init>(Lcom/android/launcher2/Folder;Landroid/animation/ObjectAnimator;)V

    invoke-virtual {p0, v1}, Lcom/android/launcher2/Folder;->post(Ljava/lang/Runnable;)Z

    goto :goto_24
.end method

.method public getDragDrawable()Landroid/graphics/drawable/Drawable;
    .registers 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/android/launcher2/Folder;->u:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getDropTargetDelegate$2b911dda()Lcom/android/launcher2/bx;
    .registers 2

    .prologue
    .line 795
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEditTextRegion()Landroid/view/View;
    .registers 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    return-object v0
.end method

.method getInfo()Lcom/android/launcher2/cu;
    .registers 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    return-object v0
.end method

.method public getItemCount()I
    .registers 2

    .prologue
    .line 949
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/launcher2/ja;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getItemsInReadingOrder()Ljava/util/ArrayList;
    .registers 2

    .prologue
    .line 1111
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->b(Z)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method getPivotXForIconAnimation()F
    .registers 2

    .prologue
    .line 886
    iget v0, p0, Lcom/android/launcher2/Folder;->I:F

    return v0
.end method

.method getPivotYForIconAnimation()F
    .registers 2

    .prologue
    .line 889
    iget v0, p0, Lcom/android/launcher2/Folder;->J:F

    return v0
.end method

.method public final h()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 708
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->h()V

    .line 709
    iput-object v1, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    .line 710
    iput-object v1, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    .line 711
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->f:Z

    .line 712
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->o:Z

    .line 713
    return-void
.end method

.method public final i()V
    .registers 2

    .prologue
    .line 785
    iget-boolean v0, p0, Lcom/android/launcher2/Folder;->E:Z

    if-eqz v0, :cond_7

    .line 786
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->H:Z

    .line 788
    :cond_7
    return-void
.end method

.method public final k()V
    .registers 1

    .prologue
    .line 1104
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->n()V

    .line 1105
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 208
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 209
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_3f

    .line 211
    check-cast v0, Lcom/android/launcher2/jd;

    .line 212
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 213
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 214
    iget-object v2, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    new-instance v3, Landroid/graphics/Rect;

    aget v4, v1, v6

    aget v5, v1, v8

    .line 215
    aget v6, v1, v6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    aget v1, v1, v8

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v7

    add-int/2addr v1, v7

    invoke-direct {v3, v4, v5, v6, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 214
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setSourceBounds(Landroid/graphics/Rect;)V

    .line 217
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v2, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v1, p1, v2, v0}, Lcom/android/launcher2/Launcher;->b(Landroid/view/View;Landroid/content/Intent;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 218
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/jd;->b:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/Launcher;->a(Landroid/content/Intent;)V

    .line 221
    :cond_3f
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 291
    const/4 v0, 0x6

    if-ne p2, v0, :cond_8

    .line 292
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->b()V

    .line 293
    const/4 v0, 0x1

    .line 295
    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected onFinishInflate()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 157
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 158
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    const-string v1, "folder_background_holo"

    invoke-virtual {v0, p0, v2, v1}, Lcom/anddoes/launcher/c/l;->b(Landroid/view/View;ILjava/lang/String;)V

    .line 159
    const v0, 0x7f0d0044

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    .line 160
    iget-object v0, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 161
    const v0, 0x7f0d0066

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/CellLayout;

    iput-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    .line 162
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0, v2, v2}, Lcom/android/launcher2/CellLayout;->a(II)V

    .line 163
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getShortcutsAndWidgets()Lcom/android/launcher2/ja;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/launcher2/ja;->setMotionEventSplittingEnabled(Z)V

    .line 164
    const v0, 0x7f0d0050

    invoke-virtual {p0, v0}, Lcom/android/launcher2/Folder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/FolderEditText;

    iput-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    .line 165
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0, v1}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;)V

    .line 166
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    const-string v2, "folder_title_color"

    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->aj:Z

    if-eqz v0, :cond_74

    .line 169
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setVisibility(I)V

    .line 170
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b006c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->C:I

    .line 188
    :goto_73
    return-void

    .line 172
    :cond_74
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/FolderEditText;->setFolder(Lcom/android/launcher2/Folder;)V

    .line 173
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/FolderEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 177
    sget v0, Landroid/view/View$MeasureSpec;->UNSPECIFIED:I

    .line 178
    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v1, v0, v0}, Lcom/android/launcher2/FolderEditText;->measure(II)V

    .line 179
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderEditText;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/Folder;->C:I

    .line 182
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->P:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    .line 183
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0, p0}, Lcom/android/launcher2/FolderEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 184
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setSelectAllOnFocus(Z)V

    .line 185
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v1}, Lcom/android/launcher2/FolderEditText;->getInputType()I

    move-result v1

    .line 186
    const/high16 v2, 0x8

    or-int/2addr v1, v2

    or-int/lit16 v1, v1, 0x2000

    .line 185
    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setInputType(I)V

    goto :goto_73
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    if-ne p1, v0, :cond_18

    if-eqz p2, :cond_18

    .line 1139
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v0, v0, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v0, :cond_19

    .line 1140
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/android/launcher2/FolderEditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/Folder;->K:Z

    .line 1145
    :cond_18
    :goto_18
    return-void

    .line 1142
    :cond_19
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->b()V

    goto :goto_18
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 225
    iget-object v0, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->b()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v2

    .line 253
    :goto_b
    return v0

    .line 227
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 228
    instance-of v1, v0, Lcom/android/launcher2/jd;

    if-eqz v1, :cond_78

    .line 229
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->b:Z

    if-nez v1, :cond_78

    .line 230
    check-cast v0, Lcom/android/launcher2/jd;

    .line 231
    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_26

    move v0, v3

    .line 232
    goto :goto_b

    .line 234
    :cond_26
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, v1, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget-boolean v1, v1, Lcom/anddoes/launcher/preference/f;->ao:Z

    if-eqz v1, :cond_39

    .line 235
    iget-object v4, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/di;

    invoke-virtual {v4, v1, p1}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/di;Landroid/view/View;)V

    .line 238
    :cond_39
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;)V

    .line 239
    iget-object v1, p0, Lcom/android/launcher2/Folder;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    invoke-virtual {v1, p1, p0}, Lcom/android/launcher2/Workspace;->a(Landroid/view/View;Lcom/android/launcher2/bt;)V

    move-object v1, p1

    .line 240
    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/android/launcher2/Folder;->u:Landroid/graphics/drawable/Drawable;

    .line 242
    iput-object v0, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    .line 243
    iget-object v1, p0, Lcom/android/launcher2/Folder;->z:[I

    iget v4, v0, Lcom/android/launcher2/jd;->l:I

    aput v4, v1, v3

    .line 244
    iget-object v1, p0, Lcom/android/launcher2/Folder;->z:[I

    iget v0, v0, Lcom/android/launcher2/jd;->m:I

    aput v0, v1, v2

    .line 245
    iput-object p1, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    .line 247
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->removeView(Landroid/view/View;)V

    .line 248
    iget-object v0, p0, Lcom/android/launcher2/Folder;->c:Lcom/android/launcher2/cu;

    iget-object v1, p0, Lcom/android/launcher2/Folder;->v:Lcom/android/launcher2/jd;

    invoke-virtual {v0, v1}, Lcom/android/launcher2/cu;->b(Lcom/android/launcher2/di;)V

    .line 249
    iput-boolean v2, p0, Lcom/android/launcher2/Folder;->E:Z

    .line 250
    iput-boolean v3, p0, Lcom/android/launcher2/Folder;->H:Z

    :cond_78
    move v0, v2

    .line 253
    goto :goto_b
.end method

.method protected onMeasure(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/high16 v6, 0x4000

    .line 905
    iget-object v0, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v0}, Lcom/android/launcher2/CellLayout;->getDesiredWidth()I

    move-result v0

    .line 906
    iget-object v1, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v1}, Lcom/android/launcher2/CellLayout;->getDesiredHeight()I

    move-result v1

    .line 907
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v3}, Lcom/android/launcher2/CellLayout;->getDesiredWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 908
    invoke-direct {p0}, Lcom/android/launcher2/Folder;->getSuitableHeight()I

    move-result v3

    .line 909
    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingTop()I

    move-result v4

    sub-int v4, v3, v4

    invoke-virtual {p0}, Lcom/android/launcher2/Folder;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    .line 910
    iget v5, p0, Lcom/android/launcher2/Folder;->C:I

    .line 909
    sub-int/2addr v4, v5

    .line 911
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 912
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 913
    iget-object v5, p0, Lcom/android/launcher2/Folder;->j:Landroid/widget/ScrollView;

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v5, v0, v4}, Landroid/widget/ScrollView;->measure(II)V

    .line 915
    iget-object v4, p0, Lcom/android/launcher2/Folder;->d:Lcom/android/launcher2/CellLayout;

    invoke-virtual {v4, v0, v1}, Lcom/android/launcher2/CellLayout;->measure(II)V

    .line 917
    iget-object v1, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    .line 918
    iget v4, p0, Lcom/android/launcher2/Folder;->C:I

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 917
    invoke-virtual {v1, v0, v4}, Lcom/android/launcher2/FolderEditText;->measure(II)V

    .line 919
    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/Folder;->setMeasuredDimension(II)V

    .line 920
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 311
    const/4 v0, 0x1

    return v0
.end method

.method public setDragController(Lcom/android/launcher2/bk;)V
    .registers 2
    .parameter

    .prologue
    .line 315
    iput-object p1, p0, Lcom/android/launcher2/Folder;->a:Lcom/android/launcher2/bk;

    .line 316
    return-void
.end method

.method setFolderIcon(Lcom/android/launcher2/FolderIcon;)V
    .registers 2
    .parameter

    .prologue
    .line 319
    iput-object p1, p0, Lcom/android/launcher2/Folder;->p:Lcom/android/launcher2/FolderIcon;

    .line 320
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1152
    iget-object v0, p0, Lcom/android/launcher2/Folder;->g:Lcom/android/launcher2/FolderEditText;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/FolderEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1153
    return-void
.end method
