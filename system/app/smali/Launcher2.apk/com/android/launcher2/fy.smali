.class public final Lcom/android/launcher2/fy;
.super Landroid/appwidget/AppWidgetHostView;
.source "SourceFile"


# instance fields
.field private a:Lcom/android/launcher2/ax;

.field private b:Landroid/view/LayoutInflater;

.field private c:Landroid/content/Context;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/appwidget/AppWidgetHostView;-><init>(Landroid/content/Context;)V

    .line 41
    iput-object p1, p0, Lcom/android/launcher2/fy;->c:Landroid/content/Context;

    .line 42
    new-instance v0, Lcom/android/launcher2/ax;

    invoke-direct {v0, p0}, Lcom/android/launcher2/ax;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/android/launcher2/fy;->a:Lcom/android/launcher2/ax;

    .line 43
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/launcher2/fy;->b:Landroid/view/LayoutInflater;

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Z
    .registers 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/android/launcher2/fy;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 60
    iget v1, p0, Lcom/android/launcher2/fy;->d:I

    if-eq v1, v0, :cond_12

    .line 61
    const/4 v0, 0x1

    .line 63
    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final cancelLongPress()V
    .registers 2

    .prologue
    .line 94
    invoke-super {p0}, Landroid/appwidget/AppWidgetHostView;->cancelLongPress()V

    .line 96
    iget-object v0, p0, Lcom/android/launcher2/fy;->a:Lcom/android/launcher2/ax;

    invoke-virtual {v0}, Lcom/android/launcher2/ax;->a()V

    .line 97
    return-void
.end method

.method public final getDescendantFocusability()I
    .registers 2

    .prologue
    .line 101
    const/high16 v0, 0x6

    return v0
.end method

.method protected final getErrorView()Landroid/view/View;
    .registers 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/launcher2/fy;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f03000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 68
    iget-object v1, p0, Lcom/android/launcher2/fy;->a:Lcom/android/launcher2/ax;

    iget-boolean v1, v1, Lcom/android/launcher2/ax;->b:Z

    if-eqz v1, :cond_e

    .line 69
    iget-object v0, p0, Lcom/android/launcher2/fy;->a:Lcom/android/launcher2/ax;

    invoke-virtual {v0}, Lcom/android/launcher2/ax;->a()V

    .line 70
    const/4 v0, 0x1

    .line 89
    :goto_d
    return v0

    .line 75
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_40

    :pswitch_15
    goto :goto_d

    .line 77
    :pswitch_16
    invoke-virtual {p0}, Lcom/android/launcher2/fy;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/anddoes/launcher/v;->c(Landroid/content/Context;)V

    .line 78
    iget-object v1, p0, Lcom/android/launcher2/fy;->a:Lcom/android/launcher2/ax;

    iput-boolean v0, v1, Lcom/android/launcher2/ax;->b:Z

    iget-object v2, v1, Lcom/android/launcher2/ax;->c:Lcom/android/launcher2/ay;

    if-nez v2, :cond_2c

    new-instance v2, Lcom/android/launcher2/ay;

    invoke-direct {v2, v1}, Lcom/android/launcher2/ay;-><init>(Lcom/android/launcher2/ax;)V

    iput-object v2, v1, Lcom/android/launcher2/ax;->c:Lcom/android/launcher2/ay;

    :cond_2c
    iget-object v2, v1, Lcom/android/launcher2/ax;->a:Landroid/view/View;

    iget-object v1, v1, Lcom/android/launcher2/ax;->c:Lcom/android/launcher2/ay;

    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->g()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v2, v1, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_d

    .line 84
    :pswitch_39
    iget-object v1, p0, Lcom/android/launcher2/fy;->a:Lcom/android/launcher2/ax;

    invoke-virtual {v1}, Lcom/android/launcher2/ax;->a()V

    goto :goto_d

    .line 75
    nop

    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_16
        :pswitch_39
        :pswitch_15
        :pswitch_39
    .end packed-switch
.end method

.method public final updateAppWidget(Landroid/widget/RemoteViews;)V
    .registers 3
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/launcher2/fy;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/android/launcher2/fy;->d:I

    .line 55
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetHostView;->updateAppWidget(Landroid/widget/RemoteViews;)V

    .line 56
    return-void
.end method
