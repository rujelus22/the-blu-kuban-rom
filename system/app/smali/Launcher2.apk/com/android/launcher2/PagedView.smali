.class public abstract Lcom/android/launcher2/PagedView;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# static fields
.field protected static aA:F

.field protected static aB:F

.field protected static aC:F

.field protected static ay:F

.field protected static az:F


# instance fields
.field protected A:F

.field protected B:F

.field protected C:I

.field protected D:Z

.field protected E:Landroid/view/View$OnLongClickListener;

.field protected F:Z

.field protected G:I

.field protected H:I

.field protected I:I

.field protected J:I

.field protected K:I

.field protected L:I

.field protected M:I

.field protected N:I

.field protected O:I

.field protected P:I

.field protected Q:Z

.field protected R:Z

.field protected S:I

.field protected T:[I

.field protected U:Z

.field protected V:I

.field protected W:F

.field protected Z:I

.field private a:Lcom/anddoes/launcher/av;

.field protected aD:Landroid/view/animation/AccelerateInterpolator;

.field protected aE:Landroid/view/animation/DecelerateInterpolator;

.field aF:Ljava/lang/Runnable;

.field private aG:Z

.field private aH:Z

.field private aI:Lcom/android/launcher2/hz;

.field protected aa:Ljava/util/ArrayList;

.field protected ab:Z

.field protected ac:Z

.field protected ad:Z

.field protected ae:Z

.field protected af:Z

.field protected ag:Z

.field protected ah:I

.field protected ai:I

.field public aj:Z

.field public ak:Z

.field public al:I

.field protected am:F

.field protected an:F

.field public ao:Z

.field protected ap:Z

.field public aq:Z

.field protected ar:Z

.field protected as:Z

.field public at:I

.field public au:Ljava/lang/String;

.field protected av:I

.field protected aw:I

.field protected ax:Lcom/android/launcher2/ij;

.field private b:F

.field private c:F

.field private d:I

.field private e:[I

.field private f:[I

.field private g:[I

.field private h:I

.field private i:I

.field private j:I

.field private k:Lcom/android/launcher2/ib;

.field private l:Landroid/animation/ValueAnimator;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z

.field protected p:I

.field protected q:F

.field protected r:F

.field protected s:F

.field protected t:Z

.field protected u:I

.field protected v:I

.field protected w:I

.field protected x:Landroid/widget/Scroller;

.field protected y:F

.field protected z:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 218
    const v0, 0x45cb2000

    sput v0, Lcom/android/launcher2/PagedView;->ay:F

    .line 219
    const/high16 v0, 0x44a0

    sput v0, Lcom/android/launcher2/PagedView;->az:F

    .line 220
    const v0, 0x3f3d70a4

    sput v0, Lcom/android/launcher2/PagedView;->aA:F

    .line 221
    const v0, 0x3f266666

    sput v0, Lcom/android/launcher2/PagedView;->aB:F

    .line 222
    const/high16 v0, 0x41b0

    sput v0, Lcom/android/launcher2/PagedView;->aC:F

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 502
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 503
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 506
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/android/launcher2/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 507
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x2

    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 510
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/launcher2/PagedView;->p:I

    .line 95
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->t:Z

    .line 98
    const/16 v0, -0x3e7

    iput v0, p0, Lcom/android/launcher2/PagedView;->v:I

    .line 109
    iput v1, p0, Lcom/android/launcher2/PagedView;->d:I

    .line 122
    iput v2, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 123
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->D:Z

    .line 128
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->F:Z

    .line 141
    iput v2, p0, Lcom/android/launcher2/PagedView;->O:I

    .line 142
    iput v2, p0, Lcom/android/launcher2/PagedView;->P:I

    .line 144
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->R:Z

    .line 146
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->T:[I

    .line 155
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/android/launcher2/PagedView;->W:F

    .line 159
    iput v1, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 166
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->ab:Z

    .line 169
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->ac:Z

    .line 173
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->ad:Z

    .line 177
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->ae:Z

    .line 179
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->af:Z

    .line 182
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->ag:Z

    .line 189
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->n:Z

    .line 190
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->o:Z

    .line 191
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->aG:Z

    .line 199
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->aj:Z

    .line 200
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->ak:Z

    .line 201
    iput v2, p0, Lcom/android/launcher2/PagedView;->al:I

    .line 202
    const/high16 v0, 0x4040

    iput v0, p0, Lcom/android/launcher2/PagedView;->am:F

    .line 203
    const/high16 v0, 0x3fc0

    iput v0, p0, Lcom/android/launcher2/PagedView;->an:F

    .line 205
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->ao:Z

    .line 206
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->ap:Z

    .line 207
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->aq:Z

    .line 208
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->ar:Z

    .line 209
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->as:Z

    .line 210
    iput v2, p0, Lcom/android/launcher2/PagedView;->at:I

    .line 211
    const-string v0, "NONE"

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->au:Ljava/lang/String;

    .line 212
    iput v1, p0, Lcom/android/launcher2/PagedView;->av:I

    .line 215
    const v0, 0x7f0d0012

    iput v0, p0, Lcom/android/launcher2/PagedView;->aw:I

    .line 217
    new-instance v0, Lcom/android/launcher2/ij;

    invoke-direct {v0}, Lcom/android/launcher2/ij;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->ax:Lcom/android/launcher2/ij;

    .line 224
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    const v1, 0x3f666666

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->aD:Landroid/view/animation/AccelerateInterpolator;

    .line 225
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x4080

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->aE:Landroid/view/animation/DecelerateInterpolator;

    .line 2368
    new-instance v0, Lcom/android/launcher2/hs;

    invoke-direct {v0, p0}, Lcom/android/launcher2/hs;-><init>(Lcom/android/launcher2/PagedView;)V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->aF:Ljava/lang/Runnable;

    .line 513
    sget-object v0, Lcom/anddoes/launcher/at;->PagedView:[I

    .line 512
    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 514
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->setPageSpacing(I)V

    .line 515
    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->I:I

    .line 518
    const/4 v1, 0x3

    .line 517
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->J:I

    .line 520
    const/4 v1, 0x4

    .line 519
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->K:I

    .line 522
    const/4 v1, 0x5

    .line 521
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->L:I

    .line 523
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->M:I

    .line 525
    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->N:I

    .line 528
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 527
    iput v1, p0, Lcom/android/launcher2/PagedView;->ah:I

    .line 530
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 529
    iput v1, p0, Lcom/android/launcher2/PagedView;->ai:I

    .line 531
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 533
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->setHapticFeedbackEnabled(Z)V

    .line 534
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->b()V

    .line 535
    return-void
.end method

.method protected static a(FFFF)D
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 248
    sub-float v0, p2, p0

    float-to-double v0, v0

    .line 249
    sub-float v2, p3, p1

    float-to-double v2, v2

    .line 250
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private a(II)I
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 454
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    .line 455
    if-lez v0, :cond_17

    .line 456
    const/4 v1, -0x1

    if-ne p1, v1, :cond_18

    .line 457
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v0, v1

    mul-int/lit8 p2, v0, -0x1

    .line 462
    :cond_17
    :goto_17
    return p2

    .line 458
    :cond_18
    if-ne p1, v0, :cond_17

    .line 459
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v0

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v0, v1

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    sub-int p2, v0, v1

    goto :goto_17
.end method

.method static synthetic a(Lcom/android/launcher2/PagedView;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    if-nez v0, :cond_a

    .line 1946
    invoke-static {}, Lcom/anddoes/launcher/av;->a()Lcom/anddoes/launcher/av;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    .line 1948
    :cond_a
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    invoke-virtual {v0, p1}, Lcom/anddoes/launcher/av;->a(Landroid/view/MotionEvent;)V

    .line 1949
    return-void
.end method

.method private c()V
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 1068
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    .line 1069
    if-nez v1, :cond_f

    .line 1070
    iput-object v0, p0, Lcom/android/launcher2/PagedView;->e:[I

    .line 1071
    iput-object v0, p0, Lcom/android/launcher2/PagedView;->f:[I

    .line 1072
    iput-object v0, p0, Lcom/android/launcher2/PagedView;->g:[I

    .line 1084
    :cond_e
    return-void

    .line 1076
    :cond_f
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->e:[I

    .line 1077
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->f:[I

    .line 1078
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->g:[I

    .line 1079
    const/4 v0, 0x0

    :goto_1c
    if-ge v0, v1, :cond_e

    .line 1080
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->e:[I

    aput v3, v2, v0

    .line 1081
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->f:[I

    aput v3, v2, v0

    .line 1082
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->g:[I

    aput v3, v2, v0

    .line 1079
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c
.end method

.method private c(Landroid/view/MotionEvent;)V
    .registers 5
    .parameter

    .prologue
    .line 1959
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 1961
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    .line 1962
    iget v2, p0, Lcom/android/launcher2/PagedView;->Z:I

    if-ne v1, v2, :cond_37

    .line 1966
    if-nez v0, :cond_38

    const/4 v0, 0x1

    .line 1967
    :goto_15
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->b:F

    iput v1, p0, Lcom/android/launcher2/PagedView;->y:F

    .line 1968
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->c:F

    iput v1, p0, Lcom/android/launcher2/PagedView;->A:F

    .line 1969
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/launcher2/PagedView;->z:F

    .line 1970
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1971
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    if-eqz v0, :cond_37

    .line 1972
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    invoke-virtual {v0}, Lcom/anddoes/launcher/av;->c()V

    .line 1975
    :cond_37
    return-void

    .line 1966
    :cond_38
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private d()V
    .registers 2

    .prologue
    .line 1952
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    if-eqz v0, :cond_c

    .line 1953
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    invoke-virtual {v0}, Lcom/anddoes/launcher/av;->b()V

    .line 1954
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    .line 1956
    :cond_c
    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    .line 2471
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_8

    .line 2484
    :cond_7
    :goto_7
    return-void

    .line 2472
    :cond_8
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->B()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2473
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    if-eqz v0, :cond_23

    .line 2474
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    const/16 v2, -0x3e7

    if-eq v0, v2, :cond_20

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    :goto_1c
    invoke-interface {v1, v0}, Lcom/android/launcher2/hz;->setCurrentPage(I)V

    goto :goto_7

    :cond_20
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    goto :goto_1c

    .line 2477
    :cond_23
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollingIndicator()Landroid/view/View;

    .line 2478
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-eqz v0, :cond_2d

    .line 2479
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->i()V

    .line 2481
    :cond_2d
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->o:Z

    if-eqz v0, :cond_7

    .line 2482
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aG:Z

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->d(Z)V

    goto :goto_7
.end method

.method private g(I)I
    .registers 3
    .parameter

    .prologue
    .line 488
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    .line 489
    if-gez p1, :cond_9

    .line 490
    add-int/lit8 p1, v0, -0x1

    .line 494
    :cond_8
    :goto_8
    return p1

    .line 491
    :cond_9
    if-lt p1, v0, :cond_8

    .line 492
    const/4 p1, 0x0

    goto :goto_8
.end method

.method private h(I)I
    .registers 4
    .parameter

    .prologue
    .line 2005
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 2006
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_14

    .line 2007
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 2009
    :cond_14
    iget v1, p0, Lcom/android/launcher2/PagedView;->j:I

    .line 2010
    if-le v1, v0, :cond_19

    move v0, v1

    :cond_19
    return v0
.end method

.method private i()V
    .registers 7

    .prologue
    .line 2487
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->B()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2513
    :cond_6
    :goto_6
    return-void

    .line 2488
    :cond_7
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 2489
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    .line 2490
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    .line 2491
    iget v2, p0, Lcom/android/launcher2/PagedView;->av:I

    if-lez v2, :cond_19

    .line 2492
    iget v0, p0, Lcom/android/launcher2/PagedView;->av:I

    .line 2494
    :cond_19
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2495
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v3

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v2

    sub-int v2, v3, v2

    .line 2496
    iget v3, p0, Lcom/android/launcher2/PagedView;->ah:I

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/android/launcher2/PagedView;->ai:I

    sub-int v3, v0, v3

    .line 2497
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    .line 2498
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    .line 2497
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    const/4 v4, 0x0

    const/high16 v5, 0x3f80

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_80

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v0

    :goto_4f
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 2501
    div-int v1, v3, v1

    .line 2502
    sub-int v2, v3, v1

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iget v2, p0, Lcom/android/launcher2/PagedView;->ah:I

    add-int/2addr v0, v2

    .line 2503
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-eq v2, v1, :cond_79

    .line 2505
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2506
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 2509
    :cond_79
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_6

    .line 2497
    :cond_80
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    goto :goto_4f
.end method


# virtual methods
.method protected final A()V
    .registers 3

    .prologue
    .line 2307
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->c(IZ)V

    .line 2308
    return-void
.end method

.method protected B()Z
    .registers 2

    .prologue
    .line 2365
    const/4 v0, 0x1

    return v0
.end method

.method protected final C()V
    .registers 2

    .prologue
    .line 2412
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 2413
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 2415
    :cond_9
    return-void
.end method

.method public final D()V
    .registers 3

    .prologue
    .line 2418
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-eqz v0, :cond_11

    .line 2419
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2420
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2422
    :cond_11
    return-void
.end method

.method protected final a(ILandroid/view/View;I)F
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 1649
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_2e

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    :goto_a
    div-int/lit8 v0, v0, 0x2

    .line 1651
    invoke-virtual {p0, p2}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v1, v2

    .line 1652
    invoke-virtual {p0, p3}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v2

    .line 1653
    invoke-virtual {p0, p3}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1652
    sub-int v0, p1, v0

    .line 1655
    int-to-float v0, v0

    int-to-float v1, v1

    mul-float/2addr v1, v4

    div-float/2addr v0, v1

    .line 1656
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1657
    const/high16 v1, -0x4080

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1658
    return v0

    .line 1649
    :cond_2e
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_a
.end method

.method a(I)Landroid/view/View;
    .registers 3
    .parameter

    .prologue
    .line 594
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(IZ)V
.end method

.method protected a(Landroid/view/MotionEvent;)V
    .registers 4
    .parameter

    .prologue
    .line 1551
    const/high16 v0, 0x3f80

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/android/launcher2/PagedView;->a(Landroid/view/MotionEvent;FZ)V

    .line 1552
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;FZ)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1563
    iget v0, p0, Lcom/android/launcher2/PagedView;->Z:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1564
    const/4 v1, -0x1

    if-ne v0, v1, :cond_a

    .line 1633
    :cond_9
    :goto_9
    return-void

    .line 1567
    :cond_a
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    .line 1568
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    .line 1569
    iget v0, p0, Lcom/android/launcher2/PagedView;->y:F

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v6, v0

    .line 1570
    iget v0, p0, Lcom/android/launcher2/PagedView;->A:F

    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-int v7, v0

    .line 1572
    iget v0, p0, Lcom/android/launcher2/PagedView;->G:I

    int-to-float v0, v0

    mul-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 1573
    iget v0, p0, Lcom/android/launcher2/PagedView;->h:I

    if-le v6, v0, :cond_84

    const/4 v0, 0x1

    .line 1574
    :goto_31
    iget v1, p0, Lcom/android/launcher2/PagedView;->h:I

    if-le v7, v1, :cond_86

    const/4 v1, 0x1

    move v3, v1

    .line 1575
    :goto_37
    if-le v6, v8, :cond_89

    const/4 v1, 0x1

    move v2, v1

    .line 1576
    :goto_3b
    if-le v7, v8, :cond_8c

    const/4 v1, 0x1

    .line 1578
    :goto_3e
    if-nez v2, :cond_48

    iget-boolean v9, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v9, :cond_8e

    if-nez v3, :cond_48

    :goto_46
    if-eqz v1, :cond_9

    .line 1579
    :cond_48
    iget-boolean v9, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v9, :cond_bf

    .line 1580
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ad:Z

    if-eqz v0, :cond_91

    if-eqz v3, :cond_93

    .line 1582
    :cond_52
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 1583
    iget v0, p0, Lcom/android/launcher2/PagedView;->B:F

    iget v1, p0, Lcom/android/launcher2/PagedView;->A:F

    sub-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->B:F

    .line 1584
    iput v5, p0, Lcom/android/launcher2/PagedView;->A:F

    .line 1585
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/PagedView;->z:F

    .line 1586
    iget v0, p0, Lcom/android/launcher2/PagedView;->mScrollY:I

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->s:F

    .line 1587
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->r:F

    .line 1588
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_80

    .line 1589
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->u()V

    .line 1631
    :cond_80
    :goto_80
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->x()V

    goto :goto_9

    .line 1573
    :cond_84
    const/4 v0, 0x0

    goto :goto_31

    .line 1574
    :cond_86
    const/4 v1, 0x0

    move v3, v1

    goto :goto_37

    .line 1575
    :cond_89
    const/4 v1, 0x0

    move v2, v1

    goto :goto_3b

    .line 1576
    :cond_8c
    const/4 v1, 0x0

    goto :goto_3e

    .line 1578
    :cond_8e
    if-nez v0, :cond_48

    goto :goto_46

    .line 1580
    :cond_91
    if-nez v1, :cond_52

    .line 1592
    :cond_93
    if-eqz p3, :cond_80

    .line 1593
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_80

    .line 1594
    int-to-float v0, v6

    int-to-float v1, v8

    iget v2, p0, Lcom/android/launcher2/PagedView;->am:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_80

    .line 1595
    int-to-float v0, v7

    int-to-float v1, v8

    iget v2, p0, Lcom/android/launcher2/PagedView;->an:F

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_80

    .line 1597
    iget v0, p0, Lcom/android/launcher2/PagedView;->y:F

    sub-float v0, v4, v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_bb

    .line 1598
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    goto :goto_80

    .line 1600
    :cond_bb
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    goto :goto_80

    .line 1605
    :cond_bf
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ad:Z

    if-eqz v1, :cond_f6

    if-eqz v0, :cond_f8

    .line 1607
    :cond_c5
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 1608
    iget v0, p0, Lcom/android/launcher2/PagedView;->B:F

    iget v1, p0, Lcom/android/launcher2/PagedView;->y:F

    sub-float/2addr v1, v4

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->B:F

    .line 1609
    iput v4, p0, Lcom/android/launcher2/PagedView;->y:F

    .line 1610
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/PagedView;->z:F

    .line 1611
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->s:F

    .line 1612
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->r:F

    .line 1613
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_80

    .line 1614
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->u()V

    goto :goto_80

    .line 1605
    :cond_f6
    if-nez v2, :cond_c5

    .line 1617
    :cond_f8
    if-eqz p3, :cond_80

    .line 1618
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_80

    .line 1619
    int-to-float v0, v7

    int-to-float v1, v8

    iget v2, p0, Lcom/android/launcher2/PagedView;->am:F

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_80

    .line 1620
    int-to-float v0, v6

    int-to-float v1, v8

    iget v2, p0, Lcom/android/launcher2/PagedView;->an:F

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_80

    .line 1622
    iget v0, p0, Lcom/android/launcher2/PagedView;->A:F

    sub-float v0, v5, v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_121

    .line 1623
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    goto/16 :goto_80

    .line 1625
    :cond_121
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    goto/16 :goto_80
.end method

.method protected a(FF)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1436
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_14

    :goto_4
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->H:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_16

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    move p2, p1

    goto :goto_4

    :cond_16
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public a_()V
    .registers 3

    .prologue
    .line 2160
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_19

    const/4 v0, -0x1

    .line 2161
    :goto_5
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 2162
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    if-le v1, v0, :cond_18

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 2166
    :cond_18
    :goto_18
    return-void

    .line 2160
    :cond_19
    const/4 v0, 0x0

    goto :goto_5

    .line 2164
    :cond_1b
    iget v1, p0, Lcom/android/launcher2/PagedView;->v:I

    if-le v1, v0, :cond_18

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    goto :goto_18
.end method

.method protected a_(III)V
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2127
    iput p1, p0, Lcom/android/launcher2/PagedView;->v:I

    .line 2129
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 2130
    if-eqz v0, :cond_18

    iget v2, p0, Lcom/android/launcher2/PagedView;->u:I

    if-eq p1, v2, :cond_18

    .line 2131
    iget v2, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v2

    if-ne v0, v2, :cond_18

    .line 2132
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 2135
    :cond_18
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->u()V

    .line 2136
    invoke-virtual {p0, p3}, Lcom/android/launcher2/PagedView;->awakenScrollBars(I)Z

    .line 2137
    if-nez p3, :cond_5e

    .line 2138
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 2141
    :goto_24
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_31

    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 2142
    :cond_31
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_4e

    .line 2143
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    iget v2, p0, Lcom/android/launcher2/PagedView;->S:I

    move v3, v1

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 2150
    :goto_3e
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ae:Z

    if-eqz v0, :cond_5a

    .line 2151
    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->b(IZ)V

    .line 2155
    :goto_47
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->t()V

    .line 2156
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 2157
    return-void

    .line 2145
    :cond_4e
    iget-object v6, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    iget v7, p0, Lcom/android/launcher2/PagedView;->S:I

    move v8, v1

    move v9, p2

    move v10, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_3e

    .line 2153
    :cond_5a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->aH:Z

    goto :goto_47

    :cond_5e
    move v5, p3

    goto :goto_24
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1376
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    if-ltz v0, :cond_15

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    if-ge v0, v1, :cond_15

    .line 1377
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1379
    :cond_15
    const/16 v0, 0x11

    if-ne p2, v0, :cond_29

    .line 1380
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    if-lez v0, :cond_28

    .line 1381
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1388
    :cond_28
    :goto_28
    return-void

    .line 1383
    :cond_29
    const/16 v0, 0x42

    if-ne p2, v0, :cond_28

    .line 1384
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_28

    .line 1385
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_28
.end method

.method protected b(I)I
    .registers 2
    .parameter

    .prologue
    .line 598
    return p1
.end method

.method protected b()V
    .registers 3

    .prologue
    .line 541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    .line 542
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 543
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->n()V

    .line 544
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 545
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->Q:Z

    .line 547
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 548
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->G:I

    .line 549
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/android/launcher2/PagedView;->h:I

    .line 550
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->i:I

    .line 551
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/launcher2/PagedView;->q:F

    .line 556
    invoke-virtual {p0, p0}, Lcom/android/launcher2/PagedView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 557
    return-void
.end method

.method protected b(F)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/high16 v4, 0x3f80

    .line 1734
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v1

    int-to-float v0, v1

    div-float v0, p1, v0

    cmpl-float v2, v0, v5

    if-eqz v2, :cond_4a

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float v2, v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float/2addr v0, v4

    mul-float v3, v0, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v4

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v4

    if-ltz v2, :cond_2c

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v0, v2

    :cond_2c
    const v2, 0x3e0f5c29

    mul-float/2addr v0, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    cmpg-float v1, p1, v5

    if-gez v1, :cond_53

    iput v0, p0, Lcom/android/launcher2/PagedView;->V:I

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_4b

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    invoke-super {p0, v0, v6}, Landroid/view/ViewGroup;->scrollTo(II)V

    :goto_47
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 1735
    :cond_4a
    return-void

    .line 1734
    :cond_4b
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v0

    invoke-super {p0, v6, v0}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_47

    :cond_53
    iget v1, p0, Lcom/android/launcher2/PagedView;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->V:I

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_66

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->w:I

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_47

    :cond_66
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_47
.end method

.method protected b(II)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x3f80

    .line 2061
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    .line 2062
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_42

    const/4 v0, -0x1

    :goto_c
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v3, :cond_44

    :goto_10
    sub-int v1, v2, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2064
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_46

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    :goto_22
    div-int/lit8 v0, v0, 0x2

    .line 2069
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v2

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 2070
    invoke-direct {p0, v1, v2}, Lcom/android/launcher2/PagedView;->a(II)I

    move-result v2

    .line 2071
    iget v3, p0, Lcom/android/launcher2/PagedView;->S:I

    sub-int/2addr v2, v3

    .line 2072
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/16 v4, 0xfa

    if-ge v3, v4, :cond_4b

    .line 2077
    const/16 v0, 0x1c2

    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/PagedView;->c(II)V

    .line 2100
    :goto_41
    return-void

    :cond_42
    move v0, v1

    .line 2062
    goto :goto_c

    :cond_44
    const/4 v1, 0x1

    goto :goto_10

    .line 2064
    :cond_46
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_22

    .line 2085
    :cond_4b
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v5

    mul-int/lit8 v4, v0, 0x2

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 2086
    int-to-float v4, v0

    int-to-float v0, v0

    .line 2087
    const/high16 v5, 0x3f00

    sub-float/2addr v3, v5

    float-to-double v5, v3

    const-wide v7, 0x3fde28c7460698c7L

    mul-double/2addr v5, v7

    double-to-float v3, v5

    float-to-double v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    double-to-float v3, v5

    mul-float/2addr v0, v3

    .line 2086
    add-float/2addr v0, v4

    .line 2089
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 2090
    const/16 v4, 0x898

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2095
    const/high16 v4, 0x447a

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    mul-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 2096
    iget v3, p0, Lcom/android/launcher2/PagedView;->at:I

    if-le v0, v3, :cond_91

    iget v3, p0, Lcom/android/launcher2/PagedView;->at:I

    if-lez v3, :cond_91

    .line 2097
    iget v0, p0, Lcom/android/launcher2/PagedView;->at:I

    .line 2099
    :cond_91
    invoke-virtual {p0, v1, v2, v0}, Lcom/android/launcher2/PagedView;->a_(III)V

    goto :goto_41
.end method

.method protected final b(IZ)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2240
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ab:Z

    if-eqz v0, :cond_1a

    .line 2241
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v4

    .line 2242
    if-ge p1, v4, :cond_1a

    .line 2243
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->d(I)I

    move-result v5

    .line 2244
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->e(I)I

    move-result v6

    move v3, v2

    .line 2248
    :goto_15
    if-lt v3, v4, :cond_1b

    move v3, v2

    .line 2267
    :goto_18
    if-lt v3, v4, :cond_5c

    .line 2282
    :cond_1a
    return-void

    .line 2251
    :cond_1b
    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2252
    instance-of v7, v0, Lcom/android/launcher2/hr;

    if-eqz v7, :cond_53

    .line 2253
    check-cast v0, Lcom/android/launcher2/hr;

    .line 2257
    :goto_25
    if-lt v3, v5, :cond_3d

    if-gt v3, v6, :cond_3d

    .line 2258
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v7, :cond_33

    if-nez p1, :cond_33

    add-int/lit8 v7, v4, -0x1

    if-eq v3, v7, :cond_3d

    .line 2259
    :cond_33
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v7, :cond_4f

    add-int/lit8 v7, v4, -0x1

    if-ne p1, v7, :cond_4f

    if-nez v3, :cond_4f

    .line 2260
    :cond_3d
    invoke-interface {v0}, Lcom/android/launcher2/hr;->getPageChildCount()I

    move-result v7

    if-lez v7, :cond_46

    .line 2261
    invoke-interface {v0}, Lcom/android/launcher2/hr;->a()V

    .line 2263
    :cond_46
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v3, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2248
    :cond_4f
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_15

    .line 2255
    :cond_53
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/hr;

    goto :goto_25

    .line 2268
    :cond_5c
    if-eq v3, p1, :cond_60

    if-nez p2, :cond_97

    .line 2269
    :cond_60
    if-gt v5, v3, :cond_64

    if-le v3, v6, :cond_78

    .line 2272
    :cond_64
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_6e

    if-nez p1, :cond_6e

    add-int/lit8 v0, v4, -0x1

    if-eq v3, v0, :cond_78

    .line 2273
    :cond_6e
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_97

    add-int/lit8 v0, v4, -0x1

    if-ne p1, v0, :cond_97

    if-nez v3, :cond_97

    .line 2274
    :cond_78
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_97

    .line 2275
    if-ne v3, p1, :cond_9c

    if-eqz p2, :cond_9c

    move v0, v1

    :goto_8b
    invoke-virtual {p0, v3, v0}, Lcom/android/launcher2/PagedView;->a(IZ)V

    .line 2276
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v0, v3, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 2267
    :cond_97
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_18

    :cond_9c
    move v0, v2

    .line 2275
    goto :goto_8b
.end method

.method protected final b([I)V
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v0, -0x1

    const/4 v5, 0x0

    .line 1146
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v6

    .line 1148
    if-lez v6, :cond_b1

    .line 1149
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_76

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    .line 1151
    :goto_11
    invoke-virtual {p0, v5}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    move-object v3, v1

    move v4, v5

    .line 1153
    :goto_17
    add-int/lit8 v1, v6, -0x1

    if-ge v4, v1, :cond_44

    .line 1154
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_83

    invoke-virtual {v3}, Landroid/view/View;->getY()F

    move-result v1

    .line 1155
    :goto_23
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v2, :cond_88

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v2

    :goto_2b
    int-to-float v2, v2

    .line 1154
    add-float/2addr v2, v1

    .line 1156
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_8d

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    :goto_35
    int-to-float v1, v1

    .line 1154
    sub-float/2addr v2, v1

    .line 1157
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_92

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v1

    :goto_3f
    int-to-float v1, v1

    cmpg-float v1, v2, v1

    if-ltz v1, :cond_7b

    .line 1162
    :cond_44
    add-int/lit8 v1, v4, 0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    move v3, v4

    .line 1163
    :goto_4c
    add-int/lit8 v1, v6, -0x1

    if-ge v3, v1, :cond_71

    .line 1164
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_a2

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v1

    .line 1165
    :goto_58
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v7, :cond_a7

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    :goto_60
    int-to-float v2, v2

    .line 1164
    sub-float v2, v1, v2

    .line 1166
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_ac

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v1

    :goto_6b
    add-int/2addr v1, v0

    int-to-float v1, v1

    cmpg-float v1, v2, v1

    if-ltz v1, :cond_97

    .line 1170
    :cond_71
    aput v4, p1, v5

    .line 1171
    aput v3, p1, v8

    .line 1176
    :goto_75
    return-void

    .line 1149
    :cond_76
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_11

    .line 1158
    :cond_7b
    add-int/lit8 v4, v4, 0x1

    .line 1159
    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    move-object v3, v1

    goto :goto_17

    .line 1154
    :cond_83
    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v1

    goto :goto_23

    .line 1155
    :cond_88
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v2

    goto :goto_2b

    .line 1156
    :cond_8d
    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    goto :goto_35

    .line 1157
    :cond_92
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v1

    goto :goto_3f

    .line 1167
    :cond_97
    add-int/lit8 v2, v3, 0x1

    .line 1168
    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    move v3, v2

    move-object v2, v1

    goto :goto_4c

    .line 1164
    :cond_a2
    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v1

    goto :goto_58

    .line 1165
    :cond_a7
    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    goto :goto_60

    .line 1166
    :cond_ac
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v1

    goto :goto_6b

    .line 1173
    :cond_b1
    aput v0, p1, v5

    .line 1174
    aput v0, p1, v8

    goto :goto_75
.end method

.method protected b(FF)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1443
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_1d

    :goto_4
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_1f

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    :goto_c
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_24

    const/4 v0, 0x1

    :goto_1c
    return v0

    :cond_1d
    move p2, p1

    goto :goto_4

    :cond_1f
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_c

    :cond_24
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method protected final c(F)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1669
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v1

    .line 1673
    const/high16 v0, 0x4000

    int-to-float v2, v1

    div-float v2, p1, v2

    mul-float/2addr v0, v2

    .line 1675
    cmpl-float v2, v0, v4

    if-nez v2, :cond_11

    .line 1699
    :goto_10
    return-void

    .line 1678
    :cond_11
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x3f80

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_20

    .line 1679
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    div-float/2addr v0, v2

    .line 1682
    :cond_20
    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1683
    cmpg-float v1, p1, v4

    if-gez v1, :cond_43

    .line 1684
    iput v0, p0, Lcom/android/launcher2/PagedView;->V:I

    .line 1685
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_3b

    .line 1686
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    invoke-super {p0, v0, v5}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 1698
    :goto_37
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_10

    .line 1688
    :cond_3b
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v0

    invoke-super {p0, v5, v0}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_37

    .line 1691
    :cond_43
    iget v1, p0, Lcom/android/launcher2/PagedView;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->V:I

    .line 1692
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_56

    .line 1693
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->w:I

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_37

    .line 1695
    :cond_56
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_37
.end method

.method protected c(I)V
    .registers 3
    .parameter

    .prologue
    .line 1036
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->B()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1037
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->e()V

    .line 1052
    :cond_9
    return-void
.end method

.method protected c(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 2111
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    .line 2112
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_2c

    const/4 v0, -0x1

    :goto_a
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v3, :cond_2e

    :goto_e
    sub-int v1, v2, v1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2119
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 2120
    invoke-direct {p0, v0, v1}, Lcom/android/launcher2/PagedView;->a(II)I

    move-result v1

    .line 2121
    iget v2, p0, Lcom/android/launcher2/PagedView;->S:I

    .line 2122
    sub-int/2addr v1, v2

    .line 2123
    invoke-virtual {p0, v0, v1, p2}, Lcom/android/launcher2/PagedView;->a_(III)V

    .line 2124
    return-void

    :cond_2c
    move v0, v1

    .line 2112
    goto :goto_a

    :cond_2e
    const/4 v1, 0x1

    goto :goto_e
.end method

.method protected final c(IZ)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x4000

    const/4 v4, 0x1

    .line 2313
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ag:Z

    if-nez v0, :cond_8

    .line 2346
    :cond_7
    :goto_7
    return-void

    .line 2317
    :cond_8
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ab:Z

    if-eqz v0, :cond_7

    .line 2319
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 2320
    const/16 v0, -0x3e7

    iput v0, p0, Lcom/android/launcher2/PagedView;->v:I

    .line 2323
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->f_()V

    .line 2327
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2328
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2327
    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->measure(II)V

    .line 2331
    if-ltz p1, :cond_3a

    .line 2332
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setCurrentPage(I)V

    .line 2336
    :cond_3a
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    .line 2337
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2338
    const/4 v0, 0x0

    :goto_44
    if-lt v0, v1, :cond_4f

    .line 2343
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0, p2}, Lcom/android/launcher2/PagedView;->b(IZ)V

    .line 2344
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->requestLayout()V

    goto :goto_7

    .line 2339
    :cond_4f
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->aa:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2338
    add-int/lit8 v0, v0, 0x1

    goto :goto_44
.end method

.method protected final c(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f00

    const/high16 v3, 0x3f80

    const/4 v2, 0x0

    .line 430
    if-eqz p1, :cond_3a

    .line 431
    iget v0, p0, Lcom/android/launcher2/PagedView;->q:F

    sget v1, Lcom/android/launcher2/PagedView;->az:F

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setCameraDistance(F)V

    .line 432
    invoke-virtual {p1, v2}, Landroid/view/View;->setRotation(F)V

    .line 433
    invoke-virtual {p1, v2}, Landroid/view/View;->setRotationY(F)V

    .line 434
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 435
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 436
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleX(F)V

    .line 437
    invoke-virtual {p1, v3}, Landroid/view/View;->setScaleY(F)V

    .line 438
    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 439
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotX(F)V

    .line 440
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v4

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotY(F)V

    .line 441
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 443
    :cond_3a
    return-void
.end method

.method protected final c(Z)V
    .registers 5
    .parameter

    .prologue
    .line 2375
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aF:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2376
    if-eqz p1, :cond_13

    const/4 v0, 0x0

    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->d(Z)V

    .line 2377
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aF:Ljava/lang/Runnable;

    const-wide/16 v1, 0x28a

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/launcher2/PagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2378
    return-void

    .line 2376
    :cond_13
    const/4 v0, 0x1

    goto :goto_8
.end method

.method public computeScroll()V
    .registers 1

    .prologue
    .line 811
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->w()Z

    .line 812
    return-void
.end method

.method protected d(I)I
    .registers 4
    .parameter

    .prologue
    .line 2285
    const/4 v0, 0x0

    add-int/lit8 v1, p1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected final d(Landroid/view/View;)I
    .registers 4
    .parameter

    .prologue
    .line 1136
    if-eqz p1, :cond_1e

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 1137
    :goto_6
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_10

    .line 1138
    if-eqz p1, :cond_23

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 1140
    :cond_10
    :goto_10
    iget v1, p0, Lcom/android/launcher2/PagedView;->j:I

    .line 1141
    if-le v1, v0, :cond_15

    move v0, v1

    .line 1142
    :cond_15
    int-to-float v0, v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->W:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0

    .line 1136
    :cond_1e
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_6

    .line 1138
    :cond_23
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    goto :goto_10
.end method

.method public d(Z)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 2381
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_b

    .line 2409
    :cond_a
    :goto_a
    return-void

    .line 2385
    :cond_b
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->o:Z

    .line 2386
    iput-boolean v3, p0, Lcom/android/launcher2/PagedView;->aG:Z

    .line 2387
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v3, :cond_a

    .line 2388
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->B()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2389
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    if-eqz v0, :cond_34

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aj:Z

    if-eqz v0, :cond_34

    .line 2390
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    const/16 v2, -0x3e7

    if-eq v0, v2, :cond_31

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    :goto_2d
    invoke-interface {v1, v0}, Lcom/android/launcher2/hz;->setCurrentPage(I)V

    goto :goto_a

    :cond_31
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    goto :goto_2d

    .line 2393
    :cond_34
    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->o:Z

    .line 2394
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollingIndicator()Landroid/view/View;

    .line 2395
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 2397
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->i()V

    .line 2398
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aj:Z

    if-eqz v0, :cond_55

    move v0, v1

    :goto_47
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2399
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->C()V

    .line 2401
    if-eqz p1, :cond_57

    .line 2402
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    goto :goto_a

    .line 2398
    :cond_55
    const/4 v0, 0x4

    goto :goto_47

    .line 2404
    :cond_57
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    const-string v2, "alpha"

    new-array v3, v3, [F

    aput v4, v3, v1

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    .line 2405
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2406
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_a
.end method

.method public d_()V
    .registers 4

    .prologue
    .line 2169
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_1f

    const/4 v0, 0x0

    .line 2170
    :goto_5
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_21

    .line 2171
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    sub-int v0, v2, v0

    if-ge v1, v0, :cond_1e

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 2175
    :cond_1e
    :goto_1e
    return-void

    .line 2169
    :cond_1f
    const/4 v0, 0x1

    goto :goto_5

    .line 2173
    :cond_21
    iget v1, p0, Lcom/android/launcher2/PagedView;->v:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    sub-int v0, v2, v0

    if-ge v1, v0, :cond_1e

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    goto :goto_1e
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 16
    .parameter

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x1

    const/4 v4, -0x1

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 1184
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_ca

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    :goto_d
    div-int/lit8 v0, v0, 0x2

    .line 1187
    iget v1, p0, Lcom/android/launcher2/PagedView;->V:I

    add-int/2addr v0, v1

    .line 1190
    iget v1, p0, Lcom/android/launcher2/PagedView;->d:I

    if-ne v0, v1, :cond_1a

    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->D:Z

    if-eqz v1, :cond_21

    .line 1193
    :cond_1a
    iput-boolean v11, p0, Lcom/android/launcher2/PagedView;->D:Z

    .line 1194
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->c(I)V

    .line 1195
    iput v0, p0, Lcom/android/launcher2/PagedView;->d:I

    .line 1200
    :cond_21
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    .line 1201
    if-lez v1, :cond_c9

    .line 1202
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->T:[I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b([I)V

    .line 1203
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->T:[I

    aget v2, v0, v11

    .line 1204
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->T:[I

    aget v3, v0, v12

    .line 1205
    if-eq v2, v4, :cond_73

    if-eq v3, v4, :cond_73

    .line 1206
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getDrawingTime()J

    move-result-wide v4

    .line 1208
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1209
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v7

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getRight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    .line 1210
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v8

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getBottom()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getTop()I

    move-result v9

    sub-int/2addr v8, v9

    .line 1209
    invoke-virtual {p1, v0, v6, v7, v8}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 1218
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6c
    if-gez v0, :cond_d0

    .line 1235
    iput-boolean v11, p0, Lcom/android/launcher2/PagedView;->U:Z

    .line 1236
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1238
    :cond_73
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_c9

    if-le v1, v12, :cond_c9

    .line 1239
    iget v0, p0, Lcom/android/launcher2/PagedView;->V:I

    if-ltz v0, :cond_83

    iget v0, p0, Lcom/android/launcher2/PagedView;->V:I

    iget v2, p0, Lcom/android/launcher2/PagedView;->w:I

    if-le v0, v2, :cond_c9

    .line 1240
    :cond_83
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1241
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v2

    .line 1242
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v2

    iget v3, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v2, v3

    .line 1243
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p0, v11}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v2

    sub-int/2addr v0, v2

    .line 1244
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1245
    iget v2, p0, Lcom/android/launcher2/PagedView;->V:I

    if-gez v2, :cond_fe

    .line 1246
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v2, :cond_f3

    .line 1247
    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v10, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1251
    :goto_ae
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1252
    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1254
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getDrawingTime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/android/launcher2/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 1256
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_f9

    .line 1257
    int-to-float v0, v0

    invoke-virtual {p1, v10, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1278
    :cond_c6
    :goto_c6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1281
    :cond_c9
    return-void

    .line 1184
    :cond_ca
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto/16 :goto_d

    .line 1219
    :cond_d0
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v6

    .line 1220
    iget-boolean v7, p0, Lcom/android/launcher2/PagedView;->U:Z

    if-nez v7, :cond_dc

    .line 1221
    if-gt v2, v0, :cond_ef

    if-gt v0, v3, :cond_ef

    .line 1224
    :cond_dc
    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->e(Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_eb

    .line 1225
    invoke-virtual {v6, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1226
    invoke-virtual {p0, p1, v6, v4, v5}, Lcom/android/launcher2/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 1218
    :goto_e8
    add-int/lit8 v0, v0, -0x1

    goto :goto_6c

    .line 1228
    :cond_eb
    invoke-virtual {v6, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e8

    .line 1232
    :cond_ef
    invoke-virtual {v6, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e8

    .line 1249
    :cond_f3
    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v2, v10}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_ae

    .line 1259
    :cond_f9
    int-to-float v0, v0

    invoke-virtual {p1, v0, v10}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_c6

    .line 1261
    :cond_fe
    iget v1, p0, Lcom/android/launcher2/PagedView;->V:I

    iget v2, p0, Lcom/android/launcher2/PagedView;->w:I

    if-le v1, v2, :cond_c6

    .line 1262
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_124

    .line 1263
    int-to-float v1, v0

    invoke-virtual {p1, v10, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1267
    :goto_10c
    invoke-virtual {p0, v11}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    .line 1268
    invoke-virtual {v1, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1270
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getDrawingTime()J

    move-result-wide v2

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/android/launcher2/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 1272
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_129

    .line 1273
    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v10, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_c6

    .line 1265
    :cond_124
    int-to-float v1, v0

    invoke-virtual {p1, v1, v10}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_10c

    .line 1275
    :cond_129
    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0, v10}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_c6
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1360
    const/16 v1, 0x11

    if-ne p2, v1, :cond_15

    .line 1361
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    if-lez v1, :cond_2f

    .line 1362
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 1371
    :goto_14
    return v0

    .line 1365
    :cond_15
    const/16 v1, 0x42

    if-ne p2, v1, :cond_2f

    .line 1366
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_2f

    .line 1367
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->r(I)V

    goto :goto_14

    .line 1371
    :cond_2f
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_14
.end method

.method protected e(I)I
    .registers 4
    .parameter

    .prologue
    .line 2288
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    .line 2289
    add-int/lit8 v1, p1, 0x1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method protected final e(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2425
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-gt v0, v2, :cond_9

    .line 2460
    :cond_8
    :goto_8
    return-void

    .line 2426
    :cond_9
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->B()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2427
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    if-eqz v0, :cond_24

    .line 2428
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    const/16 v2, -0x3e7

    if-eq v0, v2, :cond_21

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    :goto_1d
    invoke-interface {v1, v0}, Lcom/android/launcher2/hz;->setCurrentPage(I)V

    goto :goto_8

    :cond_21
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    goto :goto_1d

    .line 2431
    :cond_24
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollingIndicator()Landroid/view/View;

    .line 2432
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-eqz v0, :cond_8

    .line 2434
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->i()V

    .line 2435
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->C()V

    .line 2436
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ak:Z

    if-nez v0, :cond_39

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aj:Z

    if-nez v0, :cond_8

    .line 2437
    :cond_39
    if-eqz p1, :cond_47

    .line 2438
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2439
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    goto :goto_8

    .line 2441
    :cond_47
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    const-string v1, "alpha"

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    .line 2442
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x28a

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2443
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/ht;

    invoke-direct {v1, p0}, Lcom/android/launcher2/ht;-><init>(Lcom/android/launcher2/PagedView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2456
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->l:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_8
.end method

.method protected e(Landroid/view/View;)Z
    .registers 4
    .parameter

    .prologue
    .line 1179
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_b

    const/4 v0, 0x1

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method protected e_()V
    .registers 1

    .prologue
    .line 666
    return-void
.end method

.method public final f(Landroid/view/View;)I
    .registers 6
    .parameter

    .prologue
    .line 2178
    if-eqz p1, :cond_d

    .line 2180
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 2181
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    .line 2182
    const/4 v0, 0x0

    :goto_b
    if-lt v0, v2, :cond_f

    .line 2188
    :cond_d
    const/4 v0, -0x1

    :cond_e
    return v0

    .line 2183
    :cond_f
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v3

    if-eq v1, v3, :cond_e

    .line 2182
    add-int/lit8 v0, v0, 0x1

    goto :goto_b
.end method

.method protected f(I)V
    .registers 2
    .parameter

    .prologue
    .line 269
    return-void
.end method

.method public abstract f_()V
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 1399
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    move-object v0, p1

    .line 1402
    :goto_7
    if-ne v0, v1, :cond_d

    .line 1403
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    .line 1413
    :cond_c
    return-void

    .line 1406
    :cond_d
    if-eq v0, p0, :cond_c

    .line 1409
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 1410
    instance-of v2, v2, Landroid/view/View;

    if-eqz v2, :cond_c

    .line 1411
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_7
.end method

.method protected g()V
    .registers 1

    .prologue
    .line 670
    return-void
.end method

.method getCurrentPage()I
    .registers 2

    .prologue
    .line 583
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    return v0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .registers 5

    .prologue
    .line 2568
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0702ba

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 2569
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getNextPage()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2568
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getNextPage()I
    .registers 3

    .prologue
    .line 586
    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    :goto_8
    return v0

    :cond_9
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    goto :goto_8
.end method

.method getPageCount()I
    .registers 2

    .prologue
    .line 590
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    return v0
.end method

.method getPageNearestToCenterOfScreen()I
    .registers 8

    .prologue
    .line 2014
    const v4, 0x7fffffff

    .line 2015
    const/4 v1, -0x1

    .line 2016
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 2017
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v2, :cond_1c

    .line 2018
    iget v0, p0, Lcom/android/launcher2/PagedView;->mScrollY:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 2020
    :cond_1c
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v5

    .line 2021
    const/4 v2, 0x0

    :goto_21
    if-lt v2, v5, :cond_24

    .line 2032
    return v1

    .line 2022
    :cond_24
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v3

    .line 2023
    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v3

    .line 2024
    div-int/lit8 v3, v3, 0x2

    .line 2025
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v6

    add-int/2addr v3, v6

    .line 2026
    sub-int/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 2027
    if-ge v3, v4, :cond_3f

    move v1, v2

    .line 2021
    :goto_3b
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    goto :goto_21

    :cond_3f
    move v3, v4

    goto :goto_3b
.end method

.method protected getScrollingIndicator()Landroid/view/View;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 2351
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->n:Z

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-nez v0, :cond_29

    .line 2352
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2353
    if-eqz v0, :cond_29

    .line 2354
    iget v2, p0, Lcom/android/launcher2/PagedView;->aw:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    .line 2355
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    if-eqz v0, :cond_2c

    const/4 v0, 0x1

    :goto_1e
    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->n:Z

    .line 2356
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->n:Z

    if-eqz v0, :cond_29

    .line 2357
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2361
    :cond_29
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->m:Landroid/view/View;

    return-object v0

    :cond_2c
    move v0, v1

    .line 2355
    goto :goto_1e
.end method

.method protected final l(I)V
    .registers 15
    .parameter

    .prologue
    .line 272
    const/4 v2, 0x0

    .line 273
    const/4 v1, 0x0

    .line 274
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    if-lt v0, v3, :cond_2d

    .line 361
    if-eqz v2, :cond_11f

    .line 362
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 363
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 364
    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 365
    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 366
    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 367
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 396
    :cond_2c
    :goto_2c
    return-void

    .line 275
    :cond_2d
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v6

    .line 276
    invoke-virtual {p0, v6}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    .line 277
    if-eqz v6, :cond_c0

    .line 278
    invoke-virtual {p0, p1, v6, v0}, Lcom/android/launcher2/PagedView;->a(ILandroid/view/View;I)F

    move-result v7

    .line 280
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->ax:Lcom/android/launcher2/ij;

    const/4 v4, 0x0

    invoke-static {v7, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/launcher2/ij;->getInterpolation(F)F

    move-result v3

    .line 281
    const/high16 v4, 0x3f80

    sub-float/2addr v4, v3

    sget v5, Lcom/android/launcher2/PagedView;->aA:F

    mul-float/2addr v3, v5

    add-float v5, v4, v3

    .line 282
    const/4 v3, 0x0

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v3

    .line 285
    const/4 v3, 0x0

    cmpg-float v3, v7, v3

    if-gez v3, :cond_c7

    .line 286
    const/4 v3, 0x0

    cmpg-float v3, v7, v3

    if-gez v3, :cond_c4

    iget-object v3, p0, Lcom/android/launcher2/PagedView;->aD:Landroid/view/animation/AccelerateInterpolator;

    const/high16 v8, 0x3f80

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v9

    sub-float/2addr v8, v9

    invoke-virtual {v3, v8}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v3

    .line 293
    :goto_73
    iget v8, p0, Lcom/android/launcher2/PagedView;->q:F

    sget v9, Lcom/android/launcher2/PagedView;->ay:F

    mul-float/2addr v8, v9

    invoke-virtual {v6, v8}, Landroid/view/View;->setCameraDistance(F)V

    .line 294
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    .line 295
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 296
    int-to-float v9, v9

    const/high16 v10, 0x4000

    div-float/2addr v9, v10

    invoke-virtual {v6, v9}, Landroid/view/View;->setPivotY(F)V

    .line 297
    int-to-float v8, v8

    const/high16 v9, 0x4000

    div-float/2addr v8, v9

    invoke-virtual {v6, v8}, Landroid/view/View;->setPivotX(F)V

    .line 298
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/View;->setRotationY(F)V

    .line 299
    iget-boolean v8, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v8, :cond_e6

    .line 300
    if-nez v0, :cond_d1

    const/4 v8, 0x0

    cmpg-float v8, v7, v8

    if-gez v8, :cond_d1

    .line 301
    const/4 v2, 0x1

    move v11, v4

    move v4, v1

    move v1, v11

    move v12, v5

    move v5, v2

    move v2, v12

    .line 346
    :goto_a7
    invoke-virtual {v6, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 347
    invoke-virtual {v6, v2}, Landroid/view/View;->setScaleX(F)V

    .line 348
    invoke-virtual {v6, v2}, Landroid/view/View;->setScaleY(F)V

    .line 349
    invoke-virtual {v6, v3}, Landroid/view/View;->setAlpha(F)V

    .line 354
    const v1, 0x3caaaaab

    cmpg-float v1, v3, v1

    if-gez v1, :cond_112

    .line 355
    const/4 v1, 0x4

    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    move v1, v4

    move v2, v5

    .line 274
    :cond_c0
    :goto_c0
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    .line 286
    :cond_c4
    const/high16 v3, 0x3f80

    goto :goto_73

    .line 290
    :cond_c7
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->aE:Landroid/view/animation/DecelerateInterpolator;

    const/high16 v8, 0x3f80

    sub-float/2addr v8, v7

    invoke-virtual {v3, v8}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v3

    goto :goto_73

    .line 302
    :cond_d1
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v0, v8, :cond_19b

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-lez v7, :cond_19b

    .line 303
    const/4 v1, 0x1

    move v11, v4

    move v4, v1

    move v1, v11

    move v12, v5

    move v5, v2

    move v2, v12

    goto :goto_a7

    .line 306
    :cond_e6
    if-nez v0, :cond_f9

    const/4 v8, 0x0

    cmpg-float v8, v7, v8

    if-gez v8, :cond_f9

    .line 307
    const/4 v3, 0x0

    .line 308
    const/high16 v4, 0x3f80

    .line 309
    const/high16 v5, 0x3f80

    move v11, v3

    move v3, v5

    move v5, v2

    move v2, v4

    move v4, v1

    move v1, v11

    goto :goto_a7

    .line 310
    :cond_f9
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v0, v8, :cond_19b

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-lez v7, :cond_19b

    .line 311
    const/4 v3, 0x0

    .line 312
    const/high16 v4, 0x3f80

    .line 313
    const/high16 v5, 0x3f80

    move v11, v3

    move v3, v5

    move v5, v2

    move v2, v4

    move v4, v1

    move v1, v11

    goto :goto_a7

    .line 356
    :cond_112
    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_11c

    .line 357
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_11c
    move v1, v4

    move v2, v5

    goto :goto_c0

    .line 368
    :cond_11f
    if-eqz v1, :cond_196

    .line 369
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    .line 370
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->n(I)F

    move-result v0

    .line 371
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->ax:Lcom/android/launcher2/ij;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/launcher2/ij;->getInterpolation(F)F

    move-result v2

    .line 372
    const/high16 v3, 0x3f80

    sub-float/2addr v3, v2

    sget v4, Lcom/android/launcher2/PagedView;->aA:F

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    .line 373
    const/4 v3, 0x0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    .line 376
    const/4 v4, 0x0

    cmpg-float v4, v0, v4

    if-gez v4, :cond_17f

    .line 377
    const/4 v4, 0x0

    cmpg-float v4, v0, v4

    if-gez v4, :cond_17c

    iget-object v4, p0, Lcom/android/launcher2/PagedView;->aD:Landroid/view/animation/AccelerateInterpolator;

    const/high16 v5, 0x3f80

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float v0, v5, v0

    invoke-virtual {v4, v0}, Landroid/view/animation/AccelerateInterpolator;->getInterpolation(F)F

    move-result v0

    .line 383
    :goto_163
    invoke-virtual {v1, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 384
    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    .line 385
    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleY(F)V

    .line 386
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 388
    const v2, 0x3caaaaab

    cmpg-float v0, v0, v2

    if-gez v0, :cond_18a

    .line 389
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2c

    .line 377
    :cond_17c
    const/high16 v0, 0x3f80

    goto :goto_163

    .line 381
    :cond_17f
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->aE:Landroid/view/animation/DecelerateInterpolator;

    const/high16 v5, 0x3f80

    sub-float v0, v5, v0

    invoke-virtual {v4, v0}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_163

    .line 390
    :cond_18a
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2c

    .line 391
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2c

    .line 394
    :cond_196
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->f(I)V

    goto/16 :goto_2c

    :cond_19b
    move v11, v4

    move v4, v1

    move v1, v11

    move v12, v5

    move v5, v2

    move v2, v12

    goto/16 :goto_a7
.end method

.method protected final m(I)F
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    const/4 v3, 0x0

    .line 399
    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    .line 401
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_34

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    :goto_f
    div-int/lit8 v0, v0, 0x2

    .line 403
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v1

    iget v2, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v1, v2

    .line 404
    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v2

    .line 405
    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v0, v2

    sub-int/2addr v0, v1

    .line 404
    sub-int v0, p1, v0

    .line 407
    int-to-float v0, v0

    int-to-float v1, v1

    mul-float/2addr v1, v4

    div-float/2addr v0, v1

    .line 409
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 410
    const/high16 v1, -0x4080

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 411
    return v0

    .line 401
    :cond_34
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_f
.end method

.method protected final n(I)F
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x3f80

    .line 415
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 416
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v2

    .line 417
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_3a

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v0

    :goto_14
    div-int/lit8 v0, v0, 0x2

    .line 419
    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v2

    iget v3, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v2, v3

    .line 420
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v3

    .line 421
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    sub-int v1, v3, v1

    add-int/2addr v0, v1

    add-int/2addr v0, v2

    .line 420
    sub-int v0, p1, v0

    .line 423
    int-to-float v0, v0

    int-to-float v1, v2

    mul-float/2addr v1, v4

    div-float/2addr v0, v1

    .line 424
    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 425
    const/high16 v1, -0x4080

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 426
    return v0

    .line 417
    :cond_3a
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    goto :goto_14
.end method

.method protected final n()V
    .registers 5

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ar:Z

    if-eqz v0, :cond_18

    .line 262
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/view/animation/OvershootInterpolator;

    const v3, 0x3fd9999a

    invoke-direct {v2, v3}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    invoke-direct {v0, v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    .line 266
    :goto_17
    return-void

    .line 264
    :cond_18
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/android/launcher2/if;

    invoke-direct {v2}, Lcom/android/launcher2/if;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    goto :goto_17
.end method

.method public final o()V
    .registers 3

    .prologue
    .line 446
    const-string v0, "NONE"

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->au:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 447
    const/4 v0, 0x0

    :goto_b
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    if-lt v0, v1, :cond_12

    .line 451
    :cond_11
    return-void

    .line 448
    :cond_12
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    .line 447
    add-int/lit8 v0, v0, 0x1

    goto :goto_b
.end method

.method o(I)V
    .registers 3
    .parameter

    .prologue
    .line 479
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->setCurrentPage(I)V

    .line 480
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 481
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1058
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->D:Z

    .line 1059
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 1060
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->c()V

    .line 1061
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1065
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter

    .prologue
    const/16 v1, 0x9

    const/4 v2, 0x0

    .line 1917
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_12

    .line 1918
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_4a

    .line 1941
    :cond_12
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_16
    return v0

    .line 1923
    :pswitch_17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_39

    .line 1925
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    move v1, v2

    .line 1930
    :goto_24
    cmpl-float v3, v0, v2

    if-nez v3, :cond_2c

    cmpl-float v3, v1, v2

    if-eqz v3, :cond_12

    .line 1931
    :cond_2c
    cmpl-float v0, v0, v2

    if-gtz v0, :cond_34

    cmpl-float v0, v1, v2

    if-lez v0, :cond_45

    .line 1932
    :cond_34
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->d_()V

    .line 1936
    :goto_37
    const/4 v0, 0x1

    goto :goto_16

    .line 1927
    :cond_39
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    neg-float v1, v0

    .line 1928
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    goto :goto_24

    .line 1934
    :cond_45
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->a_()V

    goto :goto_37

    .line 1918
    nop

    :pswitch_data_4a
    .packed-switch 0x8
        :pswitch_17
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 2574
    const/4 v0, 0x1

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter

    .prologue
    .line 2536
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 2537
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    .line 2538
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_20

    .line 2539
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 2540
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 2541
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 2543
    :cond_20
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v7, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1453
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->b(Landroid/view/MotionEvent;)V

    .line 1456
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_12

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ao:Z

    if-nez v0, :cond_17

    :cond_12
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1547
    :cond_16
    :goto_16
    return v2

    .line 1463
    :cond_17
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1464
    if-ne v0, v6, :cond_2b

    .line 1465
    iget v3, p0, Lcom/android/launcher2/PagedView;->C:I

    if-eq v3, v2, :cond_16

    .line 1466
    iget v3, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_16

    .line 1467
    iget v3, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v4, 0x5

    if-eq v3, v4, :cond_16

    .line 1471
    :cond_2b
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_98

    .line 1547
    :cond_30
    :goto_30
    :pswitch_30
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    if-nez v0, :cond_16

    move v2, v1

    goto :goto_16

    .line 1477
    :pswitch_36
    iget v0, p0, Lcom/android/launcher2/PagedView;->Z:I

    if-eq v0, v7, :cond_3e

    .line 1478
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->a(Landroid/view/MotionEvent;)V

    goto :goto_30

    .line 1489
    :cond_3e
    :pswitch_3e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1490
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 1492
    iput v3, p0, Lcom/android/launcher2/PagedView;->b:F

    .line 1493
    iput v3, p0, Lcom/android/launcher2/PagedView;->y:F

    .line 1494
    iput v4, p0, Lcom/android/launcher2/PagedView;->A:F

    .line 1495
    iput v5, p0, Lcom/android/launcher2/PagedView;->z:F

    .line 1496
    iput v5, p0, Lcom/android/launcher2/PagedView;->B:F

    .line 1497
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1498
    iput-boolean v2, p0, Lcom/android/launcher2/PagedView;->F:Z

    .line 1513
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_7b

    move v0, v1

    :goto_61
    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 1517
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    if-eq v0, v6, :cond_30

    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v5, 0x3

    if-eq v0, v5, :cond_30

    .line 1518
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_30

    .line 1519
    invoke-virtual {p0, v3, v4}, Lcom/android/launcher2/PagedView;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 1520
    iput v6, p0, Lcom/android/launcher2/PagedView;->C:I

    goto :goto_30

    :cond_7b
    move v0, v2

    .line 1513
    goto :goto_61

    .line 1521
    :cond_7d
    invoke-virtual {p0, v3, v4}, Lcom/android/launcher2/PagedView;->b(FF)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 1522
    const/4 v0, 0x3

    iput v0, p0, Lcom/android/launcher2/PagedView;->C:I

    goto :goto_30

    .line 1531
    :pswitch_87
    iput v1, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 1532
    iput-boolean v1, p0, Lcom/android/launcher2/PagedView;->F:Z

    .line 1533
    iput v7, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1534
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->d()V

    goto :goto_30

    .line 1538
    :pswitch_91
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->c(Landroid/view/MotionEvent;)V

    .line 1539
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->d()V

    goto :goto_30

    .line 1471
    :pswitch_data_98
    .packed-switch 0x0
        :pswitch_3e
        :pswitch_87
        :pswitch_36
        :pswitch_87
        :pswitch_30
        :pswitch_30
        :pswitch_91
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 980
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ag:Z

    if-nez v0, :cond_5

    .line 1033
    :cond_4
    :goto_4
    return-void

    .line 985
    :cond_5
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingBottom()I

    move-result v1

    add-int v4, v0, v1

    .line 986
    iget v0, p0, Lcom/android/launcher2/PagedView;->mPaddingLeft:I

    iget v1, p0, Lcom/android/launcher2/PagedView;->mPaddingRight:I

    add-int v5, v0, v1

    .line 987
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v6

    .line 988
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v2

    .line 989
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    .line 990
    const/4 v0, 0x0

    move v3, v0

    :goto_25
    if-lt v3, v6, :cond_4a

    .line 1021
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->t:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    if-ltz v0, :cond_4

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 1022
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_ae

    .line 1023
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setVerticalScrollBarEnabled(Z)V

    .line 1024
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->s()V

    .line 1025
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setVerticalScrollBarEnabled(Z)V

    .line 1031
    :goto_46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->t:Z

    goto :goto_4

    .line 991
    :cond_4a
    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v7

    .line 992
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v8, 0x8

    if-eq v0, v8, :cond_ba

    .line 993
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_87

    .line 994
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 995
    invoke-virtual {p0, v7}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v8

    .line 996
    iget v0, p0, Lcom/android/launcher2/PagedView;->mPaddingLeft:I

    .line 997
    iget-boolean v9, p0, Lcom/android/launcher2/PagedView;->Q:Z

    if-eqz v9, :cond_71

    .line 998
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v9

    sub-int/2addr v9, v5

    sub-int/2addr v9, v2

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v0, v9

    .line 1003
    :cond_71
    add-int/2addr v2, v0

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v1

    .line 1002
    invoke-virtual {v7, v0, v1, v2, v9}, Landroid/view/View;->layout(IIII)V

    .line 1004
    iget v2, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v2, v8

    add-int/2addr v1, v2

    move v10, v1

    move v1, v0

    move v0, v10

    .line 990
    :goto_81
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_25

    .line 1006
    :cond_87
    invoke-virtual {p0, v7}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v1

    .line 1007
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 1008
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v0

    .line 1009
    iget-boolean v9, p0, Lcom/android/launcher2/PagedView;->Q:Z

    if-eqz v9, :cond_a0

    .line 1010
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v9

    sub-int/2addr v9, v4

    sub-int/2addr v9, v8

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v0, v9

    .line 1015
    :cond_a0
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v2

    add-int/2addr v8, v0

    .line 1014
    invoke-virtual {v7, v2, v0, v9, v8}, Landroid/view/View;->layout(IIII)V

    .line 1016
    iget v7, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v1, v7

    add-int/2addr v1, v2

    goto :goto_81

    .line 1027
    :cond_ae
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setHorizontalScrollBarEnabled(Z)V

    .line 1028
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->s()V

    .line 1029
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setHorizontalScrollBarEnabled(Z)V

    goto :goto_46

    :cond_ba
    move v0, v1

    move v1, v2

    goto :goto_81
.end method

.method protected onMeasure(II)V
    .registers 17
    .parameter
    .parameter

    .prologue
    .line 816
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ag:Z

    if-nez v0, :cond_8

    .line 817
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 922
    :goto_7
    return-void

    .line 821
    :cond_8
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 822
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 823
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v8

    .line 824
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 825
    const/high16 v0, 0x4000

    if-eq v7, v0, :cond_24

    .line 826
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Workspace can only be used in EXACTLY mode."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 830
    :cond_24
    if-lez v3, :cond_28

    if-gtz v2, :cond_2c

    .line 831
    :cond_28
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    goto :goto_7

    .line 840
    :cond_2c
    const/4 v4, 0x0

    .line 841
    const/4 v1, 0x0

    .line 843
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingBottom()I

    move-result v5

    add-int v9, v0, v5

    .line 844
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingRight()I

    move-result v5

    add-int v10, v0, v5

    .line 849
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v11

    .line 850
    const/4 v0, 0x0

    move v5, v1

    move v6, v4

    move v4, v0

    :goto_4a
    if-lt v4, v11, :cond_92

    .line 881
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_d3

    .line 882
    const/high16 v0, -0x8000

    if-ne v7, v0, :cond_e1

    .line 883
    add-int v0, v5, v10

    move v1, v0

    move v0, v2

    .line 891
    :goto_58
    invoke-virtual {p0, v1, v0}, Lcom/android/launcher2/PagedView;->setMeasuredDimension(II)V

    .line 896
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->c()V

    .line 898
    if-lez v11, :cond_7c

    .line 903
    iget v0, p0, Lcom/android/launcher2/PagedView;->H:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_7c

    .line 908
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v0

    .line 909
    sub-int/2addr v1, v0

    .line 910
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    .line 909
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 911
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->setPageSpacing(I)V

    .line 915
    :cond_7c
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->i()V

    .line 917
    if-lez v11, :cond_dc

    .line 918
    add-int/lit8 v0, v11, -0x1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v0

    add-int/lit8 v1, v11, -0x1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->w:I

    goto/16 :goto_7

    .line 852
    :cond_92
    invoke-virtual {p0, v4}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v12

    .line 853
    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 856
    iget v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v13, -0x2

    if-ne v0, v13, :cond_cd

    .line 857
    const/high16 v0, -0x8000

    .line 863
    :goto_a1
    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v13, -0x2

    if-ne v1, v13, :cond_d0

    .line 864
    const/high16 v1, -0x8000

    .line 870
    :goto_a8
    sub-int v13, v3, v10

    invoke-static {v13, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 872
    sub-int v13, v2, v9

    invoke-static {v13, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 874
    invoke-virtual {v12, v0, v1}, Landroid/view/View;->measure(II)V

    .line 875
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 876
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 850
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v1

    goto/16 :goto_4a

    .line 859
    :cond_cd
    const/high16 v0, 0x4000

    goto :goto_a1

    .line 866
    :cond_d0
    const/high16 v1, 0x4000

    goto :goto_a8

    .line 886
    :cond_d3
    const/high16 v0, -0x8000

    if-ne v8, v0, :cond_e1

    .line 887
    add-int v0, v6, v9

    move v1, v3

    goto/16 :goto_58

    .line 920
    :cond_dc
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/PagedView;->w:I

    goto/16 :goto_7

    :cond_e1
    move v0, v2

    move v1, v3

    goto/16 :goto_58
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1346
    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_13

    .line 1347
    iget v0, p0, Lcom/android/launcher2/PagedView;->v:I

    .line 1351
    :goto_8
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1352
    if-eqz v0, :cond_16

    .line 1353
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 1355
    :goto_12
    return v0

    .line 1349
    :cond_13
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    goto :goto_8

    .line 1355
    :cond_16
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v5, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1748
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ao:Z

    if-nez v0, :cond_13

    :cond_e
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 1912
    :cond_12
    :goto_12
    return v2

    .line 1750
    :cond_13
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->b(Landroid/view/MotionEvent;)V

    .line 1752
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1754
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_246

    :pswitch_1f
    goto :goto_12

    .line 1760
    :pswitch_20
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2d

    .line 1761
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 1765
    :cond_2d
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->y:F

    iput v0, p0, Lcom/android/launcher2/PagedView;->b:F

    .line 1766
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->A:F

    iput v0, p0, Lcom/android/launcher2/PagedView;->c:F

    .line 1767
    iput v1, p0, Lcom/android/launcher2/PagedView;->z:F

    .line 1768
    iput v1, p0, Lcom/android/launcher2/PagedView;->B:F

    .line 1769
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1770
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    if-ne v0, v2, :cond_12

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_12

    .line 1771
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->u()V

    goto :goto_12

    .line 1776
    :pswitch_55
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    if-ne v0, v2, :cond_cf

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_cf

    .line 1778
    iget v0, p0, Lcom/android/launcher2/PagedView;->Z:I

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1779
    if-eq v0, v5, :cond_12

    .line 1780
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_b6

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 1783
    :goto_6f
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_bb

    iget v1, p0, Lcom/android/launcher2/PagedView;->A:F

    :goto_75
    iget v4, p0, Lcom/android/launcher2/PagedView;->z:F

    add-float/2addr v1, v4

    sub-float/2addr v1, v0

    .line 1785
    iget v4, p0, Lcom/android/launcher2/PagedView;->B:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    add-float/2addr v4, v5

    iput v4, p0, Lcom/android/launcher2/PagedView;->B:F

    .line 1790
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x3f80

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_ca

    .line 1791
    iget v4, p0, Lcom/android/launcher2/PagedView;->s:F

    add-float/2addr v4, v1

    iput v4, p0, Lcom/android/launcher2/PagedView;->s:F

    .line 1792
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    long-to-float v4, v4

    const v5, 0x4e6e6b28

    div-float/2addr v4, v5

    iput v4, p0, Lcom/android/launcher2/PagedView;->r:F

    .line 1793
    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->ae:Z

    if-nez v4, :cond_c3

    .line 1794
    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v4, :cond_be

    .line 1795
    float-to-int v4, v1

    invoke-virtual {p0, v3, v4}, Lcom/android/launcher2/PagedView;->scrollBy(II)V

    .line 1803
    :goto_a8
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v3, :cond_c7

    .line 1804
    iput v0, p0, Lcom/android/launcher2/PagedView;->A:F

    .line 1808
    :goto_ae
    float-to-int v0, v1

    int-to-float v0, v0

    sub-float v0, v1, v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->z:F

    goto/16 :goto_12

    .line 1780
    :cond_b6
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto :goto_6f

    .line 1783
    :cond_bb
    iget v1, p0, Lcom/android/launcher2/PagedView;->y:F

    goto :goto_75

    .line 1797
    :cond_be
    float-to-int v4, v1

    invoke-virtual {p0, v4, v3}, Lcom/android/launcher2/PagedView;->scrollBy(II)V

    goto :goto_a8

    .line 1801
    :cond_c3
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_a8

    .line 1806
    :cond_c7
    iput v0, p0, Lcom/android/launcher2/PagedView;->y:F

    goto :goto_ae

    .line 1810
    :cond_ca
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->awakenScrollBars()Z

    goto/16 :goto_12

    .line 1813
    :cond_cf
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_12

    .line 1814
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_12

    .line 1815
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_12

    .line 1816
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_12

    .line 1822
    :pswitch_e4
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    if-ne v0, v2, :cond_1d4

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_1d4

    .line 1823
    iget v1, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1824
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    .line 1825
    if-eq v0, v5, :cond_12

    .line 1826
    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v4, :cond_18a

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    .line 1829
    :goto_fe
    iget-object v4, p0, Lcom/android/launcher2/PagedView;->a:Lcom/anddoes/launcher/av;

    .line 1830
    iget v6, p0, Lcom/android/launcher2/PagedView;->i:I

    int-to-float v6, v6

    invoke-virtual {v4, v6}, Lcom/anddoes/launcher/av;->a(F)V

    .line 1832
    iget-boolean v6, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v6, :cond_190

    .line 1833
    invoke-virtual {v4, v1}, Lcom/anddoes/launcher/av;->b(I)F

    move-result v1

    .line 1832
    :goto_10e
    float-to-int v7, v1

    .line 1835
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_196

    iget v1, p0, Lcom/android/launcher2/PagedView;->c:F

    :goto_115
    sub-float v1, v0, v1

    float-to-int v8, v1

    .line 1836
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v1

    .line 1837
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    int-to-float v1, v1

    .line 1838
    const v6, 0x3e99999a

    mul-float/2addr v1, v6

    cmpl-float v1, v4, v1

    if-lez v1, :cond_19a

    move v1, v2

    .line 1840
    :goto_131
    iget v6, p0, Lcom/android/launcher2/PagedView;->B:F

    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v4, :cond_19c

    iget v4, p0, Lcom/android/launcher2/PagedView;->A:F

    :goto_139
    iget v9, p0, Lcom/android/launcher2/PagedView;->z:F

    add-float/2addr v4, v9

    sub-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    add-float/2addr v0, v6

    iput v0, p0, Lcom/android/launcher2/PagedView;->B:F

    .line 1842
    iget v0, p0, Lcom/android/launcher2/PagedView;->B:F

    const/high16 v4, 0x41c8

    cmpl-float v0, v0, v4

    if-lez v0, :cond_19f

    .line 1843
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v4, p0, Lcom/android/launcher2/PagedView;->p:I

    if-le v0, v4, :cond_19f

    move v0, v2

    .line 1850
    :goto_156
    int-to-float v4, v7

    invoke-static {v4}, Ljava/lang/Math;->signum(F)F

    move-result v4

    int-to-float v6, v8

    invoke-static {v6}, Ljava/lang/Math;->signum(F)F

    move-result v6

    cmpl-float v4, v4, v6

    if-eqz v4, :cond_243

    if-eqz v0, :cond_243

    move v6, v2

    .line 1858
    :goto_167
    if-eqz v1, :cond_16d

    if-lez v8, :cond_16d

    if-eqz v0, :cond_171

    .line 1859
    :cond_16d
    if-eqz v0, :cond_1a8

    if-lez v7, :cond_1a8

    :cond_171
    iget v9, p0, Lcom/android/launcher2/PagedView;->u:I

    iget-boolean v4, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v4, :cond_1a1

    move v4, v5

    :goto_178
    if-le v9, v4, :cond_1a8

    .line 1860
    if-eqz v6, :cond_1a3

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 1861
    :goto_17e
    invoke-virtual {p0, v0, v7}, Lcom/android/launcher2/PagedView;->b(II)V

    .line 1891
    :cond_181
    :goto_181
    iput v3, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 1894
    iput v5, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1895
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->d()V

    goto/16 :goto_12

    .line 1826
    :cond_18a
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    goto/16 :goto_fe

    .line 1834
    :cond_190
    invoke-virtual {v4, v1}, Lcom/anddoes/launcher/av;->a(I)F

    move-result v1

    goto/16 :goto_10e

    .line 1835
    :cond_196
    iget v1, p0, Lcom/android/launcher2/PagedView;->b:F

    goto/16 :goto_115

    :cond_19a
    move v1, v3

    .line 1837
    goto :goto_131

    .line 1840
    :cond_19c
    iget v4, p0, Lcom/android/launcher2/PagedView;->y:F

    goto :goto_139

    :cond_19f
    move v0, v3

    .line 1843
    goto :goto_156

    :cond_1a1
    move v4, v3

    .line 1859
    goto :goto_178

    .line 1860
    :cond_1a3
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_17e

    .line 1862
    :cond_1a8
    if-eqz v1, :cond_1ae

    if-gez v8, :cond_1ae

    if-eqz v0, :cond_1b2

    .line 1863
    :cond_1ae
    if-eqz v0, :cond_1d0

    if-gez v7, :cond_1d0

    .line 1864
    :cond_1b2
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v4

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_1c9

    move v0, v3

    :goto_1bd
    sub-int v0, v4, v0

    if-ge v1, v0, :cond_1d0

    .line 1865
    if-eqz v6, :cond_1cb

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 1866
    :goto_1c5
    invoke-virtual {p0, v0, v7}, Lcom/android/launcher2/PagedView;->b(II)V

    goto :goto_181

    :cond_1c9
    move v0, v2

    .line 1864
    goto :goto_1bd

    .line 1865
    :cond_1cb
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1c5

    .line 1868
    :cond_1d0
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->y()V

    goto :goto_181

    .line 1870
    :cond_1d4
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1fa

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_1fa

    .line 1874
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_1f4

    move v0, v5

    :goto_1e4
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1875
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    if-eq v0, v1, :cond_1f6

    .line 1876
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    goto :goto_181

    :cond_1f4
    move v0, v3

    .line 1874
    goto :goto_1e4

    .line 1878
    :cond_1f6
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->y()V

    goto :goto_181

    .line 1880
    :cond_1fa
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_181

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_181

    .line 1884
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_221

    move v0, v3

    :goto_20e
    sub-int v0, v1, v0

    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1885
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    if-eq v0, v1, :cond_223

    .line 1886
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    goto/16 :goto_181

    :cond_221
    move v0, v2

    .line 1884
    goto :goto_20e

    .line 1888
    :cond_223
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->y()V

    goto/16 :goto_181

    .line 1899
    :pswitch_228
    iget v0, p0, Lcom/android/launcher2/PagedView;->C:I

    if-ne v0, v2, :cond_235

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-le v0, v2, :cond_235

    .line 1900
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->y()V

    .line 1902
    :cond_235
    iput v3, p0, Lcom/android/launcher2/PagedView;->C:I

    .line 1903
    iput v5, p0, Lcom/android/launcher2/PagedView;->Z:I

    .line 1904
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->d()V

    goto/16 :goto_12

    .line 1908
    :pswitch_23e
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->c(Landroid/view/MotionEvent;)V

    goto/16 :goto_12

    :cond_243
    move v6, v3

    goto/16 :goto_167

    .line 1754
    :pswitch_data_246
    .packed-switch 0x0
        :pswitch_20
        :pswitch_e4
        :pswitch_55
        :pswitch_228
        :pswitch_1f
        :pswitch_1f
        :pswitch_23e
    .end packed-switch
.end method

.method protected final p(I)I
    .registers 9
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1087
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->g(I)I

    move-result v4

    .line 1088
    iget v1, p0, Lcom/android/launcher2/PagedView;->W:F

    const/high16 v2, 0x3f80

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_1c

    .line 1089
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->e:[I

    move-object v3, v1

    .line 1091
    :goto_12
    if-eqz v3, :cond_20

    aget v1, v3, v4

    const/4 v2, -0x1

    if-eq v1, v2, :cond_20

    .line 1092
    aget v0, v3, v4

    .line 1104
    :cond_1b
    :goto_1b
    return v0

    .line 1089
    :cond_1c
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->g:[I

    move-object v3, v1

    goto :goto_12

    .line 1094
    :cond_20
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_1b

    .line 1097
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    move v6, v0

    move v0, v1

    move v1, v6

    .line 1098
    :goto_2d
    if-lt v1, v4, :cond_34

    .line 1101
    if-eqz v3, :cond_1b

    .line 1102
    aput v0, v3, v4

    goto :goto_1b

    .line 1099
    :cond_34
    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->d(Landroid/view/View;)I

    move-result v2

    iget v5, p0, Lcom/android/launcher2/PagedView;->H:I

    add-int/2addr v2, v5

    add-int/2addr v2, v0

    .line 1098
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_2d
.end method

.method public final p()V
    .registers 2

    .prologue
    .line 484
    iget v0, p0, Lcom/android/launcher2/PagedView;->al:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->o(I)V

    .line 485
    return-void
.end method

.method protected final q(I)I
    .registers 6
    .parameter

    .prologue
    .line 1109
    invoke-direct {p0, p1}, Lcom/android/launcher2/PagedView;->g(I)I

    move-result v1

    .line 1110
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->f:[I

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/android/launcher2/PagedView;->f:[I

    aget v0, v0, v1

    const/4 v2, -0x1

    if-eq v0, v2, :cond_14

    .line 1111
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->f:[I

    aget v0, v0, v1

    .line 1128
    :cond_13
    :goto_13
    return v0

    .line 1113
    :cond_14
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_36

    .line 1114
    iget v0, p0, Lcom/android/launcher2/PagedView;->mPaddingTop:I

    iget v2, p0, Lcom/android/launcher2/PagedView;->mPaddingBottom:I

    add-int/2addr v0, v2

    .line 1115
    iget v2, p0, Lcom/android/launcher2/PagedView;->mPaddingTop:I

    .line 1116
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v3

    sub-int v0, v3, v0

    invoke-direct {p0, v1}, Lcom/android/launcher2/PagedView;->h(I)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 1115
    add-int/2addr v0, v2

    .line 1117
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->f:[I

    if-eqz v2, :cond_13

    .line 1118
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->f:[I

    aput v0, v2, v1

    goto :goto_13

    .line 1122
    :cond_36
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingRight()I

    move-result v2

    add-int/2addr v0, v2

    .line 1123
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPaddingLeft()I

    move-result v2

    .line 1124
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v3

    sub-int v0, v3, v0

    invoke-direct {p0, v1}, Lcom/android/launcher2/PagedView;->h(I)I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    .line 1123
    add-int/2addr v0, v2

    .line 1125
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->f:[I

    if-eqz v2, :cond_13

    .line 1126
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->f:[I

    aput v0, v2, v1

    goto :goto_13
.end method

.method protected final q()V
    .registers 2

    .prologue
    .line 571
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->ag:Z

    .line 572
    return-void
.end method

.method protected r(I)V
    .registers 4
    .parameter

    .prologue
    const/16 v0, 0x1c2

    .line 2103
    .line 2104
    iget v1, p0, Lcom/android/launcher2/PagedView;->at:I

    if-le v0, v1, :cond_c

    iget v1, p0, Lcom/android/launcher2/PagedView;->at:I

    if-lez v1, :cond_c

    .line 2105
    iget v0, p0, Lcom/android/launcher2/PagedView;->at:I

    .line 2107
    :cond_c
    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/PagedView;->c(II)V

    .line 2108
    return-void
.end method

.method protected final r()Z
    .registers 2

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ag:Z

    return v0
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1981
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1982
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b(I)I

    move-result v0

    .line 1983
    if-ltz v0, :cond_1c

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPage()I

    move-result v1

    if-eq v0, v1, :cond_1c

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_1c

    .line 1984
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 1986
    :cond_1c
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1335
    invoke-virtual {p0, p1}, Lcom/android/launcher2/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b(I)I

    move-result v0

    .line 1336
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    if-ne v0, v1, :cond_14

    iget-object v1, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_19

    .line 1337
    :cond_14
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->r(I)V

    .line 1338
    const/4 v0, 0x1

    .line 1340
    :goto_18
    return v0

    :cond_19
    const/4 v0, 0x0

    goto :goto_18
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .registers 3
    .parameter

    .prologue
    .line 1423
    if-eqz p1, :cond_b

    .line 1426
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1427
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 1429
    :cond_b
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 1430
    return-void
.end method

.method protected s()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 607
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v0

    .line 608
    iget v1, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v1}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v1

    .line 609
    sub-int/2addr v0, v1

    .line 610
    iget-boolean v1, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v1, :cond_21

    .line 611
    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 612
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->setFinalY(I)V

    .line 617
    :goto_1a
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 618
    return-void

    .line 614
    :cond_21
    invoke-virtual {p0, v0, v2}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 615
    iget-object v1, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->setFinalX(I)V

    goto :goto_1a
.end method

.method protected final s(I)V
    .registers 3
    .parameter

    .prologue
    .line 2237
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/launcher2/PagedView;->b(IZ)V

    .line 2238
    return-void
.end method

.method public scrollBy(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 688
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_e

    .line 689
    iget v0, p0, Lcom/android/launcher2/PagedView;->mScrollX:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/android/launcher2/PagedView;->S:I

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 693
    :goto_d
    return-void

    .line 691
    :cond_e
    iget v0, p0, Lcom/android/launcher2/PagedView;->S:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/android/launcher2/PagedView;->mScrollY:I

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    goto :goto_d
.end method

.method public scrollTo(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 697
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_43

    .line 698
    iput p2, p0, Lcom/android/launcher2/PagedView;->S:I

    .line 699
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-nez v0, :cond_3d

    .line 700
    if-gez p2, :cond_27

    .line 704
    invoke-super {p0, p1, v1}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 705
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->R:Z

    if-eqz v0, :cond_18

    .line 706
    int-to-float v0, p2

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b(F)V

    .line 718
    :cond_18
    :goto_18
    int-to-float v0, p2

    iput v0, p0, Lcom/android/launcher2/PagedView;->s:F

    .line 744
    :goto_1b
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x4e6e6b28

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/launcher2/PagedView;->r:F

    .line 745
    return-void

    .line 708
    :cond_27
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    if-le p2, v0, :cond_3d

    .line 709
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    invoke-super {p0, p1, v0}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 710
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->R:Z

    if-eqz v0, :cond_18

    .line 711
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    sub-int v0, p2, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b(F)V

    goto :goto_18

    .line 714
    :cond_3d
    iput p2, p0, Lcom/android/launcher2/PagedView;->V:I

    .line 715
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_18

    .line 720
    :cond_43
    iput p1, p0, Lcom/android/launcher2/PagedView;->S:I

    .line 722
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-nez v0, :cond_70

    .line 723
    if-gez p1, :cond_5a

    .line 727
    invoke-super {p0, v1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 728
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->R:Z

    if-eqz v0, :cond_56

    .line 729
    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b(F)V

    .line 741
    :cond_56
    :goto_56
    int-to-float v0, p1

    iput v0, p0, Lcom/android/launcher2/PagedView;->s:F

    goto :goto_1b

    .line 731
    :cond_5a
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    if-le p1, v0, :cond_70

    .line 732
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    invoke-super {p0, v0, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 733
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->R:Z

    if-eqz v0, :cond_56

    .line 734
    iget v0, p0, Lcom/android/launcher2/PagedView;->w:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->b(F)V

    goto :goto_56

    .line 737
    :cond_70
    iput p1, p0, Lcom/android/launcher2/PagedView;->V:I

    .line 738
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_56
.end method

.method public setAllowLongPress(Z)V
    .registers 2
    .parameter

    .prologue
    .line 2203
    iput-boolean p1, p0, Lcom/android/launcher2/PagedView;->F:Z

    .line 2204
    return-void
.end method

.method setCurrentPage(I)V
    .registers 4
    .parameter

    .prologue
    .line 624
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_d

    .line 625
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 629
    :cond_d
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_14

    .line 638
    :goto_13
    return-void

    .line 633
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 634
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->s()V

    .line 635
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->e()V

    .line 636
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->t()V

    .line 637
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_13
.end method

.method public setElasticScrolling(Z)V
    .registers 3
    .parameter

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ar:Z

    if-eq v0, p1, :cond_9

    .line 255
    iput-boolean p1, p0, Lcom/android/launcher2/PagedView;->ar:Z

    .line 256
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->n()V

    .line 258
    :cond_9
    return-void
.end method

.method public setLayoutScale(F)V
    .registers 10
    .parameter

    .prologue
    const/high16 v7, 0x4000

    const/4 v1, 0x0

    .line 944
    iput p1, p0, Lcom/android/launcher2/PagedView;->W:F

    .line 945
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->c()V

    .line 948
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    .line 949
    new-array v3, v2, [F

    .line 950
    new-array v4, v2, [F

    move v0, v1

    .line 951
    :goto_11
    if-lt v0, v2, :cond_5d

    .line 957
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 958
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getMeasuredHeight()I

    move-result v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 959
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->requestLayout()V

    .line 960
    invoke-virtual {p0, v0, v5}, Lcom/android/launcher2/PagedView;->measure(II)V

    .line 961
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getBottom()I

    move-result v7

    invoke-virtual {p0, v0, v5, v6, v7}, Lcom/android/launcher2/PagedView;->layout(IIII)V

    move v0, v1

    .line 962
    :goto_3d
    if-lt v0, v2, :cond_70

    .line 970
    iget v2, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v0

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v3

    sub-int v3, v0, v3

    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v0, :cond_81

    iget v0, p0, Lcom/android/launcher2/PagedView;->mScrollY:I

    :goto_51
    sub-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v4

    move v0, v1

    :goto_57
    if-lt v0, v4, :cond_84

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->setCurrentPage(I)V

    .line 971
    return-void

    .line 952
    :cond_5d
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v5

    .line 953
    invoke-virtual {v5}, Landroid/view/View;->getX()F

    move-result v6

    aput v6, v3, v0

    .line 954
    invoke-virtual {v5}, Landroid/view/View;->getY()F

    move-result v5

    aput v5, v4, v0

    .line 951
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 963
    :cond_70
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v5

    .line 964
    aget v6, v3, v0

    invoke-virtual {v5, v6}, Landroid/view/View;->setX(F)V

    .line 965
    aget v6, v4, v0

    invoke-virtual {v5, v6}, Landroid/view/View;->setY(F)V

    .line 962
    add-int/lit8 v0, v0, 0x1

    goto :goto_3d

    .line 970
    :cond_81
    iget v0, p0, Lcom/android/launcher2/PagedView;->mScrollX:I

    goto :goto_51

    :cond_84
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v5, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v5, :cond_98

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v5

    int-to-float v6, v3

    add-float/2addr v5, v6

    invoke-virtual {v1, v5}, Landroid/view/View;->setY(F)V

    :goto_95
    add-int/lit8 v0, v0, 0x1

    goto :goto_57

    :cond_98
    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v5

    int-to-float v6, v3

    add-float/2addr v5, v6

    invoke-virtual {v1, v5}, Landroid/view/View;->setX(F)V

    goto :goto_95
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 679
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->E:Landroid/view/View$OnLongClickListener;

    .line 684
    return-void
.end method

.method public setPageIndicator(Lcom/android/launcher2/hz;)V
    .registers 3
    .parameter

    .prologue
    .line 471
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->aI:Lcom/android/launcher2/hz;

    .line 472
    if-eqz p1, :cond_10

    .line 473
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/android/launcher2/hz;->setPageCount(I)V

    .line 474
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-interface {p1, v0}, Lcom/android/launcher2/hz;->setCurrentPage(I)V

    .line 476
    :cond_10
    return-void
.end method

.method public setPageSpacing(I)V
    .registers 2
    .parameter

    .prologue
    .line 974
    iput p1, p0, Lcom/android/launcher2/PagedView;->H:I

    .line 975
    invoke-direct {p0}, Lcom/android/launcher2/PagedView;->c()V

    .line 976
    return-void
.end method

.method public setPageSwitchListener(Lcom/android/launcher2/ib;)V
    .registers 3
    .parameter

    .prologue
    .line 560
    iput-object p1, p0, Lcom/android/launcher2/PagedView;->k:Lcom/android/launcher2/ib;

    .line 561
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->k:Lcom/android/launcher2/ib;

    if-eqz v0, :cond_f

    .line 562
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->k:Lcom/android/launcher2/ib;

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 564
    :cond_f
    return-void
.end method

.method protected t()V
    .registers 2

    .prologue
    .line 641
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->k:Lcom/android/launcher2/ib;

    if-eqz v0, :cond_d

    .line 642
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->k:Lcom/android/launcher2/ib;

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 644
    :cond_d
    return-void
.end method

.method protected final u()V
    .registers 2

    .prologue
    .line 647
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->af:Z

    if-nez v0, :cond_a

    .line 648
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->af:Z

    .line 649
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->e_()V

    .line 651
    :cond_a
    return-void
.end method

.method protected final v()Z
    .registers 2

    .prologue
    .line 661
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->af:Z

    return v0
.end method

.method protected final w()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    const/16 v4, -0x3e7

    const/4 v0, 0x0

    .line 749
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_42

    .line 751
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollX()I

    move-result v0

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    if-ne v0, v2, :cond_2e

    .line 752
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getScrollY()I

    move-result v0

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    if-ne v0, v2, :cond_2e

    .line 753
    iget v0, p0, Lcom/android/launcher2/PagedView;->V:I

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    if-eq v0, v2, :cond_3d

    .line 754
    :cond_2e
    iget-object v0, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 756
    :cond_3d
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    move v0, v1

    .line 806
    :cond_41
    :goto_41
    return v0

    .line 758
    :cond_42
    iget v2, p0, Lcom/android/launcher2/PagedView;->v:I

    if-eq v2, v4, :cond_41

    .line 759
    iget v2, p0, Lcom/android/launcher2/PagedView;->v:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_c3

    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v2, :cond_c3

    .line 760
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 761
    iget v2, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v2}, Lcom/android/launcher2/PagedView;->p(I)I

    move-result v2

    iget v3, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v3}, Lcom/android/launcher2/PagedView;->q(I)I

    move-result v3

    sub-int/2addr v2, v3

    .line 762
    iget-boolean v3, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v3, :cond_b9

    .line 763
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    invoke-virtual {p0, v3, v2}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 767
    :goto_71
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    .line 780
    :goto_74
    iput v4, p0, Lcom/android/launcher2/PagedView;->v:I

    .line 781
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->t()V

    .line 784
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->aH:Z

    if-eqz v2, :cond_84

    .line 785
    iget v2, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/PagedView;->b(IZ)V

    .line 786
    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->aH:Z

    .line 791
    :cond_84
    iget v2, p0, Lcom/android/launcher2/PagedView;->C:I

    if-nez v2, :cond_91

    .line 792
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->af:Z

    if-eqz v2, :cond_91

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->af:Z

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->g()V

    .line 797
    :cond_91
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "accessibility"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 796
    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 798
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 800
    const/16 v0, 0x1000

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 801
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getCurrentPageDescription()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 802
    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_b7
    move v0, v1

    .line 804
    goto :goto_41

    .line 765
    :cond_b9
    iget-object v3, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    goto :goto_71

    .line 768
    :cond_c3
    iget v2, p0, Lcom/android/launcher2/PagedView;->v:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v3

    if-ne v2, v3, :cond_ec

    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v2, :cond_ec

    .line 769
    iput v0, p0, Lcom/android/launcher2/PagedView;->u:I

    .line 770
    iget-boolean v2, p0, Lcom/android/launcher2/PagedView;->ap:Z

    if-eqz v2, :cond_e2

    .line 771
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    invoke-virtual {p0, v2, v0}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    .line 775
    :goto_de
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->invalidate()V

    goto :goto_74

    .line 773
    :cond_e2
    iget-object v2, p0, Lcom/android/launcher2/PagedView;->x:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/android/launcher2/PagedView;->scrollTo(II)V

    goto :goto_de

    .line 777
    :cond_ec
    iget v2, p0, Lcom/android/launcher2/PagedView;->v:I

    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/android/launcher2/PagedView;->u:I

    goto/16 :goto_74
.end method

.method protected final x()V
    .registers 2

    .prologue
    .line 1636
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->F:Z

    if-eqz v0, :cond_12

    .line 1637
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/PagedView;->F:Z

    .line 1641
    iget v0, p0, Lcom/android/launcher2/PagedView;->u:I

    invoke-virtual {p0, v0}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1642
    if-eqz v0, :cond_12

    .line 1643
    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    .line 1646
    :cond_12
    return-void
.end method

.method protected y()V
    .registers 3

    .prologue
    .line 2036
    invoke-virtual {p0}, Lcom/android/launcher2/PagedView;->getPageNearestToCenterOfScreen()I

    move-result v0

    const/16 v1, 0x1c2

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/PagedView;->c(II)V

    .line 2037
    return-void
.end method

.method public z()Z
    .registers 2

    .prologue
    .line 2195
    iget-boolean v0, p0, Lcom/android/launcher2/PagedView;->F:Z

    return v0
.end method
