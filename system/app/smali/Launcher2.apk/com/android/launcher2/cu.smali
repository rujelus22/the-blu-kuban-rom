.class public final Lcom/android/launcher2/cu;
.super Lcom/android/launcher2/di;
.source "SourceFile"


# instance fields
.field a:Z

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/graphics/Bitmap;

.field d:Ljava/util/ArrayList;

.field e:Ljava/util/ArrayList;


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/android/launcher2/di;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    .line 51
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/launcher2/cu;->i:I

    .line 52
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 117
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_b

    .line 120
    return-void

    .line 118
    :cond_b
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/cv;

    invoke-interface {v0}, Lcom/android/launcher2/cv;->k()V

    .line 117
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2
.end method


# virtual methods
.method final a(Landroid/content/ContentValues;)V
    .registers 4
    .parameter

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/android/launcher2/di;->a(Landroid/content/ContentValues;)V

    .line 98
    const-string v0, "title"

    iget-object v1, p0, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_18

    .line 100
    const-string v0, "icon"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 104
    :goto_17
    return-void

    .line 102
    :cond_18
    iget-object v0, p0, Lcom/android/launcher2/cu;->c:Landroid/graphics/Bitmap;

    invoke-static {p1, v0}, Lcom/android/launcher2/cu;->a(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    goto :goto_17
.end method

.method final a(Lcom/android/launcher2/cv;)V
    .registers 3
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    return-void
.end method

.method public final a(Lcom/android/launcher2/di;)V
    .registers 4
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_13

    .line 72
    invoke-direct {p0}, Lcom/android/launcher2/cu;->b()V

    .line 73
    return-void

    .line 70
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/cv;

    invoke-interface {v0, p1}, Lcom/android/launcher2/cv;->a(Lcom/android/launcher2/di;)V

    .line 69
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter

    .prologue
    .line 89
    iput-object p1, p0, Lcom/android/launcher2/cu;->b:Ljava/lang/CharSequence;

    .line 90
    const/4 v0, 0x0

    move v1, v0

    :goto_4
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_d

    .line 93
    return-void

    .line 91
    :cond_d
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/cv;

    invoke-interface {v0, p1}, Lcom/android/launcher2/cv;->a(Ljava/lang/CharSequence;)V

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4
.end method

.method public final b(Lcom/android/launcher2/di;)V
    .registers 4
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/android/launcher2/cu;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 82
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_13

    .line 85
    invoke-direct {p0}, Lcom/android/launcher2/cu;->b()V

    .line 86
    return-void

    .line 83
    :cond_13
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/cv;

    invoke-interface {v0, p1}, Lcom/android/launcher2/cv;->b(Lcom/android/launcher2/di;)V

    .line 82
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7
.end method

.method final b_()V
    .registers 2

    .prologue
    .line 124
    invoke-super {p0}, Lcom/android/launcher2/di;->b_()V

    .line 125
    iget-object v0, p0, Lcom/android/launcher2/cu;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 126
    return-void
.end method
