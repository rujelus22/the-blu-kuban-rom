.class public final Lcom/android/launcher2/co;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static h:Landroid/graphics/drawable/Drawable;

.field public static i:Landroid/graphics/drawable/Drawable;

.field public static j:I

.field public static k:I


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:F

.field public e:Lcom/android/launcher2/FolderIcon;

.field public f:Landroid/graphics/drawable/Drawable;

.field public g:Landroid/graphics/drawable/Drawable;

.field private l:Lcom/android/launcher2/CellLayout;

.field private m:Landroid/animation/ValueAnimator;

.field private n:Landroid/animation/ValueAnimator;

.field private o:Lcom/android/launcher2/Launcher;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 197
    sput-object v1, Lcom/android/launcher2/co;->h:Landroid/graphics/drawable/Drawable;

    .line 198
    sput-object v1, Lcom/android/launcher2/co;->i:Landroid/graphics/drawable/Drawable;

    .line 199
    sput v0, Lcom/android/launcher2/co;->j:I

    .line 200
    sput v0, Lcom/android/launcher2/co;->k:I

    .line 188
    return-void
.end method

.method public constructor <init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/FolderIcon;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const v4, 0x7f020072

    const v3, 0x7f020070

    const/4 v0, 0x0

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object v0, p0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    .line 195
    iput-object v0, p0, Lcom/android/launcher2/co;->f:Landroid/graphics/drawable/Drawable;

    .line 196
    iput-object v0, p0, Lcom/android/launcher2/co;->g:Landroid/graphics/drawable/Drawable;

    .line 216
    iput-object p1, p0, Lcom/android/launcher2/co;->o:Lcom/android/launcher2/Launcher;

    .line 217
    iput-object p2, p0, Lcom/android/launcher2/co;->e:Lcom/android/launcher2/FolderIcon;

    .line 218
    invoke-virtual {p1}, Lcom/android/launcher2/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 219
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 221
    const-string v2, "folder_icon_outer_holo"

    .line 219
    invoke-virtual {v1, v4, v2}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/co;->f:Landroid/graphics/drawable/Drawable;

    .line 222
    iget-object v1, p1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 224
    const-string v2, "folder_icon_inner_holo"

    .line 222
    invoke-virtual {v1, v3, v2}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/launcher2/co;->g:Landroid/graphics/drawable/Drawable;

    .line 228
    invoke-static {}, Lcom/android/launcher2/FolderIcon;->c()Z

    move-result v1

    if-eqz v1, :cond_68

    .line 229
    const v1, 0x7f0b006a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/launcher2/co;->j:I

    .line 230
    const v1, 0x7f0b006b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/android/launcher2/co;->k:I

    .line 231
    iget-object v0, p1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 233
    const-string v1, "folder_icon_outer_holo"

    .line 231
    invoke-virtual {v0, v4, v1}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/co;->h:Landroid/graphics/drawable/Drawable;

    .line 234
    iget-object v0, p1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 236
    const-string v1, "folder_icon_inner_holo"

    .line 234
    invoke-virtual {v0, v3, v1}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/co;->i:Landroid/graphics/drawable/Drawable;

    .line 237
    iget-object v0, p1, Lcom/android/launcher2/Launcher;->i:Lcom/anddoes/launcher/c/l;

    .line 238
    const v1, 0x7f020073

    .line 239
    const-string v2, "folder_icon_rest"

    .line 237
    invoke-virtual {v0, v1, v2}, Lcom/anddoes/launcher/c/l;->b(ILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Lcom/android/launcher2/FolderIcon;->c:Landroid/graphics/drawable/Drawable;

    .line 240
    invoke-static {}, Lcom/android/launcher2/FolderIcon;->d()V

    .line 242
    :cond_68
    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/co;)Lcom/android/launcher2/CellLayout;
    .registers 2
    .parameter

    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/launcher2/co;->l:Lcom/android/launcher2/CellLayout;

    return-object v0
.end method

.method static synthetic b(Lcom/android/launcher2/co;)Lcom/android/launcher2/Launcher;
    .registers 2
    .parameter

    .prologue
    .line 204
    iget-object v0, p0, Lcom/android/launcher2/co;->o:Lcom/android/launcher2/Launcher;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 208
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 210
    :cond_9
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_12

    .line 211
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 213
    :cond_12
    return-void
.end method

.method public final a(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 322
    iput p1, p0, Lcom/android/launcher2/co;->a:I

    .line 323
    iput p2, p0, Lcom/android/launcher2/co;->b:I

    .line 324
    return-void
.end method

.method public final a(Lcom/android/launcher2/CellLayout;)V
    .registers 2
    .parameter

    .prologue
    .line 327
    iput-object p1, p0, Lcom/android/launcher2/co;->l:Lcom/android/launcher2/CellLayout;

    .line 328
    return-void
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 245
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 246
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 248
    :cond_9
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_38

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    .line 249
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 251
    sget v0, Lcom/android/launcher2/co;->j:I

    .line 252
    iget-object v1, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/android/launcher2/cp;

    invoke-direct {v2, p0, v0}, Lcom/android/launcher2/cp;-><init>(Lcom/android/launcher2/co;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 262
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/cq;

    invoke-direct {v1, p0}, Lcom/android/launcher2/cq;-><init>(Lcom/android/launcher2/co;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 270
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 271
    return-void

    .line 248
    :array_38
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final c()V
    .registers 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_9

    .line 275
    iget-object v0, p0, Lcom/android/launcher2/co;->m:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 277
    :cond_9
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_38

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    .line 278
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 280
    sget v0, Lcom/android/launcher2/co;->j:I

    .line 281
    iget-object v1, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/android/launcher2/cr;

    invoke-direct {v2, p0, v0}, Lcom/android/launcher2/cr;-><init>(Lcom/android/launcher2/co;I)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 291
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/android/launcher2/cs;

    invoke-direct {v1, p0}, Lcom/android/launcher2/cs;-><init>(Lcom/android/launcher2/co;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 311
    iget-object v0, p0, Lcom/android/launcher2/co;->n:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 312
    return-void

    .line 277
    :array_38
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x80t 0x3ft
    .end array-data
.end method

.method public final d()F
    .registers 2

    .prologue
    .line 331
    iget v0, p0, Lcom/android/launcher2/co;->c:F

    return v0
.end method

.method public final e()F
    .registers 2

    .prologue
    .line 335
    iget v0, p0, Lcom/android/launcher2/co;->d:F

    return v0
.end method
