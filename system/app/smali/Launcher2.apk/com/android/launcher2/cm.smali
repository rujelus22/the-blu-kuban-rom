.class final Lcom/android/launcher2/cm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/FolderIcon;

.field private final synthetic b:F

.field private final synthetic c:Lcom/android/launcher2/ct;

.field private final synthetic d:F


# direct methods
.method constructor <init>(Lcom/android/launcher2/FolderIcon;FLcom/android/launcher2/ct;F)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/cm;->a:Lcom/android/launcher2/FolderIcon;

    iput p2, p0, Lcom/android/launcher2/cm;->b:F

    iput-object p3, p0, Lcom/android/launcher2/cm;->c:Lcom/android/launcher2/ct;

    iput p4, p0, Lcom/android/launcher2/cm;->d:F

    .line 638
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 8
    .parameter

    .prologue
    const/high16 v5, 0x3f80

    .line 640
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 642
    iget-object v1, p0, Lcom/android/launcher2/cm;->a:Lcom/android/launcher2/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/FolderIcon;)Lcom/android/launcher2/ct;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/cm;->b:F

    iget-object v3, p0, Lcom/android/launcher2/cm;->c:Lcom/android/launcher2/ct;

    iget v3, v3, Lcom/android/launcher2/ct;->a:F

    iget v4, p0, Lcom/android/launcher2/cm;->b:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lcom/android/launcher2/ct;->a:F

    .line 643
    iget-object v1, p0, Lcom/android/launcher2/cm;->a:Lcom/android/launcher2/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/FolderIcon;)Lcom/android/launcher2/ct;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/cm;->d:F

    iget-object v3, p0, Lcom/android/launcher2/cm;->c:Lcom/android/launcher2/ct;

    iget v3, v3, Lcom/android/launcher2/ct;->b:F

    iget v4, p0, Lcom/android/launcher2/cm;->d:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lcom/android/launcher2/ct;->b:F

    .line 644
    iget-object v1, p0, Lcom/android/launcher2/cm;->a:Lcom/android/launcher2/FolderIcon;

    invoke-static {v1}, Lcom/android/launcher2/FolderIcon;->c(Lcom/android/launcher2/FolderIcon;)Lcom/android/launcher2/ct;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/cm;->c:Lcom/android/launcher2/ct;

    iget v2, v2, Lcom/android/launcher2/ct;->c:F

    sub-float/2addr v2, v5

    mul-float/2addr v0, v2

    add-float/2addr v0, v5

    iput v0, v1, Lcom/android/launcher2/ct;->c:F

    .line 645
    iget-object v0, p0, Lcom/android/launcher2/cm;->a:Lcom/android/launcher2/FolderIcon;

    invoke-virtual {v0}, Lcom/android/launcher2/FolderIcon;->invalidate()V

    .line 646
    return-void
.end method
