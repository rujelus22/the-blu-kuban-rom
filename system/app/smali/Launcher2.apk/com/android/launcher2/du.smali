.class final Lcom/android/launcher2/du;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/android/launcher2/Launcher;

.field private final synthetic c:Landroid/view/View;

.field private final synthetic d:Z

.field private final synthetic e:Lcom/android/launcher2/AppsCustomizeTabHost;

.field private final synthetic f:Z


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Landroid/view/View;ZLcom/android/launcher2/AppsCustomizeTabHost;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/du;->c:Landroid/view/View;

    iput-boolean p3, p0, Lcom/android/launcher2/du;->d:Z

    iput-object p4, p0, Lcom/android/launcher2/du;->e:Lcom/android/launcher2/AppsCustomizeTabHost;

    iput-boolean p5, p0, Lcom/android/launcher2/du;->f:Z

    .line 3168
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 3169
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/du;->a:Z

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter

    .prologue
    .line 3196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/du;->a:Z

    .line 3197
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 3179
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/du;->c:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/launcher2/du;->d:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3180
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/du;->e:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-boolean v2, p0, Lcom/android/launcher2/du;->d:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3182
    iget-boolean v0, p0, Lcom/android/launcher2/du;->f:Z

    if-nez v0, :cond_2e

    .line 3184
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 3185
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3187
    :cond_29
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3189
    :cond_2e
    iget-boolean v0, p0, Lcom/android/launcher2/du;->a:Z

    if-nez v0, :cond_41

    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_41

    .line 3190
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3192
    :cond_41
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .registers 4
    .parameter

    .prologue
    .line 3173
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/du;->c:Landroid/view/View;

    iget-boolean v1, p0, Lcom/android/launcher2/du;->d:Z

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Z)V

    .line 3174
    iget-object v0, p0, Lcom/android/launcher2/du;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, p0, Lcom/android/launcher2/du;->e:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-boolean v1, p0, Lcom/android/launcher2/du;->d:Z

    invoke-static {v0, v1}, Lcom/android/launcher2/Launcher;->a(Landroid/view/View;Z)V

    .line 3175
    return-void
.end method
