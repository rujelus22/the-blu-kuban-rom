.class final Lcom/android/launcher2/ju;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/launcher2/Workspace;

.field private final synthetic b:Ljava/util/HashSet;


# direct methods
.method constructor <init>(Lcom/android/launcher2/Workspace;Ljava/util/HashSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/ju;->a:Lcom/android/launcher2/Workspace;

    iput-object p2, p0, Lcom/android/launcher2/ju;->b:Ljava/util/HashSet;

    .line 4113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    const/4 v2, 0x0

    .line 4116
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->d()Ljava/lang/String;

    move-result-object v0

    .line 4117
    iget-object v1, p0, Lcom/android/launcher2/ju;->a:Lcom/android/launcher2/Workspace;

    invoke-virtual {v1}, Lcom/android/launcher2/Workspace;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 4119
    const-string v1, "apps.new.list"

    .line 4120
    const/4 v2, 0x0

    .line 4119
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 4122
    iget-object v0, p0, Lcom/android/launcher2/ju;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1c
    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_23

    .line 4145
    return-void

    .line 4122
    :cond_23
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 4125
    iget-object v3, p0, Lcom/android/launcher2/ju;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v3}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/launcher2/Launcher;->e()Lcom/android/launcher2/gb;

    invoke-static {v0}, Lcom/android/launcher2/gb;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 4124
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 4126
    :goto_3a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_52

    .line 4130
    if-eqz v1, :cond_1c

    .line 4131
    monitor-enter v1

    .line 4132
    :try_start_43
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 4133
    :cond_47
    :goto_47
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_62

    .line 4131
    monitor-exit v1
    :try_end_4e
    .catchall {:try_start_43 .. :try_end_4e} :catchall_4f

    goto :goto_1c

    :catchall_4f
    move-exception v0

    monitor-exit v1

    throw v0

    .line 4124
    :cond_52
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jd;

    .line 4127
    iget-object v4, p0, Lcom/android/launcher2/ju;->a:Lcom/android/launcher2/Workspace;

    invoke-static {v4}, Lcom/android/launcher2/Workspace;->c(Lcom/android/launcher2/Workspace;)Lcom/android/launcher2/Launcher;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/android/launcher2/gb;->b(Landroid/content/Context;Lcom/android/launcher2/di;)V

    goto :goto_3a

    .line 4135
    :cond_62
    :try_start_62
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 4136
    invoke-static {v0}, Lcom/android/launcher2/di;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 4137
    iget-object v4, p0, Lcom/android/launcher2/ju;->b:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 4138
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_7c
    .catchall {:try_start_62 .. :try_end_7c} :catchall_4f
    .catch Ljava/net/URISyntaxException; {:try_start_62 .. :try_end_7c} :catch_7d

    goto :goto_47

    :catch_7d
    move-exception v0

    goto :goto_47
.end method
