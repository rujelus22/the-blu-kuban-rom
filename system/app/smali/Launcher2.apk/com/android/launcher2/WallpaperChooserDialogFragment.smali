.class public Lcom/android/launcher2/WallpaperChooserDialogFragment;
.super Landroid/app/DialogFragment;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private a:Z

.field private b:Landroid/graphics/Bitmap;

.field private c:Lcom/android/launcher2/jp;

.field private d:Lcom/android/launcher2/jn;

.field private e:Ljava/util/ArrayList;

.field private f:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->b:Landroid/graphics/Bitmap;

    .line 68
    new-instance v0, Lcom/android/launcher2/jn;

    invoke-direct {v0}, Lcom/android/launcher2/jn;-><init>()V

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->d:Lcom/android/launcher2/jn;

    .line 55
    return-void
.end method

.method public static a()Lcom/android/launcher2/WallpaperChooserDialogFragment;
    .registers 2

    .prologue
    .line 71
    new-instance v0, Lcom/android/launcher2/WallpaperChooserDialogFragment;

    invoke-direct {v0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;-><init>()V

    .line 72
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->setCancelable(Z)V

    .line 73
    return-object v0
.end method

.method static synthetic a(Lcom/android/launcher2/WallpaperChooserDialogFragment;)Ljava/util/ArrayList;
    .registers 2
    .parameter

    .prologue
    .line 398
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(I)V
    .registers 6
    .parameter

    .prologue
    .line 168
    :try_start_0
    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 169
    const-string v1, "wallpaper"

    .line 168
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/WallpaperManager;

    .line 171
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, v2, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 173
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 174
    iget-object v1, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/launcher2/jo;

    .line 175
    iget-object v3, v1, Lcom/android/launcher2/jo;->a:Landroid/content/res/Resources;

    iget v1, v1, Lcom/android/launcher2/jo;->b:I

    invoke-static {v3, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 176
    invoke-virtual {v0, v1}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 177
    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 178
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 179
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_36} :catch_37
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_36} :catch_4d

    .line 185
    :goto_36
    return-void

    .line 180
    :catch_37
    move-exception v0

    .line 181
    const-string v1, "Launcher.WallpaperChooserDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to set wallpaper: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_36

    .line 182
    :catch_4d
    move-exception v0

    .line 183
    const-string v1, "Launcher.WallpaperChooserDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to set wallpaper: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_36
.end method

.method private a(Landroid/content/res/Resources;Ljava/lang/String;I)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 229
    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-lt v0, v2, :cond_9

    .line 251
    return-void

    .line 229
    :cond_9
    aget-object v3, v1, v0

    .line 230
    new-instance v4, Lcom/android/launcher2/jo;

    invoke-direct {v4, p0}, Lcom/android/launcher2/jo;-><init>(Lcom/android/launcher2/WallpaperChooserDialogFragment;)V

    .line 231
    iput-object p1, v4, Lcom/android/launcher2/jo;->a:Landroid/content/res/Resources;

    .line 232
    const-string v5, "drawable"

    invoke-virtual {p1, v3, v5, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 233
    if-eqz v5, :cond_3e

    .line 234
    iput v5, v4, Lcom/android/launcher2/jo;->b:I

    .line 235
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_small"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 236
    const-string v7, "drawable"

    .line 235
    invoke-virtual {p1, v6, v7, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 238
    if-eqz v6, :cond_41

    .line 239
    iput v6, v4, Lcom/android/launcher2/jo;->c:I

    .line 248
    :goto_39
    iget-object v3, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_3e
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 241
    :cond_41
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "_thumb"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v6, "drawable"

    invoke-virtual {p1, v3, v6, p2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 242
    if-eqz v3, :cond_5f

    .line 243
    iput v3, v4, Lcom/android/launcher2/jo;->c:I

    goto :goto_39

    .line 245
    :cond_5f
    iput v5, v4, Lcom/android/launcher2/jo;->c:I

    goto :goto_39
.end method

.method static synthetic a(Lcom/android/launcher2/WallpaperChooserDialogFragment;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 166
    invoke-direct {p0, p1}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/android/launcher2/WallpaperChooserDialogFragment;Landroid/graphics/Bitmap;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->b:Landroid/graphics/Bitmap;

    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 7
    .parameter

    .prologue
    .line 421
    if-eqz p1, :cond_12

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_12

    .line 422
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_c
    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_13

    .line 437
    :cond_12
    return-void

    .line 422
    :cond_13
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 424
    :try_start_19
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 425
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->f:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v3

    .line 426
    const-string v0, "wallpapers"

    const-string v4, "array"

    invoke-virtual {v3, v0, v4, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 427
    if-nez v0, :cond_39

    .line 428
    const-string v0, "wallpaperlist"

    const-string v4, "array"

    invoke-virtual {v3, v0, v4, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 430
    :cond_39
    if-eqz v0, :cond_c

    .line 431
    invoke-direct {p0, v3, v2, v0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a(Landroid/content/res/Resources;Ljava/lang/String;I)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_3e} :catch_3f

    goto :goto_c

    :catch_3f
    move-exception v0

    goto :goto_c
.end method

.method static synthetic b(Lcom/android/launcher2/WallpaperChooserDialogFragment;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    invoke-virtual {v0}, Lcom/android/launcher2/jp;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_17

    .line 94
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/jp;->cancel(Z)Z

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    .line 97
    :cond_17
    return-void
.end method

.method static synthetic c(Lcom/android/launcher2/WallpaperChooserDialogFragment;)Lcom/android/launcher2/jn;
    .registers 2
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->d:Lcom/android/launcher2/jn;

    return-object v0
.end method

.method private c()V
    .registers 4

    .prologue
    const v2, 0x7f0c0034

    .line 209
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->e:Ljava/util/ArrayList;

    .line 211
    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 216
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object v1

    .line 218
    invoke-direct {p0, v0, v1, v2}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a(Landroid/content/res/Resources;Ljava/lang/String;I)V

    .line 222
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->f:Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/anddoes/launcher/c/b;->b(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a(Ljava/util/List;)V

    .line 223
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->f:Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/anddoes/launcher/c/c;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a(Ljava/util/List;)V

    .line 224
    invoke-direct {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->d()V

    .line 225
    return-void
.end method

.method private d()V
    .registers 6

    .prologue
    .line 401
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->f:Landroid/content/pm/PackageManager;

    invoke-static {v0}, Lcom/anddoes/launcher/c/a;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    .line 402
    if-eqz v0, :cond_18

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_18

    .line 403
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_12
    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_19

    .line 418
    :cond_18
    return-void

    .line 403
    :cond_19
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 405
    :try_start_1f
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 406
    iget-object v2, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->f:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 407
    const-string v3, "theme_wallpaper"

    const-string v4, "drawable"

    invoke-virtual {v2, v3, v4, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 408
    if-eqz v0, :cond_12

    .line 409
    new-instance v3, Lcom/android/launcher2/jo;

    invoke-direct {v3, p0}, Lcom/android/launcher2/jo;-><init>(Lcom/android/launcher2/WallpaperChooserDialogFragment;)V

    .line 410
    iput-object v2, v3, Lcom/android/launcher2/jo;->a:Landroid/content/res/Resources;

    .line 411
    iput v0, v3, Lcom/android/launcher2/jo;->c:I

    iput v0, v3, Lcom/android/launcher2/jo;->b:I

    .line 412
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_47} :catch_48

    goto :goto_12

    :catch_48
    move-exception v0

    goto :goto_12
.end method

.method static synthetic d(Lcom/android/launcher2/WallpaperChooserDialogFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->f:Landroid/content/pm/PackageManager;

    .line 80
    if-eqz p1, :cond_20

    const-string v0, "com.android.launcher2.WallpaperChooserDialogFragment.EMBEDDED_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 81
    const-string v0, "com.android.launcher2.WallpaperChooserDialogFragment.EMBEDDED_KEY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a:Z

    .line 85
    :goto_1f
    return-void

    .line 83
    :cond_20
    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->isInLayout()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a:Z

    goto :goto_1f
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c()V

    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-direct {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c()V

    .line 145
    iget-boolean v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a:Z

    if-eqz v0, :cond_40

    .line 146
    const v0, 0x7f030035

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 147
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->d:Lcom/android/launcher2/jn;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 149
    const v0, 0x7f0d0067

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    .line 150
    invoke-virtual {v0, v2}, Landroid/widget/Gallery;->setCallbackDuringFling(Z)V

    .line 151
    invoke-virtual {v0, p0}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 152
    new-instance v2, Lcom/android/launcher2/jm;

    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/launcher2/jm;-><init>(Lcom/android/launcher2/WallpaperChooserDialogFragment;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 154
    const v2, 0x7f0d0068

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 155
    new-instance v3, Lcom/android/launcher2/jl;

    invoke-direct {v3, p0, v0}, Lcom/android/launcher2/jl;-><init>(Lcom/android/launcher2/WallpaperChooserDialogFragment;Landroid/widget/Gallery;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 163
    :goto_3f
    return-object v0

    :cond_40
    const/4 v0, 0x0

    goto :goto_3f
.end method

.method public onDestroy()V
    .registers 1

    .prologue
    .line 108
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 110
    invoke-direct {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->b()V

    .line 111
    return-void
.end method

.method public onDetach()V
    .registers 1

    .prologue
    .line 101
    invoke-super {p0}, Landroid/app/DialogFragment;->onDetach()V

    .line 103
    invoke-direct {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->b()V

    .line 104
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 120
    invoke-virtual {p0}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_c

    .line 122
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 124
    :cond_c
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 190
    invoke-direct {p0, p3}, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a(I)V

    .line 191
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 196
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    invoke-virtual {v0}, Lcom/android/launcher2/jp;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_13

    .line 197
    iget-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    invoke-virtual {v0}, Lcom/android/launcher2/jp;->a()V

    .line 199
    :cond_13
    new-instance v0, Lcom/android/launcher2/jp;

    invoke-direct {v0, p0}, Lcom/android/launcher2/jp;-><init>(Lcom/android/launcher2/WallpaperChooserDialogFragment;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/launcher2/jp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/jp;

    iput-object v0, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->c:Lcom/android/launcher2/jp;

    .line 200
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .parameter

    .prologue
    .line 204
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 89
    const-string v0, "com.android.launcher2.WallpaperChooserDialogFragment.EMBEDDED_KEY"

    iget-boolean v1, p0, Lcom/android/launcher2/WallpaperChooserDialogFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    return-void
.end method
