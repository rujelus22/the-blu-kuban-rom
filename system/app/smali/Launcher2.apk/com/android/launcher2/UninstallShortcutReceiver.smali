.class public Lcom/android/launcher2/UninstallShortcutReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/ArrayList;

.field private static b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    sput-object v0, Lcom/android/launcher2/UninstallShortcutReceiver;->a:Ljava/util/ArrayList;

    .line 45
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/UninstallShortcutReceiver;->b:Z

    .line 35
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static a()V
    .registers 1

    .prologue
    .line 69
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/launcher2/UninstallShortcutReceiver;->b:Z

    .line 70
    return-void
.end method

.method static a(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/launcher2/UninstallShortcutReceiver;->b:Z

    .line 74
    sget-object v0, Lcom/android/launcher2/UninstallShortcutReceiver;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 75
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_10

    .line 79
    return-void

    .line 76
    :cond_10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/launcher2/ji;

    invoke-static {p0, v0}, Lcom/android/launcher2/UninstallShortcutReceiver;->a(Landroid/content/Context;Lcom/android/launcher2/ji;)V

    .line 77
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_9
.end method

.method private static a(Landroid/content/Context;Lcom/android/launcher2/ji;)V
    .registers 16
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-static {}, Lcom/android/launcher2/LauncherApplication;->d()Ljava/lang/String;

    move-result-object v1

    .line 84
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 86
    iget-object v2, p1, Lcom/android/launcher2/ji;->a:Landroid/content/Intent;

    .line 88
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/android/launcher2/LauncherApplication;

    .line 89
    monitor-enter v7

    .line 90
    :try_start_13
    const-string v1, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/content/Intent;

    move-object v8, v0

    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v1, "duplicate"

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    if-eqz v8, :cond_a9

    if-eqz v10, :cond_a9

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "intent"

    aput-object v5, v3, v4

    const-string v4, "title=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v10, v5, v6

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    const-string v2, "intent"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    const-string v2, "_id"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_59
    .catchall {:try_start_13 .. :try_end_59} :catchall_d0

    move-result v5

    const/4 v2, 0x0

    :cond_5b
    :goto_5b
    :try_start_5b
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5e
    .catchall {:try_start_5b .. :try_end_5e} :catchall_cb

    move-result v6

    if-nez v6, :cond_ab

    :goto_61
    :try_start_61
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    if-eqz v2, :cond_81

    sget-object v2, Lcom/android/launcher2/hm;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    const v1, 0x7f07028f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v10, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_81
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    const-string v2, "apps.new.list"

    invoke-interface {v9, v2, v1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    monitor-enter v2
    :try_end_8d
    .catchall {:try_start_61 .. :try_end_8d} :catchall_d0

    :cond_8d
    const/4 v1, 0x0

    :try_start_8e
    invoke-virtual {v8, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8d

    monitor-exit v2
    :try_end_9d
    .catchall {:try_start_8e .. :try_end_9d} :catchall_d3

    if-eqz v1, :cond_a9

    :try_start_9f
    new-instance v1, Lcom/android/launcher2/jh;

    const-string v3, "setNewAppsThread-remove"

    invoke-direct {v1, v3, v2, v9}, Lcom/android/launcher2/jh;-><init>(Ljava/lang/String;Ljava/util/Set;Landroid/content/SharedPreferences;)V

    invoke-virtual {v1}, Lcom/android/launcher2/jh;->start()V

    .line 89
    :cond_a9
    monitor-exit v7
    :try_end_aa
    .catchall {:try_start_9f .. :try_end_aa} :catchall_d0

    return-void

    .line 90
    :cond_ab
    :try_start_ab
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v12, 0x0

    invoke-static {v6, v12}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v8, v6}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v6

    if-eqz v6, :cond_5b

    invoke-interface {v3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/android/launcher2/hm;->a(J)Landroid/net/Uri;

    move-result-object v6

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v1, v6, v12, v13}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_c7
    .catchall {:try_start_ab .. :try_end_c7} :catchall_cb
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_c7} :catch_d6

    const/4 v2, 0x1

    if-nez v11, :cond_5b

    goto :goto_61

    :catchall_cb
    move-exception v1

    :try_start_cc
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_d0
    .catchall {:try_start_cc .. :try_end_d0} :catchall_d0

    .line 89
    :catchall_d0
    move-exception v1

    monitor-exit v7

    throw v1

    .line 90
    :catchall_d3
    move-exception v1

    :try_start_d4
    monitor-exit v2

    throw v1
    :try_end_d6
    .catchall {:try_start_d4 .. :try_end_d6} :catchall_d0

    :catch_d6
    move-exception v6

    goto :goto_5b
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 56
    const-string v0, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 66
    :goto_c
    return-void

    .line 60
    :cond_d
    new-instance v0, Lcom/android/launcher2/ji;

    invoke-direct {v0, p2}, Lcom/android/launcher2/ji;-><init>(Landroid/content/Intent;)V

    .line 61
    sget-boolean v1, Lcom/android/launcher2/UninstallShortcutReceiver;->b:Z

    if-eqz v1, :cond_1c

    .line 62
    sget-object v1, Lcom/android/launcher2/UninstallShortcutReceiver;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 64
    :cond_1c
    invoke-static {p1, v0}, Lcom/android/launcher2/UninstallShortcutReceiver;->a(Landroid/content/Context;Lcom/android/launcher2/ji;)V

    goto :goto_c
.end method
