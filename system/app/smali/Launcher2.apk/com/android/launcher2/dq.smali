.class final Lcom/android/launcher2/dq;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/android/launcher2/Launcher;

.field private final synthetic c:Lcom/android/launcher2/AppsCustomizeTabHost;

.field private final synthetic d:Landroid/view/View;

.field private final synthetic e:Z

.field private final synthetic f:Z


# direct methods
.method constructor <init>(Lcom/android/launcher2/Launcher;Lcom/android/launcher2/AppsCustomizeTabHost;Landroid/view/View;ZZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    iput-object p2, p0, Lcom/android/launcher2/dq;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    iput-object p3, p0, Lcom/android/launcher2/dq;->d:Landroid/view/View;

    iput-boolean p4, p0, Lcom/android/launcher2/dq;->e:Z

    iput-boolean p5, p0, Lcom/android/launcher2/dq;->f:Z

    .line 3051
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    .line 3052
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/launcher2/dq;->a:Z

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .registers 3
    .parameter

    .prologue
    .line 3090
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/launcher2/dq;->a:Z

    .line 3091
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 3066
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dq;->d:Landroid/view/View;

    iget-boolean v2, p0, Lcom/android/launcher2/dq;->e:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3067
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    iget-object v1, p0, Lcom/android/launcher2/dq;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    iget-boolean v2, p0, Lcom/android/launcher2/dq;->e:Z

    invoke-static {v0, v1, v2, v3}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;Landroid/view/View;ZZ)V

    .line 3069
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    iget-object v0, v0, Lcom/android/launcher2/Launcher;->h:Lcom/anddoes/launcher/preference/f;

    iget v0, v0, Lcom/anddoes/launcher/preference/f;->B:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_4b

    .line 3070
    iget-boolean v0, p0, Lcom/android/launcher2/dq;->f:Z

    if-nez v0, :cond_38

    .line 3072
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 3073
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->e(Z)V

    .line 3075
    :cond_33
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->q()V

    .line 3077
    :cond_38
    iget-boolean v0, p0, Lcom/android/launcher2/dq;->a:Z

    if-nez v0, :cond_41

    .line 3078
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    invoke-virtual {v0, v3}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3082
    :cond_41
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->t(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/SearchDropTargetBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/launcher2/SearchDropTargetBar;->b(Z)V

    .line 3086
    :goto_4a
    return-void

    .line 3084
    :cond_4b
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    invoke-static {v0}, Lcom/android/launcher2/Launcher;->a(Lcom/android/launcher2/Launcher;)Lcom/android/launcher2/Workspace;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Workspace;->setVisibility(I)V

    goto :goto_4a
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 3056
    iget-object v0, p0, Lcom/android/launcher2/dq;->b:Lcom/android/launcher2/Launcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/Launcher;->b(Z)V

    .line 3058
    iget-object v0, p0, Lcom/android/launcher2/dq;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationX(F)V

    .line 3059
    iget-object v0, p0, Lcom/android/launcher2/dq;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/AppsCustomizeTabHost;->setTranslationY(F)V

    .line 3060
    iget-object v0, p0, Lcom/android/launcher2/dq;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/launcher2/AppsCustomizeTabHost;->setVisibility(I)V

    .line 3061
    iget-object v0, p0, Lcom/android/launcher2/dq;->c:Lcom/android/launcher2/AppsCustomizeTabHost;

    invoke-virtual {v0}, Lcom/android/launcher2/AppsCustomizeTabHost;->bringToFront()V

    .line 3062
    return-void
.end method
