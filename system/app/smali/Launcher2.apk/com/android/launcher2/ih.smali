.class public abstract Lcom/android/launcher2/ih;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/android/launcher2/PagedView;


# direct methods
.method protected constructor <init>(Lcom/android/launcher2/PagedView;)V
    .registers 2
    .parameter

    .prologue
    .line 2872
    iput-object p1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 12
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 2874
    .line 2876
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget v0, v0, Lcom/android/launcher2/PagedView;->v:I

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_3f

    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget v0, v0, Lcom/android/launcher2/PagedView;->v:I

    :goto_f
    move v1, v2

    move v3, v2

    move v4, v2

    .line 2878
    :goto_12
    iget-object v6, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v6}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v6

    if-lt v1, v6, :cond_44

    .line 2895
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget-boolean v0, v0, Lcom/android/launcher2/PagedView;->aq:Z

    if-eqz v0, :cond_9a

    .line 2896
    if-eqz v4, :cond_83

    .line 2897
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2898
    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    .line 2899
    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v1, p1}, Lcom/android/launcher2/PagedView;->m(I)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/ih;->a(Landroid/view/View;F)V

    .line 2913
    :cond_3e
    :goto_3e
    return-void

    .line 2876
    :cond_3f
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget v0, v0, Lcom/android/launcher2/PagedView;->u:I

    goto :goto_f

    .line 2879
    :cond_44
    iget-object v6, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v6, v1}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v6

    .line 2880
    if-eqz v6, :cond_6b

    .line 2881
    iget-object v7, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v7, p1, v6, v1}, Lcom/android/launcher2/PagedView;->a(ILandroid/view/View;I)F

    move-result v7

    .line 2882
    if-eq v1, v0, :cond_5c

    add-int/lit8 v8, v0, -0x1

    if-eq v1, v8, :cond_5c

    add-int/lit8 v8, v0, 0x1

    if-ne v1, v8, :cond_6e

    .line 2883
    :cond_5c
    iget-object v8, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v8, v6}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    .line 2884
    invoke-virtual {p0, v6, v7}, Lcom/android/launcher2/ih;->a(Landroid/view/View;F)V

    .line 2888
    :goto_64
    if-nez v1, :cond_73

    cmpg-float v6, v7, v9

    if-gez v6, :cond_73

    move v4, v5

    .line 2878
    :cond_6b
    :goto_6b
    add-int/lit8 v1, v1, 0x1

    goto :goto_12

    .line 2886
    :cond_6e
    const/4 v8, 0x4

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_64

    .line 2890
    :cond_73
    iget-object v6, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v6}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne v1, v6, :cond_6b

    cmpl-float v6, v7, v9

    if-lez v6, :cond_6b

    move v3, v5

    .line 2891
    goto :goto_6b

    .line 2900
    :cond_83
    if-eqz v3, :cond_3e

    .line 2901
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v0, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2902
    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v1, v0}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    .line 2903
    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v1, p1}, Lcom/android/launcher2/PagedView;->n(I)F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/launcher2/ih;->a(Landroid/view/View;F)V

    goto :goto_3e

    .line 2906
    :cond_9a
    if-eqz v4, :cond_ad

    .line 2907
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v1, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    .line 2911
    :cond_a7
    :goto_a7
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v0, p1}, Lcom/android/launcher2/PagedView;->f(I)V

    goto :goto_3e

    .line 2908
    :cond_ad
    if-eqz v3, :cond_a7

    .line 2909
    iget-object v0, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget-object v1, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    iget-object v2, p0, Lcom/android/launcher2/ih;->b:Lcom/android/launcher2/PagedView;

    invoke-virtual {v2}, Lcom/android/launcher2/PagedView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/android/launcher2/PagedView;->a(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/launcher2/PagedView;->c(Landroid/view/View;)V

    goto :goto_a7
.end method

.method protected a(Landroid/view/View;F)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 2916
    return-void
.end method
