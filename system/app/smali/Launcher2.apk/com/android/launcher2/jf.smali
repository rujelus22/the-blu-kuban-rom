.class public final Lcom/android/launcher2/jf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private a:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const v0, 0x3fa66666

    iput v0, p0, Lcom/android/launcher2/jf;->a:F

    .line 43
    return-void
.end method


# virtual methods
.method public final a()V
    .registers 2

    .prologue
    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/launcher2/jf;->a:F

    .line 51
    return-void
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    const v0, 0x3fa66666

    .line 46
    if-lez p1, :cond_7

    int-to-float v1, p1

    div-float/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/android/launcher2/jf;->a:F

    .line 47
    return-void
.end method

.method public final getInterpolation(F)F
    .registers 6
    .parameter

    .prologue
    const/high16 v3, 0x3f80

    .line 56
    sub-float v0, p1, v3

    .line 57
    mul-float v1, v0, v0

    iget v2, p0, Lcom/android/launcher2/jf;->a:F

    add-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/android/launcher2/jf;->a:F

    add-float/2addr v0, v2

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    return v0
.end method
