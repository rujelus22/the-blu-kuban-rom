.class final Lcom/android/launcher2/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/launcher2/AppsCustomizePagedView;

.field private final synthetic b:Landroid/appwidget/AppWidgetProviderInfo;

.field private final synthetic c:Lcom/android/launcher2/iw;


# direct methods
.method constructor <init>(Lcom/android/launcher2/AppsCustomizePagedView;Landroid/appwidget/AppWidgetProviderInfo;Lcom/android/launcher2/iw;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    iput-object p2, p0, Lcom/android/launcher2/m;->b:Landroid/appwidget/AppWidgetProviderInfo;

    iput-object p3, p0, Lcom/android/launcher2/m;->c:Lcom/android/launcher2/iw;

    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 679
    iget-object v0, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v0}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/Launcher;

    move-result-object v0

    .line 680
    invoke-virtual {v0}, Lcom/android/launcher2/Launcher;->d()Lcom/android/launcher2/fx;

    move-result-object v0

    iget-object v1, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-virtual {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    iget v2, v2, Lcom/android/launcher2/AppsCustomizePagedView;->d:I

    iget-object v3, p0, Lcom/android/launcher2/m;->b:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/launcher2/fx;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v0

    .line 681
    iget-object v1, p0, Lcom/android/launcher2/m;->c:Lcom/android/launcher2/iw;

    iput-object v0, v1, Lcom/android/launcher2/iw;->u:Landroid/appwidget/AppWidgetHostView;

    .line 682
    iget-object v1, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    const/4 v2, 0x2

    iput v2, v1, Lcom/android/launcher2/AppsCustomizePagedView;->c:I

    .line 683
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetHostView;->setVisibility(I)V

    .line 684
    iget-object v1, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->k()Lcom/android/launcher2/Workspace;

    move-result-object v1

    iget-object v2, p0, Lcom/android/launcher2/m;->c:Lcom/android/launcher2/iw;

    iget v2, v2, Lcom/android/launcher2/iw;->n:I

    .line 685
    iget-object v3, p0, Lcom/android/launcher2/m;->c:Lcom/android/launcher2/iw;

    iget v3, v3, Lcom/android/launcher2/iw;->o:I

    iget-object v4, p0, Lcom/android/launcher2/m;->c:Lcom/android/launcher2/iw;

    .line 684
    invoke-virtual {v1, v2, v3, v5}, Lcom/android/launcher2/Workspace;->a(IIZ)[I

    move-result-object v1

    .line 689
    new-instance v2, Lcom/android/launcher2/DragLayer$LayoutParams;

    aget v3, v1, v5

    .line 690
    aget v1, v1, v6

    .line 689
    invoke-direct {v2, v3, v1}, Lcom/android/launcher2/DragLayer$LayoutParams;-><init>(II)V

    .line 691
    iput v5, v2, Lcom/android/launcher2/DragLayer$LayoutParams;->b:I

    iput v5, v2, Lcom/android/launcher2/DragLayer$LayoutParams;->a:I

    .line 692
    iput-boolean v6, v2, Lcom/android/launcher2/DragLayer$LayoutParams;->c:Z

    .line 693
    invoke-virtual {v0, v2}, Landroid/appwidget/AppWidgetHostView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 694
    iget-object v1, p0, Lcom/android/launcher2/m;->a:Lcom/android/launcher2/AppsCustomizePagedView;

    invoke-static {v1}, Lcom/android/launcher2/AppsCustomizePagedView;->a(Lcom/android/launcher2/AppsCustomizePagedView;)Lcom/android/launcher2/Launcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/launcher2/Launcher;->a()Lcom/android/launcher2/DragLayer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/launcher2/DragLayer;->addView(Landroid/view/View;)V

    .line 695
    return-void
.end method
