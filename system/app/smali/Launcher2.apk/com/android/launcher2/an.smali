.class final Lcom/android/launcher2/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field final synthetic a:Lcom/android/launcher2/CellLayout;

.field private final synthetic b:Lcom/android/launcher2/dg;

.field private final synthetic c:I


# direct methods
.method constructor <init>(Lcom/android/launcher2/CellLayout;Lcom/android/launcher2/dg;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lcom/android/launcher2/an;->a:Lcom/android/launcher2/CellLayout;

    iput-object p2, p0, Lcom/android/launcher2/an;->b:Lcom/android/launcher2/dg;

    iput p3, p0, Lcom/android/launcher2/an;->c:I

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 5
    .parameter

    .prologue
    .line 279
    iget-object v0, p0, Lcom/android/launcher2/an;->b:Lcom/android/launcher2/dg;

    iget-object v0, v0, Lcom/android/launcher2/dg;->b:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 283
    if-nez v0, :cond_c

    .line 285
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 297
    :goto_b
    return-void

    .line 294
    :cond_c
    iget-object v0, p0, Lcom/android/launcher2/an;->a:Lcom/android/launcher2/CellLayout;

    invoke-static {v0}, Lcom/android/launcher2/CellLayout;->c(Lcom/android/launcher2/CellLayout;)[F

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/an;->c:I

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v1, v2

    .line 295
    iget-object v0, p0, Lcom/android/launcher2/an;->a:Lcom/android/launcher2/CellLayout;

    iget-object v1, p0, Lcom/android/launcher2/an;->a:Lcom/android/launcher2/CellLayout;

    invoke-static {v1}, Lcom/android/launcher2/CellLayout;->d(Lcom/android/launcher2/CellLayout;)[Landroid/graphics/Rect;

    move-result-object v1

    iget v2, p0, Lcom/android/launcher2/an;->c:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/launcher2/CellLayout;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_b
.end method
